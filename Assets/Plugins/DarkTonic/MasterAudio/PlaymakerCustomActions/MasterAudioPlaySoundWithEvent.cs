﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using DarkTonic.MasterAudio;
using HutongGames.PlayMaker;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;

[ActionCategory(ActionCategory.Audio)]
[Tooltip("Play a Sound in Master Audio and wait till sound is finished, then fire events")]
public class MasterAudioPlaySoundWithEvent : FsmStateAction{
	[RequiredField]
	[Tooltip("Name of Master Audio Sound Group")]
	public FsmString soundGroupName;

	[Tooltip("Name of specific variation (optional)")]
	public FsmString variationName;
		
	[RequiredField]
	[HasFloatSlider(0,1)]
	public FsmFloat volume = new FsmFloat(1f);

	[TooltipAttribute("Fixed Pitch will be used only if 'Use Fixed Pitch' is checked above.")]
	[HasFloatSlider(-3,3)]
	public FsmFloat fixedPitch = new FsmFloat(1f);

	[HasFloatSlider(0,10)]
	public FsmFloat delaySound = new FsmFloat(0f);

	[Tooltip("Event to send when the AudioClip finishes playing.")]
	public FsmEvent finishedEvent;

	PlaySoundResult sound;

	public override void Reset(){
	    volume = 1f;
        finishedEvent = null;
	}
		
	public override void OnEnter(){
	    sound = MasterAudio.PlaySound(soundGroupName.Value, volume.Value, fixedPitch.Value, delaySound.Value, variationName.Value);
        if(sound != null){
			return;
        }

		Finish();
    }

	public override void OnUpdate (){
	    if(sound == null){
	        Finish();
        }else{
            if(finishedEvent != null){
	            if (!sound.ActingVariation.IsPlaying){
	                Fsm.Event(finishedEvent);
                    Finish();
				}
                return;
            }
        }
    }
}
	