﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class GetFsmSharedVariable : FsmSharedVariableAccess 
	{
		#region Protected Methods
		protected NamedVariable Get( string key )
		{
			if( PlayMakerSharedVariables.Get( key ) == null )
			{
				Debug.LogError( "Error - couldnt find the Shared Variable with the key : " + key );
			}

			return PlayMakerSharedVariables.Get( key );
		}
		#endregion
	}
}