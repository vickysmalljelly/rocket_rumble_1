﻿
using HutongGames.PlayMaker;
using SmallJelly;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	//Sets the playmaker shared variables by using an fsm action component - this is the equivalent of
	//FsmSharedVariableAccess but using an action instead of a MonoBehaviour
	public class FsmSharedVariableAccess : FsmStateAction
	{
		#region PlayMaker Variables
		[RequiredField]
		[Tooltip("The GameObject to access.")]
		public FsmOwnerDefault GameObject;
		#endregion

		#region Protected Properties
		protected PlayMakerSharedVariables PlayMakerSharedVariables
		{
			get
			{
				GameObject targetGameObject = Fsm.GetOwnerDefaultTarget(GameObject);
				PlayMakerSharedVariables playMakerSharedVariables = targetGameObject.GetComponent<PlayMakerSharedVariables>();
				if( playMakerSharedVariables == null )
				{
					playMakerSharedVariables = targetGameObject.AddComponent<PlayMakerSharedVariables>();
				}

				return playMakerSharedVariables;
			}
		}
		#endregion
	}
}