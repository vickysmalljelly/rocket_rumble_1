﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetBoolSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmBool LocalVariable;
		#endregion

		#region Public Methods
		public override void OnEnter()
		{
			FsmBool retreivedBool = ((FsmBool)Get( LocalVariable.Name ));
			LocalVariable.Value = retreivedBool.Value;
			Finish();
		}
		#endregion
	}
}