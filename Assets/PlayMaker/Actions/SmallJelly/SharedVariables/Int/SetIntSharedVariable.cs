﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetIntSharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmInt LocalVariable;
		#endregion

		#region Public Methods
		public override void OnEnter()
		{
			Set( LocalVariable.Name, LocalVariable );
			Finish();
		}
		#endregion
	}
}