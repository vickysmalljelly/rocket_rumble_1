﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetIntSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmInt LocalVariable;
		#endregion

		#region Public Methods
		public override void OnEnter()
		{
			FsmInt retreivedInt = ((FsmInt)Get( LocalVariable.Name ));
			LocalVariable.Value = retreivedInt.Value;
			Finish();
		}
		#endregion
	}
}