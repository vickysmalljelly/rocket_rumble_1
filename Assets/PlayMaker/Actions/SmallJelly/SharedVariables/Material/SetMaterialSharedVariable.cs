﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetMaterialSharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables 
		public FsmMaterial LocalMaterial;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			Set( LocalMaterial.Name, LocalMaterial );
			Finish();
		}
		#endregion
	}
}