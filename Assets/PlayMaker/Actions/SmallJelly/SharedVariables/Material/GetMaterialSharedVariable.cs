﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetMaterialSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmMaterial LocalVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			FsmMaterial retreivedMaterial = ((FsmMaterial)Get( LocalVariable.Name ));
			LocalVariable.Value = retreivedMaterial.Value;
			Finish();
		}
		#endregion
	}
}