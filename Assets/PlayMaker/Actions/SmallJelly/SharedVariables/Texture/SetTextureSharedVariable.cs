﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetTextureSharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmTexture localVariable;
		public FsmTexture textureReference;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			localVariable.Value = textureReference.Value;
			Set( localVariable.Name, localVariable );
			Finish();
		}
		#endregion
	}
}