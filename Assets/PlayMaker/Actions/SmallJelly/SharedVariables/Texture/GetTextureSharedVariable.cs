﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetTextureSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmTexture localVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			FsmTexture retreivedTexture = ((FsmTexture)Get( localVariable.Name ));
			localVariable.Value = retreivedTexture.Value;
			Finish();
		}
		#endregion
	}
}