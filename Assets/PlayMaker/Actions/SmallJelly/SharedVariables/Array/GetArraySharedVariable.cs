﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetArraySharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Array Variable to use.")]
		public FsmArray localVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			FsmArray retrievedArray = ((FsmArray)Get( localVariable.Name ));
			localVariable.Values = retrievedArray.Values;
			Finish();
		}
		#endregion
	}
}