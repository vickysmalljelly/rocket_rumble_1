﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetArraySharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Array Variable to use.")]
		public FsmArray localVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			Set( localVariable.Name, localVariable );
			Finish();
		}
		#endregion
	}
}