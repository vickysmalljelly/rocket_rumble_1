﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetGameObjectSharedVariable : SetFsmSharedVariable
	{
		public FsmGameObject localVariable;

		public override void OnEnter()
		{
			Set( localVariable.Name, localVariable );

			Finish();
		}
	}
}