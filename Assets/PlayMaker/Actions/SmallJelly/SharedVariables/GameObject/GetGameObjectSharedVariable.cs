﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetGameObjectSharedVariable : GetFsmSharedVariable
	{
		public FsmGameObject localVariable;

		public override void OnEnter()
		{
			FsmGameObject retreivedGameObject = ((FsmGameObject)Get( localVariable.Name ));
			localVariable.Value = retreivedGameObject.Value;

			Finish();
		}
	}
}