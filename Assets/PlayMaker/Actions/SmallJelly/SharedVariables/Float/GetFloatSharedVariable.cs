﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetFloatSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmFloat LocalVariable;
		#endregion

		#region Public Methods
		public override void OnEnter()
		{
			FsmFloat retreivedFloat = ((FsmFloat)Get( LocalVariable.Name ));
			LocalVariable.Value = retreivedFloat.Value;
			Finish();
		}
		#endregion
	}
}