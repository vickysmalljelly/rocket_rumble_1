﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetFloatSharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmFloat LocalVariable;
		#endregion

		#region Public Methods
		public override void OnEnter()
		{
			Set( LocalVariable.Name, LocalVariable );
			Finish();
		}
		#endregion
	}
}