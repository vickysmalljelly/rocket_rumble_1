﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	public class RemoveFsmSharedVariable : FsmSharedVariableAccess 
	{
		#region Protected Methods
		protected void Remove(string key)
		{
			PlayMakerSharedVariables.Remove(key);
		}
		#endregion
	}
}