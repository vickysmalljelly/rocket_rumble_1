﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetStringSharedVariable : SetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmString LocalVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			Set( LocalVariable.Name, LocalVariable );
			Finish();
		}
		#endregion
	}
}