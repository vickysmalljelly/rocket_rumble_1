﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetStringSharedVariable : GetFsmSharedVariable
	{
		#region PlayMaker Variables
		public FsmString LocalVariable;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			FsmString retrivedString = ((FsmString)Get( LocalVariable.Name ));
			LocalVariable.Value = retrivedString.Value;
			Finish();
		}
		#endregion
	}
}