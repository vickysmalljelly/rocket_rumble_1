﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class ContainsFsmSharedVariable : FsmSharedVariableAccess
	{
		#region PlayMaker Variables
		public FsmString LocalVariable;

		public FsmEvent Contains;
		public FsmEvent DoesntContain;
		#endregion

		#region Protected Methods
		public override void OnEnter()
		{
			if( PlayMakerSharedVariables.ContainsKey( LocalVariable.Value ) )
			{
				Fsm.Event( Contains );
			}
			else
			{
				Fsm.Event( DoesntContain );
			}
		}
		#endregion
	}
}