﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class SetVector3SharedVariable : SetFsmSharedVariable
	{
		public FsmVector3 localVariable;

		public override void OnEnter()
		{
			Set( localVariable.Name, localVariable );
			Finish();
		}
	}
}