﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-SharedVariables")]
	[Tooltip("")]
	public class GetVector3SharedVariable : GetFsmSharedVariable
	{
		public FsmVector3 localVariable;

		public override void OnEnter()
		{
			FsmVector3 retreivedVector3 = ((FsmVector3)Get( localVariable.Name ));
			localVariable.Value = retreivedVector3.Value;
			Finish();
		}
	}
}