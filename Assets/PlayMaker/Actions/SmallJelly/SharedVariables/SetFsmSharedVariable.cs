﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	public class SetFsmSharedVariable : FsmSharedVariableAccess
	{
		#region Protected Methods
		protected void Set( string key, NamedVariable namedVariable )
		{
			PlayMakerSharedVariables.Add(key, namedVariable);
		}
		#endregion
	}
}