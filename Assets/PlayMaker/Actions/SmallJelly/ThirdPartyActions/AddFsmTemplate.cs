﻿using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions {
	[ActionCategory(ActionCategory.StateMachine)]
	public class AddFsmTemplate : FsmStateAction 
	{
		[RequiredField]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		public FsmTemplate template;
		public FsmString name;
		public FsmBool active;
		public FsmBool unique;
		[CompoundArray("Variables", "Name", "Variable")]
		[RequiredField]
		public FsmString[] variableNames;
		[RequiredField]
		public FsmVar[] variables;

		private GameObject previousGo;
		private List<PlayMakerFSM> fsms;

		public override void Reset() {
			gameObject = null;
			template = null;
			name = new FsmString { UseVariable = true };
			active = new FsmBool { Value = true };
			unique = new FsmBool { Value = false };
			variableNames = new FsmString[0];
			variables = new FsmVar[0];
		}

		public override void OnEnter() {
			var go = Fsm.GetOwnerDefaultTarget( gameObject );

			if ( go == null ) {
				return;
			}

			bool exists = false;

			if ( ! unique.Value ) {
				if ( go != previousGo ) {
					fsms = new List<PlayMakerFSM>();

					fsms.AddRange( go.GetComponents<PlayMakerFSM>() );

					previousGo = go;
				}

				if ( fsms.Count > 0 ) foreach ( PlayMakerFSM fsm in fsms ) {
						if ( ( ( name.Value != "" ) && ( fsm.FsmName == name.Value ) ) || ( ( fsm.FsmTemplate != null ) && ( fsm.FsmTemplate.name == template.name ) ) ) {
							exists = true;
						}
					}
			}

			if ( ! exists ) {
				PlayMakerFSM newFsm = go.AddComponent<PlayMakerFSM>();

				if ( name.Value != "" ) {
					newFsm.FsmName = name.Value;
				}

				if ( ( ! active.Value ) || ( variableNames.Length > 0 ) ) {
					newFsm.enabled = false;
				}

				newFsm.SetFsmTemplate( template );

				if ( variableNames.Length > 0 ) {
					if ( variableNames.Length > 0 ) for ( int i = 0; i < variableNames.Length; i++ ) {
							if ( ! variableNames[i].IsNone ) {
								NamedVariable target = newFsm.Fsm.Variables.GetVariable( variableNames[i].Value );

								if ( target != null ) {
									variables[i].ApplyValueTo( target );
								}
							}
						}

					if ( active.Value && ( ! newFsm.enabled ) ) {
						newFsm.enabled = true;
					}
				}

				if ( ! unique.Value ) {
					fsms.Add( newFsm );
				}
			}

			Finish();
		}
	}
}