﻿using UnityEngine;

namespace SmallJelly
{
	//PlayMaker requires serialized types to be of type "UnityEngine.Object" - this is just a wrapped for the "Ray" classs
	public class RayObject : Object
	{
		#region Public Properties
		public Vector2 ScreenPosition 
		{ 
			get 
			{ 
				return mScreenPosition; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly Vector2 mScreenPosition;
		#endregion

		#region Constructors
		public RayObject(Vector2 screenPosition)
		{
			mScreenPosition = screenPosition;
		}
		#endregion
	}
}