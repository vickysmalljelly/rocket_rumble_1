﻿
using UnityEngine;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Sets the shared material on a game object.")]
	public class SetSharedMaterial : ComponentAction<Renderer>
	{
		#region PlayMaker Variables
		[RequiredField]
		public FsmOwnerDefault GameObject;

		[RequiredField]
		public FsmMaterial Material;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			GameObject = null;
			Material = null;
		}

		public override void OnEnter()
		{
			var gameObject = Fsm.GetOwnerDefaultTarget(GameObject);
			if (!UpdateCache(gameObject))
			{
				return;
			}

			SJLogger.AssertCondition( renderer != null, "{0} must have a renderer component", gameObject.name );

			renderer.sharedMaterials = new UnityEngine.Material[1]{ Material.Value };

			Finish();
		}
		#endregion
	}
}