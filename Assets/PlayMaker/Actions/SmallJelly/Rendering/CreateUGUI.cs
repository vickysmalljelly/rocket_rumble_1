﻿
using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	public class CreateUGUI : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmTexture Texture;
		public FsmGameObject Output;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Texture = default( FsmTexture );
		}

		public override void OnEnter()
		{
			Sprite sprite = Sprite.Create( (Texture2D)Texture.Value, new Rect( 0, 0, Texture.Value.width, Texture.Value.height ), new Vector2( 0.5f, 0.5f ) );
			GameObject gameObject = new GameObject( "Sprite" );
		
			RectTransform rectTransform = gameObject.AddComponent< RectTransform >();
			gameObject.AddComponent< CanvasRenderer >();
			gameObject.AddComponent< Image >().sprite = sprite;

			rectTransform.anchorMin = Vector2.zero;
			rectTransform.anchorMax = Vector2.one;
			rectTransform.anchoredPosition = Vector2.zero;

			Output.Value = gameObject;

			Finish();
		}
		#endregion
	}
}