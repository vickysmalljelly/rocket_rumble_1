// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Sets a layer of an object")]
	public class SetRenderLayer : FsmStateAction
	{
		#region Public Variables
		[RequiredField]
		public FsmOwnerDefault gameObject;
		[UIHint(UIHint.Layer)]
		public FsmInt layer;
		#endregion

		public override void Reset()
		{
			gameObject = null;
			layer = 0;
		}

		public override void OnEnter()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (go == null) return;

			go.layer = layer.Value;
			
			Finish();
		}

	}
}