﻿
using UnityEngine;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	public class SharedMaterialArrayAction : FsmStateAction
	{
		#region Protected Methods
		protected List< MeshRenderer> GetOrderedMeshRenderer( Transform parent )
		{
			List< Transform > transforms = GetOrderedChildren( parent );
			List< MeshRenderer> meshRenderers = new List< MeshRenderer>();

			foreach( Transform transform in transforms )
			{
				MeshRenderer meshRenderer = transform.gameObject.GetComponent< MeshRenderer >();

				if( meshRenderer != null )
				{
					meshRenderers.Add( meshRenderer );
				}
			}

			return meshRenderers;
		}
		#endregion

		#region Private Methods
		private List< Transform > GetOrderedChildren( Transform parent )
		{
			List< Transform > currentTransforms = new List< Transform >();
			foreach( Transform transform in parent )
			{
				currentTransforms.Add( transform );

				currentTransforms.AddRange( GetOrderedChildren( transform ) );
			}

			return currentTransforms;
		}
		#endregion
	}
}