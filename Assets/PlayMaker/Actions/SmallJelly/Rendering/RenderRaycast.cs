﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Performs raycast against a container")]
	public class RenderRaycast : FsmStateAction 
	{
		public FsmObject rayToCast;
		public FsmInt[] indexesToRetreive;
		public FsmGameObject[] containerToRaycastAgainst;
		public FsmGameObject[] containersToIgnore;

		public FsmEvent hit;
		public FsmEvent missed;

		public FsmGameObject hitGameObject;
		public FsmVector3 hitPosition;

		public FsmBool runEveryFrame;

		#region Public Methods
		public override void Reset()
		{
			rayToCast = default(FsmObject);
			containerToRaycastAgainst = default(FsmGameObject[]);

			hitGameObject = default(FsmGameObject);
			hitPosition = default(FsmVector3);
		}

		public override void OnEnter()
		{
			Raycast();

			if( !runEveryFrame.Value )
			{
				Finish();
			}
		}

		public override void OnUpdate()
		{
			Raycast();
		}
		#endregion


		#region Private Methods
		private void UpdateInformation(SJRenderRaycastHit[] renderRaycastHit)
		{
			//Work our way through the indexes to return list - we're interested in the first element, if not then the second element and so on
			for( int i = 0; i < indexesToRetreive.Length; i++ )
			{
				if( indexesToRetreive[ i ].Value < renderRaycastHit.Length )
				{
					hitGameObject.Value = renderRaycastHit[ indexesToRetreive[ i ].Value ].Collider.gameObject;
					hitPosition.Value = renderRaycastHit[ indexesToRetreive[ i ].Value ].Point;

					return;
				}
			}
		}

		private void Raycast()
		{
			SJLogger.AssertCondition( rayToCast.Value is RayObject, "Ray to cast must be of type \"RayObject\"" );

			Transform[] containersToIgnoreGameObjects = new Transform[ containersToIgnore.Length ];
			for( int i = 0; i < containersToIgnoreGameObjects.Length; i++ )
			{
				containersToIgnoreGameObjects[ i ] = containersToIgnore[ i ].Value.transform;
			}

			for( int i = 0; i < containerToRaycastAgainst.Length; i++ )
			{
				Vector2 screenPosition = ((RayObject)rayToCast.Value).ScreenPosition;
				SJRenderRaycastHit[] renderRaycastHit;

				bool hit = SJRenderCanvas.Get.Raycast( containerToRaycastAgainst[ i ].Value.transform, containersToIgnoreGameObjects, screenPosition, out renderRaycastHit );

				if( hit )
				{
					UpdateInformation(renderRaycastHit);
					OnHit();

					//We dont need to try raycast against any more
					return;
				}
			}
			OnMissed();
		}
		#endregion

		#region Event Firing
		private void OnHit()
		{
			Fsm.Event(hit);
		}

		private void OnMissed()
		{
			Fsm.Event(missed);
		}
		#endregion
	}
}