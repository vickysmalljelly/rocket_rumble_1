﻿
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using UnityEngine;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Creates an instance of a material")]
	public class InstanceMaterial : FsmStateAction
	{
		#region Public Variables
		public FsmMaterial OriginalMaterial;
		public FsmMaterial CopiedMaterial;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			OriginalMaterial = null;
			CopiedMaterial = null;
		}
		
		public override void OnEnter()
		{
			Material copiedMaterial = new Material( OriginalMaterial.Value );
			copiedMaterial.name = "PlayMakerInstancedMaterial";

			CopiedMaterial = new FsmMaterial( copiedMaterial );

			Finish();
		}
		#endregion
	}
}