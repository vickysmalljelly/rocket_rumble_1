﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Sets the sibling index of an object")]
	public class SetSiblingIndex : FsmStateAction
	{
		#region Public Variables
		[Tooltip("GameObject.")]
		public FsmGameObject gameObject;

		[Tooltip("Move to front.")]
		public FsmBool moveToFront;

		[Tooltip("Move to back.")]
		public FsmBool moveToBack;
		#endregion

		public override void OnEnter()
		{
			if( gameObject.Value != null )
			{
				if( moveToFront.Value )
				{
					gameObject.Value.transform.SetAsLastSibling();
				}

				if( moveToBack.Value )
				{
					gameObject.Value.transform.SetAsFirstSibling();
				}
			}

			Finish();
		}

	}
}