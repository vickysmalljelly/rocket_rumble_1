﻿
using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Capture a screenshot")]
	public class CaptureScreenshot : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmTexture Texture;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Texture = default( FsmTexture );
		}

		public override void OnEnter()
		{
			Camera.onPostRender += HandleOnPostRender;
		}

		public override void OnExit()
		{
			Camera.onPostRender -= HandleOnPostRender;
		}
		#endregion

		#region Event Handlers
		private void HandleOnPostRender( Camera camera )
		{
			Texture2D tex = new Texture2D( Screen.width, Screen.height );
			tex.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
			tex.Apply();

			Texture.Value = tex;

			Finish();
		}
		#endregion

	}
}