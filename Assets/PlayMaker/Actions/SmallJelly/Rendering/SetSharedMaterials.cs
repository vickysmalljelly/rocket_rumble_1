﻿
using UnityEngine;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Sets the shared materials on a game object.")]
	public class SetSharedMaterials : SharedMaterialArrayAction
	{
		#region PlayMaker Variables
		[RequiredField]
		public FsmOwnerDefault GameObject;

		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Array Variable to use.")]
		public FsmArray Array;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			GameObject = null;
			Array = null;
		}

		public override void OnEnter()
		{
			GameObject gameObject = Fsm.GetOwnerDefaultTarget( GameObject );

			List< MeshRenderer > meshRenderers = GetOrderedMeshRenderer( gameObject.transform );

			for( int i = 0; i < meshRenderers.Count; i++ )
			{
				meshRenderers[ i ].sharedMaterial = ( ( FsmMaterial ) Array.Get( i ) ).Value;
			}

			Finish();
		}
		#endregion
	}
}