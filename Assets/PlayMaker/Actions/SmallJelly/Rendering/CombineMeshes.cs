// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Combines the meshes of an object")]
	public class CombineMeshes : FsmStateAction
	{
		#region Public Variables
		[RequiredField]
		public FsmOwnerDefault gameObject;

		[UIHint(UIHint.FsmMaterial)]
		public FsmMaterial Material;
		#endregion

		#region Playmaker Methods
		public override void Reset()
		{
			gameObject = null;
			Material = null;
		}

		public override void OnEnter()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (go == null) return;

			SJRenderExtensions.CombineMeshes( go, Material.Value );
			
			Finish();
		}
		#endregion
	}
}