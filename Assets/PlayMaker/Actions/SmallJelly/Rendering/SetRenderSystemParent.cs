// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Sets the Parent of a GameObject within the Render System.")]

	public class SetRenderSystemParent : FsmStateAction
	{
		#region Constants
		private const string VARIABLE_NOT_SET_ERROR_MESSAGE = "Please specify a value for {0}";
		#endregion

		#region Public PlayMaker Variables
		[RequiredField]
		[Tooltip("The child GameObject to parent.")]
		public FsmOwnerDefault Child;
		
		[Tooltip("The new parent for the child GameObject.")]
		public FsmGameObject Parent;

		[Tooltip("Set the local position to 0,0,0 after parenting.")]
		public FsmBool ReorientatePosition;

		[Tooltip("Set the local rotation to 0,0,0 after parenting.")]
		public FsmBool ReorientateRotation;

		[Tooltip("Set the local scale to 1,1,1 after parenting.")]
		public FsmBool ReorientateScale;
		#endregion

		#region PlayMaker Methods
		public override void OnEnter()
		{
			OnValidate();

			Transform parent = Parent.Value.transform;
			Transform child = Fsm.GetOwnerDefaultTarget( Child ).transform;

			SJRenderTransformExtensions.AddChild( parent, child, ReorientatePosition.Value, ReorientateRotation.Value, ReorientateScale.Value );

			Finish();
		}

		public override void Reset()
		{
			Child = default( FsmOwnerDefault );
			Parent = default( FsmGameObject );

			ReorientatePosition = default( FsmBool );
			ReorientateRotation = default( FsmBool );
			ReorientateScale = default( FsmBool );
		}
		#endregion

		#region Private Methods
		private void OnValidate()
		{
			SJLogger.AssertCondition( Child != default(FsmOwnerDefault), VARIABLE_NOT_SET_ERROR_MESSAGE, "Child" );
			SJLogger.AssertCondition( Parent != default(FsmGameObject), VARIABLE_NOT_SET_ERROR_MESSAGE, "Parent" );
			SJLogger.AssertCondition( ReorientatePosition != default(FsmBool), VARIABLE_NOT_SET_ERROR_MESSAGE, "ReorientatePosition" );
			SJLogger.AssertCondition( ReorientateRotation != default(FsmBool), VARIABLE_NOT_SET_ERROR_MESSAGE, "ReorientateRotation" );
			SJLogger.AssertCondition( ReorientateScale != default(FsmBool), VARIABLE_NOT_SET_ERROR_MESSAGE, "ReorientateScale" );
		}
		#endregion

	}
}