// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Gets a queue of a material")]
	public class GetRenderQueue : FsmStateAction
	{
		#region Public Variables
		[RequiredField]
		public FsmMaterial Material;

		[RequiredField]
		public FsmInt Queue;
		#endregion

		#region PlayMaker Methods 
		public override void Reset()
		{
			Material = null;
			Queue = 0;
		}

		public override void OnEnter()
		{
			Queue.Value = Material.Value.renderQueue;
			
			Finish();
		}
		#endregion
	}
}