﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Converts a ray to a world point on a specific camera")]
	public class RayToWorldPoint : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmOwnerDefault Object;
		public FsmObject SourceRay;
		public FsmVector3 OutputWorldPoint;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Object = default( FsmOwnerDefault );
			SourceRay = default( FsmObject );
			OutputWorldPoint = default( FsmVector3 );
		}

		public override void OnEnter()
		{
			GameObject owner = Fsm.GetOwnerDefaultTarget( Object );

			Vector2 screenPosition = ((RayObject)SourceRay.Value).ScreenPosition;
			Camera camera = SJRenderTransformExtensions.GetCamera( owner.layer );

			//Get current depth of the object
			float depth = camera.WorldToScreenPoint( owner.transform.position ).z;

			OutputWorldPoint.Value = camera.ScreenToWorldPoint( new Vector3( screenPosition.x, screenPosition.y, depth ) );
			Finish();
		}
		#endregion

	}
}