﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions

{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Draws a Quadratic Bézier Curve between three Game Objects using Unity's Line Renderer.")]
	public class CreateCardHover : FsmStateAction
	{
		#region Constants
		private const string CARD_HOVER_RESOURCE = "Prefabs/Cards/p_assembled_card_hover";
		#endregion

		#region Public Variables
		[Tooltip("Source Card.")]
		public FsmOwnerDefault SourceCard;

		[Tooltip("Output Hover Card.")]
		public FsmGameObject OutputHoverCard;
		#endregion

		/**
		CAN WE DO IT USING PLAYMAKER? THE PROBLEM WITH PLAYMAKER IS WE CANT PLAY MULTIPLE ANIMATIONS AT ONCE
		WHEN THE PLAYER STARTS DRAGGING WE'D NEED TO DO "ROLLOFF" AND THEN WHEN ROLLOFF IS FINISHED TRIGGER DRAGGING
		BUT WHAT ABOUT THE FRAME DROP?
		INSIDE OF THE DRAGGING CAN WE TRIGGER ROLLOFF AND THEN IMMEDIATELY DO THE DRAGGING ANIMATION?
		OR CAN WE INTERGRATE IT AS PART OF THE DRAGGING ANIMATION?
		**/

		#region Public Methods
		public override void OnEnter()
		{
			BattleEntity sourceBattleEntity = SourceCard.GameObject.Value.GetComponent<BattleEntity>();
			SJLogger.Assert("The source card must be a battle entity");

			GameObject resource = LoadResource();

			CardView cardBattleEntityView = CreateCard( resource );
			cardBattleEntityView.OnRefreshModelWithData( sourceBattleEntity.Data );

			Finish();
		}
		#endregion

		#region Private Methods
		private GameObject LoadResource()
		{
			return Resources.Load<GameObject>(CARD_HOVER_RESOURCE);
		}

		private CardView CreateCard( GameObject resource )
		{
			GameObject createdCard = UnityEngine.Object.Instantiate( resource, SourceCard.GameObject.Value.transform.position, Quaternion.identity ) as GameObject;

			//Set created card to the level of the source card
			createdCard.transform.parent = SourceCard.GameObject.Value.transform.parent;
			OutputHoverCard.Value = createdCard;

			CardView cardBattleEntityView =  createdCard.GetComponent<CardView>();
			SJLogger.AssertCondition( cardBattleEntityView != null, "Created card must have a card view component" );
			return cardBattleEntityView;
		}
		#endregion
	}
}