﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Gets the Camera of the supplied object")]
	public class GetCamera : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmOwnerDefault Object;
		public FsmGameObject Camera;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Object = default( FsmOwnerDefault );
			Camera = default( FsmGameObject );
		}

		public override void OnEnter()
		{
			GameObject owner = Fsm.GetOwnerDefaultTarget( Object );
			Camera.Value = SJRenderTransformExtensions.GetCamera( owner.layer ).gameObject;
			Finish();
		}
		#endregion

	}
}