// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Transforms a vector3 betwen two camera layers")]
	public class TransformPointBetweenCameras : FsmStateAction
	{
		#region Public Variables
		[RequiredField]
		public FsmVector3 Point;

		[RequiredField]
		public FsmInt CameraLayer1;

		[RequiredField]
		public FsmInt CameraLayer2;


		[RequiredField]
		public FsmVector3 Result;
		#endregion

		#region PlayMaker Methods 
		public override void Reset()
		{
			Point = null;
			CameraLayer1 = null;
			CameraLayer2 = null;
		}

		public override void OnEnter()
		{
			Result.Value = SJRenderTransformExtensions.TransformPointBetweenCameras( Point.Value, CameraLayer1.Value, CameraLayer2.Value );
			
			Finish();
		}
		#endregion
	}
}