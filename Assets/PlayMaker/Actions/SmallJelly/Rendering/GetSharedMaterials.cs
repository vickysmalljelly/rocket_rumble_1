﻿
using UnityEngine;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Gets the shared materials on a game object.")]
	public class GetSharedMaterials : SharedMaterialArrayAction
	{
		#region PlayMaker Variables
		[RequiredField]
		public FsmOwnerDefault GameObject;


		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The GameObject Array Variable to use.")]
		public FsmArray GameObjectArray;

		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Material Array Variable to use.")]
		public FsmArray MaterialArray;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			GameObject = null;
			MaterialArray = null;
		}

		public override void OnEnter()
		{
			GameObject gameObject = Fsm.GetOwnerDefaultTarget( GameObject );

			List< MeshRenderer > meshRenderers = GetOrderedMeshRenderer( gameObject.transform );

			GameObjectArray.Resize( meshRenderers.Count );
			MaterialArray.Resize( meshRenderers.Count );

			for( int i = 0; i < meshRenderers.Count; i++ )
			{
				GameObjectArray.Set( i, meshRenderers[ i ].gameObject );
				MaterialArray.Set( i, meshRenderers[ i ].sharedMaterial );
			}

			Finish();
		}
		#endregion
	}
}