﻿
using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Animates Off A Texture")]
	public class TransitionTexture2D : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmTexture Texture;
		public FsmFloat Speed;
		#endregion

		#region Member Variables
		private float mElapsedTime;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Texture = default( FsmTexture );
			Speed = default( FsmFloat );
		}

		public override void OnUpdate()
		{
			mElapsedTime += Speed.Value * Time.deltaTime;

			if( mElapsedTime >= 1.0f )
			{
				Finish();
			}
		}

		public override void OnGUI ()
		{
			GUI.DrawTexture( new Rect( new Vector2( 0, 0 ), new Vector2( Screen.width, Screen.height * ( 1.0f - mElapsedTime ) ) ), Texture.Value, ScaleMode.StretchToFill );
		}
		#endregion
	}
}