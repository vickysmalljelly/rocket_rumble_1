﻿
using UnityEngine;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Gets the shared material on a game object.")]
	public class GetSharedMaterial : FsmStateAction
	{
		#region PlayMaker Variables
		[RequiredField]
		public FsmOwnerDefault GameObject;

		[RequiredField]
		public FsmMaterial Material;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			GameObject = null;
			Material = null;
		}

		public override void OnEnter()
		{
			GameObject gameObject = Fsm.GetOwnerDefaultTarget( GameObject );
			Renderer renderer = gameObject.GetComponent<Renderer>();
			SJLogger.AssertCondition( renderer != null, "{0} must have a renderer component", gameObject.name );

			Material.Value = renderer.sharedMaterial;

			Finish();
		}
		#endregion
	}
}