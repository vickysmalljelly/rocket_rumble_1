﻿
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using UnityEngine;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Rendering")]
	[Tooltip("Checks whether a targetable entity is currently targetable")]
	public class IsTargetable : FsmStateAction
	{
		#region Public Variables
		public FsmGameObject TargetableGameObject;
		public FsmEvent Targeting;
		public FsmEvent Targetable;
		public FsmEvent NonTargetable;

		public FsmEvent Undefined;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			TargetableGameObject = null;
		}
		
		public override void OnEnter()
		{
			Targetable targetable = TargetableGameObject.Value.GetComponent<Targetable>();

			SJLogger.AssertCondition( targetable != null, "Supplied GameObject much have a Targetable component" );

			if( targetable.TargetableState == TargetableLogicController.TargetableState.Targeting )
			{
				OnTargeting();
				return;
			}

			if( targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
			{
				OnTargetable();
				return;
			}

			if( targetable.TargetableState == TargetableLogicController.TargetableState.NonTargetable )
			{
				OnNonTargetable();
				return;
			}

			if( targetable.TargetableState == TargetableLogicController.TargetableState.None )
			{
				OnUndefined();
				return;
			}			
		}
		#endregion

		#region Event Firing
		private void OnTargeting()
		{
			Fsm.Event( Targeting );
		}

		private void OnTargetable()
		{
			Fsm.Event( Targetable );
		}

		private void OnNonTargetable()
		{
			Fsm.Event( NonTargetable );
		}

		private void OnUndefined()
		{
			Fsm.Event( Undefined );
		}
		#endregion
	}
}