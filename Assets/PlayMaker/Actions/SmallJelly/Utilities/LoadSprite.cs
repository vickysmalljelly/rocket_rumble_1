﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Loads a sprite from resources")]
	public class LoadSprite : FsmStateAction
	{
		#region PlayMaker Variables
		[RequiredField]
		public FsmString mResourcePath;

		[RequiredField]
		[ObjectType(typeof(UnityEngine.Sprite))]
		public FsmObject mOutputSprite;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			Owner = null;
			mResourcePath = null;
			mOutputSprite = null;
		}

		public override void OnEnter()
		{
			mOutputSprite.Value = Resources.Load< Sprite >( mResourcePath.Value );

			Finish();
		}
		#endregion
	}
}