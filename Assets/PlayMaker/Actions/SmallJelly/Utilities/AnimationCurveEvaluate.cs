﻿using UnityEngine;
using SmallJelly.Framework;
using SmallJelly;
namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Evaluates a value from an Animation Curve")]
	public class AnimationCurveEvaluate : FsmStateAction
	{
		#region PlayMaker Variables
		[RequiredField]
		[Tooltip("The animation curve to use.")]
		public FsmAnimationCurve AnimCurve;
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The time you want to evaluate the curve at.")]
		public FsmFloat TimeToEvaluate;
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Result of the evaluation")]
		public FsmFloat Result;
		public bool EveryFrame;
		#endregion
		#region PlayMaker Methods
		public override void Reset()
		{
			TimeToEvaluate = null;
			Result = null;
			EveryFrame = false;
		}
		public override void OnEnter()
		{
			Result.Value = AnimCurve.curve.Evaluate(TimeToEvaluate.Value);
			if (!EveryFrame)
			{
				Finish();   
			}
		}
		public override void OnUpdate()
		{
			Result.Value = AnimCurve.curve.Evaluate(TimeToEvaluate.Value);
		}
		#endregion
	}
}