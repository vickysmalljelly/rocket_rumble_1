// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	public class EnableMeshRenderer : FsmStateAction
	{
		#region Exposed To PlayMaker
		[RequiredField]
		public FsmOwnerDefault gameObject;

		public FsmBool enable;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			gameObject = null;
			enable = null;
		}

		public override void OnEnter()
		{
			GameObject target = Fsm.GetOwnerDefaultTarget( gameObject );
			MeshRenderer meshRenderer = target.GetComponent< MeshRenderer >();
			meshRenderer.enabled = enable.Value;

			Finish();
		}
		#endregion
	}
}