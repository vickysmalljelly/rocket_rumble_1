﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly.Framework;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Calculate angle between Vector3s")]
	public class CalculateAngleBetweenPoints : FsmStateAction
	{
		#region PlayMaker Variables
		public FsmVector3 SourcePosition;
		public FsmVector3 DestinationPosition;

		public FsmBool LockXAxis;
		public FsmBool LockYAxis;
		public FsmBool LockZAxis;

		public FsmQuaternion ResultantQuaternion;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			SourcePosition = null;
			DestinationPosition = null;
		}

		public override void OnEnter()
		{
			Vector3 sourceVector = SourcePosition.Value;
			Vector3 destinationVector = DestinationPosition.Value;

			if( LockXAxis.Value )
			{
				sourceVector = new Vector3( 0.0f, sourceVector.y, sourceVector.z );
				destinationVector = new Vector3( 0.0f, destinationVector.y, destinationVector.z );
			}

			if( LockYAxis.Value )
			{
				sourceVector = new Vector3( sourceVector.x, 0.0f, sourceVector.z );
				destinationVector = new Vector3( destinationVector.x, 0.0f, destinationVector.z );
			}

			if( LockZAxis.Value )
			{
				sourceVector = new Vector3( sourceVector.x, sourceVector.y, 0.0f );
				destinationVector = new Vector3( destinationVector.x, destinationVector.y, 0.0f );
			}

			ResultantQuaternion.Value = Quaternion.LookRotation( destinationVector - sourceVector );
			Finish();
		}
		#endregion
	}
}