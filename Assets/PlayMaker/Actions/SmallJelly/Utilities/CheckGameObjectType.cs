// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly;
using SmallJelly.Framework;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Checks which targetable type an object is.")]
	public class CheckTargetableType : FsmStateAction
	{
		#region Exposed To PlayMaker
		[RequiredField]
		public FsmGameObject GameObjectToCheck;

		[RequiredField]
		public FsmBool IsSocket;

		[RequiredField]
		public FsmBool IsBattleEntity;

		[RequiredField]
		public FsmBool IsShip;

		[RequiredField]
		public FsmEvent TrueEvent;

		[RequiredField]
		public FsmEvent FalseEvent;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			Owner = null;
			IsSocket = null;
			IsBattleEntity = null;
			IsShip = null;

			TrueEvent = null;
			FalseEvent = null;
		}

		public override void OnEnter()
		{
			if( GameObjectToCheck.Value == null )
			{
				Fsm.Event( FalseEvent );
				return;
			}

			if( IsSocket.Value && GameObjectToCheck.Value.GetComponent< Socket >() != null )
			{
				Fsm.Event( TrueEvent );
				return;
			}

			if( IsBattleEntity.Value && GameObjectToCheck.Value.GetComponent< BattleEntity >() != null )
			{
				Fsm.Event( TrueEvent );
				return;
			}

			if( IsShip.Value && GameObjectToCheck.Value.GetComponent< Ship >() != null )
			{
				Fsm.Event( TrueEvent );
				return;
			}

			Fsm.Event( FalseEvent );
		}
		#endregion
	}
}