﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Enables/Disables a collider")]
	public class EnableCollider : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The GameObject that owns the Behaviour.")]
		public FsmOwnerDefault gameObject;

		[RequiredField]
		[Tooltip("The index of the collider (where there are multiple)")]
		public FsmInt index;

		[RequiredField]
		[Tooltip("Set to True to enable, False to disable.")]
		public FsmBool enable;


		public override void Reset()
		{
			gameObject = null;
			enable = true;
		}

		public override void OnEnter()
		{
			//Set whether the collider is enabled
			GameObject owner = Fsm.GetOwnerDefaultTarget( gameObject );
			owner.GetComponents<Collider>()[ index.Value ].enabled = enable.Value;

			Finish();
		}
	}
}