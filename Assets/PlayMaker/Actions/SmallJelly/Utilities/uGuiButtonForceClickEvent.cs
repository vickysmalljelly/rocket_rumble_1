﻿// (c) Copyright HutongGames, LLC 2010-2015. All rights reserved.
//--- __ECO__ __PLAYMAKER__ __ACTION__ __UI_MODULE__ ---//

using UnityEngine;
using SmallJelly;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Fires an event for a UGui component.")]
	public class uGuiButtonForceClickEvent : FsmStateAction
	{
		[RequiredField]
		[CheckForComponent(typeof(UnityEngine.UI.Button))]
		[Tooltip("The GameObject with the UGui button component.")]
		public FsmOwnerDefault gameObject;

		public override void Reset()
		{
			gameObject = null;
		}
		
		public override void OnEnter()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget( gameObject );
			ExecuteEvents.Execute( go, new BaseEventData( EventSystem.current ), ExecuteEvents.submitHandler );
			Finish();
		}
	}
}