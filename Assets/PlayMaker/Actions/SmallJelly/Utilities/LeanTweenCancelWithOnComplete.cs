using UnityEngine;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	
	[ActionCategory("LeanTween")]
	[Tooltip("Cancel tweens with specific ID for a GameObject")]
	public class LeanTweenCancelWithOnComplete : FsmStateAction
	{
		#region Playmaker Variables
		[RequiredField]
		[Tooltip("Gameobject that you wish to cancel tween.")]
		public FsmOwnerDefault GameObject;

		public FsmBool TriggerOnComplete;
		#endregion

		#region Playmaker Methods
		public override void OnEnter()
		{

			GameObject gameObject = Fsm.GetOwnerDefaultTarget( GameObject );

			LeanTween.cancel (gameObject, TriggerOnComplete.Value );

			Finish();
		}
		#endregion
	}
	
}
