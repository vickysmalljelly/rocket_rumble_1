﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Converts A Socket to a Battle Entity")]
	public class SocketToBattleEntity : FsmStateAction
	{
		[RequiredField]
		public FsmGameObject InputSocket;

		[RequiredField]
		public FsmGameObject OutputBattleEntity;

		public override void Reset()
		{
			InputSocket = null;
			OutputBattleEntity = null;
		}

		public override void OnEnter()
		{
			GameObject socket = InputSocket.Value;
			BattleEntity component = socket.gameObject.GetComponent< Socket >().SocketController.Component;

			if( component != null )
			{
				OutputBattleEntity.Value = component.gameObject;
			}
			else
			{
				OutputBattleEntity.Value = null;
			}

			Finish();
		}
	}
}