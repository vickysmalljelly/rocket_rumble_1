﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly.Framework;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Creates a ray from a world point or gameObject")]
	public class CreateRay : FsmStateAction
	{
		[Tooltip("The GameObject that the ray should start at.")]
		public FsmGameObject sourceGameObject;

		[Tooltip("The world space position that the ray should start at.")]
		public FsmVector3 sourcePosition;

		[RequiredField]
		[Tooltip("The direction that the ray should travel in")]
		public FsmVector3 rayDirection;

		[RequiredField]
		[Tooltip("The distance that the ray should travel")]
		public FsmFloat distance;

		[RequiredField]
		[Tooltip("The output ray.")]
		public FsmObject outputRay;


		[RequiredField]
		[Tooltip("Run every frame")]
		public FsmBool runEveryFrame;

		#region Public Methods
		public override void Reset()
		{
			sourceGameObject = null;
			sourcePosition = null;
			outputRay = null;
		}

		public override void OnEnter()
		{
			GenerateRay();

			if( !runEveryFrame.Value )
			{
				Finish();
			}
		}

		public override void OnUpdate()
		{
			GenerateRay();
		}
		#endregion

		#region Private Methods
		private void GenerateRay()
		{

			SJLogger.AssertCondition( (sourceGameObject.Value == null || sourcePosition.Value == default(Vector3)) && (sourceGameObject.Value != null || sourcePosition.Value != default(Vector3)), "_Either_ the source gameobject or the source position should have a value (XOR)");

			if( sourceGameObject.Value != null )
			{
				//Use the source gameObject
				outputRay.Value = new RayObject(sourceGameObject.Value.transform.position);
			}
			else
			{
				//Use the source position
				outputRay.Value = new RayObject(sourcePosition.Value);
			}

		}
		#endregion
	}
}