﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Sets the fill of an image")]
	public class SetImageFill : FsmStateAction
	{
		#region PlayMaker Variables
		[RequiredField]
		[Tooltip("The GameObject that owns the Behaviour.")]
		public FsmOwnerDefault GameObjectOwner;

		[RequiredField]
		public FsmFloat Fill;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			Owner = null;
			Fill = 0.0f;
		}

		public override void OnEnter()
		{
			FsmGameObject gameObject = Fsm.GetOwnerDefaultTarget( GameObjectOwner );
			Image image = gameObject.Value.GetComponent< Image >();

			image.fillAmount = Fill.Value;

			Finish();
		}
		#endregion
	}
}