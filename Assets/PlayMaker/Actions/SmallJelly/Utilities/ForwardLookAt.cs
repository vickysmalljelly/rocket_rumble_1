﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly.Framework;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Look at GameObject")]
	public class ForwardLookAt : FsmStateAction
	{
		#region Exposed To Playmaker
		public FsmOwnerDefault GameObject;
		public FsmGameObject Target;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Owner = null;
			Target = null;
		}

		public override void OnEnter()
		{
			Fsm.GetOwnerDefaultTarget( GameObject ).transform.LookAt( Target.Value.transform.position );
			Finish();
		}
		#endregion
	
	}
}