﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using SmallJelly.Framework;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Utilities")]
	[Tooltip("Sets the layer of a GameObject recursively")]
	public class SetLayerRecursively : FsmStateAction
	{
		[Tooltip("The GameObject you want to change the layer of.")]
		public FsmOwnerDefault GameObject;

		[Tooltip("The layer that the object should be on.")]
		public FsmInt Layer;

		#region Public Methods
		public override void Reset()
		{
			Owner = null;
			Layer = null;
		}

		public override void OnEnter()
		{
			SetLayer( Fsm.GetOwnerDefaultTarget( GameObject ).transform, Layer.Value );
			Finish();
		}
		#endregion

		#region Private Methods
		private void SetLayer( Transform transform, int layer )
		{
			transform.gameObject.layer = layer;
			foreach (Transform child in transform)
			{
				child.gameObject.layer = layer;
				if (child.childCount > 0)
				{
					SetLayer(child.transform, layer);
				}
			}
		}
		#endregion
	}
}