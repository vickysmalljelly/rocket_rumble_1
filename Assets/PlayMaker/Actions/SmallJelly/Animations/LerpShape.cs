﻿using HutongGames.PlayMaker;
using SmallJelly;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	public abstract class LerpShape : FsmStateAction
	{
		#region FSM Variables
		public FsmVector3 StartPosition;
		public FsmVector3 EndPosition;

		public FsmFloat ElapsedTime;

		public FsmVector3 OutputPosition;
		#endregion

		#region Methods
		public override void OnEnter()
		{
			OutputPosition.Value = CalculatePosition();

			Finish();

		}

		public abstract Vector3 CalculatePosition();
		#endregion
	}
}