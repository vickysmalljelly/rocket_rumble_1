﻿using HutongGames.PlayMaker;
using SmallJelly;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("SmallJelly-Animations")]
	[Tooltip("Used to lerp between set points")]
	public class LerpLine : LerpShape
	{
		public override Vector3 CalculatePosition()
		{
			return Vector3.Lerp( StartPosition.Value, EndPosition.Value, ElapsedTime.Value );
		}
	}
}