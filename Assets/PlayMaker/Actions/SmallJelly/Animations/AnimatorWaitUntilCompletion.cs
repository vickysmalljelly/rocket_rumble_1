﻿// (c) Copyright HutongGames, LLC 2010-2015. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Animations")]
	public class AnimatorWaitUntilCompletion : FsmStateAction
	{
		#region Playmaker Variables
		[RequiredField]
		[Tooltip("The target. An Animator component is required")]
		public FsmOwnerDefault OwnerGameObject;
		#endregion

		#region Member Variables
		private Animation mAnimation;
		#endregion

		public override void Reset()
		{
			Owner = null;
		}

		public override void OnEnter()
		{
			// get the animator component
			GameObject gameObject = Fsm.GetOwnerDefaultTarget( OwnerGameObject );
			mAnimation = gameObject.GetComponent<Animation>();

		}

		public override void OnUpdate ()
		{
			if( mAnimation == null || !mAnimation.isPlaying )
			{
				Finish();
			}
		}
	}
}