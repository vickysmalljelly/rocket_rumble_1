﻿using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Input")]
	[Tooltip("Uses the player input manager to output the results of a raycast from the input")]
	public class PlayerInputManagerRaycast : FsmStateAction
	{	
		public FsmObject ray;
		public FsmGameObject hitGameObject;
		public FsmVector3 hitPosition;

		public FsmEvent hit;

		#region Public Methods
		public override void Reset()
		{
			ray = default(FsmObject);
			hitGameObject = default(FsmGameObject);
			hitPosition = default(FsmVector3);
		}
		
		public override void OnEnter()
		{
			RegisterListeners();
		}

		public override void OnExit()
		{
			UnRegisterListeners();
		}
		#endregion

		#region Private Methods
		private void UpdateInformation(Vector2 screenPosition, SJRenderRaycastHit[] renderRaycastHit)
		{
			ray.Value = new RayObject(screenPosition);
			hitGameObject.Value = renderRaycastHit[0].GroupGameObject;
			hitPosition.Value = renderRaycastHit[0].Point;
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			PlayerInputManager.Get.RayUpdated += HandleRayUpdated;
		}

		private void UnRegisterListeners()
		{
			PlayerInputManager.Get.RayUpdated -= HandleRayUpdated;
		}
		#endregion

		#region Event Handlers
		private void HandleRayUpdated(object o, RenderRaycastEventArgs raycastHitEventArgs)
		{
			UpdateInformation( raycastHitEventArgs.ScreenPosition, raycastHitEventArgs.Array );

			Fsm.Event(hit);
		}
		#endregion
	}
}