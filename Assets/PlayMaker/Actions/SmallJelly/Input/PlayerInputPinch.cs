﻿using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Input")]
	[Tooltip("Used to detect touch screen pinches")]
	public class PlayerInputPinch : FsmStateAction
	{	
		#region PlayMaker Variables
		public FsmFloat DistanceBetweenFingers;
		public FsmEvent PlayerPinched;
		#endregion

		#region Member Variables
		private float mScreenDiagonalSize;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			DistanceBetweenFingers = default( FsmFloat );
		}
		#endregion

		#region MonoBehaviour Methods
		public override void OnEnter()
		{
			mScreenDiagonalSize = Vector2.Distance( new Vector2( 0, 0 ), new Vector2( Screen.width, Screen.height ) );
			UpdatePinch();
		}
		#endregion
	
		#region Private Methods
		private void UpdatePinch()
		{
			//If the user has multiple fingers on screen
			if( Input.touchCount == 2 )
			{
				Vector3 touch0Position = Input.GetTouch(0).position;
				Vector3 touch1Position = Input.GetTouch(1).position;
				float distanceBetweenFingersInPixels = Vector2.Distance(touch0Position, touch1Position);

				DistanceBetweenFingers.Value = distanceBetweenFingersInPixels / mScreenDiagonalSize;

				//The player has pinched!
				FirePlayerPinched();
			}

			//Nothing was hit - just finish
			Finish();
		}
		#endregion

		#region Event Firing
		//Fire the player pinched event
		private void FirePlayerPinched()
		{
			if( PlayerPinched != null ) 
			{
				Fsm.Event( PlayerPinched );
			}
		}
		#endregion
	}
}