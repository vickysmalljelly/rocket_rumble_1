﻿using HutongGames.PlayMaker;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Input")]
	[Tooltip("Uses the player input manager to detect presses")]
	public class PlayerInputManagerPressed : FsmStateAction
	{
		public FsmGameObject pressedGameObject;

		#region Public Methods
		public override void OnEnter()
		{
			RegisterListeners();
		}

		public override void OnExit()
		{
			UnRegisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			PlayerInputManager.Get.MousePressed += HandlePressed;
		}

		private void UnRegisterListeners()
		{
			PlayerInputManager.Get.MousePressed -= HandlePressed;
		}
		#endregion

		#region Event Handlers
		private void HandlePressed(object o, ArrayEventArgs<SJRenderRaycastHit> stackEventArgs)
		{
			pressedGameObject.Value = stackEventArgs.Array[0].GroupGameObject;
			Finish();
		}
		#endregion
	}
}