﻿using HutongGames.PlayMaker;
using SmallJelly;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Input")]
	[Tooltip("Uses the player input manager to detect releases")]
	public class PlayerInputManagerReleased : FsmStateAction
	{
		public FsmGameObject releasedGameObject;

		#region Public Methods
		public override void OnEnter()
		{
			RegisterListeners();
		}

		public override void OnExit()
		{
			UnRegisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			PlayerInputManager.Get.MouseReleased += HandleReleased;
		}

		private void UnRegisterListeners()
		{
			PlayerInputManager.Get.MouseReleased -= HandleReleased;
		}
		#endregion

		#region Event Handlers
		private void HandleReleased(object o, ArrayEventArgs<SJRenderRaycastHit> stackEventArgs)
		{
			releasedGameObject.Value = stackEventArgs.Array[0].GroupGameObject;
			Finish();
		}
		#endregion
	}
}