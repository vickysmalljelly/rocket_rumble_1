﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-ParticleSystem")]
	public class ParticleSystemEmit : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmOwnerDefault Object;
		public FsmBool ShouldEmit;
		public FsmBool Recursive;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Object = default( FsmOwnerDefault );
		}

		public override void OnEnter()
		{
			GameObject owner = Fsm.GetOwnerDefaultTarget( Object );

			if( Recursive.Value )
			{
				ParticleSystem[] particleSystems = owner.GetComponentsInChildren< ParticleSystem >();
				
				foreach( ParticleSystem particleSystem in particleSystems )
				{
					DisableParticleSystem( particleSystem );
				}
			}
			else
			{
				ParticleSystem particleSystem = owner.GetComponentInChildren< ParticleSystem >();
				DisableParticleSystem( particleSystem );
			}
				
			Finish();
		}
		#endregion

		#region Private Methods
		private void DisableParticleSystem( ParticleSystem particleSystem )
		{
			ParticleSystem.EmissionModule emissionModule = particleSystem.emission;

			SJLogger.AssertCondition( particleSystem != null, "Supplied object must have a particle system component" );

			emissionModule.enabled = ShouldEmit.Value;
		}
		#endregion

	}
}