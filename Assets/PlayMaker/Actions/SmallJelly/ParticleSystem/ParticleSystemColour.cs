﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-ParticleSystem")]
	public class ParticleSystemColour : FsmStateAction 
	{
		#region PlayMaker Variables
		public FsmOwnerDefault Object;
		public FsmColor Color;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			Object = default( FsmOwnerDefault );
		}

		public override void OnEnter()
		{
			GameObject owner = Fsm.GetOwnerDefaultTarget( Object );

			ParticleSystem particleSystem = owner.GetComponent< ParticleSystem >();

			SJLogger.AssertCondition( particleSystem != null, "Supplied gameobject must have a particle system component" );

			particleSystem.startColor = Color.Value;

			Finish();
		}
		#endregion

	}
}