﻿using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;
using TMPro;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-ThirdPartSupport")]
	[Tooltip("Support text mesh pro")]
	public class TextMeshProColourSetText : FsmStateAction
	{	
		#region Exposed To PlayMaker
		public FsmGameObject TextGameObject;
		public FsmString TextString;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			TextGameObject = default( FsmGameObject );
			TextString = default( FsmString );
		}

		public override void OnEnter()
		{
			TextMeshPro textMeshPro = TextGameObject.Value.GetComponent< TextMeshPro >();

			if( textMeshPro )
			{
				textMeshPro.text = TextString.Value;
				Finish();
				return;
			}

			TextMeshProUGUI textMeshProUGUI = TextGameObject.Value.GetComponent< TextMeshProUGUI >();

			if( textMeshProUGUI )
			{
				textMeshProUGUI.text = TextString.Value;
				Finish();
				return;
			}

			SJLogger.Assert( "Text Mesh Pro component must not be null" );

		}
		#endregion

	}
}