﻿using UnityEngine;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using SmallJelly;
using System.Collections.Generic;
using TMPro;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-ThirdPartSupport")]
	[Tooltip("Support text mesh pro")]
	public class TextMeshProColourConfiguration : FsmStateAction
	{	
		#region Exposed To PlayMaker
		public FsmGameObject TextGameObject;

		public FsmColor TopLeftColor;
		public FsmColor TopRightColor;
		public FsmColor BottomLeftColor;
		public FsmColor BottomRightColor;
		#endregion

		#region Public Methods
		public override void Reset()
		{
			TextGameObject = default( FsmGameObject );

			TopLeftColor = default( FsmColor );
			TopRightColor = default( FsmColor );
			BottomLeftColor = default( FsmColor );
			BottomRightColor = default( FsmColor );
		}

		public override void OnEnter()
		{
			TextMeshPro textMeshPro = TextGameObject.Value.GetComponent< TextMeshPro >();

			SJLogger.AssertCondition( textMeshPro != null, "Text Mesh Pro component must not be null" );

			VertexGradient vertexGradient = new VertexGradient( TopLeftColor.Value, TopRightColor.Value, BottomLeftColor.Value, BottomRightColor.Value );
			textMeshPro.colorGradient = vertexGradient;

			Finish();
		}
		#endregion

	}
}