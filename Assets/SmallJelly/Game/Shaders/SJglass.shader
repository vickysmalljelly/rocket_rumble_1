// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:Transparent/Diffuse,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.8382353,fgcg:0.2465398,fgcb:0.2465398,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1,x:34779,y:32652,varname:node_1,prsc:2|normal-44-RGB,custl-14-OUT,alpha-3-OUT,refract-97-OUT;n:type:ShaderForge.SFN_Color,id:2,x:33363,y:32236,ptovrint:False,ptlb:Colour,ptin:_Colour,varname:node_3315,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5808823,c2:0.5808823,c3:0.5808823,c4:1;n:type:ShaderForge.SFN_Slider,id:3,x:34329,y:32980,ptovrint:False,ptlb:Transparency,ptin:_Transparency,varname:node_8646,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_NormalVector,id:4,x:32212,y:32367,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:5,x:32201,y:32699,varname:node_5,prsc:2;n:type:ShaderForge.SFN_Dot,id:6,x:32484,y:32436,varname:node_6,prsc:2,dt:0|A-4-OUT,B-5-OUT;n:type:ShaderForge.SFN_Vector1,id:7,x:32983,y:32617,varname:node_7,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:8,x:33078,y:32440,varname:node_8,prsc:2|A-12-OUT,B-7-OUT;n:type:ShaderForge.SFN_Multiply,id:9,x:33272,y:32440,varname:node_9,prsc:2|A-8-OUT,B-7-OUT;n:type:ShaderForge.SFN_Multiply,id:10,x:33444,y:32440,varname:node_10,prsc:2|A-9-OUT,B-9-OUT;n:type:ShaderForge.SFN_Multiply,id:11,x:33614,y:32396,varname:node_11,prsc:2|A-2-RGB,B-10-OUT;n:type:ShaderForge.SFN_Multiply,id:12,x:32718,y:32446,varname:node_12,prsc:2|A-6-OUT,B-13-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:13,x:32550,y:32619,varname:node_13,prsc:2;n:type:ShaderForge.SFN_Multiply,id:14,x:33946,y:32688,varname:node_14,prsc:2|A-43-OUT,B-15-RGB;n:type:ShaderForge.SFN_LightColor,id:15,x:33703,y:32825,varname:node_15,prsc:2;n:type:ShaderForge.SFN_ViewReflectionVector,id:35,x:32203,y:32865,varname:node_35,prsc:2;n:type:ShaderForge.SFN_Dot,id:36,x:32478,y:32812,varname:node_36,prsc:2,dt:1|A-5-OUT,B-35-OUT;n:type:ShaderForge.SFN_Power,id:38,x:32859,y:32800,varname:node_38,prsc:2|VAL-36-OUT,EXP-39-OUT;n:type:ShaderForge.SFN_Exp,id:39,x:32832,y:32956,varname:node_39,prsc:2,et:0|IN-40-OUT;n:type:ShaderForge.SFN_Slider,id:40,x:32361,y:33018,ptovrint:False,ptlb:SpecPower,ptin:_SpecPower,varname:node_9255,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:3.388897,max:8;n:type:ShaderForge.SFN_Slider,id:41,x:33079,y:33003,ptovrint:False,ptlb:SpecIntesnity,ptin:_SpecIntesnity,varname:node_8738,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6365388,max:1;n:type:ShaderForge.SFN_Multiply,id:42,x:33216,y:32800,varname:node_42,prsc:2|A-38-OUT,B-41-OUT;n:type:ShaderForge.SFN_Add,id:43,x:33562,y:32678,varname:node_43,prsc:2|A-11-OUT,B-42-OUT,C-139-OUT;n:type:ShaderForge.SFN_Tex2d,id:44,x:33955,y:32382,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_8502,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_NormalVector,id:71,x:33437,y:33121,prsc:2,pt:False;n:type:ShaderForge.SFN_Transform,id:72,x:33635,y:33121,varname:node_72,prsc:2,tffrom:0,tfto:3|IN-71-OUT;n:type:ShaderForge.SFN_ComponentMask,id:73,x:33833,y:33121,varname:node_73,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-72-XYZ;n:type:ShaderForge.SFN_Multiply,id:97,x:34486,y:33101,varname:node_97,prsc:2|A-120-OUT,B-102-OUT;n:type:ShaderForge.SFN_Slider,id:102,x:34408,y:33305,ptovrint:False,ptlb:RefractionScale,ptin:_RefractionScale,varname:node_5953,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.5,cur:0,max:0;n:type:ShaderForge.SFN_ComponentMask,id:119,x:34215,y:32787,varname:node_119,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-44-RGB;n:type:ShaderForge.SFN_Add,id:120,x:34163,y:33092,varname:node_120,prsc:2|A-119-OUT,B-73-OUT;n:type:ShaderForge.SFN_Fresnel,id:137,x:32518,y:33244,varname:node_137,prsc:2|NRM-4-OUT,EXP-138-OUT;n:type:ShaderForge.SFN_Slider,id:138,x:32291,y:33468,ptovrint:False,ptlb:FresnelFalloff,ptin:_FresnelFalloff,varname:node_8789,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:139,x:33132,y:33203,varname:node_139,prsc:2|A-10-OUT,B-145-OUT,C-152-RGB;n:type:ShaderForge.SFN_Multiply,id:145,x:32820,y:33243,varname:node_145,prsc:2|A-137-OUT,B-146-OUT;n:type:ShaderForge.SFN_Slider,id:146,x:32741,y:33480,ptovrint:False,ptlb:FresnelIntensity,ptin:_FresnelIntensity,varname:node_4435,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:2,max:20;n:type:ShaderForge.SFN_Color,id:152,x:33100,y:33411,ptovrint:False,ptlb:FresnelColour,ptin:_FresnelColour,varname:node_158,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7534602,c2:0.9757009,c3:0.9852941,c4:1;proporder:2-3-40-41-44-102-138-146-152;pass:END;sub:END;*/

Shader "SmallJelly/SJglass" {
    Properties {
        _Colour ("Colour", Color) = (0.5808823,0.5808823,0.5808823,1)
        _Transparency ("Transparency", Range(0, 1)) = 0.5
        _SpecPower ("SpecPower", Range(1, 8)) = 3.388897
        _SpecIntesnity ("SpecIntesnity", Range(0, 1)) = 0.6365388
        _Texture ("Texture", 2D) = "bump" {}
        _RefractionScale ("RefractionScale", Range(-0.5, 0)) = 0
        _FresnelFalloff ("FresnelFalloff", Range(1, 5)) = 1
        _FresnelIntensity ("FresnelIntensity", Range(1, 20)) = 2
        _FresnelColour ("FresnelColour", Color) = (0.7534602,0.9757009,0.9852941,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d9 gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform sampler2D _GrabTexture;
            uniform float4 _Colour;
            uniform float _Transparency;
            uniform float _SpecPower;
            uniform float _SpecIntesnity;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _RefractionScale;
            uniform float _FresnelFalloff;
            uniform float _FresnelIntensity;
            uniform float4 _FresnelColour;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Texture_var = UnpackNormal(tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture)));
                float3 normalLocal = _Texture_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_Texture_var.rgb.rg+mul( UNITY_MATRIX_V, float4(i.normalDir,0) ).xyz.rgb.rg)*_RefractionScale);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float node_7 = 0.5;
                float node_9 = (((dot(normalDirection,lightDirection)*attenuation)+node_7)*node_7);
                float node_10 = (node_9*node_9);
                float3 finalColor = (((_Colour.rgb*node_10)+(pow(max(0,dot(lightDirection,viewReflectDirection)),exp(_SpecPower))*_SpecIntesnity)+(node_10*(pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelFalloff)*_FresnelIntensity)*_FresnelColour.rgb))*_LightColor0.rgb);
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,_Transparency),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d9 gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform sampler2D _GrabTexture;
            uniform float4 _Colour;
            uniform float _Transparency;
            uniform float _SpecPower;
            uniform float _SpecIntesnity;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _RefractionScale;
            uniform float _FresnelFalloff;
            uniform float _FresnelIntensity;
            uniform float4 _FresnelColour;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Texture_var = UnpackNormal(tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture)));
                float3 normalLocal = _Texture_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((_Texture_var.rgb.rg+mul( UNITY_MATRIX_V, float4(i.normalDir,0) ).xyz.rgb.rg)*_RefractionScale);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float node_7 = 0.5;
                float node_9 = (((dot(normalDirection,lightDirection)*attenuation)+node_7)*node_7);
                float node_10 = (node_9*node_9);
                float3 finalColor = (((_Colour.rgb*node_10)+(pow(max(0,dot(lightDirection,viewReflectDirection)),exp(_SpecPower))*_SpecIntesnity)+(node_10*(pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelFalloff)*_FresnelIntensity)*_FresnelColour.rgb))*_LightColor0.rgb);
                fixed4 finalRGBA = fixed4(finalColor * _Transparency,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Transparent/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
