// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1,x:33752,y:32905,varname:node_1,prsc:2|normal-698-RGB,emission-774-OUT,custl-169-OUT;n:type:ShaderForge.SFN_NormalVector,id:3,x:30479,y:32795,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:5,x:30479,y:32936,varname:node_5,prsc:2;n:type:ShaderForge.SFN_Dot,id:7,x:30705,y:32868,varname:node_7,prsc:2,dt:1|A-3-OUT,B-5-OUT;n:type:ShaderForge.SFN_Tex2d,id:8,x:31639,y:32446,ptovrint:False,ptlb:Diffuse Map,ptin:_DiffuseMap,varname:node_1033,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:13,x:31336,y:32873,cmnt:Diffuse Power,varname:node_13,prsc:2|VAL-7-OUT,EXP-14-OUT;n:type:ShaderForge.SFN_RemapRange,id:14,x:31132,y:32959,varname:node_14,prsc:2,frmn:0,frmx:1,tomn:5,tomx:0.5|IN-15-OUT;n:type:ShaderForge.SFN_Slider,id:15,x:30785,y:33054,ptovrint:False,ptlb:Diffuse Power,ptin:_DiffusePower,varname:node_2572,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:20,x:32250,y:32820,varname:node_20,prsc:2|A-1541-OUT,B-125-RGB,C-1975-OUT;n:type:ShaderForge.SFN_LightColor,id:125,x:32000,y:32859,varname:node_125,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:146,x:31094,y:33351,prsc:2,pt:False;n:type:ShaderForge.SFN_ViewVector,id:147,x:31094,y:33512,varname:node_147,prsc:2;n:type:ShaderForge.SFN_Dot,id:148,x:31304,y:33425,varname:node_148,prsc:2,dt:1|A-146-OUT,B-147-OUT;n:type:ShaderForge.SFN_Vector1,id:151,x:31092,y:33651,varname:node_151,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:152,x:31314,y:33692,varname:node_152,prsc:2|A-151-OUT,B-153-OUT;n:type:ShaderForge.SFN_Slider,id:153,x:30935,y:33727,ptovrint:False,ptlb:Atm Size,ptin:_AtmSize,varname:node_7643,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_RemapRange,id:154,x:31522,y:33692,varname:node_154,prsc:2,frmn:0,frmx:1,tomn:0,tomx:10|IN-152-OUT;n:type:ShaderForge.SFN_Power,id:155,x:31801,y:33425,varname:node_155,prsc:2|VAL-573-OUT,EXP-154-OUT;n:type:ShaderForge.SFN_Multiply,id:165,x:32709,y:33412,cmnt:Final Atmosphere,varname:node_165,prsc:2|A-1671-OUT,B-155-OUT,C-168-OUT;n:type:ShaderForge.SFN_Color,id:166,x:32069,y:33520,ptovrint:False,ptlb:Atm Color,ptin:_AtmColor,varname:node_587,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07450981,c2:0.4666667,c3:0.9215686,c4:1;n:type:ShaderForge.SFN_Slider,id:167,x:31912,y:33716,ptovrint:False,ptlb:Atm Power,ptin:_AtmPower,varname:node_8672,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:2,max:10;n:type:ShaderForge.SFN_Multiply,id:168,x:32328,y:33577,varname:node_168,prsc:2|A-166-RGB,B-167-OUT;n:type:ShaderForge.SFN_Add,id:169,x:33278,y:33232,varname:node_169,prsc:2|A-1929-OUT,B-1957-OUT;n:type:ShaderForge.SFN_OneMinus,id:573,x:31547,y:33425,varname:node_573,prsc:2|IN-148-OUT;n:type:ShaderForge.SFN_Tex2d,id:698,x:33426,y:32574,ptovrint:False,ptlb:Normal Map,ptin:_NormalMap,varname:node_9267,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Add,id:774,x:33295,y:32901,varname:node_774,prsc:2|A-1945-OUT,B-1957-OUT;n:type:ShaderForge.SFN_Multiply,id:1541,x:31859,y:32767,varname:node_1541,prsc:2|A-8-RGB,B-1596-OUT;n:type:ShaderForge.SFN_AmbientLight,id:1562,x:31905,y:33248,varname:node_1562,prsc:2;n:type:ShaderForge.SFN_Add,id:1596,x:31719,y:32909,varname:node_1596,prsc:2|A-13-OUT,B-1622-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:1622,x:32102,y:33146,ptovrint:False,ptlb:Enable Ambient,ptin:_EnableAmbient,varname:node_9711,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-1629-OUT,B-1562-RGB;n:type:ShaderForge.SFN_Vector1,id:1629,x:31875,y:33168,varname:node_1629,prsc:2,v1:0;n:type:ShaderForge.SFN_SwitchProperty,id:1671,x:32408,y:33297,ptovrint:False,ptlb:Atm Full Bright,ptin:_AtmFullBright,varname:node_3810,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-13-OUT,B-1674-OUT;n:type:ShaderForge.SFN_Vector1,id:1674,x:32175,y:33386,varname:node_1674,prsc:2,v1:1;n:type:ShaderForge.SFN_ToggleProperty,id:1926,x:32513,y:32696,ptovrint:False,ptlb:Full Bright,ptin:_FullBright,varname:node_2719,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_If,id:1929,x:32706,y:33003,varname:node_1929,prsc:2|A-1926-OUT,B-1930-OUT,GT-8-RGB,EQ-20-OUT,LT-20-OUT;n:type:ShaderForge.SFN_Vector1,id:1930,x:32471,y:32750,varname:node_1930,prsc:2,v1:0;n:type:ShaderForge.SFN_If,id:1945,x:32909,y:32873,varname:node_1945,prsc:2|A-1926-OUT,B-1930-OUT,GT-1930-OUT,EQ-1929-OUT,LT-1929-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:1957,x:32931,y:33353,ptovrint:False,ptlb:Enable Atm,ptin:_EnableAtm,varname:node_9376,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-1964-OUT,B-165-OUT;n:type:ShaderForge.SFN_Vector1,id:1964,x:32706,y:33282,varname:node_1964,prsc:2,v1:0;n:type:ShaderForge.SFN_LightAttenuation,id:1975,x:32000,y:32989,varname:node_1975,prsc:2;proporder:1622-8-698-15-1926-1957-166-167-153-1671;pass:END;sub:END;*/

Shader "Space Builder/Planet BA" {
    Properties {
        [MaterialToggle] _EnableAmbient ("Enable Ambient", Float ) = 0
        _DiffuseMap ("Diffuse Map", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _DiffusePower ("Diffuse Power", Range(0, 1)) = 0.5
        [MaterialToggle] _FullBright ("Full Bright", Float ) = 0
        [MaterialToggle] _EnableAtm ("Enable Atm", Float ) = 0
        _AtmColor ("Atm Color", Color) = (0.07450981,0.4666667,0.9215686,1)
        _AtmPower ("Atm Power", Range(1, 10)) = 2
        _AtmSize ("Atm Size", Range(0, 1)) = 0.5
        [MaterialToggle] _AtmFullBright ("Atm Full Bright", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _DiffuseMap; uniform float4 _DiffuseMap_ST;
            uniform float _DiffusePower;
            uniform float _AtmSize;
            uniform float4 _AtmColor;
            uniform float _AtmPower;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform fixed _EnableAmbient;
            uniform fixed _AtmFullBright;
            uniform fixed _FullBright;
            uniform fixed _EnableAtm;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float node_1930 = 0.0;
                float node_1945_if_leA = step(_FullBright,node_1930);
                float node_1945_if_leB = step(node_1930,_FullBright);
                float node_1929_if_leA = step(_FullBright,node_1930);
                float node_1929_if_leB = step(node_1930,_FullBright);
                float4 _DiffuseMap_var = tex2D(_DiffuseMap,TRANSFORM_TEX(i.uv0, _DiffuseMap));
                float node_13 = pow(max(0,dot(normalDirection,lightDirection)),(_DiffusePower*-4.5+5.0)); // Diffuse Power
                float3 node_20 = ((_DiffuseMap_var.rgb*(node_13+lerp( 0.0, UNITY_LIGHTMODEL_AMBIENT.rgb, _EnableAmbient )))*_LightColor0.rgb*attenuation);
                float3 node_1929 = lerp((node_1929_if_leA*node_20)+(node_1929_if_leB*_DiffuseMap_var.rgb),node_20,node_1929_if_leA*node_1929_if_leB);
                float3 _EnableAtm_var = lerp( 0.0, (lerp( node_13, 1.0, _AtmFullBright )*pow((1.0 - max(0,dot(i.normalDir,viewDirection))),((1.0-_AtmSize)*10.0+0.0))*(_AtmColor.rgb*_AtmPower)), _EnableAtm );
                float3 emissive = (lerp((node_1945_if_leA*node_1929)+(node_1945_if_leB*node_1930),node_1929,node_1945_if_leA*node_1945_if_leB)+_EnableAtm_var);
                float3 finalColor = emissive + (node_1929+_EnableAtm_var);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
