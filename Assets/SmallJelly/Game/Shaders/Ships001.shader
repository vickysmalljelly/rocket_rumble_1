// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33445,y:32629,varname:node_9361,prsc:2|normal-4892-RGB,emission-566-RGB,custl-8028-OUT;n:type:ShaderForge.SFN_LightVector,id:6869,x:31263,y:32776,varname:node_6869,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31263,y:32975,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:7782,x:31482,y:32909,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-6869-OUT,B-9684-OUT;n:type:ShaderForge.SFN_Multiply,id:5155,x:31695,y:32963,varname:node_5155,prsc:2|A-7782-OUT,B-6868-OUT;n:type:ShaderForge.SFN_Multiply,id:6838,x:32329,y:32945,varname:node_6838,prsc:2|A-5155-OUT,B-1614-RGB,C-1090-RGB,D-3707-RGB;n:type:ShaderForge.SFN_Tex2d,id:4892,x:33225,y:32413,ptovrint:False,ptlb:Normal Map,ptin:_NormalMap,varname:node_4892,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9e44ad05c3183084a808826bcfc8c108,ntxv:3,isnm:True;n:type:ShaderForge.SFN_LightAttenuation,id:6868,x:31482,y:33103,varname:node_6868,prsc:2;n:type:ShaderForge.SFN_Append,id:1412,x:31882,y:32822,varname:node_1412,prsc:2|A-5155-OUT,B-5155-OUT;n:type:ShaderForge.SFN_Tex2d,id:1090,x:32090,y:32745,ptovrint:False,ptlb:Ramp,ptin:_Ramp,varname:node_1090,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ebfc125326e2a354ba550770d4c06ec2,ntxv:0,isnm:False|UVIN-1412-OUT;n:type:ShaderForge.SFN_LightColor,id:1614,x:32084,y:33140,varname:node_1614,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5098,x:32536,y:32945,varname:node_5098,prsc:2|A-6838-OUT,B-4487-RGB;n:type:ShaderForge.SFN_Tex2d,id:4487,x:32305,y:33140,ptovrint:False,ptlb:AO,ptin:_AO,varname:node_4487,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:92312ad3a05b7c34cb2fd3c2f1eea904,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:8028,x:32936,y:33055,varname:node_8028,prsc:2|A-5098-OUT,B-6855-OUT,C-7187-OUT,D-5133-OUT,E-9632-RGB;n:type:ShaderForge.SFN_ViewReflectionVector,id:7473,x:31433,y:33522,varname:node_7473,prsc:2;n:type:ShaderForge.SFN_Dot,id:3162,x:31852,y:33561,varname:node_3162,prsc:2,dt:1|A-6869-OUT,B-7473-OUT;n:type:ShaderForge.SFN_Multiply,id:7790,x:32191,y:33376,varname:node_7790,prsc:2|A-2192-RGB,B-3887-OUT;n:type:ShaderForge.SFN_Tex2d,id:2192,x:31912,y:33325,ptovrint:False,ptlb:SpecMap,ptin:_SpecMap,varname:node_2060,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9be44c88f1ab8614aa0675705fd4b81c,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:3887,x:32113,y:33554,varname:node_3887,prsc:2|VAL-3162-OUT,EXP-5105-OUT;n:type:ShaderForge.SFN_Exp,id:5105,x:31893,y:33741,varname:node_5105,prsc:2,et:1|IN-8484-OUT;n:type:ShaderForge.SFN_Slider,id:8484,x:31450,y:33781,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_3237,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:4.65,max:11;n:type:ShaderForge.SFN_Multiply,id:6855,x:32553,y:33386,varname:node_6855,prsc:2|A-7790-OUT,B-822-OUT;n:type:ShaderForge.SFN_Slider,id:822,x:32082,y:33776,ptovrint:False,ptlb:specIntesity,ptin:_specIntesity,varname:node_1989,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.75,max:8;n:type:ShaderForge.SFN_Tex2d,id:3707,x:31882,y:33060,ptovrint:False,ptlb:node_3707,ptin:_node_3707,varname:node_3707,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:3beef8a679e30f8498fa2d24add185b5,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Fresnel,id:2320,x:32534,y:32699,varname:node_2320,prsc:2|EXP-5490-OUT;n:type:ShaderForge.SFN_Slider,id:757,x:31881,y:32555,ptovrint:False,ptlb:Fresnel Power,ptin:_FresnelPower,varname:_Gloss_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:3,max:3;n:type:ShaderForge.SFN_Exp,id:5490,x:32322,y:32513,varname:node_5490,prsc:2,et:1|IN-757-OUT;n:type:ShaderForge.SFN_Multiply,id:7187,x:32732,y:32821,varname:node_7187,prsc:2|A-2320-OUT,B-7059-RGB;n:type:ShaderForge.SFN_Color,id:7059,x:32339,y:32792,ptovrint:False,ptlb:FresnelColor,ptin:_FresnelColor,varname:node_7059,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.167384,c3:0.1838235,c4:1;n:type:ShaderForge.SFN_Color,id:6438,x:32100,y:32346,ptovrint:False,ptlb:FresnelColor 2,ptin:_FresnelColor2,varname:_FresnelColor_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.1004304,c3:0.1102941,c4:1;n:type:ShaderForge.SFN_Multiply,id:5133,x:32493,y:32375,varname:node_5133,prsc:2|A-5516-OUT,B-6438-RGB;n:type:ShaderForge.SFN_Fresnel,id:5516,x:32295,y:32253,varname:node_5516,prsc:2|EXP-8066-OUT;n:type:ShaderForge.SFN_Slider,id:458,x:31672,y:32190,ptovrint:False,ptlb:Fresnel Power 2,ptin:_FresnelPower2,varname:_FresnelPower_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:3;n:type:ShaderForge.SFN_Exp,id:8066,x:32054,y:32131,varname:node_8066,prsc:2,et:1|IN-458-OUT;n:type:ShaderForge.SFN_AmbientLight,id:9632,x:32793,y:33315,varname:node_9632,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:566,x:33026,y:32726,ptovrint:False,ptlb:emmissive map,ptin:_emmissivemap,varname:node_566,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4e51aeed7239ec64ea24db4b786bc57b,ntxv:2,isnm:False;proporder:4892-1090-4487-2192-8484-822-3707-757-7059-6438-458-566;pass:END;sub:END;*/

Shader "Graham Test/ShipShader05" {
    Properties {
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _Ramp ("Ramp", 2D) = "white" {}
        _AO ("AO", 2D) = "white" {}
        _SpecMap ("SpecMap", 2D) = "white" {}
        _Gloss ("Gloss", Range(0.1, 11)) = 4.65
        _specIntesity ("specIntesity", Range(0, 8)) = 0.75
        _node_3707 ("node_3707", 2D) = "black" {}
        _FresnelPower ("Fresnel Power", Range(0.1, 3)) = 3
        _FresnelColor ("FresnelColor", Color) = (0,0.167384,0.1838235,1)
        _FresnelColor2 ("FresnelColor 2", Color) = (0,0.1004304,0.1102941,1)
        _FresnelPower2 ("Fresnel Power 2", Range(0.1, 3)) = 0.1
        _emmissivemap ("emmissive map", 2D) = "black" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform sampler2D _AO; uniform float4 _AO_ST;
            uniform sampler2D _SpecMap; uniform float4 _SpecMap_ST;
            uniform float _Gloss;
            uniform float _specIntesity;
            uniform sampler2D _node_3707; uniform float4 _node_3707_ST;
            uniform float _FresnelPower;
            uniform float4 _FresnelColor;
            uniform float4 _FresnelColor2;
            uniform float _FresnelPower2;
            uniform sampler2D _emmissivemap; uniform float4 _emmissivemap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float4 _emmissivemap_var = tex2D(_emmissivemap,TRANSFORM_TEX(i.uv0, _emmissivemap));
                float3 emissive = _emmissivemap_var.rgb;
                float node_5155 = (max(0,dot(lightDirection,normalDirection))*attenuation);
                float2 node_1412 = float2(node_5155,node_5155);
                float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_1412, _Ramp));
                float4 _node_3707_var = tex2D(_node_3707,TRANSFORM_TEX(i.uv0, _node_3707));
                float4 _AO_var = tex2D(_AO,TRANSFORM_TEX(i.uv0, _AO));
                float4 _SpecMap_var = tex2D(_SpecMap,TRANSFORM_TEX(i.uv0, _SpecMap));
                float3 finalColor = emissive + (((node_5155*_LightColor0.rgb*_Ramp_var.rgb*_node_3707_var.rgb)*_AO_var.rgb)+((_SpecMap_var.rgb*pow(max(0,dot(lightDirection,viewReflectDirection)),exp2(_Gloss)))*_specIntesity)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_FresnelPower))*_FresnelColor.rgb)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_FresnelPower2))*_FresnelColor2.rgb)+UNITY_LIGHTMODEL_AMBIENT.rgb);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform sampler2D _AO; uniform float4 _AO_ST;
            uniform sampler2D _SpecMap; uniform float4 _SpecMap_ST;
            uniform float _Gloss;
            uniform float _specIntesity;
            uniform sampler2D _node_3707; uniform float4 _node_3707_ST;
            uniform float _FresnelPower;
            uniform float4 _FresnelColor;
            uniform float4 _FresnelColor2;
            uniform float _FresnelPower2;
            uniform sampler2D _emmissivemap; uniform float4 _emmissivemap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float node_5155 = (max(0,dot(lightDirection,normalDirection))*attenuation);
                float2 node_1412 = float2(node_5155,node_5155);
                float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_1412, _Ramp));
                float4 _node_3707_var = tex2D(_node_3707,TRANSFORM_TEX(i.uv0, _node_3707));
                float4 _AO_var = tex2D(_AO,TRANSFORM_TEX(i.uv0, _AO));
                float4 _SpecMap_var = tex2D(_SpecMap,TRANSFORM_TEX(i.uv0, _SpecMap));
                float3 finalColor = (((node_5155*_LightColor0.rgb*_Ramp_var.rgb*_node_3707_var.rgb)*_AO_var.rgb)+((_SpecMap_var.rgb*pow(max(0,dot(lightDirection,viewReflectDirection)),exp2(_Gloss)))*_specIntesity)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_FresnelPower))*_FresnelColor.rgb)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),exp2(_FresnelPower2))*_FresnelColor2.rgb)+UNITY_LIGHTMODEL_AMBIENT.rgb);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
