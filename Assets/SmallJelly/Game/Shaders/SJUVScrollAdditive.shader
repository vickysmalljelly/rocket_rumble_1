﻿Shader "SmallJelly/UV Scroll (Additive)" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_USpeed ("U Speed", Float) = 0.2
	_VSpeed ("V Speed", Float) = 0.2
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 100


	ZWrite Off
Blend One One

	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0


			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _USpeed;
			float _VSpeed;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX( float2( v.texcoord.x + (_USpeed * _Time.y), v.texcoord.y + (_VSpeed * _Time.y) ), _MainTex);
           		o.color = v.color;

				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * i.color;
				return col;
			}
		ENDCG
	}
}

}
