// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:Standard,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:32903,y:32687,varname:node_9361,prsc:2|diff-1007-OUT,normal-9294-RGB,emission-1007-OUT;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31904,y:32624,prsc:2,pt:False;n:type:ShaderForge.SFN_LightVector,id:4654,x:31904,y:32787,varname:node_4654,prsc:2;n:type:ShaderForge.SFN_Dot,id:4906,x:32159,y:32676,varname:node_4906,prsc:2,dt:1|A-9684-OUT,B-4654-OUT;n:type:ShaderForge.SFN_Color,id:8561,x:32196,y:32512,ptovrint:False,ptlb:Color2,ptin:_Color2,varname:node_8561,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.01724698,c2:0.0348183,c3:0.2132353,c4:1;n:type:ShaderForge.SFN_Lerp,id:1007,x:32542,y:32738,varname:node_1007,prsc:2|A-532-OUT,B-6286-OUT,T-33-OUT;n:type:ShaderForge.SFN_ComponentMask,id:33,x:32351,y:32798,varname:node_33,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-4906-OUT;n:type:ShaderForge.SFN_Fresnel,id:2845,x:31869,y:32401,varname:node_2845,prsc:2|NRM-9684-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9365,x:32070,y:32401,varname:node_9365,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-2845-OUT;n:type:ShaderForge.SFN_Color,id:586,x:32243,y:32253,ptovrint:False,ptlb:Color1,ptin:_Color1,varname:node_586,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.4586207,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:532,x:32351,y:32617,varname:node_532,prsc:2|A-8561-RGB,B-586-RGB,T-9365-OUT;n:type:ShaderForge.SFN_Color,id:4102,x:32698,y:32232,ptovrint:False,ptlb:Color3,ptin:_Color3,varname:_node_586_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9411765,c2:0.4868154,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:7774,x:32500,y:32277,ptovrint:False,ptlb:Color4,ptin:_Color4,varname:_node_8561_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4632353,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:6286,x:32668,y:32504,varname:node_6286,prsc:2|A-7774-RGB,B-4102-RGB,T-2563-OUT;n:type:ShaderForge.SFN_Tex2d,id:9294,x:32676,y:33010,ptovrint:False,ptlb:node_9294,ptin:_node_9294,varname:node_9294,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:30d039f618679b841932ef272b2c0948,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:2563,x:32453,y:32429,varname:node_2563,prsc:2|A-9365-OUT,B-6378-RGB;n:type:ShaderForge.SFN_Color,id:6378,x:32507,y:32590,ptovrint:False,ptlb:Darken,ptin:_Darken,varname:node_6378,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7720588,c2:0.7720588,c3:0.7720588,c4:1;proporder:8561-586-4102-7774-9294-6378;pass:END;sub:END;*/

Shader "SmallJelly/4 Color Fresnel" {
    Properties {
        _Color2 ("Color2", Color) = (0.01724698,0.0348183,0.2132353,1)
        _Color1 ("Color1", Color) = (0,0.4586207,0.5,1)
        _Color3 ("Color3", Color) = (0.9411765,0.4868154,0,1)
        _Color4 ("Color4", Color) = (0.4632353,0,0,1)
        _node_9294 ("node_9294", 2D) = "bump" {}
        _Darken ("Darken", Color) = (0.7720588,0.7720588,0.7720588,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d9 gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _Color2;
            uniform float4 _Color1;
            uniform float4 _Color3;
            uniform float4 _Color4;
            uniform sampler2D _node_9294; uniform float4 _node_9294_ST;
            uniform float4 _Darken;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_9294_var = UnpackNormal(tex2D(_node_9294,TRANSFORM_TEX(i.uv0, _node_9294)));
                float3 normalLocal = _node_9294_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float node_9365 = (1.0-max(0,dot(i.normalDir, viewDirection))).r;
                float3 node_1007 = lerp(lerp(_Color2.rgb,_Color1.rgb,node_9365),lerp(_Color4.rgb,_Color3.rgb,(node_9365*_Darken.rgb)),max(0,dot(i.normalDir,lightDirection)).r);
                float3 diffuseColor = node_1007;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = node_1007;
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d9 gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _Color2;
            uniform float4 _Color1;
            uniform float4 _Color3;
            uniform float4 _Color4;
            uniform sampler2D _node_9294; uniform float4 _node_9294_ST;
            uniform float4 _Darken;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_9294_var = UnpackNormal(tex2D(_node_9294,TRANSFORM_TEX(i.uv0, _node_9294)));
                float3 normalLocal = _node_9294_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float node_9365 = (1.0-max(0,dot(i.normalDir, viewDirection))).r;
                float3 node_1007 = lerp(lerp(_Color2.rgb,_Color1.rgb,node_9365),lerp(_Color4.rgb,_Color3.rgb,(node_9365*_Darken.rgb)),max(0,dot(i.normalDir,lightDirection)).r);
                float3 diffuseColor = node_1007;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Standard"
    CustomEditor "ShaderForgeMaterialInspector"
}
