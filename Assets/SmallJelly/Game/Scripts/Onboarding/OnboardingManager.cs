﻿using SmallJelly.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace SmallJelly
{
	public class OnboardingManager : MonoBehaviourSingleton< OnboardingManager >
	{
		#region Constants
		public const string RumbleIntroCheckpoint = "RumbleIntroCheckpoint";
		public const string FirstRumbleCheckpoint = "FirstRumble";
		public const string HasSelectedDeckCheckpoint = "HasSelectedDeck";
        public const string Tutorial1CheckPoint = "Tutorial1Won";
        public const string Tutorial2CheckPoint = "Tutorial2Won";
        public const string Tutorial3CheckPoint = "Tutorial3Won";
        public const string Tutorial4CheckPoint = "Tutorial4Won";

		public const string UsernameCheckPoint = "UsernameCheckpoint";
		public const string StandardCardPackBoughtCheckpoint = "StandardCardPackBought";
		public const string PackOpenedCheckpoint = "PackOpened";
		public const string FinishOnboardingCheckpoint = "FinishOnboarding";
		#endregion

		#region Enums
		public enum OnboardingId
		{
			StartScreen,

			DirectUserFromPlayScreenToMap,
			DirectUserFromPlayScreenToShop,
			DirectUserFromPlayScreenToInbox,

			DirectUserFromShopScreenToInbox,

			BuyPackStepOne,
			BuyPackStepTwo,
			BuyPackStepThree,

			OpenPackStepOne,
			OpenPackStepTwo,
			OpenPackStepThree,

			FinishOnboarding
		}
		#endregion

		#region Member Variables
		private GameObject mCurrentOnboarding;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		public bool HasPassedCheckpoint( string checkpoint )
		{
			return GameManager.Get.PlayerData.HasPassedCheckpoint( checkpoint );
		}

		public void SetCheckpointOnServer( string checkpoint ) 
		{
			ClientOnboarding.SetCheckpoint( checkpoint, HandleSetCheckpointSuccess, HandleSetCheckpointServerError );
		}

		public void SetCheckpointOnClient( string checkpoint ) 
		{
			GameManager.Get.PlayerData.SetCheckpointOnClient( checkpoint );
		}

		public void StartOnboardingSection( OnboardingId onboardingId )
		{
			SJLogger.AssertCondition( !OnboardingIsActive(), "We're attempting to start an onboarding section when one's already active" );

			string resourcePath = GetResourcePath( onboardingId );
			GameObject resource = Resources.Load< GameObject >( resourcePath );

			mCurrentOnboarding = Instantiate( resource );
		}

		public void StopOnboardingSection()
		{
			SJLogger.AssertCondition( OnboardingIsActive(), "We're attempting to stop an onboarding section that doesn't exist" );

			Destroy( mCurrentOnboarding );
			mCurrentOnboarding = null;
		}

		public bool OnboardingIsActive()
		{
			return mCurrentOnboarding != null;
		}

        public void FinishOnboarding()
        {
            // Re-enable the Buy Crystals button.
            FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();
            frontEndNavigationController.ToggleCrystalsBuyButton(true);  
        }
		#endregion

		#region Private Methods
		private string GetResourcePath( OnboardingId onboardingId )
		{
			switch( onboardingId )
			{
				case OnboardingId.StartScreen:
					return FileLocations.OnboardingStartScreen;


				case OnboardingId.DirectUserFromPlayScreenToShop:
					return FileLocations.DirectUserFromPlayScreenToShopPrefab;

				case OnboardingId.DirectUserFromPlayScreenToInbox:
					return FileLocations.DirectUserFromPlayScreenToInboxPrefab;

				case OnboardingId.DirectUserFromPlayScreenToMap:
					return FileLocations.DirectUserFromPlayScreenToMapPrefab;

				case OnboardingId.DirectUserFromShopScreenToInbox:
					return FileLocations.DirectUserFromShopScreenToInboxPrefab;



				case OnboardingId.BuyPackStepOne:
					return FileLocations.BuyPackStepOnePrefab;

				case OnboardingId.BuyPackStepTwo:
					return FileLocations.BuyPackStepTwoPrefab;

				case OnboardingId.BuyPackStepThree:
					return FileLocations.BuyPackStepThreePrefab;


				case OnboardingId.OpenPackStepOne:
					return FileLocations.OpenPackStepOnePrefab;

				case OnboardingId.OpenPackStepTwo:
					return FileLocations.OpenPackStepTwoPrefab;

				case OnboardingId.OpenPackStepThree:
					return FileLocations.OpenPackStepThreePrefab;


				case OnboardingId.FinishOnboarding:
					return FileLocations.FinishOnboardingPrefab;
			
			
				default:
					SJLogger.Assert( "No prefab resource was defined for the id {0}", onboardingId.ToString() );
					return string.Empty;

			}
		}
		#endregion

		#region Event Handlers
		private void HandleSetCheckpointSuccess()
		{
			//Do nothing
		}

		private void HandleSetCheckpointServerError( ServerError serverError )
		{
			//Do nothing
		}
		#endregion
	}

}