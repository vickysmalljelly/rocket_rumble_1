﻿using System.Collections.Generic;
using GameAnalyticsSDK;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Game Analytics implementation.
    /// 
    /// Custom events need to be carefully structured into hierarchies in order to obtain the correct visualisations on the metrics.
    /// http://www.gameanalytics.com/docs/custom-events
    /// </summary>
    public class GameAnalyticsImplementation : IAnalyticsProvider
    {
        public void Initialise() 
        {
        }
            
        public void RecordTimeoutEvent(string cluster, string shortCode, string country, string city, string playerId)
        {
            GameAnalytics.NewDesignEvent("timeout:" + cluster + ":" + country + ":" + shortCode + ":" + city + ":" + playerId);
        }

        public void RecordConnectionLoss(string s1)
        {
            GameAnalytics.NewDesignEvent("connection_loss:" + s1);
        }

        public void RecordBattleEvent(string s1, string s2)
        {
           GameAnalytics.NewDesignEvent("battle:" + s1 + ":" + s2);
        }

        public void RecordTutorialBattleEvent(string s1, string s2)
        {
           GameAnalytics.NewDesignEvent("tutorial_battle:" + s1 + ":" + s2);
        }

        public void RecordStealthAiBattleEvent(string s1, string s2)
        {
           GameAnalytics.NewDesignEvent("stealth_ai_battle:" + s1 + ":" + s2);
        }

        public void RecordBattleResult(AnalyticsManager.BattleResult result, string className)
        {
            GameAnalytics.NewDesignEvent("battle_result:" + result.ToString() + ":" + className);
        }

        public void RecordTutorialBattleResult(AnalyticsManager.BattleResult result, string className)
        {
            GameAnalytics.NewDesignEvent("tutorial_battle_result:" + result.ToString() + ":" + className);
        }

        public void RecordStealthAiBattleResult(AnalyticsManager.BattleResult result, string className)
        {
            GameAnalytics.NewDesignEvent("stealth_ai_battle_result:" + result.ToString() + ":" + className);
        }

        public void RecordGameProgress(ProgressStatus status, string s1, string s2, string s3)
        {
            GAProgressionStatus progressionStatus;

            switch(status)
            {
                case ProgressStatus.Start:
                    progressionStatus = GAProgressionStatus.Start;
                    break;
                case ProgressStatus.Complete:
                    progressionStatus = GAProgressionStatus.Complete;
                    break;
                case ProgressStatus.Fail:
                    progressionStatus = GAProgressionStatus.Fail;
                    break;
                case ProgressStatus.Undefined:
                    progressionStatus = GAProgressionStatus.Undefined;
                    break;
                default: 
                    SJLogger.LogError("GameProgressionStatus {0} not recognised", status);
                    return;
            }

            SJLogger.AssertCondition(!string.IsNullOrEmpty(s1), "s1 cannot be null or empty");

            if(!string.IsNullOrEmpty(s3))
            {
                SJLogger.AssertCondition(!string.IsNullOrEmpty(s2), "s2 cannot be null or empty if we have s3");
                GameAnalytics.NewProgressionEvent(progressionStatus, s1, s2, s3);
            }
            else if(!string.IsNullOrEmpty(s2))
            {
                GameAnalytics.NewProgressionEvent(progressionStatus, s1, s2);
            }
            else 
            {
                GameAnalytics.NewProgressionEvent(progressionStatus, s1);
            }    
        }

        public void RecordGameProgress(AnalyticsManager.GameProgress progress, int step)
        {
           GameAnalytics.NewDesignEvent("progress:" + progress.ToString() + "_" + step);
        }            

        public void RecordRegistrationSuccess(bool success)
        {
           GameAnalytics.NewDesignEvent("registration:" + success.ToString());
        }

        public void RecordClassSelect(string className)
        {
           GameAnalytics.NewDesignEvent("class:" + className.ToString());
        }

        public void RecordPlayCard(AnalyticsManager.PlayCardResult result)
        {
           GameAnalytics.NewDesignEvent("play_card:" + result.ToString());
        }
                
        public void RecordFrameRate(string deviceModel, int qualitySetting, float frameRate)
        {
          GameAnalytics.NewDesignEvent(string.Format("framerate:{0}:{1}", deviceModel, qualitySetting), frameRate);
        }
            
        public void RecordRealMoneyPurchase(string currency, int amount, string itemType, string itemId, string cartType)
        {
           GameAnalytics.NewBusinessEvent(currency, amount, itemType, itemId, cartType);
        }
            
        public void RecordAddResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId)
        {            
          GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, virtualCurrency.ToString(), amount, itemType, itemId);
        }

        public void RecordSubtractResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId)
        {
          GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, virtualCurrency.ToString(), amount, itemType, itemId);
        }

        public void RecordPacksBought(int numPacks)
        {
            GameAnalytics.NewDesignEvent(string.Format("packs_bought:{0}", numPacks));
        }

        public void RecordNewDeck()
        {
            GameAnalytics.NewDesignEvent("new_deck");
        }
    }
}