﻿#if !ANALYTICS_DISABLED
/*
 * 
 * VS: Taking out for now as limits are too harsh.
 * 
 * 
using UnityEngine;
using UnityEngine.Analytics;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Unity analytics implementation.
    /// 
    /// Note that there are many limits on number of events per user, number of parameters per event etc.
    /// These will be communicated in the AnalyticsResult.
    /// 
    /// Take care when constructing events:
    /// All numbers, ints, floats, etc., even if sent as strings, are parsed as numbers.
    /// Only strings and Booleans are considered ‘categorizable’.
    /// Consequently, if you want something to be summed or averaged, send it as a number (e.g., 51 or ‘51’). 
    /// If you want it to be categorized, as you would with a level or option, make sure it will parse as a string (e.g., ‘Level51’).
    /// </summary>
    public class UnityAnalytics : IAnalyticsProvider
    {
        public void RecordGameProgress(AnalyticsManager.GameProgress progress, int step)
        {
            ProcessResult(Analytics.CustomEvent("progress:" + progress.ToString() + "_" + step));
        }

        public void RecordRegistrationSuccess(bool success)
        {
            ProcessResult(Analytics.CustomEvent("registration:" + success.ToString()));
        }

        public void RecordClassSelect(string className)
        {
            ProcessResult(Analytics.CustomEvent("class:" + className.ToString()));
        }

        public void RecordPlayCard(AnalyticsManager.PlayCardResult result)
        {
            ProcessResult(Analytics.CustomEvent("play_card:" + result.ToString()));
        }

        public void RecordBattleResult(AnalyticsManager.BattleResult result, string className)
        {
            ProcessResult(Analytics.CustomEvent("battle_result:" + result.ToString() + ":" + className));
        }

        private void ProcessResult(AnalyticsResult result)
        {
            if(result != AnalyticsResult.Ok)
            {
                SJLogger.LogError("UnityAnalytics error: " + result);
            }
        }
    }
}
*/
#endif