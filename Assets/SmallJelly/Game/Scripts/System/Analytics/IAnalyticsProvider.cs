﻿
namespace SmallJelly
{
    /// <summary>
    /// Interface for custom analytics events
    /// </summary>
    interface IAnalyticsProvider 
    {
        void Initialise();
        void RecordGameProgress(ProgressStatus status, string s1, string s2, string s3);
        void RecordBattleEvent(string s1, string s2);
        void RecordTutorialBattleEvent(string s1, string s2);
        void RecordStealthAiBattleEvent(string s1, string s2);
        void RecordBattleResult(AnalyticsManager.BattleResult result, string className);
        void RecordTutorialBattleResult(AnalyticsManager.BattleResult result, string className);
        void RecordStealthAiBattleResult(AnalyticsManager.BattleResult result, string className);
        void RecordGameProgress(AnalyticsManager.GameProgress progress, int step);
        void RecordRegistrationSuccess(bool success);
        void RecordClassSelect(string className);
        void RecordPlayCard(AnalyticsManager.PlayCardResult result);       
        void RecordFrameRate(string deviceModel, int qualitySetting, float frameRate);
        void RecordRealMoneyPurchase(string currency, int amount, string itemType, string itemId, string cartType);
        void RecordAddResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId);
        void RecordSubtractResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId);
        void RecordConnectionLoss(string s1);
        void RecordPacksBought(int numPacks);
        void RecordNewDeck();
        void RecordTimeoutEvent(string cluster, string shortCode, string country, string city, string playerId);
    }
}