﻿using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
    public enum ProgressArea
    {
        NotSet,
        Launch,
        Onboarding,
        Authentication
    }

    public enum ProgressStatus
    {
        NotSet,
        Start,
        Complete,
        Fail,
        Undefined
    }

    /// <summary>
    /// Manages all anaytics in Rocket Rumble
    /// </summary>
    public class AnalyticsManager : MonoBehaviourSingleton<AnalyticsManager>
    {
        public enum GameProgress
        {
            NotSet,
            Rumble,
            StartBattle,
            Mulligan,
            QuitBattle,
            BattleTurn,
            CompletedBattle
        }

        public enum PlayCardResult
        {
            Success,
            NotEnoughPower,
            ConnectionError,
            Other
        }

        public enum BattleResult
        {
            Won,
            Lost
        }

        protected override void Awake()
        {
            #if !ANALYTICS_DISABLED && !UNITY_IOS
            // Initialise analytics providers
            mAnalyticsProviders.Add(new GameAnalyticsImplementation());
            //mAnalyticsProviders.Add(new UnityAnalytics());
            UnityEngine.Debug.Log("Analytics enabled");
            #else
            UnityEngine.Debug.Log("Analytics disabled");
            #endif

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.Initialise();
            }
        }

        private List<IAnalyticsProvider> mAnalyticsProviders = new List<IAnalyticsProvider>();

        #region Custom Analytic Events
        public void RecordTimeoutEvent(string cluster, string shortCode, string country, string city, string playerId)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordTimeoutEvent {0}, {1}, {2}, {3}, {4}", cluster, shortCode, country, city, playerId);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordTimeoutEvent(cluster, shortCode, country, city, playerId);   
            }
        }

        public void RecordConnectionLossEvent(string context)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordConnectionLossEvent {0}", context);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordConnectionLoss(context);   
            }
        }

        public void RecordAddResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordAddResourceEvent {0} {1} {2} {3}", virtualCurrency, amount, itemType, itemId);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordAddResourceEvent(virtualCurrency, amount, itemType, itemId);   
            }
        }

        public void RecordSubtractResourceEvent(IapData.Currency virtualCurrency, int amount, string itemType, string itemId)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordSubtractResourceEvent {0} {1} {2} {3}", virtualCurrency, amount, itemType, itemId);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordSubtractResourceEvent(virtualCurrency, amount, itemType, itemId);   
            }
        }

        public void RecordRealMoneyPurchase(string currency, int amount, string itemType, string itemId, string cartType)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordRealMoneyPurchase {0} {1} {2} {3} {4}", currency, amount, itemType, itemId, cartType);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordRealMoneyPurchase(currency, amount, itemType, itemId, cartType);   
            }
        }            

        public void RecordFrameRate(string deviceName, int qualitySetting, float frameRate)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordFrameRate {0} {1} {2}", deviceName, qualitySetting, frameRate);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordFrameRate(deviceName, qualitySetting, frameRate);   
            }
        }

        /// <summary>
        /// Used to analyse how far players get through the game.
        /// </summary>
        /// <param name="progress">Progress.</param>
        /// <param name="step">Step.</param>
        public void RecordGameProgress(GameProgress progress, int step)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "GameProgress {0} {1}", progress, step);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordGameProgress(progress, step);   
            }
        }     

        public void RecordBattleResult(BattleId battleId, AnalyticsManager.BattleResult result, string className)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordBattleResult {0} {1} {2}", battleId, result, className);

            switch(battleId)
            {
                case BattleId.PvP:
                    RecordBattleEvent(className, result.ToString());
                    RecordBattleResult(result, className);
                    break;
                case BattleId.Tutorial1:
                case BattleId.Tutorial2:
                case BattleId.Tutorial3:
                case BattleId.Tutorial4:
                    RecordTutorialBattleEvent(className, result.ToString());
                    RecordTutorialBattleResult(result, className);
                    break;
                case BattleId.SinglePlayer:
                    RecordStealthAiBattleEvent(className, result.ToString());
                    RecordStealthAiBattleResult(result, className);
                    break;
                default:
                    SJLogger.LogError("RecordBattleResult: BattleId {0} not recognised", battleId);
                    break;
            }
        }

        public void RecordBattleEvent(string s1, string s2)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "BattleEvent {0} {1}", s1, s2);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordBattleEvent(s1, s2);   
            }
        } 

        private void RecordTutorialBattleEvent(string s1, string s2)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "BattleEvent {0} {1}", s1, s2);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordTutorialBattleEvent(s1, s2);   
            }
        }  

        private void RecordStealthAiBattleEvent(string s1, string s2)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "BattleEvent {0} {1}", s1, s2);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordStealthAiBattleEvent(s1, s2);   
            }
        }  
            
        private void RecordBattleResult(BattleResult result, string className)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordBattleResult result {0}, class {1}", result, className);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordBattleResult(result, className);   
            }
        }
            
        private void RecordTutorialBattleResult(BattleResult result, string className)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordBattleResult result {0}, class {1}", result, className);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordTutorialBattleResult(result, className);   
            }
        }

        private void RecordStealthAiBattleResult(BattleResult result, string className)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordBattleResult result {0}, class {1}", result, className);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordStealthAiBattleResult(result, className);   
            }
        }

        public void RecordGameProgress(ProgressStatus status, ProgressArea progressArea, string s2, string s3)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "GameProgress {0}: {1}, {2}, {3} ", status, progressArea, s2, s3);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordGameProgress(status, progressArea.ToString(), s2, s3);   
            }
        }           

        /// <summary>
        /// Used to analyse if people are having problems registering
        /// </summary>
        /// <param name="success">If set to <c>true</c> success.</param>
        public void RecordRegistration(bool success)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordRegistration success {0}", success);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordRegistrationSuccess(success);   
            }
        }

        /// <summary>
        /// Records which class the player selected going in to battle.
        /// </summary>
        /// <param name="className">Class name.</param>
        public void RecordClassSelect(string className)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordClassSelect className {0}", className);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
              item.RecordClassSelect(className);   
            }
        }

        /// <summary>
        /// Records when a card is played successfully vs failing to play the card, for example when there is not enought power.
        /// </summary>
        public void RecordPlayCard(PlayCardResult result)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordPlayCard result {0}", result);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordPlayCard(result);   
            }
        }

        public void RecordPacksBought(int numPacks)
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordPacksBought numPacks {0}", numPacks);

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordPacksBought(numPacks);   
            }
        }

        public void RecordNewDeck()
        {
            SJLogger.LogMessage(MessageFilter.Analytics, "RecordNewDeck");

            foreach(IAnalyticsProvider item in mAnalyticsProviders)
            {
                item.RecordNewDeck();   
            }
        }
            
        #endregion
    }

}