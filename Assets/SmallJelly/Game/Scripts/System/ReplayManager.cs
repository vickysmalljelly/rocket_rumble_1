﻿using SmallJelly.Framework;

namespace SmallJelly
{
    public class ReplayManager : MonoBehaviourSingleton<ReplayManager>
    {
        /// <summary>
        /// The user we will use as the remote player
        /// </summary>
        public User UserToChallenge;

        /// <summary>
        /// The user from whose point of view we will watch the replay
        /// </summary>
        public User UserToRunReplay;

        public bool ReplayOver 
        { 
            get { return mReplayOver; } 
        }
            
        private bool mReplayOver;
        private bool mReplayStarted;
        private string mReplayName;

        public void Initialise()
        {
            mReplayName = string.Empty;
            mReplayStarted = false;
            mReplayOver = false;
        }

        public void SetReplayName(string replayName) 
        {
            mReplayName = replayName;
        }
            
        public void StartReplay()
        {
			SJLogger.AssertCondition(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay,
				"Can not start replay as battle mode is not in the correct mode.");

            if(mReplayStarted) {
                return;
            }

            ClientReplay.StartReplay(mReplayName, () => { 
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Replay started");
            }, (ServerError error) => {
                SJLogger.LogError("Failed to start the replay: {0}", error.UserFacingMessage);
            });

            mReplayStarted = true;
        }
         
        public void NotifyReplayOver()
        {
            mReplayOver = true;
        }
            
    }
}
