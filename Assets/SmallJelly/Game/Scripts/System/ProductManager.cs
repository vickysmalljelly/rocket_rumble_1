﻿using System;
using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.Purchasing;

namespace SmallJelly
{
    /// <summary>
    /// Manager for querying store data.
    /// See IAPDemo.cs for some example code
    /// </summary>
    public class ProductManager : MonoBehaviourSingleton<ProductManager>, IStoreListener
    {
        public event Action<Product> PurchaseSuccess;
        public event Action<string, PurchaseFailureReason> PurchaseFailed;

        /// <summary>
        /// Product ids.
        /// </summary>
        public readonly string CRYSTAL_PACK_1 = "crystal_pack.1";
        public readonly string CRYSTAL_PACK_2 = "crystal_pack.2";
        public readonly string CRYSTAL_PACK_3 = "crystal_pack.3";
        public readonly string CRYSTAL_PACK_4 = "crystal_pack.4";
        public readonly string CRYSTAL_PACK_5 = "crystal_pack.5";

        private static IStoreController mStoreController; 
        private static InitializationFailureReason mFailureReason;
        private static bool mInitialising;

        private void Start()
        {           
            Initialise();

            // TODO: We probably also want to refresh the product list now and again using IStoreController.FetchAdditionalProducts
        }
            
        #region Nested class
        /// <summary>
        /// For deserialising the receipt json.
        /// </summary>
        private class GooglePurchaseData
        {
            // INAPP_PURCHASE_DATA
            public string InAppPurchaseData { get; private set; }
            // INAPP_DATA_SIGNATURE
            public string InAppDataSignature { get; private set; }

            // We are making the purchase in editor
            public bool Fake;

            [System.Serializable]
            private struct GooglePurchaseReceipt
            {
                public string Payload;
                public string Store;
            }

            [System.Serializable]
            private struct GooglePurchasePayload
            {
                public string json;
                public string signature;
            }

            public GooglePurchaseData(string receipt)
            {
                try
                {
                    GooglePurchaseReceipt purchaseReceipt = JsonUtility.FromJson<GooglePurchaseReceipt>(receipt);
                    if(String.Equals(purchaseReceipt.Store, "fake"))
                    {
                        Fake = true;
                        return;
                    }

                    GooglePurchasePayload purchasePayload = JsonUtility.FromJson<GooglePurchasePayload>(purchaseReceipt.Payload);
                    InAppPurchaseData = purchasePayload.json;
                    InAppDataSignature = purchasePayload.signature;
                } 
                catch
                {
                    Debug.LogError("Could not parse receipt: " + receipt);
                }
            }
        }
        #endregion

        #region IStoreListener
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            mInitialising = false;

            // Purchasing has succeeded initializing. Collect our Purchasing references.           
            // Overall Purchasing system, configured with products for this application.
            mStoreController = controller;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            mInitialising = false;
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            mFailureReason = error;
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + mFailureReason);
        }                     

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {  
            GooglePurchaseData data = new GooglePurchaseData(args.purchasedProduct.receipt);

            if(data.Fake)
            {
                // We are on device
                return PurchaseProcessingResult.Complete;
            }
                
            ClientShop.BuyVirtualGoodFromGooglePlay(data.InAppDataSignature, data.InAppPurchaseData, args.purchasedProduct, SuccessHandler, ErrorHandler);

            return PurchaseProcessingResult.Pending;
        }

        private void SuccessHandler(Product product)
        {
            // Tell the store that we have given the player the product.
            mStoreController.ConfirmPendingPurchase(product);

            if(PurchaseSuccess == null)
            {
                // No one is listening
                return;
            }

            PurchaseSuccess(product);
        }

        private void ErrorHandler(ServerError error)
        {
            // What do we do here??
            // The Unity IAP sytem will keep calling ProcessPurchase until we confirm the pending purchase, so maybe nothing?
            Debug.LogError(error.TechnicalDetails);
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.Log("OnPurchaseFailed, reason = " + failureReason);

            if(PurchaseFailed == null)
            {
                // No one is listening
                return;
            }              
             
            PurchaseFailed(product.definition.id, failureReason);
        }
        #endregion

        public string GetLocalisedPriceString(string productId)
        {
            if(mStoreController == null)
            {
                SJLogger.LogWarning("GetLocalisedPriceString: store not initialised, trying again");
                Initialise();
                return string.Empty;
            }               

            var product = mStoreController.products.WithID(productId);           
            return product.metadata.localizedPriceString;
        }

        /// <summary>
        /// Attempts to buy the product.  Returns true if we were able to process the request, false otherwise.
        /// Listen to PurchaseSuccess and PurchaseFailed for response.
        /// </summary>
        public bool BuyConsumable(string productId)
        {
            if(mStoreController == null)
            {
                SJLogger.LogWarning("BuyConsumable: store not initialised, trying again");
                Initialise();
                return false;
            }

            Product product = mStoreController.products.WithStoreSpecificID(productId);

            if(product == null)
            {
                SJLogger.LogError("Product {0} not found", productId);
                return false;
            }

            if(!product.availableToPurchase)
            {
                SJLogger.LogError("Product not available");
                return false;
            }

            Debug.Log("InitiatePurchase of " + productId);
            // Expect a response either through ProcessPurchase or OnPurchaseFailed 
            mStoreController.InitiatePurchase(product);

            return true;
        }

        private void Initialise()
        {
            if(mInitialising)
            {
                return;
            }

            mInitialising = true;

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add all iaps to the Unity system

            // TODO: add with store specific ids when we support more than Google Play
            builder.AddProduct(CRYSTAL_PACK_1, ProductType.Consumable);
            builder.AddProduct(CRYSTAL_PACK_2, ProductType.Consumable);
            builder.AddProduct(CRYSTAL_PACK_3, ProductType.Consumable);
            builder.AddProduct(CRYSTAL_PACK_4, ProductType.Consumable);
            builder.AddProduct(CRYSTAL_PACK_5, ProductType.Consumable);           

            // Will callback either OnInitialized or OnInitializeFailed
            UnityPurchasing.Initialize(this, builder);
        }
    }
}