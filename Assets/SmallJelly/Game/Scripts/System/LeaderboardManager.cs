﻿using System;
using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
    public enum LeaderboardId
    {
        NotSet,
        All,
        AllRecent,
        Smuggler,
        SmugglerRecent,
        Enforcer,
        EnforcerRecent,
        Ancient,
        AncientRecent
    }
        
    public class LeaderboardManager : MonoBehaviourSingleton<LeaderboardManager>
    {
        public event Action<long> LeaderboardPositionUpdated;

        #region Private data
        private long mLeaderboardPosition;
        private bool mLeaderboardPositionNeedsUpdate = true;
        private bool mRecentBoardsInitialised;

        private LeaderboardData mRecentSmuggler;
        private LeaderboardData mRecentEnforcer;
        private LeaderboardData mRecentAncient;
        private LeaderboardData mRecentAllClasses;
        #endregion

        #region Public Methods
        public string GetShortCodeForLeaderboard(LeaderboardId id)
        {
            switch(id)
            {
                case LeaderboardId.AllRecent:
                case LeaderboardId.AncientRecent:
                case LeaderboardId.EnforcerRecent:
                case LeaderboardId.SmugglerRecent:
                    if(!mRecentBoardsInitialised)
                    {
                        SJLogger.LogError("Recent leaderboards not updated");
                        return string.Empty;
                    }
                    break;    
            }

            switch(id)
            {
                case LeaderboardId.All:
                    return "allWins";

                case LeaderboardId.Smuggler:
                    return "allWinsClass.class.Smuggler";

                case LeaderboardId.Enforcer:
                    return "allWinsClass.class.Enforcer";

                case LeaderboardId.Ancient:
                    return "allWinsClass.class.Ancient";

                case LeaderboardId.AllRecent:
                    return mRecentAllClasses.ShortCode;
                    
                case LeaderboardId.AncientRecent:
                    return mRecentAncient.ShortCode;

                case LeaderboardId.EnforcerRecent:
                    return mRecentEnforcer.ShortCode;

                case LeaderboardId.SmugglerRecent:                   
                    return mRecentSmuggler.ShortCode;

                default:
                    SJLogger.LogError("Failed to reccognised leaderboard id [{0}]", id);
                    return "allWins";
            }                   
        }

        public void UpdateLeaderboardPosition()
        {
            if(mLeaderboardPositionNeedsUpdate)
            {
                ClientLeaderboard.AroundMe("allWins", 1, HandleLeaderboardUpdateSuccess, HandleLeaderboardUpdateError);
                ClientLeaderboard.ListLeaderboards(HandleListLeaderboardsSuccess, HandleListLeaderboardsError);
            } else
            {
                // No need to query the server, send cached value
                if(LeaderboardPositionUpdated != null)
                {
                    LeaderboardPositionUpdated(mLeaderboardPosition);
                }
            }
        }

        public void SetLeaderboardPositionNeedsRefresh()
        {
            mLeaderboardPositionNeedsUpdate = true;
            UpdateLeaderboardPosition();
        }
        #endregion

        #region Private Methods
        private void HandleLeaderboardUpdateSuccess(List<LeaderboardRowNumWins> list)
        {            
            mLeaderboardPositionNeedsUpdate = false;

            foreach(LeaderboardRowNumWins data in list)
            {
                if(data.UserId == ClientAuthentication.PlayerAccountDetails.UserId)
                {
                    SJLogger.LogMessage(MessageFilter.Gameplay, "Player {0}, Leaderboard Rank {1}", data.DisplayName, data.Rank);
                    mLeaderboardPosition = data.Rank;

                    if(LeaderboardPositionUpdated != null)
                    {
                        LeaderboardPositionUpdated(mLeaderboardPosition);
                    }
                }
            }
        }

        private void HandleLeaderboardUpdateError(ServerError error)
        {
            mLeaderboardPositionNeedsUpdate = true;
            Debug.LogError(error.ToString());
        }

        private void HandleListLeaderboardsSuccess(List<LeaderboardData> list)
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Total {0} leaderboards", list.Count);

            List<LeaderboardData> allWinsRecent = list.FindAll(data => (string.Compare(data.Name, "allWinsRecent") == 0));
            int positionOfTimestamp = 3;
            mRecentAllClasses = FindMostRecentLeaderboard(allWinsRecent, positionOfTimestamp);

            List<LeaderboardData> allWinsClassRecent = list.FindAll(data => (string.Compare(data.Name, "allWinsClassRecent") == 0));

            List<LeaderboardData> smuggler = new List<LeaderboardData>();
            List<LeaderboardData> enforcer = new List<LeaderboardData>();
            List<LeaderboardData> ancient = new List<LeaderboardData>();

            foreach(LeaderboardData data in allWinsClassRecent)
            {                
                //SJLogger.LogMessage(MessageFilter.Gameplay, "allWinsClassRecent leaderboards {0}, {1}", data.Name, data.ShortCode);

                switch(data.GetShortCodePartition(2))
                {
                    case "Smuggler":
                        smuggler.Add(data);
                        break;
                    case "Enforcer":
                        enforcer.Add(data);
                        break;
                    case "Ancient":
                        ancient.Add(data);
                        break;
                    default:
                        SJLogger.LogError("HandleListLeaderboardsSuccess - case [{0}] not recognised", data.GetShortCodePartition(2));
                        break;
                }                        
            }

            mRecentSmuggler = FindMostRecentLeaderboard(smuggler, 4);
            mRecentEnforcer = FindMostRecentLeaderboard(enforcer, 4);
            mRecentAncient = FindMostRecentLeaderboard(ancient, 4);

            if(mRecentAllClasses != null && mRecentEnforcer != null && mRecentSmuggler != null && mRecentAncient != null)
            {
                mRecentBoardsInitialised = true;
            }
        }

        private void HandleListLeaderboardsError(ServerError error)
        {
            SJLogger.LogError("HandleListLeaderboardsError - " + error.ErrorType + " " + error.TechnicalDetails);
        }

        private LeaderboardData FindMostRecentLeaderboard(List<LeaderboardData> list, int timeStampIndex)
        {
            LeaderboardData mostRecent = null;

            foreach(LeaderboardData data in list)
            {
                if(mostRecent == null)
                {
                    mostRecent = data;
                    //Debug.Log("Timestamp init: " + mostRecent.GetTimestampForWeek(timeStampIndex));
                } else
                {
                    //Debug.Log("Timestamp 1: " + mostRecent.GetTimestampForWeek(timeStampIndex));
                    //Debug.Log("Timestamp 2: " + data.GetTimestampForWeek(timeStampIndex));
                    if(mostRecent.GetTimestampForWeek(timeStampIndex) < data.GetTimestampForWeek(timeStampIndex))
                    {
                        mostRecent = data;
                    }
                }
            }

            SJLogger.AssertCondition(mostRecent != null, "Failed to find recent leaderboard");
            if(mostRecent != null)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "Most recent = {0}", mostRecent.ShortCode);
            }
            return mostRecent;
        }
        #endregion
    }
}
