﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Groups all requests relating to setting up a challenge, so that we can use generic error handling when a connection fails.
    /// </summary>
	public class ChallengeSetupManager : MonoBehaviourSingleton< ChallengeSetupManager >
	{
		#region Public Event Handlers
        // General success event used for several types of event
		public event EventHandler< ChallengeSetupEventDataEventArgs > Success;

        // Specific success events
        public event Action<User> FindUserSuccess;
        public event Action<string> RequestChallengeSuccess;

        // GameManager listens to this and takes appropriate action if it fires.
        public event Action ConnectionError;
        // All users of this class should listen to this for errors unrelated to connection.
        public event Action<ServerError> Error;
		#endregion

		#region Public Methods
		public void ListDecks()
		{
			ClientDeck.ListDecks( HandleListDecks, HandleServerError );
		}

		public void SetCurrentDeck( string chosenDeck )
		{
			ClientDeck.SetCurrentDeck( chosenDeck, HandleSetCurrentDeck, HandleServerError );
		}

		public void RequestChallenge( Dictionary<string, string> scriptData, List< string > users )
		{
			ClientChallenge.RequestChallenge( BattleModeManager.Get.GetChallengeShortCode(), scriptData, users, HandleChallengeRequestSuccess, HandleServerError );
		}

		public void WithdrawChallenge( string challengeId )
		{
			ClientChallenge.WithdrawChallenge( challengeId, HandleWithdrawChallengeSuccess, HandleServerError);
		}

		public void FindUserByName( string user )
		{
			ClientUser.FindUserByName( user, HandleFindUserSuccess, HandleServerError );
		}

        public void FindMatch(string shortCode)
        {
            ClientMatchmaking.FindMatch(shortCode, HandleFindMatchSuccess, HandleServerError);
        }
		#endregion

		#region Event Handlers
        private void HandleFindMatchSuccess()
        {
            // No need to pass this event on, as it will trigger ChallengeManager.Get.ChallengeCreated
        }
                
		private void HandleListDecks( List< Deck > decks )
		{
			FireChallengeSetupEventDataReceived( new ListDecksEventData( decks ) );
		}

		private void HandleSetCurrentDeck()
		{
			FireChallengeSetupEventDataReceived( new SetCurrentDeckEventData() );
		}

		private void HandleChallengeRequestSuccess( string challengeId )
		{
            if(RequestChallengeSuccess != null)
            {
                RequestChallengeSuccess(challengeId);
            }              
            else
            {
                Debug.LogError("HandleChallengeRequestSuccess, no one listening to RequestChallengeSuccess");
            }
		}

		private void HandleWithdrawChallengeSuccess()
		{
			FireChallengeSetupEventDataReceived( new WithdrawChallengeEventData() );
		}

		private void HandleFindUserSuccess( User user )
		{
            if(FindUserSuccess != null)
            {
                FindUserSuccess(user);
            }
		}

		private void HandleServerError( ServerError error )
		{
			switch( error.ErrorType )
			{
				case ServerErrorType.Timeout:
                    
                    if(ConnectionError != null )
                    {
                        ConnectionError();
                    }
					break;

				default:

                    if(Error != null)
                    {
                        Error(error);
                    }
					break;
			}
		}

        private void FireChallengeSetupEventDataReceived( ChallengeSetupEventData shopEventData )
        {
            if( Success != null )
            {
                Success( this, new ChallengeSetupEventDataEventArgs( shopEventData ) );
            }
        }


		#endregion
	}
}
