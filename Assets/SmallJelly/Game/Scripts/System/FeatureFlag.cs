﻿namespace SmallJelly
{
    public class FeatureFlag 
    {
        public string Name { get; private set; }
        public bool Status { get; private set; }
        public string Param1 { get; private set; }

        public FeatureFlag(string name, bool status, string param1)
        {
            Name = name;
            Status = status;
            Param1 = param1;
        }    	
    }
}