﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Allows us to turn features on the client on and off.  
    /// Flags are updated as part of PlayerData
    /// </summary>
    public static class FeatureFlags 
    {
        public const string KTPlay = "KTPlay";

        private const string ForceUpdate = "ForceUpdate";
               
        private static List<FeatureFlag> mFlags;

        #region Public Properties
        public static bool Replays = false;

        public static bool Onboarding 
        {
            get
            {
                #if !SJ_DEBUG
                return true;
                #else
                return !GameManager.Get.DebugConfig.DisableOnboarding;
                #endif
            }
        }
		#endregion

		#region Tutorials
        public static bool SkipTutorial1
        {
            get
            {
                #if !SJ_DEBUG
                return false;
                #else
                return GameManager.Get.DebugConfig.SkipTutorial1;
                #endif
            }
        }

        public static bool SkipTutorial2
        {
            get
            {
                #if !SJ_DEBUG
                return false;
                #else
                return GameManager.Get.DebugConfig.SkipTutorial2;
                #endif
            }
        }

        public static bool SkipTutorial3
        {
            get
            {
                #if !SJ_DEBUG
                return false;
                #else
                return GameManager.Get.DebugConfig.SkipTutorial3;
                #endif
            }
        }

        public static bool SkipTutorial4
        {
            get
            {
                #if !SJ_DEBUG
                return false;
                #else
                return GameManager.Get.DebugConfig.SkipTutorial4;
                #endif
            }
        }
        #endregion
                  
        #region Public Methods
        public static bool IsFeatureEnabled(string featureName)
        {
            if(mFlags == null)
            {
                return false;
            }

            FeatureFlag flag = mFlags.Find(x => x.Name == featureName);

            if(flag == null) 
            {
                return false;
            }

            return flag.Status;
        }

        public static void UpdateFlags(List<FeatureFlag> flags)
        {
            mFlags = flags;
        }

        public static bool NeedAppUpdate()
        {
            FeatureFlag updateFlag = GetFeatureFlag(ForceUpdate);

            if(updateFlag == null)
            {
                return false;
            }

            if(!updateFlag.Status)
            {
                // Flag is not turned on
                return false;
            }                

            // We need to check what version of the app we are on.
            GameVersion currentVersion = new GameVersion(Application.version);
            GameVersion desiredVersion = new GameVersion(updateFlag.Param1);

            if(!(currentVersion.IsValid() && desiredVersion.IsValid()))
            {
                return false;
            }

            if(currentVersion.CompareTo(desiredVersion) < 0 )
            {
                // Current version is less tha desired version
                return true;
            }

            // Current version is greater than or equal to desired version
            return false;
        }
        #endregion                        

        private static FeatureFlag GetFeatureFlag(string featureName)
        {
            if(mFlags == null)
            {
                return null;
            }

            return mFlags.Find(x => x.Name == featureName);           
        }
    }
}
