﻿using System;
using System.Collections;
using System.IO;
using SmallJelly.Framework;
using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// Top level manager for Rocket Rumble
    /// </summary>
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        #region Public Events
        public event Action<long> CreditsUpdated;
        public event Action<long> CrystalsUpdated;
        public event Action NewInboxItemAdded;
        public event Action NewInboxItemCleared;
        public event Action NewCardsAdded;
        public event Action NewCardsCleared;
        public event Action NewShopItem;
        public event Action NewShopItemCleared;
        public event Action<PlayerData> PlayerDataUpdated;       

        public event EventHandler ConnectionReestablished;
        public event EventHandler ConnectionFailed;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the game state machine.
        /// </summary>
        public GameStateMachine GameStateMachine { get { return mGameStateMachine; } }

        public PlayerData PlayerData
        {
            get
            {
                SJLogger.AssertCondition(mPlayerData != null, "Trying to use null player data");
                return mPlayerData;
            }
        }

        public RRDebugConfig DebugConfig
        {
            get
            {
                if(mDebugConfig == null)
                {                
                    #if UNITY_EDITOR
                    mDebugConfig = RRDebugConfig.ReadFromFile();

                    // If the read failed, create a new file
                    if(mDebugConfig == null)
                    {
                        mDebugConfig = RRDebugConfig.SaveToFile();
                    }
                    #else
                    mDebugConfig = new RRDebugConfig();                        
                    #endif

                }

                return mDebugConfig;
            }
        }

        public string CurrentShipClass { get; set; }
        public string LastAncientDeckId { get; set; }
        public string LastSmugglerDeckId { get; set; }
        public string LastEnforcerDeckId { get; set; }

        public void SetCurrentDeckId(string shipClass, string deckId)
        {
            switch(shipClass)
            {
                case ShipClass.Ancient:
                    LastAncientDeckId = deckId;
                    break;
                case ShipClass.Smuggler:
                    LastSmugglerDeckId = deckId;
                    break;
                case ShipClass.Enforcer:
                    LastEnforcerDeckId = deckId;
                    break;
                default:
                    SJLogger.LogError("Ship class {0} not recognised", shipClass);
                    break;
            }
        }

        public string GetCurrentDeckId()
        {
            switch(CurrentShipClass)
            {
                case ShipClass.Ancient:
                    return LastAncientDeckId;
                case ShipClass.Smuggler:
                    return LastSmugglerDeckId;
                case ShipClass.Enforcer:
                    return LastEnforcerDeckId;
                default:
                    SJLogger.LogError("Ship class {0} not recognised", CurrentShipClass);
                    return string.Empty;
            }
        }


        #endregion

        #region Member Variables
        private RRDebugConfig mDebugConfig;
        private PlayerData mPlayerData;
        private GameStateMachine mGameStateMachine;

        private BattleManager mBattleManager;
        private ChallengeSetupManager mChallengeSetupManager;
        private ShopManager mShopManager;
        private CollectionManager mCollectionManager;
        private InboxManager mInboxManager;

        private System.Random mRandom = new System.Random();
        private string[] mAIPlayerNames;
        #endregion

        #region MonoBehaviour
        protected override void Awake()
        {
            base.Awake();

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Launch, null, null);

            // Create the GameStateMachine
            GameObject stateMachineRoot = new GameObject( "StateMachine" );
            stateMachineRoot.transform.parent = this.transform;
            mGameStateMachine = stateMachineRoot.AddComponent<GameStateMachine>();

            #if UNITY_IOS
            RefreshiOSQualitySettings();
            #endif

            AddChallengeSetupManager();
        }

        private void Update()
        {
            if(mPlayerData != null)
            {
                mPlayerData.Update();
            }
        }

        private void OnDestroy()
        {
            RemoveChallengeSetupManager();
        }
        #endregion

        #region Public Methods
        public void KTPlayConnectionFailed()
        {
            HandleConnectionFailure();
        }
             
        public string GetRandomName()
        {
            if(mAIPlayerNames == null)
            {
                try
                {
                    TextAsset textAsset = (TextAsset)Resources.Load(FileLocations.AiNames);
                    mAIPlayerNames = textAsset.text.Split('\n');
                }
                catch(Exception ex)
                {
                    Debug.LogError("Failed to load AI names from " + FileLocations.AiNames + " Exception: " + ex.GetType() + " " + ex.Message);
                    return "Bob";
                }
            }

            return mAIPlayerNames[mRandom.Next(0, mAIPlayerNames.Length)];
        }

        #if USERNAME_LOGIN_ALLOWED
        public void ForceLogout()
        {            
            Logout();
            mGameStateMachine.ForceTransitionToAuthenticationState();
        }
        #endif

        /// <summary>
        /// Update the player data with information retrieved from the server
        /// </summary>
        public void UpdatePlayerData(PlayerData playerData)
        {            
            
            // TODO: Move this into PlayerData and store in db
            OptionsManager.Get.SetOptionsOnAuthentication(ClientAuthentication.PlayerAccountDetails.DisplayName);

            mPlayerData = playerData;

            FireCreditsUpdated(mPlayerData.Goods.Credits);
            FireCrystalsUpdated(mPlayerData.Goods.Crystals);

            if(mPlayerData.NewInboxItem)
            {
                FireNewInboxItem();
            }
            else
            {
                FireNewInboxItemCleared();
            }

            if(mPlayerData.NewCards)
            {
                FireNewCardsAdded();
            }
            else
            {
                FireNewCardsCleared();
            }

            if(mPlayerData.FreePackAvailable)
            {
                FireNewShopItem();
            }

            // Always fire the generic event
            FirePlayerDataUpdated(mPlayerData);
        }

        public void UpdateFreePackAvailableOnClient()
        {
            if(mPlayerData == null)
            {
                return;
            }

            mPlayerData.SetFreePackAvailableOnClient();
            FireNewShopItem();
        }

        public void UpdateFreePackClaimedOnClient()
        {
            if(mPlayerData == null)
            {
                return;
            }

            mPlayerData.SetFreePackClaimedOnClient();
            FireNewShopItemCleared();
        }


        public void UpdateNewCardsOnClient()
        {
            if(mPlayerData == null) 
            {
                return;
            } 

            SJLogger.LogMessage(MessageFilter.Gameplay, "Setting new cards  on client = {0}", true);
            mPlayerData.NewCards = true;
                     
            FireNewCardsAdded();                    
        }

        public void UpdateNewInboxItemOnClient(bool isNew)
        {
            if(mPlayerData == null) 
            {
                return;
            } 

            SJLogger.LogMessage(MessageFilter.Gameplay, "Setting new inbox item on client = {0}", isNew);
            mPlayerData.NewInboxItem = isNew;

            if(mPlayerData.NewInboxItem)
            {
                FireNewInboxItem();
            }
            else
            {
                FireNewInboxItemCleared();
            }                
        }

        /// <summary>
        /// Update the player data straight from the reward data
        /// </summary>
        public void UpdateRewardsOnClient(RewardData rewardData)
        {
            if(mPlayerData == null) 
            {
                return;
            }                

            if(rewardData.GetTotalCredits() > 0)
            {
                mPlayerData.AddToCredits(rewardData.GetTotalCredits(), PlayerData.SourceOfResource.Reward);
                FireCreditsUpdated(mPlayerData.Goods.Credits);
            }

            if(rewardData.Crystals > 0)
            {
                mPlayerData.AddToCrystals(rewardData.Crystals, PlayerData.SourceOfResource.Reward);
                FireCrystalsUpdated(mPlayerData.Goods.Crystals);
            }        

			if(rewardData.Rank != default( RankReward ) )
			{
				mPlayerData.RankName = rewardData.Rank.NewRankName;
                mPlayerData.RankNumber = rewardData.Rank.NewRankNumber;
				mPlayerData.TierName = rewardData.Rank.NewTierName;
				mPlayerData.StarsAtThisRank = rewardData.Rank.NewNumStars;
				mPlayerData.TotalStarsAtThisRank = rewardData.Rank.StarsAtThisRank;
			}   
        }

        public void SpendCredits(long amount, PlayerData.SinkOfResource sink)
        {
            if(mPlayerData == null) 
            {
                return;
            }

            mPlayerData.RemoveFromCredits(amount, sink);
            FireCreditsUpdated(mPlayerData.Goods.Credits);
        }

        public void SpendCrystals(long amount, PlayerData.SinkOfResource sink)
        {
            if(mPlayerData == null) 
            {
                return;
            }

            mPlayerData.RemoveFromCrystals(amount, sink);
            FireCrystalsUpdated(mPlayerData.Goods.Crystals);
        }

        public void GainCrystals(long amount, PlayerData.SourceOfResource source) 
        {
            if(mPlayerData == null) 
            {
                return;
            }

            mPlayerData.AddToCrystals(amount, source);
            FireCrystalsUpdated(mPlayerData.Goods.Crystals);
        }
		
        /// <summary>
        /// Request a refresh of the player details from the server
        /// </summary>
        public void RefreshPlayerDetails()
        {
            ClientAuthentication.GetAccountDetails(HandleAccountDetailsSuccess, HandleAccountDetailsError);
        }

        /// <summary>
        /// Update the rewards on the client and also request from server
        /// </summary>
        /// <param name="rewardData">Reward data.</param>
        public void RefreshPlayerDetails(RewardData rewardData)
        {
            UpdateRewardsOnClient(rewardData);

            ClientAuthentication.GetAccountDetails(HandleAccountDetailsSuccess, HandleAccountDetailsError);
        }

        public void ConnectionFailedDuringUsernamEntry()
        {
            HandleConnectionFailure();
        }

        public long GetCreditsCount()
        {
            if(mPlayerData == null) 
            {
                return 0;
            }

            return mPlayerData.GetCredits();
        }

        public void Logout()
        {
            SJLogger.LogMessage(MessageFilter.System, "Logging out");

            mPlayerData = null;

            OptionsManager.Get.ResetOptions();

            #if KT_PLAY
            KTPlayManager.Get.Logout();
            #endif

            ClientAuthentication.Logout();

            // Clear any cached GS auth data
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_AUTHTOKEN_KEY);
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_DEVICEID_KEY);
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_USERID_KEY);

            PlayerPrefs.Save();
        }

        public void ForceQuit()
        {
            Application.Quit();
        }

        public void DoCoroutine(IEnumerator coroutine)
        {
            StartCoroutine( Perform( coroutine ) );
        }


		/// <summary>
		/// Cleanup assets not currently used by the game at the end of the frame (EOF to make sure they've all been removed from
		/// the heirarchy at this point.
		/// </summary>
		public void CleanupUnusedAssets()
		{
			DoCoroutine( Cleanup() );
		}

        public BattleManager AddBattleManager( RRChallenge challenge )
        {
            BattleManager battleManagerResource = Resources.Load< BattleManager >( FileLocations.BattlePrefab );
            mBattleManager = Instantiate( battleManagerResource );
            mBattleManager.Initialise( challenge );

            mBattleManager.TutorialConnectionError += HandleConnectionFailure;

            return mBattleManager;
        }

        public void RemoveBattleManager()
        {
            mBattleManager.TutorialConnectionError -= HandleConnectionFailure;

            Destroy( mBattleManager.gameObject );
        }


        public ChallengeSetupManager AddChallengeSetupManager()
        {
            GameObject challengeSetup = new GameObject( "ChallengeSetup" );
            mChallengeSetupManager = challengeSetup.AddComponent< ChallengeSetupManager >();

            mChallengeSetupManager.ConnectionError += HandleConnectionFailure;

            return mChallengeSetupManager;
        }

        public void RemoveChallengeSetupManager()
        {
            if(mChallengeSetupManager == null)
            {
                return;
            }

            mChallengeSetupManager.ConnectionError -= HandleConnectionFailure;

            Destroy( mChallengeSetupManager.gameObject );
        }


        public InboxManager AddInboxManager()
        {
            GameObject inboxManager = new GameObject( "InboxManager" );
            mInboxManager = inboxManager.AddComponent< InboxManager >();

            mInboxManager.InboxConnectionFailed += HandleConnectionFailure;

            return mInboxManager;
        }

        public void RemoveInboxManager()
        {
            mInboxManager.InboxConnectionFailed -= HandleConnectionFailure;

            Destroy( mInboxManager.gameObject );
        }


        public ShopManager AddShopManager()
        {
            GameObject shopManagerGameObject = new GameObject( "ShopManager" );
            mShopManager = shopManagerGameObject.AddComponent< ShopManager >();

            mShopManager.ShopConnectionFailed += HandleConnectionFailure;

            return mShopManager;
        }

        public void RemoveShopManager()
        {
            mShopManager.ShopConnectionFailed -= HandleConnectionFailure;

            Destroy( mShopManager.gameObject );
        }


        public CollectionManager AddCollectionManager()
        {
            GameObject collectionManagerGameObject = new GameObject( "CollectionManager" );
            mCollectionManager = collectionManagerGameObject.AddComponent< CollectionManager >();

            mCollectionManager.CollectionConnectionFailed += HandleConnectionFailure;

            return mCollectionManager;
        }

        public void RemoveCollectionManager()
        {
            mCollectionManager.CollectionConnectionFailed -= HandleConnectionFailure;

            Destroy( mCollectionManager.gameObject );
        }

        public void Ping()
        {
            ClientPing.Ping( HandlePingSuccess, HandlePingFailure ); 
        }
        #endregion

        #region Private Methods
        private void RefreshiOSQualitySettings()
        {
            #if UNITY_IOS
            switch( UnityEngine.iOS.Device.generation )
            {
            case UnityEngine.iOS.DeviceGeneration.iPad1Gen:
            case UnityEngine.iOS.DeviceGeneration.iPad2Gen:
            case UnityEngine.iOS.DeviceGeneration.iPad3Gen:  
            case UnityEngine.iOS.DeviceGeneration.iPadMini1Gen:
            case UnityEngine.iOS.DeviceGeneration.iPhone4:
            case UnityEngine.iOS.DeviceGeneration.iPhone4S:  
            QualitySettings.SetQualityLevel(6); // Simple
            break;

            default:
            // Shadows disabled as bug where ships dissapear
            QualitySettings.SetQualityLevel( 7 ); // A7
            break;
            }
            #endif
        }

        private IEnumerator Perform(IEnumerator coroutine)
        {
            yield return StartCoroutine( coroutine );
        }

		private IEnumerator Cleanup()
		{
			yield return new WaitForEndOfFrame();
			Resources.UnloadUnusedAssets();
		}
        #endregion

        #region Event Firing
        private void FireNewShopItem()
        {
            if(NewShopItem != null)
            {
                NewShopItem();
            }
        }

        private void FireNewShopItemCleared()
        {
            if(NewShopItemCleared != null)
            {
                NewShopItemCleared();
            }
        }

        private void FirePlayerDataUpdated(PlayerData playerData)
        {
            if(PlayerDataUpdated != null)
            {
                PlayerDataUpdated(playerData);
            }
        }

        private void FireNewCardsAdded()
        {
            if(NewCardsAdded != null)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "Fire NewCardsAdded");
                NewCardsAdded();
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to NewCardsAdded");
            }
        }

        private void FireNewCardsCleared()
        {
            if(NewCardsCleared != null)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "Fire NewCardsCleared");
                NewCardsCleared();
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to NewCardsCleared");
            }
        }

        private void FireNewInboxItem()
        {
            if(NewInboxItemAdded != null)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "FireNewInboxItem");
                NewInboxItemAdded();
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to NewInboxItem");
            }
        }

        private void FireNewInboxItemCleared()
        {
            if(NewInboxItemCleared != null)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "FireNewInboxItemCleared");
                NewInboxItemCleared();
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to NewInboxItemCleared");
            }
        }
            
        private void FireCreditsUpdated(long value)
        {
            if(CreditsUpdated != null)
            {
                CreditsUpdated(value);
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to FireCreditsUpdated: {0}", value);
            }
        }

        private void FireCrystalsUpdated(long value)
        {
            if(CrystalsUpdated != null)
            {
                CrystalsUpdated(value);
            }
            else 
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "No one listening to FireCrystalsUpdated: {0}", value);
            }
        }

        private void FireConnectionReestablished()
        {
            if( ConnectionReestablished != null )
            {
                ConnectionReestablished( this, EventArgs.Empty );
            }
        }

        private void FireConnectionFailed()
        {
            if( ConnectionFailed != null )
            {
                ConnectionFailed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void HandlePingSuccess()
        {
            FireConnectionReestablished();
        }

        private void HandlePingFailure( ServerError error )
        {
            SJLogger.AssertCondition( error.ErrorType == ServerErrorType.Timeout, "Error : The only error message possible from a ping should be a timeout" );
            FireConnectionFailed();
        }

        private void HandleConnectionFailure()
        {
            FireConnectionFailed();
        }

        private void HandleAccountDetailsSuccess(VirtualGoods goods, SJJson scriptData)
        {
            if(scriptData != null) 
            {
                // Store the player data
                UpdatePlayerData(new PlayerData(goods, scriptData));
            }
        }
          
        private void HandleAccountDetailsError(ServerError error)
        {
            // No need to handle this error, just fail silently
            SJLogger.LogWarning("Failed to refresh account details");
        }
        #endregion
    }
}