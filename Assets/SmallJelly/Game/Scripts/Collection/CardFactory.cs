﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

namespace SmallJelly
{
	public class CardFactory
	{
		#region Constants
		private const string WEAPON_CARD_PREFAB = "Prefabs/Cards/card_battleEntity";

		private const string SHIP_CARD_PREFAB = "Prefabs/Cards/card_ship";
		private const string RESOURCE_CARD_PREFAB = "Prefabs/Cards/card_resource";
		private const string CREDITS_CARD_PREFAB = "Prefabs/Cards/card_currency";
		#endregion

		#region Enums
		public enum CardType
		{
			BattleEntity,
			Ship,
			Currency,
			Resource
		}

		public enum CardRarity
		{
			Default
		}
		#endregion

		#region Static Variables
		private static Dictionary< CardType, GameObject > mResourcePool;
		#endregion

		#region Protected Methods
		protected static GameObject LoadCardView( CardType cardType )
		{
			if( mResourcePool == null )
			{
				mResourcePool = GetResourcePool();
			}
				
			return UnityEngine.Object.Instantiate( mResourcePool[ cardType ], Vector3.zero, Quaternion.identity ) as GameObject;
		}
		#endregion

		#region Private Methods
		private static Dictionary< CardType, GameObject > GetResourcePool()
		{
			Dictionary< CardType, GameObject > resourcePool = new Dictionary< CardType, GameObject >();

			resourcePool.Add( CardType.BattleEntity, Resources.Load< GameObject >( WEAPON_CARD_PREFAB ) );
			resourcePool.Add( CardType.Currency, Resources.Load< GameObject >( CREDITS_CARD_PREFAB ) );
			resourcePool.Add( CardType.Resource, Resources.Load< GameObject >( RESOURCE_CARD_PREFAB ) );
			resourcePool.Add( CardType.Ship, Resources.Load< GameObject >( SHIP_CARD_PREFAB ) );

			return resourcePool;
		}
		#endregion
	}
}
