﻿using UnityEngine;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// Manages a player's collection
    /// </summary>
    public class CollectionManager : MonoBehaviourSingleton< CollectionManager >
    {
		#region Constants
		private const float EPSILON = 0.01f;

        public enum Direction
        {
            NotSet,
            Left,
            Right
        }
		#endregion

		#region Public Event Handlers
		public event Action CollectionConnectionFailed;
		public event EventHandler CollectionError;
		public event EventHandler< CollectionEventDataEventArgs > CollectionEventDataReceived;
		#endregion

		#region Public Properties
		public CollectionMenuController CollectionMenuController
		{
			get
			{
				return mCollectionMenuController;
			}
		}
		#endregion

		#region Member Variables
		private CollectionMenuController mCollectionMenuController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake ();

			mCollectionMenuController = UIManager.Get.GetMenuController< CollectionMenuController >();
			mCollectionMenuController.OnConstruct( GetCollectionDisplayConfig() );
		}
		#endregion

		#region Public Methods
		public void NewDeck( string deckName, string shipClass, List< string > cards )
		{
			ClientDeck.NewDeck( deckName, shipClass, cards, HandleNewDeckSuccess, HandleServerError );
		}

		public void UpdateDeck( string deckId, List< string > cards, string displayName )
		{
            ClientDeck.UpdateDeck( deckId, cards, displayName, HandleNewDeckSuccess, HandleServerError );
		}

		public void ListDecks()
		{
			ClientDeck.ListDecks( HandleListDecksSuccess, HandleServerError );
		}

		public void GetDeck( string deckName )
		{
			ClientDeck.GetDeck( deckName, HandleGetDeckSuccess, HandleServerError );
		}
			
        public void GetCards( CollectionManager.Direction dir, string shipClass, string componentType, int powerCost, int numberOfElementsPerPage, int pageNumber )
		{
            BattleCardFilter filter = new BattleCardFilter();
            filter.ShipClass = shipClass;
			filter.ComponentType = componentType;
			filter.Power = powerCost;

            ClientCollection.GetCards( dir, filter, numberOfElementsPerPage, pageNumber, HandleGetCardsSuccess, HandleServerError );
		}
		#endregion

		#region Static Methods
		public static CollectionDisplayConfig GetCollectionDisplayConfig()
		{
			float ratio = (float)Screen.width / (float)Screen.height;

			//Handle 16:9 and wider ratios
			if( ratio >= (16.0f / 9.0f) - EPSILON )
			{
				return XmlSerialization.FromResources< CollectionDisplayConfig >( FileLocations.SixteenByNineCollectionConfig );
			}

			//Fallback for other ratios
			return XmlSerialization.FromResources< CollectionDisplayConfig >( FileLocations.FourByThreeCollectionConfig );
		}

        public static CollectionConfig GetDebugCollectionConfig()
        {
            CollectionConfig deckConfig = XmlSerialization.FromResources<CollectionConfig>(FileLocations.DebugCollectionResource);

            if(deckConfig == null) 
            {
                deckConfig = CreateNewCollectionConfig();
            }

            return deckConfig;
        }

        public static CollectionConfig CreateNewCollectionConfig()
        {
            CollectionConfig config = new CollectionConfig();
            config.AllCards = true;

            XmlSerialization.ToFile<CollectionConfig>(config, FileLocations.DebugCollectionFile);

            return config;
        }
		#endregion

		#region Event Firing
		private void FireCollectionEventDataReceived( CollectionEventData collectionEventData )
		{
			if( CollectionEventDataReceived != null )
			{
				CollectionEventDataReceived( this, new CollectionEventDataEventArgs( collectionEventData ) );
			}
		}

		private void FireCollectionError()
		{
			if( CollectionError != null )
			{
				CollectionError( this, EventArgs.Empty );
			}
		}

		private void FireCollectionConnectionFailed()
		{
			if( CollectionConnectionFailed != null )
			{
				CollectionConnectionFailed();
			}
		}
		#endregion

		#region Event Handlers
		private void HandleListDecksSuccess( List< Deck > decks )
		{	
			FireCollectionEventDataReceived( new GetDecksCollectionEventData( decks.ToArray() ) );
		}

		private void HandleNewDeckSuccess()
		{	
            AnalyticsManager.Get.RecordNewDeck();

			FireCollectionEventDataReceived( new NewDeckCollectionEventData() );
		}

		private void HandleGetDeckSuccess( Deck deck )
		{
			FireCollectionEventDataReceived( new GetDeckCollectionEventData( deck ) );
		}

        private void HandleGetCardsSuccess( CollectionQueryResult result )
		{	
			FireCollectionEventDataReceived( new GetCardsCollectionEventData(result) );
		}

		private void HandleServerError( ServerError error )
		{
			switch( error.ErrorType)
			{
				case ServerErrorType.Timeout:
					FireCollectionConnectionFailed();
					break;
                default:
					string errorMessage = string.Format( "{0}", error.UserFacingMessage );
					ButtonData buttonData = new ButtonData( "Ok", FireCollectionError );
					DialogManager.Get.ShowMessagePopup( "Cards Error", errorMessage, buttonData );
                    break;
			}
		}
		#endregion
    }
}