﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class DeckContentsUIButton : UIButton
	{
		#region Public Properties
		public int Power
		{
			get
			{
				return int.Parse( mPowerText.text );
			}

			set
			{
				mPowerText.text = string.Format( "{0}", value );
			}
		}

		public int NumberOfCopies
		{
			get
			{
				return int.Parse( mPowerText.text );
			}

			set
			{
				mNumberOfCopiesText.text = string.Format( "x{0}", value );

				//Only show the amount text where the value is greater than 1
				if( value > 1 ) 
				{ 
					mNumberOfCopiesText.gameObject.SetActive( true );
				} 
				else
				{
					mNumberOfCopiesText.gameObject.SetActive( false );
				}
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private TextMeshProUGUI mPowerText;


		[SerializeField]
		private TextMeshProUGUI mNumberOfCopiesText;
		#endregion
	}
}