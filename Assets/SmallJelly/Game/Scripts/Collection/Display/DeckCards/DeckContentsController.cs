﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;
using TMPro;

namespace SmallJelly
{
	//TODO - Id like to take a second look at this class the next time I need to expand it to split it out into two classes
	//because it's getting a little obtuse
	//I'm just re-emphasising this. Some of this should be moved to "DeckContents.cs" and some to "DeckContentsView.cs".

	/// <summary>
	/// Menu class which controls the display and selection of the decks contents in the side panel in the collections - selecting
	/// removes the card from the deck. 
	/// </summary>
	public class DeckContentsController : MenuController
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			Downloading,
			Downloaded,

			Idle,
			MoveIn,
			MoveOut
		}
		#endregion

		#region Event Handlers
		public event EventHandler AnimationFinished;

		public event EventHandler CompletePressed;

		public event EventHandler DeckUpdated;
		public event EventHandler < DeckContentsStateTransitionEventArgs > StateUpdated;
		#endregion

		#region Public Properties
		public Deck Deck
		{
			get
			{
				return mDeck;
			}
		}
		#endregion

		#region Constants
		private const string COMPLETE_BUTTON_TEXT = "Complete";
		private const string BACK_BUTTON_TEXT = "Back";
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private DeckContentsView mDeckSelectedCardsCollectionDisplayView;

		[SerializeField]
		private TMP_InputField mDeckName;

		[SerializeField]
		private Transform mButtonParent;

		[SerializeField]
		private Button mCompleteButton;

		[SerializeField]
		private TextMeshProUGUI mCompleteButtonText;

		[SerializeField]
		private TextMeshProUGUI mCardCounter;


		[SerializeField]
		private Image mDeckImage;

		[SerializeField]
		private Sprite mAncientDeckSprite;

		[SerializeField]
		private Sprite mSmugglerDeckSprite;

		[SerializeField]
		private Sprite mEnforcerDeckSprite;
		#endregion

		#region Member Variables
		private State mCurrentState;
		private List< UIButton > mContentButtons;

		private Deck mDeck;
		#endregion


		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();

			mDeck = new Deck();
			mDeck.BattleEntities = new List<BattleEntityData>();

			mContentButtons = new List< UIButton >();

			RefreshCardCounter();
			RefreshCompleteButton();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();

			//Clear the deck
			Clear();
		}
		#endregion

		#region Public Methods

		public void SetId( string deckId )
		{
			mDeck.DeckId = deckId;
		}

		public void SetDisplayName( string displayName )
		{
			mDeck.DisplayName = displayName;
			mDeckName.text = displayName;
		}

		public void SetClass( string shipClass )
		{
			mDeck.Class = shipClass;

			switch( mDeck.Class )
			{
				case ShipClass.Ancient:
					mDeckImage.sprite = mAncientDeckSprite;
					break;

				case ShipClass.Smuggler:
					mDeckImage.sprite = mSmugglerDeckSprite;
					break;

				case ShipClass.Enforcer:
					mDeckImage.sprite = mEnforcerDeckSprite;
					break;

				default:
					SJLogger.Assert( "No deck image was found for the class {0}", mDeck.Class );
					break;
			}

		}

		//Add an element to the deck
		public bool AddCard( BattleEntityData battleEntityData )
		{
			//Add it to their deck
			mDeck.BattleEntities.Add( battleEntityData );

			Refresh();

			return true;
		}

		//Remove an element from the deck
		public bool RemoveCard( BattleEntityData battleEntityData )
		{
			mDeck.BattleEntities.Remove( battleEntityData );

			Refresh();

			return true;
		}

		public void Clear()
		{
			//Clear the deck
			Deck.DeckId = string.Empty;
			Deck.DisplayName = string.Empty;
			Deck.Class = string.Empty;

			while( mDeck.BattleEntities.Count > 0 )
			{
				RemoveCard( mDeck.BattleEntities[ 0 ] );
			}
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mDeckSelectedCardsCollectionDisplayView.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( mCurrentState, state );

			mCurrentState = state;
		}
		#endregion

		#region Private Methods
		private void Refresh()
		{
			//Refresh the deck contents display
			RefreshContentButtons();

			//Refresh the card counter
			RefreshCardCounter();

			//Refresh the complete button
			RefreshCompleteButton();

			//Notify that the deck has been updated
			FireDeckUpdated( );
		}

		private void RefreshContentButtons()
		{
			//Remove all existing buttons
			while( mContentButtons.Count > 0 )
			{
				RemoveButton( mContentButtons[ 0 ] );
			}

			//Sort Deck Battle entities by cost and then display name
			mDeck.BattleEntities = mDeck.BattleEntities.OrderBy( x => x.Cost ).ThenBy( x => x.DisplayName ).ToList();

			//Add buttons associated with the supplied names
			for( int i = 0; i < mDeck.BattleEntities.Count; i++ )
			{
				int numberOfCopies = 1;

				//Count the number of copies
				while( i + 1 < mDeck.BattleEntities.Count && mDeck.BattleEntities[ i ].Id == mDeck.BattleEntities[ i + 1 ].Id )
				{
					numberOfCopies++;
					i++;
				}
				BattleEntityData battleEntityData = mDeck.BattleEntities[ i ];
				UIButton uiButton = UIButtonFactory.ConstructDeckContentsButton( battleEntityData.DisplayName, battleEntityData.Cost, numberOfCopies );

				AddButton( uiButton );
			}
		}

		private void RefreshCardCounter()
		{
			int currentCardCount = mDeck.BattleEntities.Count;
			mCardCounter.text = string.Format( "{0}/{1}", currentCardCount, CollectionConstants.MAXIMUM_CARDS );
		}

		private void RefreshCompleteButton()
		{
			int currentCardCount = mDeck.BattleEntities.Count;
			mCompleteButtonText.text = currentCardCount < CollectionConstants.MAXIMUM_CARDS ? BACK_BUTTON_TEXT : COMPLETE_BUTTON_TEXT;
		}

		private void AddButton( UIButton uiButton )
		{
			//Add button as the child of this object
			SJRenderTransformExtensions.AddChild( mButtonParent, uiButton.transform, false, false, true );

			RegisterListeners( uiButton );
			mContentButtons.Add( uiButton );
		}

		private void RemoveButton( UIButton uiButton ) 
		{
			mContentButtons.Remove( uiButton );

			UnregisterListeners( uiButton );

			//Destroy the button
			Destroy( uiButton.gameObject );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( UIButton uiButton )
		{
			uiButton.Clicked += HandleUIButtonClicked;
		}

		private void UnregisterListeners( UIButton uiButton )
		{
			uiButton.Clicked -= HandleUIButtonClicked;
		}

		private void RegisterListeners()
		{
			mDeckName.onValueChanged.AddListener( HandleDeckNameChanged );
			mCompleteButton.onClick.AddListener( HandleCompleteButtonClicked );
			mDeckSelectedCardsCollectionDisplayView.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mDeckName.onValueChanged.RemoveListener( HandleDeckNameChanged );
			mCompleteButton.onClick.RemoveListener( HandleCompleteButtonClicked );
			mDeckSelectedCardsCollectionDisplayView.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}

		private void FireDeckUpdated()
		{
			if( DeckUpdated != null )
			{
				DeckUpdated( this, EventArgs.Empty );
			}
		}

		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateUpdated != null )
			{
				StateUpdated( this, new DeckContentsStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}

		private void FireCompletePressed( )
		{
			if( CompletePressed != null )
			{
				CompletePressed( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			FireAnimationFinished();
		}

		private void HandleDeckNameChanged( string displayName )
		{
			Deck.DisplayName = displayName;
		}

		private void HandleUIButtonClicked( object o, GameObjectEventArgs gameObjectEventArgs )
		{ 
			//Remove the supplied card from the deck
			string cardname = gameObjectEventArgs.GameObject.GetComponent< UIButton >().Text;
			BattleEntityData cardToRemove = mDeck.BattleEntities.First( x => x.DisplayName == cardname );
			RemoveCard( cardToRemove );

			FireDeckUpdated( );
		}

		private void HandleCompleteButtonClicked()
		{
			FireCompletePressed();
		}
		#endregion
	}
}