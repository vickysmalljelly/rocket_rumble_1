﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of the deck contents panel (see DeckContentsController.cs)
	/// </summary>
	public class DeckContentsView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mDeckSelectedCardsCollectionDisplayAnimationController.AnimationFinished += value;
			}

			remove
			{
				mDeckSelectedCardsCollectionDisplayAnimationController.AnimationFinished += value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private DeckContentsAnimationController mDeckSelectedCardsCollectionDisplayAnimationController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();		
		}
		#endregion

		#region Methods
		public void OnPlayAnimation( DeckContentsController.State state, AnimationMetaData animationMetaData )
		{
			mDeckSelectedCardsCollectionDisplayAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion
	}
}