﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	/// <summary>
	/// Controls the animations of the deck contents panel (see DeckContentsController.cs)
	/// </summary>
	public class DeckContentsAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/DeckSelectedCardsDisplay/"; } }
		#endregion

		#region Constants
		private const string ENTERING_ANIMATION_TEMPLATE_NAME = "EnteringAnimation";
		private const string EXITING_ANIMATION_TEMPLATE_NAME = "ExitingAnimation";

		private const string DOWNLOADING_ANIMATION_TEMPLATE_NAME = "DownloadingAnimation";
		private const string DOWNLOADED_ANIMATION_TEMPLATE_NAME = "DownloadedAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( DeckContentsController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(state)
			{
				case DeckContentsController.State.Downloading:
					PlayAnimation( DOWNLOADING_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckContentsController.State.Downloaded:
					PlayAnimation( DOWNLOADED_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckContentsController.State.MoveIn:
					PlayAnimation( ENTERING_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckContentsController.State.MoveOut:
					PlayAnimation( EXITING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
