﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class PageData
	{
		#region Public Properties
		public CollectionItem[] CollectionEntities 
		{ 
			get 
			{ 
				return mCollectionEntity; 
			} 
		}

		public ObjectDistributor ObjectDistributor
		{ 
			get 
			{ 
				return mObjectDistributor; 
			} 
		}

		public GameObject Parent
		{ 
			get 
			{ 
				return mParent; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly CollectionItem[] mCollectionEntity;
		private readonly ObjectDistributor mObjectDistributor;
		private readonly GameObject mParent;
		#endregion

		#region Constructor
		public PageData( CollectionItem[] collectionEntity, ObjectDistributor objectDistributor, GameObject parent )
		{
			mCollectionEntity = collectionEntity;
			mObjectDistributor = objectDistributor;
			mParent = parent;
		}
		#endregion
	}
}