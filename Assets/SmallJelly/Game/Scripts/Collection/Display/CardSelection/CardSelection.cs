﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	/// <summary>
	/// Menu class which controls the selection of cards in the collections (whether that be to view, add them to the players deck
	/// or whatever else)
	/// </summary>
	public class CardSelection : MenuController
	{

		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mCardSelectionController.AnimationFinished += value;
			}

			remove
			{
				mCardSelectionController.AnimationFinished -= value;
			}
		}

		public event EventHandler< CardSelectionStateTransitionEventArgs > StateUpdated
		{
			add
			{
				mCardSelectionController.StateUpdated += value;
			}

			remove
			{
				mCardSelectionController.StateUpdated -= value;
			}
		}

		public event EventHandler< CollectionItemAndStateTransitionEventArgs > CollectionEntityStateTransitioned
		{
			add
			{
				mCardSelectionController.CollectionEntityStateTransitioned += value;
			}

			remove
			{
				mCardSelectionController.CollectionEntityStateTransitioned -= value;
			}
		}

		public event EventHandler< CollectionFilterEventArgs > RetrievePage;
		#endregion

		#region Public Properties
		public CollectionBattleEntityData[] Data
		{
			get
			{
				return mData;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private CardSelectionController mCardSelectionController;
		#endregion

		#region Member Variables
		private CollectionDisplayConfig mCollectionDisplayConfig;
		private CollectionBattleEntityData[] mData;

        // The position of the page relative to previously viewed pages
        private int mCurrentPagePosition;
        private bool mCurrentPagePositionInitialised;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		//Called when the object is created - used to construct the object with the
		//supplied configuration
		public void OnConstruct( CollectionDisplayConfig collectionDisplayConfig )
		{
			mCollectionDisplayConfig = collectionDisplayConfig;

			mCardSelectionController.OnConstruct( collectionDisplayConfig );
		}

        public void RefreshPage( CollectionManager.Direction refreshDirection, int pageIndex, int numberOfPages, CollectionBattleEntityData[] collectionBattleEntityData )
		{
			mData = collectionBattleEntityData;

            SetCurrentPagePosition(refreshDirection);

            mCardSelectionController.RefreshPage( mCurrentPagePosition, numberOfPages, collectionBattleEntityData );

			GameManager.Get.DoCoroutine( TransitionPageAtEndOfFrame() );
		}

        private void SetCurrentPagePosition(CollectionManager.Direction direction)
        {
            if(!mCurrentPagePositionInitialised)
            {
                mCurrentPagePosition = 0;
                mCurrentPagePositionInitialised = true;
                return;
            }

            // Update current page position
            switch(direction)
            {
                case CollectionManager.Direction.Left:
                    mCurrentPagePosition--;
                    break;
                case CollectionManager.Direction.Right:
                    mCurrentPagePosition++;
                    break;
                default:
                    SJLogger.LogError("refreshDirection [{0}] not set ", direction);
                    break;                   
            }
        }

		private IEnumerator TransitionPageAtEndOfFrame()
		{
			yield return new WaitForEndOfFrame();

            Vector3 pageOffset = mCollectionDisplayConfig.CardPageOffset * -mCurrentPagePosition;

			//Animate the page
			Dictionary< string, NamedVariable > data = new Dictionary<string, NamedVariable>();
			data.Add( "Offset", new FsmVector3( pageOffset ) );

			AnimationMetaData animationMetaData = new AnimationMetaData( data );

			mCardSelectionController.OnInsertState( CardSelectionController.State.Downloaded );
			mCardSelectionController.OnInsertState( CardSelectionController.State.Moving, animationMetaData );
		}
		#endregion
	}
}