﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	/// <summary>
	/// Controls the state changes of Card Selection (see CardSelection.cs)
	/// </summary>
	public class CardSelectionController : SJMonoBehaviour
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,
			Downloading,
			Downloaded,
			Moving
		}
		#endregion

		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mCardSelectionView.AnimationFinished += value;
			}

			remove
			{
				mCardSelectionView.AnimationFinished -= value;
			}
		}

		public event EventHandler< CollectionItemAndStateTransitionEventArgs > CollectionEntityStateTransitioned
		{
			add
			{
				mCardSelectionView.CollectionEntityStateTransitioned += value;
			}

			remove
			{
				mCardSelectionView.CollectionEntityStateTransitioned -= value;
			}
		}

		public event EventHandler< CardSelectionStateTransitionEventArgs > StateUpdated;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private CardSelectionView mCardSelectionView;
		#endregion

		#region Member Variables
		private State mCurrentState;
		#endregion

		#region Public Methods
		public void OnConstruct( CollectionDisplayConfig collectionDisplayConfig )
		{
			mCardSelectionView.OnConstruct( collectionDisplayConfig );
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mCardSelectionView.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( mCurrentState, state );

			mCurrentState = state;
		}

		public void RefreshPage( int pageIndex, int numberOfPages, CollectionBattleEntityData[] collectionBattleEntityData )
		{
			mCardSelectionView.RefreshPage( pageIndex, numberOfPages, collectionBattleEntityData );
		}
		#endregion

		#region Event Firing
		protected void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateUpdated != null )
			{
				StateUpdated( this, new CardSelectionStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion
	}
}