﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using TMPro;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of Card Selection (see CardSelection.cs)
	/// </summary>
	public class CardSelectionView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mCardSelectionCollectionDisplayAnimationController.AnimationFinished += value;
			}

			remove
			{
				mCardSelectionCollectionDisplayAnimationController.AnimationFinished -= value;
			}
		}

		public event EventHandler< CollectionItemAndStateTransitionEventArgs > CollectionEntityStateTransitioned;
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private Transform mPagesParent;

		//Prefab containing a message 
		[SerializeField]
		private GameObject mNoCardsPrefab;

		[SerializeField]
		private CardSelectionAnimationController mCardSelectionCollectionDisplayAnimationController;
		#endregion

		#region Member Variables
		// TODO: Cache loaded pages
        private PageData mPageData;

		private CollectionDisplayConfig mCollectionDisplayConfig;
		#endregion

		#region Methods
		public void OnConstruct( CollectionDisplayConfig collectionDisplayConfig )
		{
			mCollectionDisplayConfig = collectionDisplayConfig;
		}

		public void OnPlayAnimation( CardSelectionController.State state, AnimationMetaData animationMetaData )
		{
			mCardSelectionCollectionDisplayAnimationController.UpdateAnimations( state, animationMetaData );
		}


		public void RefreshPage( int pageIndex, int numberOfPages, CollectionBattleEntityData[] collectionBattleEntityData )
		{
            RemovePage();

			AddPage( pageIndex, collectionBattleEntityData );

            CollectionItem[] pageElements = mPageData.CollectionEntities;

			//Rotate the page elements
			foreach( CollectionItem collectionEntity in pageElements )
			{
				collectionEntity.OnInsertState(  CollectionItemController.State.Reveal );
			}
		}

        public void AddPage( int pageIndex, CollectionBattleEntityData[] collectionBattleEntityData )
        {
            //Add new page in the index
            GameObject pageGameObject = new GameObject("Page");
            SJRenderTransformExtensions.AddChild( mPagesParent, pageGameObject.transform, false, false, false );
            Camera targetCamera = SJRenderTransformExtensions.GetCamera( pageGameObject.layer );
            pageGameObject.transform.localPosition = mCollectionDisplayConfig.CardPageOffset * pageIndex;

            ObjectDistributor objectDistributor = new ObjectDistributor( new FaceGameObjectGridDistributionShape(Vector3.zero, targetCamera.transform.position, mCollectionDisplayConfig.NumberOfElementsPerRow, mCollectionDisplayConfig.CardPageStartPosition, mCollectionDisplayConfig.CardColumnOffset, mCollectionDisplayConfig.CardRowOffset ), 0.0f );

            List< CollectionItem > pageElementsList = new List<CollectionItem>();

            //TODO - Remove this - it's a temporary measure to display a "No cards" message where no cards are found on a specific page
            if( collectionBattleEntityData.Length == 0 )
            {
                GameObject noCardsGameObject = Instantiate( mNoCardsPrefab );
                SJRenderTransformExtensions.AddChild( pageGameObject.transform, noCardsGameObject.transform, true, true, false );

            }

            //Create the collection entities
            for( int i = 0; i < collectionBattleEntityData.Length; i++ )
            {
                CollectionItem collectionItem = BattleEntityCollectionItemFactory.ConstructCollectionItem( collectionBattleEntityData[ i ], mCollectionDisplayConfig.NumberOfCopiesTextOffset, mCollectionDisplayConfig.NewTextOffset );
                SJRenderTransformExtensions.AddChild( pageGameObject.transform, collectionItem.transform, false, false, false );

                RegisterListeners( collectionItem );

                //Distribute the cards
                objectDistributor.Insert( new int[]{ i }, new Transform[]{ collectionItem.transform } );
                pageElementsList.Add( collectionItem );
            }

            mPageData = new PageData( pageElementsList.ToArray(), objectDistributor, pageGameObject );
        }
            
        public void RemovePage()
        {
            if(mPageData == null)
            {
                return;
            }

            foreach( CollectionItem collectionEntity in mPageData.CollectionEntities )
            {
                UnregisterListeners( collectionEntity );
            }

            Destroy( mPageData.Parent );
            mPageData = null;
        }
		#endregion

		#region Event Registration
		private void RegisterListeners( CollectionItem collectionEntity )
		{
			collectionEntity.StateTransitioned += HandleStateTransitioned;
		}

		private void UnregisterListeners( CollectionItem collectionEntity )
		{
			collectionEntity.StateTransitioned -= HandleStateTransitioned;
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, CollectionItemAndStateTransitionEventArgs collectionEntityAndStateTransitionEventArgs )
		{
			FireCollectionEntityStateTransitioned( collectionEntityAndStateTransitionEventArgs.CollectionEntity, collectionEntityAndStateTransitionEventArgs.PreviousState, collectionEntityAndStateTransitionEventArgs.NewState, collectionEntityAndStateTransitionEventArgs.EntityHitLayers );
		}
		#endregion

		#region Event Firing
		protected void FireCollectionEntityStateTransitioned( CollectionItem collectionEntity, CollectionItemController.State previousCollectionEntityState, CollectionItemController.State newCollectionEntityState, SJRenderRaycastHit[] sJRenderRaycastHit )
		{
			if( CollectionEntityStateTransitioned != null )
			{
				CollectionEntityStateTransitioned( this, new CollectionItemAndStateTransitionEventArgs( collectionEntity, previousCollectionEntityState, newCollectionEntityState, sJRenderRaycastHit ) );
			}
		}
		#endregion
	}
}