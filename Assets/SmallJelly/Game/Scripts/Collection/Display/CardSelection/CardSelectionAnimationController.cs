﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class CardSelectionAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/CardSelectionDisplay/"; } }
		#endregion

		#region Constants
		private const string MOVING_ANIMATION_TEMPLATE_NAME = "MovingAnimation";

		private const string DOWNLOADING_ANIMATION_TEMPLATE_NAME = "DownloadingAnimation";
		private const string DOWNLOADED_ANIMATION_TEMPLATE_NAME = "DownloadedAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( CardSelectionController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(state)
			{
				case CardSelectionController.State.Downloading:
					PlayAnimation( DOWNLOADING_ANIMATION_TEMPLATE_NAME );
					break;

				case CardSelectionController.State.Downloaded:
					PlayAnimation( DOWNLOADED_ANIMATION_TEMPLATE_NAME );
					break;

				case CardSelectionController.State.Moving:
					PlayAnimation( MOVING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
