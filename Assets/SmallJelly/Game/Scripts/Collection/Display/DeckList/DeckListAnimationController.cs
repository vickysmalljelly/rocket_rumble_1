﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	/// <summary>
	/// Controls the animations of the deck list panel (see DeckListController.cs)
	/// </summary>
	public class DeckListAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/DeckSelectionDisplay/"; } }
		#endregion

		#region Constants
		private const string ENTERING_ANIMATION_TEMPLATE_NAME = "EnteringAnimation";
		private const string EXITING_ANIMATION_TEMPLATE_NAME = "ExitingAnimation";

		private const string DOWNLOADING_ANIMATION_TEMPLATE_NAME = "DownloadingAnimation";
		private const string DOWNLOADED_ANIMATION_TEMPLATE_NAME = "DownloadedAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( DeckListController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(state)
			{
				case DeckListController.State.Downloading:
					PlayAnimation( DOWNLOADING_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckListController.State.Downloaded:
					PlayAnimation( DOWNLOADED_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckListController.State.MoveIn:
					PlayAnimation( ENTERING_ANIMATION_TEMPLATE_NAME );
					break;

				case DeckListController.State.MoveOut:
					PlayAnimation( EXITING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
