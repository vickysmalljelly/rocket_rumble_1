﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	//TODO - Id like to take a second look at this class the next time I need to expand it to split it out into two classes
	//because it's getting a little obtuse

	/// <summary>
	/// Menu class which controls the display and selection of the players list of decks in the collections
	/// </summary>
	public class DeckListController : MenuController
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			Downloading,
			Downloaded,

			Idle,
			MoveIn,
			MoveOut
		}
		#endregion

		#region Event Handlers
		public event EventHandler AnimationFinished;

		public event EventHandler BackPressed;
		public event EventHandler NewDeckPressed;
        public event Action<string, string> EditDeckPressed;

		public event EventHandler< DeckSelectionStateTransitionEventArgs > StateUpdated;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private DeckListView mDeckListView;

		[SerializeField]
		private Transform mButtonParent;

		[SerializeField]
		private Button mNewDeckButton;
		#endregion

		#region Member Variables
		private State mCurrentState;
		private Deck[] mDecks;
		private List< UIButton > mDeckSelectionButtons;
		#endregion


		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();

			mDeckSelectionButtons = new List< UIButton >();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void Refresh( Deck[] decks )
		{
			//Remove all existing buttons
			while( mDeckSelectionButtons.Count > 0 )
			{
				RemoveButton( mDeckSelectionButtons[ 0 ] );
			}

			//Add buttons associated with the supplied names
			foreach( Deck deck in decks )
			{
                UIButton uiButton = UIButtonFactory.ConstructDeckSelectionButton( deck.Class, deck.DeckId, deck.DisplayName );
				AddButton( uiButton );
			}

			mDecks = decks;
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mDeckListView.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( mCurrentState, state );

			mCurrentState = state;
		}
		#endregion

		#region Private Methods
		private void AddButton( UIButton uiButton )
		{
			//Add button as the child of this object
			SJRenderTransformExtensions.AddChild( mButtonParent, uiButton.transform, false, false, true );

			RegisterListeners( uiButton );
			mDeckSelectionButtons.Add( uiButton );
		}

		private void RemoveButton( UIButton uiButton ) 
		{
			mDeckSelectionButtons.Remove( uiButton );
			UnregisterListeners( uiButton );

			//Destroy the button
			Destroy( uiButton.gameObject );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( UIButton uiButton )
		{
			uiButton.Clicked += HandleEditDeckButtonClicked;
		}

		private void UnregisterListeners( UIButton uiButton )
		{
			uiButton.Clicked -= HandleEditDeckButtonClicked;
		}

		private void RegisterListeners()
		{
			mDeckListView.AnimationFinished += HandleAnimationFinished;
			mNewDeckButton.onClick.AddListener( HandleNewDeckButtonClicked );
		}

		private void UnregisterListeners()
		{
			mDeckListView.AnimationFinished -= HandleAnimationFinished;
			mNewDeckButton.onClick.RemoveAllListeners();
		}
		#endregion

		#region Event Firing
        private void FireEditDeckPressed( string deckName, string className )
		{
			if( EditDeckPressed != null )
			{
                EditDeckPressed(deckName, className);
			}
		}

		private void FireNewDeckPressed()
		{
			if( NewDeckPressed != null )
			{
				NewDeckPressed( this, EventArgs.Empty );
			}
		}

		private void FireBackPressed()
		{
			if( BackPressed != null )
			{
				BackPressed( this, EventArgs.Empty );
			}
		}


		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}

		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateUpdated != null )
			{
				StateUpdated( this, new DeckSelectionStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			FireAnimationFinished();
		}

		private void HandleEditDeckButtonClicked( object o, GameObjectEventArgs args )
		{
			UIButton clickedButton = args.GameObject.GetComponent< UIButton >();

			Deck deck = mDecks[ mDeckSelectionButtons.IndexOf( clickedButton ) ];
			CollectionManager.Get.GetDeck( deck.DeckId );
            FireEditDeckPressed( deck.DeckId, deck.Class );
		}

		private void HandleNewDeckButtonClicked()
		{
			FireNewDeckPressed();
		}

		private void HandleBackButtonClicked()
		{
			FireBackPressed();
		}
		#endregion
	}
}