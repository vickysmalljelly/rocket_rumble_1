﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of the deck list panel (see DeckListController.cs)
	/// </summary>
	public class DeckListView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mDeckListAnimationController.AnimationFinished += value;
			}

			remove
			{
				mDeckListAnimationController.AnimationFinished += value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private DeckListAnimationController mDeckListAnimationController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();		
		}
		#endregion

		#region Methods
		public void OnPlayAnimation( DeckListController.State state, AnimationMetaData animationMetaData )
		{
			mDeckListAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion
	}
}