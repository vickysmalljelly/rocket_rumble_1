﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class ClassSelectionView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mClassSelectionCollectionDisplayAnimationController.AnimationFinished += value;
			}

			remove
			{
				mClassSelectionCollectionDisplayAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private ClassSelectionAnimationController mClassSelectionCollectionDisplayAnimationController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();		
		}
		#endregion

		#region Methods
		public void OnPlayAnimation( ClassSelectionController.State state, AnimationMetaData animationMetaData )
		{
			mClassSelectionCollectionDisplayAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion
	}
}