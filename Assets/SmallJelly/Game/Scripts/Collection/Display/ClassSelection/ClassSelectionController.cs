﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	public class ClassSelectionController : MenuController
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			Idle,
			MoveIn,
			MoveOut
		}
		#endregion

		#region Event Handlers
		public event EventHandler EnforcerClicked;
		public event EventHandler SmugglerClicked;
		public event EventHandler AncientClicked;

		public event EventHandler< ClassSelectionStateTransitionEventArgs > StateUpdated;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private Button mEnforcer;

		[SerializeField]
		private Button mSmuggler;

		[SerializeField]
		private Button mAncient;

		[SerializeField]
		private ClassSelectionView mClassSelectionView;
		#endregion

		#region Member Variables
		private State mCurrentState;
		#endregion


		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mClassSelectionView.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( mCurrentState, state );

			mCurrentState = state;
		}
		#endregion

		#region Event Firing
		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateUpdated != null )
			{
				StateUpdated( this, new ClassSelectionStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}

		private void FireEnforcerClicked()
		{
			if( EnforcerClicked != null )
			{
				EnforcerClicked( this, EventArgs.Empty );
			}
		}

		private void FireSmugglerClicked()
		{
			if( SmugglerClicked != null )
			{
				SmugglerClicked( this, EventArgs.Empty );
			}
		}

		private void FireAncientClicked()
		{
			if( AncientClicked != null )
			{
				AncientClicked( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mEnforcer.onClick.AddListener( FireEnforcerClicked );
			mSmuggler.onClick.AddListener( FireSmugglerClicked );
			mAncient.onClick.AddListener( FireAncientClicked );
		}

		private void UnregisterListeners()
		{
			mEnforcer.onClick.RemoveListener( FireEnforcerClicked );
			mSmuggler.onClick.RemoveListener( FireSmugglerClicked );
			mAncient.onClick.RemoveListener( FireAncientClicked );
		}
		#endregion
	}
}