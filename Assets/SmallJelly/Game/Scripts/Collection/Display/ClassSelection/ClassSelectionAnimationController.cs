﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ClassSelectionAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/ClassSelectionDisplay/"; } }
		#endregion

		#region Constants
		private const string ENTERING_ANIMATION_TEMPLATE_NAME = "EnteringAnimation";
		private const string EXITING_ANIMATION_TEMPLATE_NAME = "ExitingAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( ClassSelectionController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(state)
			{

				case ClassSelectionController.State.MoveIn:
					PlayAnimation( ENTERING_ANIMATION_TEMPLATE_NAME );
					break;

				case ClassSelectionController.State.MoveOut:
					PlayAnimation( EXITING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
