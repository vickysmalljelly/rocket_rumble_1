﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class CollectionMenuController : FrontEndMenuController
	{
		#region Public Properties
		public CollectionFilterList CollectionFilterList
		{
			get
			{
				return mCollectionFilterList;
			}
		}

		public CardSelection CardSelection
		{
			get
			{
				return mCardSelection;
			}
		}

		public DeckListController DeckSelectionCollectionDisplayController
		{
			get
			{
				return mDeckSelectionController;
			}
		}

		public DeckContentsController DeckContentsDisplayController
		{
			get
			{
				return mDeckContentsController;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private CollectionFilterList mCollectionFilterList;

		[SerializeField]
		private CardSelection mCardSelection;

		[SerializeField]
		private ClassSelectionController mClassSelectionController;

		[SerializeField]
		private DeckListController mDeckSelectionController;

		[SerializeField]
		private DeckContentsController mDeckContentsController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			UIManager.Get.AddMenu< CollectionFilterList >( mCollectionFilterList.gameObject );
			UIManager.Get.AddMenu< CardSelection >( mCardSelection.gameObject );
			UIManager.Get.AddMenu< ClassSelectionController >( mClassSelectionController.gameObject );
			UIManager.Get.AddMenu< DeckListController >( mDeckSelectionController.gameObject );
			UIManager.Get.AddMenu< DeckContentsController >( mDeckContentsController.gameObject );

			//TODO - Come up with a better solution for this!
			//Show the menu to allow its setup to happen
			UIManager.Get.ShowMenu< CollectionFilterList >();
			UIManager.Get.ShowMenu< CardSelection >( );
			UIManager.Get.ShowMenu< ClassSelectionController >( );
			UIManager.Get.ShowMenu< DeckListController >( );
			UIManager.Get.ShowMenu< DeckContentsController >( );
		}

		private void Start()
		{
			//Hide the menu now it's setup
			UIManager.Get.HideMenu< CollectionFilterList >( );
			UIManager.Get.HideMenu< CardSelection >( );
			UIManager.Get.HideMenu< ClassSelectionController >( );
			UIManager.Get.HideMenu< DeckListController >( );
			UIManager.Get.HideMenu< DeckContentsController >( );
		}

		private void OnDestroy()
		{
			//Prevent erroneously throwing errors when the game is shutting down
			if( UIManager.Get != null )
			{
				UIManager.Get.RemoveMenu< CollectionFilterList >();
				UIManager.Get.RemoveMenu< CardSelection >();
				UIManager.Get.RemoveMenu< ClassSelectionController >();
				UIManager.Get.RemoveMenu< DeckListController >();
				UIManager.Get.RemoveMenu< DeckContentsController >();
			}
		}
		#endregion

		#region Public Methods
		//Called when the object is created - used to construct the object with the
		//supplied configuration
		public void OnConstruct( CollectionDisplayConfig collectionDisplayConfig )
		{
			mCardSelection.OnConstruct( collectionDisplayConfig );
			mCollectionFilterList.OnConstruct( collectionDisplayConfig );
		}
		#endregion

	}
}