﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;


namespace SmallJelly
{
	/// <summary>
	/// Contains information about how we construct the collection display
	/// </summary>
	[Serializable]
	public class CollectionDisplayConfig  
	{
		public int NumberOfElementsPerRow; 
		public int NumberOfElementsPerColumn;

		public Vector3 CardPageStartPosition;
		public Vector3 CardColumnOffset;
		public Vector3 CardRowOffset;
		public Vector3 CardPageOffset;

		public Vector3 NumberOfCopiesTextOffset;
		public Vector3 NewTextOffset;
	}
}