﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	/// <summary>
	/// Controls the state changes of the collection filter list (see CollectionFilterList.cs)
	/// </summary>
	public class CollectionFilterListController : SJMonoBehaviour
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			MoveIn,
			Idle
		}
		#endregion

		#region Event Handlers

		public event EventHandler AnimationFinished
		{
			add
			{
				mCollectionFilterListView.AnimationFinished += value;
			}

			remove
			{
				mCollectionFilterListView.AnimationFinished -= value;
			}
		}


        public event Action<string> ClassFilterSelected
		{
			add
			{
				mCollectionFilterListView.ClassFilterSelected += value;
			}

			remove
			{
				mCollectionFilterListView.ClassFilterSelected -= value;
			}
		}

		public event EventHandler< StringEventArgs > PowerCostFilterSelected
		{
			add
			{
				mCollectionFilterListView.PowerCostFilterSelected += value;
			}

			remove
			{
				mCollectionFilterListView.PowerCostFilterSelected -= value;
			}
		}

		public event EventHandler PageLeft
		{
			add
			{
				mCollectionFilterListView.PageLeft += value;
			}

			remove
			{
				mCollectionFilterListView.PageLeft -= value;
			}
		}

		public event EventHandler PageRight
		{
			add
			{
				mCollectionFilterListView.PageRight += value;
			}

			remove
			{
				mCollectionFilterListView.PageRight -= value;
			}
		}

		public event EventHandler< FilterSelectionStateTransitionEventArgs > StateUpdated;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private CollectionFilterListView mCollectionFilterListView;
		#endregion

		#region Member Variables
		private State mCurrentState;
		#endregion

		#region Public Methods
        public void SetEditingDeck(string className)
        {
            mCollectionFilterListView.SetEditingDeck(className);
        }

        public void ClearEditingDeck()
        {
            mCollectionFilterListView.ClearEditingDeck();
        }

		public void Refresh( CollectionFilterData collectionFilterListData )
		{
			mCollectionFilterListView.Refresh( collectionFilterListData );
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mCollectionFilterListView.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( mCurrentState, state );

			mCurrentState = state;
		}
		#endregion

		#region Event Firing
		protected void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateUpdated != null )
			{
				StateUpdated( this, new FilterSelectionStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion
	}
}