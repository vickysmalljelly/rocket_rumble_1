﻿
namespace SmallJelly
{
	/// <summary>
	/// Struct containing the data relating to the collection filter panel
	/// </summary>
	public struct CollectionFilterData
	{
		public int SelectedPageIndex { get; set; }
		public int NumberOfPages { get; set; }

		public int SelectedShipClassIndex { get; set; }
		public string[] ShipClassOptions { get; set; }

		public int SelectedComponentTypeIndex { get; set; }
		public string[] ComponentTypeOptions { get; set; }

		public int SelectedPowerCostIndex { get; set; }
		public string[] PowerCostOptions { get; set; }

		public int NumberOfElementsPerPage { get; set; }

        // The direction in which the cards should be scrolled with the next update
        public CollectionManager.Direction Direction;
	}
}