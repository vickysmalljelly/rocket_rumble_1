﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of the collection filter list (see CollectionFilterList.cs)
	/// </summary>
	public class CollectionFilterListView : SJMonoBehaviour 
	{
		#region Public Event Handlers

		public event EventHandler AnimationFinished
		{
			add
			{
				//mCollectionFilterListAnimationController.AnimationFinished += value;
			}

			remove
			{
				//mCollectionFilterListAnimationController.AnimationFinished -= value;
			}
		}
      
        public event Action<string> ClassFilterSelected;

		public event EventHandler< StringEventArgs > PowerCostFilterSelected
		{
			add
			{
				mPowerDropdown.OptionSelected += value;
			}

			remove
			{
				mPowerDropdown.OptionSelected -= value;
			}
		}

		public event EventHandler PageLeft;

		public event EventHandler PageRight;
		#endregion

		#region Exposed To The Inspector
		//[SerializeField]
		//private CollectionFilterListAnimationController mCollectionFilterListAnimationController;

        [SerializeField]
        private ToggleGroup mClassToggleGroup;

        [SerializeField]
        private Toggle mClassNeutralToggle;

        [SerializeField]
        private Toggle mClassAncientToggle;

        [SerializeField]
        private Toggle mClassEnforcerToggle;

        [SerializeField]
        private Toggle mClassSmugglerToggle;

		[SerializeField]
		private Button mLeftButton;

		[SerializeField]
		private Button mRightButton;

		//[SerializeField]
		//private ToggleableButtonListController mClassButtonList;

		[SerializeField]
		private DropdownController mPowerDropdown;
		#endregion

		#region Member Variables
		private CollectionDisplayConfig mCollectionDisplayConfig;

        private TextMeshProUGUI mClassNeutralText;
        private TextMeshProUGUI mClassAncientText;
        private TextMeshProUGUI mClassSmugglerText;
        private TextMeshProUGUI mClassEnforcerText;
        private Color mHighlighted;
        private Color mDisabled;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
            SJLogger.AssertCondition(mClassToggleGroup != null, "mClassToggleGroup must be set in the inspector");
            SJLogger.AssertCondition(mClassNeutralToggle != null, "mClassNeutralToggle must be set in the inspector");
            SJLogger.AssertCondition(mClassAncientToggle != null, "mClassAncientToggle must be set in the inspector");
            SJLogger.AssertCondition(mClassEnforcerToggle != null, "mClassEnforcerToggle must be set in the inspector");
            SJLogger.AssertCondition(mClassSmugglerToggle != null, "mClassSmugglerToggle must be set in the inspector");

            mClassNeutralText = mClassNeutralToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassAncientText = mClassAncientToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassSmugglerText = mClassSmugglerToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassEnforcerText = mClassEnforcerToggle.GetComponentsInChildren<TextMeshProUGUI>().First();

            mHighlighted = new Color(1, 1, 1);
            mDisabled = new Color(0.5f, 0.5f, 0.5f);

			RegisterListeners();

            // Force an update of the text colour
            SetClassToggleTextColour("Neutral");
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void Refresh( CollectionFilterData collectionFilterListData )
		{
			RefreshDropdowns( collectionFilterListData );
            SetActiveClassTab(collectionFilterListData.ShipClassOptions[collectionFilterListData.SelectedShipClassIndex]);
		}

		public void OnPlayAnimation( CollectionFilterListController.State state, AnimationMetaData animationMetaData )
		{
            
			//mCollectionFilterListAnimationController.UpdateAnimations( state, animationMetaData );
		}

        public void SetEditingDeck(string className)
        {
            switch(className)
            {
                case "Smuggler":
                    mClassEnforcerToggle.interactable = false;
                    mClassAncientToggle.interactable = false;
                    break;
                case "Ancient":
                    mClassEnforcerToggle.interactable = false;
                    mClassSmugglerToggle.interactable = false;
                    break;
                case "Enforcer":
                    mClassSmugglerToggle.interactable = false;
                    mClassAncientToggle.interactable = false;
                    break;
                default:
                    SJLogger.Assert("className [{0}] not recognised", className);
                    break;                    
            }
        }

        public void ClearEditingDeck()
        {
            mClassSmugglerToggle.interactable = true;
            mClassAncientToggle.interactable = true;
            mClassEnforcerToggle.interactable = true;
        }
		#endregion

		#region Private Methods
        private void SetActiveClassTab(string className)
        {
            switch(className)
            {
                case "Neutral":
                    mClassNeutralToggle.isOn = true;
                    break;
                case "Ancient":
                    mClassAncientToggle.isOn = true;
                    break;
                case "Enforcer":
                    mClassEnforcerToggle.isOn = true;
                    break;
                case "Smuggler":
                    mClassSmugglerToggle.isOn = true;
                    break;
                default:
                    SJLogger.Assert("className ({0})not recognised", className);
                    break;
            }         
        }

		private void SetDropdown( ButtonListController dropdown, string[] options, int activeIndex )
		{
			dropdown.ClearOptions();
			dropdown.SetOptions( options, activeIndex );
		}

		private void RefreshDropdowns( CollectionFilterData collectionFilterListData )
		{
			//Update dropdowns
			//SetDropdown( mClassButtonList, collectionFilterListData.ShipClassOptions, collectionFilterListData.SelectedShipClassIndex );
			SetDropdown( mPowerDropdown, collectionFilterListData.PowerCostOptions, collectionFilterListData.SelectedPowerCostIndex );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
            mClassNeutralToggle.onValueChanged.AddListener(HandleClassToggleChanged);
            mClassAncientToggle.onValueChanged.AddListener(HandleClassToggleChanged);
            mClassEnforcerToggle.onValueChanged.AddListener(HandleClassToggleChanged);
            mClassSmugglerToggle.onValueChanged.AddListener(HandleClassToggleChanged);

			mLeftButton.onClick.AddListener( FirePageLeft );
			mRightButton.onClick.AddListener( FirePageRight );
		}

		private void UnregisterListeners()
		{
            mClassNeutralToggle.onValueChanged.RemoveListener(HandleClassToggleChanged);
            mClassAncientToggle.onValueChanged.RemoveListener(HandleClassToggleChanged);
            mClassEnforcerToggle.onValueChanged.RemoveListener(HandleClassToggleChanged);
            mClassSmugglerToggle.onValueChanged.RemoveListener(HandleClassToggleChanged);

			mLeftButton.onClick.RemoveListener( FirePageLeft );
			mRightButton.onClick.RemoveListener( FirePageRight );
		}
		#endregion

		#region Event Firing
		private void FirePageLeft()
		{
			if( PageLeft != null )
			{
				PageLeft( this, EventArgs.Empty );
			}
		}

		private void FirePageRight()
		{
			if( PageRight != null )
			{
				PageRight( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
        private void HandleClassToggleChanged(bool value)
		{
			mPowerDropdown.Toggle( false );

            if(!value) 
            {
                // Only interested when the value is going on
                return;
            }

            Toggle activeToggle = mClassToggleGroup.ActiveToggles().First();
            TextMeshProUGUI text = activeToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            string className = text.text;

            SetClassToggleTextColour(className);
            SetClassZOrder(className);

            // If anyone is listening to the event
            if(ClassFilterSelected != null)
            {
                ClassFilterSelected(text.text);
            }

		}

        private void SetClassZOrder(string className)
        {
            switch(className)
            {
                case "Neutral":
                    mClassNeutralToggle.transform.SetSiblingIndex(3);
                    break;
                case "Ancient":
                    mClassAncientToggle.transform.SetSiblingIndex(3);
                    break;
                case "Enforcer":
                    mClassEnforcerToggle.transform.SetSiblingIndex(3);
                    break;
                case "Smuggler":
                    mClassSmugglerToggle.transform.SetSiblingIndex(3);
                    break;
                default:
                    SJLogger.Assert("className ({0})not recognised", className);
                    break;
            }         
        }

        private void SetClassToggleTextColour(string className)
        {
            switch(className)
            {
                case "Neutral":
                    mClassNeutralText.color = mHighlighted;
                    mClassAncientText.color = mDisabled;
                    mClassEnforcerText.color = mDisabled;
                    mClassSmugglerText.color = mDisabled;
                    break;
                case "Ancient":
                    mClassNeutralText.color = mDisabled;
                    mClassAncientText.color = mHighlighted;
                    mClassEnforcerText.color = mDisabled;
                    mClassSmugglerText.color = mDisabled;
                    break;
                case "Enforcer":
                    mClassNeutralText.color = mDisabled;
                    mClassAncientText.color = mDisabled;
                    mClassEnforcerText.color = mHighlighted;
                    mClassSmugglerText.color = mDisabled;
                    break;
                case "Smuggler":
                    mClassNeutralText.color = mDisabled;
                    mClassAncientText.color = mDisabled;
                    mClassEnforcerText.color = mDisabled;
                    mClassSmugglerText.color = mHighlighted;
                    break;
                default:
                    SJLogger.Assert("className ({0})not recognised", className);
                    break;
            }         
        }
		#endregion
	}
}