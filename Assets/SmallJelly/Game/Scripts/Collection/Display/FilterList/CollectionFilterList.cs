﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace SmallJelly
{
	// <summary>
	/// Menu class which controls the list of filters in the collections - this is the panel which lets the user
	/// filter their collection by class, card power cost etc.
	/// </summary>
	public class CollectionFilterList : MenuController
	{
		#region Event Handlers
		public event EventHandler< CollectionFilterEventArgs > FiltersUpdated;


        public event Action<string> ClassFilterSelected
		{
			add
			{
				mCollectionFilterListController.ClassFilterSelected += value;
			}

			remove
			{
				mCollectionFilterListController.ClassFilterSelected -= value;
			}
		}

		public event EventHandler< StringEventArgs > PowerCostFilterSelected
		{
			add
			{
				mCollectionFilterListController.PowerCostFilterSelected += value;
			}

			remove
			{
				mCollectionFilterListController.PowerCostFilterSelected -= value;
			}
		}

		public event EventHandler PageLeft
		{
			add
			{
				mCollectionFilterListController.PageLeft += value;
			}

			remove
			{
				mCollectionFilterListController.PageLeft -= value;
			}
		}

		public event EventHandler PageRight
		{
			add
			{
				mCollectionFilterListController.PageRight += value;
			}

			remove
			{
				mCollectionFilterListController.PageRight -= value;
			}
		}

		public event EventHandler AnimationFinished
		{
			add
			{
				mCollectionFilterListController.AnimationFinished += value;
			}

			remove
			{
				mCollectionFilterListController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public CollectionFilterData CollectionFilterData
		{
			get
			{
				return mCollectionFilterData;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private CollectionFilterListController mCollectionFilterListController;
		#endregion

		#region Member Variables
		private int mPageIndex;

		private CollectionFilterData mCollectionFilterData;
		#endregion

		#region Public Methods
        public void SetEditingDeck(string className)
        {            
            mCollectionFilterListController.SetEditingDeck(className);
        }

        public void ClearEditingDeck()
        {
            mCollectionFilterListController.ClearEditingDeck();
        }

		public void OnInsertState( CollectionFilterListController.State state )
		{
			mCollectionFilterListController.OnInsertState( state );
		}

		//Called when the object is created - used to construct the object with the
		//supplied configuration
		public void OnConstruct( CollectionDisplayConfig collectionDisplayConfig )
		{
			mCollectionFilterData.NumberOfElementsPerPage = collectionDisplayConfig.NumberOfElementsPerRow * collectionDisplayConfig.NumberOfElementsPerColumn;
		}

		public void Refresh( CollectionFilterData collectionFilterListData, bool filtersUpdated )
		{
			mCollectionFilterListController.Refresh( collectionFilterListData );

			mCollectionFilterData = collectionFilterListData;

			if( filtersUpdated )
			{
				FireRetrievePage();
			}
		}
		#endregion

		#region Event Firing
		private void FireRetrievePage()
		{
			if( FiltersUpdated != null )
			{
				FiltersUpdated( this, new CollectionFilterEventArgs( mCollectionFilterData ) );
			}
		}
		#endregion

	}
}