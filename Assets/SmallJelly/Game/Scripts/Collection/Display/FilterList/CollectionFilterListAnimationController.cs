﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class CollectionFilterListAnimationController : AnimationController
	{
		#region Constants
		private const string MOVE_IN_ANIMATION_TEMPLATE_NAME = "MoveIn";
		#endregion

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/FilterDisplay/"; } }
		#endregion

		#region Methods
		public void UpdateAnimations( CollectionFilterListController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			switch( state )
			{
				case CollectionFilterListController.State.MoveIn:
					PlayAnimation( MOVE_IN_ANIMATION_TEMPLATE_NAME );
					break;
			}

			SetMetaData( animationMetaData );
		}
		#endregion
	}
}
