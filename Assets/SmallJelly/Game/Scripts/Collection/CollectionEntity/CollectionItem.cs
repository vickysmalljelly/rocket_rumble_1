﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class CollectionItem : Targetable
	{
		#region Public Events
		public event EventHandler< CollectionItemAndStateTransitionEventArgs > StateTransitioned;

		public event EventHandler AnimationFinished
		{
			add
			{
				CollectionItemController.AnimationFinished += value;
			}

			remove
			{
				CollectionItemController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public CollectionItemController CollectionItemController
		{
			get
			{
				return (CollectionItemController)TargetableLogicController;
			}

			set
			{
				TargetableLogicController = value;
			}
		}

		public ObjectInputListener ObjectInputListener { get; set; }
		#endregion

		#region Unity Methods
		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState(  CollectionItemController.State state )
		{
			CollectionItemController.OnInsertState( state );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			CollectionItemController.CollectionEntityStateUpdated += HandleStateUpdated;
		}

		private void UnregisterListeners()
		{
			CollectionItemController.CollectionEntityStateUpdated -= HandleStateUpdated;
		}

		private void RegisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver += CollectionItemController.HandleMouseRolledOver;
			ObjectInputListener.MouseHovered += CollectionItemController.HandleMouseHovered;
			ObjectInputListener.MouseRolledOff += CollectionItemController.HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver += CollectionItemController.HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged += CollectionItemController.HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff += CollectionItemController.HandleMouseDraggedOff;

			ObjectInputListener.MousePressed += CollectionItemController.HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver += CollectionItemController.HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff += CollectionItemController.HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver += CollectionItemController.HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged += CollectionItemController.HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff += CollectionItemController.HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed += CollectionItemController.HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver += CollectionItemController.HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff += CollectionItemController.HandleTouchReleasedOff;
		}

		private void UnregisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver -= CollectionItemController.HandleMouseRolledOver;
			ObjectInputListener.MouseHovered -= CollectionItemController.HandleMouseHovered;
			ObjectInputListener.MouseRolledOff -= CollectionItemController.HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver -= CollectionItemController.HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged -= CollectionItemController.HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff -= CollectionItemController.HandleMouseDraggedOff;

			ObjectInputListener.MousePressed -= CollectionItemController.HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver -= CollectionItemController.HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff -= CollectionItemController.HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver -= CollectionItemController.HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged -= CollectionItemController.HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff -= CollectionItemController.HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed -= CollectionItemController.HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver -= CollectionItemController.HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff -= CollectionItemController.HandleTouchReleasedOff;
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( CollectionItem collectionItem, CollectionItemController.State previousState, CollectionItemController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new CollectionItemAndStateTransitionEventArgs(collectionItem, previousState, newState, hitLayers) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateUpdated( object o, CollectionItemStateTransitionEventArgs battleEntityStateEventArgs )
		{
			FireStateTransitioned( this, battleEntityStateEventArgs.PreviousState, battleEntityStateEventArgs.NewState, battleEntityStateEventArgs.EntityHitLayers );
		}
		#endregion

	}
}