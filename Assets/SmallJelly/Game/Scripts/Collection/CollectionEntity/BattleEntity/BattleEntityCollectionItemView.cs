﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using TMPro;

namespace SmallJelly
{
	public class BattleEntityCollectionItemView : CollectionItemView
	{
		#region Public Properties
		public TemplateBattleEntityModel Template
		{ 
			get;
			set;
		}

		public TemplateBattleEntityParts TemplateBattleEntityParts
		{ 
			get;
			set;
		}

		public CardParts Parts
		{
			get
			{
				return Template.CardParts;
			}
		}

		public TextMeshPro NumberOfCopiesText { get; set; }

		public GameObject NewText { get; set; }
		#endregion

		#region MonoBehaviour Methods
		private void OnDestroy()
		{
		}

		protected override void OnEnable()
		{
			base.OnEnable();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( CollectionBattleEntityData collectionBattleEntityData )
		{
			Template.OnRefresh( collectionBattleEntityData.Card );

			Parts.OnRefresh( collectionBattleEntityData.Card );

			EnableParts();

			//Update the count for the number of collection items
			NumberOfCopiesText.text = string.Format( "x{0}", collectionBattleEntityData.Count );

			//Only show the number of copies text where the value is greater than 1
			if( collectionBattleEntityData.Count > 1 ) 
			{ 
				NumberOfCopiesText.gameObject.SetActive( true );
			} 
			else
			{
				NumberOfCopiesText.gameObject.SetActive( false );
			}

			NewText.SetActive( collectionBattleEntityData.New );

			NumberOfCopiesText.transform.SetAsLastSibling();
			NewText.transform.SetAsLastSibling();
		}
		#endregion

		#region Private Methods
		private void EnableParts()
		{
			SetActiveRecursively( Parts.Card, true );
			SetActiveRecursively( Parts.Component, true );
			SetActiveRecursively( Parts.ComponentHousing, true );
			SetActiveRecursively( Parts.Mask, true );
			SetActiveRecursively( Parts.BasePlate, false );
		}
		#endregion
	}
}