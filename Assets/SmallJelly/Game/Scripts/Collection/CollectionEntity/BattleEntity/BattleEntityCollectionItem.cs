﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class BattleEntityCollectionItem : CollectionItem
	{
		#region Public Properties
		//The data of the card right now in the flow - used for stat displays etc
		public CollectionBattleEntityData Data
		{
			get 
			{
				return mData;
			}
		}
		#endregion

		#region Member Variables
		private CollectionBattleEntityData mData;
		#endregion

		#region Public Methods
		public void OnRefreshData( CollectionBattleEntityData collectionBattleEntityData )
		{
			mData = collectionBattleEntityData;
			BattleEntityCollectionItemController battleEntityCollectionItemController = (BattleEntityCollectionItemController)CollectionItemController;
			battleEntityCollectionItemController.OnRefreshData( collectionBattleEntityData );
		}
		#endregion

	}
}