﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityCollectionItemController : CollectionItemController
	{
		#region Public Methods
		public void OnRefreshData( CollectionBattleEntityData collectionBattleEntityData )
		{
			( ( BattleEntityCollectionItemView ) View ).OnRefreshData( collectionBattleEntityData );
		}
		#endregion

	}
}