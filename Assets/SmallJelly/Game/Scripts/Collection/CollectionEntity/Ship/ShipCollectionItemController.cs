﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShipCollectionItemController : CollectionItemController
	{
		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			( ( ShipCollectionItemView ) View ).OnRefreshData( battleEntityData );
		}
		#endregion
	}
}