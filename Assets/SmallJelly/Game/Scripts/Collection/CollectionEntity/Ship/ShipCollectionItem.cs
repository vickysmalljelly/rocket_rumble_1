﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShipCollectionItem : CollectionItem
	{
		#region Public Properties
		//The data of the card right now in the flow - used for stat displays etc
		public BattleEntityData Data
		{
			get 
			{
				return mData;
			}
		}
		#endregion

		#region Member Variables
		private BattleEntityData mData;
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			mData = battleEntityData;
			ShipCollectionItemController shipCollectionItemController = ( ShipCollectionItemController )CollectionItemController;
			shipCollectionItemController.OnRefreshData( battleEntityData );
		}
		#endregion

	}
}