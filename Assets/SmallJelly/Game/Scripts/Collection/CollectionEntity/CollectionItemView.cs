﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class CollectionItemView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished;
		#endregion

		#region Constants
		protected const int MAXIMUM_TEXT_LENGTH = 20;
		#endregion

		#region Public Properties
		public CollectionItemAnimationController AnimationController 
		{ 
			get;
			private set;
		}
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			AnimationController = GetComponent< CollectionItemAnimationController >();
		}
		#endregion

		#region Public Methods
		public void OnPlayAnimation( CollectionItemController.State state, AnimationMetaData animationMetaData )
		{
			AnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion

		#region Protected Mehtods
		protected virtual void OnEnable()
		{
			RegisterAnimationListeners( AnimationController );
		}

		protected virtual void OnDisable()
		{
			UnregisterAnimationListeners( AnimationController );
		}

		protected void SetActiveRecursively(GameObject rootObject, bool active)
		{
			rootObject.SetActive(active);

			foreach (Transform childTransform in rootObject.transform)
			{
				SetActiveRecursively(childTransform.gameObject, active);
			}
		}
		#endregion

		#region Event Registration
		private void RegisterAnimationListeners( CollectionItemAnimationController collectionEntityAnimationController )
		{
			collectionEntityAnimationController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterAnimationListeners( CollectionItemAnimationController collectionEntityAnimationController )
		{
			collectionEntityAnimationController.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}
		#endregion
	}
}