﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class CollectionItemAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Collection/Cards/"; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string SHOWING_ANIMATION_TEMPLATE_NAME = "ShowingAnimation";
		#endregion

		#region Public Methods
		public void UpdateAnimations( CollectionItemController.State cardState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();
			switch(cardState)
			{
				case  CollectionItemController.State.Initialisation:
					PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case  CollectionItemController.State.Reveal:
					PlayAnimation( SHOWING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}