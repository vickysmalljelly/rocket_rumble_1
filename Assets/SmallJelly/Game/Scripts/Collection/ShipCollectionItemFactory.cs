﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the ship collection items
	public class ShipCollectionItemFactory : CardFactory
	{
		#region Public Methods
		public static ShipCollectionItem ConstructCollectionItem( BattleEntityData battleEntityData )
		{
			GameObject view = LoadCardView( CardType.Ship );
			view.name = string.Format( "Card{0}", battleEntityData.DrawNumber );

			AssignComponents (view, battleEntityData );

			ShipCollectionItem shipCollectionItem = view.GetComponent< ShipCollectionItem > ();
			shipCollectionItem.OnRefreshData( battleEntityData );
			shipCollectionItem.OnInsertState(ShipCollectionItemController.State.Initialisation );

			return shipCollectionItem;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, BattleEntityData battleEntityData )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the collection entity (the collection entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationController( view );
			AssignController( view );
			AssignCollectionItemComponent( view, battleEntityData);

			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< CollectionItemAnimationController > ();
		}

		private static void AssignController( GameObject view )
		{
			ShipCollectionItemController collectionEntityController = view.AddComponent< ShipCollectionItemController > ();
			collectionEntityController.View = view.GetComponent< ShipCollectionItemView >();
		}

		private static void AssignCollectionItemComponent( GameObject view, BattleEntityData battleEntityData )
		{
			//Add battle entity component
			ShipCollectionItem collectionEntity = view.AddComponent< ShipCollectionItem >();
			collectionEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			collectionEntity.CollectionItemController = view.GetComponent< ShipCollectionItemController >();
		}
		#endregion
	}
}
