﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;
using System.IO;

namespace SmallJelly
{
	//A factory class, used to construct the battle entities (cards/components)
	public class BattleEntityCollectionItemFactory : CardFactory
	{
		#region Paths
		private const string NEW_CARD_PREFAB = "ui_newCardLabel";
		private const string NUMBER_OF_COLLECTION_COPIES_PREFAB = "ui_numberOfCollectionCopies";

		#endregion

		#region Public Methods
		public static BattleEntityCollectionItem ConstructCollectionItem( CollectionBattleEntityData collectionBattleEntityData, Vector3 numberOfCopiesTextOffset, Vector3 newTextOffset )
		{
			GameObject view = LoadCardView( CardType.BattleEntity );
			view.name = string.Format( "Card{0}", collectionBattleEntityData.Card.DrawNumber );

			AssignComponents (view, collectionBattleEntityData, numberOfCopiesTextOffset, newTextOffset );

			BattleEntityCollectionItem battleEntityCollectionItem = view.GetComponent< BattleEntityCollectionItem > ();
			battleEntityCollectionItem.OnRefreshData( collectionBattleEntityData );
			battleEntityCollectionItem.OnInsertState( BattleEntityCollectionItemController.State.Initialisation );

			return battleEntityCollectionItem;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, CollectionBattleEntityData collectionBattleEntityData, Vector3 numberOfCopiesTextOffset, Vector3 newTextOffset )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the collection entity (the collection entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationController( view );
			AssignView(view);
			AssignController( view );
			AssignCollectionItemComponent( view );

			AddNumberOfCopiesText( view, numberOfCopiesTextOffset );
			AddNewText( view, newTextOffset );
			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< CollectionItemAnimationController > ();
		}

		private static void AssignView( GameObject view )
		{
			BattleEntityCollectionItemView battleEntityCollectionItemView = view.AddComponent< BattleEntityCollectionItemView > ();
			battleEntityCollectionItemView.Template = view.GetComponent< TemplateBattleEntityModel >();
			battleEntityCollectionItemView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();
		}

		private static void AssignController( GameObject view )
		{
			BattleEntityCollectionItemController collectionEntityController = view.AddComponent< BattleEntityCollectionItemController > ();
			collectionEntityController.View = view.GetComponent< BattleEntityCollectionItemView >();
		}

		private static void AssignCollectionItemComponent( GameObject view )
		{
			//Add battle entity component
			BattleEntityCollectionItem collectionEntity = view.AddComponent< BattleEntityCollectionItem >();
			collectionEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			collectionEntity.CollectionItemController = view.GetComponent< BattleEntityCollectionItemController >();
		}

		private static void AddNumberOfCopiesText( GameObject view, Vector3 numberOfCopiesTextOffset )
		{
			GameObject numberOfCopiesTextPrefab = Resources.Load< GameObject >( string.Format("{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, NUMBER_OF_COLLECTION_COPIES_PREFAB ) );
			GameObject numberOfCopiesText = UnityEngine.Object.Instantiate( numberOfCopiesTextPrefab, Vector3.zero, Quaternion.identity ) as GameObject;

			numberOfCopiesText.transform.SetParent( view.transform );
			numberOfCopiesText.transform.localPosition = numberOfCopiesTextOffset;

			BattleEntityCollectionItemView battleEntityCollectionItemView = view.GetComponent< BattleEntityCollectionItemView > ();
			battleEntityCollectionItemView.NumberOfCopiesText = numberOfCopiesText.GetComponent< TextMeshPro >();
		}

		private static void AddNewText( GameObject view, Vector3 newTextOffset )
		{
			GameObject newTextPrefab = Resources.Load< GameObject >( string.Format("{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, NEW_CARD_PREFAB ) );
			GameObject newText = UnityEngine.Object.Instantiate( newTextPrefab, Vector3.zero, Quaternion.identity ) as GameObject;

			newText.transform.SetParent( view.transform );
			newText.transform.localPosition = newTextOffset;

			BattleEntityCollectionItemView battleEntityCollectionItemView = view.GetComponent< BattleEntityCollectionItemView > ();
			battleEntityCollectionItemView.NewText = newText;
		}
		#endregion
	}
}
