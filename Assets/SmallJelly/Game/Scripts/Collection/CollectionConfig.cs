﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace SmallJelly
{
    /// <summary>
    /// Contains information about how we construct a collection.
    /// </summary>
    [Serializable]
    public class CollectionConfig  
    {
        public bool AllCards; 
    }
}