﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly 
{
    /// <summary>
    /// Displays the news notification
    /// </summary>
    public class NewsNotificationView : UIView<NewsData>  
    {
        [SerializeField]
        private Text mText;

        [SerializeField]
        private Button mButton;

        public Action NotificationClicked;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mText != null, "mText has not been set in the inspector");
            SJLogger.AssertCondition(mButton != null, "mButton has not been set in the inspector");
        }

        private void OnEnable()
        {
            mButton.onClick.AddListener(HandleButtonClicked);
        }

        private void OnDisable()
        {
            mButton.onClick.RemoveListener(HandleButtonClicked);
        }

        protected override void OnRefreshView(NewsData data)
        {
			//TODO - Come up with a proper solution for this - we're getting null ref errors when switching tabs without this
			if( mText != null )
			{
            	mText.text = data.Headline;
			}
        }

        private void HandleButtonClicked()
        {
            if(NotificationClicked != null)
            {
                NotificationClicked();
            }
        }
    }
}