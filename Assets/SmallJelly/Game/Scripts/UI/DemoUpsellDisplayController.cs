using System;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using SmallJelly.Framework;

namespace SmallJelly
{
	public class DemoUpsellDisplayController : MenuController 
	{
		#region Public Event Handlers
		public event EventHandler Finished;
		#endregion

		#region Public Methods
		public void OnFinished()
		{
			if( Finished != null )
			{
				Finished( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}