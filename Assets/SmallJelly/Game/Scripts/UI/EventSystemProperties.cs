﻿using UnityEngine;
using UnityEngine.EventSystems;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class EventSystemProperties : SJMonoBehaviour
	{
		#region Exposed To Inspector
		[SerializeField] 
		private EventSystem mEventSystem;

		[SerializeField] 
		private int mReferenceDPI = 100;

		[SerializeField] 
		private float mReferencePixelDrag = 5.0f;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition( mEventSystem != null, "Event System cannot be null" );
			RefreshPixelDrag( Screen.dpi );
		}
		#endregion

		#region Private Methods
		private void RefreshPixelDrag(float screenDpi)
		{
			mEventSystem.pixelDragThreshold = Mathf.RoundToInt( ( screenDpi / mReferenceDPI ) * mReferencePixelDrag);
		}
		#endregion
	}
}