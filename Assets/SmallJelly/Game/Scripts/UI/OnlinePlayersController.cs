using UnityEngine;
using UnityEngine.UI;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Controller for scrolling list of online players
	/// </summary>
	public class OnlinePlayersController : UIList<OnlineUserView, User>
	{
		public Action<User> OnlinePlayerClicked;

		#region Methods
		public void OnRefreshClicked()
		{
			DialogManager.Get.ShowSpinner();
			
			ClientUser.ListOnlineUsers(SuccessHandler, ErrorHandler);
		}
		#endregion
		
		#region Implementation
		private void SuccessHandler(List<User> onlineUsers)
		{
			DialogManager.Get.HideSpinner();

			SJLogger.LogMessage(MessageFilter.UI, "Repopulating the list");


			// TODO: Allow passing an event handler with args to be called when "Play" button is clicked
			RepopulateList(onlineUsers, HandleUserClicked);
		}
		
		private void ErrorHandler(ServerError error)
		{
			DialogManager.Get.HideSpinner();

			// TODO: what do we do here?
		}

		private void HandleUserClicked(User user)
		{
			SJLogger.LogMessage(MessageFilter.UI, "User {0} clicked", user.DisplayName);
			if(OnlinePlayerClicked != null)
			{
				OnlinePlayerClicked(user);
			}
		}

		#endregion Implementation
	}
}
