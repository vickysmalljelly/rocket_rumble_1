﻿using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    /// <summary>
    /// Displays a news item within a scrolling list
    /// </summary>
    public class NewsItemView : UIView<NewsItem> 
    {
        [SerializeField]
        private Text mTitle;

        [SerializeField]
        private Text mBody;

        [SerializeField]
        private Text mDate;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mTitle != null, "mTitle has not been set in the inspector");
            SJLogger.AssertCondition(mBody != null, "mBody has not been set in the inspector");
            SJLogger.AssertCondition(mDate != null, "mDate has not been set in the inspector");
        }
            
        protected override void OnRefreshView(NewsItem data)
        {
            mTitle.text = data.Title;
            mBody.text = data.Content;
            mDate.text = data.Date;
        }
    }
}
