﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Controls loading and display of news
    /// </summary>
    public class NewsController : SJMonoBehaviour 
    {
        [SerializeField]
        private NewsNotificationView mNotificationView;

        [SerializeField]
        private NewsPopupView mPopupView;

        [SerializeField]
        private PlayMenuController mStartMenuController;

        // Data to be displayed
        private NewsData mNewsData;

        #region SJMonoBehaviour
        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mNotificationView != null, "mNotificationView has not been set in the inspector");
            SJLogger.AssertCondition(mPopupView != null, "mPopupView has not been set in the inspector");
            SJLogger.AssertCondition(mStartMenuController != null, "mStartMenuController has not been set in the inspector");

            // Get the most recent news
            ClientNews.GetNewsItems(GetNewsItemsSuccessHandler, GetNewsItemsErrorHandler);
        }

        private void OnEnable()
        {
            // Show the notification
            mNotificationView.gameObject.SetActive(true);

            // Hide the popup
            mPopupView.gameObject.SetActive(false);

            mNotificationView.NotificationClicked += HandleNotificationClicked;
            mPopupView.CloseButtonClicked += HandlePopupCloseClicked;
        }

        private void OnDisable()
        {
            // Hide all 
            mNotificationView.gameObject.SetActive(false);
            mPopupView.gameObject.SetActive(false);

            mNotificationView.NotificationClicked -= HandleNotificationClicked;
            mPopupView.CloseButtonClicked -= HandlePopupCloseClicked;
        }

        #endregion

        private void HandleNotificationClicked()
        {
            // Stop lisenting to clicks while we have the popup open
            mNotificationView.NotificationClicked -= HandleNotificationClicked;

            // Show the news popup
            mPopupView.gameObject.SetActive(true);

            // Hide the notification
            mNotificationView.gameObject.SetActive(false);

            // Hide the other buttons
            mStartMenuController.HideNavigationButtons();
        }

        private void HandlePopupCloseClicked()
        {
            // Start listening to notification clicks again
            mNotificationView.NotificationClicked += HandleNotificationClicked;

            // Hide the news popup
            mPopupView.gameObject.SetActive(false);

            // Show the notification
            mNotificationView.gameObject.SetActive(true);

            // Show the other buttons
            mStartMenuController.ShowNavigationButtons();
        }

        private void GetNewsItemsSuccessHandler(List<NewsItem> newsItems)
        {
			//TODO - Come up with a proper solution for this - we're getting null ref errors when switching tabs without this
			if( mNotificationView != null && mPopupView != null )
			{
				// Get the most recent news
	            mNewsData = new NewsData(newsItems);

	            mNotificationView.OnRefresh(mNewsData);
	            mPopupView.OnRefresh(mNewsData);
			}
        }

        private void GetNewsItemsErrorHandler(ServerError error)
        {
            SJLogger.LogError("Failed to retrieve news items from server. Error Type: {0}", error.ErrorType);
        }

    }
}
