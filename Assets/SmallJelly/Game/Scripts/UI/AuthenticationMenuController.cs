﻿using UnityEngine;
using UnityEngine.UI;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Controls the Authentication Menu which handles both registering a new account and logging in an existing account.
	/// </summary>
	public class AuthenticationMenuController : MenuController 
	{
		// Temp length for development
		public const int MinPasswordLength = 2;

		public const int MaxPassswordLength = 12;

		public const int MinUsernameLength = 2;

		public const int MaxUsernameLength = 12;

        public const int MinEmailLength = 3;

        public const int MaxEmailLength = 254;

		public Action AuthenticationCompleted;

		public LoginController LoginController;

		[SerializeField]
		private RegistrationController mRegistrationController;

		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(LoginController != null, "LoginController not set in inspector");
			SJLogger.AssertCondition(mRegistrationController != null, "RegistrationController not set in inspector");
		}

        private void OnEnable()
        {
            AttachToEvents();
        }

        private void OnDisable()
        {
            DetachFromEvents();
        }

		private void HandleAuthenticationCompleted()
		{
			if(AuthenticationCompleted != null)
			{
				AuthenticationCompleted();
			}
		}

        private void AttachToEvents()
        {
            LoginController.LoginCompleted += HandleAuthenticationCompleted;
            mRegistrationController.RegistrationCompleted += HandleAuthenticationCompleted;
        }

        private void DetachFromEvents()
        {
            LoginController.LoginCompleted -= HandleAuthenticationCompleted;
            mRegistrationController.RegistrationCompleted -= HandleAuthenticationCompleted;
        }
	}
	
}
