﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class FrontEndMenuView : SJMonoBehaviour
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mFrontEndMenuAnimationController.AnimationFinished += value;
			}

			remove
			{
				mFrontEndMenuAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private FrontEndMenuAnimationController mFrontEndMenuAnimationController;
		#endregion

		#region Public Methods
		public void OnInsertState( FrontEndMenuController.State state )
		{
			mFrontEndMenuAnimationController.UpdateAnimations( state );
		}
		#endregion
	}
}