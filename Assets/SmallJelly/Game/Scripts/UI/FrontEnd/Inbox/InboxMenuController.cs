using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Handles input from the Inbox
	/// </summary>
	public class InboxMenuController : FrontEndMenuController 
	{
		#region Public Properties
		public InboxList InboxList
		{
			get
			{
				return mInboxList;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private InboxList mInboxList;

        [SerializeField]
        private TMPro.TextMeshProUGUI mUnopenedCounter;

        [SerializeField]
        private GameObject mEmptyInboxMessage;

        [SerializeField]
        private GameObject mSpinner;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			UIManager.Get.AddMenu< InboxList >( mInboxList.gameObject );
		}

		private void Start()
		{
			UIManager.Get.ShowMenu< InboxList >( );
		}

        public override void OnEnable()
        {
            base.OnEnable();

            mInboxList.InboxUpdated += HandleInboxUpdated;

            mSpinner.SetActive(true);
        }          

        public override void OnDisable()
        {            
            mInboxList.InboxUpdated -= HandleInboxUpdated;

            base.OnDisable();
        }

		private void OnDestroy()
		{
			//Prevent erroneously throwing errors when the game is shutting down
			if( UIManager.Get != null )
			{
				UIManager.Get.RemoveMenu< InboxList >();
			}
		}
		#endregion

        private void HandleInboxUpdated(int numItems)
        {
            mUnopenedCounter.text = numItems.ToString();

            mEmptyInboxMessage.SetActive(numItems == 0);

            mSpinner.SetActive(false);
        }
	}
	
}

