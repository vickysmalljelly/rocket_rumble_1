﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class InboxAnimationController : FrontEndMenuAnimationController
	{

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/FrontEnd/Inbox/"; } }
		#endregion

	}
}