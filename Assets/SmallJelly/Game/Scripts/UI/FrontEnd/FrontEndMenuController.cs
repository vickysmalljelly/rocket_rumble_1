﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Base class for controllers of our front end, i.e. Play, Shop, Collection and Inbox
    /// </summary>
	public class FrontEndMenuController : MenuController
	{
		#region Public Event Handlers
		public event EventHandler< EnumEventArgs< State > > StateTransitioned;
		public event EventHandler AnimationFinished
		{
			add
			{
				mFrontEndMenuView.AnimationFinished += value;
			}

			remove
			{
				mFrontEndMenuView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Inner classes
		public enum State
		{
			MovingIn,
			MovedIn,
			MovingOut,
			MovedOut
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private FrontEndMenuView mFrontEndMenuView;
		#endregion

		#region Member Variables
		private State mState;
		#endregion

		#region MonoBehaviour Methods
		public virtual void OnEnable()
		{
			RegisterListeners();
		}

		public virtual void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			mFrontEndMenuView.OnInsertState( state );
			mState = state;

			FireStateTransitioned( state );
		}
		#endregion

		#region Private Methods
		private void OnAnimationsFinished()
		{
			switch( mState )
			{
				case State.MovingIn:
					OnInsertState( State.MovedIn );
					break;

				case State.MovingOut:
					OnInsertState( State.MovedOut );
					break;
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mFrontEndMenuView.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mFrontEndMenuView.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion


		#region Event Firing
		private void FireStateTransitioned( State state )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new EnumEventArgs< State >( state ) );
			}
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			OnAnimationsFinished();
		}
		#endregion
	}
}