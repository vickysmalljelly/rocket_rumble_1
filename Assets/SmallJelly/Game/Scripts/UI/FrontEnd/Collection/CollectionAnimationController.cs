﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class CollectionAnimationController : FrontEndMenuAnimationController
	{

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/FrontEnd/Collection/"; } }
		#endregion

	}
}