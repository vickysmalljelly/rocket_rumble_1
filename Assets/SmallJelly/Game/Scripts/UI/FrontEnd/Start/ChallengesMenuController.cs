﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{

    public class ChallengesMenuController : MenuController 
    {
        public event Action BackButtonClicked; 

        [SerializeField]
        private Button mBackButton;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mBackButton != null, "mBackButton is not set in Inspector");

            mBackButton.onClick.AddListener(HandleBackButtonClicked);
        }

        private void HandleBackButtonClicked()
        {
            if(BackButtonClicked != null)
            {
                BackButtonClicked();
            }
        }
    }
}
