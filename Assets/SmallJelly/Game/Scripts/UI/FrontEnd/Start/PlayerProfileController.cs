﻿using SmallJelly.Framework;
using TMPro;
using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
    public class PlayerProfileController : SJMonoBehaviour 
    {
        [SerializeField]
        private TextMeshProUGUI mUserName;

        [SerializeField]
        private Image mRankInsignia;

        [SerializeField]
        private TextMeshProUGUI mRankName; 

        [SerializeField]
        private GameObject mStar1; 

        [SerializeField]
        private GameObject mStar2; 

        [SerializeField]
        private GameObject mStar3; 

        [SerializeField]
        private GameObject mStar4; 

        [SerializeField]
        private TextMeshProUGUI mLeaderboardPosition; 

        private List<GameObject> mStars = new List<GameObject>();
        private List<GameObject> mStarsOn = new List<GameObject>();

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mUserName != null, "mUserName must be set in the inspector");
            SJLogger.AssertCondition(mRankInsignia != null, "mRankInsignia must be set in the inspector");
            SJLogger.AssertCondition(mRankName != null, "mRankName must be set in the inspector");
            SJLogger.AssertCondition(mStar1 != null, "mStar1 must be set in the inspector");
            SJLogger.AssertCondition(mStar2 != null, "mStar2 must be set in the inspector");
            SJLogger.AssertCondition(mStar3 != null, "mStar3 must be set in the inspector");
            SJLogger.AssertCondition(mStar4 != null, "mStar4 must be set in the inspector");
            SJLogger.AssertCondition(mLeaderboardPosition != null, "mLeaderboardPosition must be set in the inspector");

            mStars.Add(mStar1);
            mStars.Add(mStar2);
            mStars.Add(mStar3);
            mStars.Add(mStar4);

            string name = "onGraphic";
            foreach(GameObject star in mStars)
            {
                mStarsOn.Add(star.transform.FindChild(name).gameObject);
            }
        }

        private void OnEnable()
        {
            mUserName.text = ClientAuthentication.PlayerAccountDetails.DisplayName;
            mRankName.text = GameManager.Get.PlayerData.TierName + " " + GameManager.Get.PlayerData.RankName;
            int totalStars = GameManager.Get.PlayerData.TotalStarsAtThisRank - 1;
            int numStars = GameManager.Get.PlayerData.StarsAtThisRank - 1;

            for(int i = 0; i < mStars.Count; i++)
            {
                if(i <= totalStars)
                {
                    mStars[i].SetActive(true);
                }
                else
                {
                    mStars[i].SetActive(false);
                }

                if(i <= numStars)
                {
                    mStarsOn[i].SetActive(true);
                }
                else
                {
                    mStarsOn[i].SetActive(false);
                }
            }
                
            string spriteName = "Images/Ranks/ui_insignia_rank_" + GameManager.Get.PlayerData.RankNumber;
            Sprite sprite = Resources.Load<Sprite>(spriteName);
            SJLogger.AssertCondition(sprite != null, "Failed to load sprite {0}", spriteName);
            mRankInsignia.sprite = sprite;

            LeaderboardManager.Get.LeaderboardPositionUpdated += HandleLeaderboardPositionUpdated;

            LeaderboardManager.Get.UpdateLeaderboardPosition();
        }            

        private void OnDisable()
        {
            LeaderboardManager.Get.LeaderboardPositionUpdated -= HandleLeaderboardPositionUpdated;
        }

        private void HandleLeaderboardPositionUpdated(long position)
        {           
            mLeaderboardPosition.text = AddOrdinal(position);
        }

        private string AddOrdinal(long num)
        {
            if(num <= 0) 
            {
                return num.ToString();
            }

            switch(num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch(num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }
    }
}
