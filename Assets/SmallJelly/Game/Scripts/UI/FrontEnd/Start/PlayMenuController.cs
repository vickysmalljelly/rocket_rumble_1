using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Handles input when the RR Home screen is selected.
	/// </summary>
	public class PlayMenuController : FrontEndMenuController 
	{
		#region Events
		public EventHandler MultiplayerClicked;
        public Action<BattleId> SinglePlayerClicked;
		public EventHandler MapModeClicked;
		public EventHandler ReplayClicked;
		public EventHandler LogoutClicked;
		public EventHandler RumbleClicked;
        public Action ChallengesClicked;
        public Action LootRunsClicked;
        public Action LeaderboardsClicked;
        #endregion

        #region Inspector Variables
		[SerializeField]
		private Image mLogo;

        [SerializeField]
        private Button mSinglePlayerButton;
        [SerializeField]
        private Button mKTPlayButton;
        [SerializeField]
        private Button mRumbleButton;
        [SerializeField]
        private Button mChallengesButton;
        [SerializeField]
        private Button mLootRunsButton;
		[SerializeField]
		private Button mTutorialButton;
        [SerializeField]
        private Button mSkipTutorials;
        [SerializeField]
        private Button mReplayButton;
        [SerializeField]
        private Button mTutorial1Button;
        [SerializeField]
        private Button mTutorial2Button;
        [SerializeField]
        private Button mTutorial3Button;
        [SerializeField]
        private Button mTutorial4Button;
        [SerializeField]
        private Button mTutorial5Button;
        [SerializeField]
		private Button mTutorial6Button;
		[SerializeField]
		private Button mMapButton;
        [SerializeField]
        private Button mLeaderboardsButton;
        #endregion

        #region SJMonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mSinglePlayerButton != null, "mSinglePlayerButton must be set in the inspector");
            SJLogger.AssertCondition(mKTPlayButton != null, "mKTPlayButton must be set in the inspector");
            SJLogger.AssertCondition(mRumbleButton != null, "mRumbleButton must be set in the inspector");
			SJLogger.AssertCondition(mTutorialButton != null, "mTutorialButton must be set in the inspector");
            SJLogger.AssertCondition(mSkipTutorials != null, "mMapModeButton must be set in the inspector");
            SJLogger.AssertCondition(mReplayButton != null, "mReplayButton must be set in the inspector");
            SJLogger.AssertCondition(mTutorial1Button != null, "mTutorial1Button must be set in the inspector");
            SJLogger.AssertCondition(mTutorial2Button != null, "mTutorial2Button must be set in the inspector");
            SJLogger.AssertCondition(mTutorial3Button != null, "mTutorial3Button must be set in the inspector");
            SJLogger.AssertCondition(mTutorial4Button != null, "mTutorial4Button must be set in the inspector");
            SJLogger.AssertCondition(mTutorial5Button != null, "mTutorial5Button must be set in the inspector");
            SJLogger.AssertCondition(mTutorial6Button != null, "mTutorial6Button must be set in the inspector");
			SJLogger.AssertCondition(mMapButton != null, "mMap must be set in the inspector");
            SJLogger.AssertCondition(mChallengesButton != null, "mChallengesButton must be set in the inspector");
            SJLogger.AssertCondition(mLootRunsButton != null, "mLootRunsButton must be set in the inspector");
            SJLogger.AssertCondition(mLeaderboardsButton != null, "mLeaderboardsButton must be set in the inspector");


            HideNavigationButtons();
        }

		public override void OnEnable()
        {
			base.OnEnable();

			mTutorialButton.onClick.AddListener(FireMapModeClicked);
            mRumbleButton.onClick.AddListener(HandleRumbleButtonClicked);
            mChallengesButton.onClick.AddListener(HandleChallengesButtonClicked);
            mLootRunsButton.onClick.AddListener(HandleLootRunsButtonClicked);
            mLeaderboardsButton.onClick.AddListener(HandleLeaderboardsButtonClicked);
            mSinglePlayerButton.onClick.AddListener(HandleSinglePlayerClicked);
            mReplayButton.onClick.AddListener(HandleLogoutClicked);
            mTutorial1Button.onClick.AddListener(HandleTutorial1Clicked);
            mTutorial2Button.onClick.AddListener(HandleTutorial2Clicked);
            mTutorial3Button.onClick.AddListener(HandleTutorial3Clicked);
            mTutorial4Button.onClick.AddListener(HandleTutorial4Clicked);

			mMapButton.onClick.AddListener( FireMapModeClicked );

            mSkipTutorials.onClick.AddListener(HandleDisableOnboarding);
            mKTPlayButton.onClick.AddListener(HandleKTPlayClicked);
        }
            
		public override void OnDisable()
        {
			base.OnDisable();

			mTutorialButton.onClick.RemoveListener(FireMapModeClicked);
            mRumbleButton.onClick.RemoveListener(HandleRumbleButtonClicked);
            mChallengesButton.onClick.RemoveListener(HandleChallengesButtonClicked);
            mLootRunsButton.onClick.RemoveListener(HandleLootRunsButtonClicked);
            mLeaderboardsButton.onClick.RemoveListener(HandleLeaderboardsButtonClicked);
            mSinglePlayerButton.onClick.RemoveListener(HandleSinglePlayerClicked);
            mReplayButton.onClick.RemoveListener(HandleLogoutClicked);
            mTutorial1Button.onClick.RemoveListener(HandleTutorial1Clicked);
            mTutorial2Button.onClick.RemoveListener(HandleTutorial2Clicked);
            mTutorial3Button.onClick.RemoveListener(HandleTutorial3Clicked);
            mTutorial4Button.onClick.RemoveListener(HandleTutorial4Clicked);

			mMapButton.onClick.RemoveListener( FireMapModeClicked );

            mSkipTutorials.onClick.RemoveListener(HandleDisableOnboarding);
            mKTPlayButton.onClick.RemoveListener(HandleKTPlayClicked);

            DialogManager.Get.HideOptionsButton();
        }

        #endregion

        #region Methods
        public void HideNavigationButtons()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Hide start menu buttons");

            mSinglePlayerButton.gameObject.SetActive(false);
            mKTPlayButton.gameObject.SetActive(false);
            mSkipTutorials.gameObject.SetActive(false);
            mReplayButton.gameObject.SetActive(false);
            mTutorial1Button.gameObject.SetActive(false);
            mTutorial2Button.gameObject.SetActive(false);
            mTutorial3Button.gameObject.SetActive(false);
            mTutorial4Button.gameObject.SetActive(false);
            mTutorial5Button.gameObject.SetActive(false);
            mTutorial6Button.gameObject.SetActive(false);
			mMapButton.gameObject.SetActive(false);

			mRumbleButton.transform.parent.gameObject.SetActive(false);
			mTutorialButton.transform.parent.gameObject.SetActive(false);

            DialogManager.Get.HideOptionsButton();
        }

        public void ShowNavigationButtons()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Show start menu buttons");

            #if DEBUG_BUTTONS
            mSinglePlayerButton.gameObject.SetActive(true);
            mKTPlayButton.gameObject.SetActive(true);
            mSkipTutorials.gameObject.SetActive(true);
            mReplayButton.gameObject.SetActive(true);
            mTutorial1Button.gameObject.SetActive(true);
            mTutorial2Button.gameObject.SetActive(true);
            mTutorial3Button.gameObject.SetActive(true);
            mTutorial4Button.gameObject.SetActive(true);
            mTutorial5Button.gameObject.SetActive(true);
            mTutorial6Button.gameObject.SetActive(true);
			mMapButton.gameObject.SetActive(true);

            if(FeatureFlags.Replays)
            {
                mReplayButton.gameObject.SetActive(true);
            }
            #endif

			//TODO - I dont like this sporadic way of disabling/enabling stuff - I'd like to have a think about it later on.
			//Show the tutorial button if the tutorial hasnt been completed - otherwise show the rumble button
			if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial1CheckPoint )
				|| !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial2CheckPoint )
				|| !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial3CheckPoint )
				|| !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial4CheckPoint ) )
			{
				mTutorialButton.transform.parent.gameObject.SetActive(true);
			}
			else
			{
				mRumbleButton.transform.parent.gameObject.SetActive(true);
			}

            DialogManager.Get.ShowOptionsButton();
        }     

		public void ShowLogo()
		{
			mLogo.gameObject.SetActive( true );
		}

		public void HideLogo()
		{
			mLogo.gameObject.SetActive( false );
		}
        #endregion

		#region Button Handlers

        private void HandleLogoutClicked()
        {
            if(LogoutClicked != null)
            {
                LogoutClicked(this, EventArgs.Empty);
            }
        }

        private void HandleKTPlayClicked()
        {
            #if KT_PLAY
            KTPlayManager.Get.Show();
            #endif
        }

        private void HandleDisableOnboarding()
        {
            GameManager.Get.DebugConfig.ForceSkipTutorials();
            #if USERNAME_LOGIN_ALLOWED
            GameManager.Get.ForceLogout();
            #endif
        }

        private void HandleSinglePlayerClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Single Player Clicked");

            if(SinglePlayerClicked != null)
            {
                SinglePlayerClicked(BattleId.SinglePlayer);
            }
        }

        private void HandleTutorial1Clicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Tutorial Clicked");

            if(SinglePlayerClicked != null)
            {
                SinglePlayerClicked(BattleId.Tutorial1);
            }
        }

        private void HandleTutorial2Clicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Tutorial Clicked");

            if(SinglePlayerClicked != null)
            {
                SinglePlayerClicked(BattleId.Tutorial2);
            }
        }

        private void HandleTutorial3Clicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Tutorial Clicked");

            if(SinglePlayerClicked != null)
            {
                SinglePlayerClicked(BattleId.Tutorial3);
            }
        }

        private void HandleTutorial4Clicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Tutorial Clicked");

            if(SinglePlayerClicked != null)
            {
                SinglePlayerClicked(BattleId.Tutorial4);
            }
        }

        private void HandleRumbleButtonClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Rumble Clicked");

            if(RumbleClicked != null)
            {
				RumbleClicked( this, EventArgs.Empty );
            }
        }
            
        private void HandleChallengesButtonClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Challenges Clicked");

            if(ChallengesClicked != null)
            {
                ChallengesClicked();
            }
        }

        private void HandleLootRunsButtonClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Loot Runs Clicked");

            if(LootRunsClicked != null)
            {
                LootRunsClicked();
            }
        }
            
        private void HandleLeaderboardsButtonClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Leaderboards Clicked");

            if(LeaderboardsClicked != null)
            {
                LeaderboardsClicked();
            }
        }

        public void OnMultiplayerClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Multiplayer Clicked");

            if(MultiplayerClicked != null)
            {
				MultiplayerClicked( this, EventArgs.Empty );
            }
        }

        public void FireMapModeClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Map Mode Clicked");

            if(MapModeClicked != null)
            {
				MapModeClicked( this, EventArgs.Empty );
            }
        }

        public void OnReplayClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Replay Clicked");

            if(ReplayClicked != null)
            {
				ReplayClicked( this, EventArgs.Empty );
            }
        }

        public void OnLogoutClicked()
        {
            SJLogger.LogMessage(MessageFilter.UI, "Logout Clicked");

            if(LogoutClicked != null)
            {
				LogoutClicked( this, EventArgs.Empty );
            }
        }
        #endregion
	}
	
}

