﻿using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using System;

namespace SmallJelly
{
    public class LeaderboardRowView : SJMonoBehaviour 
    {
        [SerializeField]
        private TextMeshProUGUI mRank;

        [SerializeField]
        private TextMeshProUGUI mDisplayName;

        [SerializeField]
        private TextMeshProUGUI mNumWins;

        [SerializeField]
        private TextMeshProUGUI mField1;

        [SerializeField]
        private TextMeshProUGUI mField2;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mRank != null, "mRank is not set in Inspector");
            SJLogger.AssertCondition(mDisplayName != null, "mDisplayName is not set in Inspector");
            SJLogger.AssertCondition(mNumWins != null, "mNumWins is not set in Inspector");
            SJLogger.AssertCondition(mField1 != null, "mField1 is not set in Inspector");
            SJLogger.AssertCondition(mField2 != null, "mField2 is not set in Inspector");
        }

        public void SetData(LeaderboardRowNumWins data)
        {
            mRank.text = data.Rank.ToString();
            mDisplayName.text = data.DisplayName;
            mNumWins.text = data.NumWins.ToString();
            mField1.text = data.Country;
            mField2.text = data.Time.ToShortDateString();
        }
    }
}
