﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    public class LeaderboardsMenuController : MenuController 
    {
        public event Action BackButtonClicked; 

        #region Inspector Variables
        [SerializeField]
        private Button mBackButton;

        [SerializeField]
        private GameObject mContentRoot;

        [SerializeField]
        private GameObject mSpinner;

        [SerializeField]
        private Toggle mRecentToggle;

        [SerializeField]
        private Toggle mAllTimeToggle;

        [SerializeField]
        private Toggle mAllClassesToggle;

        [SerializeField]
        private Toggle mAncientToggle;

        [SerializeField]
        private Toggle mEnforcerToggle;

        [SerializeField]
        private Toggle mSmugglerToggle;
        #endregion

        private GameObject mLeaderboardLinePrefab;
        private GameObject mLeaderboardLinePrefabPlayer;

        private TextMeshProUGUI mClassNeutralText;
        private TextMeshProUGUI mClassAncientText;
        private TextMeshProUGUI mClassSmugglerText;
        private TextMeshProUGUI mClassEnforcerText;

        private Color mHighlighted;
        private Color mDisabled;



        #region MonoBehaviour
        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mBackButton != null, "mBackButton is not set in Inspector");
            SJLogger.AssertCondition(mContentRoot != null, "mContentRoot is not set in Inspector");      
            SJLogger.AssertCondition(mSpinner != null, "mSpinner is not set in Inspector");
            SJLogger.AssertCondition(mRecentToggle != null, "mRecentToggle is not set in Inspector");
            SJLogger.AssertCondition(mAllTimeToggle != null, "mAllTimeToggle is not set in Inspector"); 

            SJLogger.AssertCondition(mAllClassesToggle != null, "mAllClassesToggle is not set in Inspector"); 
            SJLogger.AssertCondition(mAncientToggle != null, "mAncientToggle is not set in Inspector"); 
            SJLogger.AssertCondition(mEnforcerToggle != null, "mEnforcerToggle is not set in Inspector"); 
            SJLogger.AssertCondition(mSmugglerToggle != null, "mSmugglerToggle is not set in Inspector");                      

            mClassNeutralText = mAllClassesToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassAncientText = mAncientToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassSmugglerText = mSmugglerToggle.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassEnforcerText = mEnforcerToggle.GetComponentsInChildren<TextMeshProUGUI>().First();

            mLeaderboardLinePrefab = this.LoadPrefab(FileLocations.LeaderboardsLinePrefab);
            mLeaderboardLinePrefabPlayer = this.LoadPrefab(FileLocations.LeaderboardsPlayerLinePrefab);
            SJLogger.AssertCondition(mLeaderboardLinePrefab != null, "mLeaderboardLinePrefab failed to load");
            SJLogger.AssertCondition(mLeaderboardLinePrefabPlayer != null, "mLeaderboardLinePrefabPlayer failed to load");

            mHighlighted = new Color(1, 1, 1);
            mDisabled = new Color(0.5f, 0.5f, 0.5f);

            RefreshContent();
        }

        private void OnEnable()
        {
            mBackButton.onClick.AddListener(HandleBackButtonClicked);

            mRecentToggle.onValueChanged.AddListener(HandleToggleValueChanged);
            mAllTimeToggle.onValueChanged.AddListener(HandleToggleValueChanged);

            mAllClassesToggle.onValueChanged.AddListener(HandleToggleValueChanged);
            mAncientToggle.onValueChanged.AddListener(HandleToggleValueChanged);
            mEnforcerToggle.onValueChanged.AddListener(HandleToggleValueChanged);
            mSmugglerToggle.onValueChanged.AddListener(HandleToggleValueChanged);

            // Refresh the data used on the main screen as the player will now know their most recent position
            LeaderboardManager.Get.SetLeaderboardPositionNeedsRefresh();
        }

        private void OnDisable()
        {
            mRecentToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);
            mAllTimeToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);

            mAllClassesToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);
            mAncientToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);
            mEnforcerToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);
            mSmugglerToggle.onValueChanged.RemoveListener(HandleToggleValueChanged);

            mBackButton.onClick.RemoveListener(HandleBackButtonClicked);
        }
        #endregion                           

        private void HandleToggleValueChanged(bool value)
        {
            if(value)
            {                
                RefreshContent();
            }
        }

        private void SetClassToggleTextColour()
        {
            if(mAllClassesToggle.isOn)
            {
                mClassNeutralText.color = mHighlighted;
                mClassAncientText.color = mDisabled;
                mClassEnforcerText.color = mDisabled;
                mClassSmugglerText.color = mDisabled;
            }
            else if(mSmugglerToggle.isOn)
            {
                mClassNeutralText.color = mDisabled;
                mClassAncientText.color = mDisabled;
                mClassEnforcerText.color = mDisabled;
                mClassSmugglerText.color = mHighlighted;
            }
            else if(mAncientToggle.isOn)
            {
                mClassNeutralText.color = mDisabled;
                mClassAncientText.color = mHighlighted;
                mClassEnforcerText.color = mDisabled;
                mClassSmugglerText.color = mDisabled; 

            }
            else if(mEnforcerToggle.isOn)
            {
                mClassNeutralText.color = mDisabled;
                mClassAncientText.color = mDisabled;
                mClassEnforcerText.color = mHighlighted;
                mClassSmugglerText.color = mDisabled;
            }
        }           

        private void RefreshContent()
        {
            SetClassToggleTextColour();

            mSpinner.SetActive(true);

            string leaderboardName = GetLeaderboardName();

            SJLogger.LogMessage(MessageFilter.Gameplay, "Querying leaderboard {0}", leaderboardName);

            if(!string.IsNullOrEmpty(leaderboardName))
            {
                ClientLeaderboard.AroundMe(leaderboardName, 50, HandleContentRefresh, HandleContentError);
            }
        }
            
        private void HandleContentRefresh(List<LeaderboardRowNumWins> list)
        {            
            RemoveExistingContent();

            mSpinner.SetActive(false);

            List<GameObject> loadedObjects = new List<GameObject>();

            foreach(LeaderboardRowNumWins data in list)
            {
                if(string.IsNullOrEmpty(data.DisplayName)) 
                {
                    continue;
                }
                    
                GameObject instantiated;

                if(data.UserId == ClientAuthentication.PlayerAccountDetails.UserId)
                {
                    instantiated = InstantiatePrefab(mLeaderboardLinePrefabPlayer, loadedObjects, mContentRoot);
                }
                else
                {
                    instantiated = InstantiatePrefab(mLeaderboardLinePrefab, loadedObjects, mContentRoot);
                }

                LeaderboardRowView view = instantiated.GetComponent<LeaderboardRowView>();
                view.SetData(data);
            }                
        }

        private void HandleContentError(ServerError error)
        {
            RemoveExistingContent();

            if(error.ErrorType == ServerErrorType.NoEntryForPlayerInLeaderboard)
            {
                // The player doesn't yet have an entry in this leaderboard.  Show the top results instead.
                Debug.Log("Quering top of leaderboard instead");
                ClientLeaderboard.Top(GetLeaderboardName(), 50, HandleContentRefresh, HandleContentError);
                return;
            }
                 
            mSpinner.SetActive(false);
            Debug.LogError(error.ToString());
        }

        private void RemoveExistingContent()
        {
            var children = new List<GameObject>();
            foreach(Transform child in mContentRoot.transform) 
            {
                children.Add(child.gameObject);
            }

            children.ForEach(child => Destroy(child));
        }

        private string GetLeaderboardName()
        {
            if(mRecentToggle.isOn) 
            {
                return GetAllWinsRecentLeaderboardName();
            }
            else
            {
                return GetAllWinsLeaderboardName();
            }
        }

        private string GetAllWinsLeaderboardName()
        {
            if(mSmugglerToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.Smuggler);
            }
            if(mEnforcerToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.Enforcer);
            }
            if(mAncientToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.Ancient);
            }
            else
            {                
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.All);
            }
        }

        private string GetAllWinsRecentLeaderboardName()
        {
            if(mSmugglerToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.SmugglerRecent);
            }
            if(mEnforcerToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.EnforcerRecent);
            }
            if(mAncientToggle.isOn)
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.AncientRecent);
            }
            else
            {
                return LeaderboardManager.Get.GetShortCodeForLeaderboard(LeaderboardId.AllRecent);
            }         
        }

        private void HandleBackButtonClicked()
        {
            if(BackButtonClicked != null)
            {
                BackButtonClicked();
            }
        }
    }
}

