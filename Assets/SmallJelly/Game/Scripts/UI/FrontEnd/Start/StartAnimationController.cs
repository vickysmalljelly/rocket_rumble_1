﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class StartAnimationController : FrontEndMenuAnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/FrontEnd/Start/"; } }
		#endregion
	}
}