using System;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Handles input from the StartMenu
	/// </summary>
	public class FrontEndNavigationController : MenuController 
	{
		#region Public Event Handlers
		public EventHandler RumbleClicked;
		public EventHandler CollectionClicked;
		public EventHandler ShopClicked;
		public EventHandler InboxClicked;
		public EventHandler SettingsClicked;
		#endregion

		#region Public Properties
		public Button RumbleButton
		{
			get
			{
				return mRumbleButton;
			}
		}

		public Button CollectionButton
		{
			get
			{
				return mCollectionButton;
			}
		}

        public GameObject CollectionRedDot
        {
            get
            {
                return mCardsRedDotAnimator.gameObject;
            }
        }

		public Button ShopButton
		{
			get
			{
				return mShopButton;
			}
		}

        public GameObject ShopRedDot
        {
            get
            {
                return mShopRedDotAnimator.gameObject;
            }
        }

		public Button InboxButton
		{
			get
			{
				return mInboxButton;
			}
		}

        public GameObject InboxRedDot
        {
            get
            {
                return mInboxRedDotAnimator.gameObject;
            }
        }

		public Button SettingsButton
		{
			get
			{
				return mSettingsButton;
			}
		}
		#endregion

        #region Exposed To Inspector
		[SerializeField]
		private Button mRumbleButton;

		[SerializeField]
		private Button mCollectionButton;

		[SerializeField]
		private Button mShopButton;

		[SerializeField]
		private Button mInboxButton;

		[SerializeField]
		private Button mSettingsButton;

        [SerializeField]
        private TextMeshProUGUI mCreditsCounter;

        [SerializeField]
        private TextMeshProUGUI mCrystalsCounter;

        [SerializeField]
        private GameObject mCrystalsUI;

        [SerializeField]
        private GameObject mCrystalsBuyButton;

        [SerializeField]
        private Animator mInboxRedDotAnimator;

        [SerializeField]
        private Animator mCardsRedDotAnimator;

        [SerializeField]
        private Animator mShopRedDotAnimator;
        #endregion

        private const string mOffAnimName = "Off";
        private const string mOnAnimName = "On";

        #region MonoBehaviour Methods
        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mCreditsCounter != null, "Credits counter must be set in the inspector");
            SJLogger.AssertCondition(mCrystalsCounter != null, "Crystals counter must be set in the inspector");
            SJLogger.AssertCondition(mCrystalsUI != null, "mCrystalsUI must be set in the inspector");
            SJLogger.AssertCondition(mCrystalsBuyButton != null, "mCrystalsBuyButton must be set in the inspector");

            SJLogger.AssertCondition(mInboxRedDotAnimator != null, "mInboxRedDotAnimator must be set in the inspector");
            SJLogger.AssertCondition(mCardsRedDotAnimator != null, "mCardsRedDotAnimator must be set in the inspector");
            SJLogger.AssertCondition(mShopRedDotAnimator != null, "mShopRedDotAnimator must be set in the inspector");           

            Button button =  mCrystalsBuyButton.GetComponent<Button>();
            button.onClick.AddListener(HandleShopButtonClicked);

            // Initialise state of new indicators
            if(GameManager.Get.PlayerData != null)
            {
                if(GameManager.Get.PlayerData.NewInboxItem)
                {
                    HandleNewInboxItem();
                }

                if(GameManager.Get.PlayerData.NewCards)
                {
                    HandleNewCardsAdded();
                }
            }
        }

        private void OnDestroy()
        {
            Button button =  mCrystalsBuyButton.GetComponent<Button>();
            button.onClick.RemoveListener(HandleShopButtonClicked);
            
            if(GameManager.Get != null)
            {
                GameManager.Get.CreditsUpdated -= HandleCreditsUpdated;
                GameManager.Get.CrystalsUpdated -= HandleCrystalsUpdated;
            }
        }

        private void OnEnable()
        {
			//TODO - Look at a proper solution to this - but we reset the navigation controller to its origin whenever it's enabled
			//incase it's been moved elsewhere (as is the case during the inbox onboarding)
			transform.localPosition = Vector3.zero;

			RegisterButtonListeners();
            RegisterEventListeners();
        }
            
        private void OnDisable()
        {
            UnregisterEventListeners();
			UnregisterButtonListeners();
        }
        #endregion

        public void ListenToCreditsUpdated()
        {
            GameManager.Get.CreditsUpdated += HandleCreditsUpdated;
            GameManager.Get.CrystalsUpdated += HandleCrystalsUpdated;
        }

        public void ToggleCrystalsCounter(bool enable)
        {
            mCrystalsUI.gameObject.SetActive(enable);
        }

        public void ToggleCrystalsBuyButton(bool enable)
        {
            mCrystalsBuyButton.gameObject.SetActive(enable);
        }

		#region Event Registration
        private void RegisterEventListeners()
        {
            GameManager.Get.NewInboxItemAdded += HandleNewInboxItem;
            GameManager.Get.NewInboxItemCleared += HandleNewInboxItemCleared;
            GameManager.Get.NewCardsAdded += HandleNewCardsAdded;
            GameManager.Get.NewCardsCleared += HandleNewCardsCleared;
            GameManager.Get.NewShopItem += HandleNewShopItem;
            GameManager.Get.NewShopItemCleared += HandleNewShopItemCleared;
        }            
            
        private void UnregisterEventListeners()
        {
            if(GameManager.Get != null)
            {
                GameManager.Get.NewInboxItemAdded -= HandleNewInboxItem;
                GameManager.Get.NewInboxItemCleared -= HandleNewInboxItemCleared;
                GameManager.Get.NewCardsAdded -= HandleNewCardsAdded;
                GameManager.Get.NewCardsCleared -= HandleNewCardsCleared;
                GameManager.Get.NewShopItem -= HandleNewShopItem;
                GameManager.Get.NewShopItemCleared -= HandleNewShopItemCleared;
            }
        }

		private void RegisterButtonListeners()
		{
            SJLogger.LogMessage(MessageFilter.UI, "Adding listeners for nav buttons");
			mRumbleButton.onClick.AddListener( HandleRumbleButtonClicked );
			mCollectionButton.onClick.AddListener( HandleCollectionButtonClicked );
			mShopButton.onClick.AddListener( HandleShopButtonClicked );
			mInboxButton.onClick.AddListener( HandleInboxButtonClicked );
			mSettingsButton.onClick.AddListener( HandleSettingsButtonClicked );
		}

		private void UnregisterButtonListeners()
		{
            SJLogger.LogMessage(MessageFilter.UI, "Removing listeners for nav buttons");
			mRumbleButton.onClick.RemoveListener( HandleRumbleButtonClicked );
			mCollectionButton.onClick.RemoveListener( HandleCollectionButtonClicked );
			mShopButton.onClick.RemoveListener( HandleShopButtonClicked );
			mInboxButton.onClick.RemoveListener( HandleInboxButtonClicked );
			mSettingsButton.onClick.RemoveListener( HandleSettingsButtonClicked );
		}
		#endregion
     
		#region Event Handlers
        private void HandleNewShopItem()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning on new shop notification");
            AnimatorStateInfo info =  mShopRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOnAnimName)) 
            {
                mShopRedDotAnimator.Play("TurnOn");
            }
        }

        private void HandleNewShopItemCleared()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning off new shop notification");
            AnimatorStateInfo info =  mShopRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOffAnimName)) 
            {
                mShopRedDotAnimator.Play("TurnOff");
            }
        }

        private void HandleNewCardsAdded()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning on new cards notification");

            AnimatorStateInfo info =  mCardsRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOnAnimName)) 
            {
                mCardsRedDotAnimator.Play("TurnOn");
            }
        }

        private void HandleNewCardsCleared()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning off new cards notification");
            AnimatorStateInfo info =  mCardsRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOffAnimName)) 
            {
                mCardsRedDotAnimator.Play("TurnOff");
            }
        }

        private void HandleNewInboxItem()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning on Inbox new notification");
            AnimatorStateInfo info =  mInboxRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOnAnimName)) 
            {
                mInboxRedDotAnimator.Play("TurnOn");
            }
        }

        private void HandleNewInboxItemCleared()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turning off Inbox new notification");
            AnimatorStateInfo info =  mInboxRedDotAnimator.GetCurrentAnimatorStateInfo(0);
            if(!info.IsName(mOffAnimName)) 
            {
                mInboxRedDotAnimator.Play("TurnOff");
            }
        }

        private void HandleCreditsUpdated(long value)
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Updating credits counter to {0}", value);
            mCreditsCounter.text = value.ToString();
        }

        private void HandleCrystalsUpdated(long value)
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Updating crystals counter to {0}", value);
            mCrystalsCounter.text = value.ToString();
        }

		private void HandleRumbleButtonClicked()
		{
            SJLogger.LogMessage(MessageFilter.UI, "clicked");
			if( RumbleClicked != null )
			{
				RumbleClicked( this, EventArgs.Empty );
			}
		}

		private void HandleCollectionButtonClicked()
		{
            SJLogger.LogMessage(MessageFilter.UI, "clicked");
			if( CollectionClicked != null )
			{
				CollectionClicked( this, EventArgs.Empty );
			}
		}

		private void HandleShopButtonClicked()
		{
            SJLogger.LogMessage(MessageFilter.UI, "clicked");
			if( ShopClicked != null )
			{
				ShopClicked( this, EventArgs.Empty );
			}
		}

		private void HandleInboxButtonClicked()
		{
            SJLogger.LogMessage(MessageFilter.UI, "clicked");
			if( InboxClicked != null )
			{
				InboxClicked( this, EventArgs.Empty );
			}
		}

		private void HandleSettingsButtonClicked()
		{
            SJLogger.LogMessage(MessageFilter.UI, "clicked");
			if( SettingsClicked != null )
			{
				SettingsClicked( this, EventArgs.Empty );
			}
		}
		#endregion
	}
	
}

