﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public abstract class FrontEndMenuAnimationController : AnimationController
	{

		#region Public Constants
		private const string MOVING_IN_ANIMATION_TEMPLATE_NAME = "MovingInAnimation";
		private const string MOVING_OUT_ANIMATION_TEMPLATE_NAME = "MovingOutAnimation";
		#endregion

		#region Public Methods
		public virtual void UpdateAnimations( FrontEndMenuController.State state )
		{
			StopExistingAnimations();

			switch( state )
			{
				case FrontEndMenuController.State.MovingIn:
					PlayAnimation( MOVING_IN_ANIMATION_TEMPLATE_NAME );
					break;

				case FrontEndMenuController.State.MovingOut:
					PlayAnimation( MOVING_OUT_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}