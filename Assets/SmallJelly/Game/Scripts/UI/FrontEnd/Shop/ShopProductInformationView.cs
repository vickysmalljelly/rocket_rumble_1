﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	public class ShopProductInformationView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mShopProductInformationAnimationController.AnimationFinished += value;
			}

			remove
			{
				mShopProductInformationAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private ShopProductInformationAnimationController mShopProductInformationAnimationController;

		[SerializeField]
		private TextMeshProUGUI mProductTitle;

		[SerializeField]
		private Text mProductDescription;

		[SerializeField]
		private Text mSelectACurrencyTitle;

		[SerializeField]
		private Button mCashButton;

		[SerializeField]
		private Button mCreditsButton;

		[SerializeField]
		private Button mCrystalButton;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
		}

		private void OnDisable()
		{
		}
		#endregion

		#region Public Methods
		public void OnPlayAnimation( ShopProductInformationController.State state, AnimationMetaData animationMetaData )
		{
			mShopProductInformationAnimationController.UpdateAnimations( state, animationMetaData );
		}

		public void OnRefreshData( IapData iapData )
		{
			mProductTitle.text = iapData.Name;
			mProductDescription.text = iapData.Description;

			int availableCurrencyOptions = 0;
			if( iapData.HasPriceInCredits() )
			{
				availableCurrencyOptions++;

				mCreditsButton.gameObject.SetActive( true );
				mCreditsButton.GetComponentInChildren< Text >().text = string.Format("{0}", iapData.GetDisplayPriceInCredits() );
			}
			else
			{
				mCreditsButton.gameObject.SetActive( false );
			}

			if( iapData.HasPriceInCrystals() )
			{
				availableCurrencyOptions++;

				mCrystalButton.gameObject.SetActive( true );
				mCrystalButton.GetComponentInChildren< Text >().text = string.Format( "{0}", iapData.GetDisplayPriceInCrystals() );
			}
			else
			{
				mCrystalButton.gameObject.SetActive( false );
			}

			if( iapData.HasPriceInCash() )
			{
				availableCurrencyOptions++;

				mCashButton.gameObject.SetActive( true );
				mCashButton.GetComponentInChildren< Text >().text = string.Format( "{0}", iapData.GetPriceInCash() );
			}
			else
			{
				mCashButton.gameObject.SetActive( false );
			}

			mSelectACurrencyTitle.gameObject.SetActive( availableCurrencyOptions > 1 );
		}
		#endregion
	}
}