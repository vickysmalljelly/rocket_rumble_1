﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ShopProductInformationAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return string.Empty; } }
		#endregion

		#region Methods
		public void UpdateAnimations( ShopProductInformationController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );
		}
		#endregion
	}
}
