using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
	/// <summary>
	/// Handles input from the Shop
	/// </summary>
	public class ShopMenuController : FrontEndMenuController 
	{
		#region Public Events
		public event EventHandler< ShopItemAndStateTransitionEventArgs > ShopItemStateTransitioned;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private ShopItem[] mShopItems;

        [SerializeField]
        private GameObject mPurchaseInProgressSpinner;
		#endregion

		#region MonoBehaviour Methods
        protected override void Awake()
        {           
            base.Awake();

            SJLogger.AssertCondition(mPurchaseInProgressSpinner != null, "mPurchaseInProgressSpinner not set in inspector");
        }

        public override void OnEnable()
        {
            base.OnEnable();

			RegisterListeners();
        }

        public override void OnDisable()
        {
            base.OnDisable();

			UnregisterListeners();
        }
		#endregion

		#region Public Methods
		public void OnRefreshData( List< IapData > iapData )
		{
			foreach( IapData data in iapData )
			{
				ShopItem shopItem = mShopItems.FirstOrDefault( x => x.ShortCode == data.ShortCode );

				if( shopItem == null )
				{
					SJLogger.LogWarning( "No shop item UI could be found for the product " + data.ShortCode ); 
					continue;
				}

				shopItem.OnRefreshData( data );
			}
		}

        public void TogglePurchaseInProgressSpinner(bool enable)
        {
            mPurchaseInProgressSpinner.SetActive(enable);
        }
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			foreach( ShopItem shopItem in mShopItems )
			{
				shopItem.StateTransitioned += HandleStateTransitioned;
			}
		}

		private void UnregisterListeners()
		{
			foreach( ShopItem shopItem in mShopItems )
			{
				shopItem.StateTransitioned -= HandleStateTransitioned;
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, ShopItemAndStateTransitionEventArgs shopItemAndStateTransitionEventArgs )
		{
			
			switch( shopItemAndStateTransitionEventArgs.NewState )
			{
				case ShopItemController.State.Selected:
					shopItemAndStateTransitionEventArgs.ShopItem.OnInsertState( ShopItemController.State.Idle );

					FireShopItemStateTransitioned( shopItemAndStateTransitionEventArgs.ShopItem, shopItemAndStateTransitionEventArgs.PreviousState, shopItemAndStateTransitionEventArgs.NewState );
					break;
			}
		}
		#endregion

		#region Event Firing
		private void FireShopItemStateTransitioned( ShopItem collectionItem, ShopItemController.State previousState, ShopItemController.State newState )
		{
			if( ShopItemStateTransitioned != null )
			{
				ShopItemStateTransitioned( this, new ShopItemAndStateTransitionEventArgs(collectionItem, previousState, newState ) );
			}
		}
		#endregion
	}
	
}

