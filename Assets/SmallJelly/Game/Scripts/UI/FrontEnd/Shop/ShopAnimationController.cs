﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShopAnimationController : FrontEndMenuAnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/FrontEnd/Shop/"; } }
		#endregion
	}
}