﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//Currently we have no animations for shop items but we may well do in the future
	public class ShopItemAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Shop/ShopItem/"; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string DOWNLOADING_ANIMATION_TEMPLATE_NAME = "DownloadingAnimation";
		private const string IDLE_ANIMATION_TEMPLATE_NAME = "IdleAnimation";
		#endregion

		#region Public Methods
		public void UpdateAnimations( ShopItemController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			switch( state )
			{
				case ShopItemController.State.Initialisation:
					PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case ShopItemController.State.Downloading:
					PlayAnimation( DOWNLOADING_ANIMATION_TEMPLATE_NAME );
					break;

				case ShopItemController.State.Idle:
					PlayAnimation( IDLE_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}