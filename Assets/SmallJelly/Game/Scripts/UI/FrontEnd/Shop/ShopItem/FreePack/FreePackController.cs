﻿using System;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    public class FreePackController : SJMonoBehaviour
    {
        private enum FreePackState
        {
            NotSet,
            CountingDown,
            Available
        }

        private string PrefabPath
        {
            get
            {
                return string.Format( "{0}/{1}", FileLocations.SmallJellyShopParticleEffectPrefabs, "pfx_awardFreePack");
            }
        }
        private FreePackState mState = FreePackState.NotSet;

        #region Fields set in Inspector
        [SerializeField]
        private TextMeshProUGUI mTimerText;

        [SerializeField]
        private GameObject mClaimNowObject;

        [SerializeField]
        private GameObject mTimerObject;

        [SerializeField]
        private Button mButton;
        #endregion

        #region MonoBehaviour Methods
        protected override void Awake()
        {           
            base.Awake();

            SJLogger.AssertCondition(mTimerText != null, "mTimerText not set in inspector");
            SJLogger.AssertCondition(mTimerObject != null, "mTimerObject not set in inspector");
            SJLogger.AssertCondition(mClaimNowObject != null, "mClaimNowObject not set in inspector");
            SJLogger.AssertCondition(mButton != null, "mButton not set in inspector");

            mClaimNowObject.SetActive(false);
            mTimerObject.SetActive(false);
            mTimerText.text = string.Empty;
        }
            
        private void OnEnable()
        {
            GameManager.Get.PlayerDataUpdated += HandlePlayerDataUpdated;
            UpdateState();
        }

        private void OnDisable()
        {
            if(GameManager.Get != null) 
            {               
                GameManager.Get.PlayerDataUpdated -= HandlePlayerDataUpdated;
            }
        }           

        private void Update()
        {
            UpdateState();
        }
        #endregion

        #region Events Attached in Inspector
        /// <summary>
        /// Attached in inspector
        /// </summary>
        public void HandleButtonClick()
        {
            ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
            shopMenuController.TogglePurchaseInProgressSpinner(true);

            ClientShop.ClaimFreePack(HandleSuccess, HandleError);
        }
        #endregion

        #region Private Methods
        private void UpdateState()
        {
            switch(mState)
            {
                case FreePackState.NotSet:
                    if(PackAvailable())
                    {
                        SetPackAvailable();
                    } 
                    else
                    {
                        SetCountingDown();
                    }
                    break;

                case FreePackState.CountingDown:
                    if(PackAvailable())
                    {
                        SetPackAvailable();
                    } 
                    else
                    {
                        UpdateTimerText();
                    }
                    break;
                case FreePackState.Available:
                    if(!PackAvailable())
                    {
                        SetCountingDown();
                    }
                    break;
                default:
                    break;
            }
        }

        private void HandleSuccess()
        {
            ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
            shopMenuController.TogglePurchaseInProgressSpinner(false);

            GameManager.Get.UpdateFreePackClaimedOnClient();

            PlayEffect();

            SetCountingDown();
        }

        private void HandleError(ServerError error)
        {
            ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
            shopMenuController.TogglePurchaseInProgressSpinner(false);

            switch(error.ErrorType)
            {
                case ServerErrorType.Timeout:
                    ShopManager.Get.FireShopConnectionFailed();
                    break;
                case ServerErrorType.NoFreePack:
                    ButtonData messageClose = new ButtonData("Ok", HandleMessageClose);
                    DialogManager.Get.ShowMessagePopup("Error", error.UserFacingMessage, messageClose);
                    break;
            }                
        }

        private void HandleMessageClose()
        {
            // No need to do anything
        }

        private void PlayEffect()
        {
            // Instantiate the prefab which displays the purchase effect - e.g. dragging a card pack to the inbox
            GameObject effectPrefab = Instantiate(Resources.Load< GameObject >(PrefabPath));

            // Set the game object from which the effect should originate
            PlayMakerSharedVariables psv = effectPrefab.GetComponent<PlayMakerSharedVariables>();
            psv.Add("from", new FsmGameObject(this.gameObject));
            psv.Add("numPacks", new FsmInt(1));
        }

        private void HandlePlayerDataUpdated(PlayerData obj)
        {
            // Force refresh of state with new data from server
            mState = FreePackState.NotSet;
            UpdateState();
        }

        private bool PackAvailable()
        {
            if(GameManager.Get.PlayerData != null)
            {
                if(GameManager.Get.PlayerData.FreePackAvailable)
                {
                    return true;
                }
            }

            return false;
        }

        private void SetPackAvailable()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Pack available");
            mState = FreePackState.Available;
            mClaimNowObject.SetActive(true);
            mTimerObject.SetActive(false);
            mButton.interactable = true;
        }

        private void SetCountingDown()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Counting down");
            mState = FreePackState.CountingDown;
            mClaimNowObject.SetActive(false);
            mTimerObject.SetActive(true);
            mButton.interactable = false;
        }

        private void UpdateTimerText()
        {
            if(GameManager.Get.PlayerData == null)
            {
                return;
            }

            TimeSpan timeLeft = GameManager.Get.PlayerData.TimeLeftUntilFreePack;
            string hours = timeLeft.Hours < 10 ? ("0" + timeLeft.Hours.ToString()) : timeLeft.Hours.ToString();
            string minutes = timeLeft.Minutes < 10 ? ("0" + timeLeft.Minutes.ToString()) : timeLeft.Minutes.ToString();
            string seconds = timeLeft.Seconds < 10 ? ("0" + timeLeft.Seconds.ToString()) : timeLeft.Seconds.ToString();
            mTimerText.text = string.Format("{0}:{1}:{2}", hours, minutes, seconds);
        }
        #endregion
    }
}
