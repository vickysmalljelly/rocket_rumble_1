﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System;

namespace SmallJelly
{
	public class ShopItemController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,
			Initialisation,
			Downloading,
			Idle,
			Selected
		}
		#endregion

		#region Exposed To Inspector
		//Display in the inspector
		[SerializeField]
		private State mState;
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished;

		public event EventHandler< ShopItemStateTransitionEventArgs > ShopItemStateUpdated;
		#endregion

		#region Public Properties
		public ShopItemView View
		{
			get
			{
				return GetComponent< ShopItemView >();
			}
		}
		#endregion

		#region Unity Methods
		public void OnEnable()
		{
			RegisterListeners();
		}

		public void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new System.Collections.Generic.Dictionary<string, NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			UpdateState( state, animationMetaData );
		}


		public void OnRefreshData( IapData iapData )
		{
			View.OnRefreshData( iapData );

			switch( mState )
			{
				case State.Downloading:
					OnInsertState( State.Idle );
					break;
			}
		}

		public void OnClicked()
		{
            // The shop item has been selected, set game object as current
            ShopManager.Get.CurrentShopItem = this.gameObject;

			OnInsertState( State.Selected );
		}
		#endregion

		#region Private Methods
		protected void UpdateState( State state, AnimationMetaData animationMetaData )
		{
			State previousState = mState;
			mState = state;

			View.OnPlayAnimation( state, animationMetaData );

			FireStateUpdated( previousState, state );
		}

		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			View.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			View.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			FireAnimationFinished();
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion


		#region Event Firing
		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( ShopItemStateUpdated != null )
			{
				ShopItemStateUpdated( this, new ShopItemStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion
	}
}