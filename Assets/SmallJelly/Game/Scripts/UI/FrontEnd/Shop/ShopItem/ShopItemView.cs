﻿using UnityEngine;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class ShopItemView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished;
		#endregion

		#region Constants
		protected const int MAXIMUM_TEXT_LENGTH = 20;
		#endregion

		#region Public Properties
		public ShopItemAnimationController AnimationController 
		{ 
			get
			{
				return GetComponent< ShopItemAnimationController >();
			}
		}
		#endregion

		#region Exposed To Inspector
        [SerializeField]
        private TextMeshProUGUI mCashText;

        [SerializeField]
        private TextMeshProUGUI mCreditsText;

        [SerializeField]
        private TextMeshProUGUI mCrystalsText;

        [SerializeField]
        private TextMeshProUGUI mDisplayName;
		#endregion

		#region Public Methods
		public void OnPlayAnimation( ShopItemController.State state, AnimationMetaData animationMetaData )
		{
			AnimationController.UpdateAnimations( state, animationMetaData );
		}

		public void OnRefreshData( IapData iapData )
		{
            mDisplayName.text = iapData.Name;

			if( iapData.HasPriceInCredits() )
			{				
                mCreditsText.text = iapData.GetDisplayPriceInCredits();			
			}

			if( iapData.HasPriceInCrystals() )
			{
                mCrystalsText.text = iapData.GetDisplayPriceInCrystals();
			}

			if( iapData.HasPriceInCash() )
			{
				mCashText.text = iapData.GetPriceInCash();
			}
		}
		#endregion

		#region Protected Mehtods
        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mDisplayName != null, "{0}: mDisplayName must be set in inspector", this.gameObject.name);
        }

		protected virtual void OnEnable()
		{
			RegisterAnimationListeners( AnimationController );
		}

		protected virtual void OnDisable()
		{
			UnregisterAnimationListeners( AnimationController );
		}

		protected void SetActiveRecursively(GameObject rootObject, bool active)
		{
			rootObject.SetActive(active);

			foreach (Transform childTransform in rootObject.transform)
			{
				SetActiveRecursively(childTransform.gameObject, active);
			}
		}
		#endregion

		#region Event Registration
		private void RegisterAnimationListeners( ShopItemAnimationController shopItemAnimationController )
		{
			shopItemAnimationController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterAnimationListeners( ShopItemAnimationController shopItemAnimationController )
		{
			shopItemAnimationController.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}
		#endregion
	}
}