﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;

namespace SmallJelly
{
	public class ShopItem : Targetable
	{
		#region Public Events
		public event EventHandler< ShopItemAndStateTransitionEventArgs > StateTransitioned;

		public event EventHandler AnimationFinished
		{
			add
			{
				ShopItemController.AnimationFinished += value;
			}

			remove
			{
				ShopItemController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public IapData Data
		{
			get;
			private set;
		}

		public string ShortCode
		{
			get
			{
				return mShortCode;
			}
		}

		public ShopItemController ShopItemController
		{
			get;
			set;
		}
			
		public ObjectInputListener ObjectInputListener { get; set; }
		#endregion

		#region Exposed To Inspector
		//TODO - We'll eventually refresh everything "live' so the shop items will be generated on the fly - we will then have no use for
		//product ids here - when that's the case we should take it out!
		[SerializeField]
		private string mShortCode;

		[SerializeField]
		private Button mButton;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			ShopItemController = GetComponent< ShopItemController >();

			OnInsertState( ShopItemController.State.Initialisation );
			OnInsertState( ShopItemController.State.Downloading );
		}

		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState(  ShopItemController.State state )
		{
			ShopItemController.OnInsertState( state );
		}

		public void OnRefreshData( IapData iapData )
		{
			Data = iapData;

			ShopItemController.OnRefreshData( iapData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ShopItemController.ShopItemStateUpdated += HandleStateUpdated;
		}

		private void UnregisterListeners()
		{
			ShopItemController.ShopItemStateUpdated -= HandleStateUpdated;
		}

		private void RegisterInputListeners()
		{
			mButton.onClick.AddListener( ShopItemController.OnClicked );
		}

		private void UnregisterInputListeners()
		{
			mButton.onClick.RemoveListener( ShopItemController.OnClicked );
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( ShopItem collectionItem, ShopItemController.State previousState, ShopItemController.State newState )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new ShopItemAndStateTransitionEventArgs(collectionItem, previousState, newState ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateUpdated( object o, ShopItemStateTransitionEventArgs shopItemStateEventArgs )
		{
			FireStateTransitioned( this, shopItemStateEventArgs.PreviousState, shopItemStateEventArgs.NewState );
		}
		#endregion

	}
}