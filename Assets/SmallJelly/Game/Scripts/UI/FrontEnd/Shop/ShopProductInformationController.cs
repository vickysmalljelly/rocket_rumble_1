﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class ShopProductInformationController : SJMonoBehaviour
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			PurchaseUsingCredits,
			PurchaseUsingCrystals,
			PurchaseUsingCash,

			Back
		}
		#endregion

		#region Public Events
		public event EventHandler< ShopProductInformationStateTransitionEventArgs > StateTransitioned;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private ShopProductInformationView mShopProductInformationView;
		#endregion

		#region Member Variables
		private State mState;
		#endregion

		#region Public Methods
		public void OnRefreshData( IapData iapData )
		{
			mShopProductInformationView.OnRefreshData( iapData );
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, NamedVariable>() ) );

			FireStateTransitioned( mState, state );

			mState = state;
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mShopProductInformationView.OnPlayAnimation( state, animationMetaData );
		}

		public void OnCreditsButtonClicked()
		{
			OnInsertState( State.PurchaseUsingCredits );
		}

		public void OnCrystalButtonClicked()
		{
			OnInsertState( State.PurchaseUsingCrystals );
		}

		public void OnCashButtonClicked()
		{
			OnInsertState( State.PurchaseUsingCash );
		}

		public void OnBackButtonClicked()
		{
			OnInsertState( State.Back );
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( State previousBattleEntityState, State newBattleEntityState )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new ShopProductInformationStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion
	}
}