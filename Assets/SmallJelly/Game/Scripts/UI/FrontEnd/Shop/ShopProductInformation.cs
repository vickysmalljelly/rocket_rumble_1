﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace SmallJelly
{
    /// <summary>
    /// Displays information about the selected product.
    /// </summary>
	public class ShopProductInformation : MenuController
	{
		#region Public Events
		public event EventHandler< ShopProductInformationAndStateTransitionEventArgs > StateTransitioned;
		#endregion

		#region Public Properties
		public IapData Data { get; private set; }
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private ShopProductInformationController mShopProductInformationController;

		[SerializeField]
		private Button mCashButton;

		[SerializeField]
		private Button mCreditsButton;

		[SerializeField]
		private Button mCrystalButton;

		[SerializeField]
		private Button mBackButton;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( ShopProductInformationController.State state )
		{
			mShopProductInformationController.OnInsertState( state );
		}

		public void OnRefreshData( IapData iapData )
		{
			Data = iapData;

			mShopProductInformationController.OnRefreshData( iapData );
		}
		#endregion

		#region Private Methods
		private void RegisterListeners()
		{
			mShopProductInformationController.StateTransitioned += HandleStateTransitioned;
		}

		private void UnregisterListeners()
		{
			mShopProductInformationController.StateTransitioned -= HandleStateTransitioned;
		}

		private void RegisterInputListeners()
		{
			if( mCashButton != null )
			{
				mCashButton.onClick.AddListener( mShopProductInformationController.OnCashButtonClicked );
			}

			if( mCreditsButton != null )
			{
				mCreditsButton.onClick.AddListener( mShopProductInformationController.OnCreditsButtonClicked );
			}

			if( mCrystalButton != null )
			{
				mCrystalButton.onClick.AddListener( mShopProductInformationController.OnCrystalButtonClicked );
			}

			if( mBackButton != null )
			{
				mBackButton.onClick.AddListener( mShopProductInformationController.OnBackButtonClicked );
			}
		}

		private void UnregisterInputListeners()
		{
			if( mCashButton != null )
			{
				mCashButton.onClick.RemoveListener( mShopProductInformationController.OnCashButtonClicked );
			}

			if( mCreditsButton != null )
			{
				mCreditsButton.onClick.RemoveListener( mShopProductInformationController.OnCreditsButtonClicked );
			}

			if( mCrystalButton != null )
			{
				mCrystalButton.onClick.RemoveListener( mShopProductInformationController.OnCrystalButtonClicked );
			}

			if( mBackButton != null )
			{
				mBackButton.onClick.RemoveListener( mShopProductInformationController.OnBackButtonClicked );
			}
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( ShopProductInformation shopProductInformation, ShopProductInformationController.State previousState, ShopProductInformationController.State newState )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new ShopProductInformationAndStateTransitionEventArgs( shopProductInformation, previousState, newState ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, ShopProductInformationStateTransitionEventArgs shopProductInformationStateTransitionEventArgs )
		{
			FireStateTransitioned( this, shopProductInformationStateTransitionEventArgs.PreviousState, shopProductInformationStateTransitionEventArgs.NewState );
		}
		#endregion
	}
}