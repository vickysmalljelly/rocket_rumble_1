using System;
using System.Collections.Generic;
using System.Linq;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SmallJelly
{
	public class ChooseCustomDeckDisplayController : MenuController 
	{
		#region Public Event Handlers
		public event EventHandler PressedBack;
		public event EventHandler< StringEventArgs > PressedCustomDeck;
		#endregion

		#region Exposed To Inspector
        [SerializeField]
        private TextMeshProUGUI mPlayerNameText;

        [SerializeField]
        private Image mRankInsignia;

        [SerializeField]
        private GameObject mStar1Off;

        [SerializeField]
        private GameObject mStar2Off;

        [SerializeField]
        private GameObject mStar3Off;

        [SerializeField]
        private GameObject mStar4Off;

        [SerializeField]
        private GameObject mStar1On;

        [SerializeField]
        private GameObject mStar2On;

        [SerializeField]
        private GameObject mStar3On;

        [SerializeField]
        private GameObject mStar4On;

		[SerializeField]
		private Transform mButtonPanelViewport;

		[SerializeField]
		private Toggle mSmugglerButton;

		[SerializeField]
		private Toggle mEnforcerButton;

		[SerializeField]
		private Toggle mAncientButton;

		[SerializeField]
		private UIButton mLeftButton;

		[SerializeField]
		private UIButton mRightButton;

		[SerializeField]
		private Button mStartButton;

		[SerializeField]
		private Button mBackButton;

		[SerializeField]
		private ChooseShip mChooseShip;

		[SerializeField]
		private ChoosePilot mChoosePilot;
		#endregion

		#region Member Variables
		private Deck[] mDecks;
		private List< UIButton > mDeckSelectionButtons;

        private TextMeshProUGUI mClassAncientText;
        private TextMeshProUGUI mClassSmugglerText;
        private TextMeshProUGUI mClassEnforcerText;

        private Color mHighlighted;
        private Color mDisabled;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

            SJLogger.AssertCondition( mPlayerNameText != null, "mPlayerNameText has not been set in the inspector" );

            SJLogger.AssertCondition( mStar1Off != null, "mStar1Off has not been set in the inspector" );
            SJLogger.AssertCondition( mStar2Off != null, "mStar2Off has not been set in the inspector" );
            SJLogger.AssertCondition( mStar3Off != null, "mStar3Off has not been set in the inspector" );
            SJLogger.AssertCondition( mStar4Off != null, "mStar4Off has not been set in the inspector" );

            SJLogger.AssertCondition( mStar1On != null, "mStar1On has not been set in the inspector" );
            SJLogger.AssertCondition( mStar2On != null, "mStar2On has not been set in the inspector" );
            SJLogger.AssertCondition( mStar3On != null, "mStar3On has not been set in the inspector" );
            SJLogger.AssertCondition( mStar4On != null, "mStar4On has not been set in the inspector" );

            mClassAncientText = mAncientButton.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassSmugglerText = mSmugglerButton.GetComponentsInChildren<TextMeshProUGUI>().First();
            mClassEnforcerText = mEnforcerButton.GetComponentsInChildren<TextMeshProUGUI>().First();

            mHighlighted = new Color(1, 1, 1);
            mDisabled = new Color(0.5f, 0.5f, 0.5f);

            mClassAncientText.color = mDisabled;
            mClassSmugglerText.color = mHighlighted;
            mClassEnforcerText.color = mDisabled;

			mChooseShip.OnInsertState( ChooseShipController.State.Initialisation );
			mChoosePilot.OnInsertState( ChoosePilotController.State.Initialisation );

			mDecks = new Deck[ 0 ];
			mDeckSelectionButtons = new List< UIButton >();
		}

		private void OnEnable()
		{
			RegisterListeners();

            SetPlayerDetails();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
        public void Refresh( Deck[] decks )
		{
            mDecks = decks;          

            if(string.IsNullOrEmpty(GameManager.Get.CurrentShipClass))
            {
                // Default to smuggler
                GameManager.Get.CurrentShipClass = ShipClass.Smuggler;
            } 

            RefreshClassButtons();

			//Remove all existing buttons
			while( mDeckSelectionButtons.Count > 0 )
			{
				RemoveButton( mDeckSelectionButtons[ 0 ] );
			}                

            switch( GameManager.Get.CurrentShipClass )
			{
				case ShipClass.Ancient:
					mChooseShip.OnInsertState( ChooseShipController.State.Ancient );
					mChoosePilot.OnInsertState( ChoosePilotController.State.Ancient );
					break;

				case ShipClass.Smuggler:
					mChooseShip.OnInsertState( ChooseShipController.State.Smuggler );
					mChoosePilot.OnInsertState( ChoosePilotController.State.Smuggler );
					break;

				case ShipClass.Enforcer:
					mChooseShip.OnInsertState( ChooseShipController.State.Enforcer );
					mChoosePilot.OnInsertState( ChoosePilotController.State.Enforcer );
					break;
			}

            Deck[] currentClassDecks = mDecks.Where( x => x.Class == GameManager.Get.CurrentShipClass ).ToArray();    

            if(!string.IsNullOrEmpty(GameManager.Get.GetCurrentDeckId()))
            {
                // Check the deck exists in the list
                Deck[] currentDeckArray = currentClassDecks.Where( x => x.DeckId == GameManager.Get.GetCurrentDeckId() ).ToArray();

                if(currentDeckArray.Length == 0)
                {
                    // Current deck no longer exists
                    GameManager.Get.SetCurrentDeckId(GameManager.Get.CurrentShipClass, string.Empty);
                }                    
            }

            if(string.IsNullOrEmpty(GameManager.Get.GetCurrentDeckId()))
            {
                // Default to the first deck in the list
                GameManager.Get.SetCurrentDeckId(GameManager.Get.CurrentShipClass, currentClassDecks[0].DeckId);
            }

			//Add buttons associated with the supplied names
			for( int i = 0; i < currentClassDecks.Length; i++ )
			{
				Deck deck = currentClassDecks[ i ];

                UIButton uiButton = UIButtonFactory.ConstructDeckSelectionButton( deck.Class, deck.DeckId, deck.DisplayName );
				AddButton( uiButton, mButtonPanelViewport );

				//If not the deck is not selected then make the button semi transparent
                if(deck.DeckId != GameManager.Get.GetCurrentDeckId())
				{
					Image image = uiButton.gameObject.GetComponent< Image >();
					image.color = new Color( 1, 1, 1, 0.5f );

					TextMeshProUGUI textMeshProUGUI = uiButton.gameObject.GetComponentInChildren< TextMeshProUGUI >();
					textMeshProUGUI.color = new Color( 1, 1, 1, 0.5f );
				}               
			}
		}
		#endregion

		#region Private Methods
        private void SetPlayerDetails() 
        {
            mPlayerNameText.text = ClientAuthentication.PlayerAccountDetails.DisplayName;         

            int numStars = GameManager.Get.PlayerData.StarsAtThisRank;
            int totalStars = GameManager.Get.PlayerData.TotalStarsAtThisRank;

            if(numStars >= 1)
            {
                mStar1On.SetActive(true);
                mStar1Off.SetActive(false);
            }
            else
            {
                mStar1On.SetActive(false);
                mStar1Off.SetActive(true);
            }

            if(totalStars < 2)
            {
                mStar2On.SetActive(false);
                mStar2Off.SetActive(false);
            }
            else if(numStars >= 2)
            {
                mStar2On.SetActive(true);
                mStar2Off.SetActive(false);
            }
            else
            {
                mStar2On.SetActive(false);
                mStar2Off.SetActive(true);
            }            

            if(totalStars < 3)
            {
                mStar3On.SetActive(false);
                mStar3Off.SetActive(false);
            }
            else if(numStars >= 3)
            {
                mStar3On.SetActive(true);
                mStar3Off.SetActive(false);
            }
            else
            {
                mStar3On.SetActive(false);
                mStar3Off.SetActive(true);
            }            

            if(totalStars < 4)
            {
                mStar4On.SetActive(false);
                mStar4Off.SetActive(false);
            }
            else if(numStars >= 4)
            {
                mStar4On.SetActive(true);
                mStar4Off.SetActive(false);
            }
            else
            {
                mStar4On.SetActive(false);
                mStar4Off.SetActive(true);
            }

            string spriteName = "Images/Ranks/ui_insignia_rank_" + GameManager.Get.PlayerData.RankNumber;
            Sprite sprite = Resources.Load<Sprite>(spriteName);
            SJLogger.AssertCondition(sprite != null, "Failed to load sprite {0}", spriteName);
            mRankInsignia.sprite = sprite;
        }

		private void AddButton( UIButton uiButton, Transform parent )
		{
			//Add button as the child of this object
			SJRenderTransformExtensions.AddChild( parent, uiButton.transform, true, false, true );

			RegisterButtonListeners( uiButton );
			mDeckSelectionButtons.Add( uiButton );
		}

		private void RemoveButton( UIButton uiButton ) 
		{
			mDeckSelectionButtons.Remove( uiButton );
			UnregisterButtonListeners( uiButton );

			//Destroy the button
			Destroy( uiButton.gameObject );
		}
		#endregion

		#region Event Firing
		public void FirePressedBack( )
		{
			if( PressedBack != null )
			{
				PressedBack( this, EventArgs.Empty );
			}
		}

		private void FirePickedRemoteDeck( string deckId )
		{
			if( PressedCustomDeck != null )
			{
				PressedCustomDeck( this, new StringEventArgs( deckId ) );
			}
		}
		#endregion

		#region Event Registration

		private void RegisterButtonListeners( UIButton uiButton )
		{
			uiButton.Clicked += HandleUseDeckButtonClicked;
		}

		private void UnregisterButtonListeners( UIButton uiButton )
		{
			uiButton.Clicked -= HandleUseDeckButtonClicked;
		}

		private void HandleUseDeckButtonClicked( object o, GameObjectEventArgs args )
		{
            DeckIdComponent deckId = args.GameObject.GetComponent<DeckIdComponent>();
            GameManager.Get.SetCurrentDeckId(GameManager.Get.CurrentShipClass, deckId.DeckId);

            Refresh( mDecks );
		}

		private void RegisterListeners()
		{
			mLeftButton.Clicked += HandleLeftButtonClicked;
			mRightButton.Clicked += HandleRightButtonClicked;

			mBackButton.onClick.AddListener( HandlePressedBack );
			mStartButton.onClick.AddListener( HandlePressedStart );

			mSmugglerButton.onValueChanged.AddListener( HandlePressedSmuggler );
			mEnforcerButton.onValueChanged.AddListener( HandlePressedEnforcer );
			mAncientButton.onValueChanged.AddListener( HandlePressedAncient );
		}

		private void UnregisterListeners()
		{
			mLeftButton.Clicked -= HandleLeftButtonClicked;
			mRightButton.Clicked -= HandleRightButtonClicked;

			mBackButton.onClick.RemoveListener( HandlePressedBack );
			mStartButton.onClick.RemoveListener( HandlePressedStart );

			mSmugglerButton.onValueChanged.RemoveListener( HandlePressedSmuggler );
			mEnforcerButton.onValueChanged.RemoveListener( HandlePressedEnforcer );
			mAncientButton.onValueChanged.RemoveListener( HandlePressedAncient );
		}
		#endregion

		#region Event Handlers
		private void HandlePressedSmuggler( bool enabled )
		{
			UnregisterListeners();

			if( enabled )
			{
                mClassAncientText.color = mDisabled;
                mClassSmugglerText.color = mHighlighted;
                mClassEnforcerText.color = mDisabled;

                GameManager.Get.CurrentShipClass = ShipClass.Smuggler;
                Refresh( mDecks );
			}

			RegisterListeners();
		}
            
		private void HandlePressedEnforcer( bool enabled )
		{
			UnregisterListeners();

			if( enabled )
			{
                mClassAncientText.color = mDisabled;
                mClassSmugglerText.color = mDisabled;
                mClassEnforcerText.color = mHighlighted;

                GameManager.Get.CurrentShipClass = ShipClass.Enforcer;
                Refresh( mDecks );
			}

			RegisterListeners();
		}

		private void HandlePressedAncient( bool enabled )
		{
			UnregisterListeners();

			if( enabled )
			{
                mClassAncientText.color = mHighlighted;
                mClassSmugglerText.color = mDisabled;
                mClassEnforcerText.color = mDisabled;

                GameManager.Get.CurrentShipClass = ShipClass.Ancient;
                Refresh( mDecks );
			}

			RegisterListeners();
		}

        private void RefreshClassButtons()
        {
            switch(GameManager.Get.CurrentShipClass)
            {
                case ShipClass.Ancient:
                    mAncientButton.isOn = true;
                    mSmugglerButton.isOn = false;
                    mEnforcerButton.isOn = false;
                    break;
                case ShipClass.Smuggler:
                    mSmugglerButton.isOn = true;
                    mAncientButton.isOn = false;                   
                    mEnforcerButton.isOn = false;
                    break;
                case ShipClass.Enforcer:
                    mEnforcerButton.isOn = true;
                    mSmugglerButton.isOn = false;
                    mAncientButton.isOn = false;
                    break;
                default:
                    SJLogger.LogError("Ship class {0} not recognised", GameManager.Get.CurrentShipClass);
                    break;
            }
        }
            
		private void HandlePressedBack()
		{
			FirePressedBack();
		}

		private void HandlePressedStart()
		{           
            FirePickedRemoteDeck( GameManager.Get.GetCurrentDeckId() );
		}

		private void HandleLeftButtonClicked( object o, GameObjectEventArgs gameObjectEventArgs )
		{
            switch( GameManager.Get.CurrentShipClass )
			{
				case ShipClass.Ancient:
					HandlePressedEnforcer( true );
					break;

				case ShipClass.Enforcer:
					HandlePressedSmuggler( true );
					break;

				case ShipClass.Smuggler:
					HandlePressedAncient( true );
					break;
			}
		}

		private void HandleRightButtonClicked( object o, GameObjectEventArgs gameObjectEventArgs )
		{
            switch( GameManager.Get.CurrentShipClass )
			{
				case ShipClass.Ancient:
					HandlePressedSmuggler( true );
					break;

				case ShipClass.Enforcer:
					HandlePressedAncient( true );
					break;

				case ShipClass.Smuggler:
					HandlePressedEnforcer( true );
					break;
			}
		}
		#endregion
	}
}