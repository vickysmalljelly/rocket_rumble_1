﻿using System;
using System.Text.RegularExpressions;
using SmallJelly.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    public class UsernameEntryMenuController : MenuController
    {
        #region Public Event Handlers
        public event Action<string> SubmittedUsername;
        #endregion

        #region Exposed To Inspector
        [SerializeField]
        private Text mUsernameText;

        [SerializeField]
        private TextMeshProUGUI mErrorText;

        [SerializeField]
        private GameObject mError;

        [SerializeField]
        private UIButton mButton;
        #endregion

        #region MonoBehaviour Methods
        private void OnEnable()
        {
            SJLogger.AssertCondition(mUsernameText != null, "mUsernameText must be set in inspector");
            SJLogger.AssertCondition(mErrorText != null, "mErrorText must be set in inspector");
            SJLogger.AssertCondition(mError != null, "mError must be set in inspector");
            SJLogger.AssertCondition(mButton != null, "mButton must be set in inspector");
        }

        public override void OnShow()
        {
            RegisterListeners();
        }

        public override void OnHide()
        {
            UnregisterListeners();
        }
        #endregion

        #region Event Firing
        private void FireSubmittedUsername(string newName)
        {
            if( SubmittedUsername != null )
            {
                SubmittedUsername(newName);
            }
        }
        #endregion

        #region Event Registration
        private void RegisterListeners()
        {
            mButton.Clicked += HandleButtonClicked;
        }

        private void UnregisterListeners()
        {
            mButton.Clicked -= HandleButtonClicked;
        }
        #endregion

        #region Event Handlers
        private void HandleButtonClicked( object o, GameObjectEventArgs gameObjectEventArgs )
        {
            string newName = mUsernameText.text;

            if(ValidateUsername(newName))
            {
                FireSubmittedUsername(newName);
            }
        }

        private bool ValidateUsername(string username)
        {
            mError.gameObject.SetActive(false);

            if(username.Length < AuthenticationMenuController.MinUsernameLength || username.Length > AuthenticationMenuController.MaxUsernameLength)
            {
                mErrorText.text = string.Format("Your callsign must be between {0} and {1} characters long!", AuthenticationMenuController.MinUsernameLength, AuthenticationMenuController.MaxUsernameLength);
                mError.gameObject.SetActive(true);
                SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: false");
                return false;
            }

            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if(!r.IsMatch(username))
            {
                mErrorText.text = "Your callsign can only contain alphanumeric characters.";
                mError.gameObject.SetActive(true);
                SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: false");
                return false;
            }

            return true;
        }
        #endregion

        #region Public Methods
        public void ReportUsernameErrorToUser(ServerError error)
        {
            mErrorText.text = string.Format("Error: {0}", error.UserFacingMessage);
            mError.gameObject.SetActive(true);
        }
        #endregion
    }
}
