using UnityEngine;
using UnityEngine.UI;

using System;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Login controller, for when the user has an existing id and username
    /// </summary>
    public class LoginController : SJMonoBehaviour
    {
		#region Actions
		public Action LoginCompleted;
		#endregion

		#region Exposed To Inspector
        [SerializeField]
        private InputField mUsername;

        [SerializeField]
        private InputField mPassword1;

        [SerializeField]
        private Button mOk;

        [SerializeField]
        private Text mEmailError;

        [SerializeField]
        private Text mPassword1Error;

        [SerializeField]
        private Text mGeneralError;

        [SerializeField]
        private Button mLeaveLoginButton;
		#endregion

		#region Member Variables
        // used by the AT system
        private ServerError mServerError;
        private const string mAnalyticsString = "login";
		#endregion

		#region MonoBehaviour Methods
        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mUsername != null, "mUsername has not been set in Inspector");
            SJLogger.AssertCondition(mPassword1 != null, "mPassword1 has not been set in Inspector");
            SJLogger.AssertCondition(mOk != null, "mOk has not been set in Inspector");
            SJLogger.AssertCondition(mEmailError != null, "mEmailError has not been set in Inspector");
            SJLogger.AssertCondition(mPassword1Error != null, "mPassword1Error has not been set in Inspector");
            SJLogger.AssertCondition(mGeneralError != null, "mGeneralError has not been set in Inspector");
            SJLogger.AssertCondition(mLeaveLoginButton != null, "mLeaveLoginButton has not been set in Inspector");

            // Hide ok button to start with
            mOk.gameObject.SetActive(false);

            mEmailError.gameObject.SetActive(false);
            mPassword1Error.gameObject.SetActive(false);
            mGeneralError.gameObject.SetActive(false);

            mLeaveLoginButton.onClick.AddListener(HandleReset);
        }

        private void OnDestroy()
        {
            mLeaveLoginButton.onClick.RemoveListener(HandleReset);
        }

        protected void Start()
        {
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Authentication, mAnalyticsString, null);
        }
		#endregion

		#region Public Methods
        public void OnOkButtonClicked()
        {
            // Hide all previous errors
            mEmailError.gameObject.SetActive(false);
            mPassword1Error.gameObject.SetActive(false);
            mGeneralError.gameObject.SetActive(false);

            mServerError = null;

            if (PasswordsValid() && UsernamesValid())
            {
                DialogManager.Get.ShowSpinner();
                ClientAuthentication.Login(mUsername.text, mPassword1.text, OnLoginSuccess, OnLoginError);
            }
        }


		public void OnEmailEntered()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Email: {0}", mUsername.text);
			ProcessInputs();
		}

		public void OnPassword1Entered()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Password1: {0}", mPassword1.text);
			ProcessInputs();
		}
		#endregion

		#region Private Methods
        private void HandleReset()
        {
            mUsername.text = string.Empty;
            mPassword1.text = string.Empty;

            mGeneralError.text = string.Empty;
            mGeneralError.gameObject.SetActive(false);

            mEmailError.text = string.Empty;
            mEmailError.gameObject.SetActive(false);

            mPassword1Error.text = string.Empty;
            mPassword1Error.gameObject.SetActive(false);
        }

        private void OnLoginSuccess()
        {
            DialogManager.Get.HideSpinner();

            if (LoginCompleted != null)
            {
                LoginCompleted();
            }

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Authentication, mAnalyticsString, null);
        }


        private void OnLoginError(ServerError error)
        {
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Fail, ProgressArea.Authentication, mAnalyticsString, error.ErrorType.ToString());

            DialogManager.Get.HideSpinner();
            SJLogger.LogMessage(MessageFilter.UI, "Login failed, {0}, {1}", error.UserFacingMessage, error.TechnicalDetails);

            mServerError = error;

			switch( error.ErrorType )
			{
				case ServerErrorType.Timeout:
					DisplayError( error.UserFacingMessage );
					break;

				default:
					mGeneralError.text = error.UserFacingMessage;
					mGeneralError.gameObject.SetActive(true);
					break;
			}
        }

        private bool UsernamesValid()
        {
            if(string.Compare(mUsername.text, 0, "system_", 0, 7) == 0)
            {
                mEmailError.text = "Username cannot start with 'system_'";
                mEmailError.gameObject.SetActive(true);
                return false;
            }
            return true;
        }

        private bool PasswordsValid()
        {
            // Check password length
            if (mPassword1.text.Length < AuthenticationMenuController.MinPasswordLength)
            {
                mPassword1Error.text = string.Format("Password must be at least {0} characters", AuthenticationMenuController.MinPasswordLength);
                mPassword1Error.gameObject.SetActive(true);
                return false;
            }

            return true;
        }

        private void ProcessInputs()
        {
            mOk.gameObject.SetActive(CheckHaveEntries());
        }

        private bool CheckHaveEntries()
        {
            if (mUsername.text.Length == 0)
            {
                return false;
            }

            if (mPassword1.text.Length == 0)
            {
                return false;
            }

            return true;
        }

		//TODO We should be able to generalise this error handling and other login/registration stuff into
		//an authentication parent class
		private void DisplayError( string error )
		{
			ButtonData okButton = new ButtonData("Ok", HandleErrorClosed);
			DialogManager.Get.ShowMessagePopup("Error", error, okButton);
		}
		#endregion

		#region Event Handlers
		private void HandleErrorClosed()
		{
			DialogManager.Get.HideMessagePopup();
		}
		#endregion

		#region Automated Tests
		public void ATSetUsernameAndPassword(string username, string password)
		{
			mUsername.text = username;
			mPassword1.text = password;
		}

        /// <summary>
        /// Returns the server error if it exists. Used by the automated testing system.
        /// </summary>
        /// <returns></returns>
        public ServerError ATGetServerError()
        {
            return mServerError;
        }
		#endregion
    }
}
