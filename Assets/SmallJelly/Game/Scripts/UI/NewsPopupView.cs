﻿using System;
using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly 
{
    /// <summary>
    /// Displays the news popup
    /// </summary>
    public class NewsPopupView : UIView<NewsData>  
    {
        [SerializeField]
        private GameObject mContentRoot;

        [SerializeField]
        private Button mCloseButton;

        private List<GameObject> mCurrentNewsItems = new List<GameObject>();

        public Action CloseButtonClicked;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mContentRoot != null, "mContentRoot has not been set in the inspector");
            SJLogger.AssertCondition(mCloseButton != null, "mCloseButton has not been set in the inspector");
        }

        private void OnEnable()
        {
            mCloseButton.onClick.AddListener(HandleButtonClicked);
        }

        private void OnDisable()
        {
            mCloseButton.onClick.RemoveListener(HandleButtonClicked);
        }

        protected override void OnRefreshView(NewsData data)
        {
            // Remove existing items
            foreach(GameObject item in mCurrentNewsItems)
            {
                Destroy(item.gameObject);
            }

            mCurrentNewsItems.Clear();

            // Create new items
            GameObject prefab = LoadPrefab(FileLocations.NewsItemPrefab);

            foreach(NewsItem item in data.NewsItems)
            {
                GameObject newGameObject = InstantiatePrefab(prefab, mCurrentNewsItems, mContentRoot);
                NewsItemView view = newGameObject.GetComponent<NewsItemView>();
                SJLogger.AssertCondition(view != null, "Failed to find NewsItemView on prefab");
                view.OnRefresh(item);
            }

        }

        private void HandleButtonClicked()
        {
            if(CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }
    }
}