﻿using SmallJelly.Framework;

namespace SmallJelly
{
    public class DeckIdComponent : SJMonoBehaviour 
    {
        public string DeckId { get; set; }       
    }
}
