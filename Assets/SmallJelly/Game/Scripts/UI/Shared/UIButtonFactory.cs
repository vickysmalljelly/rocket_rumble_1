﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.IO;
using UnityEngine.UI;
using System;

namespace SmallJelly
{
	public class UIButtonFactory
	{
		#region Enums
		private enum ButtonStyle
		{
			None,
			AncientDeckSelection,
			SmugglerDeckSelection,
			EnforcerDeckSelection,
			DeckContents,
			Inbox
		}
		#endregion

		#region Public Methods
		public static UIButton ConstructDeckSelectionButton( string shipClass, string deckId, string text )
		{
			ButtonStyle buttonStyle = default( ButtonStyle );
			switch( shipClass )
			{
				case ShipClass.Ancient:
					buttonStyle = ButtonStyle.AncientDeckSelection;
					break;

				case ShipClass.Smuggler:
					buttonStyle = ButtonStyle.SmugglerDeckSelection;
					break;

				case ShipClass.Enforcer:
					buttonStyle = ButtonStyle.EnforcerDeckSelection;
					break;
			}

            UIButton button = ConstructUIButton( buttonStyle, text );

            DeckIdComponent component = button.gameObject.AddComponent<DeckIdComponent>();
            component.DeckId = deckId;

            return button;

		}

		public static UIButton ConstructInboxButton( string text )
		{
			return ConstructUIButton( ButtonStyle.Inbox, text );
		}

		public static UIButton ConstructDeckContentsButton( string text, int power, int numberOfCopies )
		{
			DeckContentsUIButton deckContentsUIButton = (DeckContentsUIButton)ConstructUIButton( ButtonStyle.DeckContents, text );
			deckContentsUIButton.Power = power;
			deckContentsUIButton.NumberOfCopies = numberOfCopies;

			return deckContentsUIButton;
		}
		#endregion

		#region Private Methods
		private static UIButton ConstructUIButton( ButtonStyle buttonStyle, string text )
		{
			//Create the button
			GameObject button = Resources.Load< GameObject >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyButtonPrefabs, Path.DirectorySeparatorChar, buttonStyle.ToString() ) );
			SJLogger.AssertCondition( button != null, "No prefab for the button with type \"{0}\" was found", buttonStyle );

			button = UnityEngine.Object.Instantiate( button );

			UIButton uiButton = button.GetComponent< UIButton >();
			SJLogger.AssertCondition( button != null, "Supplied button style \"{0}\", did not have a button component", buttonStyle );

			//Assign the text
			uiButton.Text = text;

			return uiButton;
		}
		#endregion
	}
}