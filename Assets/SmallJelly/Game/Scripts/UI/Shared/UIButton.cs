﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class UIButton : SJMonoBehaviour
	{
		#region Public Event Handlers
		public event EventHandler< GameObjectEventArgs > Clicked;
		#endregion

		#region Public Properties
		public string Text
		{
			get
			{
				return mText.text;
			}

			set
			{
				mText.text = value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private TextMeshProUGUI mText;
		#endregion

		#region Public Methods
		public void OnClicked()
		{
			FireClicked();
		}
		#endregion

		#region Event Firing
		private void FireClicked()
		{
			if( Clicked != null )
			{
				Clicked( this, new GameObjectEventArgs( gameObject ) );
			}
		}
		#endregion
	}
}