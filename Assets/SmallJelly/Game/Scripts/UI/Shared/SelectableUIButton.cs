﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class SelectableUIButton : UIButton
	{
		#region Exposed To Inspector
		[SerializeField]
		private Image mImage;
		#endregion

		#region Properties
		public bool Selected
		{
			get
			{
				return mImage.gameObject.activeSelf;
			}

			set
			{
				mImage.gameObject.SetActive( value );
			}
		}
		#endregion
	}
}