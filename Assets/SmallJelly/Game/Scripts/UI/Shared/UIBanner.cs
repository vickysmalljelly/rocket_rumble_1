﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class UIBanner : SJMonoBehaviour
	{
		#region Public Properties
		public string Text
		{
			get
			{
				return mText.text;
			}

			set
			{
				mText.text = value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private TextMeshProUGUI mText;

		[SerializeField]
		private GameObject mBannerView;
		#endregion

		#region Public Methods
		public void SetViewPosition( Vector3 position )
		{
			mBannerView.transform.localPosition = position;
		}
		#endregion
	}
}