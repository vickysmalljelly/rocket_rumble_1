using UnityEngine;
using UnityEngine.UI;
using System;
using SmallJelly.Framework;
using System.Text.RegularExpressions;

namespace SmallJelly
{
	/// <summary>
	/// Handles Registration input
	/// </summary>
	public class RegistrationController : SJMonoBehaviour 
	{
		#region Inspector Variables
        [SerializeField]
        private InputField mUserName;

        [SerializeField]
        private InputField mPassword1;

        [SerializeField]
        private InputField mPassword2;

        [SerializeField]
        private Button mOk;

        [SerializeField]
        private Text mUserNameError;

        [SerializeField]
        private Text mPassword1Error;

        [SerializeField]
        private Text mPassword2Error;

        [SerializeField]
        private Text mGeneralError;

        [SerializeField]
        private Button mLeaveRegistrationButton;
        #endregion

		public Action RegistrationCompleted;

        private const string mAnalyticsString = "registration";
	
		#region MonoBehaviour methods
        protected override void Awake()
        {            
            base.Awake();

            SJLogger.AssertCondition(mUserName != null, "mUserName has not been set in Inspector");
            SJLogger.AssertCondition(mPassword1 != null, "mPassword1 has not been set in Inspector");
            SJLogger.AssertCondition(mPassword2 != null, "mPassword2 has not been set in Inspector");
            SJLogger.AssertCondition(mOk != null, "mOk has not been set in Inspector");
            SJLogger.AssertCondition(mUserNameError != null, "mUserNameError has not been set in Inspector");
            SJLogger.AssertCondition(mPassword1Error != null, "mPassword1Error has not been set in Inspector");
            SJLogger.AssertCondition(mPassword2Error != null, "mPassword2Error has not been set in Inspector");
            SJLogger.AssertCondition(mGeneralError != null, "mGeneralError has not been set in Inspector");
            SJLogger.AssertCondition(mLeaveRegistrationButton != null, "mLeaveRegistrationButton has not been set in Inspector");

            // Hide ok button to start with
            mOk.gameObject.SetActive(false);

            mUserNameError.gameObject.SetActive(false);
            mPassword1Error.gameObject.SetActive(false);
            mPassword2Error.gameObject.SetActive(false);
            mGeneralError.gameObject.SetActive(false);

            mLeaveRegistrationButton.onClick.AddListener(HandleReset);
        }

        private void Start()
        {
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Authentication, mAnalyticsString, null);            
        }

        private void OnDestroy()
        {
            mLeaveRegistrationButton.onClick.RemoveListener(HandleReset);
        }

        private void OnEnable()
        {
            mUserName.text = string.Empty;
            mPassword1.text = string.Empty;
            mPassword2.text = string.Empty;

            mUserName.onEndEdit.AddListener(ProcessInputs);
            mPassword1.onEndEdit.AddListener(ProcessInputs);
            mPassword2.onEndEdit.AddListener(ProcessInputs);

            mOk.onClick.AddListener(HandleOkButtonClicked);
        }

        private void OnDisable()
        {
            mUserName.onEndEdit.RemoveListener(ProcessInputs);
            mPassword1.onEndEdit.RemoveListener(ProcessInputs);
            mPassword2.onEndEdit.RemoveListener(ProcessInputs);

            mOk.onClick.RemoveListener(HandleOkButtonClicked);
        }
        #endregion

		#region Private Methods
		private bool UsernameValid()
		{
			if (string.Compare(mUserName.text, 0, "system_", 0, 7) == 0)
			{
				mUserNameError.text = "Username cannot start with 'system_'";
				mUserNameError.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: false");
				return false;
			}

			if(mUserName.text.Length < AuthenticationMenuController.MinUsernameLength || mUserName.text.Length > AuthenticationMenuController.MaxUsernameLength)
			{
				mUserNameError.text = string.Format("Username must be between {0} and {1} characters long.", AuthenticationMenuController.MinUsernameLength, AuthenticationMenuController.MaxUsernameLength);
				mUserNameError.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: false");
				return false;
			}

			Regex r = new Regex("^[a-zA-Z0-9]*$");
			if(!r.IsMatch(mUserName.text))
			{
				mUserNameError.text = "Username can only contain alphanumeric characters.";
				mUserNameError.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: false");
				return false;
			}

			SJLogger.LogMessage(MessageFilter.UI, "UsernameValid: true");
			return true;
		}

		private bool PasswordValid()
		{
			// Check password length
			if(mPassword1.text.Length < AuthenticationMenuController.MinPasswordLength || mPassword1.text.Length > AuthenticationMenuController.MaxPassswordLength)
			{
				mPassword1Error.text = string.Format("Password must be between {0} and {1} characters long.", AuthenticationMenuController.MinPasswordLength, AuthenticationMenuController.MaxPassswordLength);
				mPassword1Error.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "PasswordValid: false");
				return false;
			}

			Regex r = new Regex("^[a-zA-Z0-9]*$");
			if(!r.IsMatch(mPassword1.text))
			{
				mPassword1Error.text = "Password can only contain alphanumeric characters.";
				mPassword1Error.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "PasswordValid: false");
				return false;
			}

			// Check passwords match
			if(string.Compare(mPassword1.text, mPassword2.text) != 0)
			{
				mPassword2Error.text = string.Format("Passwords do not match");
				mPassword2Error.gameObject.SetActive(true);
				SJLogger.LogMessage(MessageFilter.UI, "PasswordValid: false");
				return false;
			}

			SJLogger.LogMessage(MessageFilter.UI, "PasswordValid: true");
			return true;
		}

		private void ProcessInputs(string text)
		{
			mOk.gameObject.SetActive(CheckHaveEntries());
		}

		private bool CheckHaveEntries()
		{
			if(mUserName.text.Length == 0)
			{
				return false;
			}

			if(mPassword1.text.Length == 0)
			{
				return false;
			}

			if(mPassword2.text.Length == 0)
			{
				return false;
			}

			return true;
		}

		private void DisplayError( string error )
		{
			ButtonData okButton = new ButtonData("Ok", HandleErrorClosed);
			DialogManager.Get.ShowMessagePopup("Error", error, okButton);
		}
		#endregion

		#region Event Handlers
        private void HandleReset()
        {
            mUserName.text = string.Empty;
            mPassword1.text = string.Empty;
            mPassword2.text = string.Empty;

            mUserNameError.text = string.Empty;
            mPassword1Error.text = string.Empty;
            mPassword2Error.text = string.Empty;
            mGeneralError.text = string.Empty;
                                   
            mUserNameError.gameObject.SetActive(false);
            mPassword1Error.gameObject.SetActive(false);
            mPassword2Error.gameObject.SetActive(false);
            mGeneralError.gameObject.SetActive(false);                       
        }

		private void HandleOkButtonClicked()
		{
			// Hide all previous errors
			mUserNameError.gameObject.SetActive(false);
			mPassword1Error.gameObject.SetActive(false);
			mPassword2Error.gameObject.SetActive(false);
			mGeneralError.gameObject.SetActive(false);

            if(PasswordValid() && UsernameValid())
			{
				DialogManager.Get.ShowSpinner();
                ClientAuthentication.Register(mUserName.text, mPassword1.text, HandleRegistrationSuccess, HandleRegistrationError);
			}
		}

		private void HandleRegistrationSuccess()
		{
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Authentication, mAnalyticsString, null);

			DialogManager.Get.HideSpinner();

			//Fire analytic
			FireRecordRegistraionAnalytic( true );

			if(RegistrationCompleted != null)
			{
				RegistrationCompleted();
			}
		}
		
		private void HandleRegistrationError(ServerError error)
		{
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Fail, ProgressArea.Authentication, mAnalyticsString, error.ErrorType.ToString());

			DialogManager.Get.HideSpinner();
			SJLogger.LogMessage(MessageFilter.UI, "Registration failed");

			//Fire analytic
			FireRecordRegistraionAnalytic( false );

			switch( error.ErrorType )
			{
				case ServerErrorType.Timeout:
					DisplayError( error.UserFacingMessage );
					break;

				case ServerErrorType.UsernameTaken:
                case ServerErrorType.Profanity:
					mUserNameError.text = error.UserFacingMessage;
					mUserNameError.gameObject.SetActive(true);
					break;

				default:
					mGeneralError.text = error.UserFacingMessage;
					mGeneralError.gameObject.SetActive(true);
					break;
			}
		}

		private void HandleErrorClosed()
		{
			DialogManager.Get.HideMessagePopup();
		}
		#endregion

		#region Analytics Methods
		private void FireRecordRegistraionAnalytic( bool success )
		{
			AnalyticsManager.Get.RecordRegistration( success );
		}
		#endregion
	}
}

