using UnityEngine;
using UnityEngine.UI;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// View for an item in the list of challenge requests.
	/// </summary>
	public class ChallengeRequestView : UIView<Challenge> 
	{
		[SerializeField]
		private Text mOpponentName;

		protected override void OnRefreshView(Challenge challenge)
		{
			mOpponentName.text = challenge.User1.DisplayName;
		}

		public void OnClickAccept()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Challenge accepted!");

			ClientChallenge.AcceptChallenge(AcceptSuccessHandler, AcceptErrorHandler);
		}

		public void OnClickIgnore()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Challenge ignored :(");
			ClientChallenge.DeclineChallenge(DeclineSuccessHandler, DeclineErrorHandler);
		}
	
		private void AcceptSuccessHandler()
		{

		}

		private void AcceptErrorHandler(ServerError error)
		{

		}

		private void DeclineSuccessHandler()
		{
			
		}
		
		private void DeclineErrorHandler(ServerError error)
		{
			
		}
	}
}
