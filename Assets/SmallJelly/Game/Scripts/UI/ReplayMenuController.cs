﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    /// <summary>
    /// Controller for playing a replay
    /// </summary>
    public class ReplayMenuController : MenuController 
    {
        [SerializeField]
        private InputField mReplayName;

        [SerializeField]
        private Button mOk;

        [SerializeField]
        private Text mReplayError;

        public Action OkClicked;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mReplayName != null, "mReplayName has not been set in Inspector");
            SJLogger.AssertCondition(mOk != null, "mOk has not been set in Inspector");
            SJLogger.AssertCondition(mReplayError != null, "mReplayError has not been set in Inspector");

            mReplayName.onEndEdit.AddListener(HandleEndEdit);

            // Hide ok button to start with
            mOk.gameObject.SetActive(false);

            mReplayError.gameObject.SetActive(false);
        }

        public void OnOkButtonClicked()
        {
            // Hide all previous errors
            mReplayError.gameObject.SetActive(false);

            if(OkClicked != null)
            {
                OkClicked();
            }

            // Hide ok button again
            mOk.gameObject.SetActive(false);
        }

        public string GetReplayName()
        {
            return mReplayName.text;
        }

        public void ATSetReplayName(string replayName)
        {
            mReplayName.text = replayName;
        }

        private void HandleEndEdit(string input)
        {
            // Validate input
            if(string.IsNullOrEmpty(input))
            {
                mOk.gameObject.SetActive(false);
                return;
            }
                
            mOk.gameObject.SetActive(true);
        }
    }
}
