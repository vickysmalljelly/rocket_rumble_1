﻿using UnityEngine;
using UnityEngine.UI;

using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Player profile view. Displays the user id and display name
	/// // TODO: Should inherit from UIView
	/// </summary>
	public class PlayerProfileView : SJMonoBehaviour 
	{
		[SerializeField]
		private Text mUserName;

		[SerializeField]
		private Text mDisplayName;

		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(mUserName != null, "UserName has not been set in inspector");
            SJLogger.AssertCondition(mDisplayName != null, "DisplayName has not been set in inspector");
		}

		private void OnEnable()
		{
            mUserName.text = ClientAuthentication.PlayerAccountDetails.UserId;
            mDisplayName.text = ClientAuthentication.PlayerAccountDetails.DisplayName;
		}
	}
}
