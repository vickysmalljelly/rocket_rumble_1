﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SmallJelly
{
	public class MapMenuController : MenuController 
	{
		#region Public Events
		public event EventHandler Back;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private TextMeshProUGUI mCreditsCounter;
		#endregion

		#region Protected Methods
		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition( mCreditsCounter != null, "mCreditsCounter has not been set in the inspector" );

			RegisterListeners();

            // Set the initial value of credits
            mCreditsCounter.text = GameManager.Get.GetCreditsCount().ToString();
		}
		#endregion

		#region Private Methods
		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			GameManager.Get.CreditsUpdated += HandleCreditsUpdated;
		}

		private void UnregisterListeners()
		{
			if(GameManager.Get != null)
			{
				GameManager.Get.CreditsUpdated -= HandleCreditsUpdated;
			}
		}
		#endregion

		#region Event Firing
		public void FireBack()
		{
			if( Back != null )
			{
				Back( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleCreditsUpdated(long value)
		{
			SJLogger.LogMessage(MessageFilter.Gameplay, "Updating credits counter to {0}", value);
			mCreditsCounter.text = value.ToString();
		}
		#endregion
	}

}