﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Handles input from the Matchmaking menu
	/// </summary>
	public class MatchmakingMenuController : MenuController 
	{
		[SerializeField]
		private OnlinePlayersController mOnlinePlayersController;

        [SerializeField]
        private Button mBackButton;

		public Action<User> OnlineUserClicked;
		public Action LogoutClicked;
        public Action BackButtonClicked;

		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(mOnlinePlayersController != null, "mOnlinePlayersController has not been set in the inspector");
            SJLogger.AssertCondition(mBackButton != null, "mBackButton has not been set in the inspector");
		}

		private void OnEnable()
		{
			mOnlinePlayersController.OnlinePlayerClicked += HandleOnlinePlayerClicked;
            mBackButton.onClick.AddListener(HandleBackClicked);
		}

		private void OnDisable()
		{
			mOnlinePlayersController.OnlinePlayerClicked -= HandleOnlinePlayerClicked;
            mBackButton.onClick.RemoveListener(HandleBackClicked);
		}

		public void OnLogoutClicked()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Logout clicked");
			if(LogoutClicked != null)
			{
				LogoutClicked();
			}
		}

        private void HandleBackClicked()
        {
            if(BackButtonClicked != null)
            {
                BackButtonClicked();
            }
        }

		private void HandleOnlinePlayerClicked(User user)
		{
			SJLogger.LogMessage(MessageFilter.UI, "User {0} clicked", user.DisplayName);
			if(OnlineUserClicked != null)
			{
				OnlineUserClicked(user);
			}
		}
	}
	
}