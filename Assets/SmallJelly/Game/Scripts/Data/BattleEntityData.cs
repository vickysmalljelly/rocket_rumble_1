﻿using System;
using System.Text;

using SmallJelly.Framework;

using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
	public enum BattleEntityType
	{
		NotSet,
		Unknown, // e.g. a card that has not been revealed or a hidden virus 
		Weapon,
		Utility,
		TargetingCrew,
		NonTargetingCrew,
		Virus
	}

	public static class BattleEntityRarity
	{
		public const string Unknown = "Unknown"; // e.g. a card that has not been revealed or a hidden virus 
		public const string Common = "Common";
		public const string Rare = "Rare";
		public const string Epic = "Epic";
		public const string Legendary = "Legendary";
	}

	/// <summary>
    /// Includes the number of the cards the player owns.
    /// </summary>
    public struct CollectionBattleEntityData
    {
        public int Count;
        public BattleEntityData Card;
        public bool New;
    }

	/// <summary>
	/// Base class for all battle entities, i.e. Weapons, Utilities, Crew and Viruses
	/// </summary>
	[Serializable]	
	public abstract class BattleEntityData
	{
		public BattleEntityData() {}

		public BattleEntityData(BattleEntityType entityType)
		{
			EntityType = entityType;
		}

		#region Public Variables

        // Must be public member variable rather than properties for serialisation

		// Stored in the database 
		public PlayerType Player;
		public string Id;
		public string DisplayName;
		public string Rarity;
		public int Cost;
		public string State;
        public string CardDisplayText;
		public string Class;
		public string InteriorColour = "#FF6633";

		// Set on the server
		public int DrawNumber = -1;

		public BattleEntityController.Buff[] Buffs { get; set; }

		// Set in the constructor 
		public BattleEntityType EntityType { get; private set; }
		#endregion

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.AppendFormat("" +
				"[BattleEntityData: Id={0}, " +
				"DisplayName={1}, " +
				"Rarity={2}, " +
				"PowerCost={3}, " +
				"CardText={4}, " +
				"DrawNumber={5}, " +
				"EntityType={6}]", 
				Id, 
				DisplayName, 
				Rarity, 
				Cost, 
				CardDisplayText,
				DrawNumber, 
				EntityType);
			
			return builder.ToString();
		}

		public virtual bool Validate()
		{
			if(string.IsNullOrEmpty(Id))
			{
				SJLogger.LogError("BattleEntity must have an Id");
				return false;
			}

			if(string.IsNullOrEmpty(DisplayName))
			{
				SJLogger.LogError("BattleEntity must have a Name");
				return false;
			}

			if(string.IsNullOrEmpty(Rarity))
			{
				SJLogger.LogError("BattleEntity must have a Rarity");
				return false;
			}

			return true;
		}
	}
}