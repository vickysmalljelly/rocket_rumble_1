﻿namespace SmallJelly
{
    public static class ShipClass
    {
        public const string Debug = "Debug";
		public const string Neutral = "Neutral";
        public const string Ancient = "Ancient";
        public const string Smuggler = "Smuggler";
        public const string Enforcer = "Enforcer";
    }
}
