﻿using System;

namespace SmallJelly
{
    [Serializable]  
	public class UtilityData : ComponentData
    {
        public UtilityData() : base(BattleEntityType.Utility) {}

    }
}
