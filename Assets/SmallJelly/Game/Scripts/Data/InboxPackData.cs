﻿namespace SmallJelly
{
    /// <summary>
    /// Describes the data for a pack of cards in the inbox.
    /// </summary>
    public class InboxPackData : InboxItemData
    {
        public string PackId;
        public string PackType;
    }
}