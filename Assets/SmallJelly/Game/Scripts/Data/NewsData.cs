﻿using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// News data for display to the player
    /// </summary>
    public class NewsData
    {
        private List<NewsItem> mNewsItems;
        private string mHeadline;

        public NewsData(List<NewsItem> list)
        {
            mNewsItems = list;

            if(mNewsItems.Count > 0)
            {
                mHeadline = mNewsItems[0].Title;
            }
        }

        public string Headline { get { return mHeadline; } }

        public List<NewsItem> NewsItems { get { return mNewsItems; } }

    }
}
