﻿using System;

using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]	
	public class WeaponData : ComponentData
	{
		public WeaponData() : base(BattleEntityType.Weapon) {}

		// Stored in the database, must be public member variable rather than properties for serialisation
		public string Manufacturer; // TODO: Move to ManufacturedEntityData
		public int Attack;
		public string DamageType;

		public override string ToString()
		{
			string baseString = base.ToString();
			string derivedString = string.Format ("[WeaponData: Manufacturer={0}, HP={1}, Damage={2}, DamageType={3}]", Manufacturer, HP, Attack, DamageType);
			return baseString + " " + derivedString;
		}
			
		public override bool Validate()
		{
			if(!base.Validate())
			{
				return false;
			}

			if(string.IsNullOrEmpty(Manufacturer))
			{
				SJLogger.LogError("Weapon {0} must have a Manufacturer", DisplayName);
				return false;
			}

			if(string.IsNullOrEmpty(DamageType))
			{
				SJLogger.LogError("Weapon {0} must have a DamageType", DisplayName);
				return false;
			}

			return true;
		}
	}
}