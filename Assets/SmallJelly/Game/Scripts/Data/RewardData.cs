﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// A reward that has been assigned to the player
    /// </summary>
    public class RewardData 
    {
		#region Constants
		public const string XELION_GAS = "Xelion Gas";
		public const string FLUOREX_CRYSTALS = "Fluorex Crystals";
		public const string AZOMITE = "Azomite";
		public const string CREDITS = "Credits";
		#endregion

        /// <summary>
        /// A list of BattleEntityData that the player has been given
        /// </summary>
        public List<BattleEntityData> BattleEntityCards = new List<BattleEntityData>();

        public bool NewCards { get; set; }

        /// <summary>
        /// A list of the ship cards the player has been given
        /// </summary>
        public List<ShipCardData> ShipCards = new List<ShipCardData>();

        /// <summary>
        /// A list of the packs the player has been given. These will appear in the inbox.
        /// </summary>
        public List<InboxItemData> Packs = new List<InboxItemData>();

        /// <summary>
        /// The number of credits contained in the reward
        /// </summary>
        public List<int> Credits;

        /// <summary>
        /// The number of crystals contained in the reward
        /// </summary>
        public int Crystals;

        public RankReward Rank;

        /// <summary>
        /// The resources contained in the reward
        /// </summary>
        public Dictionary<string, int> Resources = new Dictionary<string, int>();

        public static RewardData FromJson(SJJson json)
        {
            RewardData reward = new RewardData();

            reward.NewCards = json.GetBoolean("NewCards").GetValueOrDefault();

            if(json.ContainsKey("Cards")) 
            {
                List<SJJson> cards = json.GetSJJsonList("Cards");
                foreach(SJJson card in cards)
                {    
					BattleEntityData battleEntity = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, card );
                    reward.BattleEntityCards.Add(battleEntity);
                }
            }

            if(json.ContainsKey("ShipCards")) 
            {
                List<SJJson> ships = json.GetSJJsonList("ShipCards");
                foreach(SJJson ship in ships)
                {    
                    ShipCardData scd = JsonUtility.FromJson<ShipCardData>(ship.JSON);
                    reward.ShipCards.Add(scd);
                }
            }

            if(json.ContainsKey("Packs")) 
            {
                List<SJJson> packs = json.GetSJJsonList("Packs");
                foreach(SJJson packJson in packs)
                {    
                    InboxPackData pack = JsonUtility.FromJson<InboxPackData>(packJson.JSON);
                    reward.Packs.Add(pack);
                }
            }

            if(json.ContainsKey("Crystals"))
            {
                int? crystals = json.GetInt("Crystals");
                reward.Crystals = crystals.GetValueOrDefault();
            }                

            if(json.ContainsKey("Credits"))
            {
                reward.Credits = json.GetIntList("Credits");
            }

            if(json.ContainsKey("Rank"))
            {
                SJJson rank = json.GetObject("Rank");
                reward.Rank = JsonUtility.FromJson<RankReward>(rank.JSON);              
            }

            if(json.ContainsKey("XelionGas"))
            {
                int? resource = json.GetInt("XelionGas");
				reward.Resources.Add(XELION_GAS, resource.GetValueOrDefault());
            }

            if(json.ContainsKey("FluorexCrystals"))
            {
                int? resource = json.GetInt("FluorexCrystals");
				reward.Resources.Add(FLUOREX_CRYSTALS, resource.GetValueOrDefault());
            }

            if(json.ContainsKey("Azomite"))
            {
                int? resource = json.GetInt("Azomite");
				reward.Resources.Add(AZOMITE, resource.GetValueOrDefault());
            }

            return reward;
        }

        public int GetTotalCredits()
        {
            int total = 0;
            foreach(int amount in Credits)
            {
                total += amount;
            }

            return total;
        }
    }
}
