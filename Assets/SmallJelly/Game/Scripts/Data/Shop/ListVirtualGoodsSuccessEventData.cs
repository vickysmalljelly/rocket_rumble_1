﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class ListVirtualGoodsSuccessEventData : ShopEventData
	{
		#region Properties
		public List< IapData > Data
		{
			get;
			private set;
		}
		#endregion

		#region Constructors
		public ListVirtualGoodsSuccessEventData(  List< IapData > iapData )
		{
			Data = iapData;
		}
		#endregion
	}
}
