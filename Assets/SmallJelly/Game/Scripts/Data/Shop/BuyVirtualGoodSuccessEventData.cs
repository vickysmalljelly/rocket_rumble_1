﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class BuyVirtualGoodSuccessEventData : ShopEventData
	{
        public int NumPacks { get; private set; }

		#region Constructors
        public BuyVirtualGoodSuccessEventData(string shortCode)
		{
            // Extract the number of packs from the short code
            string match = "STANDARD_CARD_PACK_";
            int index = shortCode.IndexOf(match);
            if(index == 0)
            {
                string amount = shortCode.Substring(match.Length, shortCode.Length - match.Length);
                int tempAmount;
                int.TryParse(amount, out tempAmount);
                NumPacks = tempAmount;
            }
		}
		#endregion
	}
}
