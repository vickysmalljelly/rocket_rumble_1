﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class BuyVirtualGoodFromGooglePlaySuccessEventData : ShopEventData
	{

		#region Public Properties
		public string Id { get; private set; }

        public long NumCrystals { get; private set; }

		#endregion

		#region Constructors
        public BuyVirtualGoodFromGooglePlaySuccessEventData( string id, long numCrystals ) : base()
		{
			Id = id;
            NumCrystals = numCrystals;
		}
		#endregion
	}
}
