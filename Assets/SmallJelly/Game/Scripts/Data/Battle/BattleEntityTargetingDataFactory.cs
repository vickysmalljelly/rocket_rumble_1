﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntityTargetingDataFactory
	{
		#region Public Methods
		/// <summary>
		/// Deserialise the appropriate entity type
		/// </summary>
		public static BattleEntityTargetingData GetEntityFromJson( SJJson json )
		{
			BattleEntityTargetingData battleEntityTargetingData = new BattleEntityTargetingData();

			if( json.ContainsKey( "cardTextTargets" ) )
			{
				battleEntityTargetingData.TargetData = json.GetSJJsonList( "cardTextTargets" );
			}
				
			return battleEntityTargetingData;
		}

		#endregion
	}
}