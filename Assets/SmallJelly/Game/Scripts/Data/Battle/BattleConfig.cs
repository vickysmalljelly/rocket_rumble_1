﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Describes the initial setup for a battle needed by the client
    /// </summary>
    public sealed class BattleConfig 
    {
        public BattleConfig()
        {
            Players = new List<PlayerBattleConfig>();
        }

        public sealed class PlayerBattleConfig
        {
            public string PlayerId;
            public string ShipClass;
        }
            
        public bool MulliganEnabled;
        public string BattleId;
        public List<PlayerBattleConfig> Players;

        public static BattleConfig FromJson(SJJson json)
        {
            SJLogger.AssertCondition(json != null, "Need a reference to SJJson");

            BattleConfig battleConfig = new BattleConfig();

            List<SJJson> players = json.GetSJJsonList("Players");
            SJLogger.AssertCondition(players != null, "Can't find Players array in BattleConfig json");

            foreach(SJJson player in players)
            {
                PlayerBattleConfig playerConfig = JsonUtility.FromJson<PlayerBattleConfig>(player.JSON);
                battleConfig.Players.Add(playerConfig);
            }
                
            battleConfig.BattleId = json.GetString("BattleId");
            battleConfig.MulliganEnabled = json.GetBoolean("MulliganEnabled").GetValueOrDefault();
              
            return battleConfig;
        }
    }
}
