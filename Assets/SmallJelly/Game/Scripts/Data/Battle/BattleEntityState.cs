﻿namespace SmallJelly
{
    /// <summary>
    /// Container for names of BattleEntityStates
    /// </summary>
    public static class BattleEntityState 
    {
        public const string Initialised = "Initialised";
        public const string InHandPlayable = "InHandPlayable";
		public const string InHandNotEnoughPower = "InHandNotEnoughPower";
        public const string InHandNoTarget = "InHandNoTarget";
        public const string InHandNotMyTurn = "InHandNotMyTurn";
        public const string Playing = "Playing";
		public const string Installing = "Installing";
        public const string Spawning = "Spawning";
        public const string Online = "Online";
        public const string OnlineDoubleShot = "OnlineDoubleShot";
        public const string Fired = "Fired";
        public const string Offline = "Offline";
        public const string Destroyed = "Destroyed";
        public const string Jettisoned = "Jettisoned";
        public const string Unknown = "Unknown";
    }
}

