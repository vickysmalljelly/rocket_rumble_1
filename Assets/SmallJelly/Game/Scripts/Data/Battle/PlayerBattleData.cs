﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	/// <summary>
	/// Data to be displayed for a player during a battle.
	/// </summary>
	[Serializable]
	public class PlayerBattleData
	{
		public PlayerBattleData()
		{

		}

		/// <summary>
		/// The amount of power the player has available this turn.
		/// </summary>
		public int Power;

		/// <summary>
		/// The maximum power that a player can have in a turn.
		/// </summary>
		public int MaxPower;

        /// <summary>
        /// The power available to the player this turn.
        /// </summary>
        public int PowerThisTurn;

        /// <summary>
        /// The number of cards the player has left in their deck
        /// </summary>
        public int NumCardsInDeck;

		//Ship data
		public ShipBattleData ShipData;

		//Data of the cards in the players hand
		public List<BattleEntityData> HandCardData = new List<BattleEntityData>();

		//Data of the slots on the board
		public SocketData[] SocketData = new SocketData[9];

		//Data of the cards on the board
		public BattleEntityData[] BoardCardData = new BattleEntityData[9];

		public List<BattleRunner> BattleEvents = new List<BattleRunner>();
	}
}
