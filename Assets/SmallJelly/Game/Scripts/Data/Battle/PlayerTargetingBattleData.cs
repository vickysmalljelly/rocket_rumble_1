﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	/// <summary>
	/// Data to be displayed for a player during a battle.
	/// </summary>
	[Serializable]
	public class PlayerTargetingBattleData
	{
		//Key : Draw number, Value: Entities Targeting Data
		public Dictionary< int, BattleEntityTargetingData > BattleEntityTargetingData = new Dictionary< int, BattleEntityTargetingData >();

		public PlayerTargetingBattleData()
		{

		}
	}
}
