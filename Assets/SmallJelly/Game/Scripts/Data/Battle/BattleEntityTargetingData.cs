﻿using System;
using System.Text;

using SmallJelly.Framework;

using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntityTargetingData
	{
		public BattleEntityTargetingData() {}

		#region Public Variables
		// Set on the server
		public List<SJJson> TargetData;
		#endregion

		#region Public Methods
		public string DebugCardTextTargets()
		{
			if(TargetData == null)
			{
				return "No targets needed";
			}

			if(TargetData.Count == 0)
			{
				return "No targets available";
			}

			StringBuilder builder = new StringBuilder();
			foreach(SJJson json in TargetData)
			{
				builder.AppendFormat(" {0}, ", json.JSON);
			}

			return builder.ToString();
		}
		#endregion
	}
}