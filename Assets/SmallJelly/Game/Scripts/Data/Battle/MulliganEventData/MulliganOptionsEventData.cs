﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	public class MulliganOptionsEventData : MulliganEventData
	{
		#region Public Properties
		public BattleEntityData[] BattleEntityData
		{
			get
			{
				return mBattleEntityData;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityData[] mBattleEntityData;
		#endregion

		#region Constructors
		public MulliganOptionsEventData( BattleEntityData[] battleEntityData ) : base()
		{
			mBattleEntityData = battleEntityData;
		}
		#endregion
	}
}
