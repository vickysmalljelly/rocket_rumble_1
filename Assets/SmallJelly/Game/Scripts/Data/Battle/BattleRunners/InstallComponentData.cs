﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class InstallComponentData : BattleRunnerData
	{
		#region Public Properties
		public string PlayerId
		{
			get;
			private set;
		}

		public int DrawNumber
		{
			get;
			private set;
		}

		public int SocketId
		{
			get;
			private set;
		}
		#endregion

		#region Constructors
		public InstallComponentData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData,PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string playerId, int drawNumber, int socketId ) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			PlayerId = playerId;
			DrawNumber = drawNumber;
			SocketId = socketId;
		}
		#endregion
	}
}