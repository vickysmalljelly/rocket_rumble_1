﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class HullTakeDamageData : BattleRunnerData
	{
		#region Public Properties
		public string PlayerId
		{
			get;
			private set;
		}

		public int DamageAmount
		{
			get;
			private set;
		}
		#endregion

		//TODO - runnerJSON isnt actually neccessary for "HullTakeDamage" because we parse it into the above values. It is neccessary for other runners
		//so let's leave it here for now until we've done the same for all of those.
		public HullTakeDamageData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData,PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string playerId, int damageAmount) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			PlayerId = playerId;
			DamageAmount = damageAmount;
		}
	}
}