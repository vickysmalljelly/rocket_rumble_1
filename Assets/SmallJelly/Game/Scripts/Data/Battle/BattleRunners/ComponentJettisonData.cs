﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ComponentJettisonData : BattleRunnerData
	{
		#region Public Properties
		public string PlayerId
		{
			get;
			private set;
		}

		public int SocketId
		{
			get;
			private set;
		}
		#endregion

		#region Constructor
		public ComponentJettisonData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string playerId, int socketId) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			PlayerId = playerId;
			SocketId = socketId;
		}
		#endregion
	}
}