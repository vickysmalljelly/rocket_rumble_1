﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class NanotechPlayedData : BattleRunnerData
	{
		#region Public Properties
		public string PlayerId
		{
			get;
			private set;
		}

		public int DrawNumber
		{
			get;
			private set;
		}

		public SJJson NanotechEntity
		{
			get;
			private set;
		}
		#endregion

		public NanotechPlayedData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData,PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string playerId, int drawNumber, SJJson nanotechEntity ) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			PlayerId = playerId;
			DrawNumber = drawNumber;
			NanotechEntity = nanotechEntity;
		}
	}
}