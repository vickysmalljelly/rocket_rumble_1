﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace SmallJelly
{
	public class BattleRunnerDataFactory
	{
		#region Public Methods

		public static BattleRunnerData GetBattleRunnerData( string id, SJJson localAfterJson, SJJson remoteAfterJson, SJJson runnerJSON )
		{
			PlayerBattleData localAfterData = new PlayerBattleData();

			//Assign ship data
			localAfterData.ShipData = new ShipBattleData();
			localAfterData.ShipData.Player = PlayerType.Local;
			localAfterData.ShipData.HullHp = localAfterJson.GetInt( "hullHP" ).Value;
			localAfterData.ShipData.MaxHullHp = localAfterJson.GetInt( "maxHullHP" ).Value;

			localAfterData.Power = localAfterJson.GetInt( "power" ).Value;
			localAfterData.MaxPower = localAfterJson.GetInt( "maxPower" ).Value;
			localAfterData.PowerThisTurn = localAfterJson.GetInt( "powerThisTurn" ).Value;
			localAfterData.NumCardsInDeck = localAfterJson.GetInt( "numCards" ).Value;


			PlayerBattleData remoteAfterData = new PlayerBattleData();

			//Assign ship data
			remoteAfterData.ShipData = new ShipBattleData();
			remoteAfterData.ShipData.Player = PlayerType.Remote;
			remoteAfterData.ShipData.HullHp = remoteAfterJson.GetInt( "hullHP" ).Value;
			remoteAfterData.ShipData.MaxHullHp = remoteAfterJson.GetInt( "maxHullHP" ).Value;

			remoteAfterData.Power = remoteAfterJson.GetInt( "power" ).Value;
			remoteAfterData.MaxPower = remoteAfterJson.GetInt( "maxPower" ).Value;
            remoteAfterData.PowerThisTurn = remoteAfterJson.GetInt( "powerThisTurn" ).Value;
			remoteAfterData.NumCardsInDeck = remoteAfterJson.GetInt( "numCards" ).Value;          


			PlayerTargetingBattleData localTargetingBattleData = new PlayerTargetingBattleData();

			ProcessLocalHand(localAfterJson.GetSJJsonList( "hand" ), localAfterData, localTargetingBattleData);
			ProcessBoard( PlayerType.Local, localAfterJson.GetSJJsonList( "board" ), localAfterData, localTargetingBattleData );

			PlayerTargetingBattleData remoteTargetingBattleData = new PlayerTargetingBattleData();

			ProcessRemoteHand(remoteAfterJson.GetSJJsonList( "hand" ), remoteAfterData, remoteTargetingBattleData);
			ProcessBoard( PlayerType.Remote, remoteAfterJson.GetSJJsonList( "board" ), remoteAfterData, remoteTargetingBattleData );

			switch (id) 
			{
				case BattleRunnerFactory.HULL_TAKE_DAMAGE:
					return GetHullTakeDamageData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				case BattleRunnerFactory.COMPONENT_INSTALL_SUCCESS:
					return GetInstallComponentData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				case BattleRunnerFactory.BATTLE_DRAW_CARD:
					return GetDrawCardData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );
			
				case BattleRunnerFactory.COMPONENT_TAKE_DAMAGE:
				return GetComponentTakeDamageData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				case BattleRunnerFactory.JETTISON:
					return GetComponentJettisonData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				case BattleRunnerFactory.NANOTECH_PLAYED:
					return GetNanotechPlayedData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				case BattleRunnerFactory.HULL_DESTROYED:
					return GetHullDestroyedData ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );

				default:
					return GetDefault ( id, localAfterData, remoteAfterData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );
			}
		}

		#endregion

		#region Private Methods
		private static BattleRunnerData GetHullTakeDamageData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string playerId = runnerJSON.GetString ( "playerId" );
			int? damageAmount = (int)runnerJSON.GetInt ( "damageAmount" );

			return new HullTakeDamageData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, playerId, damageAmount.GetValueOrDefault() );
		}

		private static BattleRunnerData GetInstallComponentData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string playerId = runnerJSON.GetString ( "installedOnPlayerId" );
			int? drawNumber = runnerJSON.GetInt ( "drawNumber" );
			int? socketId = runnerJSON.GetInt ( "slotId" );

			SJLogger.LogMessage( MessageFilter.George, "Received data to install a component with the draw number {0}", drawNumber.GetValueOrDefault() );

			return new InstallComponentData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, playerId, drawNumber.GetValueOrDefault(), socketId.GetValueOrDefault() );
		}

		private static BattleRunnerData GetDrawCardData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			SJJson entityJson = runnerJSON.GetObject( "entity" );
		
			string drawingPlayerId = runnerJSON.GetString( "drawingPlayerId" );
			PlayerType player = drawingPlayerId == ChallengeManager.Get.LocalPlayer.UserId ? PlayerType.Local : PlayerType.Remote;
			BattleEntityData entityData = BattleEntityDataFactory.GetEntityFromJson( player, entityJson );

			return new DrawCardData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, drawingPlayerId, entityData );
		}

		private static BattleRunnerData GetComponentTakeDamageData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string defendingPlayerId = runnerJSON.GetString( "defendingPlayerId" );
            int targetSlotId = (int)runnerJSON.GetInt( "targetSlotId" );
            int damageAmount = runnerJSON.GetInt( "damageAmount" ).GetValueOrDefault();

			return new ComponentTakeDamageData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, defendingPlayerId, targetSlotId, damageAmount );
		}

		private static BattleRunnerData GetNanotechPlayedData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string playerId = runnerJSON.GetString( "playerId" );
			SJJson nanotechEntity = runnerJSON.GetObject ( "entity" );
			int drawNumber = nanotechEntity.GetInt( "DrawNumber" ).GetValueOrDefault();

			return new NanotechPlayedData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, playerId, drawNumber, nanotechEntity );
		}

		private static BattleRunnerData GetComponentJettisonData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string playerId = runnerJSON.GetString ( "playerId" );
			int? socketId = (int)runnerJSON.GetInt ( "slotId" );

			return new ComponentJettisonData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, playerId, socketId.GetValueOrDefault() );
		}

		private static BattleRunnerData GetHullDestroyedData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			string playerId = runnerJSON.GetString ( "playerId" );

			return new HullDestroyedData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON, playerId );
		}
			
		private static BattleRunnerData GetDefault( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localTargetingBattleData, PlayerTargetingBattleData remoteTargetingBattleData, SJJson runnerJSON )
		{
			return new BattleRunnerData( id, localPlayerBattleData, remotePlayerBattleData, localTargetingBattleData, remoteTargetingBattleData, runnerJSON );
		}


		private static void ProcessLocalHand(List<SJJson> hand, PlayerBattleData playerBattleData, PlayerTargetingBattleData playerTargetingBattleData)
		{
			if(hand == null)
			{
				// No cards in hand
				return;
			}

			foreach(SJJson battleEntityJSON in hand)
			{
				BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, battleEntityJSON );
				playerBattleData.HandCardData.Add( battleEntityData );

				BattleEntityTargetingData battleEntityTargetingData = BattleEntityTargetingDataFactory.GetEntityFromJson( battleEntityJSON );
				playerTargetingBattleData.BattleEntityTargetingData.Add( battleEntityData.DrawNumber, battleEntityTargetingData );
			}
		}


		private static void ProcessRemoteHand(List<SJJson> hand, PlayerBattleData playerBattleData, PlayerTargetingBattleData playerTargetingBattleData)
		{
			if(hand == null)
			{
				// No cards in hand
				return;
			}

			foreach(SJJson sjJson in hand)
			{
				BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Remote, sjJson );
				playerBattleData.HandCardData.Add( battleEntityData );

				BattleEntityTargetingData battleEntityTargetingData = BattleEntityTargetingDataFactory.GetEntityFromJson( sjJson );
				playerTargetingBattleData.BattleEntityTargetingData.Add( battleEntityData.DrawNumber, battleEntityTargetingData );
			}
		}  

		private static void ProcessBoard(PlayerType player, List<SJJson> board, PlayerBattleData playerBattleData, PlayerTargetingBattleData playerTargetingBattleData)
		{
			if(board == null)
			{
				// No cards on the board
				return;
			}

			foreach(SJJson slotContents in board)
			{
				int slotId = (int)slotContents.GetInt( "slotId" );
				SJJson entityJson = slotContents.GetObject( "entity" );
				List<SJJson> buffsJson = slotContents.GetSJJsonList( "socketBuffs" );

				if(entityJson == null)
				{
					// Remove any existing card from the slot
					playerBattleData.BoardCardData[slotId] = null;
				}
				else
				{
					BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( player, entityJson );
					playerBattleData.BoardCardData[slotId] = battleEntityData;

					BattleEntityTargetingData battleEntityTargetingData = BattleEntityTargetingDataFactory.GetEntityFromJson( entityJson );
					playerTargetingBattleData.BattleEntityTargetingData.Add( battleEntityData.DrawNumber, battleEntityTargetingData );
				}

				List< SocketEffectController.SocketEffect > socketBuffData = new List< SocketEffectController.SocketEffect >();
				foreach(SJJson buffJson in buffsJson)
				{
					string buffType = buffJson.GetString( "type" );
					switch(buffType)
					{
					case "shield":
						socketBuffData.Add( SocketEffectController.SocketEffect.Shield );
						break;

					default:
						SJLogger.LogWarning( "buffType {0} not recognised", buffType);
						break;
					}
				}

				SocketData socketData = new SocketData();
				socketData.Player = player;
				socketData.SocketId = slotId;
				socketData.Effects = socketBuffData.ToArray();

				playerBattleData.SocketData[slotId] = socketData;
			}
		}
		#endregion
	}
}
