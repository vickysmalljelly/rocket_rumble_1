﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DrawCardData : BattleRunnerData
	{
		#region Public Properties
		// The id of the player drawing the card
		public string DrawingPlayerId
		{
			get;
			private set;
		}

		// The draw number of the component we have drawn
		public BattleEntityData EntityData
		{
			get;
			private set;
		}
		#endregion

		#region Constructors
		public DrawCardData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData,PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string drawingPlayerId, BattleEntityData entityData ) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			DrawingPlayerId = drawingPlayerId;
			EntityData = entityData;
		}
		#endregion
	}
}