﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ComponentTakeDamageData : BattleRunnerData
	{
		#region Public Properties
		public string DefendingPlayerId
		{
			get;
			private set;
		}

		public int TargetSlotId
		{
			get;
			private set;
		}

        public int DamageAmount
        {
            get;
            private set;
        }
		#endregion

		public ComponentTakeDamageData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string defendingPlayerId, int targetSlotId, int damageAmount) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			DefendingPlayerId = defendingPlayerId;
			TargetSlotId = targetSlotId;
            DamageAmount = damageAmount;
		}
	}
}