﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class HullDestroyedData : BattleRunnerData
	{
		#region Public Properties
		public string PlayerId
		{
			get;
			private set;
		}
		#endregion

		#region Constructor
		public HullDestroyedData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData,PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON, string playerId ) : base( id,  localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, runnerJSON ) 
		{
			PlayerId = playerId;
		}
		#endregion
	}
}