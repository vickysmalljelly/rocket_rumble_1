﻿namespace SmallJelly
{
    /// <summary>
    /// Defines common data for inbox items.
    /// </summary>
    public abstract class InboxItemData
    {
        public string DisplayName;
        public string Description;
        public string ItemType;
        public string UniqueId;
    }
}