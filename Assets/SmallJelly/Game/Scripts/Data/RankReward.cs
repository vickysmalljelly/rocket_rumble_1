﻿namespace SmallJelly
{
    /// <summary>
    /// Describes how the player is ranking up or down
    /// </summary>
    public class RankReward
    {
        public int StarsAtThisRank;
        public int StarsAtPreviousRank; // Only valid if we are ranking up or down, -1 otherwise
        public int PreviousNumStars;
        public int NewNumStars;
        public string PreviousRankName;
        public string NewRankName;
        public string PreviousTierName;
        public string NewTierName;
        public int PreviousRankNumber;
        public int NewRankNumber;

        // For debug only
        public string PreviousRankId;
        public string NewRankId;
        public string PreviousTierId;
        public string NewTierId;
    }
}