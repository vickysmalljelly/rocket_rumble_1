﻿using System;
using System.Text;

using SmallJelly.Framework;

using UnityEngine;

namespace SmallJelly
{
	[Serializable]	
	public class SocketData
	{
		public PlayerType Player;
		public int SocketId;
		public SocketEffectController.SocketEffect[] Effects;

		public SocketData()
		{
		}

		#region Public Variables


		#endregion
	}
}