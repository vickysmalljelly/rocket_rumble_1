﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class BattleEntityDataFactory
	{
		#region Public Methods
		/// <summary>
		/// Deserialise the appropriate entity type
		/// </summary>
		public static BattleEntityData GetEntityFromJson( PlayerType player, SJJson json )
		{
			string type = json.GetString( "CardType" );

			BattleEntityType battleEntityType;

			//TODO - we should really use a simple "parse" but we need to differentiate between targetable and non-targetable cards! Must be
			//a preferable solution!
			switch(type)
			{
				case "Weapon":
					battleEntityType = BattleEntityType.Weapon;
					break;

				case "Utility": 
					battleEntityType = BattleEntityType.Utility;
					break;

				case "Crew": // Soon to be obsolete
				case "Nanotech":        
					string targeting = json.GetString( "Targeting" );
					battleEntityType = string.IsNullOrEmpty( targeting ) || targeting.Equals( "True" ) ? BattleEntityType.TargetingCrew : BattleEntityType.NonTargetingCrew;
					break;

				case "Virus": 
					battleEntityType = BattleEntityType.Virus;
					break;

				case "Unknown": 
					battleEntityType = BattleEntityType.Unknown;
					break;

				default:
                    SJLogger.LogError( "Card type {0} not recognised, entity {1}", type, json.GetString("Id"));
				    battleEntityType = default( BattleEntityType );
					break;
			}

			BattleEntityData battleEntityData = GetData( json, battleEntityType );
			battleEntityData = AddBuffs( json, battleEntityData );
			battleEntityData.Player = player;

			return battleEntityData;
		}
		#endregion

		#region Private Methods
		private static BattleEntityData GetData( SJJson json, BattleEntityType type )
		{
			switch( type )
			{
				case BattleEntityType.Weapon:
					return JsonUtility.FromJson< WeaponData >( json.JSON );
			
				case BattleEntityType.Utility:
					return JsonUtility.FromJson< UtilityData >( json.JSON );
				
				case BattleEntityType.TargetingCrew:
					return JsonUtility.FromJson< TargetingCrewData >( json.JSON );
				
				case BattleEntityType.NonTargetingCrew:
					return JsonUtility.FromJson< NonTargetingCrewData >( json.JSON );
				
				case BattleEntityType.Virus:
					return JsonUtility.FromJson< VirusData >( json.JSON );
				
				case BattleEntityType.Unknown:
					return JsonUtility.FromJson< UnknownData >( json.JSON );

				case BattleEntityType.NotSet:
					SJLogger.LogError( "Error: Type has not yet been set" );
					return null;
			}

			SJLogger.LogError( "Error: Type {0} does not parse any data", type.ToString() );

			return null;
		}

		private static BattleEntityData AddBuffs( SJJson json, BattleEntityData battleEntityData )
		{
			List< BattleEntityController.Buff > battleEntityBuffData = new List< BattleEntityController.Buff >();

			List<SJJson> buffsJson = new List<SJJson>();
			if( json.ContainsKey( "permBuffs" ) )
			{
				buffsJson.AddRange( json.GetSJJsonList( "permBuffs" ) );
			}

			if( json.ContainsKey( "tempBuffs" ) )
			{
				buffsJson.AddRange( json.GetSJJsonList( "tempBuffs" ) );
			}

			foreach(SJJson buffJson in buffsJson)
			{
				string buffType = buffJson.GetString( "type" );
				switch(buffType)
				{
				case "targetLock":
					battleEntityBuffData.Add( BattleEntityController.Buff.TargetLock );
					break;

				case "attack":
					battleEntityBuffData.Add( BattleEntityController.Buff.DamageBuff );
					break;

				case "maxHP":
					battleEntityBuffData.Add( BattleEntityController.Buff.HealthBuff );
					break;

                case "doubleShot":
                    // TODO!
                    break;

                case "cost":
                    // Ignore cost buff, only applies to cards in the hand.
                    break;

				default:
					SJLogger.LogWarning( "buffType {0} not recognised", buffType);
					break;
				}
			}
			battleEntityData.Buffs = battleEntityBuffData.ToArray();

			return battleEntityData;
		}
		#endregion
	}
}