﻿using System.Collections.Generic;
using System.Text;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
    /// <summary>
    /// Describes in-app purchases for Rocket Rumble
    /// </summary>
    public class IapData 
    {
        /// <summary>
        /// Available currency types.
        /// </summary>
        public enum Currency
        {
            NotSet,
            Cash,
            Credits,
            Crystals
        }

        public enum IapType
        {
            NotSet,
            StandardCardPacks,
            Crystals
        }

        /// <summary>
        /// Associates a price amount with a currency.
        /// </summary>
        public class Price
        {
            public Price(Currency currency, long amount)
            {
                Currency = currency;
                Amount = amount;
                AmountToDisplay = amount.ToString();
            }

            public Price(Currency currency, string amount)
            {
                Currency = currency;
                Amount = -1; // We don't record the value of real world currency
                AmountToDisplay = amount;
            }

            public Currency Currency { get; private set; }
            public long Amount { get; private set; }
            public string AmountToDisplay { get; private set; }
        }

        public string Name;
        public string ShortCode;
        public string Description;
        public IapType ItemType = IapType.StandardCardPacks;

        public string GooglePlayProductId { get; set; }

        private List<Price> mPrices = new List<Price>();

		public bool HasPriceInCredits()
		{
			return mPrices.FirstOrDefault( x => x.Currency == Currency.Credits ) != default( Price );
		}
			
		public string GetDisplayPriceInCredits()
        {
            Price price = mPrices.Find(x => x.Currency == Currency.Credits);
            if(price == null)
            {
                SJLogger.Assert("{0} does not have a price in credits", this.ShortCode);
                return string.Empty;
            }

            return price.AmountToDisplay;
        }

        public long GetPriceInCredits()
        {
            Price price = mPrices.Find(x => x.Currency == Currency.Credits);
            if(price == null)
            {
                SJLogger.Assert("{0} does not have a price in credits", this.ShortCode);
                return -1;
            }

            return price.Amount;
        }

		public bool HasPriceInCrystals()
		{
			return mPrices.FirstOrDefault( x => x.Currency == Currency.Crystals ) != default( Price );
		}

		public string GetDisplayPriceInCrystals()
        {
            Price price = mPrices.Find( x => x.Currency == Currency.Crystals );
            if(price == null)
            {
                SJLogger.Assert("{0} does not have a price in crystals", this.ShortCode);
                return string.Empty;
            }

            return price.AmountToDisplay;
        }

        public long GetPriceInCrystals()
        {
            Price price = mPrices.Find( x => x.Currency == Currency.Crystals );
            if(price == null)
            {
                SJLogger.Assert("{0} does not have a price in crystals", this.ShortCode);
                return -1;
            }

            return price.Amount;
        }

		public bool HasPriceInCash()
		{
			return mPrices.FirstOrDefault( x => x.Currency == Currency.Cash ) != default( Price );
		}

		public string GetPriceInCash()
		{
			Price price = mPrices.Find( x => x.Currency == Currency.Cash );
			if(price == null)
			{
				SJLogger.Assert("{0} does not have a price in crystals", this.ShortCode);
				return string.Empty;
			}

			return price.AmountToDisplay;
		}

        public void AddPrice(Currency currency, long amount)
        {
            mPrices.Add(new Price(currency, amount));
        }
            
        public void AddGooglePlayPrice(Currency currency, string amount, string productId)
        {
            GooglePlayProductId = productId;
            mPrices.Add(new Price(currency, amount));
        }

        public override string ToString()
        {
            StringBuilder prices = new StringBuilder();
            foreach(Price price in mPrices)
            {
                prices.AppendFormat("{0}: {1}, ", price.Currency, price.AmountToDisplay);
            }

            return string.Format("[IapData] {0}, {1}, {2}, {3}", Name, ShortCode, Description, prices.ToString());
        }
    }
}
