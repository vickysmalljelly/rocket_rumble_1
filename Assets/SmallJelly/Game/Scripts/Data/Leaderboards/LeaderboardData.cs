﻿using System;

namespace SmallJelly
{
    /// <summary>
    /// Represents a GameSparks Leaderboard
    /// </summary>
    public class LeaderboardData 
    {
        public string Name;
        public string ShortCode 
        { 
            get
            {
                return mShortCode;
            }
            set
            {
                mShortCode = value;
                mSplitShortCode = mShortCode.Split('.');
            }
        }            

        private string mShortCode;
        /// Leaderboards with partitions are accessed by a concatenation of the name and partitions
        private string[] mSplitShortCode;

        public string GetShortCodePartition(int index)
        {
            if(mSplitShortCode.Length <= index)
            {
                return string.Empty;
            }

            return mSplitShortCode[index];
        }

        public long GetTimestampForWeek(int timeStampPosition)
        {
            if(mSplitShortCode.Length < timeStampPosition + 1)
            {
                return 0;
            }
                
            return Convert.ToInt64(mSplitShortCode[timeStampPosition]);
        }

        public override string ToString()
        {
            return string.Format("[LeaderboardData] Name - {0}, ShortCode - {1}", Name, ShortCode);
        }
    }
}