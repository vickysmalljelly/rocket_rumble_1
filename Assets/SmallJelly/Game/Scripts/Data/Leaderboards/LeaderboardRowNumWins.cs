﻿using System;

namespace SmallJelly
{
    public class LeaderboardRowNumWins : LeaderboardRow
    {
        public long NumWins;

        public override string ToString()
        {
            return string.Format("[LeaderboardRowNumWins] NumWins {0}, {1}", NumWins, base.ToString());
        }
    }
}
