﻿using System;

namespace SmallJelly
{
    public class LeaderboardRow 
    {
        public string UserId;
        public string DisplayName;
        public DateTime Time;
        public string Country;
        public long Rank;

        public override string ToString()
        {
            return string.Format("[LeaderboardRow] UserId {0}, DisplayName {1}, Time {2}, Country {3}, Rank {4}", UserId, DisplayName, Time.ToShortDateString(), Country, Rank);
        }
    }
}
