﻿using System;

namespace SmallJelly
{
	/// <summary>
	/// Encapsulates information identifying a News Item.
	/// </summary>
	[Serializable]
	public class NewsItem
	{
		public string Id;
		public string Date;
		public string Title;
		public string Content;
	}
}