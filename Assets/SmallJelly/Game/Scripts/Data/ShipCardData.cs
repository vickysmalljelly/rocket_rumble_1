﻿namespace SmallJelly
{
    /// <summary>
    /// Data associated with a card representing a ship that is stored in the database.
    /// </summary>
    public class ShipCardData
    {
        public string Id;

        public string DisplayName;

        public string Class;
    }
}
