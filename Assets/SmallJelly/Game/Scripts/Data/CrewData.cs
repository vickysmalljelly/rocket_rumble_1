﻿using System;

namespace SmallJelly
{
	[Serializable]	
	public class CrewData : BattleEntityData
	{
		public CrewData( BattleEntityType battleEntityType ) : base( battleEntityType ) {}
	}
}