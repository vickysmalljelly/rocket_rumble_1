﻿using UnityEngine;

using System;

using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// An entity that is only showing it's card back.
    /// Will still have the same fields as BattleEntityData, but they will be set to the default values.
	/// </summary>
	[Serializable]	
	public class UnknownData : BattleEntityData
	{
		public UnknownData() : base(BattleEntityType.Unknown) {}

		public override string ToString()
		{
			string baseString = base.ToString();
			string derivedString = string.Format("[UnknownData]");
			return baseString + " " + derivedString;
		}
	}
}