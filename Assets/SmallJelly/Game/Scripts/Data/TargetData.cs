﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Target data.
    /// </summary>
    public struct TargetData 
    {
        public enum TargetType
        {
            NotSet,
            Hull,
            Socket
        }
            
        /// <summary>
        /// The type of target.
        /// </summary>
        public TargetType TypeOfTarget;

        /// <summary>
        /// The id of the player being targeted
        /// </summary>
        public string PlayerId;
       
        /// <summary>
        /// Only used if the target is a socket
        /// </summary>
        public int SocketId;

        public string GetTargetType()
        {
            switch(TypeOfTarget)
            {
                case TargetType.Hull:
                    return "hull";
                case TargetType.Socket:
                    return "socket";
                default:
                    SJLogger.LogError("TypeOfTarget: {0} not recognised", TypeOfTarget);
                    return string.Empty;
            }
        }
    }
}
