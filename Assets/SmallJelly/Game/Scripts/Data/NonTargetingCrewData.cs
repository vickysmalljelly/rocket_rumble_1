﻿using System;

namespace SmallJelly
{
	[Serializable]	
	public class NonTargetingCrewData : CrewData
	{
		public NonTargetingCrewData() : base( BattleEntityType.NonTargetingCrew ) {}
	}
}