﻿using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	[Serializable]	
	public class TargetingCrewData : CrewData
	{
		#region Constructors
		public TargetingCrewData( ) : base( BattleEntityType.TargetingCrew ) {}
		#endregion
	}
}