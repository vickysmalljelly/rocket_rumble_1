﻿namespace SmallJelly
{
    /// <summary>
    /// Filter for cards used in battles.
    /// By default, all cards will be returned.
    /// </summary>
    public class BattleCardFilter
    {
        /// <summary>
        /// The class of cards to filter by
        /// </summary>
        public string ShipClass = "any";

        /// <summary>
        /// If -1, no power filter is applied
        /// </summary>
        public int Power = -1;

        /// <summary>
        /// The type of the component, e.g. Weapon, Nanotech, Utility
        /// </summary>
        public string ComponentType = "any";
    }
}