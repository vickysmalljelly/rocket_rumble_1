﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class GetDeckCollectionEventData : CollectionEventData 
	{
		#region Public Properties
		public Deck Deck
		{
			get
			{
				return mDeck;
			}
		}
		#endregion

		#region Member Variables
		private readonly Deck mDeck;
		#endregion

		#region Constructors
		public GetDeckCollectionEventData( Deck deck )
		{
			mDeck = deck;
		}
		#endregion
	}
}
