﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class GetCardsCollectionEventData : CollectionEventData 
	{
		#region Public Properties
		public int Page
		{
			get
			{
                return mResult.PageNum;
			}
		}

		public int NumberOfPages
		{
			get
			{
                return mResult.NumPages;;
			}
		}
			
		public bool FirstPage
		{
			get
			{
                return mResult.FirstPage;
			}
		}

		public bool LastPage
		{
			get
			{
                return mResult.LastPage;
			}
		}

		public CollectionBattleEntityData[] Cards
		{
			get
			{
				return mResult.Cards;
			}
		}

        public CollectionManager.Direction Direction
        {
            get
            {
                return mResult.Direction;
            }
        }
		#endregion

		#region Member Variables
        private readonly CollectionQueryResult mResult;
		#endregion

		#region Constructors
		public GetCardsCollectionEventData( CollectionQueryResult result)
		{
            mResult = result;
		}
		#endregion
	}
}
