﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class GetDecksCollectionEventData : CollectionEventData 
	{
		#region Public Properties
		public Deck[] Decks
		{
			get
			{
				return mDecks;
			}
		}
		#endregion

		#region Member Variables
		private readonly Deck[] mDecks;
		#endregion

		#region Constructors
		public GetDecksCollectionEventData( Deck[] decks )
		{
			mDecks = decks;
		}
		#endregion
	}
}
