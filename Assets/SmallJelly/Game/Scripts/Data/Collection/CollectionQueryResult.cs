﻿namespace SmallJelly
{
    /// <summary>
    /// Encapsulates the data returned from the server when querying the player's collection.
    /// </summary>
    public struct CollectionQueryResult
    {
        public CollectionBattleEntityData[] Cards;
        public bool FirstPage;
        public bool LastPage;
        public int NumPages;
        public int PageNum;
        // The direction in which the cards should be scrolled with the next update
        public CollectionManager.Direction Direction;
    }
}