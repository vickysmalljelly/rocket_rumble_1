﻿using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	[Serializable]  
	public class ComponentData : BattleEntityData
	{
		#region Serializable
		public int HP;
		public int MaxHP;
		#endregion

		public ComponentData( BattleEntityType battleEntityType ) : base( battleEntityType ) {}

	}
}
