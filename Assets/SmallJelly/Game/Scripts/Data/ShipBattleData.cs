﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Data to be displayed for a ship during a battle.
	/// </summary>
	[Serializable]
	public class ShipBattleData
	{
		public PlayerType Player;

		/// <summary>
		/// The amount of HP the hull has left.  
		/// </summary>
		public int HullHp;

		/// <summary>
		/// The maximum value of the hull HP.
		/// </summary>
		public int MaxHullHp;
	}
}
