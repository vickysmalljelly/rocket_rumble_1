﻿using System.Collections.Generic;

namespace SmallJelly
{
	public class ListItemsInboxEventData : InboxEventData
	{
		#region Public Properties
		public List< InboxItemData > InboxItemData
		{
			get
			{
				return mInboxItemData;
			}
		}
		#endregion

		#region Member Variables
		private List< InboxItemData > mInboxItemData;
		#endregion

		#region Constructors
		public ListItemsInboxEventData( List< InboxItemData > inboxItemData )
		{
			mInboxItemData = inboxItemData;
		}
		#endregion
	}
}
