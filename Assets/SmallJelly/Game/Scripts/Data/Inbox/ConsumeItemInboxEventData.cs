﻿using System.Collections.Generic;

namespace SmallJelly
{
	public class ConsumeItemInboxEventData : InboxEventData
	{
		#region Public Properties
		public RewardData RewardData
		{
			get
			{
				return mRewardData;
			}
		}
		#endregion

		#region Member Variables
		private RewardData mRewardData;
		#endregion

		#region Constructors
		public ConsumeItemInboxEventData( RewardData rewardData )
		{
			mRewardData = rewardData;
		}
		#endregion
	}
}
