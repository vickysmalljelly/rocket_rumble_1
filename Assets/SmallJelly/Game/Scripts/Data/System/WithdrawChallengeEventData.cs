﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class WithdrawChallengeEventData : ChallengeSetupEventData
	{

		#region Constructors
		public WithdrawChallengeEventData() : base()
		{
		}
		#endregion
	}
}
