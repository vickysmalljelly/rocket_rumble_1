﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class ListDecksEventData : ChallengeSetupEventData
	{
		#region Public Properties
		public List< Deck > Decks
		{
			get;
			private set;
		}
		#endregion

		#region Constructors
		public ListDecksEventData( List< Deck > decks ) : base()
		{
			Decks = decks;
		}
		#endregion
	}
}
