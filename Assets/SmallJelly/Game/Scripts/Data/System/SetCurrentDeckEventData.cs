﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	[Serializable]
	public class SetCurrentDeckEventData : ChallengeSetupEventData
	{

		#region Constructors
		public SetCurrentDeckEventData() : base()
		{
		}
		#endregion
	}
}
