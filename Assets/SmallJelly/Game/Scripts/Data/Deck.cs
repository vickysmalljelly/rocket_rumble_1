﻿using System;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Encapsulates information identifying a deck.
	/// </summary>
	[Serializable]
	public class Deck {

		public string DeckId;
		public string DisplayName;
        public string Class;
		public List<string> CardIds;
		public List<BattleEntityData> BattleEntities;
	}
}