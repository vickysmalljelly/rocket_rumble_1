﻿using System.Collections.Generic;
using GameSparks.Api.Responses;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Contains virtual goods owned by the player.
    /// </summary>
    public class VirtualGoods 
    {
        public VirtualGoods(AccountDetailsResponse response)
        {
            Credits = response.Currency1.GetValueOrDefault();
            Crystals = response.Currency2.GetValueOrDefault();
             
            SJLogger.LogMessage(MessageFilter.Gameplay, "Credits: {0}", Credits);
            SJLogger.LogMessage(MessageFilter.Gameplay, "Crystals: {0}", Crystals);

            SJJson goods = new SJJson(response.VirtualGoods);

            if(goods.ContainsKey("XelionGas"))
            {
                int? gas = goods.GetInt("XelionGas");
                mGoods.Add("XelionGas", gas.GetValueOrDefault());
                SJLogger.LogMessage(MessageFilter.Gameplay, "XelionGas: {0}", gas.GetValueOrDefault());
            }
        }
            
        private Dictionary<string, int> mGoods = new Dictionary<string, int>();

        public long Credits { get; set; }

        public long Crystals { get; set; }
    }
}
