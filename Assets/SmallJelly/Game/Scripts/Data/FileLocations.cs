﻿using System.IO;

using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// All file locations for the project should be stored here.
	/// </summary>
	public static class FileLocations 
	{
		#region Game Prefabs
		public static string SmallJellyPrefabs { get { return "Prefabs"; } }
		public static string SmallJellyMapPrefabs { get { return SmallJellyPrefabs + Path.DirectorySeparatorChar + "Map"; } }
		public static string SmallJellyUIPrefabs { get { return SmallJellyPrefabs + Path.DirectorySeparatorChar + "UI"; } }
		public static string SmallJellyBattlePrefabs { get { return SmallJellyPrefabs + Path.DirectorySeparatorChar + "Battle"; } }
		public static string SmallJellyInboxPrefabs { get { return SmallJellyPrefabs + Path.DirectorySeparatorChar + "Inbox"; } }
		public static string SmallJellyBuffPrefabs { get { return SmallJellyBattlePrefabs + Path.DirectorySeparatorChar + "Buffs"; } }
		public static string SmallJellyCoinTossPrefabs { get { return SmallJellyBattlePrefabs + Path.DirectorySeparatorChar + "CoinToss"; } }
		public static string SmallJellyMulliganPrefabs { get { return SmallJellyBattlePrefabs + Path.DirectorySeparatorChar + "Mulligan"; } }
		public static string SmallJellyButtonPrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyPrefabs, Path.DirectorySeparatorChar, "Buttons" ); } }
		public static string SmallJellyParticleEffectPrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyPrefabs, Path.DirectorySeparatorChar, "ParticleEffects" ); } }
		public static string SmallJellyShopParticleEffectPrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyParticleEffectPrefabs, Path.DirectorySeparatorChar, "Shop" ); } }
		public static string SmallJellyBattleParticleEffectPrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyParticleEffectPrefabs, Path.DirectorySeparatorChar, "Battle" ); } }
		public static string SmallJellyBattleComponentParticleEffectPrefabs { get { return string.Format( "{0}{1}{2}{3}{4}", FileLocations.SmallJellyBattleParticleEffectPrefabs, Path.DirectorySeparatorChar, "Components", Path.DirectorySeparatorChar, "Components" ); } }
		public static string SmallJellyShipPrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyPrefabs, Path.DirectorySeparatorChar, "Ships" ); } }
		public static string SmallJellyResourcePrefabs { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyPrefabs, Path.DirectorySeparatorChar, "VirtualGoods" ); } }

		public static string CoinTossPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "CoinToss" ); } }
		public static string BattlePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "Battle" ); } }
		public static string BattleOnboardingPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "BattleOnboarding" ); } }
		public static string BattleIntroPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "BattleIntro" ); } }
		public static string BattleMulliganPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "BattleMulligan" ); } }
		public static string BattleGameplayPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "BattleGameplay" ); } }
		public static string BattleResultPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattlePrefabs, Path.DirectorySeparatorChar, "BattleResult" ); } }

		public static string MapPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "Map" ); } }
		public static string MapOnboardingPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "MapOnboarding" ); } }
		public static string MapMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_mapMenu" ); } }

		public static string InboxPackPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyInboxPrefabs, Path.DirectorySeparatorChar, "Pack" ); } }

		public static string NewsItemPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "NewsItem" ); } }

		public static string PlayMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_startMenu" ); } }
        public static string ChallengesMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_challenges" ); } }
        public static string LootRunsMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_lootRuns" ); } }
        public static string LeaderboardsMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_leaderboards" ); } }
        public static string LeaderboardsLinePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_leaderboardLine_other" ); } }
        public static string LeaderboardsPlayerLinePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_leaderboardLine_player" ); } }
		public static string UsernameEntryMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_username_entry" ); } }
		public static string ChooseCustomDeckMenuPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_chooseCustomDeck" ); } }
		public static string CollectionsPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_collection" ); } }
		public static string ShopPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_shop" ); } }
		public static string ShopProductInformationPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_shopProductInformation" ); } }
		public static string InboxPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_inbox" ); } }
        public static string DemoIntroPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_demoIntro" ); } }

		public static string OnboardingStartScreen { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_onboardingStartScreen" ); } }

		public static string DirectUserFromPlayScreenToShopPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_directFromPlayToShop" ); } }
		public static string DirectUserFromPlayScreenToInboxPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_directFromPlayToInbox" ); } }
		public static string DirectUserFromPlayScreenToMapPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_directFromPlayToMap" ); } }
		public static string DirectUserFromShopScreenToInboxPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_directFromShopToInbox" ); } }

		public static string BuyPackStepOnePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_buyPackStep1" ); } }
		public static string BuyPackStepTwoPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_buyPackStep2" ); } }
		public static string BuyPackStepThreePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_buyPackStep3" ); } }

		public static string OpenPackStepOnePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_openPackStep1" ); } }
		public static string OpenPackStepTwoPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_openPackStep2" ); } }
		public static string OpenPackStepThreePrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_openPackStep3" ); } }

		public static string FinishOnboardingPrefab { get { return string.Format( "{0}{1}{2}", FileLocations.SmallJellyUIPrefabs, Path.DirectorySeparatorChar, "ui_finishOnboarding" ); } }
		#endregion

		#region Game Images
		public static string SmallJellyImages { get { return "Images"; } }
		public static string SmallJellyCharacterImages { get { return SmallJellyImages + Path.DirectorySeparatorChar + "Characters"; } }
		#endregion

		#region Game Animations
		public static string SmallJellyPlayMaker { get { return "PlayMaker"; } }
		public static string SmallJellyPlayMakerTemplates { get { return string.Format("{0}{1}{2}", SmallJellyPlayMaker, Path.DirectorySeparatorChar, "Templates" ); } }
		public static string SmallJellyCoinTossPlayMakerTemplates { get { return string.Format("{0}{1}{2}{3}", SmallJellyPlayMakerTemplates, Path.DirectorySeparatorChar, "CoinToss", Path.DirectorySeparatorChar ); } }
		public static string SmallJellyMulliganPlayMakerTemplates { get { return string.Format("{0}{1}{2}{3}", SmallJellyPlayMakerTemplates, Path.DirectorySeparatorChar, "Mulligan", Path.DirectorySeparatorChar ); } }
		public static string SmallJellySocketsPlayMakerTemplates { get { return string.Format("{0}{1}{2}{3}", SmallJellyPlayMakerTemplates, Path.DirectorySeparatorChar, "Sockets", Path.DirectorySeparatorChar ); } }
		public static string SmallJellyMulliganCardsPlayMakerTemplates { get { return string.Format("{0}{1}{2}", SmallJellyMulliganPlayMakerTemplates, "Cards", Path.DirectorySeparatorChar ); } }
		#endregion

        #region Game Data
        public static string AiNames { get { return "GameData" + Path.DirectorySeparatorChar + "AiNames"; } }
        #endregion

		#region Editor & Debug	

        // Top level folder containing game data to be loaded for editing
        private static string GameData { get { return FileLocations.SmallJellyResources + Path.DirectorySeparatorChar + "GameData" + Path.DirectorySeparatorChar; } }

		// Top level folder containing tools data (eg. snapshot config) to be imported from resources
		public static string ToolsData { get { return Application.dataPath + Path.DirectorySeparatorChar + "SmallJelly" + Path.DirectorySeparatorChar + "Game" + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "ToolsData" + Path.DirectorySeparatorChar; } }

		// Tools files
		public static string SnapshotConfigFile { get { return FileLocations.ToolsData + "SnapshotConfig.txt"; } }

		public static string DebugCollectionFile { get { return FileLocations.GameData + Path.DirectorySeparatorChar + "CollectionConfig.txt"; } }

		public static string DebugDirectory { get { return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Debug"; } }

		// Relative to SmallJelly/Game/Resources
		public static string DefaultDeckResource { get { return "GameData" + Path.DirectorySeparatorChar + "Default"; } }
		public static string DebugDeckResource { get { return "GameData" + Path.DirectorySeparatorChar + "DebugDeck"; } }
		public static string AIDebugDeckResource { get { return "GameData" + Path.DirectorySeparatorChar + "AIDebugDeck"; } }
		public static string DebugCollectionResource { get { return "GameData" + Path.DirectorySeparatorChar + "CollectionConfig"; } }
		public static string CollectionConfig { get { return "GameData" + Path.DirectorySeparatorChar + "Collection"; } }
		public static string FourByThreeCollectionConfig { get { return CollectionConfig + Path.DirectorySeparatorChar + "4x3CollectionDisplayConfig"; } }
		public static string SixteenByNineCollectionConfig { get { return CollectionConfig + Path.DirectorySeparatorChar + "16x9CollectionDisplayConfig"; } }
        public static string DebugServerConfig { get { return "GameData" + Path.DirectorySeparatorChar + "DebugServerConfig"; } }

        // Small Jelly Resources
        public static string SmallJellyResources { get { return Application.dataPath + Path.DirectorySeparatorChar + "SmallJelly" + Path.DirectorySeparatorChar + "Game" + Path.DirectorySeparatorChar + "Resources"; } }

        // Location for replays in AutomatedTests
        public static string AutomatedTestsDirectory { get { return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "AutomatedTests" + Path.DirectorySeparatorChar; } }
        public static string BattleStateComparisonsDirectory { get { return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "BattleStateComparisons"; } }

		public static string G2UJson { get { return Application.dataPath + Path.DirectorySeparatorChar + "Google2UGen" + Path.DirectorySeparatorChar + "JSON"; } }

        public static string DebugConfigFile { get { return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Debug" + Path.DirectorySeparatorChar + "RRDebugConfig.xml"; } }
       
        public static string AiDebugFile { get { return Application.dataPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Debug" + Path.DirectorySeparatorChar + "AiDebug.json"; } }
		#endregion

		#region Battle Entities
		public static string CardBodyModel { get { return "Models/Cards/Body/card_{0}_body"; } }
		public static string CardBodyMaterial { get { return "Materials/Cards/Body/card_{0}_body_{1}"; } }

		public static string CardBackModel { get { return "Models/Cards/Back/card_back001"; } }
		public static string CardBackMaterial { get { return "Materials/Cards/Back/card_back001"; } }

		public static string CardInteriorModel { get { return "Models/Cards/Interior/card_{0}_tinted"; } }
		public static string CardInteriorMaterial { get { return "Materials/Cards/Interior/card_{0}_tinted"; } }

		public static string CardTemplateModel { get { return "Prefabs/Cards/card_{0}_template"; } }

		public static string CardRarityModel { get { return "Models/Cards/Rarity/card_{0}_rarity"; } }
		public static string CardRarityMaterial { get { return "Materials/Cards/Rarity/card_{0}_rarity_{1}"; } }

		public static string CardStatBackgroundModel { get { return "Models/Cards/Stats/card_{0}_statBackground"; } }
		public static string CardStatBackgroundMaterial { get { return "Materials/Cards/Stats/card_statBackground"; } }

		public static string CardStatFrameModel { get { return "Models/Cards/Stats/card_{0}_statFrame"; } }
		public static string CardStatFrameMaterial { get { return "Materials/Cards/Stats/card_{0}_statFrame_{1}"; } }

		public static string CardNameplateModel { get { return "Models/Cards/Nameplate/card_nameplate"; } }
		public static string CardNameplateMaterial { get { return "Materials/Cards/Nameplate/card_{0}_nameplate"; } }

		public static string CommonRarityFileCode { get { return "common"; } }
		public static string RareRarityFileCode { get { return "rare"; } }
		public static string EpicRarityFileCode { get { return "epic"; } }
		public static string LegendaryRarityFileCode { get { return "legendary"; } }

		public static string NeutralClassFileCode { get { return "neu"; } }
		public static string AncientClassFileCode { get { return "anc"; } }
		public static string SmugglerClassFileCode { get { return "smu"; } }
		public static string EnforcerClassFileCode { get { return "enf"; } }

		public static string WeaponFileCode { get { return "weapon"; } }
		public static string UtilityFileCode { get { return "utility"; } }
		public static string NanotechFileCode { get { return "nanotech"; } }
		#endregion
	}
}