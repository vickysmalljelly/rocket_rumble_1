﻿using System;

namespace SmallJelly
{
    [Serializable]  
    public class VirusData : BattleEntityData
    {
        public VirusData() : base(BattleEntityType.Virus) {}

    }
}