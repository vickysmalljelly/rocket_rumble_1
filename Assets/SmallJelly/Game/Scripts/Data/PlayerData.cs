﻿using System;
using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Encapsulates persistent data about the player.
    /// </summary>
    public class PlayerData 
    {
        /// <summary>
        /// To suport analytics
        /// </summary>
        public enum SourceOfResource
        {
            NotSet,
            Reward,
            BoughtInShop
        }

        /// <summary>
        /// To support analytics
        /// </summary>
        public enum SinkOfResource
        {
            NotSet,
            SpentInShop
        }

        #region Public Variables
		public string RankName { get; set; }
        public int RankNumber { get; set; }
		public string TierName { get; set; }
		public int StarsAtThisRank { get; set; }
		public int TotalStarsAtThisRank { get; set; }
        public VirtualGoods Goods { get { return mGoods; } }
        public string AiPlayerId { get { return mAiPlayerId; } }
        public bool NewInboxItem { get; set; }
        public bool NewCards { get; set; }
        public bool FreePackAvailable { get; set; }
        public TimeSpan TimeLeftUntilFreePack { get { return mFreePackTimeLeft; } }
        #endregion

        #region Private Variables
        private VirtualGoods mGoods;
        private List<string> mOnboardingCheckPoints = new List<string>();
        private string mAiPlayerId;
        private TimeSpan mFreePackTimeLeft;
        private DateTime mNextFreePackUpdate;
        //private readonly int mFreePackBufferInMinutes = 10;
        private readonly int mFreePackBufferInMinutes = 2;
        private bool mFreePackSetOnClient;
        #endregion

        public PlayerData(VirtualGoods goods, SJJson scriptData)
        {
            SJLogger.AssertCondition(scriptData != null, "PlayerData constructor must have scriptData");
            SJLogger.LogMessage(MessageFilter.Gameplay, "Updating player data from server");

            mGoods = goods;

            mAiPlayerId = scriptData.GetString("aiPlayerId");

            SJJson onboarding = scriptData.GetObject("onboarding");
            if(onboarding != null)
            {
                mOnboardingCheckPoints = onboarding.GetStringList("checkPoints");
            }
            else
            {
                mOnboardingCheckPoints = new List<string>();
            }

            SJJson rank = scriptData.GetObject("rank");
            SJLogger.AssertCondition(rank != null, "rank not found in script data");
            if(rank != null)
            {
				RankName = rank.GetString("rankName");
                RankNumber = rank.GetInt("rank").GetValueOrDefault();
				TierName = rank.GetString("tierName");
				StarsAtThisRank = rank.GetInt("starsAtThisRank").GetValueOrDefault();
				TotalStarsAtThisRank = rank.GetInt("totalStarsAtThisRank").GetValueOrDefault();
            }

            ProcessFreePack(scriptData);

            ProcessFeatureFlags(scriptData);

            ProcessNewItems(scriptData);

            SJLogger.LogMessage(MessageFilter.Gameplay, "Tier: {3}, Rank: {0}, RankNumber: {4}, num stars {1}, total stars {2}", RankName, StarsAtThisRank, TotalStarsAtThisRank, TierName, RankNumber);
            SJLogger.LogMessage(MessageFilter.Gameplay, "Checkpoints: {0}", string.Join(", ", mOnboardingCheckPoints.ToArray()));
        }

        public void Update()
        {
            mFreePackTimeLeft = mNextFreePackUpdate - DateTime.Now;

            if(mFreePackSetOnClient)
            {
                // Don't want to update again until this has been cleared by an update from the server
                return;
            }                

            if(mFreePackTimeLeft < TimeSpan.Zero)
            {
                mFreePackTimeLeft = TimeSpan.Zero;
                GameManager.Get.UpdateFreePackAvailableOnClient();
            }
        }

        public bool HasPassedCheckpoint(string checkPoint)
        {
            if(!FeatureFlags.Onboarding) 
            {
                return true;
            }

            SJLogger.LogMessage(MessageFilter.Onboarding, "Checkpoint {0} is {1}", checkPoint, mOnboardingCheckPoints.Contains(checkPoint));

            return mOnboardingCheckPoints.Contains(checkPoint);
        }

        public void SetCheckpointOnClient(string checkpoint) 
        {
            if(!mOnboardingCheckPoints.Contains(checkpoint))
            {
                SJLogger.LogMessage(MessageFilter.Onboarding, "Setting checkpoint {0} on client", checkpoint);
                mOnboardingCheckPoints.Add(checkpoint);
            }
        }

        public void AddToCredits(long credits, SourceOfResource source)
        {
            mGoods.Credits += credits;
            AnalyticsManager.Get.RecordAddResourceEvent(IapData.Currency.Credits, (int)credits, source.ToString(), "credits");
        }

        public void RemoveFromCredits(long credits, SinkOfResource sink)
        {
            mGoods.Credits -= credits;
            AnalyticsManager.Get.RecordSubtractResourceEvent(IapData.Currency.Credits, (int)credits, sink.ToString(), "credits");
        }

        public void AddToCrystals(long crystals, SourceOfResource source)
        {
            mGoods.Crystals += crystals;
            AnalyticsManager.Get.RecordAddResourceEvent(IapData.Currency.Crystals, (int)crystals, source.ToString(), "crystals");
        }

        public void RemoveFromCrystals(long crystals, SinkOfResource sink)
        {
            mGoods.Crystals -= crystals;
            AnalyticsManager.Get.RecordSubtractResourceEvent(IapData.Currency.Crystals, (int)crystals, sink.ToString(), "crystals");
        }

        public long GetCredits()
        {
            return mGoods.Credits;
        }

        public void SetFreePackAvailableOnClient()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Setting free pack available on client.");
            FreePackAvailable = true;
            mFreePackSetOnClient = true;
        }

        public void SetFreePackClaimedOnClient()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "Setting free pack claimed on client.");
            FreePackAvailable = false;
            mNextFreePackUpdate = DateTime.Now.AddDays(1);

            GameManager.Get.UpdateNewInboxItemOnClient(true);
        }

        private void ProcessFreePack(SJJson scriptData)
        {
            mFreePackSetOnClient = false;

            FreePackAvailable = scriptData.GetBoolean("freePackAvailable").GetValueOrDefault();

            if(FreePackAvailable)
            {
                SJLogger.LogMessage(MessageFilter.Gameplay, "From server: free pack available");
            }
            else
            {
                double timeLeftInMilliseconds = scriptData.GetInt("freePackTimeLeft").GetValueOrDefault();

                if(timeLeftInMilliseconds == -1)
                {
                    SJLogger.LogError("Data from server - time left not set");
                    mNextFreePackUpdate = DateTime.Now;
                }

                TimeSpan timeLeft = TimeSpan.FromMilliseconds(timeLeftInMilliseconds);
                int hoursLeft = timeLeft.Hours;
                int minutesLeft = (timeLeft.Minutes % 60);

                mNextFreePackUpdate = DateTime.Now.AddHours(hoursLeft).AddMinutes(minutesLeft + mFreePackBufferInMinutes);
                mFreePackTimeLeft = mNextFreePackUpdate - DateTime.Now;

                SJLogger.LogMessage(MessageFilter.Gameplay, "From server: - time left until free pack H: {0}, M: {1}", hoursLeft, minutesLeft);
            }
        }

        private void ProcessFeatureFlags(SJJson scriptData)
        {
            List<SJJson> featureFlags = scriptData.GetSJJsonList("featureFlags");
            List<FeatureFlag> list = new List<FeatureFlag>();

            foreach(SJJson flag in featureFlags)
            {
                FeatureFlag flagToAdd = new FeatureFlag(flag.GetString("featureName"), (string.Compare("TRUE", flag.GetString("featureStatus")) == 0), flag.GetString("param1"));
                list.Add(flagToAdd);
            }

            FeatureFlags.UpdateFlags(list);
        }

        private void ProcessNewItems(SJJson scriptData)
        {
            NewInboxItem = scriptData.GetBoolean("newInboxItem").GetValueOrDefault();
            NewCards = scriptData.GetBoolean("newCards").GetValueOrDefault();

            SJLogger.LogMessage(MessageFilter.Gameplay, "From server - NewInboxItem = {0}, NewCards = {1}", NewInboxItem, NewCards);
        }
     }
}
