﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.IO;

namespace SmallJelly
{
	//A factory class, used to construct effects
	public class BattleEffectsFactory
	{
		#region Enums
		public enum EffectType
		{
			ValidTargetSelected,
			InvalidTargetSelected
		}
		#endregion

		#region Constants
		private const string VALID_TARGET_PREFAB_NAME = "pfx_TargetValid";
		private const string INVALID_TARGET_PREFAB_NAME = "pfx_TargetInvalid";
		#endregion

		#region Public Methods
		public static GameObject ConstructEffect( EffectType effectType )
		{
			string prefabName = GetPrefabName( effectType );
			GameObject effectResource = Resources.Load< GameObject >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyBattleParticleEffectPrefabs, Path.DirectorySeparatorChar, prefabName ) );
			GameObject instantiatedEffect = UnityEngine.Object.Instantiate( effectResource );
		
			return instantiatedEffect;
		}
		#endregion

		#region Get Prefab Name
		private static string GetPrefabName( EffectType effectType )
		{
			switch( effectType )
			{
				case EffectType.ValidTargetSelected:
					return VALID_TARGET_PREFAB_NAME;

				case EffectType.InvalidTargetSelected:
					return INVALID_TARGET_PREFAB_NAME;
			}

			SJLogger.LogError( "No prefab name was defined for the effect type {0}", effectType.ToString() );
			return string.Empty;
		}
		#endregion
	}
}
