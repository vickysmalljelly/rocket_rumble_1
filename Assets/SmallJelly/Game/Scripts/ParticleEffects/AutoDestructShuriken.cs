using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
	public class AutoDestructShuriken : SJMonoBehaviour
	{
		#region Member Variables
		private List< ParticleSystem > mActiveParticleSystems;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			mActiveParticleSystems = GetComponentsInChildren< ParticleSystem >().ToList();
		}

		private void Update()
		{
			int i = 0;
			while( i < mActiveParticleSystems.Count )
			{
				//If the indexed particle system is alive then move to the next index, otherwise remove it
				if( mActiveParticleSystems[ i ].IsAlive( true ) )
				{
					i++;
				}
				else
				{
					mActiveParticleSystems.RemoveAt( i );
				}
			}

			//No particle effects left
			if( mActiveParticleSystems.Count == 0 )
			{
				Destroy( gameObject );
			}
		}
		#endregion
	}
}