﻿using UnityEngine;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using GameSparks.Api.Requests;
using GameSparks.Core;

namespace SmallJelly
{
	/// <summary>
	/// Class that handles the retrieval of news items from the server.
	/// </summary>
	public static class ClientNews 
	{
		private static readonly string mShortCodeRetrieveNewsItems = "retrieveNewsItems";

		/// <summary>
		/// Method that retrieves all news items from the server and if
		/// successful, returns them via the successHandler.
		/// </summary>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
		public static void GetNewsItems(Action<List<NewsItem>> successHandler, Action<ServerError> errorHandler)
		{
            string shortCode = mShortCodeRetrieveNewsItems;
			LogEventRequest request = new LogEventRequest();
            request.SetEventKey(shortCode)
				.Send((response) =>
				{
					if(!response.HasErrors)
					{
						SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeRetrieveNewsItems);
						List<NewsItem> newsItemList = new List<NewsItem>();
						foreach(GSData gsNewsItem in response.ScriptData.GetGSDataList("newsItems"))
						{
							newsItemList.Add(JsonUtility.FromJson<NewsItem>(gsNewsItem.JSON));
						}
						successHandler(newsItemList);
					}
					else
					{
						SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeRetrieveNewsItems); 
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
					}
				});
		}
	}
}