﻿using System;
using GameSparks.Api.Requests;
using SmallJelly.Framework;

namespace SmallJelly
{
    public static class ClientOnboarding  
    {
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeSetCheckpoint = "setCheckpoint";

        public static void SetCheckpoint(string checkpoint, Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeSetCheckpoint;

            new LogEventRequest()
                .SetEventAttribute("checkpoint", checkpoint)
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}


