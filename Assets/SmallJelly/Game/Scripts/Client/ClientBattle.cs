using System;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using SmallJelly.Framework;
using UnityEngine;
using System.Timers;


namespace SmallJelly
{
	/// <summary>
	/// Contains requests relating to to the battle.
	/// </summary>
	public static class ClientBattle  
	{
		#region Constants
		public const float THRESHOLD_FOR_SLOW_CONNECTION = 2.5f; 
		#endregion

		#region Public Properties
		public static bool IsExperiencingConnectionProblems;
		#endregion

		#region Static Private Variables
		private static bool mServerIsProcessingGameplay;
		#endregion

		// Short codes for events set up on the GameSparks portal
		private static readonly string mShortCodeFireComponent = "fireComponent";
        private static readonly string mShortCodeEndTurn = "endTurn";
        private static readonly string mShortCodeInstallComponent = "installComponent";
        private static readonly string mShortCodeInstallComponentWithTarget = "installComponentWithTarget";
        private static readonly string mShortCodeFireAtHull = "fireAtHull";
        private static readonly string mShortCodeJettison = "jettison";
        private static readonly string mShortCodeCrewCard = "crewCard";
        private static readonly string mShortCodeCrewCardWithTarget = "crewCardWithTarget";
        private static readonly string mShortCodeConcedeBattle = "concede";
        private static readonly string mShortCodeRefreshBattle = "getLatestBattleData";

        //#if SJ_DEBUG
        private static readonly string mShortCodeDebugDrawCard = "debugDrawCard";
        //#endif

		#region Public Methods
        public static bool ServerIsProcessingGameplay()
        {
            return mServerIsProcessingGameplay;
        }
		#endregion

        #region Gameplay Server Calls

        //#if SJ_DEBUG
        public static void DebugForceCardDraw(string cardId, Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Force the player to draw card with id {0}", cardId);

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("cardId", cardId);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeDebugDrawCard, successHandler, errorHandler);
        }
        //#endif      

        public static void GetLatestBattleData(Action<SJJson> successHandler, Action<ServerError> errorHandler)
        {
            string shortCode = mShortCodeRefreshBattle;

            new LogEventRequest().SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId) // as this is not a challenge event, pass the challange id
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
                        SJJson scriptData = null;
                        if(response.ScriptData != null)
                        {
                            scriptData = new SJJson(response.ScriptData.GetGSData("battleView"));
                        }
                        else
                        {
                            SJLogger.LogError("No script data found");
                        }
                            
                        successHandler(scriptData);
                    } else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        public static void JettisonEntity(int slotId, Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("slotId", slotId);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeJettison, successHandler, errorHandler);
        }
       
        public static void InstallComponent(int drawNumber, int socketId, Action successHandler, Action<ServerError> errorHandler)
        {
			SJLogger.LogMessage( MessageFilter.George, "AAA Starting to install component with the draw number {0}", drawNumber );

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("drawNumber", drawNumber);
            request.SetEventAttribute("slotId", socketId);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeInstallComponent, successHandler, errorHandler);
        }

        public static void InstallComponentWithTarget(int drawNumber, int socketId, TargetData targetData, Action successHandler, Action<ServerError> errorHandler)
        {
			SJLogger.LogMessage( MessageFilter.George, "BBB Starting to install component with the draw number {0}", drawNumber );

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("drawNumber", drawNumber);
            request.SetEventAttribute("socketId", socketId);
            request.SetEventAttribute("targetType", targetData.GetTargetType());
            request.SetEventAttribute("targetedPlayerId", targetData.PlayerId);
            request.SetEventAttribute("targetedSocketId", targetData.SocketId);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeInstallComponentWithTarget, successHandler, errorHandler);
        }

        public static void PlayCrewCard(int drawNumber, Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("drawNumber", drawNumber);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeCrewCard, successHandler, errorHandler);
        }

        public static void PlayCrewCardWithTarget(int drawNumber, TargetData targetData, Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("drawNumber", drawNumber);

            request.SetEventAttribute("targetType", targetData.GetTargetType());
            request.SetEventAttribute("targetedPlayerId", targetData.PlayerId);
            request.SetEventAttribute("targetedSocketId", targetData.SocketId);

            LogEventGameplayRequestWithChallengeId(request, mShortCodeCrewCardWithTarget, successHandler, errorHandler);
        }

        public static void FireComponentAtHull(int fromSlotId, Vector3 hitPos, Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.AssertCondition(fromSlotId >= 0 && fromSlotId < 9, "fromSlotId must be between 0 and 8, value is {0}", fromSlotId);            

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("fromSlotId", fromSlotId);
            request.SetEventAttribute("xPos", hitPos.x.ToString());
            request.SetEventAttribute("yPos", hitPos.y.ToString());
            request.SetEventAttribute("zPos", hitPos.z.ToString());

            LogEventGameplayRequestWithChallengeId(request, mShortCodeFireAtHull, successHandler, errorHandler);
        }

        public static void FireComponentAtComponent(int fromSlotId, int targetSlotId, Vector3 hitPos, Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.AssertCondition(fromSlotId >= 0 && fromSlotId < 9, "fromSlotId must be between 0 and 8, value is {0}", fromSlotId);
            SJLogger.AssertCondition(targetSlotId >= 0 && targetSlotId < 9, "targetSlotId must be between 0 and 8, value is {0}", targetSlotId);

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("fromSlotId", fromSlotId);
            request.SetEventAttribute("targetSlotId", targetSlotId);
            request.SetEventAttribute("xPos", hitPos.x.ToString());
            request.SetEventAttribute("yPos", hitPos.y.ToString());
            request.SetEventAttribute("zPos", hitPos.z.ToString());

            LogEventGameplayRequestWithChallengeId(request, mShortCodeFireComponent, successHandler, errorHandler);
        }

        public static void EndTurn(Action successHandler, Action<ServerError> errorHandler)
        {
            mServerIsProcessingGameplay = true;

            string shortCode = mShortCodeEndTurn;

			LogChallengeEventRequest( new LogChallengeEventRequest(), shortCode, successHandler, errorHandler );     
        }

        public static void ConcedeBattle(Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
            LogEventGameplayRequestWithChallengeId(request, mShortCodeConcedeBattle, successHandler, errorHandler);
        }

        #endregion

        #region Private Methods
        private static void LogEventGameplayRequestWithChallengeId(string shortCode, Action successHandler, Action<ServerError> errorHandler)
        {
            mServerIsProcessingGameplay = true;

            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}", shortCode);

            new LogEventRequest()
                .SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId) // as this is not a challenge event, pass the challange id
                .SetDurable(false)
                .Send((response) =>
            {
                HandleResponse(response, shortCode, successHandler, errorHandler);
            });
        }

		//TODO - I'd like to re-visit this method to break down the ping/connection loss work
		//It works by monitoring the timings of server calls and detecting slow calls. If a call is slow then "IsExperiencingConnectionProblems" is set to true,
		//if the connection speeds up again then "IsExperiencingConnectionProblems" is set to false. Where we're experiecing connection problems and we havent
		//done a server call in a while then we need to ping the server to ensure it's still problematic
        private static void LogEventGameplayRequestWithChallengeId(LogEventRequest request, string shortCode, Action successHandler, Action<ServerError> errorHandler)
        {
			bool didElapse = false;

			mServerIsProcessingGameplay = true;

			//If the connection takes longer than "THRESHOLD_FOR_SLOW_CONNECTION" seconds then mark that we're experiencing a slow connection
			Timer connectionLossDetectionTimer = new Timer();
			connectionLossDetectionTimer.Interval = THRESHOLD_FOR_SLOW_CONNECTION * 1000.0f;
			connectionLossDetectionTimer.Elapsed += new ElapsedEventHandler( 
				( object source, ElapsedEventArgs e ) => 
				{ 
					didElapse = true;
					IsExperiencingConnectionProblems = true;
					connectionLossDetectionTimer.Stop();
				} 
			);

			connectionLossDetectionTimer.Start();

            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", shortCode, request.JSONString);

            request.SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId) // as this is not a challenge event, pass the challange id
                .SetDurable(false)
                .Send((response) =>
            {
                HandleResponse(response, shortCode, successHandler, errorHandler);
				connectionLossDetectionTimer.Stop();

				//If the connection takes less than "THRESHOLD_FOR_SLOW_CONNECTION" seconds then mark that we're NOT experiencing a slow connection
				if( !didElapse )
				{
					IsExperiencingConnectionProblems = false;
				}
            });
        }

		private static void LogChallengeEventRequest(LogChallengeEventRequest request, string shortCode, Action successHandler, Action<ServerError> errorHandler)
		{
			bool didElapse = false;
			mServerIsProcessingGameplay = true;
				
			//If the connection takes longer than "THRESHOLD_FOR_SLOW_CONNECTION" seconds then mark that we're experiencing a slow connection
			Timer connectionLossDetectionTimer = new Timer();
			connectionLossDetectionTimer.Interval = THRESHOLD_FOR_SLOW_CONNECTION * 1000.0f;
			connectionLossDetectionTimer.Elapsed += new ElapsedEventHandler( 
				( object source, ElapsedEventArgs e ) => 
				{ 
					didElapse = true;
					IsExperiencingConnectionProblems = true;
					connectionLossDetectionTimer.Stop();
				} 
			);

			connectionLossDetectionTimer.Start();

			SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", shortCode, request.JSONString);

			request.SetChallengeInstanceId(ChallengeManager.Get.ChallengeId)
				.SetEventKey(shortCode) 
                .SetDurable(false)
				.Send((response) =>
					{
						if(!response.HasErrors)
						{   
							SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
							successHandler();
						} else
						{
							SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                            ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
						}

						OnGameplayProcessed(shortCode);

						//If the connection takes less than "THRESHOLD_FOR_SLOW_CONNECTION" seconds then mark that we're NOT experiencing a slow connection
						if( !didElapse )
						{
							IsExperiencingConnectionProblems = false;
						}
					});
		}

        private static void HandleResponse(LogEventResponse response, string shortCode, Action successHandler, Action<ServerError> errorHandler)
        {
            if(!response.HasErrors)
            {   
                SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
                successHandler();
            } else
            {
                SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
            }

            OnGameplayProcessed(shortCode);
        }
            
        private static void OnGameplayProcessed(string shortCode)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Processed {0} gameplay on server", shortCode);

			mServerIsProcessingGameplay = false;
        }
        #endregion

    }
    
}

