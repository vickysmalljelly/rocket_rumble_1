﻿using System;
using GameSparks.Core;

namespace SmallJelly.Framework
{
    /// <summary>
    /// Data representing a Rocket Rumble Challenge
    /// </summary>
    public class RRChallenge : Challenge
    {
        /// <summary>
        /// Fires when there is a battle update from the server
        /// </summary>
        public event Action<SJJson> BattleUpdated;

        /// <summary>
        /// Notifys when both players have completed the mulligan phase.
        /// </summary>
        public event Action MulliganCompleted; 

        /// <summary>
        /// Fires when the player is running out of time, passing the number of seconds left before the turn is forcibly ended.
        /// </summary>
        public event Action<string, int> TurnNearlyOver; 


        public RRChallenge(string challengeId, User challenger, User challenged, string nextPlayerId) : base(challengeId, challenger, challenged , nextPlayerId) {}

        /// <summary>
        /// Configuration for this battle
        /// </summary>
        public BattleConfig BattleConfig;

        /// <summary>
        /// User to set up replays
        /// </summary>
        public string ReplayFirstUserId { get; set; }

        private OrderedQueue<SJJson> mMessageQueue = new OrderedQueue<SJJson>();
		private int mMinimumIssueNumber;

		#region Public Methods
        public override void OnCustomDataReceived(CustomServerMessage message)
        {
            base.OnCustomDataReceived(message);
				
            string messageType = message.Json.GetString("messageType");

            switch(messageType)
            {
                case "mulliganComplete":
                    if(MulliganCompleted != null)
                    {
                        MulliganCompleted();
                    }
                    break;
                case "updateView":
                    ProcessUpdateView(message.Json);
                    break;
                case "turnNearlyOver":
                    ProcessTurnNearlyOver(message.Json);
                    break;
                default:
                    SJLogger.LogError("Challenge message {0} not recognised", messageType);
                    break;
            } 
        }

		public override void ProcessTurnTaken( string nextPlayerId )
		{
			base.ProcessTurnTaken( nextPlayerId );

			//We wipe the queue after each turn is taken - this ensures it can never be blocked for longer than a turn
			mMessageQueue.Clear();
		}

		public void SetMinimumIssueNumber( int minimumIssueNumber )
		{
			mMinimumIssueNumber = minimumIssueNumber;
			mMessageQueue.Clear();
		}
		#endregion

		#region Private Methods
        private void ProcessUpdateView(SJJson json)
        {
            // Does this message have an issue number?
            int? issueNumber = json.GetInt("issueNumber");

			SJLogger.AssertCondition( issueNumber.HasValue, "All view update events should have an issue number" );

            // VJS: Turns out we can't rely on the order in which these messages are received from GameSparks.
            // Our server side code adds an issueNumber to each message that is sent.  
            // Using an OrderedQueue ensures that they are sent to our game logic in the right order

			if( issueNumber.GetValueOrDefault() >= mMinimumIssueNumber )
			{
				// Store the message
				mMessageQueue.AddItemToQueue(issueNumber.GetValueOrDefault(), json);
			}

			AttemptToRunNextItem();
        }

		private void AttemptToRunNextItem()
		{
			// Send the messages in the right order
			while( mMessageQueue.HaveNextItem() && mMessageQueue.GetNextIssueNumber() >= mMinimumIssueNumber )
			{
				if(BattleUpdated != null)
				{
					BattleUpdated( mMessageQueue.GetNextItem() );
				}
			}
		}

        private void ProcessTurnNearlyOver(SJJson json)
        {
            if(TurnNearlyOver == null)
            {
                // No one is listening to the event
                return;
            }

            int? seconds = json.GetInt("secondsRemaining");

            if(!seconds.HasValue)
            {
                SJLogger.LogError("Message needs to contain secondsRemaining");
                return;
            }

            string playerId = json.GetString("currentPlayerId");

            TurnNearlyOver( playerId, seconds.GetValueOrDefault() );
        }
		#endregion
    }
}