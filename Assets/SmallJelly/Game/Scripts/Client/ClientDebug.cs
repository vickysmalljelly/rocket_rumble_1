﻿using System;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using UnityEngine;

//#if SJ_DEBUG

namespace SmallJelly
{
    /// <summary>
    /// Contains requests relating to debug functionality.
    /// </summary>
    public static class ClientDebug  
    {
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeDebugGetState = "getStateDebug";
        private static readonly string mShortCodeDebugAddBattleCard = "debugAddBattleCard";
        private static readonly string mShortCodeDebugAddAllToCollection = "debugAddAllToCollection";
        private static readonly string mShortCodeDebugGivePacks = "debugGivePacks";
        private static readonly string mShortCodeDebugConsumeAllInboxItems = "debugConsumeAllInboxItems";
        private static readonly string mShortCodeDebugGiveCrystals = "debugGiveCrystals";
        private static readonly string mShortCodeDebugGiveCredits = "debugGiveCredits";
        private static readonly string mShortCodeDebugTestDurable = "setDurableTest";

        public static void FakeWin(string shipClass, Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = "rumbleWinIncrement";
            new LogEventRequest()
                .SetEventKey(shortCode)
                .SetEventAttribute("class", shipClass)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);                      
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        public static void ConsumeAllInboxItems(Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeDebugConsumeAllInboxItems;
            new LogEventRequest()
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);                      
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
            });
        }

        public static void GivePacks(int numPacks, Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeDebugGivePacks;

            new LogEventRequest()
                .SetEventAttribute("numPacks", numPacks)
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);                       
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }      

        public static void GiveCrystals(long numCrystals, Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeDebugGiveCrystals;

            new LogEventRequest()
                .SetEventAttribute("numCrystals", numCrystals)
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);                       
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }    

        public static void GiveCredits(long numCredits, Action successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeDebugGiveCredits;

            new LogEventRequest()
                .SetEventAttribute("numCredits", numCredits)
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);                       
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }    

        public static void AddAllToCollection(Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Add all cards to the player's collection");
            string shortCode = mShortCodeDebugAddAllToCollection;

            LogEventRequest request = new LogEventRequest();
            request.SetEventKey(shortCode)
                .Send((response) => 
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} success", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        public static void AddBattleCardToCollection(string cardId, Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Force the player to draw card with id {0}", cardId);
            string shortCode = mShortCodeDebugAddBattleCard;
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("cardId", cardId)
                .SetEventKey(shortCode)
                .Send((response) => 
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} success", mShortCodeDebugAddBattleCard);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeDebugAddBattleCard);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
       
        public static void GetBattleState(Action<SJJson> successHandler, Action<ServerError> errorHandler)
        {
            string shortCode = mShortCodeDebugGetState;
            new LogEventRequest()
                .SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId) // as this is not a challenge event, pass the challange id
                .Send((response) => 
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} success", shortCode);
                        GSData gsData = response.ScriptData.GetGSData("battleState");
                        successHandler(new SJJson(gsData));
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        public static void TestDurable(Action successHandler, Action<ServerError> errorHandler)
        {
            string shortCode = mShortCodeDebugTestDurable;
            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, setting durable to false", shortCode);

            new LogEventRequest()
                .SetEventKey(shortCode) 
                .SetDurable(false)
                .Send((response) => 
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} success", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}
//#endif

