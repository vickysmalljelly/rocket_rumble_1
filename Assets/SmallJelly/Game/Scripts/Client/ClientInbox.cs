﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Contains requests relating to the player's inbox.
    /// </summary>
    public static class ClientInbox  
    {
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeConsumeItem = "consumeInboxItem";
        private static readonly string mShortCodeListInboxItems = "listInboxItems";

        /// <summary>
        /// Consumes an item from the inbox, giving the contents to the player and removing the item from the inbox.
        /// </summary>
        /// <param name="uniqueId">The unique id of the inbox item</param>
        public static void ConsumeItem(string uniqueId, Action<RewardData> successHandler, Action<ServerError> errorHandler) 
        {
            string shortCode = mShortCodeConsumeItem;

            new LogEventRequest()
                .SetEventAttribute("uniqueId", uniqueId)
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeConsumeItem);

                        SJJson rewardJson = new SJJson(response.ScriptData.GetGSData("reward")); 
                        RewardData reward = RewardData.FromJson(rewardJson);

                        successHandler(reward);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeConsumeItem);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
            
        /// <summary>
        /// Lists all items in the player's inbox.
        /// </summary>
        public static void ListInbox(Action<List<InboxItemData>> successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "List the contents of the player's inbox");
            string shortCode = mShortCodeListInboxItems;
            new LogEventRequest().SetEventKey(shortCode)
                .Send((response) => 
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeListInboxItems);
                        SJJson inboxJson = new SJJson(response.ScriptData.GetGSData("inbox"));

                        List<InboxItemData> list = new List<InboxItemData>();

                        foreach(SJJson packJson in inboxJson.GetSJJsonList("items"))
                        {
                            // Currently all items are packs
                            list.Add(JsonUtility.FromJson<InboxPackData>(packJson.JSON));
                        }

                        successHandler(list);
                    } 
                    else 
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeListInboxItems);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}

