﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using SmallJelly.Framework;

namespace SmallJelly
{
    public class ClientLeaderboard 
    {
        public static void ListLeaderboards(Action<List<LeaderboardData>> successHandler, Action<ServerError> errorHandler) 
        {
            SJLogger.LogMessage(MessageFilter.Client, "Sending ListLeaderboards");

            new ListLeaderboardsRequest()
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "ListLeaderboards sent");  
                        List<LeaderboardData> list = new List<LeaderboardData>();
                        GSEnumerable<ListLeaderboardsResponse._Leaderboard> leaderboards = response.Leaderboards; 

                        foreach(var item in leaderboards)
                        {
                            LeaderboardData data = new LeaderboardData();
                            data.Name = item.Name;
                            data.ShortCode = item.ShortCode;

                            list.Add(data);

                            //Debug.Log(item.JSONString);
                            //Debug.Log(data.ToString());
                        }

                        successHandler(list);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "ListLeaderboards error");         
                        ServerErrorInterpreter.InterpretError<LeaderboardErrorInterpreter>(response.Errors, errorHandler, "ListLeaderboardsRequest");
                    }
                });
        }

        public static void Top(string leaderboardShortCode, int numEntries, Action<List<LeaderboardRowNumWins>> successHandler, Action<ServerError> errorHandler) 
        {
            SJLogger.LogMessage(MessageFilter.Client, "Sending Top for leaderboard {0}", leaderboardShortCode);

            new LeaderboardDataRequest()
                .SetLeaderboardShortCode(leaderboardShortCode)
                .SetEntryCount(numEntries)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "Top sent, leaderboard {0}", leaderboardShortCode);  

                        GSEnumerable<LeaderboardDataResponse._LeaderboardData> data = response.Data;         
                        List<LeaderboardRowNumWins> list = new List<LeaderboardRowNumWins>();

                        foreach(LeaderboardDataResponse._LeaderboardData item in data)
                        {
                            LeaderboardRowNumWins row = new LeaderboardRowNumWins();
                            row.DisplayName = item.UserName;
                            row.UserId = item.UserId;
                            row.Country = item.Country;
                            row.Rank = item.Rank.GetValueOrDefault();

                            // Num wins can be stored in one of two variables
                            long? numWins = item.GetNumberValue("COUNT-num");
                            if(numWins.HasValue)
                            {
                                row.NumWins = numWins.GetValueOrDefault();
                            }
                            else
                            {
                                row.NumWins = item.GetNumberValue("num").GetValueOrDefault();
                            }

                            DateTime.TryParse(item.When, out row.Time);

                            list.Add(row);

                            //Debug.Log(item.JSONString);
                            //Debug.Log(row.ToString());
                        }

                        successHandler(list);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "Top error, leaderboard {0}", leaderboardShortCode);         
                        ServerErrorInterpreter.InterpretError<LeaderboardErrorInterpreter>(response.Errors, errorHandler, "AroundMeLeaderboardRequest");
                    }
                });
        }

        public static void AroundMe(string leaderboardShortCode, int numEntries, Action<List<LeaderboardRowNumWins>> successHandler, Action<ServerError> errorHandler) 
        {
            SJLogger.LogMessage(MessageFilter.Client, "Sending AroundMe for leaderboard {0}", leaderboardShortCode);

            new AroundMeLeaderboardRequest()
                .SetLeaderboardShortCode(leaderboardShortCode)
                .SetEntryCount(numEntries)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "AroundMe sent, leaderboard {0}", leaderboardShortCode);  
                         
                        GSEnumerable<AroundMeLeaderboardResponse._LeaderboardData> data = response.Data;         
                        List<LeaderboardRowNumWins> list = new List<LeaderboardRowNumWins>();

                        foreach(AroundMeLeaderboardResponse._LeaderboardData item in data)
                        {
                            LeaderboardRowNumWins row = new LeaderboardRowNumWins();
                            row.DisplayName = item.UserName;
                            row.UserId = item.UserId;
                            row.Country = item.Country;
                            row.Rank = item.Rank.GetValueOrDefault();

                            // Num wins can be stored in one of two variables
                            long? numWins = item.GetNumberValue("COUNT-num");
                            if(numWins.HasValue)
                            {
                                row.NumWins = numWins.GetValueOrDefault();
                            }
                            else
                            {
                                row.NumWins = item.GetNumberValue("num").GetValueOrDefault();
                            }

                            DateTime.TryParse(item.When, out row.Time);

                            list.Add(row);

                            //Debug.Log(item.JSONString);
                            //Debug.Log(row.ToString());
                        }

                        successHandler(list);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "AroundMe error, leaderboard {0}", leaderboardShortCode);         
                        ServerErrorInterpreter.InterpretError<LeaderboardErrorInterpreter>(response.Errors, errorHandler, "AroundMeLeaderboardRequest");
                    }
                });
        }

    }
}
