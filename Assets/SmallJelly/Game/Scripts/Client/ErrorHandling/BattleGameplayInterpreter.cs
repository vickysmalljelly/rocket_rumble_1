﻿using GameSparks.Core;
using SmallJelly.Framework;

namespace SmallJelly
{
    public class BattleGameplayInterpreter : ConnectionErrorInterpreter
    {
        private const string sBattleGameplayKey = "sjBattleGameplay";

        private const string sChallengeId = "challengeInstanceId";
        private const string sChallengeComplete = "COMPLETE";

        public override ServerError InterpretError(GSData errorData, string shortCode)
        {
            if(errorData.ContainsKey(sBattleGameplayKey))
            {
                GSData battleGameplayObject = errorData.GetGSData(sBattleGameplayKey);

                string technicalDetails = string.Format("name: {0}, message: {1}, stack: {2}", 
                    battleGameplayObject.GetString("name"),
                    battleGameplayObject.GetString("message"),
                    battleGameplayObject.GetString("stack")
                );
                    
                return new ServerError(ServerErrorType.CustomGameplay, "The server encountered an error", technicalDetails);
            }

            if(errorData.ContainsKey(sChallengeId))
            {
                string challengeError = errorData.GetString(sChallengeId);
                if(string.Compare(challengeError, sChallengeComplete) == 0)
                {
                    return new ServerError(ServerErrorType.ChallengeComplete, "The battle is already finished", "The battle is already finished");
                }
            }

            return base.InterpretError(errorData, shortCode);
        }
    }
}
