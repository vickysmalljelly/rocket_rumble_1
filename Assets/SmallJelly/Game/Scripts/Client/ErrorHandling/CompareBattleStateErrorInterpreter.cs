﻿using System;
using System.Diagnostics;
using System.IO;
using GameSparks.Core;
using SmallJelly.Framework;


namespace SmallJelly
{
    /// <summary>
    /// Interprets errors from battleCompareState event
    /// </summary>
    public class CompareBattleStateErrorInterpreter : BattleGameplayInterpreter
    {
        public override ServerError InterpretError(GSData errorData, string shortCode)
        {
            GSData error = errorData.GetGSData("error");

            if(error.ContainsKey("type"))
            {
                string errorType = error.GetString("type");

                switch(errorType)
                {
                    case "battleStateMismatch":
                        {
                            string message = error.GetString("message");
                            string recordedBattleState = error.GetString("recordedBattleState");
                            string replayBattleState = error.GetString("replayBattleState");
                            string replayName = error.GetString("replayName");
                            AnalyseBattleStateMismatch(replayName, recordedBattleState, replayBattleState);

                            return new ServerError(ServerErrorType.AutomatedTest, replayName + ":", message);
                        }
                    case "battleStateOverwrite":
                        {
                            string message = error.GetString("message");
                            string replayName = error.GetString("replayName");
                            string recordedBattleState = error.GetString("recordedBattleState");
                            string replayBattleState = error.GetString("replayBattleState");

                            AnalyseBattleStateMismatch(replayName, recordedBattleState, replayBattleState);

                            // We also need to Import from Database to Local Json
                            Debug.LogWarning("Remember to import replays from database to local json");
                           
                            return new ServerError(ServerErrorType.AutomatedTest, replayName + ":", message);
                        }
                    case "initialisation":
                        {
                            string message = errorData.GetString("message");
                            return new ServerError(ServerErrorType.AutomatedTest, "Failed to compare battle states", message);
                        }
                    default:
                        break;
                }
            }

            return base.InterpretError(errorData, shortCode);
        }

        private void AnalyseBattleStateMismatch(string replayName, string recordedBattleState, string replayBattleState)
        {
            SJLogger.AssertCondition(!string.IsNullOrEmpty(replayName), "Do not have a replay name");

            if(!Directory.Exists(FileLocations.BattleStateComparisonsDirectory))
            {
                Directory.CreateDirectory(FileLocations.BattleStateComparisonsDirectory);
            }

            string replayDirectory = Path.Combine(FileLocations.BattleStateComparisonsDirectory, replayName);

            if(!Directory.Exists(replayDirectory))
            {
                Directory.CreateDirectory(replayDirectory);
            }
           
            recordedBattleState = JsonUtils.FormatJson(recordedBattleState);
            replayBattleState = JsonUtils.FormatJson(replayBattleState);

            string recordedFile = Path.Combine(replayDirectory, "recorded.json");
            string replayFile = Path.Combine(replayDirectory, "current.json");

            if(File.Exists(recordedFile))
            {
                File.Delete(recordedFile);
            }

            if(File.Exists(replayFile))
            {
                File.Delete(replayFile);
            }
                
            File.WriteAllText(recordedFile, recordedBattleState);
            File.WriteAllText(replayFile, replayBattleState);

            string commandLineArgs =  recordedFile + " " + replayFile;
            Debug.Log("commandLineArgs:  " + commandLineArgs);

            try
            {
                // TODO: Write shell script to call opendiff with the two files passed as args
                // For now, just open the the diff program and the containing directory
                Process.Start("/Applications/Xcode5.0/Xcode.app/Contents/Applications/FileMerge.app");
                Process.Start(replayDirectory);

            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
    }
}

