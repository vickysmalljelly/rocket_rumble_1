﻿using System;
using SmallJelly.Framework;
using GameSparks.Api.Requests;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Helper for classes which use LogEventRequests.
    /// TODO: Move to Framework
	/// </summary>
	public static class ClientRequest
	{
		#region Public Methods
		/// <summary>
		/// Creates and sends the LogEventRequest, automatically adding the Challenge Id
		/// </summary>
        public static void LogEventRequestWithChallengeId<T>(string shortCode, Action successHandler, Action<ServerError> errorHandler) where T : ServerErrorInterpreter, new()
		{
            LogEventRequestWithChallengeId<T>(new LogEventRequest(), shortCode, successHandler, errorHandler);
		}

		/// <summary>
		/// Sends the LogEventRequest with attributes, automatically adding the Challenge Id
		/// </summary>
        public static void LogEventRequestWithChallengeId<T>(LogEventRequest request, string shortCode, Action successHandler, Action<ServerError> errorHandler)  where T : ServerErrorInterpreter, new()
		{
            request.SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId);

            LogEventRequest<T>(request, shortCode, successHandler, errorHandler);
		}
            
        /// <summary>
        /// Sends the LogEventRequest with attributes
        /// </summary>
        public static void LogEventRequest<T>(LogEventRequest request, string shortCode, Action successHandler, Action<ServerError> errorHandler) where T : ServerErrorInterpreter, new()
        {
            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", shortCode, request.JSONString);

            request.SetEventKey(shortCode) 
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<T>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
		#endregion
	}
}
