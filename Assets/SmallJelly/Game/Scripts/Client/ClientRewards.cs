﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using UnityEngine;


namespace SmallJelly
{
    /// <summary>
    /// A list of all the possible reward events in the game.  Used for debug purposes only.
    /// </summary>
    public enum RewardEvent
    {
        NotSet,
        BattleWin,
        BattleLose,
        Welcome
    }

    /// <summary>
    /// Contains requests relating to rewards for the player
    /// </summary>
    public static class ClientRewards  
    {
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeGetReward = "debugGiveReward";

        public static void DebugGiveReward(RewardEvent rewardEvent, Action<RewardData> successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Get reward for event {0}", rewardEvent);
            string shortCode = mShortCodeGetReward;

            new LogEventRequest()
                .SetEventAttribute("rewardEvent", rewardEvent.ToString())
                .SetEventKey(shortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeGetReward);

                        SJJson rewardJson = new SJJson(response.ScriptData.GetGSData("reward")); 
                        RewardData reward = RewardData.FromJson(rewardJson);

                        successHandler(reward);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeGetReward);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}

