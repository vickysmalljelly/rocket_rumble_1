﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;


namespace SmallJelly.Framework
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientMatchmaking  
    {
        private static readonly string sShortCodeFindMatch = "findMatch";

        public static void FindMatch(string matchShortCode, Action successHandler, Action<ServerError> errorHandler)
        {
            string shortCode = sShortCodeFindMatch;
            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, {1}", shortCode, matchShortCode);

            new LogEventRequest()
                .SetEventKey(shortCode)
                .SetEventAttribute("matchShortCode", matchShortCode)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {   
                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}

