﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using UnityEngine;


namespace SmallJelly
{

    /// <summary>
    /// Contains requests relating to managing the player's deck.
    /// </summary>
    public static class ClientDeck  
    {
        // Short codes for events set up on the GameSparks portal
		private static readonly string mShortCodeNewDeck = "newDeck";
        private static readonly string mShortCodeUpdateDeck = "deckUpdate";
		private static readonly string mShortCodeChangeDeckDisplayName = "deckUpdateDisplayName";
		private static readonly string mShortCodeGetDeck = "getDeck";
        private static readonly string mShortCodeListDecks = "listDecks";
        private static readonly string mShortCodeSetCurrentDeck = "deckSetCurrent";


        public static void SetCurrentDeck(string deckId, Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("deckId", deckId);

            ClientRequest.LogEventRequest<ConnectionErrorInterpreter>(request, mShortCodeSetCurrentDeck, successHandler, errorHandler);
        }

		/// <summary>
		/// Method that triggers a GameSparks event to create a new deck with a specific displayName and list of cards.
		/// </summary>
		/// <param name="displayName">Display name of new deck.</param>
		/// <param name="cards">Cards list of new deck.</param>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void NewDeck(string displayName, string deckClass, List<string> cards, Action successHandler, Action<ServerError> errorHandler)
        {
        	LogEventRequest request = new LogEventRequest();
        	request.SetEventAttribute("cards", cards);
        	request.SetEventAttribute("displayName", displayName);
            request.SetEventAttribute("deckClass", deckClass.ToString());

            ClientRequest.LogEventRequest<ConnectionErrorInterpreter>(request, mShortCodeNewDeck, successHandler, errorHandler);
        }

        /// <summary>
        /// Method that triggers a GameSparks event to update a deck with a specific id.
        /// </summary>
        /// <param name="deckId">Deck identifier.</param>
        /// <param name="cards">Cards.</param>
        /// <param name="successHandler">Success handler.</param>
        /// <param name="errorHandler">Error handler.</param>
        public static void UpdateDeck(string deckId, List<string> cards, string displayName, Action successHandler, Action<ServerError> errorHandler) 
        {
        	LogEventRequest request = new LogEventRequest();
			request.SetEventAttribute("deckId", deckId);
			request.SetEventAttribute("cards", cards);
            request.SetEventAttribute("displayName", displayName);

            ClientRequest.LogEventRequest<ConnectionErrorInterpreter>(request, mShortCodeUpdateDeck, successHandler, errorHandler);
        }

		/// <summary>
		/// Method that triggers a GameSparks event to update the displayName of an existing deck.
		/// </summary>
		/// <param name="deckId">Deck identifier.</param>
		/// <param name="displayName">Display name.</param>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void DeckUpdateDisplayName(string deckId, string displayName, Action successHandler, Action<ServerError> errorHandler)
        {
            LogEventRequest request = new LogEventRequest();
        	request.SetEventAttribute("deckId", deckId);
        	request.SetEventAttribute("displayName", displayName);

            ClientRequest.LogEventRequest<ConnectionErrorInterpreter>(request, mShortCodeChangeDeckDisplayName, successHandler, errorHandler);
        }

		/// <summary>
		/// Method that triggers a GameSparks event to retrieve the list of decks beloging to the local player.
		/// It implements a custom response handler to parse the retrieved list and return it in the successHandler.
		/// </summary>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void ListDecks(Action<List<Deck>> successHandler, Action<ServerError> errorHandler)
        {
        	SJLogger.LogMessage(MessageFilter.Client, "List all decks for local player");
            string shortCode = mShortCodeListDecks;
            new LogEventRequest().SetEventKey(shortCode)
        	.Send((response) => 
        	{
        		if(!response.HasErrors)
        		{
        			SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeListDecks);
        			List<Deck> decks = new List<Deck>();
        			foreach(GSData gsDeck in response.ScriptData.GetGSDataList("result"))
        			{
        				decks.Add(JsonUtility.FromJson<Deck>(gsDeck.JSON));
        			}
					
        			successHandler(decks);
        		} 
        		else 
        		{
        			SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeListDecks);
                    ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
        		}
        	});
        }

		/// <summary>
		/// Method that triggers a GameSparks event to retrieve a deck by its deckId.
		/// It implements a custom response handler to parse the retrieved deck and return it in the successHandler.
		/// </summary>
		/// <param name="deckId">Deck identifier.</param>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void GetDeck(string deckId, Action<Deck> successHandler, Action<ServerError> errorHandler)
        {
        	SJLogger.LogMessage(MessageFilter.Client, "Get deck for local player");
            string shortCode = mShortCodeGetDeck;
        	new LogEventRequest().SetEventAttribute("deckId", deckId)
                .SetEventKey(shortCode)
        	    .Send((response) =>
        	{
        		if(!response.HasErrors)
        		{
        			SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeGetDeck);

        			// Get deckId and displayName first from "deckData"
        			Deck deck = JsonUtility.FromJson<Deck>(response.ScriptData.GetGSData("deckData").JSON);

        			// Then retrieve battle entity information from "battleEntities"
        			List<GSData> gsEntities = response.ScriptData.GetGSDataList("battleEntities");

        			// Initialise the battleEntities list inside the new deck
					deck.BattleEntities = new List<BattleEntityData>();
					
					// Add each battle entity to deck
					foreach(GSData gsEntity  in gsEntities) {
						// Convert the GSData to SJJson so we can use the helper methods and factories
						SJJson jsonEntity = new SJJson(gsEntity);

						// Convert the SJJson entity to an actual BattleEntityData
						BattleEntityData entity =  BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, jsonEntity );

						// Add the new entity to the BattleEntities in the new deck
						deck.BattleEntities.Add(entity);
					}

        			successHandler(deck);
        		}
        		else
        		{
					SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeGetDeck);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
        		}
        	});
        }
    }

}

