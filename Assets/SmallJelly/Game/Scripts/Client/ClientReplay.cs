﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using System.Text.RegularExpressions;

namespace SmallJelly
{
	/// <summary>
	/// Contains request to save a specific replay name.
	/// </summary>
	public static class ClientReplay
	{
		private static readonly string mShortCodeReplaySave = "replaySave";
		private static readonly string mShortCodeReplayStart = "replayStart";
        private static readonly string mCompareBattleStateShortCode = "compareBattleState";

		public static void RecordBug(string bugName, string bugDescription, Action successHandler, Action<ServerError> errorHandler)
		{
			// TODO: Check if environment is staging
			LogEventRequest request = new LogEventRequest();
			request.SetEventAttribute("replayName", bugName);
			request.SetEventAttribute("replayDescription", bugDescription);
			request.SetEventAttribute("isBug", "true");

            ClientRequest.LogEventRequestWithChallengeId<ConnectionErrorInterpreter>(request, mShortCodeReplaySave, successHandler, errorHandler);
		}

		public static void SaveReplay(string replayName, string replayDescription, Action successHandler, Action<ServerError> errorHandler)
		{
			LogEventRequest request = new LogEventRequest();
			request.SetEventAttribute("replayName", replayName);
			request.SetEventAttribute("replayDescription", replayDescription);
			request.SetEventAttribute("isBug", "false");

            ClientRequest.LogEventRequestWithChallengeId<ConnectionErrorInterpreter>(request, mShortCodeReplaySave, successHandler, errorHandler);
		}

		public static void StartReplay(string replayName, Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.AssertCondition(!string.IsNullOrEmpty(replayName), "Must have a replay name");
			LogEventRequest request = new LogEventRequest();
			request.SetMaxResponseTimeInSeconds(30);
			request.SetEventAttribute("replayName", replayName);

            ClientRequest.LogEventRequestWithChallengeId<ConnectionErrorInterpreter>(request, mShortCodeReplayStart, successHandler, errorHandler);
		}
            
		public static void RequestReplayChallenge(string replayName, string challengeName, List<string> usersToChallenge, Action successHandler, Action<ServerError> errorHandler)
		{ 
			SJLogger.LogMessage(MessageFilter.Client, "Requesting replay with {0}", usersToChallenge[0]);
			SJLogger.AssertCondition(usersToChallenge.Count == 1, "We must challenge 1 user");

			DateTime endTime = DateTime.UtcNow.AddDays(1);

			GSRequestData data = new GSRequestData();
			data.AddString("replayName", replayName);

			new CreateChallengeRequest().SetChallengeShortCode(challengeName)
                .SetUsersToChallenge(usersToChallenge)
                .SetEndTime(endTime)
                .SetChallengeMessage(ChallengeManager.ChallengeRequestMessage)
                .SetScriptData(data)
                .Send((response) =>
				{
					if(response.HasErrors)
					{
                        ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, "CreateChallengeRequest");
					}
					else
					{
						// Challenge has been created
						SJLogger.LogMessage(MessageFilter.Client, "ChallengeRequestCreated");
						successHandler();
					}
				});
		}

		public static void CompareBattleStates(bool overwrite, Action successHandler, Action<ServerError> errorHandler)
		{
            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("overwrite", overwrite.ToString());

            ClientRequest.LogEventRequestWithChallengeId<CompareBattleStateErrorInterpreter>(request, mCompareBattleStateShortCode, successHandler, errorHandler);
		}
       
	}
}