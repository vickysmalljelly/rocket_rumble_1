using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Contains requests relating to managing the player's collection.
    /// </summary>
    public static class ClientCollection  
    {
		#region Member Variables 
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeGetCollection = "collectionGet";
		#endregion

		#region Public Methods
        /// <summary>
        /// Gets cards in the player's collection according to the BattleCardFilter.
        /// </summary>
        /// <param name="filter">What cards to match.</param>
        /// <param name="numberOfCards">Number of cards in each page.</param>
        /// <param name="page">Which page of results to return. If -1, returns the last page.</param>
        public static void GetCards(CollectionManager.Direction direction, BattleCardFilter filter, int numberOfCards, int page, Action<CollectionQueryResult> successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Get cards from collection. ShipClass: {0}, Power: {1}, ComponentType: {2}", filter.ShipClass, filter.Power, filter.ComponentType);
            string shortCode = mShortCodeGetCollection;

            LogEventRequest request = new LogEventRequest();
            request.SetEventAttribute("numberOfCards", numberOfCards);
            request.SetEventAttribute("page", page);
            request.SetEventAttribute("shipClass", filter.ShipClass);
            request.SetEventAttribute("power", filter.Power);
            request.SetEventAttribute("componentType", filter.ComponentType);

            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", mShortCodeGetCollection, request.JSONString);

            request.SetEventKey(shortCode) 
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {   
                        GSData data = response.ScriptData.GetGSData("page"); 

                        List<GSData> list = data.GetGSDataList("cards");
                        List<CollectionBattleEntityData> cards = new List<CollectionBattleEntityData>();

                        foreach(GSData gsData in list)
                        {
							BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, new SJJson( gsData.GetGSData( "card" ) ) );                                

                            CollectionBattleEntityData item = new CollectionBattleEntityData();
                            item.Card = battleEntityData;
                            item.Count = gsData.GetInt("count").GetValueOrDefault();
                            item.New = gsData.GetBoolean("new").GetValueOrDefault();

                            cards.Add(item);                                                  
                        }

                        SJLogger.LogMessage(MessageFilter.Client, "{0} sent", mShortCodeGetCollection);

                        CollectionQueryResult result = new CollectionQueryResult();
                        result.NumPages = data.GetInt("numPages").GetValueOrDefault();
						result.PageNum = MathsExtensions.Modulo( page, Mathf.Max( 1, result.NumPages ) );

                        result.FirstPage = data.GetBoolean("firstPage").GetValueOrDefault();
                        result.LastPage = data.GetBoolean("lastPage").GetValueOrDefault();
                        result.Cards = cards.ToArray();
                        result.Direction = direction;

                        SJLogger.LogMessage(MessageFilter.Client, "Page num {3}, First page {0}, last page {1}, num pages {2}", result.FirstPage, result.LastPage, result.NumPages, result.PageNum);

                        successHandler(result);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeGetCollection);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
        #endregion
    }

}

