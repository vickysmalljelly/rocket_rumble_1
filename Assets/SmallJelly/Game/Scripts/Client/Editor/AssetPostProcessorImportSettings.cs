﻿using UnityEditor;

public class AssetPostProcessorImportSettings : AssetPostprocessor
{
    public void OnPreprocessModel()
    {
        ModelImporter modelImporter = (ModelImporter)assetImporter;
        modelImporter.importMaterials = false;
    }
}