﻿using SmallJelly.Framework;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Class that enables the deleting of replays from your GameSparks database and the adding
    /// of replays from your Unity Project into the GameSparks database.
    /// TODO: Move functionality into MongoDbCollections?
    /// </summary>
    public class MongoDbReplays
    {
        private const string sMetaCollectionName = "meta.replays";
        private const string sCollectionName = "replays";

        /// <summary>
        /// Public method used to make the make a server call and get all the meta.replays entries.
        /// This sends a gigantic string with everything in one line and we are parsing everything inside SaveImportedReplays().
        /// </summary>
		public static void ImportAllReplays(Action successHandler, Action errorHandler)
        {
            Debug.Log("Importing all replays...");

            if(!GameSparksDBAccount.InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            StringBuilder urlBuilder = GetUrlStub(sMetaCollectionName);
            urlBuilder.Append("/find");

            WWWForm form = new WWWForm();

            // find all
            form.AddField("query", "{}");
            form.AddField("limit", "10000");

            Dictionary<string, string> headers = form.headers;
            byte[] rawData = form.data;

            string auth = string.Format("{0}:{1}", GameSparksDBAccount.Email, GameSparksDBAccount.Password);
            headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

            WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

            CoroutineExecutor.Add(() => www.isDone, () =>
               {
					if (www.error == null)
					{
					  SaveImportedReplays(www.text);
					  successHandler();
					}
					else
					{
					  Debug.LogError("WWW Error: " + www.error);
					  errorHandler();
					}
              });
        }

        /// <summary>
        /// Uses regex to extract data from the gigantic string containing all the replays.
        /// </summary>
        /// <param name="jsonString">Gigantic string containing all the replays</param>
        private static void SaveImportedReplays(string jsonString)
        {
            string bodyPattern = @"[{|{ ]*?""_id""[\s\S]*?:[\s\S]*?}[\s\S]*?,"; // this pattern is used to extract all the _id blocks from the replays
            string namePattern = @"(?<=""replayName"":"")[^""]*"; // this pattern returnes the replayName by positive lookbehind until a "

            MatchCollection matches = Regex.Matches(jsonString, bodyPattern);

            if(matches.Count<=0)
            {
                Debug.LogError("ClientReplayExport: Could not find any replays matching regex!");
                return;
            }

            DirectoryInfo parentDirectory = new DirectoryInfo(FileLocations.AutomatedTestsDirectory);

            // This is the meat of the algorithm. It loops through all found "_id" blocks of each replay
            // and uses string indexOf to extract the replay contents in between each "_id" block
            // and with the help of some arithmetic* creates and saves each replay
            // as a valid json. The name of each file is extracted using the namePattern from within the replay.
            // * It calculates the position of where it found an "_id" block and takes everything from there +
            // the lenght of said id block (seems to always be 44) and the beginning of the next "_id" block
            // (or just the end in case of the last one) and creates a replay from that.
            for (int index = 0; index < matches.Count; index++)
            {
                int start = jsonString.IndexOf(matches[index].Value);
                int finish = jsonString.Length - matches[index].Value.Length;
                int endCutoff = 1;

                if (index < matches.Count - 1)
                {
                    finish = jsonString.IndexOf(matches[index + 1].Value);
                    endCutoff = matches[index + 1].Value.Length + 1;
                }

                string entry = "{" + jsonString.Substring(start + matches[index].Value.Length, finish - start - endCutoff) + "\n";

                Match nameMatch = Regex.Match(entry, namePattern);

                if (!nameMatch.Success)
                {
                    Debug.Log("ClientReplayExport: Could not find replayName in json!");
                    return;
                }

                string fileName = nameMatch.Value;
                FileInfo someFile = new FileInfo(Path.Combine(parentDirectory.FullName, fileName + ".json"));
				JToken entryJToken = JToken.Parse(entry);
				string formattedJson = entryJToken.ToString(Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(someFile.FullName, formattedJson);
            }

            Debug.Log("ClientReplayExport: Successfully imported " + matches.Count + " replays!");
        }

        /// <summary>
        /// Prepares the url stub for making API calls to GameSparks.
        /// </summary>
        private static StringBuilder GetUrlStub(string collection)
        {
            StringBuilder urlBuilder = new StringBuilder("https://portal.gamesparks.net/rest/games/");
            // key
            urlBuilder.Append(string.Format("{0}/", GameSparksDBAccount.Key));
            // stage
            urlBuilder.Append("/mongo/preview/");
            urlBuilder.Append(collection);

            return urlBuilder;
        }
    }
}