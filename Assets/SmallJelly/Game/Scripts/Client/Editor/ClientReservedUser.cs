﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections.Generic;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{



    /// <summary>
    /// Adds reserved users in the gamesparks environments players collection.
    /// </summary>
    public class ClientReservedUser
    {
        private static string mEmail;
        private static string mPW;

        private static string mCallbackShortCode = "reservedUsersRegistration";

        private static int mEnvironmentsStarted;
        private static int mEnvironmentsFinished;

        /// <summary>
        /// Method used to load all environments that need users created on and calls the method to create them on each environment.
        /// TODO: It would be better if this class took a GamesparksEnvironment as a parameter.  The logic to loop through the environments
        /// would be better off in the runner class. 
        /// </summary>
        public static void RegisterReservedUsers(SnapshotConfig snapshotConfig, Action successHandler, Action<string> errorHandler)
        {
            SJLogger.AssertCondition(snapshotConfig != null, "snapshotConfig cannot be null");

            Debug.Log("Registering reserved users...");

            if (!InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set.");
                return;
            }

            mEnvironmentsStarted = 0;
            mEnvironmentsFinished = 0;

            mEnvironmentsStarted++;
            RegisterUsersOnEnvironment(snapshotConfig.Source, successHandler, errorHandler);

            foreach(GameSparksEnvironment environment in snapshotConfig.DestinationList)
            {
                mEnvironmentsStarted++;
                RegisterUsersOnEnvironment(environment, successHandler, errorHandler);
            }
        }

        /// <summary>
        /// Used for deserialising the json response
        /// </summary>
        public class RegisterUsersOnEnvironmentResponse
        {
            public string success;
            public string[] usernames;
        }

        private static void RegisterUsersOnEnvironment(GameSparksEnvironment environment, Action successHandler, Action<string> errorHandler)
        {
            Debug.Log("Registering users on " + environment.Name + "...");
            StringBuilder urlBuilder = GetUrlStub(environment.Key, mCallbackShortCode, environment.ReservedUsersRegistrationCallbackSecret);

            WWWForm form = new WWWForm();
            form.AddField("query", "{}");

            Dictionary<string, string> headers = form.headers;
            byte[] rawData = form.data;

            string auth = string.Format("{0}:{1}", mEmail, mPW);
            headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

            WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

            CoroutineExecutor.Add(() => www.isDone, () =>
            {
                if (www.error != null)
                {
                    errorHandler(www.error);
                }
                else if (www.text.Contains("error"))
                {
                    errorHandler(www.text);
                }
                else
                {
                    Debug.Log(www.text);

                    // VS: this is causing errors, taking out for now, wil fix at a later date...

                    //RegisterUsersOnEnvironmentResponse response = JsonUtility.FromJson<RegisterUsersOnEnvironmentResponse>(www.text);
                    //string users = String.Join(", ", response.usernames);
                    //Debug.Log("Finished registering users " + users + " on " + environment.Name);

                    mEnvironmentsFinished++;
                    if(mEnvironmentsStarted == mEnvironmentsFinished)
                    {
                        successHandler();
                        Debug.Log("Finished creating reserved users on all environments");
                    }
                }
            });
        }

        
        
        private static bool InitialiseAccount()
        {
            mEmail = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksEmailAddress);
            mPW = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksPw);

            if (string.IsNullOrEmpty(mEmail) || string.IsNullOrEmpty(mPW))
            {
                return false;
            }

            return true;
        }

        private static StringBuilder GetUrlStub(string environmentKey, string callbackShortcode, string callbackSecret)
        {
            StringBuilder urlBuilder = new StringBuilder("https://preview.gamesparks.net/callback/");
            // key
            urlBuilder.Append(string.Format("{0}/", environmentKey));
            // stage
            urlBuilder.Append(string.Format("{0}/{1}/", callbackShortcode, callbackSecret));

            return urlBuilder;
        }
    }
}