using UnityEngine;
using SmallJelly.Framework;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System;

namespace SmallJelly
{
    public static class ClientGameSparksSnapshot
    {
        private class SnapshotObject
        {
            [JsonProperty("id")]
            public string SnapshotId { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }
        }
            
        private static Dictionary<string, string> mHeaders;
        private static string mLatestSourceSnapshot = string.Empty;

        public static void CopySnapshotToListOfEnvironments(SnapshotConfig snapshotConfig, Action successHandler, Action errorHandler)
        {
            if(!GameSparksDBAccount.InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            SetHeaders();

            WWWTracker tracker = new WWWTracker("... finished copying snapshots", true);
          
            foreach (var destination in snapshotConfig.DestinationList)
            {
                CopySnapshotToEnvironement(snapshotConfig.Source.Key, destination, tracker, ()=>
                {
                    successHandler();
                },
                    ()=>
                {
                    Debug.LogError("Copy snapshot failed");
                    errorHandler();
                });
            }
        }

        private static void CopySnapshotToEnvironement(string fromKey, GameSparksEnvironment destEnvironment, WWWTracker tracker, Action successHandler, Action errorHandler)
        {
            tracker.AddStartedExport(destEnvironment.Name);

            StringBuilder urlBuilder = GetUrlStub(fromKey);
            urlBuilder.Append(string.Format("{0}/copy/to/{1}/", mLatestSourceSnapshot, destEnvironment.Key));

            Dictionary<string, bool> postData = new Dictionary<string, bool>();
            postData.Add("includeGameConfig", destEnvironment.IncludeGameConfig);
            postData.Add("includeMetadata", destEnvironment.IncludeMetadata);
            postData.Add("includeBinaries", destEnvironment.IncludeBinaries);
            postData.Add("includeCollaborators", destEnvironment.IncludeCollaborators);

            string jsonData = JsonConvert.SerializeObject(postData);

            byte[] rawData = Encoding.UTF8.GetBytes(jsonData);

            WWW www = new WWW(urlBuilder.ToString(), rawData, mHeaders);

            CoroutineExecutor.Add(() => www.isDone, () =>
            {
                if (www.error == null)
                {
                    tracker.AddFinishedExport(destEnvironment.Name);

                    if(tracker.HasFinishedExporting())
                    {
                        successHandler();
                    }
                }
                else
                {
                    Debug.LogError("WWW Error: " + www.error);
                    errorHandler();
                }
            });
        }

        public static void CreateSnapshot(string snapshotSourceKey, string snapshotDescription, Action successHandler, Action errorHandler)
        {
            Debug.Log("*** Creating new gamesparks snapshot.");

            if(!GameSparksDBAccount.InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            SetHeaders();

			// Construct json data for new snapshot description
			Dictionary<string, string> postData = new Dictionary<string, string>();
			postData.Add("description", snapshotDescription);

			string jsonData = JsonConvert.SerializeObject(postData);

			byte[] rawData = Encoding.ASCII.GetBytes(jsonData);

			// Retrieve url to make request
            StringBuilder urlBuilder = GetUrlStub(snapshotSourceKey);

            WWW www = new WWW(urlBuilder.ToString(), rawData, mHeaders);

            CoroutineExecutor.Add(() => www.isDone, () =>
            {
                if (www.error == null)
                {
					Debug.Log("*** Retrieving id of new snapshot.");
					SnapshotObject newSnapshot = JsonConvert.DeserializeObject<SnapshotObject>(www.text);
					mLatestSourceSnapshot = newSnapshot.SnapshotId;
					
					Debug.Log(string.Format("*** Successfully created snapshot with id \"{0}\" and description \"{1}\"", newSnapshot.SnapshotId, newSnapshot.Description));
                    successHandler();
                }
                else
                {
                    Debug.LogError("WWW Error: " + www.error);
                    errorHandler();
                }
            });

        }

        private static StringBuilder GetUrlStub(string snapshotSourceKey)
        {
            StringBuilder urlBuilder = new StringBuilder("https://portal.gamesparks.net/restv2/game/");
            //source key
            urlBuilder.Append(string.Format("{0}/", snapshotSourceKey));
            //snapshot
            urlBuilder.Append("admin/snapshots/");

            return urlBuilder;
        }

        private static void SetHeaders()
        {
            mHeaders = new Dictionary<string, string>();

            string auth = string.Format("{0}:{1}", GameSparksDBAccount.Email, GameSparksDBAccount.Password);

            mHeaders.Add("Authorization", "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth)));
			mHeaders.Add("Content-Type", "application/json");
        }
    }
}