﻿using UnityEngine;

using System.Collections.Generic;
using System.Text;
using System.IO;

using SmallJelly.Framework;
using System.Collections;
using System;
using System.Text.RegularExpressions;

namespace SmallJelly
{
    /// <summary>
    /// Exports the cards from json into the database.
    /// TODO: Need a generic way of handling folders to ignore
    /// TODO: Move functionality into MongoDbCollections?
    /// </summary>
    public static class MongoDbBattleEntities
    {
        private static string mEmail;
        private static string mPW;
        private static string mKey;

        private const string sMetaCollectionName = "meta.battleEntities";
        private const string sCollectionName = "battleEntities";

        /// <summary>
        /// Method that deletes all cards from the database.
        /// </summary>
        public static void DeleteAllCards(Action successHandler, Action errorHandler)
        {
            Debug.Log("Deleting all cards...");

            if (!InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            StringBuilder urlBuilder = GetUrlStub(sMetaCollectionName);
            urlBuilder.Append("/remove");

            WWWForm form = new WWWForm();

            // remove all
            form.AddField("query", "{}");

            Dictionary<string, string> headers = form.headers;
            byte[] rawData = form.data;

            // Add a custom header for authorisation 
            string auth = string.Format("{0}:{1}", mEmail, mPW);
            headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

            // Post a request to an URL with our custom headers
            WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

            CoroutineExecutor.Add(() => www.isDone, () =>
            {
                if (www.error == null)
                {
                    string pattern = @"(?<=Affected Rows : .*)(\d*)";
                    Match match = Regex.Match(www.text, pattern);
                    if (match.Success)
                    {
                        Debug.Log(string.Format("Deleted {0} cards", match.Value));
                    }
                    successHandler();
                }
                else
                {
                    Debug.LogError("WWW Error: " + www.error);
                    errorHandler();
                }
            });
        }

        /// <summary>
        /// Method that exports all the json cards to the database.
        /// </summary>
        public static void ExportAllCards(Action<string, int> successHandler, Action<string> errorHandler)
        {
            Debug.Log("Exporting cards...");

            if (!InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            // Read in the entities from the json file.
            DirectoryInfo parentDirectory = new DirectoryInfo(FileLocations.G2UJson);

            foreach (DirectoryInfo dir in parentDirectory.GetDirectories())
            {
                // Only want battleEntities
                if(!(dir.Name.StartsWith("Weapons") 
                    || dir.Name.StartsWith("Utilities") 
                    || dir.Name.StartsWith("Nanotech")
                    || dir.Name.StartsWith("DebugCards")
                    || dir.Name.StartsWith("AutomatedTest")))
            	{
            		continue;
            	}

                foreach (FileInfo file in dir.GetFiles())
                {
                    if (file.Name.StartsWith("Test") || file.Name.StartsWith("News"))
                    {
                        continue;
                    }
                    ExportFile(file.FullName, successHandler, errorHandler);
                }
            }
        }

        public static void ExportTestCards(Action<string, int> successHandler, Action<string> errorHandler)
        {
            Debug.Log("Exporting test cards...");

            if (!InitialiseAccount())
            {
                Debug.LogError("GameSparks Account details not set");
                return;
            }

            // Read in the entities from the json file.
            DirectoryInfo parentDirectory = new DirectoryInfo(FileLocations.G2UJson);

            foreach (DirectoryInfo dir in parentDirectory.GetDirectories())
            {
                foreach (FileInfo file in dir.GetFiles())
                {
                    if (file.Name.StartsWith("Test"))
                    {
                        ExportFile(file.FullName, successHandler, errorHandler);
                    }
                }
            }
        }

        private static void ExportFile(string filename, Action<string, int> successHandler, Action<string> errorHandler)
        {
            if (filename.EndsWith(".meta"))
            {
                return;
            }

            WWWTracker tracker = new WWWTracker(filename, true);
                
            string text = File.ReadAllText(filename);

            string[] array = text.Split('}');

            foreach (string str in array)
            {
                string entityJson = str;

                // Make sure '{' is the first character
                if (!entityJson.StartsWith("{"))
                {
                    int index = entityJson.IndexOf('{');
                    if (index == -1)
                    {
                        // This is the trailing ]
                        continue;
                    }
                    entityJson = entityJson.Remove(0, index);
                }

                entityJson = string.Concat(entityJson, "}");

                // Find Id
                int idIndex = entityJson.IndexOf("\"Id\"");
                int sliceStart = idIndex + 6;
                string idString = entityJson.Substring(sliceStart, entityJson.Length - sliceStart);
                int endIndex = idString.IndexOf("\"");
                idString = idString.Substring(0, endIndex);

                // Add Json to db
                AddToBattleEntitiesDB(idString, entityJson, tracker, successHandler, errorHandler);
            }
        }

        private static bool InitialiseAccount()
        {
            mEmail = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksEmailAddress);
            mPW = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksPw);
            mKey = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksKey);

            if (string.IsNullOrEmpty(mEmail) || string.IsNullOrEmpty(mPW) || string.IsNullOrEmpty(mKey))
            {
                return false;
            }

            return true;
        }

        private static void AddToBattleEntitiesDB(string entityId, string entityData, WWWTracker tracker, Action<string, int> successHandler, Action<string> errorHandler)
        {
            tracker.AddStartedExport(entityId);

            StringBuilder urlBuilder = GetUrlStub(sMetaCollectionName);
            urlBuilder.Append("/update");

            WWWForm form = new WWWForm();

            string queryValue = "{\"entityId\" : \"" + entityId + "\"}";
            string updateValue = "{\"entityId\" : \"" + entityId + "\", \"entityData\" : " + entityData.ToString() + "}";

            form.AddField("query", queryValue);
            form.AddField("update", updateValue);
            form.AddField("upsert", "true");

            Dictionary<string, string> headers = form.headers;
            byte[] rawData = form.data;

            // Add a custom header for authorisation 
            string auth = string.Format("{0}:{1}", mEmail, mPW);
            headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

            // Post a request to an URL with our custom headers
            WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

            CoroutineExecutor.Add(() => www.isDone, () =>
            {
                if (www.error == null)
                {
                    tracker.AddFinishedExport(entityId);

                    if(tracker.HasFinishedExporting())
                    {
                        Debug.Log(string.Format("Exported {0} cards", tracker.GetNumExportedItems()));
                        successHandler(sMetaCollectionName, tracker.GetNumExportedItems());
                    }
                }
                else
                {
                    Debug.LogError("WWW Error: " + www.error);
                    errorHandler(sMetaCollectionName);
                }
            });
        }

        private static StringBuilder GetUrlStub(string collection)
        {
            StringBuilder urlBuilder = new StringBuilder("https://portal.gamesparks.net/rest/games/");
            // key
            urlBuilder.Append(string.Format("{0}/", mKey));
            // stage
            urlBuilder.Append("mongo/preview/");
            urlBuilder.Append(collection);

            return urlBuilder;
        }
    }
}