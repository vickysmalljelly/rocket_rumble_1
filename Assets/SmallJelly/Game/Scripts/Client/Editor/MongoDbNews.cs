﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using SmallJelly.Framework;
using System.Text.RegularExpressions;
using System.IO;

namespace SmallJelly
{
	/// <summary>
	/// Class that enables the deleting of and exporting to the meta.newsItems
	/// collection in the database.
    /// TODO: Replace with MongoDbCollections?
	/// </summary>
    public class MongoDbNews
    {
		private static string mEmail;
		private static string mPW;
		private static string mKey;

		private const string sMetaCollectionName = "meta.newsItems";
		private const string sCollectionName = "newsItems";

		/// <summary>
        /// Creates the newsItems collection in the database.
        /// </summary>
        public static void CreateCollection(Action<string, int> successHandler, Action<string> errorHandler)
		{
			Debug.Log("Creating meta.newsItems collection...");

			if (!InitialiseAccount())
			{
				Debug.LogError("GameSparks Account details not set");
				return;
			}

			StringBuilder urlBuilder = GetUrlStub(sCollectionName);
			urlBuilder.Append("/create");

			WWWForm form = new WWWForm();

			form.AddField("collectionType", "meta");

			Dictionary<string, string> headers = form.headers;
			byte[] rawData = form.data;

			// Add a custom header for authorisation
			string auth = string.Format("{0}:{1}", mEmail, mPW);
			headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

			// Post a request to an URL with our custom headers
			WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

			CoroutineExecutor.Add(() => www.isDone, () =>
			{
				if(www.error == null) {
					Debug.Log("Successfully created meta.newsItems");
					successHandler("news", 0);
				} else {
					Debug.LogError("WWW Error: " + www.error);
					errorHandler("news");
				}
    		});
		}

		/// <summary>
		/// Method used to empty the contents of the meta.newsItems collection
		/// in the database.
		/// </summary>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void DeleteNewsItems(Action<string, int> successHandler, Action<string> errorHandler)
		{
			Debug.Log("Deleting all news items...");

			if(!InitialiseAccount())
			{
				Debug.LogError("GameSparks Account details not set");
				return;
			}

			StringBuilder urlBuilder = GetUrlStub(sMetaCollectionName);
			urlBuilder.Append("/remove");

			WWWForm form = new WWWForm();

			// remove all
			form.AddField("query", "{}");

			Dictionary<string, string> headers = form.headers;
			byte[] rawData = form.data;

			// Add a custom header for authorisation
			string auth = string.Format("{0}:{1}", mEmail, mPW);
			headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

			WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

			CoroutineExecutor.Add(() => www.isDone, () =>
			{
				if(www.error == null)
				{
					if(ResponseHasErrors(www.text))
					{
						errorHandler("news");
						return;
					}

					const string pattern = @"(?<=Affected Rows : .*)(\d*)";
					Match match = Regex.Match(www.text, pattern);
					if(match.Success)
					{
						Debug.Log(string.Format("Deleted {0} cards", match.Value));
					}
                    else
                    {
                        Debug.LogError("Deleted cards, failed to match number of cards in Regex");
                    }
                    successHandler(sCollectionName, 0);
				}
				else
				{
					Debug.LogError("WWW Error: " + www.error);
                    errorHandler(sCollectionName);
				}
			});
		}

		/// <summary>
		/// Method used to export all the local news items stored by G2U
		/// to the meta.newsItems collection in the database.
		/// </summary>
		/// <param name="successHandler">Success handler.</param>
		/// <param name="errorHandler">Error handler.</param>
        public static void ExportAllNewsItems(Action<string, int> successHandler, Action<string> errorHandler)
		{
			Debug.Log("Exporting all news items...");

			if(!InitialiseAccount())
			{
				Debug.LogError("GameSparks Account details not set");
				return;
			}

            WWWTracker tracker = new WWWTracker("exported news items", true);

			DirectoryInfo parentDirectory = new DirectoryInfo(FileLocations.G2UJson);

			foreach (DirectoryInfo dir in parentDirectory.GetDirectories())
			{
				// Ignore folders that don't start with news
				if(!dir.Name.StartsWith("News")) 
				{
					continue;
				}

				foreach(FileInfo file in dir.GetFiles()) 
				{
					if(!file.Name.StartsWith("News")) 
					{
						continue;
					}

					ExportFile(file.FullName, tracker, successHandler, errorHandler);
				}
			}
		}

        private static void ExportFile(string filename, WWWTracker tracker, Action<string, int> successHandler, Action<string> errorHandler)
		{
			if(filename.EndsWith(".meta"))
			{
				return;
			}

			string text = File.ReadAllText(filename);

			string[] array = text.Split('}');
            int order = 0;

			foreach(string str in array)
			{
				string itemJson = str;

				if(!itemJson.StartsWith("{"))
				{
					int  index = itemJson.IndexOf('{');

					if(index == -1)
					{
						continue;
					}

					itemJson = itemJson.Remove(0, index);
				}

				itemJson = string.Concat(itemJson, "}");

				int idIndex = itemJson.IndexOf("\"Id\"");
				int sliceStart = idIndex + 6;
				string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
				int endIndex  = idString.IndexOf("\"");
				idString = idString.Substring(0, endIndex);

				AddToNewsItemsDB(idString, itemJson, order++, tracker, successHandler, errorHandler);
			}
		}

        private static void AddToNewsItemsDB(string itemId, string itemData, int itemOrder, WWWTracker tracker, Action<string, int> successHandler, Action<string> errorHandler)
		{
            tracker.AddStartedExport(itemId);

			StringBuilder urlBuilder = GetUrlStub(sMetaCollectionName);
			urlBuilder.Append("/update");

			WWWForm form = new WWWForm();

			string queryValue = "{\"itemId\" : \"" + itemId + "\"}";
			string updateValue = "{\"itemId\" : \"" + itemId + "\", \"itemData\" : " +
                itemData.ToString() + ", \"order\" : " + itemOrder + "}";

			form.AddField("query", queryValue);
			form.AddField("update", updateValue);
			form.AddField("upsert", "true");

			Dictionary<string, string> headers = form.headers;
			byte[] rawData = form.data;

			string auth = string.Format("{0}:{1}", mEmail, mPW);
			headers["Authorization"] = "Basic " + System.Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));

			WWW www = new WWW(urlBuilder.ToString(), rawData, headers);

			CoroutineExecutor.Add(()=> www.isDone, ()=>
			{
				if(www.error == null)
				{
					if(ResponseHasErrors(www.text))
					{
						errorHandler("news");
						return;
					}

                    tracker.AddFinishedExport(itemId);
					
                    if(tracker.HasFinishedExporting()) 
					{
                        Debug.Log(string.Format("Exported {0} news items", tracker.GetNumExportedItems()));
						successHandler("news", 0);
					}
				}
				else
				{
					Debug.LogError("WWW Error: " + www.error);
					errorHandler("news");
				}
			});
		}

		private static bool InitialiseAccount()
		{
			mEmail = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksEmailAddress);
			mPW = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksPw);
			mKey = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksKey);

			if(string.IsNullOrEmpty(mEmail) || string.IsNullOrEmpty(mPW) || string.IsNullOrEmpty(mKey))
			{
				return false;
			}

			return true;
		}

		private static StringBuilder GetUrlStub(string collection)
		{
			StringBuilder urlBuilder = new StringBuilder("https://portal.gamesparks.net/rest/games/");
			// key
			urlBuilder.Append(string.Format("{0}/", mKey));
			// stage
			urlBuilder.Append("mongo/preview/");
			urlBuilder.Append(collection);

			return urlBuilder;
		}

		private static bool ResponseHasErrors(string response)
		{
			const string pattern = @"(?<=errorMessage"":"")[^""]*";
			if(response.IndexOf("responseCode") != -1) {				
				Match match = Regex.Match(response, pattern);
				if(match.Success)
				{
					Debug.LogError("WWW Error: " + match.Value);
					return true;
				}
			}
			return false;
		}
    }
}
