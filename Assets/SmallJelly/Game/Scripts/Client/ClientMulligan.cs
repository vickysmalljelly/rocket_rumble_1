﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Core;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Contains requests relating to the mulligan phase of the battle
    /// </summary>
    public static class ClientMulligan  
    {
        // Short codes for events set up on the GameSparks portal
        private static readonly string mShortCodeMulliganGetOptions = "mulliganGetOptions";
        private static readonly string mShortCodeMulliganSetOptions = "mulliganSetOptions";
        private static readonly string mShortCodeMulliganComplete = "mulliganComplete";
        private static readonly string mShortCodeMulliganSkipped = "mulliganSkipped";

        /// <summary>
        /// Requests the cards for the mulligan.
        /// </summary>
        /// <param name="successHandler">Contains the BattleEntityData for the cards to be presented as options in the mulligan</param>
        /// <param name="errorHandler">Error handler.</param>
        public static void GetOptions(Action<BattleEntityData[]> successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Get card options for the mulligan");

            LogEventRequest request = new LogEventRequest();

            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", mShortCodeMulliganGetOptions, request.JSONString);
            string shortCode = mShortCodeMulliganGetOptions;
            request.SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {  
                        List<GSData> list = response.ScriptData.GetGSDataList("cards");
                        List<BattleEntityData> cards = new List<BattleEntityData>();

                        foreach(GSData gsData in list)
                        {
							BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, new SJJson(gsData) );
                            cards.Add(battleEntityData);
                            SJLogger.LogMessage(MessageFilter.Client, "Mulligan option: {0}", battleEntityData.DisplayName);
                        }

                        SJLogger.LogMessage(MessageFilter.Client, "{0} responded", mShortCodeMulliganGetOptions);

                        successHandler(cards.ToArray());
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeMulliganGetOptions);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        /// <summary>
        /// Sends the cards chosen by the player in the mulligan to the server and responds with the the final contents of the mulligan.
        /// </summary>
        /// <param name="selectedCards">Values should be set to true if the card in that position has been selected, false otherwise.</param>
        /// <param name="successHandler">Contains the BattleEntityData of the contents of the mulligam after the player has made their selection.</param>
        /// <param name="errorHandler">Error handler.</param>
        public static void SetOptions(List<bool> selectedCards, Action<BattleEntityData[]> successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Set card options for the mulligan");

            List<string> selectedCardsAsString = new List<string>();
            foreach(bool item in selectedCards)
            {
                selectedCardsAsString.Add(item.ToString());
            }

            string shortCode = mShortCodeMulliganSetOptions;
            LogEventRequest request = new LogEventRequest();

            request.SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId)
                .SetEventAttribute("cards", selectedCardsAsString);

            SJLogger.LogMessage(MessageFilter.Client, "Sending {0}, data: {1}", mShortCodeMulliganGetOptions, request.JSONString);

            request.Send((response) =>
                {
                    if(!response.HasErrors)
                    {  
                        List<GSData> list = response.ScriptData.GetGSDataList("cards");
                        List<BattleEntityData> cards = new List<BattleEntityData>();

                        foreach(GSData gsData in list)
                        {
							BattleEntityData battleEntityData = BattleEntityDataFactory.GetEntityFromJson( PlayerType.Local, new SJJson(gsData) );
                            SJLogger.LogMessage(MessageFilter.Client, "Mulligan option final: {0}", battleEntityData.DisplayName);
                            cards.Add(battleEntityData);
                        }

                        SJLogger.LogMessage(MessageFilter.Client, "{0} responded", mShortCodeMulliganSetOptions);

                        successHandler(cards.ToArray());
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", mShortCodeMulliganSetOptions);
                    ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }

        public static void NotifyComplete(Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Notify that this player has completed the mulligan");
            string shortCode = mShortCodeMulliganComplete;

            LogEventRequest request = new LogEventRequest();

            request.SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId)
                .Send((response) =>
            {
                if(!response.HasErrors)
                {  
                    SJLogger.LogMessage(MessageFilter.Client, "{0} responded", shortCode);
                    successHandler();
                }
                else
                {
                    SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                    ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                }
            });
        }

        public static void NotifySkipped(Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "Notify that this player has skipped the mulligan");
            string shortCode = mShortCodeMulliganSkipped;

            LogEventRequest request = new LogEventRequest();

            request.SetEventKey(shortCode) 
                .SetEventAttribute("challengeId", ChallengeManager.Get.ChallengeId)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {  
                        SJLogger.LogMessage(MessageFilter.Client, "{0} responded", shortCode);
                        successHandler();
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "{0} error", shortCode);
                        ServerErrorInterpreter.InterpretError<BattleGameplayInterpreter>(response.Errors, errorHandler, shortCode);
                    }
                });
        }
    }
}

