﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using SmallJelly.Framework;
using UnityEngine.Purchasing;

namespace SmallJelly
{
    /// <summary>
    /// Contains requests relating to purchases
    /// </summary>
    public static class ClientShop  
    {           
        public static void ClaimFreePack(Action successHandler, Action<ServerError> errorHandler)
        {
            SJLogger.LogMessage(MessageFilter.Client, "ClaimFreePack");
            string shortCode = "claimFreePack";

            LogEventRequest request = new LogEventRequest();
            request.SetEventKey(shortCode);
            request.Send((response) => 
            {
                if(!response.HasErrors)
                {
                    SJLogger.LogMessage(MessageFilter.Client, "ClaimFreePack success");
                    successHandler();
                }
                else
                {
                    SJLogger.LogMessage(MessageFilter.Client, "ClaimFreePack error");
                    ServerErrorInterpreter.InterpretError<FreePackErrorInterpreter>(response.Errors, errorHandler, shortCode);
                }
            });
        }

        public static void BuyVirtualGoodFromGooglePlay(string signature, string signedData, Product product, Action<Product> successHandler, Action<ServerError> errorHandler) 
        {            
            SJLogger.AssertCondition(!string.IsNullOrEmpty(signature) && !string.IsNullOrEmpty(signedData), "Need both signature and signedData to purchase from GooglePlay");

            new GooglePlayBuyGoodsRequest()
                .SetSignature(signature)
                .SetSignedData(signedData)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        AnalyticsManager.Get.RecordRealMoneyPurchase(product.metadata.isoCurrencyCode, Convert.ToInt32(product.metadata.localizedPrice), product.definition.id, product.metadata.localizedTitle, "shop");
                        AnalyticsManager.Get.RecordAddResourceEvent(IapData.Currency.Crystals, (int)ShopManager.GetNumCrystalsFromProduct(product), IapData.IapType.Crystals.ToString(), product.definition.id);
                        successHandler(product);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "BuyVirtualGoodFromGooglePlay error");
                        ServerErrorInterpreter.InterpretError<BuyVirtualGoodsErrorInterpreter>(response.Errors, errorHandler, "GooglePlayBuyGoodsRequest");
                    }
                });
        }

        public static void BuyVirtualGoodWithCrystals(IapData data, Action <string, IapData.Currency, long> successHandler, Action<ServerError> errorHandler) 
        {            
            SJLogger.LogMessage(MessageFilter.Client, "Buying {0}", data.ShortCode);

            long price = data.GetPriceInCrystals();
            SJLogger.AssertCondition(price != -1, "Trying to buy inappropriate item with crystals");

            new BuyVirtualGoodsRequest()
                .SetCurrencyType(2)
                .SetShortCode(data.ShortCode)
                .SetQuantity(1)
                .Send((response) =>
                {
                    if(!response.HasErrors)
                    {
                        AnalyticsManager.Get.RecordSubtractResourceEvent(IapData.Currency.Crystals, (int)price, data.ItemType.ToString(), data.ShortCode);
                        successHandler(data.ShortCode, IapData.Currency.Crystals, price);
                    }
                    else
                    {
                        SJLogger.LogMessage(MessageFilter.Client, "BuyVirtualGoodWithCrystals error");
                        ServerErrorInterpreter.InterpretError<BuyVirtualGoodsErrorInterpreter>(response.Errors, errorHandler, "BuyVirtualGoodsRequest");
                    }
                });
        }
            
        public static void BuyVirtualGoodWithCredits(IapData data, Action <string, IapData.Currency, long> successHandler, Action<ServerError> errorHandler) 
        {            
            SJLogger.LogMessage(MessageFilter.Client, "Buying {0}", data.ShortCode);

            long price = data.GetPriceInCredits();
            SJLogger.AssertCondition(data.GetPriceInCredits() != -1, "Trying to buy inappropriate item with credits");

            new BuyVirtualGoodsRequest()
                .SetCurrencyType(1)
                .SetShortCode(data.ShortCode)
                .SetQuantity(1)
                .Send((response) =>
            {
                if(!response.HasErrors)
                {
                    AnalyticsManager.Get.RecordSubtractResourceEvent(IapData.Currency.Credits, (int)price, data.ItemType.ToString(), data.ShortCode);
                    successHandler(data.ShortCode, IapData.Currency.Credits, price);
                }
                else
                {
                    SJLogger.LogMessage(MessageFilter.Client, "BuyVirtualGoodWithCredits error");
                    ServerErrorInterpreter.InterpretError<BuyVirtualGoodsErrorInterpreter>(response.Errors, errorHandler, "BuyVirtualGoodsRequest");
                }
            });
        }

        public static void ListVirtualGoods(Action<List<IapData>> successHandler, Action<ServerError> errorHandler) 
        {
            new ListVirtualGoodsRequest().Send((response) =>
            {
                if(!response.HasErrors)
                {
                    List<IapData> list = new List<IapData>();

                    foreach(ListVirtualGoodsResponse._VirtualGood data in response.VirtualGoods)
                    {
                        IapData iapData = new IapData();
                        iapData.Name = data.Name;
                        iapData.ShortCode = data.ShortCode;
                        iapData.Description = data.Description;

                        if(data.Type == "VGOOD")
                        {
                            if(data.Currency1Cost.GetValueOrDefault() != 0)
                            {
                                iapData.AddPrice(IapData.Currency.Credits, data.Currency1Cost.GetValueOrDefault());                        
                            }

                            if(data.Currency2Cost.GetValueOrDefault() != 0)
                            {
                                iapData.AddPrice(IapData.Currency.Crystals, data.Currency2Cost.GetValueOrDefault());                           
                            }   
                        }
                        else
                        {
                            // Product is a currency pack
                            if(!string.IsNullOrEmpty(data.GooglePlayProductId))
                            {
                                iapData.AddGooglePlayPrice(IapData.Currency.Cash, ProductManager.Get.GetLocalisedPriceString(data.GooglePlayProductId), data.GooglePlayProductId);
                            }
                        }

                        list.Add(iapData);
                    }

                    successHandler(list);
                }
                else
                {
                    SJLogger.LogMessage(MessageFilter.Client, "ListVirtualGoodsRequest error");
                    ServerErrorInterpreter.InterpretError<ConnectionErrorInterpreter>(response.Errors, errorHandler, "ListVirtualGoodsRequest");
                }
            });

        } 
    }
}

