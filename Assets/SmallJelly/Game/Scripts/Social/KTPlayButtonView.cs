﻿using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
    public class KTPlayButtonView : SJMonoBehaviour 
    {
        [SerializeField]
        private Button mButton;

        private bool mWaitingForKTPlay;
        private float mTimeLeft;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mButton != null, "mButton has not been set in inspector");

            mButton.onClick.AddListener(HandleButtonClick);

            KTPlayManager.Get.LoggedIn += HandleLoggedIn;
            KTPlayManager.Get.LoggedOut += HandleLoggedOut;
        }            

        private void OnDestroy()
        {
            mButton.onClick.RemoveListener(HandleButtonClick);
            KTPlayManager.Get.KTPlayShown -= HandleKTPlayShown;

            if(KTPlayManager.Get != null)
            {
                KTPlayManager.Get.LoggedIn -= HandleLoggedIn;
                KTPlayManager.Get.LoggedOut -= HandleLoggedOut;
            }
        }    

        private void OnEnable()
        {
            if(FeatureFlags.IsFeatureEnabled(FeatureFlags.KTPlay) && KTPlayManager.Get.IsLoggedIn())
            {
                mButton.interactable = true;
            }
            else
            {
                mButton.interactable = false;               
            }    
        }

        private void Update()
        {
            if(mWaitingForKTPlay)
            {
                mTimeLeft -= Time.deltaTime;
                if(mTimeLeft < 0)
                {
                    mWaitingForKTPlay = false;

                    DialogManager.Get.HideSpinner();
                    GameManager.Get.KTPlayConnectionFailed();
                    mButton.onClick.AddListener(HandleButtonClick);
                }
            }
        }

        private void HandleButtonClick()
        {            
            // Stop listening to button clicks until KTPlay is closed again
            mButton.onClick.RemoveListener(HandleButtonClick);

            KTPlayManager.Get.KTPlayShown -= HandleKTPlayShown;
            KTPlayManager.Get.KTPlayShown += HandleKTPlayShown;

            mWaitingForKTPlay = true;
            mTimeLeft = 10;

            KTPlayManager.Get.Show();

            DialogManager.Get.ShowSpinner();
        }

        private void HandleKTPlayShown()
        {
            mWaitingForKTPlay = false;
            mButton.onClick.AddListener(HandleButtonClick);
            KTPlayManager.Get.KTPlayShown -= HandleKTPlayShown;

            DialogManager.Get.HideSpinner();
        }


        private void HandleLoggedOut()
        {            
            mButton.interactable = false;
        }

        private void HandleLoggedIn(string displayName)
        {
            if(FeatureFlags.IsFeatureEnabled(FeatureFlags.KTPlay))
            {                
                mButton.interactable = true;
            }
        }
    }
}