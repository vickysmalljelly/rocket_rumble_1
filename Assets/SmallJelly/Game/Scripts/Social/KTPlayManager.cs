﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
    public class KTPlayManager : MonoBehaviourSingleton<KTPlayManager> 
    {
        public event Action<string> LoggedIn;
        public event Action LoggedOut;
        public event Action KTPlayShown;
        public event Action KTPlayHidden;

        private KTPlay.KTPlayDelegate KTPlayShow;
        private KTPlay.KTPlayDelegate KTPlayHide;
        private float mPlaylistVolume;
        private float mMasterVolume;


        private void OnEnable()
        {
            KTPlayShow = HandleKTPlayShow;
            KTPlayHide = HandleKTPlayHide;

            KTPlay.SetViewDidAppearDelegate(KTPlayShow);
            KTPlay.SetViewDidDisappearDelegate(KTPlayHide);
        }

        public void Show()
        {
            SJLogger.LogMessage(MessageFilter.System, "Show KT Play");      

            KTPlay.Show();
        }

        public void Refresh()
        {
            SJLogger.LogMessage(MessageFilter.System, "Refresh KT Play - ");
            if(KTAccountManager.isLoggedIn())
            {
                SJLogger.LogMessage(MessageFilter.System, "KT Play - already logged in");
                return;
            }

            if(ClientAuthentication.PlayerAccountDetails != null)
            {
                // Try to log in
                SJLogger.LogMessage(MessageFilter.System, "KT Play - try to log in");
                Login(ClientAuthentication.PlayerAccountDetails.DisplayName);
            }
        }

        public void Login(string displayName)
        {         
            KTAccountManager.LoginWithGameUser(displayName, (KTUser user, KTError error) => 
            {
                if(error == null)
                {                  
                    FireLoggedIn(displayName);

                    KTAccountManager.UpdateProfile(displayName, string.Empty, 0, 
                        (bool isSuccess, KTUser user1, KTError error1) => 
                        {
                            if(isSuccess)
                            {                                                 
                                SJLogger.LogMessage(MessageFilter.System, "KTPlay UpdateProfile success");     
                                SJLogger.LogMessage(MessageFilter.System, "Header url = " + user1.headerUrl);
                            } 
                            else 
                            {                              
                                Debug.LogError("KTPlay UpdateProfile error: " + error1.code + " " + error1.description);                                
                            }
                        });
                } 
                else 
                {
                    Debug.LogError("Failed to log in: " + error.code + " " + error.description);
                    FireLoggedOut();
                }
            });
        }

        public bool IsLoggedIn()
        {
            return KTAccountManager.isLoggedIn();
        }

        public void Logout()
        {
            KTAccountManager.Logout();
        }

        private void FireLoggedIn(string displayName)
        {
            SJLogger.LogMessage(MessageFilter.System, "KTPlay logged in {0}", displayName);
            if(LoggedIn != null)
            {
                LoggedIn(displayName);
            }
        }

        private void FireLoggedOut()
        {
            if(LoggedOut != null)
            {
                LoggedOut();
            }
        }

        private void HandleKTPlayShow()
        {
            SJLogger.LogMessage(MessageFilter.System, "HandleKTPlayShow");

            // Remember current audio settings
            mPlaylistVolume = DarkTonic.MasterAudio.MasterAudio.PlaylistMasterVolume;
            mMasterVolume = DarkTonic.MasterAudio.MasterAudio.MasterVolumeLevel;

            // Reduce volume to 0
            DarkTonic.MasterAudio.MasterAudio.PlaylistMasterVolume = 0;
            DarkTonic.MasterAudio.MasterAudio.MasterVolumeLevel = 0;

            if(KTPlayShown != null)
            {
                KTPlayShown();
            }
        }            

        private void HandleKTPlayHide()
        {
            SJLogger.LogMessage(MessageFilter.System, "HandleKTPlayHide");

            // Revert audio settings
            DarkTonic.MasterAudio.MasterAudio.PlaylistMasterVolume = mPlaylistVolume;
            DarkTonic.MasterAudio.MasterAudio.MasterVolumeLevel = mMasterVolume;

            if(KTPlayHidden != null)
            {
                KTPlayHidden();
            }
        }
    }
}