﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Handles Mouse and Touch Input for the SJRendering System.
    /// TODO: Move to Framework.
    /// </summary>
	public class PlayerInputManager : MonoBehaviourSingleton<PlayerInputManager> 
	{
		#region Mouse Event Handlers
		public event EventHandler< InputEventArgs > MouseRolledOver
		{ 
			add 
			{ 
				mInput.MouseRolledOver += value;
			} 
			remove 
			{ 
				mInput.MouseRolledOver -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseHovered
		{ 
			add 
			{ 
				mInput.MouseHovered += value;
			} 
			remove 
			{ 
				mInput.MouseHovered -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseRolledOff
		{ 
			add 
			{ 
				mInput.MouseRolledOff += value;
			} 
			remove 
			{ 
				mInput.MouseRolledOff -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseDraggedOver
		{ 
			add 
			{ 
				mInput.MouseDraggedOver += value;
			} 
			remove 
			{ 
				mInput.MouseDraggedOver -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseDragged
		{ 
			add 
			{ 
				mInput.MouseDragged += value;
			} 
			remove 
			{ 
				mInput.MouseDragged -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseDraggedOff
		{ 
			add 
			{ 
				mInput.MouseDraggedOff += value;
			} 
			remove 
			{ 
				mInput.MouseDraggedOff -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MousePressed
		{ 
			add 
			{ 
				mInput.MousePressed += value;
			} 
			remove 
			{ 
				mInput.MousePressed -= value; 
			} 
		}

		public event EventHandler< InputEventArgs > MouseReleased
		{ 
			add 
			{ 
				mInput.MouseReleased += value;
			} 
			remove 
			{ 
				mInput.MouseReleased -= value; 
			} 
		}
		#endregion

		#region Touch Event Handlers
		public event EventHandler< InputEventArgs > TouchDraggedOver
		{  
			add 
			{  
				mInput.TouchDraggedOver += value;
			}  
			remove 
			{  
				mInput.TouchDraggedOver -= value; 
			}  
		}

		public event EventHandler< InputEventArgs > TouchDragged
		{  
			add 
			{  
				mInput.TouchDragged += value;
			}  
			remove 
			{  
				mInput.TouchDragged -= value; 
			}  
		}

		public event EventHandler< InputEventArgs > TouchDraggedOff
		{  
			add 
			{  
				mInput.TouchDraggedOff += value;
			}  
			remove 
			{  
				mInput.TouchDraggedOff -= value; 
			}  
		}

		public event EventHandler< InputEventArgs > TouchPressed
		{  
			add 
			{  
				mInput.TouchPressed += value;
			}  
			remove 
			{  
				mInput.TouchPressed -= value; 
			}  
		}

		public event EventHandler< InputEventArgs > TouchReleased
		{  
			add 
			{  
				mInput.TouchReleased += value;
			}  
			remove 
			{  
				mInput.TouchReleased -= value; 
			}  
		}
		#endregion

		#region General Event Handlers
		public event EventHandler<RenderRaycastEventArgs> RayUpdated
		{ 
			add 
			{ 
				mInput.RayUpdated += value;
			} 
			remove 
			{ 
				mInput.RayUpdated -= value; 
			} 
		}
		#endregion

		#region Member Variables
		private AbstractInput mInput;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			mInput = GetInput();
		}

		private void OnEnable()
		{
		}

		private void OnDisable()
		{
		}
		#endregion

		#region Public Methods
		public void OnUpdate()
		{
			mInput.OnUpdate();
		}
		#endregion

		#region Private Methods
		private AbstractInput GetInput()
		{
			#if UNITY_STANDALONE
			return gameObject.AddComponent< MouseInput >();
			#elif UNITY_EDITOR
			return gameObject.AddComponent< MouseInput >();
			#elif UNITY_IOS
			return gameObject.AddComponent< TouchInput >();
			#elif UNITY_ANDROID
			return gameObject.AddComponent< TouchInput >();
			#else
			return gameObject.AddComponent< MouseInput >();
			#endif
		}
		#endregion
	}
}
