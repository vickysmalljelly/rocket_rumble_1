﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public abstract class AbstractInput : SJMonoBehaviour
	{
		#region Event Handlers
		public event EventHandler< InputEventArgs > MouseRolledOver;
		public event EventHandler< InputEventArgs > MouseHovered;
		public event EventHandler< InputEventArgs > MouseRolledOff;
		public event EventHandler< InputEventArgs > MouseDraggedOver;
		public event EventHandler< InputEventArgs > MouseDragged;
		public event EventHandler< InputEventArgs > MouseDraggedOff;
		public event EventHandler< InputEventArgs > MousePressed;
		public event EventHandler< InputEventArgs > MouseReleased;


		public event EventHandler< InputEventArgs > TouchDraggedOver;
		public event EventHandler< InputEventArgs > TouchDragged;
		public event EventHandler< InputEventArgs > TouchDraggedOff;
		public event EventHandler< InputEventArgs > TouchPressed;
		public event EventHandler< InputEventArgs > TouchReleased;


		public event EventHandler< RenderRaycastEventArgs > RayUpdated;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();
		}

		public abstract void OnUpdate();
		#endregion

		#region Mouse Event Firing
		protected void OnMouseRolledOver( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseRolledOver != null)
			{
				MouseRolledOver( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		
		protected void OnMouseHovered( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseHovered != null)
			{
				MouseHovered( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		
		protected void OnMouseRolledOff( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseRolledOff != null)
			{
				MouseRolledOff( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}

		protected void OnMouseDraggedOver( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseDraggedOver != null)
			{
				MouseDraggedOver( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}

		protected void OnMouseDragged( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseDragged != null)
			{
				MouseDragged( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}


		protected void OnMouseDraggedOff( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseDraggedOff != null)
			{
				MouseDraggedOff( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		
		protected void OnMousePressed( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MousePressed != null)
			{
				MousePressed( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		
		protected void OnMouseReleased( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(MouseReleased != null)
			{
				MouseReleased( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		#endregion

		#region Touch Event Firing
		protected void OnTouchDraggedOver( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(TouchDraggedOver != null)
			{
				TouchDraggedOver( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}

		protected void OnTouchDragged( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(TouchDragged != null)
			{
				TouchDragged( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}


		protected void OnTouchDraggedOff( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(TouchDraggedOff != null)
			{
				TouchDraggedOff( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}

		protected void OnTouchPressed( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(TouchPressed != null)
			{
				TouchPressed( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}

		protected void OnTouchReleased( SJRenderRaycastHit[] renderHitStack, Vector3 movementDuringFrame )
		{
			if(TouchReleased != null)
			{
				TouchReleased( this, new InputEventArgs( renderHitStack, movementDuringFrame ) );
			}
		}
		#endregion

		#region Event Handlers
		protected void OnRayUpdated(Vector2 screenPositon, SJRenderRaycastHit[] renderHitStack)
		{
			if(RayUpdated != null)
			{
				RayUpdated( this, new RenderRaycastEventArgs( screenPositon, renderHitStack ) );
			}
		}
		#endregion
	}
}