using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class MouseInput : AbstractInput 
	{
		#region Enums
		public enum MouseClickPhase
		{
			None,
			Pressing,
			Holding,
			Releasing
		}
		#endregion

		#region Member Variables
		private SJRenderRaycastHit mLastRaycastHit;
		private Vector2 mPreviousMousePosition;
		#endregion

		#region Unity Methods
		public override void OnUpdate()
		{
			SJRenderRaycastHit[] renderHit;
			bool hit = SJRenderCanvas.Get.Raycast( Input.mousePosition, out renderHit);

			//Calculate movement since last frame as a percentage of the screen height/width
			Vector2 mouseDeltaPosition = new Vector2( Input.mousePosition.x - mPreviousMousePosition.x, Input.mousePosition.y - mPreviousMousePosition.y );
			Vector2 movementSinceLastFrame = new Vector2( mouseDeltaPosition.x / Screen.width, mouseDeltaPosition.y / Screen.height );

			MouseClickPhase phase;
			if( Input.GetMouseButtonDown (0) ) 
			{
				phase = MouseClickPhase.Pressing;
			} 
			else if( Input.GetMouseButtonUp (0) ) 
			{
				phase = MouseClickPhase.Releasing;

			} 
			else if( Input.GetMouseButton (0) ) 
			{
				phase = MouseClickPhase.Holding;
			} 
			else 
			{
				phase = MouseClickPhase.None;
			}
			

			//If nothing was hit in this frame
			if(!hit)
			{
				//If something was hit in the previous frame then we have rolled off of it.
				if( mLastRaycastHit != null )
				{
					List<SJRenderRaycastHit> renderHitStackClone = new List<SJRenderRaycastHit>( renderHit );
					renderHitStackClone.Insert( 0, mLastRaycastHit );

					SJRenderRaycastHit[] hitArray = renderHitStackClone.ToArray();
					OnMouseRolledOff( hitArray, movementSinceLastFrame );

					if( phase == MouseClickPhase.Holding )
					{
						OnMouseDraggedOff( hitArray, movementSinceLastFrame );
					}
				}
				return;
			}

			//If we have hit a new gameObject
			SJRenderRaycastHit raycastHit = renderHit[0];

			if( mLastRaycastHit == null || raycastHit.GroupGameObject != mLastRaycastHit.GroupGameObject )
			{
				//If we hit an object in the last frame then we have rolled off of it.
				if( mLastRaycastHit != null )
				{
					List<SJRenderRaycastHit> renderHitStackClone = new List<SJRenderRaycastHit>( renderHit );
					renderHitStackClone.Insert( 0, mLastRaycastHit );

					SJRenderRaycastHit[] hitArray = renderHitStackClone.ToArray();
					OnMouseRolledOff( hitArray, movementSinceLastFrame );

					if( phase == MouseClickPhase.Holding )
					{
						OnMouseDraggedOff( hitArray, movementSinceLastFrame );
					}
				}

				//We want to rollover the new gameObject
				OnMouseRolledOver( renderHit, movementSinceLastFrame );

				if( phase == MouseClickPhase.Holding )
				{
					OnMouseDraggedOver( renderHit, movementSinceLastFrame );
				}
			}
			else
			{
				//Hitting the same gameObject
				OnMouseHovered( renderHit, movementSinceLastFrame );
			}

			//If the mouse button was pressed in this frame
			if( phase == MouseClickPhase.Pressing )
			{				
				OnMousePressed( renderHit, movementSinceLastFrame );
			}

			if( phase == MouseClickPhase.Holding )
			{
				if( !(Mathf.Abs( mouseDeltaPosition.x ) < 1.0f && Mathf.Abs( mouseDeltaPosition.y ) < 1.0f) )
				{
					OnMouseDragged( renderHit, movementSinceLastFrame );
				}
			}

			//If the mouse button was released in this frame
			if( phase == MouseClickPhase.Releasing )
			{
				OnMouseReleased( renderHit, movementSinceLastFrame );
			}
				
			mLastRaycastHit = raycastHit;

			OnRayUpdated( Input.mousePosition, renderHit );

			mPreviousMousePosition = Input.mousePosition;
		}
		#endregion
	}
}
