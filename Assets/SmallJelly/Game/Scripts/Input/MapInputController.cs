﻿using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    public class MapInputController : SJMonoBehaviour 
    {

        #region Tweakable variables

		[SerializeField]
		private float mZoomMouseIncrement = 100f;

        [SerializeField]
		private float mZoomTouchIncrement = 500.0f;

        [SerializeField]
        private float mMinX = 0f;

        [SerializeField]
        private float mMaxX = 1000f;

        [SerializeField]
        private float mMinY = -100f;

        [SerializeField]
        private float mMaxY = 150f;

        [SerializeField]
        private float mMinZ = -950f;

        [SerializeField]
        private float mMaxZ = 400f;

		[SerializeField]
		private Camera mCamera;

		[SerializeField]
		private Vector3 targetPosition;
        #endregion

        [SerializeField]
        private GameObject mMapCameraParent;

		private Vector2 mPrevPointerPosition;

		private bool mIsZooming;

        protected override void Awake()
        {
            base.Awake();

            SJLogger.AssertCondition(mMapCameraParent != null, "mMapCameraParent must be set in the inspector");
            SJLogger.AssertCondition(mCamera != null, "mCamera must be set in the inspector");
        }

        private void Update()
        {
			#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			//Use touch controls for touch devices
			UpdateTouchScroll();
			UpdateTouchPan();
			#else
			//Use mouse controls for all other devices
			UpdateMouseScroll();
			UpdateMousePan();
			#endif


        }

		#region Mouse Input
        private void UpdateMouseScroll()
        {
            float scroll = Input.GetAxis(MouseButton.ScrollWheelAxis);
            if(scroll == 0.0f)
            {
                // No zooming needed
                return;
            }
                
            float zoomIncrement;
            if(scroll > 0f)
            {
                zoomIncrement = mZoomMouseIncrement;
            }
            else
            {
                zoomIncrement = -mZoomMouseIncrement;
            }
            Vector3 currentPos = mMapCameraParent.transform.position;
            mMapCameraParent.transform.position = new Vector3(currentPos.x, currentPos.y, Mathf.Clamp(currentPos.z + zoomIncrement, mMinZ, mMaxZ));
        
			EnsureCameraIsInBounds();
		}

        private void UpdateMousePan()
        {
			if(!Input.GetMouseButton(MouseButton.Left))
			{
				// We are not dragging
				return;
			}

            if(Input.GetMouseButtonDown(MouseButton.Left))
            {
				mPrevPointerPosition = Input.mousePosition;
                return;
            }
				
			RaycastHit hitInfo;
			Ray ray = mCamera.ScreenPointToRay( Input.mousePosition );

			if( Physics.Raycast( ray, out hitInfo ) )
			{
				Vector2 currentTouchPosition = Input.mousePosition;
				Vector3 screenPointDifference = currentTouchPosition - mPrevPointerPosition;
				Vector3 targetPosition = mCamera.ScreenToWorldPoint( new Vector3( ( Screen.width / 2.0f ) - screenPointDifference.x,  ( Screen.height / 2.0f ) - screenPointDifference.y, hitInfo.distance ) );
				targetPosition = new Vector3( targetPosition.x, targetPosition.y, mMapCameraParent.transform.position.z );

				mMapCameraParent.transform.position = new Vector3( targetPosition.x, targetPosition.y, mMapCameraParent.transform.position.z );

				mPrevPointerPosition = currentTouchPosition;
			}

			EnsureCameraIsInBounds();
        }
		#endregion

		#region Touch Input
		private void UpdateTouchScroll()
		{
			//We've finished zooming
			if( mIsZooming && Input.touchCount == 0 )
			{
				mIsZooming = false;
			}

			if( Input.touchCount < 2 )
			{
				return;
			}

			//We're now zooming!
			mIsZooming = true;

			Touch firstTouch = Input.GetTouch(0);
			Touch secondTouch = Input.GetTouch(1);

			Vector2 firstTouchPrevPosition = firstTouch.position - firstTouch.deltaPosition;
			Vector2 secondTouchPrevPosition = secondTouch.position - secondTouch.deltaPosition;

			float prevTouchDeltaMag = ( firstTouchPrevPosition - secondTouchPrevPosition ).magnitude;
			float touchDeltaMag = ( firstTouch.position - secondTouch.position ).magnitude;

			float deltaMagDiff = (prevTouchDeltaMag - touchDeltaMag) / ((float)Screen.height);
			mMapCameraParent.transform.position = new Vector3( mMapCameraParent.transform.position.x, mMapCameraParent.transform.position.y, Mathf.Clamp( mMapCameraParent.transform.position.z - ( deltaMagDiff * mZoomTouchIncrement ), mMinZ, mMaxZ ) );
		
			EnsureCameraIsInBounds();
		}

		private void UpdateTouchPan()
		{
			//No touch panning if the controller is zooming
			if( mIsZooming )
			{
				return;
			}

			if( Input.touchCount != 1 )
			{
				// We are not dragging
				return;
			}

			RaycastHit hitInfo;
			Ray ray = mCamera.ScreenPointToRay( Input.GetTouch(0).position );

			if( Input.GetTouch(0).phase == TouchPhase.Began )
			{
				mPrevPointerPosition = Input.GetTouch( 0 ).position;
				return;
			}

			if( Physics.Raycast( ray, out hitInfo ) )
			{
				Vector2 currentTouchPosition = Input.GetTouch( 0 ).position;
				Vector3 screenPointDifference = currentTouchPosition - mPrevPointerPosition;
				Vector3 newPosition = mCamera.ScreenToWorldPoint( new Vector3( ( Screen.width / 2.0f ) - screenPointDifference.x,  ( Screen.height / 2.0f ) - screenPointDifference.y, hitInfo.distance ) );

				mMapCameraParent.transform.position = new Vector3( newPosition.x, newPosition.y, mMapCameraParent.transform.position.z );

				mPrevPointerPosition = currentTouchPosition;
			}

			EnsureCameraIsInBounds();
		}
		#endregion

		#region Private Methods
		private void EnsureCameraIsInBounds()
		{
			if( mMapCameraParent.transform.position.x < mMinX )
			{
				mMapCameraParent.transform.position = new Vector3( mMinX, mMapCameraParent.transform.position.y, mMapCameraParent.transform.position.z );
			}

			if( mMapCameraParent.transform.position.x > mMaxX )
			{
				mMapCameraParent.transform.position = new Vector3( mMaxX, mMapCameraParent.transform.position.y, mMapCameraParent.transform.position.z );
			}

			if( mMapCameraParent.transform.position.y < mMinY )
			{
				mMapCameraParent.transform.position = new Vector3( mMapCameraParent.transform.position.x, mMinY, mMapCameraParent.transform.position.z );
			}

			if( mMapCameraParent.transform.position.y > mMaxY )
			{
				mMapCameraParent.transform.position = new Vector3( mMapCameraParent.transform.position.x, mMaxY, mMapCameraParent.transform.position.z );
			}

			if( mMapCameraParent.transform.position.z < mMinZ )
			{
				mMapCameraParent.transform.position = new Vector3( mMapCameraParent.transform.position.x, mMapCameraParent.transform.position.y, mMinZ );
			}

			if( mMapCameraParent.transform.position.z > mMaxZ )
			{
				mMapCameraParent.transform.position = new Vector3( mMapCameraParent.transform.position.x, mMapCameraParent.transform.position.y, mMaxZ );
			}
		}
		#endregion
    }
}

