﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public abstract class ObjectInputListener : SJMonoBehaviour 
	{
		#region Public Events

		//Mouse
		//Fired when the object is initially rolled over
		public event EventHandler< InputEventArgs > MouseRolledOver;

		//Fired each frame that the user continues to rollover the object
		public event EventHandler< InputEventArgs > MouseHovered;

		//Fired when the object is rolled off
		public event EventHandler< InputEventArgs > MouseRolledOff;

		//Fired when the object is dragged over
		public event EventHandler< InputEventArgs > MouseDraggedOver;

		//Fired when the object is dragged
		public event EventHandler< InputEventArgs > MouseDragged;

		//Fired when the object is dragged off
		public event EventHandler< InputEventArgs > MouseDraggedOff;

		//Fired when the object is pressed
		public event EventHandler< InputEventArgs > MousePressed;

		//Fired when the object is released
		public event EventHandler< InputEventArgs > MouseReleasedOver;

		//Fired when the press is cancelled by releasing the input outside of the object collision area 
		//(i.e. Pressing, rolling off and then releasing - useful for returning button states to previous values etc)
		public event EventHandler< InputEventArgs > MouseReleasedOff;


		//Touch
		//Fired when the object is dragged over
		public event EventHandler< InputEventArgs > TouchDraggedOver;

		//Fired when the object is dragged
		public event EventHandler< InputEventArgs > TouchDragged;

		//Fired when the object is dragged off
		public event EventHandler< InputEventArgs > TouchDraggedOff;

		//Fired when the object is pressed
		public event EventHandler< InputEventArgs > TouchPressed;

		//Fired when the object is released
		public event EventHandler< InputEventArgs > TouchReleasedOver;

		//Fired when the press is cancelled by releasing the input outside of the object collision area 
		//(i.e. Pressing, rolling off and then releasing - useful for returning button states to previous values etc)
		public event EventHandler< InputEventArgs > TouchReleasedOff;
		#endregion



		#region Mouse Event Firing
		protected void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseDraggedOver != null)
			{
				MouseDraggedOver( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseDragged( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseDragged != null)
			{
				MouseDragged( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseDraggedOff != null)
			{
				MouseDraggedOff( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseRollOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseRolledOver != null)
			{
				MouseRolledOver( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseHover( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseHovered != null)
			{
				MouseHovered( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseRollOff( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseRolledOff != null)
			{
				MouseRolledOff( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMousePress( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MousePressed != null)
			{
				MousePressed( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(MouseReleasedOver != null)
			{
				MouseReleasedOver( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			if(MouseReleasedOff != null)
			{
				MouseReleasedOff(this, new InputEventArgs( elementsHit,  movementDuringFrame ) );
			}
		}
		#endregion

		#region Touch Event Firing
		protected void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(TouchDraggedOver != null)
			{
				TouchDraggedOver( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnTouchDragged( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(TouchDragged != null)
			{
				TouchDragged( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(TouchDraggedOff != null)
			{
				TouchDraggedOff( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnTouchPress( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(TouchPressed != null)
			{
				TouchPressed( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if(TouchReleasedOver != null)
			{
				TouchReleasedOver( this, new InputEventArgs( elementsHitBehind, movementDuringFrame )  );
			}
		}

		protected void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			if(TouchReleasedOff != null)
			{
				TouchReleasedOff(this, new InputEventArgs( elementsHit, movementDuringFrame ) );
			}
		}
		#endregion
	}
}
