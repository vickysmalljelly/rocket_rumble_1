﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class PlayerObjectInputListener : ObjectInputListener
	{
		#region Member Variables
		private bool mIsApplicationQuitting;
		#endregion

		#region Unity Methods
		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			//Do not unregister if the application is quitting - the singletons that events are registered to could have been destroyed
			if( !mIsApplicationQuitting )
			{
				UnRegisterListeners();
			}
		}

		private void OnMouseApplicationQuit()
		{
			mIsApplicationQuitting = true;
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			//Mouse
			PlayerInputManager.Get.MouseRolledOver += HandleMouseRolledOver;
			PlayerInputManager.Get.MouseHovered += HandleMouseHovered;
			PlayerInputManager.Get.MouseRolledOff += HandleMouseRolledOff;
			PlayerInputManager.Get.MouseDraggedOver += HandleMouseDraggedOver;
			PlayerInputManager.Get.MouseDragged += HandleMouseDragged;
			PlayerInputManager.Get.MouseDraggedOff += HandleMouseDraggedOff;
			PlayerInputManager.Get.MousePressed += HandleMousePressed;
			PlayerInputManager.Get.MouseReleased += HandleMouseReleased;

			//Touch
			PlayerInputManager.Get.TouchDraggedOver += HandleTouchDraggedOver;
			PlayerInputManager.Get.TouchDragged += HandleTouchDragged;
			PlayerInputManager.Get.TouchDraggedOff += HandleTouchDraggedOff;
			PlayerInputManager.Get.TouchPressed += HandleTouchPressed;
			PlayerInputManager.Get.TouchReleased += HandleTouchReleased;
		}

		private void UnRegisterListeners()
		{
            if(PlayerInputManager.Get != null)
            {
    			//Mouse
    			PlayerInputManager.Get.MouseRolledOver -= HandleMouseRolledOver;
    			PlayerInputManager.Get.MouseHovered -= HandleMouseHovered;
    			PlayerInputManager.Get.MouseRolledOff -= HandleMouseRolledOff;
    			PlayerInputManager.Get.MouseDraggedOver -= HandleMouseDraggedOver;
    			PlayerInputManager.Get.MouseDragged -= HandleMouseDragged;
    			PlayerInputManager.Get.MouseDraggedOff -= HandleMouseDraggedOff;
    			PlayerInputManager.Get.MousePressed -= HandleMousePressed;
    			PlayerInputManager.Get.MouseReleased -= HandleMouseReleased;

    			//Touch
    			PlayerInputManager.Get.TouchDraggedOver -= HandleTouchDraggedOver;
    			PlayerInputManager.Get.TouchDragged -= HandleTouchDragged;
    			PlayerInputManager.Get.TouchDraggedOff -= HandleTouchDraggedOff;
    			PlayerInputManager.Get.TouchPressed -= HandleTouchPressed;
    			PlayerInputManager.Get.TouchReleased -= HandleTouchReleased;
            }
		}
		#endregion

		#region Mouse Event Handlers
		private void HandleMouseRolledOver(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseRollOver( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseHovered(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseHover( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseRolledOff(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseRollOff( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseDraggedOver(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseDraggedOver( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseDragged(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseDragged( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseDraggedOff(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseDraggedOff( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMousePressed(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMousePress( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleMouseReleased(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnMouseReleasedOver(inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
			else
			{
				OnMouseReleasedOff( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}
		#endregion

		#region Touch Event Handlers
		private void HandleTouchDraggedOver(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnTouchDraggedOver( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleTouchDragged(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnTouchDragged( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleTouchDraggedOff(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnTouchDraggedOff( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleTouchPressed(object o, InputEventArgs inputEventArgs)
		{
			if(inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnTouchPress( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		private void HandleTouchReleased(object o, InputEventArgs inputEventArgs)
		{
			if( inputEventArgs.Array[0].GroupGameObject == gameObject)
			{
				OnTouchReleasedOver( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
			else
			{
				OnTouchReleasedOff( inputEventArgs.Array, inputEventArgs.MovementDuringFrame );
			}
		}

		#endregion
	}
}