﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Animations")]
	[Tooltip("This shakes the camera but I suppose it could be used for shaking other things too if you wanted to.")]
	public class CameraShake : FsmStateAction
	{
		#region PlayMaker Variables
		public FsmOwnerDefault ownerGameObject;
		[RequiredField]
		[Tooltip("The time in seconds you want to shake for.")]
		public FsmFloat shakeTime;

		[HasFloatSlider(0.1f, 5f)]
		[Tooltip("Increase or decrease the intesity of the rotation shake.")]
		public FsmFloat rotationShakeIntensityMultiplier;

		[HasFloatSlider(0.1f, 5f)]
		[Tooltip("Increase or decrease the intesity of the translation shake.")]
		public FsmFloat translationShakeIntensityMultiplier;

		[Tooltip("Tick to use the intestity curve.")]
		public bool useIntensityCurve;

		[RequiredField]
		[Tooltip("Set a curve to change the intensity over time")]
		public FsmAnimationCurve shakeIntensityCurve;

		private GameObject objectToShake;
		private Transform objTransform;
		private Vector3 originPosition;
		private Quaternion originRotation;
		private Vector3 objectPosition;
		private Quaternion objectRotation;
		private float currentShakeTime;
		private float rotationShakeIntensity;
		private float translationShakeIntensity;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			shakeTime = 0f;
			rotationShakeIntensityMultiplier = 1f;
			translationShakeIntensityMultiplier = 1f;
			useIntensityCurve = false;
			shakeIntensityCurve = null;

		}

		public override void OnEnter()
		{
			objectToShake = Fsm.GetOwnerDefaultTarget(ownerGameObject);
			objTransform = objectToShake.GetComponent(typeof(Transform)) as Transform;
			originPosition = objTransform.localPosition;
			originRotation = objTransform.localRotation;
		}

		public override void OnUpdate()
		{

			if (currentShakeTime <= shakeTime.Value)
			{
				rotationShakeIntensity = 1f;
				translationShakeIntensity = 1f;

				if (useIntensityCurve)
				{
					//Normalise the shake time
					float curveEvaluationTime = 1 / shakeTime.Value * currentShakeTime;

					//Evaluate curve at the normalised time - get the shake intensity out
					rotationShakeIntensity = shakeIntensityCurve.curve.Evaluate(curveEvaluationTime);
					translationShakeIntensity = shakeIntensityCurve.curve.Evaluate(curveEvaluationTime);
				}

				//Modify the shake intensity
				translationShakeIntensity = translationShakeIntensity * translationShakeIntensityMultiplier.Value * 0.1f;
				rotationShakeIntensity = rotationShakeIntensity * rotationShakeIntensityMultiplier.Value * 0.01f;

				//Shake it baby
				objectToShake.transform.position = originPosition + Random.insideUnitSphere * translationShakeIntensity;
				objectToShake.transform.rotation = new Quaternion
					(
					originRotation.x + Random.Range (-rotationShakeIntensity,rotationShakeIntensity),
					originRotation.y + Random.Range (-rotationShakeIntensity,rotationShakeIntensity),
					originRotation.z + Random.Range (-rotationShakeIntensity,rotationShakeIntensity),
 					originRotation.w + Random.Range (-rotationShakeIntensity,rotationShakeIntensity) 
					);
				
				currentShakeTime += Time.deltaTime;
				
			}

			else
			{
				ExitState();
			}
				
		}
		private void ExitState()
		{
			currentShakeTime = 0;
			Finish();
		}
		#endregion
	}
}