﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class AnimationMetaData
	{
		#region Public Properties
		public Dictionary< string, NamedVariable> Data
		{
			get
			{
				return mData;
			}
		}
		#endregion

		#region Member Variables
		private readonly Dictionary< string, NamedVariable> mData;
		#endregion

		#region Constructors
		public AnimationMetaData( Dictionary< string, NamedVariable> data )
		{
			mData = data;
		}
		#endregion
	}
}