﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
    /// <summary>
    /// For use with Playmaker
    /// </summary>
	public abstract class AnimationController : SJMonoBehaviour
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished;
		#endregion

		#region Public Properties
		public List<PlayMakerFSM> ActiveFSMs
		{ 
			get
			{
				return mActiveFSMs;
			}
			set
			{ 
				mActiveFSMs = value;
			}
		}

		public List<PlayMakerFSM> PooledInactiveFSMs
		{ 
			get
			{
				return mPooledInactiveFSMs;
			}
			set
			{ 
				mPooledInactiveFSMs = value;
			}
		}
		#endregion

		#region Protected Properties
		protected abstract string ResourcePath { get; }

		[SerializeField]
		private List<PlayMakerFSM> mActiveFSMs;

		[SerializeField]
		private List<PlayMakerFSM> mPooledInactiveFSMs;
		#endregion

		#region Member Variables
		protected PlayMakerSharedVariables mPlayMakerSharedVariables;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake ();

			ActiveFSMs = new List<PlayMakerFSM>();
			mPooledInactiveFSMs = new List<PlayMakerFSM>();

			mPlayMakerSharedVariables = gameObject.GetComponent<PlayMakerSharedVariables>();
			if( mPlayMakerSharedVariables == null )
			{
				mPlayMakerSharedVariables = gameObject.AddComponent<PlayMakerSharedVariables>();
			}
		}



		private void Update()
		{
			//Remove inactive FSMs
			for(int count = 0; count < ActiveFSMs.Count; count++)
			{
				if( !ActiveFSMs[ count ].Active )
				{
					StopAnimation( ActiveFSMs[ count ] );
					count--;
				}
			}
		}

		private void OnEnable()
		{
			//Add an initial pooled FSM
			PlayMakerFSM playMakerFSM = gameObject.AddComponent< PlayMakerFSM >();
			mPooledInactiveFSMs.Add( playMakerFSM );
		}

		private void OnDisable()
		{
			//Stop animations when the animation controller is disabled
			StopExistingAnimations();

			//Remove playmaker fsms
			foreach( PlayMakerFSM playmakerFsm in mPooledInactiveFSMs )
			{
				if ( playmakerFsm != null )
				{
					Destroy( playmakerFsm );
				}
			}

			mPooledInactiveFSMs.Clear();
		}
		#endregion

		#region Protected Methods
		protected bool AnimationExists( params string[] pathElements )
		{
			string templateName = String.Join( "/", pathElements );
			string fullPath = string.Format("{0}{1}", ResourcePath, templateName );

			return Resources.Load< FsmTemplate >( fullPath ) != default( FsmTemplate );
		}

		protected void PlayAnimation( params string[] pathElements )
		{
			//Create complete path to the animation
			PlayAnimation( String.Join( "/", pathElements ) );
		}

		protected void PlayAnimation( string templateName )
		{
			FsmTemplate fsmTemplate = Resources.Load< FsmTemplate >(string.Format("{0}{1}", ResourcePath, templateName) );

			SJLogger.AssertCondition( fsmTemplate != null, "FsmTemplate must not be null" );

			PlayMakerFSM playMakerFSM;

			if( mPooledInactiveFSMs.Count > 0 )
			{
				playMakerFSM = mPooledInactiveFSMs[ 0 ];
				mPooledInactiveFSMs.RemoveAt( 0 );
			}
			else
			{
				playMakerFSM = gameObject.AddComponent<PlayMakerFSM>();
			}

			//If the playmaker fsm is disabled then a parent must be disabled somewhere!
			if( !playMakerFSM.gameObject.activeSelf )
			{
				GameObject disabledObject = gameObject;
				while( disabledObject.activeSelf )
				{
					disabledObject = disabledObject.transform.parent.gameObject;
				}

				SJLogger.LogError( "{0} is disabled, it's preventing the animation {1} from running! ({2})", disabledObject.name, templateName, disabledObject.activeSelf );
			}
			playMakerFSM.SetFsmTemplate( fsmTemplate );
			playMakerFSM.Fsm.Start();

			ActiveFSMs.Add(playMakerFSM);
		}

		public void StopExistingAnimations()
		{
			while( ActiveFSMs.Count > 0 )
			{
				PlayMakerFSM firstFSM = ActiveFSMs[ 0 ];
				StopAnimation( firstFSM );
			}
		}

		protected void SetMetaData( AnimationMetaData animationMetaData )
		{
			foreach( KeyValuePair< string, NamedVariable > animationVariableData in animationMetaData.Data )
			{
				mPlayMakerSharedVariables.Add( animationVariableData.Key, animationVariableData.Value );
			}
		}
		#endregion

		#region Private Methods
		private void StopAnimation( PlayMakerFSM playMakerFsm )
		{
			SJLogger.LogMessage(MessageFilter.George, "Stop Playmaker FSM on GameObject " + gameObject.name );
			ActiveFSMs.Remove( playMakerFsm );
			mPooledInactiveFSMs.Add( playMakerFsm );

			SJLogger.LogMessage(MessageFilter.George, "Template name " + playMakerFsm.FsmTemplate.name );

			playMakerFsm.Fsm.Stop();
			playMakerFsm.SetFsmTemplate( default( FsmTemplate ) );

			SJLogger.LogMessage(MessageFilter.George, "Number of FSMs " + ActiveFSMs.Count );

			foreach( PlayMakerFSM playmaker in ActiveFSMs )
			{
				SJLogger.LogMessage(MessageFilter.George, "Existing FSM name " + playmaker.FsmTemplate.name );
			}

			if(ActiveFSMs.Count == 0)
			{
				SJLogger.LogMessage(MessageFilter.George, "FireAnimationsFinished" );

				FireAnimationsFinished();
			}
		}
		#endregion

		#region Event Firing
		private void FireAnimationsFinished()
		{
			if(AnimationFinished != null)
			{
				AnimationFinished(this, EventArgs.Empty);
			}
		}
		#endregion
	}
}
