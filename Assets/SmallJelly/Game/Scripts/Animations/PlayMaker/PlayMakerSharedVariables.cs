﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class PlayMakerSharedVariables : SJMonoBehaviour 
	{
		#region Member Variables
		private Dictionary<string, NamedVariable > mCachedVariables;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			mCachedVariables = new Dictionary<string, NamedVariable>();
		}
		#endregion

		#region Methods
		public void Add( string key, NamedVariable namedVariable )
		{
			//Remove existing variable - where it exists
			mCachedVariables.Remove( key );
			mCachedVariables.Add( key, namedVariable );
		}

		public NamedVariable Get( string name )
		{
			NamedVariable namedVariable = null;
			mCachedVariables.TryGetValue( name, out namedVariable );
			return namedVariable;
		}

		public bool ContainsKey( string name )
		{
			return mCachedVariables.ContainsKey( name );
		}

		public bool Remove( string name )
		{
			return mCachedVariables.Remove( name );
		}
		#endregion
	}
}
