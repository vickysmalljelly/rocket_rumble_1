﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("SmallJelly-Animations")]
	[Tooltip("This shakes the camera but I suppose it could be used for shaking other things too if you wanted to.")]
	public class GuiShake : FsmStateAction
	{
		#region PlayMaker Variables
		public FsmOwnerDefault ownerGameObject;
		[RequiredField]
		[Tooltip("The time in seconds you want to shake for.")]
		public FsmFloat shakeTime;

		[HasFloatSlider(0.1f, 5f)]
		[Tooltip("Increase or decrease the intesity of the translation shake.")]
		public FsmFloat translationShakeIntensityMultiplier;

		[Tooltip("Tick to use the intestity curve.")]
		public bool useIntensityCurve;

		[RequiredField]
		[Tooltip("Set a curve to change the intensity over time")]
		public FsmAnimationCurve shakeIntensityCurve;

		private GameObject objectToShake;
		private RectTransform objTransform;
		private Vector3 originPosition;
		//private Quaternion originRotation;
		private Vector3 objectPosition;
		private float currentShakeTime;
		private float rotationShakeIntensity;
		private float translationShakeIntensity;
		#endregion

		#region PlayMaker Methods
		public override void Reset()
		{
			shakeTime = 0f;
			translationShakeIntensityMultiplier = 1f;
			useIntensityCurve = false;
			shakeIntensityCurve = null;

		}

		public override void OnEnter()
		{
			objectToShake = Fsm.GetOwnerDefaultTarget(ownerGameObject);
			objTransform = objectToShake.GetComponent(typeof(RectTransform)) as RectTransform;
			originPosition = objTransform.position;

		}

		public override void OnUpdate()
		{

			if (currentShakeTime <= shakeTime.Value)
			{

				translationShakeIntensity = 1f;

				if (useIntensityCurve)
				{
					//Normalise the shake time
					float curveEvaluationTime = 1 / shakeTime.Value * currentShakeTime;

					//Evaluate curve at the normalised time - get the shake intensity out
					translationShakeIntensity = shakeIntensityCurve.curve.Evaluate(curveEvaluationTime);
				}

				//Modify the shake intensity
				translationShakeIntensity = translationShakeIntensity * translationShakeIntensityMultiplier.Value;

				objectToShake.transform.position = new Vector3
					(
					originPosition.x + Random.Range(-translationShakeIntensity,translationShakeIntensity),
					originPosition.y + Random.Range(-translationShakeIntensity,translationShakeIntensity),
					originPosition.z
					);
						
				currentShakeTime += Time.deltaTime;
				
			}

			else
			{
				ExitState();
			}
				
		}
		private void ExitState()
		{
			currentShakeTime = 0;
			Finish();
		}
		#endregion
	}
}