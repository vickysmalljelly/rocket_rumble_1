﻿public abstract class SequentialTest 
{
    public enum SequentialTestState
    {
        NotStarted,
        InProgress,
        Failed,
        Succeeded
    }

    protected SequentialTestState mCurrentState;

    public SequentialTestState CurrentState { get { return mCurrentState; } }

    public abstract void Initialise();
    public abstract void Update();
}
