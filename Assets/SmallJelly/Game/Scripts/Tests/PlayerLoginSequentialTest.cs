﻿using SmallJelly.Framework;
using System.Collections;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Tests if a player can log in and get to the GAME_STATE_START_MENU state.
    /// </summary>
    public class PlayerLoginSequentialTest : SequentialTest
    {
        private string mUsername;
        private string mPassword;

        //private bool mSentLogin;
        private bool mCanLogIn;

        private LoginController mLoginController;

        public override void Initialise()
        {
            /*
            // Are we in the right state to start running this test?
            if (!GameManager.Get.GameStateMachine.CheckActiveStateId(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH) &&
                !GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE))
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Starting in incorrect state");
                mCurrentState = SequentialTestState.Failed;
                return;
            }

            GameManager.Get.DoCoroutine(LogoutPlayer());

            SetLoginDetails();

            mCurrentState = SequentialTestState.InProgress;
            */
        }

        public override void Update()
        {
            if (mCurrentState != SequentialTestState.InProgress)
            {
                return;
            }

            //if (!mSentLogin)
            //{
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for authentication state");
                // We are waiting to reach the authentication state 
                /*
                if (GameManager.Get.GameStateMachine.CheckActiveStateId(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH))
                {
                    // Trigger the login
                    SJLogger.LogMessage(MessageFilter.AutomatedTests, "Log the player in");
                    GameManager.Get.DoCoroutine(LoginPlayer());
                    mSentLogin = true;
                    return;
                }
                */

                //;
           // }
            
            if(mLoginController != null)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Checking login errors");
                CheckLoginErrors();
            }

            if (mCanLogIn)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for rumble state");
                // Have we reached the state after the player has logged in?  
                if (GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE))
                {
                    mCurrentState = SequentialTestState.Succeeded;
                    return;
                }
            }
        }

        private void SetLoginDetails()
        {
            this.mUsername = "autotest";
            this.mPassword = "pw";
        }

        private void CheckLoginErrors()
        {
            ServerError serverError = mLoginController.ATGetServerError();
            if (serverError == null)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "No login errors");
                return;
            }

            if (serverError.ErrorType == ServerErrorType.AuthDetailsNotRecognised)
            {
                Debug.LogError(this.GetType() + ": Tried to log in with username that doesn't exist in the db: " + this.mUsername);
            }
            else
            {
                Debug.LogError(this.GetType() + ": Error type: " + serverError.ErrorType.ToString() + "; Error message: " + serverError.UserFacingMessage + "; Error details: " + serverError.TechnicalDetails);
            }
            mCurrentState = SequentialTestState.Failed;
        }

        private IEnumerator LogoutPlayer()
        {
            yield return new WaitForSeconds(2);
            PlayMenuController startMenuController = UIManager.Get.GetMenuController<PlayMenuController>();
            startMenuController.OnLogoutClicked();
        }

        private IEnumerator LoginPlayer()
        {
            yield return new WaitForSeconds(2);
            AuthenticationMenuController authenticationMenuController = UIManager.Get.GetMenuController<AuthenticationMenuController>();
            mLoginController = authenticationMenuController.LoginController;
            mLoginController.ATSetUsernameAndPassword(mUsername, mPassword);
            mLoginController.OnOkButtonClicked();
            mCanLogIn = true;
        }
    }
}
