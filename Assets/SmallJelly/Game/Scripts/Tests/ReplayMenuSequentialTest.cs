﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Tests if game can get to the replay screen which is in REPLAY_STATE_START state.
    /// </summary>
    public class ReplayMenuSequentialTest : SequentialTest
    {

        private bool mClickedReplay;

        public override void Initialise()
        {
            if(!GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE))
            {
                SJLogger.LogError("Starting in incorrect state");
                mCurrentState = SequentialTestState.Failed;
                return;
            }

            if(!FeatureFlags.Replays)
            {
                Debug.LogError(this.GetType() + ": The replay feature is not turned on in the project.");
                mCurrentState = SequentialTestState.Failed;
                return;
            }

            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if(mCurrentState != SequentialTestState.InProgress)
            {
                return;
            }

            if(!mClickedReplay)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for Rumble state");
                if(GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE))
                {
                    PlayMenuController startMenuController = UIManager.Get.GetMenuController<PlayMenuController>();
                    startMenuController.OnReplayClicked();
                    mClickedReplay = true;
                    return;
                }
            }

            if(GameManager.Get.GameStateMachine.CheckActiveStateId(ReplayStateMachine.REPLAY_STATE_START)) {
                mCurrentState = SequentialTestState.Succeeded;
                return;
            }
        }
    }

}
