﻿using SmallJelly.Framework;
using System.Collections;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Tests if game can successfully start a replay and gets to the correct battleState (checked on the backend).
    /// </summary>
    public class ReplayStartSequentialTest : SequentialTest
    {
        private string mReplayName;
        private bool mSentReplayName;
        private bool mBattleStateMatched;
        private bool mAnimationsCheckStarted;
        private bool mAnimationsCheckFinished;
        private bool mBattleCheckStarted;
        private bool mOverwriteIfBattleStatesDontMatch = true;

        public ReplayStartSequentialTest(string replay)
        {
            if (string.IsNullOrEmpty(replay))
            {
                Debug.LogError(this.GetType() + ": Replay name can not be empty.");
                mCurrentState = SequentialTestState.Failed;
            }

            mReplayName = replay;
        }

        public override void Initialise()
        {
            ReplayManager.Get.Initialise();

            if (!GameManager.Get.GameStateMachine.CheckActiveStateId(ReplayStateMachine.REPLAY_STATE_START))
            {
                Debug.LogError(this.GetType() + ": Starting in incorrect state.");
                mCurrentState = SequentialTestState.Failed;
                return;
            }
            
            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if (mCurrentState != SequentialTestState.InProgress)
            {
                return;
            }

            if (!mSentReplayName)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for REPLAY_STATE_START");
                if (GameManager.Get.GameStateMachine.CheckActiveStateId(ReplayStateMachine.REPLAY_STATE_START))
                {
                    SendReplayName();
                    mSentReplayName = true;
                    return;
                }
            }

            if(!ReplayManager.Get.ReplayOver) 
            {
                return;
            }

            if (!mAnimationsCheckStarted)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for the battle to have started");
                 
                if (GameManager.Get.GameStateMachine.CheckActiveStateId( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE ) ||
					GameManager.Get.GameStateMachine.CheckActiveStateId( RemotePlayerTurnInteractionStateMachine.BATTLE_REMOTE_PLAYER_INTERACTION_STATE ))
                {
                    GameManager.Get.DoCoroutine(CheckAnimationsHaveFinished());
                    mAnimationsCheckStarted = true;
                    return;
                }
            }
         
            if (mAnimationsCheckFinished)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Animations have finished");
                if (!mBattleCheckStarted)
                {
                    SJLogger.LogMessage(MessageFilter.AutomatedTests, "Checking battle states");
                    GameManager.Get.DoCoroutine(CheckBattleStates());
                    mBattleCheckStarted = true;
                    return;
                }

                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Checking mBattleStateMatched");
                if(mBattleStateMatched)
                {
                    mCurrentState = SequentialTestState.Succeeded;
                }
            }
        }

        private void SendReplayName()
        {
            SJLogger.LogMessage(MessageFilter.AutomatedTests, "Set replay name {0}", mReplayName);

            ReplayMenuController replayMenuController = UIManager.Get.GetMenuController<ReplayMenuController>();
            replayMenuController.ATSetReplayName(mReplayName);
            replayMenuController.OnOkButtonClicked();       
        }

        private IEnumerator CheckBattleStates()
        {
            yield return new WaitForSeconds(2);
            ClientReplay.CompareBattleStates(mOverwriteIfBattleStatesDontMatch, () =>
            {
                mBattleStateMatched = true;
            }, (ServerError error) =>
            {
                // Log an error so that the test fails
                Debug.LogError("Replay test failed: " + error.UserFacingMessage + " " + error.TechnicalDetails);
            });
        }

        private IEnumerator CheckAnimationsHaveFinished()
        {
            yield return new WaitForSeconds(2);
            if(BattleManager.Get.AnimationsHaveFinished)
            {
                mAnimationsCheckFinished = true;
            } else
            {
                mAnimationsCheckStarted = false;
            }
        }
    }
}
