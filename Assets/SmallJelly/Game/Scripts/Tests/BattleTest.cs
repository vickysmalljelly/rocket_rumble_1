﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Game test which tries to check if you can end a turn.
    /// Times out and fails after 20 seconds.
    /// </summary>
    /// <remarks>
    /// Timeout is set in the Test Component Inspector.
    /// </remarks>
    public class BattleTest : MulliganPhase
    {
        private RocketRumbleTestRunner mTestRunner;

        void Start()
        {
            mTestRunner = new RocketRumbleTestRunner();
            mTestRunner.AddTest(new LoadSceneSequentialTest());
            mTestRunner.AddTest(new PlayerLoginSequentialTest());
            mTestRunner.AddTest(new MulliganPhaseSequentialTest());
            mTestRunner.AddTest(new BattleSequentialTest());
        }

        void Update()
        {
            mTestRunner.Update();
        }
    }
}
