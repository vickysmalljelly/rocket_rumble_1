﻿using UnityEngine;
using UnityEngine.Assertions;

namespace SmallJelly
{
    /// <summary>
    /// Performs a basic Unity assertion. This example should always pass.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.
    /// Unity raiseExceptions should be set to true unless it will just log.
    /// </remarks>
    public class AssertPass : MonoBehaviour
    {
        void Start()
        {
            Assert.raiseExceptions = true;
            try
            {
                Assert.AreEqual(2 + 2, 4);
                IntegrationTest.Pass();
            }
            catch(AssertionException)
            {
                IntegrationTest.Fail();
            }
        }
    }
}