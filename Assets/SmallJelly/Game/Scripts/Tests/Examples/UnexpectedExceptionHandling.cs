﻿using System;

using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Performs an exception throw that is "unexpected". This example should always fail.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.                                         
    /// Notice in the Unity Test Component that "Expect Exception" is turned off.
    /// </remarks>
    public class UnexpectedExceptionHandling : MonoBehaviour
    {
        void Start()
        {
            throw new Exception();
        }
    }
}