﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Performs a basic check and makes sure the integration test fails.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.
    /// </remarks>
    public class BasicFail : MonoBehaviour
    {
        void Start()
        {
            if(2 + 2 != 5)
            {
                IntegrationTest.Fail();
            }
        }
    }
}