﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Performs a basic check and makes sure the integration test passes.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.
    /// </remarks>
    public class BasicPass : MonoBehaviour
    {
        void Start()
        {
            if(2 + 2 == 4)
            {
                IntegrationTest.Pass();
            }
        }
    }
}