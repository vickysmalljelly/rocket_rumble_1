﻿using UnityEngine;
using UnityEngine.Assertions;

namespace SmallJelly
{
    /// <summary>
    /// Performs a basic Unity assertion. This example should always fail.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.
    /// Unity raiseExceptions should be set to true unless it will just log.
    /// </remarks>
    public class AssertFail : MonoBehaviour
    {
        void Start()
        {
            Assert.raiseExceptions = true;
            try
            {
                Assert.AreEqual(2 + 2, 5);
                IntegrationTest.Pass();
            }
            catch(AssertionException)
            {
                IntegrationTest.Fail();
            }
        }
    }
}