﻿using System;

using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Performs an exception throw that is "expected". This example should always pass.
    /// </summary>
    /// <remarks>
    /// This is just a simple test.                                         
    /// Notice in the Unity Test Component that "Expect Exception" and "Succeed when exception is thrown" is turned on.
    /// </remarks>
    public class ExpectedExceptionHandling : MonoBehaviour
    {
        void Start()
        {
            throw new Exception();
        }
    }
}