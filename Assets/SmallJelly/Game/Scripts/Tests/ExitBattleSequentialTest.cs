﻿using UnityEngine.SceneManagement;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// 
    /// </summary>
    public class ExitBattleSequentialTest : SequentialTest
    {
        private Scene mMainScene;
        private bool mNoNeedToCheckBattle;
        private bool mBattleConceeded;

        public override void Initialise()
        {
            mMainScene = SceneManager.GetSceneByName("Main");

            if(!mMainScene.IsValid())
            {
                // This is the first test, the scene isn't loaded yet.
                mNoNeedToCheckBattle = true;
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "The scene is not loaded, no need to check battle");
            }

            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if(mNoNeedToCheckBattle)
            {
                mCurrentState = SequentialTestState.Succeeded;
                return;
            }

            if(!mBattleConceeded) {
                if(GameManager.Get.GameStateMachine.CheckActiveStateId(RemotePlayerTurnInteractionStateMachine.BATTLE_REMOTE_PLAYER_INTERACTION_STATE)
                    || GameManager.Get.GameStateMachine.CheckActiveStateId(LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE))
                {
                    SJLogger.LogMessage(MessageFilter.AutomatedTests, "Quit the battle");
                    BattleManager.Get.BattleGameplayController.HandleExitButtonClicked();
                    mBattleConceeded = true;
                    return;
                }
            }


            if(GameManager.Get.GameStateMachine.CheckActiveStateId(GameStateMachine.GAME_STATE_DEMO_UPSELL))
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Ok the upsell");
                DemoUpsellDisplayController controller = UIManager.Get.GetMenuController<DemoUpsellDisplayController>();
                controller.OnFinished();
                return;
            }

            SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting to return to rumble state");
            if(GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE))
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Returned to rumble state");
                mCurrentState = SequentialTestState.Succeeded;
            }
        }
    }
}
