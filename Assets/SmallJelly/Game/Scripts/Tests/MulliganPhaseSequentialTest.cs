﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Tests if player can successfully perform a mulligan and get to the BATTLE_IDLE_INTERACTION_STATE.
    /// </summary>
    public class MulliganPhaseSequentialTest : SequentialTest
    {
        
        private bool mClickedBattle;
        //private bool mClickedMulligan;

        public override void Initialise()
        {
            // Are we in the right state to start running this test?
            if(!GameManager.Get.GameStateMachine.CheckActiveStateId(SinglePlayerStateMachine.SINGLE_PLAYER_STATE_IDLE))
            {
                Debug.LogError(this.GetType() + ": Starting in incorrect state");
                mCurrentState = SequentialTestState.Failed;
                return;
            }

            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if(!mClickedBattle)
            {
                //SinglePlayerMenuController menuController = UIManager.Get.GetMenuController<SinglePlayerMenuController>();

                //if(menuController.IsBattleReady() && !this.mClickedBattle)
                {
                    //menuController.OnBattleClicked();
                    mClickedBattle = true;
                    return;
                }
            }

            Debug.LogError("Taken out mulligan for map demo");

            //if(GameManager.Get.GameStateMachine.CheckActiveStateId(BattleStateMachine.BATTLE_STATE_MULLIGAN_FIRST) || 
            //    GameManager.Get.GameStateMachine.CheckActiveStateId(BattleStateMachine.BATTLE_STATE_MULLIGAN_SECOND) && 
            //    !mClickedMulligan)
            {                        
                return;
            }
                
            // Have we reached the state after the mulligan?
            //if(GameManager.Get.GameStateMachine.CheckActiveStateId(InteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE))
            //{
            //    mCurrentState = SequentialTestState.Succeeded;
            //    return;
            //}
        }
    }
    
}
