﻿using SmallJelly.Framework;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Tests the end turn functionality in battle by clicking end turn and handling accordingly.
    /// </summary>
    public class BattleSequentialTest : SequentialTest
    {
        private bool mClickedEndTurn;

        public override void Initialise()
        {
            if (!GameManager.Get.GameStateMachine.CheckActiveStateId(LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE))
            {
                Debug.LogError(this.GetType() + ": Starting in incorrect state");
                mCurrentState = SequentialTestState.Failed;
                return;
            }

            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if(!mClickedEndTurn)
            {
                ClientBattle.EndTurn(HandleSuccess, HandleFailure);
                mClickedEndTurn = true;
            }
        }

        public void HandleSuccess()
        {
            mCurrentState = SequentialTestState.Succeeded;
            return;
        }

        public void HandleFailure(ServerError error)
        {
            Debug.LogError(this.GetType() + ": Throwing server error: " + error.ToString());
            mCurrentState = SequentialTestState.Failed;
            return;
        }
    }
}
