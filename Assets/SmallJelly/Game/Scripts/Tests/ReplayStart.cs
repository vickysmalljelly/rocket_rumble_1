﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Game test which tries to perform the replay matching the public ReplayName member variable.
    /// Can be used for any replay as long as it exists in your database. Fails if it doesn't.
    /// </summary>
    /// <remarks>
    /// Timeout is set in the Test Component Inspector.
    /// </remarks>
    public class ReplayStart : MonoBehaviour
    {
        public string ReplayName;
        
        private RocketRumbleTestRunner mTestRunner;

        void Start()
        {
            mTestRunner = new RocketRumbleTestRunner();

            mTestRunner.AddTest(new ExitBattleSequentialTest()); // If the previous test failed we need to exit the battle
            mTestRunner.AddTest(new LoadSceneSequentialTest());
            mTestRunner.AddTest(new PlayerLoginSequentialTest());
            mTestRunner.AddTest(new ReplayMenuSequentialTest());
            mTestRunner.AddTest(new ReplayStartSequentialTest(ReplayName));
            mTestRunner.AddTest(new ExitBattleSequentialTest());
        }

        void Update()
        {
            mTestRunner.Update();
        }
    }
}
