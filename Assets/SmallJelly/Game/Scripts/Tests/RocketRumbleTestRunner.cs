﻿using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Main test runner. Queues up tests and runs them sequentially.
    /// </summary>
    public class RocketRumbleTestRunner 
    {
        public RocketRumbleTestRunner()
        {
        }

        private Queue<SequentialTest> mTests = new Queue<SequentialTest>();
        private SequentialTest mCurrentTest;

        public void AddTest(SequentialTest test)
        {
            Debug.Log("Adding test " + test.GetType());

            mTests.Enqueue(test);
        }

        public void Update()
        {
            if(mCurrentTest == null)
            {
                // Initialise the first test
                mCurrentTest = mTests.Dequeue();
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Initialising test {0}", mCurrentTest.GetType());
                mCurrentTest.Initialise();
            }

            switch(mCurrentTest.CurrentState)
            {
                case SequentialTest.SequentialTestState.NotStarted:
                    Debug.LogError("Test " + mCurrentTest.GetType() + " failed to initialise");
                    IntegrationTest.Fail();
                    return;

                case SequentialTest.SequentialTestState.InProgress:
                    //SJLogger.LogMessage(MessageFilter.AutomatedTests, "Updating test {0}, replay {1}", mCurrentTest.GetType(), mReplayName);
                    mCurrentTest.Update();
                    return;

                case SequentialTest.SequentialTestState.Failed:
                    IntegrationTest.Fail();
                    return;

                case SequentialTest.SequentialTestState.Succeeded:

                    SJLogger.LogMessage(MessageFilter.AutomatedTests, "Test succeeded {0}", mCurrentTest.GetType());
                    
                    if(mTests.Count == 0)
                    {
                        // All tests have succeded
                        IntegrationTest.Pass();
                        return;
                    }

                    // Initialise the next test
                    mCurrentTest = mTests.Dequeue();
                    mCurrentTest.Initialise();

                    return;

                default:
                    Debug.LogError(mCurrentTest.CurrentState + " not recognised");
                    return;
               
            }
           
        }
    }
}
