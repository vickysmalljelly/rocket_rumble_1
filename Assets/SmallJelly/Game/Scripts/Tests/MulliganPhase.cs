﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Game test which tries to performa a mulligan.
    /// Times out and fails after 10 seconds.
    /// </summary>
    /// <remarks>
    /// Timeout is set in the Test Component Inspector.
    /// </remarks>
    public class MulliganPhase : MonoBehaviour
    {
        private RocketRumbleTestRunner mTestRunner;

        void Start()
        {
            mTestRunner = new RocketRumbleTestRunner();
            mTestRunner.AddTest(new LoadSceneSequentialTest());
            mTestRunner.AddTest(new PlayerLoginSequentialTest());
            mTestRunner.AddTest(new MulliganPhaseSequentialTest());
        }

        void Update()
        {
            mTestRunner.Update();
        }
    }
}
