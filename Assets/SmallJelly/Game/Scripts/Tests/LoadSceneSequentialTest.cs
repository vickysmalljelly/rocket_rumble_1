﻿using UnityEngine.SceneManagement;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Tests if the main scene loads and the game gets to the GAME_STATE_START_MENU or GAME_STATE_AUTHENTICATION_MENU state.
    /// </summary>
    public class LoadSceneSequentialTest : SequentialTest
    {
        private Scene mMainScene;

        public override void Initialise()
        {
            mMainScene = SceneManager.GetSceneByName("Main");

            if(mMainScene.IsValid())
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "The scene is already loaded");
                mCurrentState = SequentialTestState.InProgress;
                return;
            }

            SceneManager.LoadScene("Main", LoadSceneMode.Additive);
            mMainScene = SceneManager.GetSceneByName("Main");

            mCurrentState = SequentialTestState.InProgress;
        }

        public override void Update()
        {
            if (mCurrentState != SequentialTestState.InProgress)
            {
                return;
            }

            if (mMainScene.isLoaded)
            {
                SJLogger.LogMessage(MessageFilter.AutomatedTests, "Waiting for for scene loaded state");
                // If the game can't log you in automatically for any reason it will jump the start menu state
                // and go straight into authentication menu
                /*
                if(GameManager.Get.GameStateMachine.CheckActiveStateId(FrontEndStateMachine.PLAY_STATE) ||
                    GameManager.Get.GameStateMachine.CheckActiveStateId(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH))
                {
                    mCurrentState = SequentialTestState.Succeeded;
                    
                }*/
            }
        }
    }
}
