﻿using SmallJelly.Framework;

namespace SmallJelly
{
    public enum MatchmakingMode
    {
        NotSet,
        Random,
        Ranked
    }

    public enum BattleMode
    { 
        NotSet,
        TwoPlayer,
        SinglePlayer,
        Replay
    }

    public enum BattleId
    {
        NotSet,
        PvP,
        Replay,
        SinglePlayer,
        Tutorial1,
        Tutorial2,
        Tutorial3,
        Tutorial4
    }

    /// <summary>
    /// Responsible for holding information about the current battle mode.
    /// </summary>
    public class BattleModeManager : MonoBehaviourSingleton<BattleModeManager>
    {
        public void SetBattleConfig(BattleMode battleMode, BattleId battleId, MatchmakingMode matchmakingMode)
        {
            CurrentBattleMode = battleMode;
            CurrentBattleId = battleId;
            CurrentMatchmakingMode = matchmakingMode;
            SJLogger.LogMessage(MessageFilter.Gameplay, "BattleMode: {0}, BattleId: {1}, MatchmakingMode: {2}", CurrentBattleMode, CurrentBattleId, CurrentMatchmakingMode);
        }

        /// <summary>
        /// The battle mode we are using
        /// </summary>
        public BattleMode CurrentBattleMode { get; private set; }

        /// <summary>
        /// How the battle config is identified in the database
        /// </summary>
        public BattleId CurrentBattleId { get; private set; } 

        public MatchmakingMode CurrentMatchmakingMode { get; private set; } 

        /// <summary>
        /// Gets the name of the challenge as set on the server
        /// </summary>
        public string GetChallengeShortCode()
        {
            switch(CurrentBattleMode)
            {
                case BattleMode.TwoPlayer:
                    return "battleChallenge";
                case BattleMode.SinglePlayer:
                    return "aiChallenge";
                case BattleMode.Replay:
                    return "replayChallenge";
                default:
                    SJLogger.Assert("Battle mode {0} not recognised", CurrentBattleMode);
                    return string.Empty;
            }
        }

        public string GetMatchmakingMode() 
        {
            switch(CurrentMatchmakingMode)
            {
                case MatchmakingMode.Random:
                    return "matchRandom";
                case MatchmakingMode.Ranked:
                    return "matchRanked";
                default:
                    SJLogger.Assert("Matchmaking mode {0} not recognised", CurrentMatchmakingMode);
                    return string.Empty;       
            }
        }

        public string GetRankedBattleMatchType()
        {
            return "matchRanked";
        }

        public string GetRandomBattleMatchType()
        {
            return "matchRandom";
        }
    }
}
