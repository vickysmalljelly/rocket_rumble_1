﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChoosePilot : SJMonoBehaviour
	{
		#region Exposed To Inspector
		[SerializeField]
		private ChoosePilotController mChoosePilotController;
		#endregion

		#region Public Methods
		public void OnInsertState( ChoosePilotController.State state )
		{
			mChoosePilotController.OnInsertState( state );
		}
		#endregion
	}
}