﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChoosePilotView : SJMonoBehaviour
	{
		#region Exposed To Inspector
		[SerializeField]
		private ChoosePilotAnimationController mChoosePilotAnimationController;
		#endregion

		#region Public Methods
		public void OnPlayAnimation( ChoosePilotController.State state )
		{
			mChoosePilotAnimationController.OnPlayAnimation( state );
		}
		#endregion
	}
}