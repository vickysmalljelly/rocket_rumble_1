﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChooseShipAnimationController : AnimationController
	{
		#region Constants
		private const string INITIALIZATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string ANCIENT_ANIMATION_TEMPLATE_NAME = "AncientAnimation";
		private const string ENFORCER_ANIMATION_TEMPLATE_NAME = "EnforcerAnimation";
		private const string SMUGGLER_ANIMATION_TEMPLATE_NAME = "SmugglerAnimation";

		#endregion

		#region Protected Properties
		protected override string ResourcePath 
		{ 
			get 
			{ 
				return string.Format( "{0}/{1}/{2}/", FrameworkFileLocations.PLAYMAKER_TEMPLATES_DIRECTORY, "FrontEnd", "ChooseShips" ); 
			}
		}
		#endregion


		#region Public Methods
		public void OnPlayAnimation( ChooseShipController.State state )
		{
			switch( state )
			{
				case ChooseShipController.State.Initialisation:
					PlayAnimation( INITIALIZATION_ANIMATION_TEMPLATE_NAME );
					break;

				case ChooseShipController.State.Ancient:
					PlayAnimation( ANCIENT_ANIMATION_TEMPLATE_NAME );
					break;

				case ChooseShipController.State.Enforcer:
					PlayAnimation( ENFORCER_ANIMATION_TEMPLATE_NAME );
					break;

				case ChooseShipController.State.Smuggler:
					PlayAnimation( SMUGGLER_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}