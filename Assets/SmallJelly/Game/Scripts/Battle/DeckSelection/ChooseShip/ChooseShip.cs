﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChooseShip : SJMonoBehaviour
	{
		#region Exposed To Inspector
		[SerializeField]
		private ChooseShipController mChooseShipController;
		#endregion

		#region Public Methods
		public void OnInsertState( ChooseShipController.State state )
		{
			mChooseShipController.OnInsertState( state );
		}
		#endregion
	}
}