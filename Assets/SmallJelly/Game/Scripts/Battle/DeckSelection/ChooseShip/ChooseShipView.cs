﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChooseShipView : SJMonoBehaviour
	{
		#region Exposed To Inspector
		[SerializeField]
		private ChooseShipAnimationController mChooseShipAnimationController;
		#endregion

		#region Public Methods
		public void OnPlayAnimation( ChooseShipController.State state )
		{
			mChooseShipAnimationController.OnPlayAnimation( state );
		}
		#endregion
	}
}