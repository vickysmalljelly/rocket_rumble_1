﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ChooseShipController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,
			Initialisation,
			Enforcer,
			Smuggler,
			Ancient
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private ChooseShipView mChooseShipView;
		#endregion

		#region Member Variables
		private State mState;
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			UpdateState( state );
		}
		#endregion

		#region Protected Methods
		protected void UpdateState( State state )
		{
			//If the state has changed
			if( mState != state )
			{
				mState = state;
				mChooseShipView.OnPlayAnimation( state );
			}
		}
		#endregion
	}
}