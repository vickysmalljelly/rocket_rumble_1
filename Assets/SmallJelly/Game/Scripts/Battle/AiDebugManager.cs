﻿using System;
using System.Collections.Generic;
using System.IO;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Singleton that exists for the lifetime of a battle.
    /// </summary>
    public class AiDebugManager : MonoBehaviourSingleton<AiDebugManager>
    {
        private string mFilename;

        public void Initialise(RRChallenge challenge)
        {
            mFilename = FileLocations.AiDebugFile;

            if(GameManager.Get.DebugConfig.AiDebug) 
            {
                challenge.BattleUpdated += ChallengeBattleUpdated;
            }
        }

        private void ChallengeBattleUpdated(SJJson aiHistoryHson)
        {
            SJJson aiHistory = aiHistoryHson.GetObject("aiHistory");

            if(aiHistory != null) {

                WriteToFile(aiHistory);
                
                Debug.Log("AI --------------->");

                List<SJJson> turnList = aiHistory.GetSJJsonList("turns");

                Debug.Log("Num AI turns = " + turnList.Count);

                SJJson mostRecent = turnList[turnList.Count - 1];
                Debug.Log(mostRecent.JSON);

                int turnNumber = mostRecent.GetInt("turnNum").GetValueOrDefault();

                List<SJJson> moves = mostRecent.GetSJJsonList("moves");
                Debug.Log("TURN NUM: " + turnNumber + " NUM MOVES: " + moves.Count);

                foreach(SJJson move in moves)
                {                    
                    List<SJJson> possibleMoves = move.GetSJJsonList("possibleMoves");
                    Debug.Log("TURN NUM: " + turnNumber + " MOVE: " + move.GetInt("moveNum").GetValueOrDefault() + " NUM POSSIBILITIES = " + possibleMoves.Count);
                    foreach(SJJson possibleMove in possibleMoves)
                    {
                        Debug.Log(possibleMove.JSON);
                    }
                }
            }
        }

        private void WriteToFile(SJJson aiHistory)
        {
            if(File.Exists(mFilename))
            {
                File.Delete(mFilename);
            }

            try 
            {
                System.IO.File.WriteAllText(mFilename, JsonUtils.FormatJson(aiHistory.JSON));
            }
            catch(Exception ex)
            {
                SJLogger.LogError("Failed to write ai to file {0}", ex.Message);
            }

        }
    }
}

