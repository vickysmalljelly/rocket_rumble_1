﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class LocalPlayerBattleView : PlayerBattleView
	{
		#region Public Event Handlers
		public event EventHandler< RenderButtonEventArgs > EndTurnButtonClicked
		{
			add
			{
				((LocalPlayerHUD)PlayerHUD).EndTurnButtonClicked += value;
			}

			remove
			{
				((LocalPlayerHUD)PlayerHUD).EndTurnButtonClicked -= value;
			}
		}

		public event EventHandler< BattleEntityEventArgs > JettisonButtonClicked
		{
			add
			{
				((LocalPlayerHUD)PlayerHUD).JettisonButtonClicked += value;
			}

			remove
			{
				((LocalPlayerHUD)PlayerHUD).JettisonButtonClicked -= value;
			}
		}

		public event Action ExitButtonClicked
		{
			add
			{
				((LocalPlayerHUD)PlayerHUD).ExitButtonClicked += value;
			}

			remove
			{
				((LocalPlayerHUD)PlayerHUD).ExitButtonClicked -= value;
			}
		}
		#endregion

		#region Public Methods
		public override void OnConstruct( BattleConfig battleConfig )
		{
			PlayerBattleSceneView.OnConstruct( battleConfig, PlayerType.Local );
		}

		public void OnRefreshConnectionProblems( bool isExperiencingProblems )
		{
			( ( LocalPlayerHUD ) PlayerHUD ).OnRefreshConnectionProblems( isExperiencingProblems );
		}
		#endregion

		#region Protected Methods
		protected override void TempLoadHUD( Transform hudParent )
		{
			string path = "Prefabs/HUD/PlayerHUD";

			GameObject localHudResource = Resources.Load<GameObject>(path);
			GameObject localHudGameObject = Instantiate( localHudResource );
			SJRenderTransformExtensions.AddChild( hudParent, localHudGameObject.transform, true, true, true );

			PlayerHUD = localHudGameObject.GetComponent<PlayerHUD>();
			SJLogger.AssertCondition(PlayerHUD != null, "PlayerHudView is not found");
		}
		#endregion
	}
}