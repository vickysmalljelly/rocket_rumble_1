using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Displays player data during a battle.
	/// </summary>
	public abstract class PlayerBattleView : UIView<PlayerBattleData>  
	{
		#region Public Event Handlers
		public event EventHandler<BattleEntityAndStateTransitionEventArgs> BattleEntityStateUpdated
		{
			add
			{
				mPlayerBattleSceneView.BattleEntityStateUpdated += value;
			}

			remove
			{
				mPlayerBattleSceneView.BattleEntityStateUpdated -= value;
			}
		}


		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> BattleEntityTargetableStateUpdated
		{
			add
			{
				mPlayerBattleSceneView.BattleEntityTargetableStateUpdated += value;
			}

			remove
			{
				mPlayerBattleSceneView.BattleEntityTargetableStateUpdated -= value;
			}
		}

		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> BattleEntitySelectionStateUpdated
		{
			add
			{
				mPlayerBattleSceneView.BattleEntitySelectionStateUpdated += value;
			}

			remove
			{
				mPlayerBattleSceneView.BattleEntitySelectionStateUpdated -= value;
			}
		}
		#endregion

		#region Protected Properties
		public PlayerHUD PlayerHUD
		{
			get;
			protected set;
		}

		public PlayerBattleSceneView PlayerBattleSceneView
		{
			get
			{
				return mPlayerBattleSceneView;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private PlayerBattleSceneView mPlayerBattleSceneView;

		[SerializeField]
		private Transform mHUD;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();
			
			SJLogger.AssertCondition(mPlayerBattleSceneView != null, "mPlayerCardView Component has not been set in the inspector");
			SJLogger.AssertCondition(mHUD != null, "mHUD has not been set in the inspector");

			TempLoadHUD( mHUD );
		}

		private void OnEnable()
		{
			BattleEntityStateUpdated += HandleBattleEntityStateUpdated;
			BattleEntityTargetableStateUpdated += HandleBattleEntityTargetableStateUpdated;
			BattleEntitySelectionStateUpdated += HandleBattleEntitySelectionStateUpdated;
		}

		private void OnDisable()
		{
			BattleEntityStateUpdated -= HandleBattleEntityStateUpdated;
			BattleEntityTargetableStateUpdated -= HandleBattleEntityTargetableStateUpdated;
			BattleEntitySelectionStateUpdated -= HandleBattleEntitySelectionStateUpdated;
		}
		#endregion

		#region Abstract Methods
		public abstract void OnConstruct( BattleConfig battleConfig );
		#endregion

		#region Public Methods
		public void OnStartTurn( PlayerType newPlayersTurn)
        {
			mPlayerBattleSceneView.OnStartTurn( newPlayersTurn );
            PlayerHUD.OnStartTurn( newPlayersTurn );
        }

		public void OnTurnNearlyOver( int secondsRemaining )
		{
			PlayerHUD.OnTurnNearlyOver( secondsRemaining );
		}

		public void OnEndTurn( PlayerType playerType )
		{
			PlayerHUD.OnEndTurn( playerType);
		}

		public void OnRefreshTargets( PlayerTargetingBattleData playerTargetingBattleData )
		{
			mPlayerBattleSceneView.UpdateTargetData( playerTargetingBattleData.BattleEntityTargetingData );
		}

		public void OnHardReset( PlayerBattleData newBattleData )
		{
			mPlayerBattleSceneView.OnHardReset( newBattleData );
			PlayerHUD.OnRefresh( newBattleData );
		}

		public void OnRollback()
		{
			mPlayerBattleSceneView.OnRollback();
		}
		#endregion

		#region Protected Methods
		protected abstract void TempLoadHUD( Transform hudParent );

		protected override void OnRefreshView( PlayerBattleData playerBattleData )
		{
			mPlayerBattleSceneView.OnRefresh( playerBattleData );
		
			PlayerHUD.OnRefresh( playerBattleData );
		}
		#endregion

		#region Event Handlers
		private void HandleBattleEntityStateUpdated( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs ) 
		{
			PlayerHUD.OnBattleEntityStateUpdated( battleEntityAndStateTransitionEventArgs.BattleEntity, battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState );
		}

		private void HandleBattleEntityTargetableStateUpdated( object o, BattleEntityAndTargetableStateTransitionEventArgs battleEntityAndTargetableStateTransitionEventArgs ) 
		{
			PlayerHUD.OnBattleEntityTargetableStateUpdated( battleEntityAndTargetableStateTransitionEventArgs.BattleEntity, battleEntityAndTargetableStateTransitionEventArgs.State, battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState );
		}

		private void HandleBattleEntitySelectionStateUpdated( object o, BattleEntityAndSelectionStateTransitionEventArgs battleEntityAndSelectionStateTransitionEventArgs ) 
		{
			PlayerHUD.OnBattleEntitySelectionStateUpdated( battleEntityAndSelectionStateTransitionEventArgs.BattleEntity, battleEntityAndSelectionStateTransitionEventArgs.PreviousState, battleEntityAndSelectionStateTransitionEventArgs.NewState );
		}
		#endregion
	}
}
