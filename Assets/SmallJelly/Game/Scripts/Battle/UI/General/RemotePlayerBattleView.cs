﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class RemotePlayerBattleView : PlayerBattleView
	{
		#region Public Methods
		//Called when the object is created - just before OnEnable - used to construct the object with the
		//supplied configuration
		public override void OnConstruct( BattleConfig battleConfig )
		{
			PlayerBattleSceneView.OnConstruct( battleConfig, PlayerType.Remote );
		}
		#endregion

		#region Protected Methods
		protected override void TempLoadHUD( Transform hudParent )
		{
			string path = "Prefabs/HUD/OpponentHUD";

			GameObject remoteHudResource = Resources.Load<GameObject>(path);
			GameObject remoteHudGameObject = Instantiate( remoteHudResource );

			SJRenderTransformExtensions.AddChild( hudParent, remoteHudGameObject.transform, true, true, true );

			PlayerHUD = remoteHudGameObject.GetComponent<PlayerHUD>();
			SJLogger.AssertCondition(PlayerHUD != null, "PlayerHudView is not found");
		}
		#endregion
	}
}