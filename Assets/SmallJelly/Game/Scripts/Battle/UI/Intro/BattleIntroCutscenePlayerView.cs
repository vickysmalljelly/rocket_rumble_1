﻿
using SmallJelly.Framework;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

namespace SmallJelly
{
	public class BattleIntroCutscenePlayerView : SJMonoBehaviour
	{
		#region Exposed Variables
		[SerializeField]
		private Transform mShipParent; 

		[SerializeField]
		private BattleIntroPlayerStatsView mBattleIntroPlayerStatsView; 
		#endregion

		#region Public Methods
		public void OnConstruct( BattleConfig.PlayerBattleConfig playerBattleConfig, PlayerType playerType )
		{
			Ship ship = ShipFactory.ConstructShip( playerType, playerBattleConfig.ShipClass );
			SJRenderTransformExtensions.AddChild( mShipParent, ship.transform, true, true, true );

			mBattleIntroPlayerStatsView.OnConstruct( playerBattleConfig, playerType );
		}
		#endregion
	}
}