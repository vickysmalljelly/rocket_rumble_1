﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class BattleIntroCutsceneController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,

			Initialisation,
			Playing
		}
		#endregion

		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mBattleIntroView.AnimationFinished += value;
			}

			remove
			{
				mBattleIntroView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private BattleIntroCutsceneView mBattleIntroView;
		#endregion

		#region Public Methods
		public void OnConstruct( BattleConfig battleConfig )
		{
            SJLogger.AssertCondition(battleConfig != null, "Must have a battleConfig");
			mBattleIntroView.OnConstruct( battleConfig );
		}

		public void OnInsertState( State state )
		{
			mBattleIntroView.OnPlayAnimation( state );
		}
		#endregion
	}
}