﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class BattleIntroCutsceneAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Intro/"; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string INTRO_ANIMATION_TEMPLATE_NAME = "IntroAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( BattleIntroCutsceneController.State state )
		{
			StopExistingAnimations();

			switch(state)
			{
			case BattleIntroCutsceneController.State.Initialisation:
				PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
				break;
			case BattleIntroCutsceneController.State.Playing:
				PlayAnimation( INTRO_ANIMATION_TEMPLATE_NAME );
				break;
			}
		}
		#endregion
	}
}
