﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class BattleIntroController : MenuController
	{
		#region Public Properties
		public BattleCoinController BattleCoinController
		{
			get
			{
				return mBattleCoinController;
			}
		}

		public BattleIntroCutsceneController BattleIntroCutsceneController
		{
			get
			{
				return mBattleIntroCutsceneController;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private BattleCoinController mBattleCoinController;


		[SerializeField]
		private BattleIntroCutsceneController mBattleIntroCutsceneController;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			//Enable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = true;
		}

		private void OnDisable()
		{
			//Disable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = false;
		}
		#endregion
	}
}