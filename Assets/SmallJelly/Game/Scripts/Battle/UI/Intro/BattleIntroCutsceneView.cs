﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class BattleIntroCutsceneView : MenuController
	{
		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mBattleIntroAnimationController.AnimationFinished += value;
			}

			remove
			{
				mBattleIntroAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private BattleIntroCutscenePlayerView mLocalBattleIntroPlayer;

		[SerializeField]
		private BattleIntroCutscenePlayerView mRemoteBattleIntroPlayer;

		[SerializeField]
		private BattleIntroCutsceneAnimationController mBattleIntroAnimationController;
		#endregion

		#region Public Methods
		public void OnConstruct( BattleConfig battleConfig )
		{
			BattleConfig.PlayerBattleConfig localPlayerBattleConfig = ChallengeManager.Get.LocalPlayer.UserId == battleConfig.Players[0].PlayerId ? battleConfig.Players[0] : battleConfig.Players[1];
			BattleConfig.PlayerBattleConfig remotePlayerBattleConfig = ChallengeManager.Get.RemotePlayerId == battleConfig.Players[0].PlayerId ? battleConfig.Players[0] : battleConfig.Players[1];
			mLocalBattleIntroPlayer.OnConstruct( localPlayerBattleConfig, PlayerType.Local );
			mRemoteBattleIntroPlayer.OnConstruct( remotePlayerBattleConfig, PlayerType.Remote );

		}

		public void OnPlayAnimation( BattleIntroCutsceneController.State state )
		{
			mBattleIntroAnimationController.UpdateAnimations( state );
		}
		#endregion
	}
}