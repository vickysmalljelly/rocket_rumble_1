﻿
using SmallJelly.Framework;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.IO;

namespace SmallJelly
{
	public class BattleIntroPlayerStatsView : SJMonoBehaviour
	{
		#region Exposed Variables
		[SerializeField]
		private TextMeshProUGUI mUsername; 

		[SerializeField]
		private TextMeshProUGUI mClass; 

		[SerializeField]
		private Image mCharacterImage; 
		#endregion

		#region Public Methods
		public void OnConstruct( BattleConfig.PlayerBattleConfig playerBattleConfig, PlayerType playerType )
		{
			mUsername.text = playerType == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.DisplayName : ChallengeManager.Get.RemotePlayerName;
			mClass.text = playerBattleConfig.ShipClass;
			mCharacterImage.sprite = GetSprite( playerType, playerBattleConfig.ShipClass );
		}
		#endregion

		#region Private Methods
		private static Sprite GetSprite( PlayerType playerType, string shipClass )
		{
			return Resources.Load< Sprite >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyCharacterImages, Path.DirectorySeparatorChar, GetPortraitName( playerType, shipClass ) ) );
		}

		private static string GetPortraitName( PlayerType playerType, string shipClass )
		{
			//If the game is a tutorial and the player is the remote player then we should use the autopilot image
			if( playerType == PlayerType.Remote )
			{
				if( BattleManager.Get.IsScriptedTutorial() )
				{
					return "ui_portrait_Autopilot";
				}
			}

			switch( shipClass )
			{
				case ShipClass.Ancient: 
					return "ui_portrait_Wyvox";

				case ShipClass.Enforcer: 
					return "ui_portrait_Joyforce";

				case ShipClass.Smuggler: 
					return "ui_portrait_Darnelle";

				case ShipClass.Debug: 
					return "ui_portrait_Autopilot";

				default:
					SJLogger.LogError("Ship class {0} not recognised", shipClass);
					return "ui_portrait_Autopilot";
			}

		}
		#endregion
	}
}