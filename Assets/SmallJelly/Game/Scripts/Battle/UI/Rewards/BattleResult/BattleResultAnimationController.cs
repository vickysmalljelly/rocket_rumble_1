﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;


namespace SmallJelly
{
	public class BattleResultAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/BattleResult/"; } }
		#endregion

		#region Constants
		private const string INITIALIZATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string DISPLAY_RESULT_ANIMATION_TEMPLATE_NAME = "DisplayResultAnimation";
		private const string DISPLAY_CHANGE_STARS_ANIMATION_TEMPLATE_NAME = "DisplayChangeStarsAnimation";
		private const string DISPLAY_SHIFT_TIER_ANIMATION_TEMPLATE_NAME = "DisplayShiftTierAnimation";
		private const string DISPLAY_REWARD_ANIMATION_TEMPLATE_NAME = "DisplayRewardAnimation";

		#endregion

		#region Public Methods
		public void UpdateAnimations( BattleResultController.State state, AnimationMetaData animationMetaData)
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch( state )
			{
				case BattleResultController.State.Initialisation:
					PlayAnimation( INITIALIZATION_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleResultController.State.DisplayResult:
					PlayAnimation( DISPLAY_RESULT_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleResultController.State.DisplayChangeStars:
				case BattleResultController.State.DisplayChangeLevelUpStars:
					PlayAnimation( DISPLAY_CHANGE_STARS_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleResultController.State.DisplayShiftTier:
					PlayAnimation( DISPLAY_SHIFT_TIER_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleResultController.State.DisplayReward:
					PlayAnimation( DISPLAY_REWARD_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}