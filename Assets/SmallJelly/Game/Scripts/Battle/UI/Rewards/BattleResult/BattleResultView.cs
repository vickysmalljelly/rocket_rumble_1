﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using TMPro;

namespace SmallJelly
{
	public class BattleResultView : SJMonoBehaviour
	{
		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mBattleResultAnimationController.AnimationFinished += value;
			}

			remove
			{
				mBattleResultAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private TextMeshProUGUI CurrentRankText;

		[SerializeField]
		private TextMeshProUGUI NewRankText;

		[SerializeField]
		private Image CurrentRankInsignia;

		[SerializeField]
		private Image NewRankInsignia;

		[SerializeField]
		private BattleResultAnimationController mBattleResultAnimationController;
		#endregion

		#region Public Methods
		public void Refresh( RewardData rewardData )
		{
			CurrentRankText.text = string.Format( "{0} {1}", rewardData.Rank.PreviousTierName, rewardData.Rank.PreviousRankName );
			NewRankText.text = string.Format( "{0} {1}", rewardData.Rank.NewTierName, rewardData.Rank.NewRankName );

			//CurrentRankInsignia.sprite = Resources.Load< Sprite >( rewardData.Rank.PreviousRankNumber );
			//NewRankInsignia.sprite = Resources.Load< Sprite >( rewardData.Rank.NewRankNumber );
		}

		public void OnInsertState( BattleResultController.State state, AnimationMetaData animationMetaData )
		{
			mBattleResultAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion
	}
}