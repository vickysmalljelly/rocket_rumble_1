﻿using System;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	public class BattleResult : MenuController
	{
		#region Public Events
		public event EventHandler FlowFinished
		{
			add
			{
				mBattleResultController.FlowFinished += value;
			}

			remove
			{
				mBattleResultController.FlowFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private BattleResultController mBattleResultController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{}

		private void OnEnable()
		{
			OnInsertState( BattleResultController.State.Initialisation );

			//Enable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = true;
		}

		private void OnDisable()
		{
			//Disable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = false;
		}
		#endregion

		#region Public Methods
		public void Refresh( bool won, RewardData rewardData )
		{
			mBattleResultController.Refresh( won, rewardData );
		}

		public void OnInsertState( BattleResultController.State state )
		{
			//Send the new state to the view controller - this allows it to play any relevant animations
			mBattleResultController.OnInsertState( state );
		}
		#endregion

	}
}