﻿
using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	/// <summary>
	/// Controls the Rewards state
	/// </summary>
	public class BattleResultController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,

			Initialisation,
			DisplayResult,
			DisplayChangeStars,
			DisplayChangeLevelUpStars,
			DisplayShiftTier,
			DisplayReward
		}
		#endregion

		#region Constants
		private const string DID_WIN_KEY = "DidWin";
		private const string PREVIOUS_STARS_KEY = "PreviousStars";
		private const string PREVIOUS_RANK_KEY = "PreviousRank";
		private const string NEW_RANK_KEY = "NewRank";
		private const string NEW_STARS_KEY = "NewStars";
		private const string STARS_AT_THIS_RANK_KEY = "StarsAtThisRank";
		private const string TOTAL_CREDITS_KEY = "TotalCredits";
		private const string REWARD_CREDITS_KEY = "RewardCredits";
		#endregion

		#region Public Events
		public event EventHandler FlowFinished;
		public event EventHandler AnimationFinished;
		#endregion

		#region Public Properties
		public BattleResultView BattleResultView
		{
			get
			{
				return mBattleResultView;
			}

			set
			{
				mBattleResultView = value;
			}
		}
		#endregion

		#region Member Variables
		private bool mDidWin;
		private State mState;
		private RewardData mRewardData;

		[SerializeField]
		private BattleResultView mBattleResultView;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		public void OnEnable()
		{
			RegisterListeners();
		}

		public void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			//mBattleResultView.OnInsertState( state );
			OnInsertState( state, new AnimationMetaData( new System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			//mBattleResultView.OnInsertState( state );
			mBattleResultView.OnInsertState( state, animationMetaData );
			mState = state;
		}

		public void Refresh( bool hasWon, RewardData rewardData )
		{
			mDidWin = hasWon;
			mRewardData = rewardData;

			OnInsertState( State.DisplayResult, GetResultMetaData() );
			mBattleResultView.Refresh( rewardData );
		}
		#endregion

		#region Private Methods
		private bool IsRankingUp()
		{
			return mRewardData.Rank.PreviousRankNumber < mRewardData.Rank.NewRankNumber;
		}

		private bool IsRankingDown()
		{
			return mRewardData.Rank.PreviousRankNumber > mRewardData.Rank.NewRankNumber;
		}

		private void OnAnimationsFinished()
		{
			System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable> dictionary = new System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable>();

			switch( mState )
			{
				case State.DisplayResult:
					if( mRewardData.Rank.StarsAtPreviousRank != -1 )
					{
						dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtPreviousRank ) );
					}
					else
					{
						dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );
					}

					if( IsRankingUp() )
					{
						dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( mRewardData.Rank.PreviousNumStars ) );
						
						if( mRewardData.Rank.StarsAtPreviousRank != -1 )
						{
							dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.StarsAtPreviousRank ) );
						}
						else
						{
							dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );
						}


					}
					else if( IsRankingDown() )
					{
						dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( mRewardData.Rank.PreviousNumStars ) );
						dictionary.Add( NEW_STARS_KEY, new FsmInt( 0 ) );
					}
					else
					{
						dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( mRewardData.Rank.PreviousNumStars ) );
						dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.NewNumStars ) );
					}

					OnInsertState( State.DisplayChangeStars, new AnimationMetaData( dictionary ) );
					break;

				case State.DisplayChangeStars:
					if( IsRankingUp() || IsRankingDown() )
					{
						dictionary.Add( "MovedUp", new FsmBool( true ) );

						OnInsertState( State.DisplayShiftTier, new AnimationMetaData( dictionary ) );
					}
					else
					{
						dictionary.Add( TOTAL_CREDITS_KEY, new FsmInt( (int)( GameManager.Get.GetCreditsCount() - mRewardData.GetTotalCredits() ) ) );
						dictionary.Add( REWARD_CREDITS_KEY, new FsmInt( mRewardData.GetTotalCredits() ) );
						OnInsertState(	State.DisplayReward, new AnimationMetaData( dictionary ) );
					}
					break;

				case State.DisplayShiftTier:
					dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );

					if( IsRankingUp() )
					{
						dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( 0 ) );
						dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.NewNumStars ) );
					}
					else if( IsRankingDown() )
					{
						dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );
						dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.NewNumStars ) );
					}

					OnInsertState( State.DisplayChangeLevelUpStars, new AnimationMetaData( dictionary ) );
					break;

				case State.DisplayChangeLevelUpStars:
					dictionary.Add( TOTAL_CREDITS_KEY, new FsmInt( (int)( GameManager.Get.GetCreditsCount() - mRewardData.GetTotalCredits() ) ) );
					dictionary.Add( REWARD_CREDITS_KEY, new FsmInt( mRewardData.GetTotalCredits() ) );

					dictionary.Add( NEW_STARS_KEY, new FsmInt( mRewardData.Rank.NewNumStars ) );
					dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );

					OnInsertState(	State.DisplayReward, new AnimationMetaData( dictionary ) );
					break;

				case State.DisplayReward:
					FireFlowFinished();
					break;
			}
		}

		private AnimationMetaData GetResultMetaData()
		{
			System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable> dictionary = new System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable>();
			dictionary.Add( DID_WIN_KEY, new FsmBool( mDidWin ) );
			dictionary.Add( PREVIOUS_STARS_KEY, new FsmInt( mRewardData.Rank.PreviousNumStars ) );

			if( mRewardData.Rank.StarsAtPreviousRank != -1 )
			{
				dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtPreviousRank ) );
			}
			else
			{
				dictionary.Add( STARS_AT_THIS_RANK_KEY, new FsmInt( mRewardData.Rank.StarsAtThisRank ) );
			}

			dictionary.Add( PREVIOUS_RANK_KEY, new FsmInt( mRewardData.Rank.PreviousRankNumber ) );
			dictionary.Add( NEW_RANK_KEY, new FsmInt( mRewardData.Rank.NewRankNumber ) );

			return new AnimationMetaData( dictionary );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mBattleResultView.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mBattleResultView.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Evnet Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			OnAnimationsFinished();
			FireAnimationFinished();
		}
		#endregion

		#region Event Firing
		private void FireFlowFinished()
		{
			if( FlowFinished != null )
			{
				FlowFinished( this, EventArgs.Empty );
			}
		}

		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}