﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class BattleCoinView : SJMonoBehaviour
	{
		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mBattleCoinAnimationController.AnimationFinished += value;
			}

			remove
			{
				mBattleCoinAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private BattleCoinAnimationController mBattleCoinAnimationController;
		#endregion

		#region Public Methods
		public void OnPlayAnimation( BattleCoinController.State state )
		{
			mBattleCoinAnimationController.UpdateAnimations( state );
		}
		#endregion
	}
}