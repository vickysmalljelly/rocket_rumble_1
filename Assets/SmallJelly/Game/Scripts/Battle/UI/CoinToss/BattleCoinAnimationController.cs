﻿using UnityEngine;
using System.Collections;


namespace SmallJelly
{
	public class BattleCoinAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return FileLocations.SmallJellyCoinTossPlayMakerTemplates; } }
		#endregion

		#region Public Methods
		public void UpdateAnimations( BattleCoinController.State coinTossState )
		{
			StopExistingAnimations();

			switch( coinTossState )
			{
				case BattleCoinController.State.FirstTurn:
					PlayAnimation( "FirstAnimation" );
					break;

				case BattleCoinController.State.SecondTurn:
					PlayAnimation( "SecondAnimation" );
					break;
			}
		}
		#endregion
	}
}