﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class BattleCoinController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,

			FirstTurn,
			SecondTurn
		}
		#endregion

		#region Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mBattleCoinView.AnimationFinished += value;
			}

			remove
			{
				mBattleCoinView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public BattleCoinView BattleCoinView
		{
			get
			{
				return mBattleCoinView;
			}

			set
			{
				mBattleCoinView = value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private BattleCoinView mBattleCoinView;
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			mBattleCoinView.OnPlayAnimation( state );
		}
		#endregion
	}
}