﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class PlayerHUDController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,

			Initialisation,
			Showing,
			Hiding,

			//Used for the tutorial
			MoveInTop,
			MoveInEndTurn,
			MoveInJettison
		}

		public enum TimerState
		{
			None,
			Hidden,
			Counting
		}
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mPlayerHUDView.AnimationFinished += value;
			}

			remove
			{
				mPlayerHUDView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private State mState;

		[SerializeField]
		private TimerState mTimerState;


		[SerializeField]
		private PlayerType mPlayerType;

		[SerializeField]
		private PlayerHUDView mPlayerHUDView;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshViewData( PlayerBattleData data )
		{
			mPlayerHUDView.OnRefreshData( mPlayerType, data );
		}

		private void OnAnimationFinished()
		{
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			SJLogger.AssertCondition( mState != State.Hiding, "State is hiding, trying to insert {0} which is erroneous ", state );
			if( mState != state )
			{
				mPlayerHUDView.OnPlayAnimation( mPlayerType, state, animationMetaData );

				mState = state;
			}
		}

		public void OnInsertTimerState( TimerState state )
		{
			OnInsertTimerState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertTimerState( TimerState state, AnimationMetaData animationMetaData )
		{
			if( mTimerState != state )
			{
				mPlayerHUDView.OnPlayTimerAnimation( mPlayerType, state, animationMetaData );

				mTimerState = state;
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			OnAnimationFinished();
		}
		#endregion
	}
}