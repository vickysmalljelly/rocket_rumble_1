﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;
using UnityEngine.UI;
using System;

namespace SmallJelly
{
	public class PlayerHUDView : SJMonoBehaviour
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mAnimationController.AnimationFinished += value;
			}

			remove
			{
				mAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Constants
		private const float HP_INDICATOR_START_POINT = 0.05f;
		private const float HP_INDICATOR_END_POINT = 0.95f;

		private const float POWER_ACTIVE_ALPHA = 1.0f;
		private const float POWER_INACTIVE_ALPHA = 0.5f;
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private PlayerHUDAnimationController mAnimationController;

		[SerializeField]
		private PlayerHUDTimerAnimationController mTimerAnimationController;


		[SerializeField]
		private TextMeshProUGUI mNameText;

		[SerializeField]
		private TextMeshProUGUI mPowerText;

		[SerializeField]
		private TextMeshProUGUI mHullHPText;

		[SerializeField]
		private Text mCardsRemainingInDeckText;

		[SerializeField]
		private Image mHPImage;

		[SerializeField]
		private Image[] mPowerIcons;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(mPowerText != null, "mPowerText Text has not been set in the inspector");
			SJLogger.AssertCondition(mHullHPText != null, "mHullHPText Text has not been set in the inspector");

		}
		#endregion

		#region Public Methods
		public void OnRefreshData( PlayerType playerType, PlayerBattleData data )
		{
            mNameText.text = playerType == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.DisplayName : ChallengeManager.Get.RemotePlayerName;

			mHullHPText.text =  data.ShipData.HullHp.ToString();

			float healthPercentage = (float)data.ShipData.HullHp / (float)data.ShipData.MaxHullHp;
			mHPImage.fillAmount = Mathf.Lerp( HP_INDICATOR_START_POINT, HP_INDICATOR_END_POINT, healthPercentage );

			mPowerText.text = data.Power.ToString();

			for( int i = 0; i < mPowerIcons.Length; i++ )
			{
				mPowerIcons[ i ].gameObject.SetActive( i < data.PowerThisTurn );

				Color previousColor = mPowerIcons[ i ].color;
				float alpha = i < data.Power ? POWER_ACTIVE_ALPHA : POWER_INACTIVE_ALPHA;
				mPowerIcons[ i ].color = new Color( previousColor.r, previousColor.g, previousColor.b, alpha );
			}

			mCardsRemainingInDeckText.text = data.NumCardsInDeck.ToString();
		}

		public void OnPlayAnimation( PlayerType playerType, PlayerHUDController.State state, AnimationMetaData animationMetaData )
		{
			mAnimationController.UpdateAnimations( playerType, state, animationMetaData );
		}

		public void OnPlayTimerAnimation( PlayerType playerType, PlayerHUDController.TimerState state, AnimationMetaData animationMetaData )
		{
			mTimerAnimationController.UpdateAnimations( playerType, state, animationMetaData );
		}
		#endregion
	}
}