﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class PlayerHUDAnimationController : AnimationController
	{
		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string MOVE_IN_ANIMATION_TEMPLATE_NAME = "MoveInAnimation";
		private const string MOVE_OUT_ANIMATION_TEMPLATE_NAME = "MoveOutAnimation";
		private const string MOVE_IN_TOP_ANIMATION_TEMPLATE_NAME = "MoveInTopAnimation";
		private const string MOVE_IN_END_TURN_ANIMATION_TEMPLATE_NAME = "MoveInEndTurnAnimation";
		private const string MOVE_IN_JETTISON_ANIMATION_TEMPLATE_NAME = "MoveInJettisonAnimation";

		private const string LOCAL_ANIMATION_SUBFOLDER = "Local";
		private const string REMOTE_ANIMATION_SUBFOLDER = "Remote";
		#endregion

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/HUD/"; } }
		#endregion

		#region Methods
		public void UpdateAnimations( PlayerType playerType, PlayerHUDController.State state, AnimationMetaData animationMetaData )
		{
			if( playerType == PlayerType.Local )
			{
				//Play the animations specifically for the local player
				PlayLocalAnimations( state, animationMetaData );
			}
			else if( playerType == PlayerType.Remote )
			{
				//Play the animations specifically for the remote player
				PlayRemoteAnimations( state, animationMetaData );
			}
		}
		#endregion

		#region Private Methods
		//Play the animations specifically for the local player
		private void PlayLocalAnimations(PlayerHUDController.State state, AnimationMetaData animationMetaData )
		{
			switch(state)
			{
				case PlayerHUDController.State.Initialisation:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.Showing:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, MOVE_IN_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.MoveInTop:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, MOVE_IN_TOP_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.MoveInEndTurn:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, MOVE_IN_END_TURN_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.MoveInJettison:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, MOVE_IN_JETTISON_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.Hiding:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, MOVE_OUT_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}

		//Play the animations specifically for the remote player
		private void PlayRemoteAnimations(PlayerHUDController.State state, AnimationMetaData animationMetaData )
		{
			switch(state)
			{
				case PlayerHUDController.State.Initialisation:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.Showing:
				case PlayerHUDController.State.MoveInTop:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, MOVE_IN_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.State.Hiding:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, MOVE_OUT_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}