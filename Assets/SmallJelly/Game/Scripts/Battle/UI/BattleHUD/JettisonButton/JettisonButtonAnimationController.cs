﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class JettisonButtonAnimationController : RenderButtonAnimationController
	{

		#region Constants
		private const string mEnablingTemplateName = "";
		private const string mGreyedOutTemplateName = "";
		private const string mDisablingTemplateName = "";
		private const string mPressingTemplateName = "";
		private const string mReleasingTemplateName = "";
		#endregion

		#region Methods
		public override void UpdateAnimations( RenderButtonController.ButtonState buttonState )
		{
			base.UpdateAnimations( buttonState );

			switch(buttonState)
			{
			case RenderButtonController.ButtonState.Showing:
				PlayAnimation( mEnablingTemplateName );
				break;

			case RenderButtonController.ButtonState.GreyedOut:
				PlayAnimation( mGreyedOutTemplateName );
				break;

			case RenderButtonController.ButtonState.Hiding:
				PlayAnimation( mDisablingTemplateName );
				break;

			case RenderButtonController.ButtonState.Pressing:
				PlayAnimation( mPressingTemplateName );
				break;

			case RenderButtonController.ButtonState.Releasing:
				PlayAnimation( mReleasingTemplateName );
				break;
			}
		}
		#endregion
	}
}
