﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class PlayerHUDTimerAnimationController : AnimationController
	{
		#region Constants
		private const string COUNTING_ANIMATION_TEMPLATE_NAME = "CountingAnimation";
		private const string HIDDEN_ANIMATION_TEMPLATE_NAME = "HiddenAnimation";

		private const string LOCAL_ANIMATION_SUBFOLDER = "Local";
		private const string REMOTE_ANIMATION_SUBFOLDER = "Remote";
		#endregion

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/HUD/Timer/"; } }
		#endregion

		#region Public Methods
		public void UpdateAnimations( PlayerType playerType, PlayerHUDController.TimerState state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			if( playerType == PlayerType.Local )
			{
				//Play the animations specifically for the local player
				PlayLocalAnimations( state );
			}
			else if( playerType == PlayerType.Remote )
			{
				//Play the animations specifically for the remote player
				PlayRemoteAnimations( state );
			}
		}
		#endregion

		#region Private Methods
		private void PlayLocalAnimations( PlayerHUDController.TimerState state )
		{
			switch(state)
			{
				case PlayerHUDController.TimerState.Counting:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, COUNTING_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.TimerState.Hidden:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, HIDDEN_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}

		private void PlayRemoteAnimations( PlayerHUDController.TimerState state )
		{
			switch(state)
			{
				case PlayerHUDController.TimerState.Counting:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, COUNTING_ANIMATION_TEMPLATE_NAME );
					break;

				case PlayerHUDController.TimerState.Hidden:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, HIDDEN_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}