﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Displays the remote player hud during a battle.
    /// </summary>
    public class RemotePlayerHUD : PlayerHUD
    {        
		#region Public Methods
		public override void OnStartTurn( PlayerType playerType )
		{
			if( playerType == PlayerType.Remote )
			{
				OnInsertTimerState( PlayerHUDController.TimerState.Hidden );
			}
		}

		public override void OnEndTurn( PlayerType playerType )
		{
			if( playerType == PlayerType.Remote )
			{
				OnInsertTimerState( PlayerHUDController.TimerState.Hidden );
			}		
		}

		public override void OnBattleEntityStateUpdated( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState ){}
		public override void OnBattleEntityTargetableStateUpdated( BattleEntity battleEntity, BattleEntityController.State state, BattleEntityController.TargetableState previousTargetableStateState, BattleEntityController.TargetableState newTargetableStateState ){}
		public override void OnBattleEntitySelectionStateUpdated( BattleEntity battleEntity, BattleEntityController.SelectionState previousSelectionState, BattleEntityController.SelectionState newSelectionState ){}
		#endregion

    }
}