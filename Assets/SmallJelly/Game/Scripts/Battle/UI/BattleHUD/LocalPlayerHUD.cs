﻿using UnityEngine;
using SmallJelly.Framework;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
    /// <summary>
    /// Displays player data during a battle.
    /// </summary>
    public class LocalPlayerHUD : PlayerHUD
    {   
		#region Public Events
		public event EventHandler< RenderButtonEventArgs > EndTurnButtonClicked
		{
			add
			{
				mEndTurnButton.Clicked += value;
			}

			remove
			{
				mEndTurnButton.Clicked -= value;
			}
		}

		public event Action ExitButtonClicked;

		public event EventHandler< BattleEntityEventArgs > JettisonButtonClicked;
		#endregion

		#region Public Properties
		public RenderButton JettisonButton
		{
			get
			{
				return mJettisonButton;
			}
		}

		public RenderButton EndTurnButton
		{
			get
			{
				return mEndTurnButton;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private Button mConnectionLostButton1;

		[SerializeField]
		private Button mConnectionLostButton2;

		[SerializeField]
		private RenderButton mJettisonButton;

		[SerializeField]
		private RenderButton mEndTurnButton;

		[SerializeField]
		private Button mExitButton;

		[SerializeField]
		private GameObject mSelectTargetTip;

		[SerializeField]
		private GameObject mMyTurnTip;
		#endregion

		#region Member Variables
		private BattleEntity mSelectedBattleEntity;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		protected override void Start()
		{
			base.Start();

			DisableEndTurn();

			//Disable the jettison button by default
			DisableJettison();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshConnectionProblems( bool isExperiencingProblems )
		{
			mConnectionLostButton1.gameObject.SetActive( isExperiencingProblems );
			mConnectionLostButton2.gameObject.SetActive( isExperiencingProblems );
		}

		public override void OnStartTurn( PlayerType playerType )
		{
			DisableJettison();

			mMyTurnTip.SetActive( playerType == PlayerType.Local );

			if( playerType == PlayerType.Local )
			{
				OnInsertTimerState( PlayerHUDController.TimerState.Hidden );
			}
		}

		public override void OnEndTurn( PlayerType playerType )
		{
			if( playerType == PlayerType.Local )
			{
				OnInsertTimerState( PlayerHUDController.TimerState.Hidden );
			}
		}

		public override void OnBattleEntityTargetableStateUpdated( BattleEntity battleEntity, BattleEntityController.State state, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState )
		{
			//Don't show the HUD for the card dragging!
			if( state == BattleEntityController.State.CardDragging )
			{
				return;
			}
				
			if( newState == TargetableLogicController.TargetableState.Targeting )
			{
				mSelectTargetTip.SetActive( true );
			}
			else if( previousState == TargetableLogicController.TargetableState.Targeting )
			{
				mSelectTargetTip.SetActive( false );
			}
		}

		public override void OnBattleEntityStateUpdated( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState )
		{
		}

		public override void OnBattleEntitySelectionStateUpdated( BattleEntity battleEntity, BattleEntityController.SelectionState previousSelectionState, BattleEntityController.SelectionState newSelectionState )
		{
			//If an entity is no longer selected then disable the jettison
			if( newSelectionState == BattleEntityController.SelectionState.NotSelected )
			{
				DisableJettison( battleEntity );
			}

			//If an entity is selected
			if( newSelectionState == BattleEntityController.SelectionState.Selected )
			{
				//Deselect the existing entity
				if( mSelectedBattleEntity )
				{
					//Entity is no longer selected
					mSelectedBattleEntity.OnInsertSelectionState(BattleEntityController.SelectionState.NotSelected );
				}

				//Select the new entity
				EnableJettison( battleEntity );
			}
		}
		#endregion

		#region Private Methods
		private void EnableJettison( BattleEntity selectedEntity )
		{
			//We have selected the new entity
			mSelectedBattleEntity = selectedEntity;
			mJettisonButton.OnInsertState( RenderButtonController.ButtonState.Showing );
		}

		private void DisableJettison( BattleEntity selectedBattleEntity )
		{
			//If existing battle entity is selected then let's deselect it!
			//Don't allow another battle entity to cancel a different battle entities jettison
			if (selectedBattleEntity == mSelectedBattleEntity) 
			{
				DisableJettison ();
			}
		}


		private void DisableJettison()
		{
			mSelectedBattleEntity = null;
			mJettisonButton.OnInsertState (RenderButtonController.ButtonState.Hiding);
		}


		private void EnableEndTurn()
		{
			mEndTurnButton.OnInsertState(RenderButtonController.ButtonState.Showing);
		}

		private void DisableEndTurn()
		{
			mEndTurnButton.OnInsertState(RenderButtonController.ButtonState.Hiding);
		}
		#endregion

		#region Event Firing
		private void FireJettisonButtonClicked()
		{
			SJLogger.AssertCondition( mSelectedBattleEntity != null, "Selected entity should not be null" );

			if( JettisonButtonClicked != null )
			{
				JettisonButtonClicked( this, new BattleEntityEventArgs( mSelectedBattleEntity ) );
			}

			//Jettison has been clicked - now we can disable it!
			DisableJettison();

		}

		private void FireExitButtonClicked()
		{
			if( ExitButtonClicked != null )
			{
				ExitButtonClicked();
			}
		}
		#endregion

		#region Event Handlers
		private void HandleJettisonButtonClicked( object o, EventArgs args )
		{
			FireJettisonButtonClicked();
		}

		private void HandleConnectionIssuesButtonClicked()
		{
			ButtonData buttonData = new ButtonData( "Ok", HandleConnectionIssuesDialogDismissed );
			DialogManager.Get.ShowMessagePopup( "Connection Issues", "We're sorry you are having connection issues. If the problem continues, please email support@smalljelly.com with your location and user name.", buttonData );
		}

		private void HandleConnectionIssuesDialogDismissed()
		{
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mJettisonButton.Clicked += HandleJettisonButtonClicked;

			mConnectionLostButton1.onClick.AddListener( HandleConnectionIssuesButtonClicked );
			mConnectionLostButton2.onClick.AddListener( HandleConnectionIssuesButtonClicked );

			mExitButton.onClick.AddListener( FireExitButtonClicked );
		}

		private void UnregisterListeners()
		{			
			mJettisonButton.Clicked -= HandleJettisonButtonClicked;

			mConnectionLostButton1.onClick.RemoveListener( HandleConnectionIssuesButtonClicked );
			mConnectionLostButton2.onClick.RemoveListener( HandleConnectionIssuesButtonClicked );

			mExitButton.onClick.RemoveListener( FireExitButtonClicked );
		}
		#endregion

    }
}
