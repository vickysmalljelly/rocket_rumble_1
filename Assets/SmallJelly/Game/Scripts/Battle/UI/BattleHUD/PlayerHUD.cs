﻿using System;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
    /// <summary>
    /// Displays player hud during a battle.
    /// </summary>
    public abstract class PlayerHUD : UIView< PlayerBattleData >  
    {        
		#region Constants
		private const float HP_INDICATOR_START_POINT = 0.05f;
		private const float HP_INDICATOR_END_POINT = 0.95f;

		private const float POWER_ACTIVE_ALPHA = 1.0f;
		private const float POWER_INACTIVE_ALPHA = 0.5f;
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mPlayerHUDController.AnimationFinished += value;
			}

			remove
			{
				mPlayerHUDController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private PlayerHUDController mPlayerHUDController;

		[SerializeField]
		private Canvas mCanvas;
		#endregion

        #region Unity Methods
        protected override void Awake()
        {
            base.Awake();

			OnInsertState( PlayerHUDController.State.Initialisation );
        }

		protected virtual void Start()
		{
			RefreshCanvasConfig();
		}
        #endregion

		#region Public Methods
		public void OnInsertState( PlayerHUDController.State state )
		{
			mPlayerHUDController.OnInsertState( state );
		}

		public void OnInsertTimerState( PlayerHUDController.TimerState timerState )
		{
			mPlayerHUDController.OnInsertTimerState( timerState );
		}

		public void OnInsertTimerState( PlayerHUDController.TimerState timerState, AnimationMetaData animationMetaData )
		{
			mPlayerHUDController.OnInsertTimerState( timerState, animationMetaData );
		}
			
		public void OnTurnNearlyOver( int secondsRemaining )
		{
			Dictionary< string, NamedVariable > metaData = new Dictionary< string, NamedVariable >();

			FsmFloat fsmFloat =  new FsmFloat();
			fsmFloat.Value = secondsRemaining;

			metaData.Add( "TurnTimerTime", fsmFloat );
			AnimationMetaData animationMetaData = new AnimationMetaData( metaData );
			OnInsertTimerState( PlayerHUDController.TimerState.Counting, animationMetaData );
		}

		public abstract void OnStartTurn( PlayerType playerType );
		public abstract void OnEndTurn( PlayerType playerType );

		public abstract void OnBattleEntityStateUpdated( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState );
		public abstract void OnBattleEntityTargetableStateUpdated( BattleEntity battleEntity, BattleEntityController.State state, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState );
		public abstract void OnBattleEntitySelectionStateUpdated( BattleEntity battleEntity, BattleEntityController.SelectionState previousSelectionState, BattleEntityController.SelectionState newSelectionState );
		#endregion

        #region Protected Methods
		protected override void OnRefreshView( PlayerBattleData data )
		{
			mPlayerHUDController.OnRefreshViewData( data );
        }
        #endregion

		#region Private Methods
		private void RefreshCanvasConfig()
		{
			//Refresh canvas camera
			mCanvas.worldCamera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
		}
		#endregion
    }
}
