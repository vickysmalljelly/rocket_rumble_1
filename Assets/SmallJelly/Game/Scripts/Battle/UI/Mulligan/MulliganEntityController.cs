using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class MulliganEntityController : TargetableLogicController
	{
		#region Enums
		public enum State
		{
			None,

			Initialisation,
			ServerProcessing,
			OptionsIdle,
			FinalisedIdle
		}
		#endregion

		#region Public Properties
		public MulliganEntityView View 
		{ 
			private get; 
			set; 
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private State mState;
		#endregion

		#region Mouse Controls
		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame ){}


		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}


		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			switch( CurrentSelectionState )
			{
				case SelectionState.Selected:
					DeselectCard();
					break;

				case SelectionState.NotSelected:
					SelectCard();
					break;
			}
		}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame ){}
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		#endregion

		#region Touch Controls
		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMousePressedOver( elementsHit, movementDuringFrame );
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary< string, NamedVariable >() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mState = state;
			View.OnPlayAnimation( state, animationMetaData );
		}

		public override void OnInsertSelectionState( SelectionState selectionState )
		{
			OnInsertSelectionState( selectionState, new AnimationMetaData( new Dictionary< string, NamedVariable >() )  );
		}

		public void OnInsertSelectionState( SelectionState selectionState, AnimationMetaData animationMetaData )
		{
			base.OnInsertSelectionState( selectionState );

			View.OnPlaySelectionAnimation( selectionState, animationMetaData );
		}

		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			View.OnRefreshData( battleEntityData );
		}
		#endregion

		#region Private Methods
		private void SelectCard()
		{
			SJLogger.AssertCondition( CurrentSelectionState != SelectionState.Selected, "Selection State should not already be selected" );

			//We can only select cards in the idle options state
			if( mState != State.OptionsIdle )
			{
				return;
			}

			OnInsertSelectionState( SelectionState.Selected );
		}

		private void DeselectCard()
		{
			SJLogger.AssertCondition( CurrentSelectionState != SelectionState.NotSelected, "Selection State should not already be deselected" );

			//We can only deselect cards in the idle options or finalised state
			if( mState != State.OptionsIdle && mState != State.FinalisedIdle )
			{
				return;
			}
			OnInsertSelectionState( SelectionState.NotSelected );
		}
		#endregion
	}
}
