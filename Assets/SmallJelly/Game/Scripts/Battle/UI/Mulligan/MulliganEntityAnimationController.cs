﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class MulliganEntityAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  FileLocations.SmallJellyMulliganCardsPlayMakerTemplates; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( MulliganEntityController.State mulliganCardState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			switch( mulliganCardState )
			{
				case MulliganEntityController.State.Initialisation:
					PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
