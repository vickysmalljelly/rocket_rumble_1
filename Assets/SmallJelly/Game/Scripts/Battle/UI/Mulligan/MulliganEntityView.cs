﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class MulliganEntityView : SJMonoBehaviour 
	{
		#region Constants
		private const int MAXIMUM_TEXT_LENGTH = 20;
		#endregion

		#region Public Event Handlers
		public event EventHandler AnimationFinished;
		#endregion

		#region Public Properties
		public TemplateBattleEntityModel Template
		{ 
			get;
			set;
		}

		public TemplateBattleEntityParts TemplateBattleEntityParts
		{ 
			get;
			set;
		}

		public CardParts Parts
		{
			get
			{
				return Template.CardParts;
			}
		}

		public MulliganEntityAnimationController AnimationController 
		{ 
			get;
			private set;
		}

		public MulliganEntitySelectionAnimationController SelectionAnimationController 
		{ 
			get;
			private set;
		}
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			AnimationController = GetComponent< MulliganEntityAnimationController >();
			SelectionAnimationController = GetComponent< MulliganEntitySelectionAnimationController >();
		}

		private void OnDestroy()
		{
		}

		protected void OnEnable()
		{
			RegisterAnimationListeners( AnimationController );
		}

		protected void OnDisable()
		{
			UnregisterAnimationListeners( AnimationController );
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			Template.OnRefresh( battleEntityData );

			Parts.OnRefresh( battleEntityData );
				
			EnableParts();
		}

		public void OnPlayAnimation( MulliganEntityController.State state, AnimationMetaData animationMetaData )
		{
			AnimationController.UpdateAnimations( state, animationMetaData );
		}

		public void OnPlaySelectionAnimation( BattleEntityController.SelectionState state, AnimationMetaData animationMetaData )
		{
			SelectionAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion

		#region Protected Mehtods
		protected void SetActiveRecursively(GameObject rootObject, bool active)
		{
			rootObject.SetActive(active);

			foreach (Transform childTransform in rootObject.transform)
			{
				SetActiveRecursively(childTransform.gameObject, active);
			}
		}
		#endregion

		#region Private Methods
		private void EnableParts()
		{
			SetActiveRecursively( Parts.Card, true );
			SetActiveRecursively( Parts.Component, true );
			SetActiveRecursively( Parts.ComponentHousing, true );
			SetActiveRecursively( Parts.Mask, true );
			SetActiveRecursively( Parts.BasePlate, false );
		}

		private void RefreshComponents( )
		{
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterAnimationListeners( MulliganEntityAnimationController mulliganEntityAnimationController )
		{
			mulliganEntityAnimationController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterAnimationListeners( MulliganEntityAnimationController mulliganEntityAnimationController )
		{
			mulliganEntityAnimationController.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}
		#endregion
	}
}