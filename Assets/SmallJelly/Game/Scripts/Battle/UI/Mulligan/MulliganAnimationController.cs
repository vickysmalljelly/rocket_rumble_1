﻿using UnityEngine;
using System.Collections;


namespace SmallJelly
{
	public class MulliganAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return string.Empty; } }
		#endregion

		#region Public Methods
		public void UpdateAnimations( MulliganController.State state )
		{
		}
		#endregion
	}
}