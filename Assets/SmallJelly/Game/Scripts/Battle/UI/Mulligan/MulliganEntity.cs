using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class MulliganEntity : Targetable
	{
		#region Event Handlers
		public event EventHandler< MulliganEntityAndSelectionStateTransitionEventArgs > SelectionStateTransitioned;
		#endregion

		#region Public Properties
		public ObjectInputListener ObjectInputListener { get; set; }

		public MulliganEntityController MulliganEntityController
		{
			get
			{
				return (MulliganEntityController)TargetableLogicController;
			}

			set
			{
				TargetableLogicController = value;
			}
		}
		#endregion

		#region Member Variables
		private BattleEntityData mData;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			MulliganEntityController.OnRefreshData( battleEntityData );
		}


		public void OnInsertState( MulliganEntityController.State battleEntityState )
		{
			MulliganEntityController.OnInsertState( battleEntityState );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			MulliganEntityController.SelectionStateUpdated += HandleSelectionStateUpdated;
		}

		private void UnregisterListeners()
		{
			MulliganEntityController.SelectionStateUpdated -= HandleSelectionStateUpdated;
		}

		private void RegisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver += HandleMouseRolledOver;
			ObjectInputListener.MouseHovered += HandleMouseHovered;
			ObjectInputListener.MouseRolledOff += HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver += HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged += HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff += HandleMouseDraggedOff;

			ObjectInputListener.MousePressed += HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver += HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff += HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver += HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged += HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff += HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed += HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver += HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff += HandleTouchReleasedOff;
		}

		private void UnregisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver -= HandleMouseRolledOver;
			ObjectInputListener.MouseHovered -= HandleMouseHovered;
			ObjectInputListener.MouseRolledOff -= HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver -= HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged -= HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff -= HandleMouseDraggedOff;

			ObjectInputListener.MousePressed -= HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver -= HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff -= HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver -= HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged -= HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff -= HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed -= HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver -= HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff -= HandleTouchReleasedOff;
		}

		#endregion

		#region Event Firing
		private void FireSelectionStateTransitioned( MulliganEntity mulliganEntity, MulliganEntityController.SelectionState previousState, MulliganEntityController.SelectionState newState, SJRenderRaycastHit[] hitLayers )
		{
			if( SelectionStateTransitioned != null )
			{
				SelectionStateTransitioned( this, new MulliganEntityAndSelectionStateTransitionEventArgs( mulliganEntity, previousState, newState ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleSelectionStateUpdated( object o, SelectionStateTransitionEventArgs selectionStateTransitionEventArgs )
		{
			FireSelectionStateTransitioned( this, selectionStateTransitionEventArgs.PreviousState, selectionStateTransitionEventArgs.NewState, selectionStateTransitionEventArgs.EntityHitLayers );
		}
		#endregion
	}
}