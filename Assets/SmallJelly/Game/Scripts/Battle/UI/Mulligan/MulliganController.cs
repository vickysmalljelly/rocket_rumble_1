﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Controls the Mulligans state
	/// </summary>
	public class MulliganController : SJMonoBehaviour
	{
		#region Enums
		public enum State
		{
			None,

			ServerProcessing,
			OptionsIdle,
			FinalisedIdle
		}
		#endregion

		#region Public Events
		public event EventHandler CompleteClicked;
		#endregion

		#region Public Properties
		public MulliganView MulliganView
		{
			get
			{
				return mMulliganView;
			}

			set
			{
				mMulliganView = value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private MulliganView mMulliganView;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			mMulliganView.OnInsertState( state );
		}

		public void OnCompleteClicked()
		{
			FireCompleteClicked();
		}
		#endregion

		#region Event Firing
		private void FireCompleteClicked()
		{
			if( CompleteClicked != null )
			{
				CompleteClicked( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}