﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Controls the Mulligans ownership of the Mulligan Entities
	/// </summary>
	public class Mulligan : SJMonoBehaviour
	{
		//TODO - Consider intergrating this class into MulliganController?
		#region Public Event Handlers
		public event EventHandler CompleteClicked
		{
			add
			{
				mMulliganController.CompleteClicked += value;
			}

			remove
			{
				mMulliganController.CompleteClicked -= value;
			}
		}
		#endregion

		#region Public Properties
		public List< bool > MulliganEntitiesSelected
		{
			get
			{
				return mMulliganEntitiesSelected;
			}
		}

		public List< MulliganEntity > MulliganEntities
		{
			get
			{
				return mMulliganEntities;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private MulliganController mMulliganController;

		[SerializeField]
		private Vector3 mCardsCentrePosition;

		[SerializeField]
		private Vector3 mCardsColumnOffset;


		[SerializeField]
		private Vector3 mTicksCentrePosition;

		[SerializeField]
		private Vector3 mTicksColumnOffset;


		[SerializeField]
		private Toggle[] mTickBoxes;
		#endregion
	
		#region Private Variables
		private List< MulliganEntity > mMulliganEntities;
		private List< bool > mMulliganEntitiesSelected;

		private ObjectDistributor mCardDistributor;
		private ObjectDistributor mTickDistributor;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			mMulliganEntities = new List<MulliganEntity>();
			mMulliganEntitiesSelected = new List<bool>();

			Camera targetCamera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			mCardDistributor = new ObjectDistributor( new FaceGameObjectLineDistributionShape( transform.position, targetCamera.transform.position, mCardsCentrePosition, mCardsColumnOffset ), 0.0f );
			mTickDistributor = new ObjectDistributor( new LineDistributionShape( mTicksCentrePosition, mTicksColumnOffset ), 0.0f );
		}

		private void OnEnable()
		{
			//Enable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = true;
		}

		private void OnDisable()
		{
			//Disable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = false;
		}
		#endregion

		#region Public Methods
		public void Refresh( BattleEntityData[] battleEntityDataArray )
		{
			UnregisterListeners( mMulliganEntities );

			//Add mulligan entities in the data to the list
			int i = 0;
			while( mMulliganEntities.Count < battleEntityDataArray.Length )
			{
				MulliganEntity mulliganEntity = MulliganEntityFactory.ConstructMulliganEntity( battleEntityDataArray[ i ] );

				//Make entity a child of this
				SJRenderTransformExtensions.AddChild( transform, mulliganEntity.transform, true, true, true );

				mCardDistributor.Insert( new int[]{ i }, new Transform[]{ mulliganEntity.transform } );
				mTickDistributor.Insert( new int[]{ i }, new Transform[]{ mTickBoxes[i].transform } );

				mMulliganEntities.Add( mulliganEntity );
				MulliganEntitiesSelected.Add( false );

				//TODO - We're not even sure if we're going to use tick boxes yet but if we do then we should come up with
				//a generic toggleable system (possibly intergrated with the object distributor) rather than setting them
				//manually
				mTickBoxes[ i ].gameObject.SetActive( true );

				i++;
			}
				
			//Remove mulligan entities not in the data from the list
			while( mMulliganEntities.Count > battleEntityDataArray.Length )
			{
				Destroy( mMulliganEntities[ 0 ].gameObject );
				mMulliganEntities.RemoveAt( 0 );
				MulliganEntitiesSelected.RemoveAt( 0 );
			}

			RegisterListeners( mMulliganEntities );
			
			//Assert the above conditions were completed successfully
			SJLogger.AssertCondition( mMulliganEntities.Count == battleEntityDataArray.Length, "The amount of supplied data must be equal to the number of entities" );

			for( int j = 0; j < mMulliganEntities.Count; j++ )
			{
				mMulliganEntities[ j ].OnRefreshData( battleEntityDataArray[ j ] );
				mMulliganEntities[ j ].OnInsertState( MulliganEntityController.State.Initialisation );

			}
		}

		public void OnInsertState( MulliganController.State mulliganState )
		{
			//Send the new state to the view controller - this allows it to play any relevant animations
			mMulliganController.OnInsertState( mulliganState );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( List< MulliganEntity > mulliganEntities )
		{
			foreach( MulliganEntity mulliganEntity in mulliganEntities )
			{
				mulliganEntity.SelectionStateTransitioned += HandleSelectionStateTransitioned;
			}
		}

		private void UnregisterListeners( List< MulliganEntity > mulliganEntities )
		{
			foreach( MulliganEntity mulliganEntity in mulliganEntities )
			{
				mulliganEntity.SelectionStateTransitioned -= HandleSelectionStateTransitioned;
			}
		}
		#endregion

		#region Event Handlers
		private void HandleSelectionStateTransitioned( object o, MulliganEntityAndSelectionStateTransitionEventArgs mulliganEntityAndSelectionStateTransitionEventArgs )
		{
			int indexOfEntity = mMulliganEntities.IndexOf( mulliganEntityAndSelectionStateTransitionEventArgs.MulliganEntity );

			bool selected = mulliganEntityAndSelectionStateTransitionEventArgs.NewState == TargetableLogicController.SelectionState.Selected;
			mMulliganEntitiesSelected[ indexOfEntity ] = selected;

			mTickBoxes[ indexOfEntity ].isOn = selected;
		}
		#endregion
	}
}