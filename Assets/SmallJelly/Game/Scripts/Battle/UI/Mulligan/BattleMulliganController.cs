﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace SmallJelly
{
	public class BattleMulliganController : MenuController
	{
		#region Public Event Handlers
		public event EventHandler CompleteClicked
		{
			add
			{
				mMulligan.CompleteClicked += value;
			}

			remove
			{
				mMulligan.CompleteClicked -= value;
			}
		}
		#endregion

		#region Public Properties
		public List< bool > MulliganEntitiesSelected
		{
			get
			{
				return mMulligan.MulliganEntitiesSelected;
			}
		}

		public List< MulliganEntity > MulliganEntities
		{
			get
			{
				return mMulligan.MulliganEntities;
			}
		}

		public Mulligan Mulligan
		{
			get
			{
				return mMulligan;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private Mulligan mMulligan;

		[SerializeField]
		private Canvas mCanvas;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			RefreshCanvasConfig();
		}
		#endregion

		#region Public Methods
		public void Refresh( BattleEntityData[] data )
		{
			mMulligan.Refresh( data );
		}
		#endregion

		#region Private Methods
		private void RefreshCanvasConfig()
		{
			//Refresh canvas camera
			mCanvas.worldCamera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
		}
		#endregion
	}
}