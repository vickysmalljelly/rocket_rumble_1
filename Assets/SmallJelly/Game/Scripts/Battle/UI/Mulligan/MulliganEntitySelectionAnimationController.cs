﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class MulliganEntitySelectionAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Mulligan/Cards/"; } }
		#endregion

		#region Constants
		private const string SELECTED_ANIMATION_TEMPLATE_NAME = "SelectedAnimation";
		private const string DESELECTED_ANIMATION_TEMPLATE_NAME = "DeselectedAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( MulliganEntityController.SelectionState mulliganSelectionState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			switch( mulliganSelectionState )
			{
				case TargetableLogicController.SelectionState.Selected:
					PlayAnimation( SELECTED_ANIMATION_TEMPLATE_NAME );
					break;

				case TargetableLogicController.SelectionState.NotSelected:
					PlayAnimation( DESELECTED_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
