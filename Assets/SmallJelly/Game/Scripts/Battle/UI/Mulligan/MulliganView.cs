﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using UnityEngine.UI;

namespace SmallJelly
{
	public class MulliganView : SJMonoBehaviour
	{
		#region Constants
		private const string OPTIONS_IDLE_TITLE_TEXT = "KEEP OR DEAL AGAIN?";
		private const string WAITING_FOR_OPPONENT_TITLE_TEXT = "WAITING FOR OPPONENT";
		#endregion

		#region Member Variables
		[SerializeField]
		private MulliganAnimationController mMulliganAnimationController;

		[SerializeField]
		private Text mTitle;

		[SerializeField]
		private GameObject mButton;
		#endregion

		#region Public Methods
		public void OnInsertState( MulliganController.State state )
		{
			mMulliganAnimationController.UpdateAnimations( state );

			UpdateTitle( state );
			UpdateButtons( state );
		}
		#endregion


		#region Private Methods
		private void UpdateTitle( MulliganController.State state )
		{
			switch( state )
			{
				case MulliganController.State.OptionsIdle:
					mTitle.text = OPTIONS_IDLE_TITLE_TEXT;
					break;

				case MulliganController.State.FinalisedIdle:
					mTitle.text = WAITING_FOR_OPPONENT_TITLE_TEXT;
					break;
			}
		}

		private void UpdateButtons( MulliganController.State state )
		{
			//TODO - Take this out, I feel that this should be controlled by the playmaker graph but in the
			//interests of time, lets do this here for now. When we add playmaker graphs for the rest of the 
			//mulligan we can switch it out.
			switch( state )
			{
				case MulliganController.State.OptionsIdle:
					mButton.SetActive( true );
					break;

				default:
					mButton.SetActive( false );
					break;
			}
		}
		#endregion
	}
}