﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class MulliganEntityFactory : CardFactory
	{
		#region Public Methods
		public static MulliganEntity ConstructMulliganEntity( BattleEntityData battleEntityData )
		{
			GameObject view = LoadCardView( CardType.BattleEntity );
			view.name = string.Format( "Card{0}", battleEntityData.DrawNumber );

			view.gameObject.SetActive( false );

			AssignComponents (view, battleEntityData );

			MulliganEntity mulliganEntity = view.GetComponent<MulliganEntity> ();
			view.gameObject.SetActive( true );

			mulliganEntity.OnRefreshData( battleEntityData );

			return mulliganEntity;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, BattleEntityData battleEntityData )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the mulligan entity (the mulligan entity handles the views)

			AssignAnimationController( view );
			AssignView(view);
			AssignController( view );
			AssignMulliganEntityComponent( view, battleEntityData);
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< MulliganEntityAnimationController > ();
			view.AddComponent< MulliganEntitySelectionAnimationController > ();
		}

		private static void AssignView( GameObject view )
		{
			MulliganEntityView mulliganEntityView = view.AddComponent< MulliganEntityView > ();
			mulliganEntityView.Template = view.GetComponent< TemplateBattleEntityModel >();
			mulliganEntityView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();		}

		private static void AssignController( GameObject view )
		{
			MulliganEntityController mulliganEntityController = view.AddComponent< MulliganEntityController > ();
			mulliganEntityController.View = view.GetComponent< MulliganEntityView >();
		}

		private static void AssignMulliganEntityComponent( GameObject view, BattleEntityData battleEntityData )
		{
			//Add battle entity component
			MulliganEntity mulliganEntity = view.AddComponent< MulliganEntity >();
			mulliganEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			mulliganEntity.MulliganEntityController = view.GetComponent< MulliganEntityController >();
		}
		#endregion
	}
}
