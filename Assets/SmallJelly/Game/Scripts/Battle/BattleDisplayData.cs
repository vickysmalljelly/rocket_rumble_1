using System;
using System.Collections.Generic;

using SmallJelly.Framework;
using System.Linq;

using UnityEngine;



namespace SmallJelly
{
	//TODO - I want to revise this class as soon as possible - splitting it into a factory and data class, it's gone far
	//far beyond its original intention.

	/// <summary>
	/// Interprets the battle ScriptData from JSON into values that can be displayed in the UI.
	/// Serialisable so it can be displayed in the Inspector
	/// </summary>
	[Serializable]
	public sealed class BattleDisplayData 
	{
		#region Public Event Handlers
		public event Action< PlayerTargetingBattleData > LocalPlayerTargetableDataUpdated;
		public event Action< PlayerTargetingBattleData > RemotePlayerTargetableDataUpdated;
		#endregion

		#region Public Properties
		public List< BattleRunnerData > RunnerHistory
		{
			get;
			private set;
		}

		public PlayerBattleData LocalLastRunData
		{
			get;
			set;
		}


		public PlayerBattleData RemoteLastRunData
		{
			get;
			set;
		}
		#endregion
		public Dictionary<string, PlayerBattleData> mPlayers = new Dictionary<string, PlayerBattleData>();

//#if SJ_DEBUG
		/// <summary>
		/// For debug display in the Inspector only
		/// </summary>
		public string Json;
		public PlayerBattleData LocalPlayer;
		public PlayerBattleData RemotePlayer;
//#endif

		/// <summary>
		/// The server will send messages to add to this list.  
		/// The client should remove events once they have been processed.
		/// </summary>

		#region Member Variables
		//Any runners which have already been created and queued up
		private Queue< BattleRunner > mQueuedUpRunners = new Queue<BattleRunner>();

		//The queue of battle runner data. Each of these battle runner datas may be dequeued into one or multiple runners.
		private BidirectionalQueue< BattleRunnerData > mBattleRunnerData = new BidirectionalQueue< BattleRunnerData >();
		#endregion

		#region Constructor
		public BattleDisplayData()
		{
			RunnerHistory = new List< BattleRunnerData >();
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Gets the local or remote player data by id.
		/// </summary>
		public PlayerBattleData GetPlayerData(string playerId)
		{
			return mPlayers[playerId];
		}

		/// <summary>
		/// Update the local and remote player data using the ScriptData from the server.
		/// </summary>
		/// <param name="scriptData">Script data.</param>
		public void Update( SJJson scriptData )
		{
			SJLogger.AssertCondition( scriptData != null, "Supplied scriptData must not be null" );
			// WIP
			SJLogger.LogMessage(MessageFilter.Battle, "Updating BattleDisplayData {0}", scriptData.JSON);

			SJJson battleView = scriptData.GetObject( "battleView" );

			ProcessEvents( battleView.GetSJJsonList( "events" ) );
		}

		public bool HasNextBattleRunner()
		{
			return mBattleRunnerData.Count > 0 || mQueuedUpRunners.Count > 0;
		}

		public BattleRunner GetNextBattleRunner( PlayerBattleSceneView localPlayerBattleSceneView, PlayerBattleSceneView remotePlayerBattleSceneView )
		{
			SJLogger.AssertCondition( localPlayerBattleSceneView != null && remotePlayerBattleSceneView != null, "Supplied scene views must not be null" );

			SJLogger.AssertCondition( HasNextBattleRunner(), "At least one runner's data must be contained within mBattleRunnerData or the queued up runners" );

			//If there are no queued up runners, then deserialize the next queue from the supplied data
			if( mQueuedUpRunners.Count == 0 )
			{
				//Remove the from the front of the queue
				BattleRunnerData battleRunnerData = mBattleRunnerData.DequeueFront();

				mQueuedUpRunners = GetRunnerQueueFromJson( battleRunnerData, localPlayerBattleSceneView, remotePlayerBattleSceneView );
			}

			return mQueuedUpRunners.Dequeue();
		}
		#endregion

		#region Private Methods
		private BattleRunnerData GetDataFromJson( SJJson eventJson )
		{
			string id = eventJson.GetString( "EventType" );
			SJJson localAfterJson = eventJson.GetObject( "localAfter" );
			SJJson remoteAfterJson = eventJson.GetObject( "remoteAfter" );
			SJJson runnerJSON = eventJson.GetObject( "properties" );

			BattleRunnerData battleRunnerData = BattleRunnerDataFactory.GetBattleRunnerData( id, localAfterJson, remoteAfterJson, runnerJSON );           

			return battleRunnerData;
		}

		private Queue< BattleRunner > GetRunnerQueueFromJson( BattleRunnerData battleRunnerData, PlayerBattleSceneView localPlayerBattleSceneView, PlayerBattleSceneView remotePlayerBattleSceneView )
		{
            SJLogger.LogMessage(MessageFilter.Battle, "Running event id {0}, obj {1}", battleRunnerData.Id, battleRunnerData.LegacyRunnerJSON.JSON);

			BattleRunnerArgs args = new BattleRunnerArgs( localPlayerBattleSceneView, remotePlayerBattleSceneView, LocalLastRunData, RemoteLastRunData, battleRunnerData.LocalPlayerBattleData, battleRunnerData.RemotePlayerBattleData);

			#if TEST_CONNECTION_LOSS
			EmptyRunner emptyRunner = new EmptyRunner( args );
			return new Queue< BattleRunner > { new []{ emptyRunner } };
			#else

			//Get the new battle events
			Queue< BattleRunner > battleRunners = BattleRunnerFactory.GetBattleRunnerQueue( battleRunnerData, args );

			LocalLastRunData = battleRunnerData.LocalPlayerBattleData;
			RemoteLastRunData = battleRunnerData.RemotePlayerBattleData;

			return battleRunners;
			#endif
		}

		private void ProcessEvents( List<SJJson> events )
		{
			SJLogger.AssertCondition(events != null, "msg must contain events" );

			foreach(SJJson eventJson in events)
			{
				BattleRunnerData battleRunnerData = GetDataFromJson( eventJson );
				mBattleRunnerData.EnqueueRear( battleRunnerData );

				//Add the runner data to out history!
				RunnerHistory.Add( battleRunnerData );

				FireLocalPlayerTargetableDataUpdated( battleRunnerData.LocalPlayerTargetingBattleData );
				FireRemotePlayerTargetableDataUpdated( battleRunnerData.RemotePlayerTargetingBattleData );
            }
        }
		#endregion

		#region Event Firing
		private void FireLocalPlayerTargetableDataUpdated( PlayerTargetingBattleData localPlayerTargetingBattleData )
		{
			if( LocalPlayerTargetableDataUpdated != null )
			{
				LocalPlayerTargetableDataUpdated( localPlayerTargetingBattleData );
			}
		}

		private void FireRemotePlayerTargetableDataUpdated( PlayerTargetingBattleData remotePlayerTargetingBattleData )
		{
			if( RemotePlayerTargetableDataUpdated != null )
			{
				RemotePlayerTargetableDataUpdated( remotePlayerTargetingBattleData );
			}
		}
		#endregion
	}
}

