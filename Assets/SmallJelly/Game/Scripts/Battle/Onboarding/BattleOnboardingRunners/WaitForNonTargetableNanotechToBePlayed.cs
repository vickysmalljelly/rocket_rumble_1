﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForNonTargetableNanotechToBePlayed : HistoryPredicateOnboardingRunner
	{
		#region Member Variables
		private LocalPlayerBattleView mLocalPlayerBattleView;

		private Func< BattleEntity, bool > mCardsToHighlight;

		private bool mIndicatorArrowShown;
		#endregion

		#region Constructor
		public WaitForNonTargetableNanotechToBePlayed( Func< BattleEntity, bool > cardsToHighlight ) : base( ( Func< BattleRunnerData, bool > )( x  => x is NanotechPlayedData ) )
		{
			mCardsToHighlight = cardsToHighlight;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );		
		}

		public override void OnUpdate()
		{
			RefreshIndicatorArrow();

			base.OnUpdate();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			BattleOnboardingDialogManager.Get.HideIndicatorArrow();
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( stateConstructionData.StateId == LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE )
			{
				ModifyIdleStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
			else if( stateConstructionData.StateId == LocalPlayerTurnInteractionStateMachine.PLAYING_NON_TARGETING_CREW_CARD_INTERACTION_STATE )
			{
			}
			else
			{
				ModifyStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
		}
		#endregion

		#region Private Methods
		private void RefreshIndicatorArrow()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Where( mCardsToHighlight ).ToArray();
			if( battleEntities.Length > 0 && !mIndicatorArrowShown )
			{
				BattleOnboardingDialogManager.Get.ShowIndicatorArrow( battleEntities[ 0 ].transform, Vector3.zero, battleEntities[ 0 ].transform, new Vector3(0, 14f, 0f) );

				mIndicatorArrowShown = true;
			}
			else if( battleEntities.Length == 0 && mIndicatorArrowShown )
			{
				BattleOnboardingDialogManager.Get.HideIndicatorArrow();

				mIndicatorArrowShown = false;
			}
		}

		private void ModifyIdleStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow local player entities
			BattleEntityPredicateBattleFilter battleEntityPredicateBattleFilter = new BattleEntityPredicateBattleFilter( mCardsToHighlight );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( false, true );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, battleEntityPredicateBattleFilter, buttonBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, battleEntityInteractionFilterResult };
		}


		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow attacking the ship where the target is ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			//Not interested in opponent players slots
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter (false, false);

			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( false, false );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, shipOwnerBattleFilter, shipOwnerBattleFilter, socketOwnerBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, shipInteractionFilterResult, shipInteractionFilterResult, socketInteractionFilterResult, battleEntityInteractionFilterResult };

		}
		#endregion
	}
}