﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class HUDUpdateOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private PlayerHUDController.State mNewState;

		private bool mEnableLocalHud;
		private bool mEnableRemoteHud;
		#endregion

		#region Constructor
		public HUDUpdateOnboardingRunner( PlayerHUDController.State state, bool enableLocalHud, bool enableRemoteHud )
		{
			mNewState = state;

			mEnableLocalHud = enableLocalHud;
			mEnableRemoteHud = enableRemoteHud;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
			OnFinishRunning();
		}
		#endregion

		#region Public Methods
		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( mEnableLocalHud, mEnableRemoteHud );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( mNewState, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ hudBattleFilter, buttonBattleFilter, jettisonButtonBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult };

		}
		#endregion
	}
}