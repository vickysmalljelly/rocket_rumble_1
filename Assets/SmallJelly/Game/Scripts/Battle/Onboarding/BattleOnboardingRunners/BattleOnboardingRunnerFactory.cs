﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace SmallJelly
{
	public class BattleOnboardingRunnerFactory
	{
		#region Public Constants
		public const string TUTORIAL_1 = "Tutorial1";
		public const string TUTORIAL_2 = "Tutorial2";
		public const string TUTORIAL_3 = "Tutorial3";
		public const string TUTORIAL_4 = "Tutorial4";
		#endregion

		#region Public Methods
		public static Queue< BattleOnboardingRunner > GetBattleOnboardingRunnerQueue ( string battleId )
		{
			string id = battleId;

			switch (id) 
			{
			//Moved over to the system of just accepting battleRunnerData
			case TUTORIAL_1:
				return GetTutorialOne();

			case TUTORIAL_2:
				return GetTutorialTwo();

			case TUTORIAL_3:
				return GetTutorialThree();

			case TUTORIAL_4:
				return GetTutorialFour();

			default:
				SJLogger.LogMessage ( MessageFilter.George, "This battle ( {0} ) appears not to have a tutorial", battleId);
				return new Queue< BattleOnboardingRunner > ( );
			}
		}

		#endregion

		#region Private Methods	
		private static Queue< BattleOnboardingRunner > GetTutorialOne ()
		{
			//ARROW POINTING TO POWER
			//TOP LEFT HAND CORNER APPEARS
			//Before you've fired move the opponents health down

			Queue< BattleOnboardingRunner > tutorialOne = new Queue< BattleOnboardingRunner >();

			//TODO - I think these make more sense as individual playmaker graphs with modularised functionality
            tutorialOne.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_1, null));


			//LOCAL PLAYER TURN ONE
			tutorialOne.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			tutorialOne.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );
			tutorialOne.Enqueue( new DisableQuitButtonBattleOnboardingRunner() );

			tutorialOne.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInTop, true, true ) );
			tutorialOne.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInEndTurn, true, true ) );


			//Once the local players card has been drawn
			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );

			tutorialOne.Enqueue( new DisplayPopupAndHighlightCardAndLocalHudBattleOnboardingRunner( "Co-Bee", "That was a nasty hit, Captain - hull down to <color=#00ff00>4 HP</color>!", "", x => false, Vector3.zero, Vector3.zero, Vector3.zero, PlayerType.Local, new Vector3( 150, -100, 0 ), new Vector3( 0, 0, -135 ), new Vector3( 100, 100, 100 ) ) );


			//tutorialOne.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			//tutorialOne.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			//Wait a couple of seconds
			//tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 2.0f ) );

			//Display a sequence of popups with arrows facing certain battle entities

		

			//tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "That was a nasty hit, Captain - hull down to 4 HP!", "Ouch!" ) );
			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );


			tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "Looks like we have 2 cards left - <color=#F5EA58>Delta Cannon</color> and <color=#83C602>Unstable Load</color>!", "" ) );
			
			//tutorialOne.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInTop, true, true ) );
			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );

			
			tutorialOne.Enqueue( new DisplayPopupAndHighlightCardAndLocalHudBattleOnboardingRunner( "Co-Bee", "We only have enough <color=#00ffff>power</color> right now to install the <color=#F5EA58>Delta Cannon</color>. ", "", x => false, Vector3.zero, Vector3.zero, Vector3.zero, PlayerType.Local, new Vector3( 90, -220, 0 ), new Vector3( 0, 0, 225 ), new Vector3( 100, 100, 100 ) ) );

			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );


			tutorialOne.Enqueue( new ShowBannerBattleOnboardingRunner("Drag the <color=#F5EA58>Delta Cannon</color> onto the ship!", new Vector3( 0, 420, 0 ) ) );


			//Enable the cards and a specific socket
			tutorialOne.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 5 }, new Vector3(0.0f,0.25f,0.0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );

			tutorialOne.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			//Wait a couple of seconds
			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			//Display some more popups
			tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "It'll take a few moments for the <color=#F5EA58>Delta Cannon</color> to come online. If we even have a few moments...", "" ) );

			tutorialOne.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );

			//Wait for the next local players turn
			tutorialOne.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			tutorialOne.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

            // TODO: Can add more of these if we would like a more finegrained analysis
            // tutorialOne.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_1, "1"));

			//LOCAL PLAYER TURN TWO
			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );


			tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "Hull at <color=#00ff00>1 HP</color>! We're in real trouble here!", "" ) );

			//tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "<color=#F5EA58>Delta Cannon</color> online and ready to fire! Aim for their hull!", "Fire!!" ) );
			tutorialOne.Enqueue( new ShowBannerBattleOnboardingRunner("<color=#F5EA58>Delta Cannon</color> online and ready to fire! Aim for their hull!!", new Vector3( 0, 420, 0 ) ) );


			tutorialOne.Enqueue( new WaitForComponentToFireAtShip( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, Vector3.zero, new Vector3(-90,0,0), new Vector3( 2, 2, 2 ) ) );
			tutorialOne.Enqueue( new HideBannerBattleOnboardingRunner( ) );


			tutorialOne.Enqueue( new WaitForSecondsOnboardingRunner( 2.0f ) );

			tutorialOne.Enqueue( new DisplayPopupBattleOnboardingRunner( "Co-Bee", "Good shot! Enemy hull down to <color=#00ff00>4 HP</color>! We need to finish them off now, before they can fire back!", "" ) );

			tutorialOne.Enqueue( new ShowBannerBattleOnboardingRunner("We've only got one card left to play… Unleash the <color=#83C602>Unstable Load</color>!", new Vector3( 0, 420, 0 ) ) );

			tutorialOne.Enqueue( new WaitForNonTargetableNanotechToBePlayed( y => y != null && y.Data.EntityType == BattleEntityType.NonTargetingCrew && y.Data.State == BattleEntityState.InHandPlayable ) );
			tutorialOne.Enqueue( new HideBannerBattleOnboardingRunner( ) );

            tutorialOne.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_1, null));

			tutorialOne.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );

			return tutorialOne;
		}

		private static Queue< BattleOnboardingRunner > GetTutorialTwo ()
		{
			Queue< BattleOnboardingRunner > tutorialTwo = new Queue< BattleOnboardingRunner >();

            tutorialTwo.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_2, null));

			//LOCAL PLAYER TURN ONE
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			tutorialTwo.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );
			tutorialTwo.Enqueue( new DisableQuitButtonBattleOnboardingRunner() );

			tutorialTwo.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInTop, true, true ) );

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );

			//tutorialTwo.Enqueue( new DisplayPopupBattleOnboardingRunner( "Yikes!", "Now they're fighting back. They've got a <color=#FFDE29>Tiny Laser</color>!", "Okay" ) );

			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner("We have <color=#00ffff>1 POWER POINT</color> to use. Install the <color=#FF0F10>ALPHA BLASTER</color>!!", new Vector3( 0, 420, 0 ) ) );


			tutorialTwo.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 4 }, new Vector3(0.0f,0.25f,0.0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );

			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialTwo.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInEndTurn, true, true ) );

			tutorialTwo.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );

			//Wait for the next local players turn
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

            tutorialTwo.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_2, "1"));


			//LOCAL PLAYER TURN TWO
			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );

				
			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner("Destroy your enemy's weapons to keep the upper hand!", new Vector3( 0, 420, 0 ) ) );

			tutorialTwo.Enqueue( new WaitForComponentToFireAtComponent( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, x => x.SocketController.Component != null, new Vector3(0.0f, 0.0f, 0.0f), new Vector3(-90,0.0f,0.0f), new Vector3( 2, 2, 2 ) ) );


			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner( "Excellent work, Captain! Now install the <color=#BAF17C>PUNCH PACKER!</color>!", new Vector3( 0, 420, 0 ) ) );

			tutorialTwo.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 6 }, new Vector3(0.0f,0.25f,0.0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );


			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );
			
			tutorialTwo.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );


			//Wait for the next local players turn
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );



            tutorialTwo.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_2, "2"));


			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );

			//LOCAL PLAYER TURN THREE

			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner( "We now have <color=#00ffff>3 POWER POINTS</color>. Install the mighty <color=#D19EEC>STAR STINGER!</color>", new Vector3( 0, 420, 0 ) ) );


			tutorialTwo.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 8 }, new Vector3(0.0f,0.25f,0.0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );
			
			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );


			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner( "We have the upper hand! Concentrate fire on their hull!", new Vector3( 0, 420, 0 ) ) );

			tutorialTwo.Enqueue( new WaitForComponentToFireAtShip( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, Vector3.zero, new Vector3(-90,0,0), new Vector3( 2, 2, 2 ) ) );
			
			tutorialTwo.Enqueue( new WaitForComponentToFireAtShip( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, Vector3.zero, new Vector3(-90,0,0), new Vector3( 2, 2, 2 ) ) );

			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialTwo.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );

			//Wait for the next local players turn
			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );			

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 5f ) );
			
			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner("It seems our opponent was unaware that WEAPONS RETALIATE if not destroyed!", new Vector3( 0, 420, 0 ) ) );

			tutorialTwo.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 2.5f ) );
			
			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialTwo.Enqueue( new WaitForSecondsOnboardingRunner( 1f ) );

			tutorialTwo.Enqueue( new ShowBannerBattleOnboardingRunner( "We have sufficient firepower to finish them! FIRE AT WILL!", new Vector3( 0, 420, 0 ) ) );

			tutorialTwo.Enqueue( new WaitForShipToBeDestroyedOnboardingRunner( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, Vector3.zero, new Vector3(-90,0,0), new Vector3( 2, 2, 2 ) ) );

			tutorialTwo.Enqueue( new HideBannerBattleOnboardingRunner( ) );

            // TODO: This only signifies that we have reached the end of the instructions to the player.  Should actually wait for the battle to end
            tutorialTwo.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_2, null));

			tutorialTwo.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );

			return tutorialTwo;
		}

		private static Queue< BattleOnboardingRunner > GetTutorialThree ()
		{
			Queue< BattleOnboardingRunner > tutorialThree = new Queue< BattleOnboardingRunner >();

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 0.0f ) );

            tutorialThree.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_3, null));

			//LOCAL PLAYER TURN ONE
			tutorialThree.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );
			tutorialThree.Enqueue( new DisableQuitButtonBattleOnboardingRunner() );

			tutorialThree.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInTop, true, true ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "The enemy drone got the drop on us, Captain! They get the first move!", new Vector3( 0, 420, 0 ) ) );


			tutorialThree.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 3f ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );
			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Hold your finger over a component or card to see what it does.", new Vector3( 0, 420, 0 ) ) );

			tutorialThree.Enqueue( new WaitForCardToBeHighlightedBattleOnboardingRunner( x => x, 1.0f, new Vector3(0.0f, 4.0f, 4.0f), Vector3.zero, new Vector3( 2, 2, 2 ) ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );
			
			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );


			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Install your <color=#F0D500>Alpha Blaster</color>, a strong first move!", new Vector3( 0, 420, 0 ) ) );



			tutorialThree.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 3 }, new Vector3(0,0.25f,0),  new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );
			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			//tutorialThree.Enqueue( new DisplayPopupBattleOnboardingRunner( "Hmm, interesting", "This card does something when you <color=#E92900>Jettison</color> it but it costs <color=#00ffff>2 power</color>, so we can’t play it yet.", "Okay" ) );

			tutorialThree.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInEndTurn, true, true ) );
			tutorialThree.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInTop, true, true ) );


			tutorialThree.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );

			//Wait for the next local players turn
			tutorialThree.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			//tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Notice that <color=#00E67D>Social Climber's</color> attack was buffed by the <color=#7CA9F1>Pop'N'Lock</color>?", new Vector3( 0, 420, 0 ) ) );
			tutorialThree.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );


			//Player Turn 2
			//tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 2.5f ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "<color=#7CA9F1>Pop 'N' Lock</color> has TARGET LOCK! We can't target anything else while it's active!", new Vector3( 0, 420, 0 ) ) );

			tutorialThree.Enqueue( new WaitForComponentToFireAtComponent( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, x => x.SocketController.Component != null && x.SocketController.Component.Data.Buffs.Contains( BattleEntityController.Buff.TargetLock ), new Vector3(0.0f, 0.0f, 0.0f), new Vector3(-90,0.0f,0.0f), new Vector3( 2, 2, 2 ) ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Target Lock deactivated! Now take out their <color=#00E67D>Social Climber</color> with <color=#C6B102>Slamdown</color>!", new Vector3( 0, 420, 0 ) ) );

			tutorialThree.Enqueue (new WaitForNanotechToFireAtComponent ( x => x.Data.EntityType == BattleEntityType.TargetingCrew, y => y.SocketController.Component != null && y.SocketController.Component.Data.State != BattleEntityState.Destroyed ));

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "<color=#FFF187>Junk Trunk</color> spawns a component on install. Hope it's a good one!", new Vector3( 0, 420, 0 ) ) );

			tutorialThree.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 5 }, new Vector3(0,0.25f,0),  new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 2.75f ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "We got a <color=#CA574A>HULL PUNCH</color>! Just what we need!", new Vector3( 0, 420, 0 ) ) );

			//tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Look, <color=#B9351C>Hard Exit</color> is highlighted. Now you can install it.", new Vector3( 0, -200, 0 ) ) );
			//tutorialThree.Enqueue( new WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( new int[ 1 ]{ 3 }, new Vector3(0,0.25f,0),  new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );

			tutorialThree.Enqueue( new HighlightEndTurnBattleOnboardingRunner( new Vector3( 0, 0.75f, 0 ), Vector3.zero, Vector3.one ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

            tutorialThree.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_3, "1"));

			//Wait for the next local players turn
			tutorialThree.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Remote ) );
			tutorialThree.Enqueue( new WaitForTurnBattleOnboardingRunner( PlayerType.Local ) );

			//tutorialThree.Enqueue( new DisplayPopupBattleOnboardingRunner( "Darn!", "Oh No! They destroyed your weapon. Didn’t it do something on <color=#E92900>Jettison</color>?", "Okay" ) );

			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Hull Punch does something cool when <color=#E92900>JETTISONED</color>. Tap on it...", new Vector3( 0, 420, 0 ) ) );
			
			tutorialThree.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.MoveInJettison, true, true ) );
			tutorialThree.Enqueue( new WaitForComponentToBeSelectedBattleOnboardingRunner( x => x && x.Data.Id == "US_HullPunch", x => x.SocketController.Component != null && x.SocketController.Component.Data.Id == "US_HullPunch", new Vector3(0.0f, 0.25f, 0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 )  ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			//This causes issues, the reason being that during these 0.5 seconds the player can deselect the US_HullPunch!
			//tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );
		
			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "...And hit the <color=#E92900>big red button</color>!", new Vector3( 0, 420, 0 ) ) );

			//We need to force the components 
			tutorialThree.Enqueue( new HighlightJettisonBattleOnboardingRunner(  x => x && x.Data.Id == "US_HullPunch", x => x.SocketController.Component != null && x.SocketController.Component.Data.Id == "US_HullPunch", new Vector3( 0, 1, 0 ), Vector3.zero, Vector3.one ) );



			//tutorialThree.Enqueue( new WaitForComponentToBeHighlightedBattleOnboardingRunner( x => x, 1.5f, new Vector3(0.0f, 0.25f, 0f), new Vector3( -90, 0, 0 ), new Vector3( 2, 2, 2 ) ) );
			//tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 0.5f ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

			tutorialThree.Enqueue( new WaitForSecondsOnboardingRunner( 1.5f ) );


			tutorialThree.Enqueue( new ShowBannerBattleOnboardingRunner( "Victory is ours for the taking! Fire at will!", new Vector3( 0, 420, 0 ) ) );


            tutorialThree.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_3, "1"));

			tutorialThree.Enqueue( new WaitForShipToBeDestroyedOnboardingRunner( y => y != null && y.Data.State == BattleEntityState.Online && y.TargetableState == TargetableLogicController.TargetableState.None, Vector3.zero, new Vector3(-90,0,0), new Vector3( 2, 2, 2 ) ) );

			tutorialThree.Enqueue( new HideBannerBattleOnboardingRunner( ) );

            // TODO: This only signifies that we have reached the end of the instructions to the player.  Should actually wait for the battle to end
            tutorialThree.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_3, null));

			tutorialThree.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );

			return tutorialThree;
		}

		private static Queue< BattleOnboardingRunner > GetTutorialFour ()
		{
			Queue< BattleOnboardingRunner > tutorialFour = new Queue< BattleOnboardingRunner >();

            tutorialFour.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_4, null));

			tutorialFour.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );

			tutorialFour.Enqueue( new HistoryPredicateOnboardingRunner( x  => x is DrawCardData ) );

			tutorialFour.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.Showing, true, true ) );
			tutorialFour.Enqueue( new WaitForSecondsOnboardingRunner( 3.5f ) );

			tutorialFour.Enqueue( new DisplayPopupBattleOnboardingRunner( "Rumble!", "It's all up to you now. Go get 'em!", "" ) );

            tutorialFour.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_4, "1"));

			tutorialFour.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.Showing, true, true ) );

            tutorialFour.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_4, "1"));

            // TODO: This only signifies that we have reached the end of the instructions to the player.  Should actually wait for the battle to end
            tutorialFour.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_4, null));

			tutorialFour.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );

			return tutorialFour;
		}
        /*
		private static Queue< BattleOnboardingRunner > GetTutorialFive ()
		{
			Queue< BattleOnboardingRunner > tutorialFive = new Queue< BattleOnboardingRunner >();

            tutorialFive.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_5, null));

			tutorialFive.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );

			tutorialFive.Enqueue( new HistoryPredicateOnboardingRunner( x  => x is DrawCardData ) );

			tutorialFive.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.Showing, true, true ) );

            // TODO: This only signifies that we have reached the end of the instructions to the player.  Should actually wait for the battle to end
            tutorialFive.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_5, null));

			tutorialFive.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );
           
			return tutorialFive;
		}

		private static Queue< BattleOnboardingRunner > GetTutorialSix ()
		{
			Queue< BattleOnboardingRunner > tutorialSix = new Queue< BattleOnboardingRunner >();

            tutorialSix.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding , TUTORIAL_6, null));

			tutorialSix.Enqueue( new DisableEmoteButtonBattleOnboardingRunner() );

			tutorialSix.Enqueue( new HistoryPredicateOnboardingRunner( x  => x is DrawCardData ) );

            tutorialSix.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Start, ProgressArea.Onboarding, TUTORIAL_6, "1"));

			tutorialSix.Enqueue( new HUDUpdateOnboardingRunner( PlayerHUDController.State.Showing, true, true ) );

            // TODO: This only signifies that we have reached the end of the instructions to the player.  Should actually wait for the battle to end
            tutorialSix.Enqueue( new AnalyticsOnboardingRunner(ProgressStatus.Complete, ProgressArea.Onboarding, TUTORIAL_6, null));

			tutorialSix.Enqueue( new WaitForBattleToEndBattleOnboardingRunner() );                       

			return tutorialSix;
		}
        */      
		#endregion
	}
}
