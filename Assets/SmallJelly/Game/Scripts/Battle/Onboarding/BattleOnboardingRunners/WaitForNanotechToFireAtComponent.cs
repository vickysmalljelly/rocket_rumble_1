﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForNanotechToFireAtComponent : HistoryPredicateOnboardingRunner
	{
		#region Member Variables
		private LocalPlayerBattleView mLocalPlayerBattleView;
		private RemotePlayerBattleView mRemotePlayerBattleView;

		private readonly Func< BattleEntity, bool > mCardsToHighlight;
		private readonly Func< Socket, bool > mComponentsToTarget;

		private bool mIndicatorArrowShown;
		#endregion

		#region Constructor
		public WaitForNanotechToFireAtComponent( Func< BattleEntity, bool > cardsToHighlight,  Func< Socket, bool > componentsToTarget ) : base( ( Func< BattleRunnerData, bool > )( x  => x is ComponentTakeDamageData) )
		{
			mCardsToHighlight = cardsToHighlight;
			mComponentsToTarget = componentsToTarget;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();
			mRemotePlayerBattleView = GameObject.FindObjectOfType< RemotePlayerBattleView >();

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );		
		}

		public override void OnUpdate()
		{
			RefreshIndicatorArrow();

			base.OnUpdate();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			BattleOnboardingDialogManager.Get.HideIndicatorArrow();
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( stateConstructionData.StateId == LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE )
			{
				ModifyIdleStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
			else
			{
				ModifyStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
		}
		#endregion

		#region Private Methods
		private void RefreshIndicatorArrow()
		{
			BattleEntity[] sourceEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Where( mCardsToHighlight ).ToArray();
			Socket[] targetSockets = mRemotePlayerBattleView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets.Where( mComponentsToTarget ).ToArray();

			if( sourceEntities.Length > 0 && targetSockets.Length > 0 && !mIndicatorArrowShown )
			{
				BattleOnboardingDialogManager.Get.ShowIndicatorArrow( sourceEntities[ 0 ].transform, Vector3.zero, targetSockets[ 0 ].transform, Vector3.zero );

				mIndicatorArrowShown = true;
			}
			else if( ( sourceEntities.Length == 0 || targetSockets.Length == 0 ) && mIndicatorArrowShown )
			{
				BattleOnboardingDialogManager.Get.HideIndicatorArrow();

				mIndicatorArrowShown = false;
			}
		}


		private void ModifyIdleStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow local player entities
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( false, true );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, battleEntityInteractionFilterResult };
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow attacking the ship where the target is ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( true, false );

			//Interested in opponent players slots
			SocketPredicateBattleFilter socketPredicateBattleFilter = new SocketPredicateBattleFilter( mComponentsToTarget );
			SocketOwnerBattleFilter draggingSocketOwnerBattleFilter = new SocketOwnerBattleFilter (false, true);

			//SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter ( true, false );

			// Only interested in ships/battle entities which are alive (hp > 0)
			AliveBattleFilter aliveBattleFilter = new AliveBattleFilter();

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, shipOwnerBattleFilter, shipOwnerBattleFilter, socketPredicateBattleFilter, draggingSocketOwnerBattleFilter, aliveBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, shipInteractionFilterResult, shipInteractionFilterResult, socketInteractionFilterResult, battleEntityInteractionFilterResult };

		}
		#endregion
	}
}