﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DisableQuitButtonBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Constructor
		public DisableQuitButtonBattleOnboardingRunner( )
		{
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			//Disable the quit button
			Transform quitButton = SJRenderTransformExtensions.FindDeepChild( Resources.FindObjectsOfTypeAll< LocalPlayerHUD >()[0].transform, "Quit" );
			quitButton.gameObject.SetActive( false );

			OnFinishRunning();
		}
		#endregion
	}
}