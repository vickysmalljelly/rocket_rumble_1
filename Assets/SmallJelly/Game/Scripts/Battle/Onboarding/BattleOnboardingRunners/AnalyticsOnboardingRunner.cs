﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Used to record analytics during onboarding.
    /// </summary>
    public class AnalyticsOnboardingRunner : BattleOnboardingRunner
    {
        private ProgressStatus mStatus;
        private ProgressArea mProgressArea;
        private string mS2, mS3;

        public AnalyticsOnboardingRunner(ProgressStatus status, ProgressArea s1, string s2, string s3)
        {
            mStatus = status;
            mProgressArea = s1;
            mS2 = s2;
            mS3 = s3;
         }

        public override void OnStartRunning()
        {
            SJLogger.LogMessage(MessageFilter.Onboarding, "------------------------------------ " + mStatus + " " + mProgressArea + " " + mS2 + " " + mS3);
            AnalyticsManager.Get.RecordGameProgress(mStatus, mProgressArea, mS2, mS3);

            OnFinishRunning();
        }                  
    }
}