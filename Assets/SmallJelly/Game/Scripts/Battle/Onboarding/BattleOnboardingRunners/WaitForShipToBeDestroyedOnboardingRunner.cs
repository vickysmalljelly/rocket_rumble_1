﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForShipToBeDestroyedOnboardingRunner : HistoryPredicateOnboardingRunner
	{
		#region Member Variables

		private LocalPlayerBattleView mLocalPlayerBattleView;
		private RemotePlayerBattleView mRemotePlayerBattleView;

		private Func< BattleEntity, bool > mComponentsToHighlight;

		private Dictionary< int, GameObject > mHighlightArrows;

		private bool mIndicatorArrowShown;
		private int mNumberOfPossibleIndicators;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion

		#region Constructor
		public WaitForShipToBeDestroyedOnboardingRunner( Func< BattleEntity, bool > componentsToHighlight, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale ) : base( x => x is HullDestroyedData )
		{
			mComponentsToHighlight = componentsToHighlight;
			mHighlightArrows = new Dictionary< int, GameObject >();

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();
			mRemotePlayerBattleView = GameObject.FindObjectOfType< RemotePlayerBattleView >();

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			RefreshHighlightArrows();	
			RefreshIndicatorArrow();

			base.OnUpdate();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			BattleOnboardingDialogManager.Get.HideIndicatorArrow();

			foreach( GameObject elementToRemove in mHighlightArrows.Values )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( elementToRemove );
			}
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( stateConstructionData.StateId == LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE )
			{
				ModifyIdleStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
			else
			{
				ModifyStateConstructionData( (InteractionStateConstructionData) stateConstructionData );
			}
		}
		#endregion

		#region Private Methods
		private void RefreshIndicatorArrow()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities.Where( mComponentsToHighlight ).ToArray();

			//If we've already fired one of the components then remove its indicator arrow and generate a new one!
			if( battleEntities.Length != mNumberOfPossibleIndicators && mIndicatorArrowShown )
			{
				BattleOnboardingDialogManager.Get.HideIndicatorArrow();
				mIndicatorArrowShown = false;
			}
			mNumberOfPossibleIndicators = battleEntities.Length;

			if( battleEntities.Length > 0 && !mIndicatorArrowShown )
			{
				//Show the indicator arrow because at least one component has met the criteria
				Ship ship = mRemotePlayerBattleView.PlayerBattleSceneView.PlayerBoardView.Ship;
				BattleOnboardingDialogManager.Get.ShowIndicatorArrow( battleEntities[ 0 ].transform, Vector3.zero, ship.transform, new Vector3(2, 1.2f, 7.5f) );

				mIndicatorArrowShown = true;
			}
			else if( battleEntities.Length == 0 && mIndicatorArrowShown )
			{
				//Hide the indicator arrow because no components have met the criteria
				BattleOnboardingDialogManager.Get.HideIndicatorArrow();

				mIndicatorArrowShown = false;
			}
		}

		private void RefreshHighlightArrows()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities.Where( mComponentsToHighlight ).ToArray();

			//Remove elements which should no longer be highlighted!
			int[] elementsToRemove = mHighlightArrows.Keys.Where( x => !battleEntities.Select( y => y.Data.DrawNumber ).Contains( x ) ).ToArray();

			foreach( int elementToRemove in elementsToRemove )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightArrows[ elementToRemove ] );
				mHighlightArrows.Remove( elementToRemove );
			}

			BattleEntity[] elementsToAdd = battleEntities.Where( x => !mHighlightArrows.ContainsKey( x.Data.DrawNumber ) ).ToArray();
			foreach( BattleEntity elementToAdd in elementsToAdd )
			{
				mHighlightArrows.Add( elementToAdd.Data.DrawNumber, BattleOnboardingDialogManager.Get.CreateHighlightRing( elementToAdd.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale ) );
			}			
		}

		private void ModifyIdleStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow local player entities
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( true, false );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, battleEntityInteractionFilterResult };
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Allow attacking the ship where the target is ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, true );

			//Not interested in opponent players slots
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter (false, false);

			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );

			//SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter ( true, false );

			// Only interested in ships/battle entities which are alive (hp > 0)
			AliveBattleFilter aliveBattleFilter = new AliveBattleFilter();

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, shipOwnerBattleFilter, shipOwnerBattleFilter, socketOwnerBattleFilter, aliveBattleFilter, battleEntityOwnerBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult, shipInteractionFilterResult, shipInteractionFilterResult, socketInteractionFilterResult, battleEntityInteractionFilterResult };

		}
		#endregion
	}
}