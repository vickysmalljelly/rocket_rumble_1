﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class HighlightJettisonBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private Func< BattleEntity, bool > mComponentsToHighlight;
		private Func< Socket, bool > mSocketsToHighlight;

		private LocalPlayerBattleView mLocalPlayerBattleView;

		private GameObject mHighlightArrow;
		private BattleEntity mSelectedBattleEntity;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion

		#region Constructor
		public HighlightJettisonBattleOnboardingRunner( Func< BattleEntity, bool > componentsToBeHighlighted, Func< Socket, bool > socketsToHighlight, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale )
		{
			mComponentsToHighlight = componentsToBeHighlighted;
			mSocketsToHighlight = socketsToHighlight;

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			LocalPlayerHUD localPlayerHud = GameObject.FindObjectOfType< LocalPlayerHUD >();
			mHighlightArrow = BattleOnboardingDialogManager.Get.CreateHighlightArrow( localPlayerHud.JettisonButton.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale );

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );		
	
			RegisterListeners();
		}

		public override void OnUpdate()
		{
			//If the supplied history element exists then finish running
			if( BattleManager.Get.BattleGameplayController.GetHistoryWhere( x => x is ComponentJettisonData ).Length >= 1 )
			{
				OnFinishRunning();
			}
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			UnregisterListeners();

			BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightArrow );
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void RegisterListeners()
		{
			//Grab the selected battle entity
			mSelectedBattleEntity = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities.First( x => x != null && x.SelectionState == TargetableLogicController.SelectionState.Selected );

			SJLogger.AssertCondition( mSelectedBattleEntity != null, "If we're going to force the user to jettison then a battle entity should be selected by this point!" );
		
			mSelectedBattleEntity.SelectionStateTransitioned += HandleSelectionStateTransitioned;
		}

		private void UnregisterListeners()
		{
			//Grab the selected battle entity
			mSelectedBattleEntity.SelectionStateTransitioned -= HandleSelectionStateTransitioned;
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( true );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );
			SocketPredicateBattleFilter socketPredicateBattleFilter = new SocketPredicateBattleFilter( mSocketsToHighlight );
			BattleEntityPredicateBattleFilter battleEntityPredicateBattleFilter = new BattleEntityPredicateBattleFilter( mComponentsToHighlight );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, battleEntityPredicateBattleFilter, battleEntityOwnerBattleFilter, socketPredicateBattleFilter, };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ socketInteractionFilterResult, jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, battleEntityInteractionFilterResult };

		}
		#endregion

		#region Event Handlers
		private void HandleSelectionStateTransitioned( object o, BattleEntityAndSelectionStateTransitionEventArgs battleEntityAndSelectionStateTransitionEventArgs )
		{
			//If we de-select a battle entity then force it's reselection
			if( battleEntityAndSelectionStateTransitionEventArgs.NewState == TargetableLogicController.SelectionState.NotSelected )
			{
				battleEntityAndSelectionStateTransitionEventArgs.BattleEntity.OnInsertSelectionState( TargetableLogicController.SelectionState.Selected );
			}
		}
		#endregion
	}
}