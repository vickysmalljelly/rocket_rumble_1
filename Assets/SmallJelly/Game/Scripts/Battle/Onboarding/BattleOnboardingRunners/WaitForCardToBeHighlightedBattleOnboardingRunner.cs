﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForCardToBeHighlightedBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private LocalPlayerBattleView mLocalPlayerBattleView;

		private bool mIndicatorArrowShown;

		private Func< BattleEntity, bool > mCardsToHighlight;

		private Dictionary< BattleEntity, GameObject > mHighlightArrows;

		private IEnumerator WaitForRolloverFinishEnumerator;

		private float mMinimumSeconds;

		private bool mFinishedTimer;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion

		#region Constructor
		public WaitForCardToBeHighlightedBattleOnboardingRunner( Func< BattleEntity, bool > cardsToHighlight, float minimumSeconds, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale )
		{
			mCardsToHighlight = cardsToHighlight;
			mHighlightArrows = new Dictionary< BattleEntity, GameObject >();

			mMinimumSeconds = minimumSeconds;

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			//Pretend its the remote players turn to stop the cards from being playable
			for( int i = 0; i < mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Count; i++ )
			{
				mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities[ i ].OnStartTurn( PlayerType.Remote );
			}

			//Let's request an interaction state change to idle
			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			base.OnUpdate();

			RefreshHighlightArrows();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			//Return the cards to their previous playability
			for( int i = 0; i < mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Count; i++ )
			{
				mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities[ i ].OnStartTurn( PlayerType.Local );
			}

			foreach( BattleEntity battleEntity in mHighlightArrows.Keys )
			{
				battleEntity.StateTransitioned -= HandleStateTransitioned;
			}

			foreach( GameObject elementToRemove in mHighlightArrows.Values )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( elementToRemove );
			}
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			InteractionStateConstructionData interactionStateConstructionData = (InteractionStateConstructionData) stateConstructionData;

			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( interactionStateConstructionData );
		}
		#endregion

		#region Private Methods
		private void RefreshHighlightArrows()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Where( mCardsToHighlight ).ToArray();

			foreach( GameObject arrow in mHighlightArrows.Values )
			{
				arrow.transform.localPosition = mHighlightOffset;
			}

			//Remove elements which should no longer be highlighted!
			BattleEntity[] elementsToRemove = mHighlightArrows.Keys.Where( x => !battleEntities.Contains( x ) ).ToArray();

			foreach( BattleEntity elementToRemove in elementsToRemove )
			{
				elementToRemove.StateTransitioned -= HandleStateTransitioned;

				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightArrows[ elementToRemove ] );
				mHighlightArrows.Remove( elementToRemove );
			}

			BattleEntity[] elementsToAdd = battleEntities.Where( x => !mHighlightArrows.ContainsKey( x ) ).ToArray();
			foreach( BattleEntity elementToAdd in elementsToAdd )
			{
				elementToAdd.StateTransitioned += HandleStateTransitioned;

				mHighlightArrows.Add( elementToAdd, BattleOnboardingDialogManager.Get.CreateHighlightArrow( elementToAdd.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale ) );
			}
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
			SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter( false, true );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, socketOccupiedBattleFilter, shipOwnerBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, shipInteractionFilterResult };
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			if( battleEntityAndStateTransitionEventArgs.NewState == BattleEntityController.State.CardRollover )
			{
				WaitForRolloverFinishEnumerator = WaitForRolloverFinish();
				GameManager.Get.DoCoroutine( WaitForRolloverFinishEnumerator );
			}

			if( battleEntityAndStateTransitionEventArgs.NewState == BattleEntityController.State.CardRolloff  )
			{
				if( mFinishedTimer )
				{
					OnFinishRunning();
				}

				if( WaitForRolloverFinishEnumerator != null )
				{
					GameManager.Get.StopCoroutine( WaitForRolloverFinishEnumerator );
				}
			}
		}
		#endregion

		#region Coroutines
		private IEnumerator WaitForRolloverFinish()
		{
			yield return new WaitForSeconds( mMinimumSeconds );

			mFinishedTimer = true;
		}
		#endregion
	}
}