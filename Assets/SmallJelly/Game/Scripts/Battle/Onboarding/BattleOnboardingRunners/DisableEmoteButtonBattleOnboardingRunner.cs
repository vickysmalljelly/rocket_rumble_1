﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DisableEmoteButtonBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Constructor
		public DisableEmoteButtonBattleOnboardingRunner( )
		{
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			//Disable the quit button
			Transform emoteButton = SJRenderTransformExtensions.FindDeepChild( Resources.FindObjectsOfTypeAll< LocalPlayerHUD >()[0].transform, "emoteButton" );
			Collider collider = emoteButton.gameObject.GetComponent< Collider >();
			UnityEngine.Object.Destroy( collider );

			OnFinishRunning();
		}
		#endregion
	}
}