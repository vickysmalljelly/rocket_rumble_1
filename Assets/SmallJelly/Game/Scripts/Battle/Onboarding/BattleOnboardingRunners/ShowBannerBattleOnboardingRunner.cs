﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ShowBannerBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private string mText;
		private Vector3 mPosition;
		#endregion

		#region Constructor
		public ShowBannerBattleOnboardingRunner( string text, Vector3 position )
		{
			mText = text;
			mPosition = position;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			PlayerHUD playerHUD = BattleManager.Get.BattleGameplayController.LocalPlayerBattleView.PlayerHUD;
			GameObject banner = BattleOnboardingDialogManager.Get.CreateBanner( mText, mPosition, playerHUD.transform );
			banner.GetComponent< Canvas >().worldCamera = SJRenderTransformExtensions.GetCamera( banner.layer );

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		
			OnFinishRunning();
		}
		#endregion

		#region Public Methods
		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, battleEntityOwnerBattleFilter, shipOwnerBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, battleEntityInteractionFilterResult, shipInteractionFilterResult };

		}
		#endregion
	}
}