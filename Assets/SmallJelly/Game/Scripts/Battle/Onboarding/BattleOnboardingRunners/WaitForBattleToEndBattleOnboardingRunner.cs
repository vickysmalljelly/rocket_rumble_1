﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
	public class WaitForBattleToEndBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, new BattleFilter[ 0 ], new FilterStateResult[ 0 ] ) );
		}


		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			List< FilterStateResult > filterStateResults = new List< FilterStateResult >( interactionStateConstructionData.FilterStateResults );
			int count = filterStateResults.RemoveAll( x => x.GetType() == typeof( HUDInteractionFilterResult )  );

			if( count > 0 )
			{
				filterStateResults.Add( new HUDInteractionFilterResult( PlayerHUDController.State.MoveInEndTurn, PlayerHUDController.State.Hiding ) );

				interactionStateConstructionData.FilterStateResults = filterStateResults.ToArray();
			}
		}
		#endregion
	}
}