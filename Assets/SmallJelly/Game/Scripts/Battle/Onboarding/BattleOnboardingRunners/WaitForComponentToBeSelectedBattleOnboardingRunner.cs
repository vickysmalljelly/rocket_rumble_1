﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForComponentToBeSelectedBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private LocalPlayerBattleView mLocalPlayerBattleView;

		private bool mIndicatorArrowShown;

		private Func< BattleEntity, bool > mComponentsToHighlight;
		private Func< Socket, bool > mSocketsToHighlight;

		private Dictionary< BattleEntity, GameObject > mHighlightRings;

		private IEnumerator WaitForRolloverFinishEnumerator;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion


		#region Constructor
		public WaitForComponentToBeSelectedBattleOnboardingRunner( Func< BattleEntity, bool > componentsToBeHighlighted, Func< Socket, bool > socketsToHighlight, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale )
		{
			mComponentsToHighlight = componentsToBeHighlighted;
			mSocketsToHighlight = socketsToHighlight;

			mHighlightRings = new Dictionary< BattleEntity, GameObject >();

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			//Let's request an interaction state change to idle
			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			base.OnUpdate();

			RefreshHighlightRings();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			foreach( BattleEntity battleEntity in mHighlightRings.Keys )
			{
				battleEntity.SelectionStateTransitioned -= HandleHighlightedStateTransitioned;
			}

			foreach( GameObject elementToRemove in mHighlightRings.Values )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightRing( elementToRemove );
			}
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			InteractionStateConstructionData interactionStateConstructionData = (InteractionStateConstructionData) stateConstructionData;

			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( interactionStateConstructionData );
		}
		#endregion

		#region Private Methods
		private void RefreshHighlightRings()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities.Where( mComponentsToHighlight ).ToArray();

			//Remove elements which should no longer be highlighted!
			BattleEntity[] elementsToRemove = mHighlightRings.Keys.Where( x => !battleEntities.Contains( x ) ).ToArray();

			foreach( BattleEntity elementToRemove in elementsToRemove )
			{
				elementToRemove.SelectionStateTransitioned -= HandleHighlightedStateTransitioned;

				BattleOnboardingDialogManager.Get.RemoveHighlightRing( mHighlightRings[ elementToRemove ] );
				mHighlightRings.Remove( elementToRemove );
			}

			BattleEntity[] elementsToAdd = battleEntities.Where( x => !mHighlightRings.ContainsKey( x ) ).ToArray();
			foreach( BattleEntity elementToAdd in elementsToAdd )
			{
				elementToAdd.SelectionStateTransitioned += HandleHighlightedStateTransitioned;

				mHighlightRings.Add( elementToAdd, BattleOnboardingDialogManager.Get.CreateHighlightRing( elementToAdd.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale ) );
			}
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketPredicateBattleFilter socketPredicateBattleFilter = new SocketPredicateBattleFilter( mSocketsToHighlight );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( true, false );
			BattleEntityPredicateBattleFilter battleEntityPredicateBattleFilter = new BattleEntityPredicateBattleFilter( mComponentsToHighlight );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketPredicateBattleFilter, battleEntityPredicateBattleFilter, shipOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, shipInteractionFilterResult, battleEntityInteractionFilterResult };
		}
		#endregion

		#region Event Handlers
		private void HandleHighlightedStateTransitioned( object o, BattleEntityAndSelectionStateTransitionEventArgs battleEntityAndSelectionStateTransitionEventArgs )
		{
			if( battleEntityAndSelectionStateTransitionEventArgs.NewState == TargetableLogicController.SelectionState.Selected )
			{
				OnFinishRunning();
			}
		}
		#endregion
	}
}