﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	//TODO - Each of these onboarding runners essentially represents a playmaker graph!
	public abstract class BattleOnboardingRunner
	{
		#region Event Handlers
		public event Action< StateMachine.State, StateConstructionData> RequestInteractionStateChange;
		public event Action FinishedRunning;
		#endregion

		#region Public Methods
		public virtual void OnStartRunning()
		{
			SJLogger.LogMessage( MessageFilter.George, "Start running {0}", GetType().ToString() );
		}

		public virtual void OnUpdate()
		{
		}

		public virtual void OnFinishRunning()
		{
			SJLogger.LogMessage( MessageFilter.George, "Finish running {0}", GetType().ToString() );

			FireFinishedRunning();
		}

		public virtual void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			
		}

		public virtual void OnRemotePlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData((InteractionStateConstructionData) stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ new EndTurnButtonBattleFilter( false ), new JettisonButtonBattleFilter( false ) };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding ), new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding ) };
		}
		#endregion

		#region Event Firing
		protected void FireRequestInteractionStateChange( StateConstructionData stateConstructionData )
		{
			if( RequestInteractionStateChange != null )
			{
				RequestInteractionStateChange( default( StateMachine.State ), stateConstructionData );
			}
		}

		private void FireFinishedRunning()
		{
			if( FinishedRunning != null )
			{
				FinishedRunning( );
			}
		}
		#endregion
	}
}