﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class HighlightEndTurnBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private GameObject mHighlightArrow;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion

		#region Constructor
		public HighlightEndTurnBattleOnboardingRunner( Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale )
		{
			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			LocalPlayerHUD localPlayerHud = GameObject.FindObjectOfType< LocalPlayerHUD >();
			mHighlightArrow = BattleOnboardingDialogManager.Get.CreateHighlightArrow( localPlayerHud.EndTurnButton.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale );

			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			if( BattleManager.Get.CurrentTurn == PlayerType.Remote )
			{
				OnFinishRunning();
			}
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightArrow );
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			//Show end turn button but not jettison button
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );

			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ buttonBattleFilter, jettisonButtonBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ buttonInteractionFilterResult, jettisonButtonInteractionFilterResult };

		}
		#endregion
	}
}