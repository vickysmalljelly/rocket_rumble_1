﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner : HistoryPredicateOnboardingRunner
	{
		#region Protected Properties
		protected int[] SocketsToEnable;
		#endregion

		#region Member Variables
		//TODO - Take this out once we move over to playmaker!
		private bool mFinishing;

		private GameObject mHighlightRing;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion

		#region Constructor
		public WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner( int[] socketsToEnable, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale ) : base( ( Func< BattleRunnerData, bool > )( x  => x is InstallComponentData ) )
		{
			SocketsToEnable = socketsToEnable;

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			//Let's request an interaction state change to idle
			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			mFinishing = true;

			if( mHighlightRing )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightRing );
			}

			BattleOnboardingDialogManager.Get.HideIndicatorArrow( );
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			InteractionStateConstructionData interactionStateConstructionData = (InteractionStateConstructionData) stateConstructionData;

			//Select first weapon
			LocalPlayerBattleView localPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			BattleEntity battleEntity = localPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.FirstOrDefault( x => x.Data.State == BattleEntityState.InHandPlayable );
			Socket socket = localPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets[ SocketsToEnable[ 0 ] ];


			if( interactionStateConstructionData.StateId == LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE )
			{
				if( mHighlightRing )
				{
					BattleOnboardingDialogManager.Get.RemoveHighlightRing( mHighlightRing );
				}

				if( !mFinishing && battleEntity != default( BattleEntity ) )
				{
					BattleOnboardingDialogManager.Get.ShowIndicatorArrow( battleEntity.transform, Vector3.zero, socket.transform, Vector3.zero );
				}
			}
			else
			{
				if( !mHighlightRing )
				{
					mHighlightRing = BattleOnboardingDialogManager.Get.CreateHighlightRing( socket.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale );
				}

				BattleOnboardingDialogManager.Get.HideIndicatorArrow();
			}

			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( interactionStateConstructionData, SocketsToEnable );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData, int[] socketsToEnable )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketListBattleFilter socketListBattleFilter = new SocketListBattleFilter( new List< int > ( socketsToEnable ), new List< int >() );
			SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter( false, true );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( false, true );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketListBattleFilter, shipOwnerBattleFilter, socketOccupiedBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, shipInteractionFilterResult, battleEntityInteractionFilterResult };
	
		}
		#endregion
	}
}