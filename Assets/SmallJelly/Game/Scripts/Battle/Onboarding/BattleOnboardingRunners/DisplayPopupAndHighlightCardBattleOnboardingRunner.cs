﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class DisplayPopupAndHighlightCardBattleOnboardingRunner : DisplayPopupBattleOnboardingRunner
	{
		#region Member Variables
		private Func< BattleEntity, bool > mPredicate;
		private Vector3 mPositionOffset;
		private Vector3 mRotationOffset;
		private Vector3 mScale;

		private List< GameObject > mHighlightArrows;
		#endregion

		#region Constructor
		public DisplayPopupAndHighlightCardBattleOnboardingRunner( string title, string message, string buttonName, Func< BattleEntity, bool > predicate, Vector3 positionOffset, Vector3 rotationOffset, Vector3 scale ) : base( title, message, buttonName )
		{
			mPredicate = predicate;
			mPositionOffset = positionOffset;
			mRotationOffset = rotationOffset;
			mScale = scale;

			mHighlightArrows = new List< GameObject >();
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			LocalPlayerBattleView localPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			BattleEntity[] battleEntities = localPlayerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities.Where( mPredicate ).ToArray();

			foreach( BattleEntity battleEntity in battleEntities )
			{
				mHighlightArrows.Add( BattleOnboardingDialogManager.Get.CreateHighlightArrow( battleEntity.gameObject, mPositionOffset, mRotationOffset, mScale ) );
			}
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			foreach( GameObject highlightArrow in mHighlightArrows )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightArrow( highlightArrow );
			}
		}
		#endregion
	}
}