﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class DisplayPopupAndHighlightCardAndLocalHudBattleOnboardingRunner : DisplayPopupAndHighlightCardBattleOnboardingRunner
	{
		#region Member Variables
		private PlayerType mPlayerHudToHighlight;

		private Vector3 mHudHighlightPositionOffset;
		private Vector3 mHudHighlightRotationOffset;
		private Vector3 mHudHighlightScale;

		private GameObject mHighlightArrow;
		#endregion

		#region Constructor
		public DisplayPopupAndHighlightCardAndLocalHudBattleOnboardingRunner( string title, string message, string buttonName, Func< BattleEntity, bool > battleEntityHighlightPredicate, Vector3 battleEntityHighlightPositionOffset, Vector3 battleEntityHighlightRotationOffset, Vector3 battleEntityHighlightScale, PlayerType playerHudToHighlight, Vector3 hudHighlightPositionOffset, Vector3 hudHighlightRotationOffset, Vector3 hudHighlightScale ) : base( title, message, buttonName, battleEntityHighlightPredicate, battleEntityHighlightPositionOffset, battleEntityHighlightRotationOffset, battleEntityHighlightScale )
		{
			mPlayerHudToHighlight = playerHudToHighlight;

			mHudHighlightPositionOffset = hudHighlightPositionOffset;
			mHudHighlightRotationOffset = hudHighlightRotationOffset;
			mHudHighlightScale = hudHighlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();


			//TODO - Take this out, it's the easiest way to access the bar but it's not very nice
			GameObject topBar = mPlayerHudToHighlight == PlayerType.Local ? GameObject.FindObjectOfType< LocalPlayerHUD >().transform.Find( "Canvas/LocalTopBar" ).gameObject : GameObject.FindObjectOfType< RemotePlayerHUD >().transform.Find( "Canvas/RemoteTopBar" ).gameObject;

			mHighlightArrow = BattleOnboardingDialogManager.Get.CreateHighlightArrow( topBar, mHudHighlightPositionOffset, mHudHighlightRotationOffset, mHudHighlightScale );
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			BattleOnboardingDialogManager.Get.RemoveHighlightArrow( mHighlightArrow );
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( (InteractionStateConstructionData)stateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, battleEntityOwnerBattleFilter, shipOwnerBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, battleEntityInteractionFilterResult, shipInteractionFilterResult };

		}
		#endregion
	}
}