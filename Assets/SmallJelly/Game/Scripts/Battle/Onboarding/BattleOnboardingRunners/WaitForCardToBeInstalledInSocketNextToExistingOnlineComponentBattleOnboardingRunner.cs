﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class WaitForCardToBeInstalledInSocketNextToExistingOnlineComponentBattleOnboardingRunner : WaitForCardToBeInstalledInSpecifiedSocketBattleOnboardingRunner
	{
		#region Member Variables
		//There's no point calculating the adjaceny because we only have a concept of adjacency on the server (on the client it's a 1D array) and 
		//there are only 9 variations!
		private readonly int[] mSocketZeroAdjacency = { 1, 3 };
		private readonly int[] mSocketOneAdjacency = { 0, 2, 4 };
		private readonly int[] mSocketTwoAdjacency = { 1, 5 };
		private readonly int[] mSocketThreeAdjacency = { 0, 4, 6 };
		private readonly int[] mSocketFourAdjacency = { 1, 3, 5, 7 };
		private readonly int[] mSocketFiveAdjacency = { 2, 4, 8 };
		private readonly int[] mSocketSixAdjacency = { 3, 7 };
		private readonly int[] mSocketSevenAdjacency = { 4, 6, 8 };
		private readonly int[] mSocketEightAdjacency = { 5, 7 };
		#endregion

		#region Constructor
		public WaitForCardToBeInstalledInSocketNextToExistingOnlineComponentBattleOnboardingRunner( Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale ) : base( new int[ 0 ], highlightOffset, highlightRotation, highlightScale )
		{
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			LocalPlayerBattleView localPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			for( int i = 0; i < localPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.Count; i++ )
			{
				if( localPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities[ i ] == null )
				{
					continue;
				}

				if( localPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities[ i ].Data.State != BattleEntityState.Online )
				{
					continue;
				}

				switch( i )
				{
					case 0:
						SocketsToEnable = mSocketZeroAdjacency;
						break;

					case 1:
						SocketsToEnable = mSocketOneAdjacency;
						break;

					case 2:
						SocketsToEnable = mSocketTwoAdjacency;
						break;

					case 3:
						SocketsToEnable = mSocketThreeAdjacency;
						break;

					case 4:
						SocketsToEnable = mSocketFourAdjacency;
						break;

					case 5:
						SocketsToEnable = mSocketFiveAdjacency;
						break;

					case 6:
						SocketsToEnable = mSocketSixAdjacency;
						break;

					case 7:
						SocketsToEnable = mSocketSevenAdjacency;
						break;

					case 8:
						SocketsToEnable = mSocketEightAdjacency;
						break;
				}
			}

			base.OnStartRunning();
		}
		#endregion
	}
}