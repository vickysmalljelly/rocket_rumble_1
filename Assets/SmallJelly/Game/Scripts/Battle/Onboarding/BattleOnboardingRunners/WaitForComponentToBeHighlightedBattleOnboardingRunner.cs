﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForComponentToBeHighlightedBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private LocalPlayerBattleView mLocalPlayerBattleView;

		private bool mIndicatorArrowShown;

		private Func< BattleEntity, bool > mComponentsToHighlight;

		private Dictionary< BattleEntity, GameObject > mHighlightRings;

		private IEnumerator WaitForRolloverFinishEnumerator;

		private float mMinimumSeconds;

		private bool mFinishedTimer;

		private Vector3 mHighlightOffset;
		private Vector3 mHighlightRotation;
		private Vector3 mHighlightScale;
		#endregion


		#region Constructor
		public WaitForComponentToBeHighlightedBattleOnboardingRunner( Func< BattleEntity, bool > componentsToBeHighlighted, float minimumSeconds, Vector3 highlightOffset, Vector3 highlightRotation, Vector3 highlightScale )
		{
			mComponentsToHighlight = componentsToBeHighlighted;
			mHighlightRings = new Dictionary< BattleEntity, GameObject >();

			mMinimumSeconds = minimumSeconds;

			mHighlightOffset = highlightOffset;
			mHighlightRotation = highlightRotation;
			mHighlightScale = highlightScale;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			mLocalPlayerBattleView = GameObject.FindObjectOfType< LocalPlayerBattleView >();

			//Let's request an interaction state change to idle
			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			base.OnUpdate();

			RefreshHighlightRings();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			foreach( BattleEntity battleEntity in mHighlightRings.Keys )
			{
				battleEntity.HighlightedStateTransitioned -= HandleHighlightedStateTransitioned;
			}

			foreach( GameObject elementToRemove in mHighlightRings.Values )
			{
				BattleOnboardingDialogManager.Get.RemoveHighlightRing( elementToRemove );
			}
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			InteractionStateConstructionData interactionStateConstructionData = (InteractionStateConstructionData) stateConstructionData;

			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( interactionStateConstructionData );
		}
		#endregion

		#region Private Methods
		private void RefreshHighlightRings()
		{
			BattleEntity[] battleEntities = mLocalPlayerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities.Where( mComponentsToHighlight ).ToArray();

			//Remove elements which should no longer be highlighted!
			BattleEntity[] elementsToRemove = mHighlightRings.Keys.Where( x => !battleEntities.Contains( x ) ).ToArray();

			foreach( BattleEntity elementToRemove in elementsToRemove )
			{
				elementToRemove.HighlightedStateTransitioned -= HandleHighlightedStateTransitioned;

				BattleOnboardingDialogManager.Get.RemoveHighlightRing( mHighlightRings[ elementToRemove ] );
				mHighlightRings.Remove( elementToRemove );
			}

			BattleEntity[] elementsToAdd = battleEntities.Where( x => !mHighlightRings.ContainsKey( x ) ).ToArray();
			foreach( BattleEntity elementToAdd in elementsToAdd )
			{
				elementToAdd.HighlightedStateTransitioned += HandleHighlightedStateTransitioned;

				mHighlightRings.Add( elementToAdd, BattleOnboardingDialogManager.Get.CreateHighlightRing( elementToAdd.gameObject, mHighlightOffset, mHighlightRotation, mHighlightScale ) );
			}
		}

		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
			SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter( false, true );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );
			InstalledBattleEntityBattleFilter installedBattleEntityBattleFilter = new InstalledBattleEntityBattleFilter( true, false );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.NonTargetable );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, socketOccupiedBattleFilter, shipOwnerBattleFilter, battleEntityOwnerBattleFilter, installedBattleEntityBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, shipInteractionFilterResult, battleEntityInteractionFilterResult };
		}
		#endregion

		#region Event Handlers
		private void HandleHighlightedStateTransitioned( object o, BattleEntityAndHighlightedStateTransitionEventArgs battleEntityAndHighlightedStateTransitionEventArgs )
		{
			if( battleEntityAndHighlightedStateTransitionEventArgs.NewState == ComponentController.HighlightedState.Highlighted )
			{
				WaitForRolloverFinishEnumerator = WaitForRolloverFinish();
				GameManager.Get.DoCoroutine( WaitForRolloverFinishEnumerator );
			}

			if( battleEntityAndHighlightedStateTransitionEventArgs.NewState == ComponentController.HighlightedState.NotHighlighted )
			{
				if( mFinishedTimer )
				{
					OnFinishRunning();
				}

				GameManager.Get.StopCoroutine( WaitForRolloverFinishEnumerator );
			}
		}
		#endregion

		#region Coroutines
		private IEnumerator WaitForRolloverFinish()
		{
			yield return new WaitForSeconds( mMinimumSeconds );

			mFinishedTimer = true;
		}
		#endregion
	}
}