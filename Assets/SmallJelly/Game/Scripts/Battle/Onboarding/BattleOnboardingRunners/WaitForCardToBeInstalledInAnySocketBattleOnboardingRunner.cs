﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class WaitForCardToBeInstalledInAnySocketBattleOnboardingRunner : BattleOnboardingRunner
	{
		#region Member Variables
		private int mCompletionHistoryCount;
		#endregion

		#region Constructor
		public WaitForCardToBeInstalledInAnySocketBattleOnboardingRunner( int completionHistoryCount )
		{
			mCompletionHistoryCount = completionHistoryCount;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			//Let's request an interaction state change to idle
			FireRequestInteractionStateChange( new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, null, null ) );
		}

		public override void OnUpdate()
		{
			base.OnUpdate();

			//If the supplied history element exists then finish running
			if( BattleManager.Get.BattleGameplayController.GetHistoryWhere( x => x is InstallComponentData ).Length >= mCompletionHistoryCount )
			{
				OnFinishRunning();
				return;
			}

		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();
		}

		public override void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			InteractionStateConstructionData interactionStateConstructionData = (InteractionStateConstructionData) stateConstructionData;

			//If we ever happen to move into another state while displaying a popup - let's immediately disable all of its interaction!
			ModifyStateConstructionData( interactionStateConstructionData );
		}
		#endregion

		#region Private Methods
		private void ModifyStateConstructionData( InteractionStateConstructionData interactionStateConstructionData )
		{
			JettisonButtonBattleFilter JettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( true, false );
			SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter( false, true );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );
			//BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( true, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			//BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.None, TargetableLogicController.TargetableState.None );

			interactionStateConstructionData.BattleFilters = new BattleFilter[]{ JettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, socketOccupiedBattleFilter, shipOwnerBattleFilter };
			interactionStateConstructionData.FilterStateResults = new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, shipInteractionFilterResult };
		}
		#endregion
	}
}