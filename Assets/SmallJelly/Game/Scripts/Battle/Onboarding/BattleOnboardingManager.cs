﻿using SmallJelly.Framework;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SmallJelly
{
	public class BattleOnboardingManager : MonoBehaviourSingleton< BattleOnboardingManager >
	{
		#region Event Handlers
		public event Action< StateMachine.State, StateConstructionData> RequestInteractionStateChange;
		#endregion

		#region Member Variables
		private Queue< BattleOnboardingRunner > mSectionRunnerQueue;
		private BattleOnboardingRunner mCurrentRunner;
		private bool mRunnerFinished;
		#endregion

		#region Public Methods
		public void Initialise( RRChallenge challenge )
		{
			mSectionRunnerQueue = BattleOnboardingRunnerFactory.GetBattleOnboardingRunnerQueue( challenge.BattleConfig.BattleId );
			mRunnerFinished = true;
		}

		public void OnStartRunning( BattleOnboardingRunner battleOnboardingRunner )
		{
			mRunnerFinished = false;

			mCurrentRunner = battleOnboardingRunner;

			RegisterListeners( battleOnboardingRunner );

			battleOnboardingRunner.OnStartRunning();
		}

		public void Update()
		{
			if( mRunnerFinished )
			{
				if( mCurrentRunner != null )
				{
					OnFinishRunning( mCurrentRunner );
				}

				if( mSectionRunnerQueue.Count > 0 )
				{
					OnStartRunning( mSectionRunnerQueue.Dequeue() );
				}
				return;
			}


			//TODO - I dont like this approach but it'll do for now - running the events in this way by forcing an update
			//will make it easy to move over to playmaker!
			mCurrentRunner.OnUpdate();
		}

		public void OnFinishRunning( BattleOnboardingRunner battleOnboardingRunner )
		{
			UnregisterListeners( battleOnboardingRunner );

			mCurrentRunner = null;
		}

		public void OnLocalPlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( mCurrentRunner == null )
			{
				return;
			}

			mCurrentRunner.OnLocalPlayerInteractionStateFinished( state, stateConstructionData );

		}

		public void OnRemotePlayerInteractionStateFinished( LocalPlayerTurnInteractionStateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( mCurrentRunner == null )
			{
				return;
			}

			mCurrentRunner.OnRemotePlayerInteractionStateFinished( state, stateConstructionData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( BattleOnboardingRunner battleOnboardingRunner )
		{
			//Onboarding section runners have the authority to terminate states early to move to a custom state of their choosing.
			//This is important because we may wish to divert to an alternate state when popups are displayed and so on!
			battleOnboardingRunner.RequestInteractionStateChange += HandleRequestInteractionStateChange;
			battleOnboardingRunner.FinishedRunning += HandleBattleOnboardingRunnerFinished;
		}

		private void UnregisterListeners( BattleOnboardingRunner battleOnboardingRunner )
		{
			battleOnboardingRunner.RequestInteractionStateChange -= HandleRequestInteractionStateChange;
			battleOnboardingRunner.FinishedRunning -= HandleBattleOnboardingRunnerFinished;
		}
		#endregion

		#region Event Firing
		private void FireRequestInteractionStateChange( StateMachine.State state, StateConstructionData stateConstructionData )
		{
			if( RequestInteractionStateChange != null )
			{
				RequestInteractionStateChange( state, stateConstructionData );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleRequestInteractionStateChange( StateMachine.State state, StateConstructionData stateConstructionData )
		{
			FireRequestInteractionStateChange( state, stateConstructionData );
		}

		private void HandleBattleOnboardingRunnerFinished( )
		{
			mRunnerFinished = true;
		}
		#endregion
	}

}