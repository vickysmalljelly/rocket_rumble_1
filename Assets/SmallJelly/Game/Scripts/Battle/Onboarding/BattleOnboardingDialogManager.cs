﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly.Framework
{
	/// <summary>
	/// Responsible for all dialogs relating specifically to the battle onboarding process
	/// </summary>
	public class BattleOnboardingDialogManager : MonoBehaviourSingleton< BattleOnboardingDialogManager >
	{
		#region Inspector Variables
		[SerializeField]
		private GameObject mHighlightArrowPrefab;

		[SerializeField]
		private GameObject mHighlightRingPrefab;

		[SerializeField]
		private GameObject mBannerPrefab;

		[SerializeField]
		private GameObject mIndicatorArrow;

		[SerializeField]
		private GameObject mPopupDialog;
		#endregion

		#region Private Member Variables
		private PopupViewTMP mPopupView;
		private GameObject mBanner;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(mHighlightArrowPrefab != null, "mSpinnerWithMessage has not been set in the inspector");
			SJLogger.AssertCondition(mPopupDialog != null, "mPopupMessage has not been set in the inspector");

			//Initialise the indicator arrow
			mIndicatorArrow.SetActive( true );
			mIndicatorArrow.SetActive( false );

			mPopupView = mPopupDialog.GetComponent<PopupViewTMP>();
			SJLogger.AssertCondition(mPopupView != null, "mPopupMessage does not contain a PopupView component");
		}
		#endregion

		#region Public Methods
		public GameObject CreateBanner( string text, Vector3 offset, Transform parent )
		{
			mBanner = Instantiate( mBannerPrefab );
			SJRenderTransformExtensions.AddChild( parent, mBanner.transform, true, true, true );

			mBanner.transform.localScale = Vector3.zero;
			mBanner.transform.localEulerAngles = Vector3.zero;

			UIBanner uiBanner = mBanner.GetComponent< UIBanner >();
			uiBanner.Text = text;
			uiBanner.SetViewPosition( offset );

			PlayMakerFSM[] components = mBanner.GetComponents< PlayMakerFSM >();
			components[ 0 ].enabled = true;
			components[ 1 ].enabled = false;

			return mBanner;
		}

		public void RemoveBanner()
		{
			PlayMakerFSM[] components = mBanner.GetComponents< PlayMakerFSM >();
			components[ 0 ].enabled = false;
			components[ 1 ].enabled = true;

			mBanner = default( GameObject );
		}

		public GameObject CreateHighlightRing( GameObject gameObjectToHighlight, Vector3 offset, Vector3 rotation, Vector3 scale )
		{
			SJLogger.LogMessage(MessageFilter.UI, "Showing highlight arrow");
			GameObject highlightRing = Instantiate( mHighlightRingPrefab );

			SJRenderTransformExtensions.AddChild( gameObjectToHighlight.transform, highlightRing.transform, false, false, false );

			//Set the rings position
			highlightRing.transform.localPosition = offset;

			//Set the rings rotation
			highlightRing.transform.localEulerAngles = rotation;

			//Set the rings scale
			highlightRing.transform.localScale = scale;

			return highlightRing;
		}

		public void RemoveHighlightRing( GameObject highlightRing )
		{
			Destroy( highlightRing );
		}

		public GameObject CreateHighlightArrow( GameObject gameObjectToHighlight, Vector3 offset, Vector3 rotation, Vector3 scale )
		{
			SJLogger.LogMessage(MessageFilter.UI, "Showing highlight arrow");
			GameObject highlightArrow = Instantiate( mHighlightArrowPrefab );

			SJRenderTransformExtensions.AddChild( gameObjectToHighlight.transform, highlightArrow.transform, false, false, false );

			//Set the arrows position
			highlightArrow.transform.localPosition = offset;

			//Set the arrows rotation
			highlightArrow.transform.localEulerAngles = rotation;

			//Set the arrows scale
			highlightArrow.transform.localScale = scale;

			return highlightArrow;
		}

		public void RemoveHighlightArrow( GameObject highlightArrow )
		{
			Destroy( highlightArrow );
		}


		public void ShowIndicatorArrow( Transform startTransform, Vector3 startOffset, Transform endTransform, Vector3 endOffset )
		{
			SJLogger.LogMessage(MessageFilter.UI, "Showing indicator arrow");

			PlayMakerSharedVariables playMakerSharedVariables = mIndicatorArrow.GetComponent<PlayMakerSharedVariables>();

			playMakerSharedVariables.Add( "Parent", new FsmGameObject( BattleManager.Get.gameObject ) );

			playMakerSharedVariables.Add( "StartGameObject", new FsmGameObject( startTransform.gameObject ) );
			playMakerSharedVariables.Add( "StartOffset", new FsmVector3( startOffset ) );

			playMakerSharedVariables.Add( "EndGameObject", new FsmGameObject( endTransform.gameObject ) );
			playMakerSharedVariables.Add( "EndOffset", new FsmVector3( endOffset ) );

			mIndicatorArrow.SetActive( true );
		}

		public void HideIndicatorArrow()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Hiding indicator arrow");

			//Hide the arrow
			mIndicatorArrow.SetActive( false );
		}

		public void ShowMessagePopup(string title, string message, params ButtonData[] buttons)
		{
			SJLogger.LogMessage(MessageFilter.UI, "Opening popup {0}, {1}", title, message);

			mPopupDialog.SetActive(true);

			PopupData data = new PopupData(title, message, buttons);
			mPopupView.OnRefresh(data);

			// Listen to all buttons so we know when to close the dialog
			RegisterPopupViewListeners( mPopupView );
		}

		public void HideMessagePopup()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Hiding popup");

			// Stop listening to the buttons
			UnregisterPopupViewListeners( mPopupView );

			mPopupDialog.SetActive(false);
		}
		#endregion

		#region Event Registration
		private void RegisterPopupViewListeners( PopupViewTMP popupView )
		{
			// Listen to all buttons so we know when to close the dialog
			popupView.ButtonClicked += HandlePopupButtonClicked;
		}

		private void UnregisterPopupViewListeners( PopupViewTMP popupView )
		{
			popupView.ButtonClicked -= HandlePopupButtonClicked;
		}
		#endregion

		#region Event Handlers
		private void HandlePopupButtonClicked()
		{
			HideMessagePopup();
		}
		#endregion
	}
}
