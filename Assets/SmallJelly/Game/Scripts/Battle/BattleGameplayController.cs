using System;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class BattleGameplayController : MenuController 
	{
		#region Public Event Handlers
		public event EventHandler<SocketInformationEventArgs> TargetJettison;

		public event EventHandler<BattleEntityAndStateTransitionEventArgs> BattleEntityStateTransitioned
		{
			add
			{
				mLocalPlayerView.BattleEntityStateUpdated += value;
				mRemotePlayerView.BattleEntityStateUpdated += value;
			}

			remove
			{
				mLocalPlayerView.BattleEntityStateUpdated -= value;
				mRemotePlayerView.BattleEntityStateUpdated -= value;
			}
		}

		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> BattleEntityTargetableStateTransitioned
		{
			add
			{
				mLocalPlayerView.BattleEntityTargetableStateUpdated += value;
				mRemotePlayerView.BattleEntityTargetableStateUpdated += value;
			}

			remove
			{
				mLocalPlayerView.BattleEntityTargetableStateUpdated -= value;
				mRemotePlayerView.BattleEntityTargetableStateUpdated -= value;
			}
		}

		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> BattleEntitySelectionStateTransitioned
		{
			add
			{
				mLocalPlayerView.BattleEntitySelectionStateUpdated += value;
				mRemotePlayerView.BattleEntitySelectionStateUpdated += value;
			}

			remove
			{
				mLocalPlayerView.BattleEntitySelectionStateUpdated -= value;
				mRemotePlayerView.BattleEntitySelectionStateUpdated -= value;
			}
		}
		#endregion

		#region Unity Actions
		public UnityAction EndTurnClicked;
		public UnityAction ExitClicked;
		public UnityAction TestClicked;
		#endregion

		#region Properties
		public bool RunnerIsActive
		{
			get;
			private set;
		}

        /// <summary>
        /// True when the game has finished processing all the battle events and their animations.
        /// </summary>
        public bool AnimationsHaveFinished
        {
            get
            {
                return(!RunnerIsActive && !mData.HasNextBattleRunner());
            }
        }

		public LocalPlayerBattleView LocalPlayerBattleView
		{
			get
			{
				return mLocalPlayerView;
			}
		}

		public RemotePlayerBattleView RemotePlayerBattleView
		{
			get
			{
				return mRemotePlayerView;
			}
		}

		public BattleDisplayData Data
		{
			get
			{
				return mData;
			}
		}
		#endregion

		#region Exposed To The Inspector
        [SerializeField]
        private Button mTestButton;

		[SerializeField]
		private LocalPlayerBattleView mLocalPlayerView;

		[SerializeField]
		private RemotePlayerBattleView mRemotePlayerView;
		#endregion

		#region Member Variables
		private PlayerTargetingBattleData mLastReceivedLocalPlayerTargetableData;
		private BattleDisplayData mData = new BattleDisplayData();
		private InteractionFilter mActiveInteractionFilter;
		#endregion

		#region MonoBehaviour methods
		protected override void Awake()
		{
			base.Awake();

            SJLogger.AssertCondition(mTestButton != null, "mTestButton has not been set in the inspector");

            // Hide the test button while not testing
            //mTestButton.gameObject.SetActive(true);

			SJLogger.AssertCondition(mLocalPlayerView != null, "mLocalPlayerView has not been set in the inspector");
			SJLogger.AssertCondition(mRemotePlayerView != null, "mRemotePlayerView has not been set in the inspector");
        }

		private void Start()
		{	
			RegisterListeners();
		}

		private void Update()
		{
			//If no events are active
			if( !RunnerIsActive )
			{
				if( mData.HasNextBattleRunner() )
				{
					BattleRunner nextBattleRunner = mData.GetNextBattleRunner( mLocalPlayerView.PlayerBattleSceneView, mRemotePlayerView.PlayerBattleSceneView );
					OnStartRunning( nextBattleRunner );
				}
			}
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}

		#endregion

		#region Public Methods
		//Called when the object is created - just before OnEnable - used to construct the object with the
		//supplied configuration
		public void OnConstruct( BattleConfig battleConfig )
		{
            // TODO: If this is false, could be a hack, prevent battle being played. 
            SJLogger.AssertCondition(battleConfig.BattleId == BattleModeManager.Get.CurrentBattleId.ToString(), "BattleId mismatch");

			mLocalPlayerView.OnConstruct( battleConfig );
			mRemotePlayerView.OnConstruct( battleConfig );
		}

		public void OnRefreshConnectionProblems( bool isExperiencingProblems )
		{
			mLocalPlayerView.OnRefreshConnectionProblems( isExperiencingProblems );
		}

		public void OnStartTurn( PlayerType newPlayersTurn )
		{
			//Don't do a full view refresh on replays - it's unneccessary
			if( BattleModeManager.Get.CurrentBattleMode != BattleMode.Replay )
			{
				//Do a rollback
				OnRollback();
			}

			mLocalPlayerView.OnStartTurn( newPlayersTurn );
			mRemotePlayerView.OnStartTurn( newPlayersTurn );
		}

		public void OnTurnNearlyOver( PlayerType currentTurn, int secondsRemaining )
		{
            SJLogger.LogMessage(MessageFilter.Gameplay, "Turn nearly over for player {0}, seconds remaining {1}", currentTurn, secondsRemaining);
			switch( currentTurn )
			{
				case PlayerType.Local: 
					mLocalPlayerView.OnTurnNearlyOver( secondsRemaining );
					break;

				case PlayerType.Remote: 
					mRemotePlayerView.OnTurnNearlyOver( secondsRemaining );
					break;
			}
		}

		public void OnEndTurn( PlayerType newPlayersTurn )
		{
			mLocalPlayerView.OnEndTurn( newPlayersTurn );
			mRemotePlayerView.OnEndTurn( newPlayersTurn );
		}

		public void OnApplyInteractionFilter( InteractionFilter interactionFilter )
		{
			mActiveInteractionFilter = interactionFilter;

			interactionFilter.ApplyFilter( mData.LocalLastRunData, mData.RemoteLastRunData, mLocalPlayerView, mRemotePlayerView );
		}

		public void OnRemoveInteractionFilter()
		{
            // VJS: Temp fix for null ref ex
            if(mActiveInteractionFilter != null)
            {
			    mActiveInteractionFilter.Remove();
            }
			mActiveInteractionFilter = null;
		}

		public void OnRefreshData()
		{
			mLocalPlayerView.OnRefresh( mData.LocalLastRunData );
			mRemotePlayerView.OnRefresh( mData.RemoteLastRunData );

			//Re-apply the active interaction filter
			if(mActiveInteractionFilter != null)
			{
				InteractionFilter previousFilter = mActiveInteractionFilter;
				OnRemoveInteractionFilter();
				OnApplyInteractionFilter( previousFilter );
			}
		}

		public void OnHardResetData( PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData )
		{
			mLocalPlayerView.OnHardReset( localPlayerBattleData );
			mRemotePlayerView.OnHardReset( remotePlayerBattleData );

			mData.LocalLastRunData = localPlayerBattleData;
			mData.RemoteLastRunData = remotePlayerBattleData;

			mLocalPlayerView.OnRefreshTargets( localPlayerTargetingBattleData );
			mRemotePlayerView.OnRefreshTargets( remotePlayerTargetingBattleData );

			//Re-apply the active interaction filter
			if(mActiveInteractionFilter != null)
			{
				InteractionFilter previousFilter = mActiveInteractionFilter;
				OnRemoveInteractionFilter();
				OnApplyInteractionFilter( previousFilter );
			}
		}

		public void OnRollback()
		{
			mLocalPlayerView.OnRollback();
			mRemotePlayerView.OnRollback();


			//Re-apply the active interaction filter
			if(mActiveInteractionFilter != null)
			{
				InteractionFilter previousFilter = mActiveInteractionFilter;
				OnRemoveInteractionFilter();
				OnApplyInteractionFilter( previousFilter );
			}
		}

		//Returns the information about a given ship - this is useful for sending information to the server
		public void GetShipInformation( Ship ship, out PlayerType owner )
		{
			owner = mLocalPlayerView.PlayerBattleSceneView.PlayerBoardView.Ship == ship ? PlayerType.Local : PlayerType.Remote;
		}

		//Returns the information about a given card - this is useful for sending information to the server
		public void GetBattleEntityInformation( BattleEntity battleEntity, out PlayerType owner, out bool installed, out int index )
		{
			//Local component handling
			index = mLocalPlayerView.PlayerBattleSceneView.PlayerBoardView.IndexOf( battleEntity );
			if( index != -1 )
			{
				owner = PlayerType.Local;
				installed = true;
				return;
			}

			//Remote component handling
			index = mRemotePlayerView.PlayerBattleSceneView.PlayerBoardView.IndexOf( battleEntity );
			if( index != -1 )
			{
				owner = PlayerType.Remote;
				installed = true;
				return;
			}

			//Local card handling
			index = mLocalPlayerView.PlayerBattleSceneView.PlayerHandView.IndexOf( battleEntity );
			if( index != -1 )
			{
				owner = PlayerType.Local;
				installed = false;
				return;
			}

			//Remote card handling
			index = mRemotePlayerView.PlayerBattleSceneView.PlayerHandView.IndexOf( battleEntity );
			if( index != -1 )
			{
				owner = PlayerType.Remote;
				installed = false;
				return;
			}

			SJLogger.Assert("Battle entity was never found!");
			owner = default( PlayerType );
			installed = default( bool );
		}

		public void GetSocketInformation( Socket socket, out PlayerType owner, out int slotIndex  )
		{
			//Local
			slotIndex = Array.IndexOf( mLocalPlayerView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets, socket );
			if( slotIndex != -1 )
			{
				owner = PlayerType.Local;
				return;
			}

			//Remote
			slotIndex = Array.IndexOf( mRemotePlayerView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets, socket );
			if( slotIndex != -1 )
			{
				owner = PlayerType.Remote;
				return;
			}

			SJLogger.Assert("Slot was not found in either local or remote views");
			owner = PlayerType.None;
		}

		public void GetButtonInformation( RenderButton renderButton, out bool isJettison, out bool isEndTurn )
		{
			LocalPlayerHUD localPlayerHudView = ((LocalPlayerHUD)mLocalPlayerView.PlayerHUD);
			isJettison = renderButton == localPlayerHudView.JettisonButton;
			isEndTurn = renderButton == localPlayerHudView.EndTurnButton;
		}

		public BattleRunnerData[] GetHistoryWhere( Func< BattleRunnerData, bool > predicate )
		{
			return mData.RunnerHistory.Where( predicate ).ToArray();
		}
		#endregion

		#region Private Methods
		private void OnStartRunning( BattleRunner messageRunner )
		{
			RunnerIsActive = true;

			RegisterBattleEventListeners( messageRunner );

			messageRunner.OnStartRunning();
		}

		private void OnFinishRunning( BattleRunner battleRunner )
		{
			OnRefreshData();

			mLocalPlayerView.OnRefreshTargets( mLastReceivedLocalPlayerTargetableData );

			UnregisterBattleEventListeners( battleRunner );

			RunnerIsActive = false;
		}


		private void OnTargetJettison( BattleEntity jettisoningEntity )
		{
			int slotIndex = mLocalPlayerView.PlayerBattleSceneView.PlayerBoardView.IndexOf( jettisoningEntity );

			//If an entity is electing
			if( slotIndex != -1 )
			{
				FireTargetJettison( jettisoningEntity.Data.DrawNumber, slotIndex );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterBattleEventListeners( BattleRunner battleEvent )
		{
			battleEvent.FinishedRunning += HandleBattleEventFinished;
		}

		private void UnregisterBattleEventListeners( BattleRunner battleEvent )
		{
			battleEvent.FinishedRunning -= HandleBattleEventFinished;
		}

		private void RegisterListeners()
		{
            mTestButton.onClick.AddListener(HandleTestButtonClicked);

			mLocalPlayerView.JettisonButtonClicked += HandleJettisonButtonClicked;
			mLocalPlayerView.ExitButtonClicked += HandleExitButtonClicked;
			mLocalPlayerView.EndTurnButtonClicked += HandleEndTurnButtonClicked;

			mData.LocalPlayerTargetableDataUpdated += HandleLocalPlayerTargetableDataUpdated;
		}

		private void UnregisterListeners()
		{
            mTestButton.onClick.RemoveListener(HandleTestButtonClicked);

			mLocalPlayerView.JettisonButtonClicked -= HandleJettisonButtonClicked;
			mLocalPlayerView.ExitButtonClicked -= HandleExitButtonClicked;
			mLocalPlayerView.EndTurnButtonClicked -= HandleEndTurnButtonClicked;

			mData.LocalPlayerTargetableDataUpdated -= HandleLocalPlayerTargetableDataUpdated;
		}
		#endregion

		#region Public Event Handlers
		public void HandleViewDataRecieved(SJJson battleViewJson)
		{
			SJLogger.LogMessage( MessageFilter.Client, "View data received" );

			mData.Update( battleViewJson );			
		}

		public void HandleTurnNearlyOver( PlayerType owner, int secondsRemaining )
		{
			OnTurnNearlyOver( owner, secondsRemaining );
		}
		#endregion

		#region Private Event Handlers
		private void HandleLocalPlayerTargetableDataUpdated( PlayerTargetingBattleData playerTargetingBattleData )
		{
			mLastReceivedLocalPlayerTargetableData = playerTargetingBattleData;
			mLocalPlayerView.OnRefreshTargets( playerTargetingBattleData );
		}

		private void HandleBattleEventFinished( object o, BattleRunnerEventArgs args )
		{
			//Event has finished
			OnFinishRunning( args.BattleRunner );
		}

		private void HandleJettisonButtonClicked( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnTargetJettison( battleEntityEventArgs.BattleEntity );
		}

		private void HandleEndTurnButtonClicked( object o, EventArgs args )
		{
			SJLogger.LogMessage(MessageFilter.UI, "End turn button clicked");

			if(EndTurnClicked != null)
			{
				EndTurnClicked();
			}
		}

        // Public for automated tests
		public void HandleExitButtonClicked()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Exit button clicked");

			if(ExitClicked != null)
			{
				ExitClicked();
			}
		}

		private void HandleTestButtonClicked()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Fire button clicked");
			
			if(TestClicked != null)
			{
				TestClicked();
			}
		}
		#endregion

		#region Event Firing
		private void FireTargetJettison( int drawNumber, int slotIndex )
		{
			if( TargetJettison != null )
			{
				TargetJettison( this, new SocketInformationEventArgs( drawNumber, slotIndex ) );
			}
		}
		#endregion
	}
}