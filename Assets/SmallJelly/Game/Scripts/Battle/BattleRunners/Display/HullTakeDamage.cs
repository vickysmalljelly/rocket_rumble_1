﻿using UnityEngine;
using System;

/*
    properties["playerId"] = defendingPlayerState.id;
    properties["damageAmount"] = damage;
*/
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System.Collections.Generic;

namespace SmallJelly
{
	[Serializable]
	public class HullTakeDamage : BattleDisplayEvent 
	{
		#region Constants
		private const string DAMAGE_AMOUNT_ANIMATION_KEY = "DamageAmount";
		#endregion

		#region Member Variables
		//TODO - It actually makes sense to move this up to the parent class - just not yet since the others runners arent moved over to the new system!
		private HullTakeDamageData mHullTakeDamageData;
		#endregion

		#region Constructor
		public HullTakeDamage( HullTakeDamageData hullTakeDamageData, BattleRunnerArgs args ) : base(BattleEventType.HullTakeDamage, args) 
		{
			mHullTakeDamageData = hullTakeDamageData;
		}
		#endregion

		#region Member Variables
		private Ship mDefendingShip;
		#endregion


		#region Public Methods
		public override void OnStartRunning( )
		{
			PlayerBattleSceneView mDefendingPlayerCardView = IsLocalPlayerId( mHullTakeDamageData.PlayerId ) ? LocalSceneView : RemoteSceneView;

			mDefendingShip = mDefendingPlayerCardView.PlayerBoardView.Ship;

			mDefendingShip.OnInsertState( ShipController.ShipState.ShipTakeDamage, GetAnimationMetaData() );

			RegisterBattleEntityListeners();
		}

		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterBattleEntityListeners();

			mDefendingShip.OnInsertState( ShipController.ShipState.ShipIdle );

			base.OnFinishRunning();
		}
		#endregion

		#region Private Methods
		private AnimationMetaData GetAnimationMetaData( )
		{
			Dictionary< string, NamedVariable > animationVariables = new Dictionary< string, NamedVariable >();

			//Firing target object
			animationVariables.Add( DAMAGE_AMOUNT_ANIMATION_KEY, new FsmInt( mHullTakeDamageData.DamageAmount ) );

			return new AnimationMetaData( animationVariables );
		}
		#endregion
		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mDefendingShip.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mDefendingShip.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion


		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs eventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}
