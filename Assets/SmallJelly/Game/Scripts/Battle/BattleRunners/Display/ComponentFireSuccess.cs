﻿using UnityEngine;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System.Collections.Generic;

namespace SmallJelly
{
	[Serializable]
	public abstract class ComponentFireSuccess : BattleDisplayEvent 
	{
		#region Enums
		public enum FiringStage
		{
			None,
			Prefire,
			Fire,
			PostFire
		}
		#endregion

		#region Constants
		public const string FIRING_TARGET_OBJECT = "FiringTargetObject";
		public const string FIRING_TARGET_POINT = "FiringTargetPoint";

		public const string PROJECTILE_SHARED_VARIABLE_ID = "WeaponProjectile";
		public const string PROJECTILE_IMPACT_SHARED_VARIABLE_ID = "WeaponProjectileImpact";
		public const string PROJECTILE_TRAIL_SHARED_VARIABLE_ID = "WeaponProjectileTrail";
		public const string MUZZLE_FLASH_SHARED_VARIABLE_ID = "WeaponMuzzleFlash";
	
		public const string WEAPON_FIRE_SOUND_EFFECT_SHARED_VARIABLE_ID = "WeaponFireSoundEffect";
		public const string WEAPON_IMPACT_SOUND_EFFECT_SHARED_VARIABLE_ID = "WeaponImpactSoundEffect";
		public const string WEAPON_EXPLOSION_SOUND_EFFECT_SHARED_VARIABLE_ID = "WeaponExplosionSoundEffect";

		public const string WEAPON_TYPE_SHARED_VARIABLE_ID = "WeaponType";
		public const string NUMBER_OF_PROJECTILES_SHARED_VARIABLE_ID = "WeaponNumberOfProjectiles";
		public const string PROJECTILE_SPEED_SHARED_VARIABLE_ID = "WeaponProjectileSpeed";
		public const string PROJECTILE_FIRE_DELAY_SHARED_VARIABLE_ID = "WeaponProjectileFireDelay";
		#endregion

		#region Protected Properties
		protected string AttackingPlayerId { get; set; }
		protected int FromSlotId { get; set; }
		protected Vector3 HitPosition { get; set; }
		#endregion

		#region Member Variables
		private BattleEntity mFiringBattleEntity;
		private FiringStage mStage;
		#endregion

		#region Constructors
		protected ComponentFireSuccess(BattleRunnerArgs args, string attackingPlayerId, int fromSlotId, Vector3 hitPosition) : base(BattleEventType.FireComponent, args) 
		{
			AttackingPlayerId = attackingPlayerId;
			FromSlotId = fromSlotId;
			HitPosition = hitPosition;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.AssertCondition(LocalSceneView != null, "playerCardView cannot be null");
			SJLogger.AssertCondition(LocalBeforeData != null, "playerBattleData cannot be null");
			SJLogger.AssertCondition(RemoteSceneView != null, "remoteCardView cannot be null");
			SJLogger.AssertCondition(RemoteBeforeData != null, "remoteBattleData cannot be null");

			//If the player is attacking then assign "attackingCardView" as the players, otherwise it is the remote - the same with the data
			bool isLocalPlayer = IsLocalPlayerId( AttackingPlayerId );
			PlayerBattleSceneView attackingCardView = isLocalPlayer ? LocalSceneView : RemoteSceneView;
			PlayerBattleData attackingBattleData = isLocalPlayer ? LocalBeforeData : RemoteBeforeData;

			BattleEntityData battleEntityData = attackingBattleData.BoardCardData[ FromSlotId ];
			mFiringBattleEntity = attackingCardView.PlayerBoardView.Get( battleEntityData.DrawNumber );

			StartPrefire();
		}


		public override void OnFinishRunning()
		{
			base.OnFinishRunning();
		}
		#endregion

		#region Protected Methods
		protected abstract GameObject GetTarget();
		protected abstract Vector3 GetDefaultTargetPosition();
		#endregion

		#region Private Methods
		private void StartPrefire()
		{
			mStage = FiringStage.Prefire;
			mFiringBattleEntity.OnInsertState( BattleEntityController.State.ComponentPrefire );

			RegisterBattleEntityListeners();
		}

		private void StopPrefire()
		{
			mStage = FiringStage.None;

			UnregisterBattleEntityListeners();
		}

		private void StartFire()
		{
			mStage = FiringStage.Fire;
			mFiringBattleEntity.OnInsertState( BattleEntityController.State.ComponentFire, GetAnimationMetaData( mFiringBattleEntity ) );

			RegisterBattleEntityListeners();
		}

		private void StopFire()
		{			
			mStage = FiringStage.None;

			UnregisterBattleEntityListeners();
		}

		private void StartPostFire()
		{
			mStage = FiringStage.PostFire;
			mFiringBattleEntity.OnInsertState( BattleEntityController.State.ComponentPostfire );

			RegisterBattleEntityListeners();
		}

		private void StopPostFire()
		{
			mStage = FiringStage.None;

			UnregisterBattleEntityListeners();
		}


		private AnimationMetaData GetAnimationMetaData( BattleEntity sourceEntity )
		{
			WeaponFiringSharedVariables weaponFiringSharedVariables = sourceEntity.BattleEntityController.BattleEntityView.Parts.ComponentBattleEntityModel.WeaponFiringSharedVariables;

			Dictionary< string, NamedVariable > animationVariables = new Dictionary< string, NamedVariable >();

			if( HitPosition == Vector3.zero )
			{
				HitPosition = GetDefaultTargetPosition();
			}

			//Firing target object
			animationVariables.Add( FIRING_TARGET_OBJECT, new FsmGameObject( GetTarget() ) );

			//Firing target point
			animationVariables.Add( FIRING_TARGET_POINT, new FsmVector3( HitPosition ) );


			//Add data from the WeaponFiringSharedVariables component
			//Weapon type - currently unused in the graph but will be in the near future
			FsmString weaponTypeString = new FsmString(  );
			weaponTypeString.Value = weaponFiringSharedVariables.WeaponType;
			animationVariables.Add( WEAPON_TYPE_SHARED_VARIABLE_ID, weaponTypeString );



			//Number of projectiles
			animationVariables.Add( NUMBER_OF_PROJECTILES_SHARED_VARIABLE_ID, new FsmInt( weaponFiringSharedVariables.NumberOfProjectiles ) );

			//Projectile speed
			animationVariables.Add( PROJECTILE_SPEED_SHARED_VARIABLE_ID, new FsmFloat( weaponFiringSharedVariables.ProjectileSpeed ) );

			//Projectile file display
			animationVariables.Add( PROJECTILE_FIRE_DELAY_SHARED_VARIABLE_ID, new FsmFloat( weaponFiringSharedVariables.ProjectileFireDelay ) );



			//Projectile
			if( weaponFiringSharedVariables.Projectile != default( GameObject ) )
			{
				//Projectile itself
				animationVariables.Add( PROJECTILE_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.Projectile ) );
			}


			//Projectile impact
			if( weaponFiringSharedVariables.ProjectileImpact != default( GameObject ) )
			{
				animationVariables.Add( PROJECTILE_IMPACT_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.ProjectileImpact ) );
			}

			//Projectile trail
			if( weaponFiringSharedVariables.ProjectileTrail != default( GameObject ) )
			{
				animationVariables.Add( PROJECTILE_TRAIL_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.ProjectileTrail ) );
			}

			//Muzzle flash
			if( weaponFiringSharedVariables.MuzzleFlash != default( GameObject ) )
			{
				animationVariables.Add( MUZZLE_FLASH_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.MuzzleFlash ) );
			}


			//Fire sound effect
			if( weaponFiringSharedVariables.FireSoundEffectName != string.Empty )
			{
				FsmString weaponFireSoundEffect = new FsmString(  );
				weaponFireSoundEffect.Value = weaponFiringSharedVariables.FireSoundEffectName;
				animationVariables.Add( WEAPON_FIRE_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponFireSoundEffect );
			}

			//Impact sound effect
			if( weaponFiringSharedVariables.ImpactSoundEffectName != string.Empty )
			{
				FsmString weaponImpactSoundEffect = new FsmString(  );
				weaponImpactSoundEffect.Value = weaponFiringSharedVariables.ImpactSoundEffectName;
				animationVariables.Add( WEAPON_IMPACT_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponImpactSoundEffect );
			}

			//Explosion sound effect
			if( weaponFiringSharedVariables.ExplosionSoundEffectName != string.Empty )
			{
				FsmString weaponExplosionSoundEffect = new FsmString(  );
				weaponExplosionSoundEffect.Value = weaponFiringSharedVariables.ExplosionSoundEffectName;
				animationVariables.Add( WEAPON_EXPLOSION_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponExplosionSoundEffect );
			}

			return new AnimationMetaData( animationVariables );
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mFiringBattleEntity.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mFiringBattleEntity.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			if( mStage == FiringStage.Prefire )
			{
				StopPrefire();
				StartFire();
			}
			else if( mStage == FiringStage.Fire )
			{
				StopFire();
				StartPostFire();

				OnFinishRunning();
			}
			else if( mStage == FiringStage.PostFire )
			{
				StopPostFire();
			}
		}
		#endregion
	}
}