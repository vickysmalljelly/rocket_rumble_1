﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class ComponentJettisonFail : BattleDisplayEvent
	{
		public enum Reason
		{
			NotSet,
			NotEnoughPower
		}

		private readonly Reason mReason;

		public ComponentJettisonFail(BattleRunnerArgs args, Reason reason) : base(BattleEventType.JettisonFail, args) 
		{
			mReason = reason;
		}

		#region Public Methods
		public override void OnStartRunning()
		{
			ButtonData okButton = new ButtonData("Ok", HandleOkButton);
			switch(mReason)
			{
			case Reason.NotEnoughPower:
				DialogManager.Get.ShowMessagePopup("Not Enough Power", "You don't have enough power to jettison this component!", okButton);
				break;
			}
		}
		#endregion

		#region Private Methods
		private void HandleOkButton()
		{
			OnFinishRunning();
		}
		#endregion
	}
}