﻿using UnityEngine;
using System;

/*
 *     properties["defendingPlayerId"] = defendingPlayerId;
    properties["targetSlotId"] = targetSlotId;
 * */
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;


namespace SmallJelly
{
	[Serializable]
	public class TakeDamage : BattleDisplayEvent 
	{
		#region Member Variables
		private ComponentTakeDamageData mComponentTakeDamageData;
		#endregion

		#region Constructors
		public TakeDamage( ComponentTakeDamageData componentTakeDamageData, BattleRunnerArgs args ) : base(BattleEventType.TakeDamage, args) 
		{
			mComponentTakeDamageData = componentTakeDamageData;
		}
		#endregion

		#region Member Variables
		private BattleEntity mDefendingBattleEntity;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			//Get the defending players view
			PlayerBattleSceneView defendingPlayerView = IsLocalPlayerId( mComponentTakeDamageData.DefendingPlayerId ) ? LocalSceneView : RemoteSceneView;
			PlayerBattleData defendingPlayerBattleData = IsLocalPlayerId( mComponentTakeDamageData.DefendingPlayerId ) ? LocalBeforeData : RemoteBeforeData;

			//Make the relevant component in the defending players view take damage
			BattleEntityData defendingBattleEntityData = defendingPlayerBattleData.BoardCardData[ mComponentTakeDamageData.TargetSlotId ];
			mDefendingBattleEntity = defendingPlayerView.PlayerBoardView.Get( defendingBattleEntityData.DrawNumber );
			mDefendingBattleEntity.OnInsertState(BattleEntityController.State.ComponentTakeDamage, GetAnimationMetaData() );

			RegisterBattleEntityListeners();
		}


		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterBattleEntityListeners();

			mDefendingBattleEntity.OnReturnToPreviousState();

			base.OnFinishRunning();
		}
		#endregion

		#region Private Methods
		private AnimationMetaData GetAnimationMetaData( )
		{
			Dictionary< string, NamedVariable > data = new Dictionary< string, NamedVariable > ();
			data.Add( "DamageAmount", new FsmInt( mComponentTakeDamageData.DamageAmount ) );
			return new AnimationMetaData( data );
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mDefendingBattleEntity.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mDefendingBattleEntity.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}
