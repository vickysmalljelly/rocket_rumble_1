﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class ComponentBuff : BattleDisplayEvent 
	{
		public ComponentBuff(BattleRunnerArgs args, string playerId, int slotId) : base(BattleEventType.Buff, args) 
		{
			mPlayerId = playerId;
			mSlotId = slotId;
		}

		#region Member Variables
		private readonly string mPlayerId;
		private readonly int mSlotId;

		private BattleEntity mBattleEntityToBuff;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.LogMessage( MessageFilter.George, "Buff Success : Player Id {0}, Slot Id {1}", mPlayerId, mSlotId );

			bool isLocalPlayer = IsLocalPlayerId( mPlayerId );

			//Get the scene view of the player who is buffing
			PlayerBattleSceneView buffingBattleSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;

			//Get the battle entity we're buffing
			mBattleEntityToBuff = buffingBattleSceneView.PlayerBoardView.BattleEntities[ mSlotId ];

			SJLogger.AssertCondition( mBattleEntityToBuff != null, "The buffing slot ({0}) should contain a component", mSlotId );
		
			//Set the state of the battle entity that we're buffing
			mBattleEntityToBuff.OnInsertState( BattleEntityController.State.ComponentBuff );
		
			//Register listeners for the "animation completed" event
			RegisterBattleEntityListeners();
		}


		public override void OnFinishRunning()
		{
			//Unregister listeners for the "animation completed" event
			UnregisterBattleEntityListeners();

			//Return to the state before the buff animation started playing
			mBattleEntityToBuff.OnReturnToPreviousState();

			base.OnFinishRunning();
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mBattleEntityToBuff.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mBattleEntityToBuff.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}