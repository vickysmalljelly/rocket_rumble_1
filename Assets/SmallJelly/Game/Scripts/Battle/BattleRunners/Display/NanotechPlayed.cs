﻿using UnityEngine;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	[Serializable]
	public class NanotechPlayed : PlayCardSuccess 
	{
		#region Member Variables
		private readonly string mPlayerId;
		private readonly int mDrawNumber;

		private BattleEntity mCrewCard;
		#endregion

		#region Constructor
		public NanotechPlayed( NanotechPlayedData nanotechPlayedData, BattleRunnerArgs args ) : base( BattleEventType.Empty, args, nanotechPlayedData.PlayerId ) 
		{
			mPlayerId = nanotechPlayedData.PlayerId;
			mDrawNumber = nanotechPlayedData.DrawNumber;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			base.OnStartRunning ();

			bool isLocalPlayer = IsLocalPlayerId( mPlayerId );

			//Get the scene view of the player who is playing the crew card
			PlayerBattleSceneView sourcePlayerSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;
			PlayerBattleSceneView opponentPlayerSceneView = !isLocalPlayer ? LocalSceneView : RemoteSceneView;

			//Get the battle entity we're playing
			mCrewCard = sourcePlayerSceneView.PlayerHandView.Get( mDrawNumber );

			//Set the state of the card that we're playing
			mCrewCard.OnInsertState( BattleEntityController.State.CardDissapearing, GetAnimationMetaData( sourcePlayerSceneView, opponentPlayerSceneView ) );

			//Register listeners for the "animation completed" event
			RegisterBattleEntityListeners();		
		}

		public override void OnFinishRunning( )
		{	
			UnregisterBattleEntityListeners ();

			base.OnFinishRunning();
		}
		#endregion

		#region Private Methods
		private AnimationMetaData GetAnimationMetaData( PlayerBattleSceneView sourcePlayerSceneView, PlayerBattleSceneView opponentPlayerSceneView )
		{
			Dictionary< string, NamedVariable > data = new Dictionary< string, NamedVariable > ();
			data.Add( "SourcePlayersShip", new FsmGameObject( sourcePlayerSceneView.PlayerBoardView.Ship.gameObject ) );
			data.Add( "OpponentPlayersShip", new FsmGameObject( opponentPlayerSceneView.PlayerBoardView.Ship.gameObject ) );

			return new AnimationMetaData( data );
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mCrewCard.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mCrewCard.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}