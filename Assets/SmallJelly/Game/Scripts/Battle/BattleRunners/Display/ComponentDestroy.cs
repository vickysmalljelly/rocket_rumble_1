﻿using UnityEngine;
using System;

/*
    properties["defendingPlayerId"] = defendingPlayerId;
    properties["targetSlotId"] = targetSlotId;
 * */
using SmallJelly.Framework;


namespace SmallJelly
{
	[Serializable]
	public class ComponentDestroy : BattleDisplayEvent 
	{
		public ComponentDestroy(BattleRunnerArgs args, string defendingPlayerId, int targetSlotId) : base(BattleEventType.DestroyComponent, args) 
		{
			DefendingPlayerId = defendingPlayerId;
			TargetSlotId = targetSlotId;
		}

		#region Public Variables
		public string DefendingPlayerId;
		public int TargetSlotId;
		#endregion

		#region Member Variables
		private BattleEntity mDestroyingBattleEntity;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			//Get the destroying players view
			PlayerBattleSceneView defendingPlayerView = IsLocalPlayerId( DefendingPlayerId ) ? LocalSceneView : RemoteSceneView;
			PlayerBattleData defendingPlayerBattleData = IsLocalPlayerId( DefendingPlayerId ) ? LocalBeforeData : RemoteBeforeData;

			BattleEntityData defendingBattleEntityData = defendingPlayerBattleData.BoardCardData[ TargetSlotId ];
			mDestroyingBattleEntity = defendingPlayerView.PlayerBoardView.Get( defendingBattleEntityData.DrawNumber );
			mDestroyingBattleEntity.OnInsertState(BattleEntityController.State.ComponentDestroying);

			RegisterBattleEntityListeners();
		}

		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterBattleEntityListeners();

			//Component is now destroyed
			mDestroyingBattleEntity.OnInsertState(BattleEntityController.State.ComponentDestroyed);

			base.OnFinishRunning();
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mDestroyingBattleEntity.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mDestroyingBattleEntity.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}
