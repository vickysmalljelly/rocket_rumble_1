﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public abstract class BattleDisplayEvent : BattleRunner
	{
		#region Constructors
		public BattleDisplayEvent(BattleEventType eventType, BattleRunnerArgs args) : base( eventType, args ){}
		#endregion

		#region Public Methods
		public override void OnStartRunning( ){}
		#endregion
	}
}