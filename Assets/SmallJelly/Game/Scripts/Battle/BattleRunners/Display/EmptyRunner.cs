﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class EmptyRunner : BattleDisplayEvent 
	{
		public EmptyRunner(BattleRunnerArgs args) : base(BattleEventType.Empty, args) 
		{
		}

		#region Public Methods
		public override void OnStartRunning( )
		{
			OnFinishRunning();
		}

		public override void OnFinishRunning()
		{	
			base.OnFinishRunning();
		}
		#endregion
	}
}