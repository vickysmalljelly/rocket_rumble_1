﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class OutOfCards : BattleDisplayEvent 
	{
		#region Constants
		private const string OUT_OF_CARDS_TITLE = "Out of cards!";
		private const string OUT_OF_CARDS_MESSAGE = "Cargo Hold Empty! No more cards left to draw";
		#endregion

		#region Member Variables
		private string mPlayerId;
		#endregion

		#region Constructor
		public OutOfCards( BattleRunnerArgs args, string playerId ) : base(BattleEventType.Empty, args) 
		{
			mPlayerId = playerId;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			base.OnStartRunning ();

			//If it's the local player which is involved in out of cards AND we're NOT in a scripted tutorial
			//then display a popup, otherwise terminate immediately
			if( IsLocalPlayerId( mPlayerId ) && !BattleManager.Get.IsScriptedTutorial() )
			{
				ButtonData buttonData = new ButtonData( "Ok", HandleDialogDismissed );
				DialogManager.Get.ShowMessagePopup( OUT_OF_CARDS_TITLE, OUT_OF_CARDS_MESSAGE, buttonData );
			}
			else
			{
				OnFinishRunning();
			}
		}

		public override void OnFinishRunning()
		{	
			base.OnFinishRunning();
		}
		#endregion

		#region Event Handlers
		private void HandleDialogDismissed()
		{
			OnFinishRunning();
		}
		#endregion
	}
}