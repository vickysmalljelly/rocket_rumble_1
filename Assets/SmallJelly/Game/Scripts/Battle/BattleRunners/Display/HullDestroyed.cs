﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class HullDestroyed : BattleDisplayEvent 
	{
		#region Member Variables
		private HullDestroyedData mHullDestroyedData;
		private Ship mWonShip;
		private Ship mLostShip;

		private bool mWonAnimationFinished;
		private bool mLostAnimationFinished;
		#endregion


		public HullDestroyed( HullDestroyedData hullDestroyedData, BattleRunnerArgs battleRunnerArgs ) : base(BattleEventType.HullDestroyed, battleRunnerArgs) 
		{
			mHullDestroyedData = hullDestroyedData;
		}
			
	
		#region Public Methods
		public override void OnStartRunning( )
		{
			PlayerBattleSceneView wonPlayerCardView = !IsLocalPlayerId( mHullDestroyedData.PlayerId ) ? LocalSceneView : RemoteSceneView;
			PlayerBattleSceneView lostPlayerCardView = IsLocalPlayerId( mHullDestroyedData.PlayerId ) ? LocalSceneView : RemoteSceneView;

			mWonShip = wonPlayerCardView.PlayerBoardView.Ship;
			mWonShip.OnInsertState( ShipController.ShipState.ShipWon );

			mLostShip = lostPlayerCardView.PlayerBoardView.Ship;
			mLostShip.OnInsertState( ShipController.ShipState.ShipLost );

			RegisterBattleEntityListeners();
		}

		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterBattleEntityListeners();

			base.OnFinishRunning();
		}
		#endregion

		#region Private Methods
		private void CheckAnimationFinished()
		{
			if( mWonAnimationFinished && mLostAnimationFinished )
			{
				OnFinishRunning();
			}
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mLostShip.AnimationFinished += HandleLostAnimationFinished;
			mWonShip.AnimationFinished += HandleWonAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mLostShip.AnimationFinished -= HandleLostAnimationFinished;
			mWonShip.AnimationFinished -= HandleWonAnimationFinished;
		}
		#endregion


		#region Event Handlers
		private void HandleWonAnimationFinished( object o, EventArgs eventArgs )
		{
			mWonAnimationFinished = true;
			CheckAnimationFinished();
		}

		private void HandleLostAnimationFinished( object o, EventArgs eventArgs )
		{
			mLostAnimationFinished = true;
			CheckAnimationFinished();
		}
		#endregion

	}
}
