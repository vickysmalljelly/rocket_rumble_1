﻿using System;
using System.Text;

using UnityEngine;

namespace SmallJelly
{
	[Serializable]
	public class UpdatePower : BattleDisplayEvent 
	{
		public UpdatePower(BattleRunnerArgs args, string playerId) : base(BattleEventType.UpdatePower, args) 
		{
			PlayerId = playerId;
		}

		// The id of the player whose power is to be updated
		public string PlayerId;

		#region Public Methods
		public override void OnStartRunning()
		{
			OnFinishRunning();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append(base.ToString());
			builder.Append(string.Format("[UpdatePower: PlayerId={0}]", PlayerId));

			return builder.ToString();
		}
		#endregion
	}
}