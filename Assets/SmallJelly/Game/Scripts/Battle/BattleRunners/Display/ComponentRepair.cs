﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class ComponentRepair : BattleDisplayEvent 
	{
		public ComponentRepair(BattleRunnerArgs args, string playerId, int slotId) : base(BattleEventType.Repair, args) 
		{
			mPlayerId = playerId;
			mSlotId = slotId;
		}

		#region Member Variables
		private readonly string mPlayerId;
		private readonly int mSlotId;

		private BattleEntity mBattleEntityToRepair;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.LogMessage( MessageFilter.George, "Repair Success : Player Id {0}, Slot Id {1}", mPlayerId, mSlotId );

			bool isLocalPlayer = IsLocalPlayerId( mPlayerId );

			//Get the scene view of the player who is repairing
			PlayerBattleSceneView repairingBattleSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;

			//Get the battle entity we're repairing
			mBattleEntityToRepair = repairingBattleSceneView.PlayerBoardView.BattleEntities[ mSlotId ];

			SJLogger.AssertCondition( mBattleEntityToRepair != null, "The repairing slot ({0}) should contain a component", mSlotId );
		
			//Set the state of the battle entity that we're repairing
			mBattleEntityToRepair.OnInsertState( BattleEntityController.State.ComponentRepair );
		
			//Register listeners for the "animation completed" event
			RegisterBattleEntityListeners();
		}


		public override void OnFinishRunning()
		{
			//Unregister listeners for the "animation completed" event
			UnregisterBattleEntityListeners();

			//Return to the state before the repair animation started playing
			mBattleEntityToRepair.OnReturnToPreviousState();

			base.OnFinishRunning();
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mBattleEntityToRepair.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mBattleEntityToRepair.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}