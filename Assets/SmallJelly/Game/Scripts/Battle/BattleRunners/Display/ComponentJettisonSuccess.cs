﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class ComponentJettisonSuccess : BattleDisplayEvent 
	{
		public ComponentJettisonSuccess( ComponentJettisonData componentJettisonData, BattleRunnerArgs args ) : base(BattleEventType.JettisonSuccess, args) 
		{
			mComponentJettisonData = componentJettisonData;
		}

		#region Member Variables
		private readonly ComponentJettisonData mComponentJettisonData;

		private BattleEntity mBattleEntityToJettison;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.LogMessage( MessageFilter.George, "Jettision Success : Player Id {0}, Socket Id {1}", mComponentJettisonData.PlayerId, mComponentJettisonData.SocketId );

			bool isLocalPlayer = IsLocalPlayerId( mComponentJettisonData.PlayerId );

			//Get the scene view of the player who is jettisoning
			PlayerBattleSceneView jettisoningBattleSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;

			//Get the battle entity we're jettisoning
			mBattleEntityToJettison = jettisoningBattleSceneView.PlayerBoardView.BattleEntities[ mComponentJettisonData.SocketId ];

			SJLogger.AssertCondition( mBattleEntityToJettison != null, "The jettisoning socket ({0}) should contain a component", mComponentJettisonData.SocketId );
		
			//Set the state of the battle entity that we're jettisoning
			mBattleEntityToJettison.OnInsertState( BattleEntityController.State.ComponentJettison );
		
			//Register listeners for the "animation completed" event
			RegisterBattleEntityListeners();
		}


		public override void OnFinishRunning()
		{
			//Unregister listeners for the "animation completed" event
			UnregisterBattleEntityListeners();

			base.OnFinishRunning();
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mBattleEntityToJettison.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mBattleEntityToJettison.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}