﻿using UnityEngine;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	[Serializable]
	public abstract class CardTextFireSuccess : BattleDisplayEvent 
	{
		#region Constants
		public const string FIRING_TARGET_OBJECT = "FiringTargetObject";
		public const string FIRING_TARGET_POINT = "FiringTargetPoint";
		#endregion

		#region Protected Properties
		protected string AttackingPlayerId { get; set; }
		protected string DefendingPlayerId { get; set; }
		protected int FromSocketId { get; set; }
		#endregion

		#region Member Variables
		private Socket mFiringSocket;
		#endregion

		#region Constructors
		protected CardTextFireSuccess( BattleRunnerArgs args, string attackingPlayerId, string defendingPlayerId, int fromSlotId ) : base(BattleEventType.FireComponent, args) 
		{
			AttackingPlayerId = attackingPlayerId;
			DefendingPlayerId = defendingPlayerId;
			FromSocketId = fromSlotId;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.AssertCondition(LocalSceneView != null, "playerCardView cannot be null");
			SJLogger.AssertCondition(LocalBeforeData != null, "playerBattleData cannot be null");
			SJLogger.AssertCondition(RemoteSceneView != null, "remoteCardView cannot be null");
			SJLogger.AssertCondition(RemoteBeforeData != null, "remoteBattleData cannot be null");

			//If the player is attacking then assign "attackingCardView" as the players, otherwise it is the remote - the same with the data
			bool isLocalPlayer = IsLocalPlayerId( AttackingPlayerId );
			PlayerBattleSceneView attackingCardView = isLocalPlayer ? LocalSceneView : RemoteSceneView;

			mFiringSocket = attackingCardView.PlayerBoardView.Ship.BattleEntitySockets[ FromSocketId ];
			mFiringSocket.OnInsertGameplayState( SocketController.GameplayState.Firing, GetAnimationMetaData() );

			RegisterSocketListeners();
		}


		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterSocketListeners();

			mFiringSocket.OnInsertGameplayState( SocketController.GameplayState.None );

			base.OnFinishRunning();
		}
		#endregion

		#region Protected Methods
		protected abstract GameObject GetTarget();
		#endregion

		#region Private Methods
		private AnimationMetaData GetAnimationMetaData( )
		{
			Dictionary< string, NamedVariable > animationVariables = new Dictionary< string, NamedVariable >();

			//Firing target object
			animationVariables.Add( FIRING_TARGET_OBJECT, new FsmGameObject( GetTarget() ) );

			//Firing target point
			animationVariables.Add( FIRING_TARGET_POINT, new FsmVector3( Vector3.zero ) );


			return new AnimationMetaData( animationVariables );
		}
		#endregion

		#region Event Listeners
		private void RegisterSocketListeners()
		{
			mFiringSocket.GameplayAnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterSocketListeners()
		{
			mFiringSocket.GameplayAnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, SocketEventArgs socketEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}