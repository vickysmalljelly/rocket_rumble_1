﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class PlayCardSuccess : BattleDisplayEvent
	{
		#region Member Variables
		private readonly string mPlayerId;
		#endregion

		#region Constructors
		public PlayCardSuccess( BattleEventType eventType, BattleRunnerArgs args, string playerId ) : base( eventType, args )
		{
			mPlayerId = playerId;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			base.OnStartRunning();

			//Only log events for the local player
			bool isLocalPlayer = IsLocalPlayerId( mPlayerId );
			if( isLocalPlayer )
			{
				FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult.Success );
			}
		}
		#endregion

		#region Analytics Methods
		private void FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult playCardResult )
		{
			AnalyticsManager.Get.RecordPlayCard( playCardResult );
		}
		#endregion
	}
}