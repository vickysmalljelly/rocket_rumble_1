﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class RemotePlayCardRunner : BattleDisplayEvent
	{
		#region Member Variables
		private readonly int mDrawNumber;

		private BattleEntity mBattleEntity;
		#endregion

		#region Constructors
		public RemotePlayCardRunner( int drawNumber, BattleRunnerArgs args ) : base( BattleEventType.RemotePlayCard, args )
		{
			mDrawNumber = drawNumber;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			base.OnStartRunning();

			mBattleEntity = RemoteSceneView.PlayerHandView.Get( mDrawNumber );

			SJLogger.AssertCondition( mBattleEntity != null, "Opponents hand must contain a battle entity with the draw number {0}", mDrawNumber );

			RegisterBattleEntityListeners();	

			mBattleEntity.OnInsertState( BattleEntityController.State.CardPlaying );
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			UnregisterBattleEntityListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterBattleEntityListeners()
		{
			mBattleEntity.AnimationFinished += HandleBattleEntityAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mBattleEntity.AnimationFinished -= HandleBattleEntityAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleBattleEntityAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}