﻿using UnityEngine;
using System.Collections;
using SmallJelly;
using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	[Serializable]
	public class ComponentFireAtHullSuccess : ComponentFireSuccess
	{
		#region Constants
		private const string CARD_TEXT_HIT_POSITION_NAME = "CardTextHitPosition";
		#endregion

		#region Public Methods
		public ComponentFireAtHullSuccess(BattleRunnerArgs args, string attackingPlayerId, int fromSlotId, Vector3 hitPosition) : base(args, attackingPlayerId, fromSlotId, hitPosition) 
		{
		}

		public override void OnStartRunning( )
		{
			base.OnStartRunning();
		}
		#endregion

		#region Private Methods
		protected override GameObject GetTarget()
		{

			bool isLocalPlayer = IsLocalPlayerId( AttackingPlayerId );
			PlayerBattleSceneView defendingPlayerSceneView = isLocalPlayer ? RemoteSceneView : LocalSceneView;
			Ship ship = defendingPlayerSceneView.PlayerBoardView.Ship;

			return ship.gameObject;
		}

		protected override Vector3 GetDefaultTargetPosition()
		{
			bool isLocalPlayer = IsLocalPlayerId( AttackingPlayerId );
			PlayerBattleSceneView defendingPlayerSceneView = isLocalPlayer ? RemoteSceneView : LocalSceneView;
			Ship ship = defendingPlayerSceneView.PlayerBoardView.Ship;

			return SJRenderTransformExtensions.FindDeepChild( ship.transform, CARD_TEXT_HIT_POSITION_NAME ).localPosition;
		}
		#endregion
	}
}