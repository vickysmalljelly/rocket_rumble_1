﻿using UnityEngine;
using System.Collections;
using System;


namespace SmallJelly
{
	[Serializable]
	public class CardTextFireAtComponentSuccess : CardTextFireSuccess
	{
		public int ToSocketId;

		public CardTextFireAtComponentSuccess(BattleRunnerArgs args, string attackingPlayerId, int fromSlotId, string defendingPlayerId, int toSlotId ) : base( args, attackingPlayerId, defendingPlayerId, fromSlotId ) 
		{
			ToSocketId = toSlotId;
		}

		public override void OnStartRunning( )
		{
			base.OnStartRunning();
		}

		protected override GameObject GetTarget()
		{
			bool isLocalPlayer = IsLocalPlayerId( DefendingPlayerId );
			PlayerBattleSceneView defendingPlayerSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;
			Socket socket = defendingPlayerSceneView.PlayerBoardView.Ship.BattleEntitySockets[ ToSocketId ];
			return socket.gameObject;
		}
	}
}