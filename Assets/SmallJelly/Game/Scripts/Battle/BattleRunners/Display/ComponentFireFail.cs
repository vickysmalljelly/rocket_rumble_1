﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class ComponentFireFail : BattleDisplayEvent 
	{
		public ComponentFireFail(BattleRunnerArgs args, string attackingPlayerId, int fromSlotId) : base(BattleEventType.FireComponent, args) 
		{
			AttackingPlayerId = attackingPlayerId;
			FromSlotId = fromSlotId;
		}

		public string AttackingPlayerId;
		public int FromSlotId;


		#region Public Methods
		public override void OnStartRunning( )
		{
			SJLogger.AssertCondition(LocalSceneView != null, "LocalSceneView cannot be null");
			SJLogger.AssertCondition(LocalBeforeData != null, "playerBattleData cannot be null");
			SJLogger.AssertCondition(RemoteSceneView != null, "remoteCardView cannot be null");
			SJLogger.AssertCondition(RemoteBeforeData != null, "remoteBattleData cannot be null");

			//If the player is attacking then assign "attackingCardView" as the players, otherwise it is the remote - the same with the data
			bool isLocalPlayer = IsLocalPlayerId( AttackingPlayerId );

			if( isLocalPlayer )
			{
				ButtonData okButton = new ButtonData("Ok", HandleOkButton);
				DialogManager.Get.ShowMessagePopup("Not Ready To Fire", "The component isn't ready to fire!", okButton);
			}
		}


		public override void OnFinishRunning()
		{
			base.OnFinishRunning();
		}
		#endregion

		#region Event Handlers
		private void HandleOkButton()
		{
			OnFinishRunning();
		}
		#endregion
	}
}