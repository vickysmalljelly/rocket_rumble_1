﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class PlayCardFail : BattleDisplayEvent
	{
		public enum Reason
		{
			NotSet,
			SlotIsFull,
			NotEnoughPower
		}

		private readonly Reason mReason;
		private readonly int mDrawNumber;

		public PlayCardFail( BattleRunnerArgs args, string playerId, int drawNumber, int slotId, Reason reason ) : base( BattleEventType.PlayCardFail, args ) 
		{
			mReason = reason;
			mDrawNumber = drawNumber;
		}

		#region Public Methods
		public override void OnStartRunning( )
		{
            //SJLogger.AssertCondition(IsLocalPlayerId( mPlayerId ), "We should only get play card fail messages for the local player");

			// Return the card to the hand
			LocalSceneView.PlayerHandView.Get( mDrawNumber ).OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );
		
			ButtonData okButton = new ButtonData( "Ok", HandleOkButton );
			switch(mReason)
			{
				case Reason.NotEnoughPower:
					DialogManager.Get.ShowMessagePopup( "Not Enough Power", "You don't have enough power to play this card!", okButton );
					
					FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult.NotEnoughPower );
				break;
                
                case Reason.SlotIsFull:
                    SJLogger.LogMessage( "Slot is full.  Should only get this message if there is hackery involved" );
                    DialogManager.Get.ShowMessagePopup( "Failed to install component", "Slot is full", okButton );
                    
					FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult.Other );
				break;

				default:
                    SJLogger.LogError( "Play card fail reason {0} not recognised", mReason );

					FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult.Other );
				    break;
			}
			
            OnFinishRunning();
		}

		public override void OnFinishRunning( )
		{
			base.OnFinishRunning();
		}
		#endregion

		#region Event Handlers
		private void HandleOkButton(){}
		#endregion

		#region Analytics Methods
		private void FireRecordPlayCardAnalytic( AnalyticsManager.PlayCardResult playCardResult )
		{
			AnalyticsManager.Get.RecordPlayCard( playCardResult );
		}
		#endregion
	}
}