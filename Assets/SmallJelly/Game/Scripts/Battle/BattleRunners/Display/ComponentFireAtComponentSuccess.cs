﻿using UnityEngine;
using System.Collections;
using System;


namespace SmallJelly
{
	[Serializable]
	public class ComponentFireAtComponentSuccess : ComponentFireSuccess
	{
		#region Member Variables
		private string mDefendingPlayerId;
		private int mToSockedId;
		#endregion

		#region Public Methods
		public ComponentFireAtComponentSuccess(BattleRunnerArgs args, string attackingPlayerId, int fromSlotId, string defendingPlayerId, int toSlotId, Vector3 hitPosition ) : base(args, attackingPlayerId, fromSlotId, hitPosition) 
		{
			mDefendingPlayerId = defendingPlayerId;
			mToSockedId = toSlotId;
		}

		public override void OnStartRunning( )
		{
			base.OnStartRunning();
		}
		#endregion

		#region Protected Methods
		protected override GameObject GetTarget()
		{
			bool isLocalPlayer = IsLocalPlayerId( mDefendingPlayerId );
			PlayerBattleSceneView defendingPlayerSceneView = isLocalPlayer ? LocalSceneView : RemoteSceneView;
			BattleEntity battleEntity = defendingPlayerSceneView.PlayerBoardView.BattleEntities[ mToSockedId ];
			return battleEntity.gameObject;
		}

		protected override Vector3 GetDefaultTargetPosition()
		{
			return Vector3.zero;
		}
		#endregion
	}
}