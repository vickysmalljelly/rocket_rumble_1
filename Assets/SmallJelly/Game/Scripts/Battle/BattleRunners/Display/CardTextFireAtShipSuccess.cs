﻿using UnityEngine;
using System.Collections;
using System;


namespace SmallJelly
{
	[Serializable]
	public class CardTextFireAtHullSuccess : CardTextFireSuccess
	{
		#region Constants
		private const string CARD_TEXT_HIT_POSITION_NAME = "CardTextHitPosition";
		#endregion

		public CardTextFireAtHullSuccess(BattleRunnerArgs args, string attackingPlayerId, string defendingPlayerId, int fromSlotId ) : base( args, attackingPlayerId, defendingPlayerId, fromSlotId ) 
		{
		}

		public override void OnStartRunning( )
		{
			base.OnStartRunning();
		}

		protected override GameObject GetTarget()
		{
			bool isLocalPlayer = IsLocalPlayerId( DefendingPlayerId );
			PlayerBattleSceneView defendingPlayerSceneView = isLocalPlayer ?  LocalSceneView : RemoteSceneView;
			Ship ship = defendingPlayerSceneView.PlayerBoardView.Ship;

			return SJRenderTransformExtensions.FindDeepChild( ship.transform, CARD_TEXT_HIT_POSITION_NAME ).gameObject;
		}
	}
}