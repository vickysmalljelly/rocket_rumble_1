﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class HUDBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mPassLocalHUD;
		private readonly bool mPassRemoteHUD;
		#endregion

		#region Constructor
		public HUDBattleFilter( bool passLocalHUD, bool passRemoteHUD )
		{
			mPassLocalHUD = passLocalHUD;
			mPassRemoteHUD = passRemoteHUD;
		}
		#endregion

		#region Public Methods
		public override void GetHUDs( PlayerHUD[] sourceEntities, out PlayerHUD[] passedConstraint, out PlayerHUD[] failedConstraint )
		{
			List< PlayerHUD > passedConstraintList = new List< PlayerHUD >();
			List< PlayerHUD > failedConstraintList = new List< PlayerHUD >();

			foreach( PlayerHUD playerHUD in sourceEntities )
			{
				if( playerHUD is LocalPlayerHUD )
				{
					if( mPassLocalHUD )
					{
						passedConstraintList.Add( playerHUD );
					}
					else
					{
						failedConstraintList.Add( playerHUD );
					}
				}
				else if( playerHUD is RemotePlayerHUD )
				{
					if( mPassRemoteHUD )
					{
						passedConstraintList.Add( playerHUD );
					}
					else
					{
						failedConstraintList.Add( playerHUD );
					}
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}