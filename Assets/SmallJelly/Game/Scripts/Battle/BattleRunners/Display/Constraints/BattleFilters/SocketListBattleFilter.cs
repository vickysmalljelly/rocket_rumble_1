﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	//Passes battle entities and sockets associated with the given socket slots
	public class SocketListBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly List<int> mLocalFilteredSlotNumbers;
		private readonly List<int> mRemoteFilteredSlotNumbers;
		#endregion

		#region Constructor
		public SocketListBattleFilter( List<int> localFilteredSlotNumbers, List<int> remoteFilteredSlotNumbers )
		{
			mLocalFilteredSlotNumbers = localFilteredSlotNumbers;
			mRemoteFilteredSlotNumbers = remoteFilteredSlotNumbers;
		}
		#endregion

		#region Public Methods
		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			List<Socket> passedConstraintList = new List<Socket>();
			List<Socket> failedConstraintList = new List<Socket>();

			foreach( Socket socket in sourceEntities )
			{
				PlayerType playerType;
				int socketIndex;
				BattleManager.Get.GetSocketInformation( socket, out playerType, out socketIndex );

				if( playerType == PlayerType.Local )
				{
					if( mLocalFilteredSlotNumbers.Contains( socketIndex ) )
					{
						passedConstraintList.Add( socket );
					}
					else
					{
						failedConstraintList.Add( socket );
					}
				}
				else
				{
					if( mRemoteFilteredSlotNumbers.Contains( socketIndex ) )
					{
						passedConstraintList.Add( socket );
					}
					else
					{
						failedConstraintList.Add( socket );
					}
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion

	}
}