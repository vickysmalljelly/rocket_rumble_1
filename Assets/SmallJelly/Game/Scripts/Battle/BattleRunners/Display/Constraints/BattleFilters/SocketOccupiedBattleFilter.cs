﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class SocketOccupiedBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mAllowOccupied;
		private readonly bool mAllowUnoccupied;
		#endregion

		#region Constructor
		public SocketOccupiedBattleFilter( bool allowOccupied, bool allowUnoccupied )
		{
			mAllowOccupied = allowOccupied;
			mAllowUnoccupied = allowUnoccupied;
		}
		#endregion

		#region Public Methods
		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			List<Socket> passedConstraintList = new List<Socket>();
			List<Socket> failedConstraintList = new List<Socket>();

			foreach( Socket socket in sourceEntities )
			{
				SocketData socketData = GetMasterDataForSocket( masterSocketData, socket );
				BattleEntityData battleEntityData = GetBattleEntityDataInSocket( socketData, masterBoardData );

				if( battleEntityData != null && mAllowOccupied )
				{
					passedConstraintList.Add( socket );
					continue;
				}

				if( battleEntityData == null && mAllowUnoccupied )
				{
					passedConstraintList.Add( socket );
					continue;
				}

				failedConstraintList.Add( socket );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}