﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class SocketInteractionFilterResult : FilterStateResult
	{
		#region Public Properties
		public SocketController.TargetableState PassedInteractionState
		{
			get
			{
				return mPassedInteractionState;
			}
		}

		public SocketController.TargetableState FailedInteractionState
		{
			get
			{
				return mFailedInteractionState;
			}
		}
		#endregion

		#region Member Variables
		private readonly SocketController.TargetableState mPassedInteractionState;
		private readonly SocketController.TargetableState mFailedInteractionState;
		#endregion

		#region Constructor
		public SocketInteractionFilterResult( SocketController.TargetableState passedInteractionState, SocketController.TargetableState failedInteractionState )
		{
			mPassedInteractionState = passedInteractionState;
			mFailedInteractionState = failedInteractionState;
		}
		#endregion
	}
}