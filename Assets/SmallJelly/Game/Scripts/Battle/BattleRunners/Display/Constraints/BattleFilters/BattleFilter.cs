﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace SmallJelly
{
	public abstract class BattleFilter
	{
		#region Public Methods
		public virtual void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new BattleEntity[0];

		}
		public virtual void GetShips( ShipBattleData[] masterShipData, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Ship[] sourceEntities, out Ship[] passedConstraint, out Ship[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new Ship[0];
		}

		public virtual void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new Socket[0];
		}

		public virtual void GetEndTurnButton( RenderButton[] sourceEntities, out RenderButton[] passedConstraint, out RenderButton[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new RenderButton[0];
		}

		public virtual void GetJettisonButton( RenderButton[] sourceEntities, out RenderButton[] passedConstraint, out RenderButton[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new RenderButton[0];
		}

		public virtual void GetHUDs( PlayerHUD[] sourceEntities, out PlayerHUD[] passedConstraint, out PlayerHUD[] failedConstraint )
		{
			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = sourceEntities;
			failedConstraint = new PlayerHUD[0];
		}
		#endregion

		#region Protected Methods
		public BattleEntityData GetMasterDataForBattleEntity( BattleEntityData[] masterBattleEntityData, BattleEntity battleEntity )
		{
			return masterBattleEntityData.FirstOrDefault( x => x != null && battleEntity != null && x.DrawNumber == battleEntity.Data.DrawNumber );
		}

		protected ShipBattleData GetMasterDataForShip( ShipBattleData[] masterShipBattleData, Ship ship )
		{
			return masterShipBattleData.FirstOrDefault( x => x != null && ship != null && x.Player == ship.ShipData.Player );
		}

		protected SocketData GetMasterDataForSocket( SocketData[] masterSocketData, Socket socket )
		{
			return masterSocketData.FirstOrDefault( x => x != null && socket != null && x.SocketId == socket.SocketData.SocketId && x.Player == socket.SocketData.Player );
		}

		protected BattleEntityData GetBattleEntityDataInSocket( SocketData masterSocketData, BattleEntityData[] masterBoardData )
		{
			int startIndex = masterSocketData.Player == PlayerType.Local ? 0 : 9;
			return masterBoardData[ startIndex + masterSocketData.SocketId ];
		}
		#endregion
	}
}