﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class EndTurnButtonBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mPassEndTurnButton;
		#endregion

		#region Constructor
		public EndTurnButtonBattleFilter( bool passEndTurn )
		{
			mPassEndTurnButton = passEndTurn;
		}
		#endregion

		#region Public Methods
		public override void GetEndTurnButton( RenderButton[] sourceEntities, out RenderButton[] passedConstraint, out RenderButton[] failedConstraint )
		{
			List<RenderButton> passedConstraintList = new List<RenderButton>();
			List<RenderButton> failedConstraintList = new List<RenderButton>();

			foreach( RenderButton renderButton in sourceEntities )
			{
				bool isJettisonButton;
				bool isEndTurnButton;

				BattleManager.Get.GetRenderButtonInformation( renderButton, out isJettisonButton, out isEndTurnButton );

				if( isEndTurnButton )
				{
					if( mPassEndTurnButton )
					{
						passedConstraintList.Add( renderButton );
					}
					else
					{
						failedConstraintList.Add( renderButton );
					}
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}