﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class ShipOwnerBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mAllowLocalShip;
		private readonly bool mAllowRemoteShip;
		#endregion

		#region Constructor
		public ShipOwnerBattleFilter( bool allowLocalShip, bool allowRemoteShip )
		{
			mAllowLocalShip = allowLocalShip;
			mAllowRemoteShip = allowRemoteShip;
		}
		#endregion

		#region Public Methods
		public override void GetShips( ShipBattleData[] masterShipData, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Ship[] sourceEntities, out Ship[] passedConstraint, out Ship[] failedConstraint )
		{
			List<Ship> passedConstraintList = new List<Ship>();
			List<Ship> failedConstraintList = new List<Ship>();

			PlayerType player;
			foreach( Ship ship in sourceEntities )
			{
				player = GetMasterDataForShip( masterShipData, ship ).Player;

				if( ( player == PlayerType.Local && mAllowLocalShip ) || ( player == PlayerType.Remote && mAllowRemoteShip ) )
				{
					passedConstraintList.Add( ship );
				}
				else
				{
					failedConstraintList.Add( ship );
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}