﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntityOwnerBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mAllowLocalEntities;
		private readonly bool mAllowRemoteEntities;
		#endregion

		#region Constructor
		public BattleEntityOwnerBattleFilter( bool allowLocalEntities, bool allowRemoteEntities )
		{
			mAllowLocalEntities = allowLocalEntities;
			mAllowRemoteEntities = allowRemoteEntities;
		}
		#endregion

		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			List<BattleEntity> passedConstraintList = new List<BattleEntity>();
			List<BattleEntity> failedConstraintList = new List<BattleEntity>();

			foreach( BattleEntity battleEntity in sourceEntities )
			{
				BattleEntityData masterData = GetMasterDataForBattleEntity( masterBattleEntityData, battleEntity );

				//If the battle entity exists in the data - check which player its associated with!
				if( masterData != null && ( ( masterData.Player == PlayerType.Local && mAllowLocalEntities ) || ( masterData.Player == PlayerType.Remote && mAllowRemoteEntities ) ) )
				{
					passedConstraintList.Add( battleEntity );
				}
				else
				{
					failedConstraintList.Add( battleEntity );
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}