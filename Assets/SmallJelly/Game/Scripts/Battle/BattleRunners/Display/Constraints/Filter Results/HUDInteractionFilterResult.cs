﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class HUDInteractionFilterResult : FilterStateResult
	{
		#region Public Properties
		public PlayerHUDController.State PassedInteractionState
		{
			get
			{
				return mPassedInteractionState;
			}
		}

		public PlayerHUDController.State FailedInteractionState
		{
			get
			{
				return mFailedInteractionState;
			}
		}
		#endregion

		#region Member Variables
		private readonly PlayerHUDController.State mPassedInteractionState;
		private readonly PlayerHUDController.State mFailedInteractionState;
		#endregion

		#region Constructor
		public HUDInteractionFilterResult( PlayerHUDController.State passedInteractionState, PlayerHUDController.State failedInteractionState )
		{
			mPassedInteractionState = passedInteractionState;
			mFailedInteractionState = failedInteractionState;
		}
		#endregion
		
	}
}