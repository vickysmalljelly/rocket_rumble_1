﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class JettisonButtonInteractionFilterResult : FilterStateResult
	{
		#region Public Properties
		public RenderButtonController.ButtonState PassedInteractionState
		{
			get
			{
				return mPassedInteractionState;
			}
		}

		public RenderButtonController.ButtonState FailedInteractionState
		{
			get
			{
				return mFailedInteractionState;
			}
		}
		#endregion

		#region Member Variables
		private readonly RenderButtonController.ButtonState mPassedInteractionState;
		private readonly RenderButtonController.ButtonState mFailedInteractionState;
		#endregion

		#region Constructor
		public JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState passedInteractionState, RenderButtonController.ButtonState failedInteractionState )
		{
			mPassedInteractionState = passedInteractionState;
			mFailedInteractionState = failedInteractionState;
		}
		#endregion
		
	}
}