﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class AliveBattleFilter : BattleFilter
	{
		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			List<BattleEntity> passedConstraintList = new List<BattleEntity>();
			List<BattleEntity> failedConstraintList = new List<BattleEntity>();

			BattleEntityData battleEntityData;
			foreach( BattleEntity sourceEntity in sourceEntities )
			{
				battleEntityData = GetMasterDataForBattleEntity( masterBattleEntityData, sourceEntity );

				//If the battle entity still exists in the data and is a component
				if( battleEntityData != null && battleEntityData is ComponentData )
				{
					ComponentData componentData = (ComponentData)battleEntityData;
					if( componentData.HP > 0 )
					{
						passedConstraintList.Add( sourceEntity );
						continue;
					}
				}

				failedConstraintList.Add( sourceEntity );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}

		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			List<Socket> passedConstraintList = new List<Socket>();
			List<Socket> failedConstraintList = new List<Socket>();

			SocketData socketData;
			BattleEntityData battleEntityData;
			foreach( Socket sourceEntity in sourceEntities )
			{
				socketData = GetMasterDataForSocket( masterSocketData, sourceEntity );
				battleEntityData = GetBattleEntityDataInSocket( socketData, masterBoardData );
				if( battleEntityData == null )
				{
					passedConstraintList.Add( sourceEntity );
					continue;
				}

				if( ((ComponentData)battleEntityData).HP > 0 )
				{
					passedConstraintList.Add( sourceEntity );
					continue;
				}

				failedConstraintList.Add( sourceEntity );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}

		public override void GetShips( ShipBattleData[] masterShipData, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Ship[] sourceEntities, out Ship[] passedConstraint, out Ship[] failedConstraint )
		{
			List<Ship> passedConstraintList = new List<Ship>();
			List<Ship> failedConstraintList = new List<Ship>();

			foreach( Ship sourceEntity in sourceEntities )
			{
				ShipBattleData shipData = GetMasterDataForShip( masterShipData, sourceEntity );
				if( shipData.HullHp > 0 )
				{
					passedConstraintList.Add( sourceEntity );
					continue;
				}

				failedConstraintList.Add( sourceEntity );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}