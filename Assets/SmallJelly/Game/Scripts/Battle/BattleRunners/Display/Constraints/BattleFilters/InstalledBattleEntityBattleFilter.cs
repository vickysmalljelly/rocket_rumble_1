﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SmallJelly.Framework;

namespace SmallJelly
{
	//Passes battle entities and sockets associated with the given socket slots
	public class InstalledBattleEntityBattleFilter : BattleFilter
	{
		#region Member Variables
		private bool mAllowInstalledBattleEntities;
		private bool mAllowUninstalledBattleEntities;
		#endregion

		#region Constructor
		public InstalledBattleEntityBattleFilter( bool allowInstalledBattleEntities, bool allowUninstalledBattleEntities )
		{
			mAllowInstalledBattleEntities = allowInstalledBattleEntities;
			mAllowUninstalledBattleEntities = allowUninstalledBattleEntities;
		}
		#endregion

		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			List< BattleEntity > passedBattleEntities = new List< BattleEntity >();
			List< BattleEntity > failedBattleEntities = new List< BattleEntity >();

			BattleEntityData battleEntityData;
			foreach( BattleEntity battleEntity in sourceEntities )
			{
				battleEntityData = GetMasterDataForBattleEntity( masterBattleEntityData, battleEntity );

				//If the entitity still exists in the data and it's on the board then it installed!
				bool installed = battleEntityData != null && masterBoardData.Any( x => x != null && x.DrawNumber == battleEntityData.DrawNumber );

				if( ( installed && mAllowInstalledBattleEntities ) || ( !installed && mAllowUninstalledBattleEntities ) )
				{
					passedBattleEntities.Add( battleEntity );
				}
				else
				{
					failedBattleEntities.Add( battleEntity );
				}
			}


			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = passedBattleEntities.ToArray();
			failedConstraint = failedBattleEntities.ToArray();

		}
		#endregion

	}
}