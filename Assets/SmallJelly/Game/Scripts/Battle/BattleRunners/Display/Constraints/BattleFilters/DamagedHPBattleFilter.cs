﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class DamagedHPBattleFilter : BattleFilter
	{
		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			List<BattleEntity> passedConstraintList = new List<BattleEntity>();
			List<BattleEntity> failedConstraintList = new List<BattleEntity>();

			BattleEntityData masterData;
			foreach( BattleEntity sourceEntity in sourceEntities )
			{
				masterData = GetMasterDataForBattleEntity( masterBattleEntityData, sourceEntity );

				//If the entity still exists in the data and is a weapon
				if( masterData != null && masterData is WeaponData )
				{
					WeaponData weaponData = (WeaponData)masterData;
					if( weaponData.HP < weaponData.MaxHP )
					{
						passedConstraintList.Add( sourceEntity );
						continue;
					}
				}

				failedConstraintList.Add( sourceEntity );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}

		public override void GetShips( ShipBattleData[] masterShipData, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Ship[] sourceShips, out Ship[] passedConstraint, out Ship[] failedConstraint )
		{
			List<Ship> passedConstraintList = new List<Ship>();
			List<Ship> failedConstraintList = new List<Ship>();

			ShipBattleData masterData;
			foreach( Ship sourceShip in sourceShips )
			{
				masterData = GetMasterDataForShip( masterShipData, sourceShip );
				if( masterData.HullHp < masterData.MaxHullHp )
				{
					passedConstraintList.Add( sourceShip );
					continue;
				}

				failedConstraintList.Add( sourceShip );
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}