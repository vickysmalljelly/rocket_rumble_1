﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class BattleEntityInteractionFilterResult : FilterStateResult
	{
		#region Public Properties
		public BattleEntityController.TargetableState PassedInteractionState
		{
			get
			{
				return mPassedInteractionState;
			}
		}

		public BattleEntityController.TargetableState FailedInteractionState
		{
			get
			{
				return mFailedInteractionState;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.TargetableState mPassedInteractionState;
		private readonly BattleEntityController.TargetableState mFailedInteractionState;
		#endregion

		#region Constructor
		public BattleEntityInteractionFilterResult( BattleEntityController.TargetableState passedInteractionState, BattleEntityController.TargetableState failedInteractionState )
		{
			mPassedInteractionState = passedInteractionState;
			mFailedInteractionState = failedInteractionState;
		}
		#endregion
	}
}