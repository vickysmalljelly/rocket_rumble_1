﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntityListBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly List<int> mFilteredDrawNumbers;
		#endregion

		#region Constructor
		public BattleEntityListBattleFilter( List<int> filteredDrawNumbers )
		{
			mFilteredDrawNumbers = filteredDrawNumbers;
		}
		#endregion

		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			List<BattleEntity> passedConstraintList = new List<BattleEntity>();
			List<BattleEntity> failedConstraintList = new List<BattleEntity>();

			foreach( BattleEntity sourceEntity in sourceEntities )
			{
				if( mFilteredDrawNumbers.Contains( sourceEntity.Data.DrawNumber ) )
				{
					passedConstraintList.Add( sourceEntity );
				}
				else
				{
					failedConstraintList.Add( sourceEntity );
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}