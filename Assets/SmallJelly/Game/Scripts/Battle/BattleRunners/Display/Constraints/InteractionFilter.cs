﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class InteractionFilter
	{
		#region Public Properties
		public BattleEntity[] PassedBattleEntities
		{
			get
			{
				return mPassedBattleEntities;
			}
		}

		public BattleEntity[] FailedBattleEntities
		{
			get
			{
				return mFailedBattleEntities;
			}
		}

		public Ship[] PassedShips
		{
			get
			{
				return mPassedShips;
			}
		}

		public Ship[] FailedShips
		{
			get
			{
				return mFailedShips;
			}
		}

		public Socket[] PassedSockets
		{
			get
			{
				return mPassedSockets;
			}
		}

		public Socket[] FailedSockets
		{
			get
			{
				return mFailedSockets;
			}
		}


		public RenderButton[] PassedEndTurnButtons
		{
			get
			{
				return mPassedEndTurnButtons;
			}
		}

		public RenderButton[] FailedEndTurnButtons
		{
			get
			{
				return mFailedEndTurnButtons;
			}
		}

		public RenderButton[] PassedJettisonButtons
		{
			get
			{
				return mPassedJettisonButtons;
			}
		}

		public RenderButton[] FailedJettisonButtons
		{
			get
			{
				return mFailedJettisonButtons;
			}
		}

		public PlayerHUD[] PassedHUDs
		{
			get
			{
				return mPassedHUDs;
			}
		}

		public PlayerHUD[] FailedHUDs
		{
			get
			{
				return mFailedHUDs;
			}
		}
		#endregion

		#region Member Variables
		//The battle filters to be applied
		private readonly BattleFilter[] mBattleFilters;

		private readonly FilterStateResult[] mInteractionFilterResults;

		//Those entities that we do not wish to filter
		private readonly BattleEntity[] mExcludedEntities;

		private BattleEntityData[] mPrefilteredEntityData;
		private ShipBattleData[] mPrefilteredShipData;
		private SocketData[] mPrefilteredSocketData;
		private BattleEntityData[] mPrefilteredBoardData;

		private BattleEntity[] mPrefilteredBattleEntities;
		private Ship[] mPrefilteredShips;
		private Socket[] mPrefilteredSockets;
		private RenderButton[] mPrefilteredEndTurnButtons;
		private RenderButton[] mPrefilteredJettisonButtons;
		private PlayerHUD[] mPrefilteredHUDs;

		private BattleEntity[] mPassedBattleEntities;
		private BattleEntity[] mFailedBattleEntities;

		private Ship[] mPassedShips;
		private Ship[] mFailedShips;

		private Socket[] mPassedSockets;
		private Socket[] mFailedSockets;

		private RenderButton[] mPassedEndTurnButtons;
		private RenderButton[] mFailedEndTurnButtons;

		private RenderButton[] mPassedJettisonButtons;
		private RenderButton[] mFailedJettisonButtons;

		private PlayerHUD[] mPassedHUDs;
		private PlayerHUD[] mFailedHUDs;
		#endregion

		#region Constructors
		public InteractionFilter( BattleFilter[] battleFilters, FilterStateResult[] interactionFilterResults, BattleEntity[] excludedEntities )
		{
			mBattleFilters = battleFilters;
			mInteractionFilterResults = interactionFilterResults;
			mExcludedEntities = excludedEntities;

			//Initialize pre-filtered arrays
			mPrefilteredBattleEntities = new BattleEntity[0];
			mPrefilteredSockets = new Socket[0];
			mPrefilteredShips = new Ship[0];
		}
		#endregion

		#region Public Methods
		//Applies the filter
		public void ApplyFilter( PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerBattleView localPlayerBattleView, PlayerBattleView remotePlayerBattleView )
		{
			GetPreFilteredData( localPlayerBattleData, remotePlayerBattleData, out mPrefilteredEntityData, out mPrefilteredShipData, out mPrefilteredSocketData, out mPrefilteredBoardData );
			GetPreFilteredEntities( localPlayerBattleView, remotePlayerBattleView, mExcludedEntities, out mPrefilteredBattleEntities, out mPrefilteredShips, out mPrefilteredSockets, out mPrefilteredEndTurnButtons, out mPrefilteredJettisonButtons, out mPrefilteredHUDs );

			mPassedBattleEntities = new BattleEntity[0];
			mPassedShips = new Ship[0];
			mPassedSockets = new Socket[0];
			mPassedJettisonButtons = new RenderButton[0];
			mPassedEndTurnButtons = new RenderButton[0];
			mPassedHUDs = new PlayerHUD[0];

			mFailedBattleEntities = new BattleEntity[0];
			mFailedShips = new Ship[0];
			mFailedSockets = new Socket[0];
			mFailedJettisonButtons = new RenderButton[0];
			mFailedEndTurnButtons = new RenderButton[0];
			mFailedHUDs = new PlayerHUD[0];

			GetFilteredEntities( mPrefilteredBattleEntities, mPrefilteredEntityData, mPrefilteredShips, mPrefilteredShipData, mPrefilteredSockets, mPrefilteredSocketData, mPrefilteredBoardData, mPrefilteredEndTurnButtons, mPrefilteredJettisonButtons, mPrefilteredHUDs, mBattleFilters, out mPassedBattleEntities, out mPassedShips, out mPassedSockets, out mPassedEndTurnButtons, out mPassedJettisonButtons, out mPassedHUDs, out mFailedBattleEntities, out mFailedShips, out mFailedSockets, out mFailedEndTurnButtons, out mFailedJettisonButtons, out mFailedHUDs );

			if( mPassedHUDs.Length > 0 || mFailedHUDs.Length > 0 )
			{
				HUDInteractionFilterResult hudInteractionFilterResult = ArrayExtensions.GetObjectOfType< HUDInteractionFilterResult >( mInteractionFilterResults );

				//HUDs have passed filter
				ApplyHUDStates( mPassedHUDs, hudInteractionFilterResult.PassedInteractionState );

				//HUDs have failed filter
				ApplyHUDStates( mFailedHUDs, hudInteractionFilterResult.FailedInteractionState );
			}

			if( mPassedBattleEntities.Length > 0 || mFailedBattleEntities.Length > 0 )
			{
				BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = ArrayExtensions.GetObjectOfType< BattleEntityInteractionFilterResult >( mInteractionFilterResults );

				//Battle entities have passed filter
				ApplyBattleEntityStates( mPassedBattleEntities, battleEntityInteractionFilterResult.PassedInteractionState );

				//Battle entities have failed filter
				ApplyBattleEntityStates( mFailedBattleEntities, battleEntityInteractionFilterResult.FailedInteractionState );
			}

			if( mPassedShips.Length > 0 || mFailedShips.Length > 0 )
			{
				ShipInteractionFilterResult shipInteractionFilterResult = ArrayExtensions.GetObjectOfType< ShipInteractionFilterResult >( mInteractionFilterResults );

				//Ship entities have passed filter
				ApplyShipStates( mPassedShips, shipInteractionFilterResult.PassedInteractionState );

				//Ship entities have failed filter
				ApplyShipStates( mFailedShips, shipInteractionFilterResult.FailedInteractionState );
			}

			if( mPassedSockets.Length > 0 || mFailedSockets.Length > 0 )
			{
				SocketInteractionFilterResult socketInteractionFilterResult = ArrayExtensions.GetObjectOfType< SocketInteractionFilterResult >( mInteractionFilterResults );

				//Slot entities have passed filter
				ApplySocketStates( mPassedSockets, socketInteractionFilterResult.PassedInteractionState );

				//Slot entities have failed filter
				ApplySocketStates( mFailedSockets, socketInteractionFilterResult.FailedInteractionState );
			}

			if( mPassedJettisonButtons.Length > 0 || mFailedJettisonButtons.Length > 0 )
			{
				JettisonButtonInteractionFilterResult buttonInteractionFilterResult = ArrayExtensions.GetObjectOfType< JettisonButtonInteractionFilterResult >( mInteractionFilterResults );

				//Button entities have passed filter
				ApplyButtonStates( mPassedJettisonButtons, buttonInteractionFilterResult.PassedInteractionState );

				//Button entities have failed filter
				ApplyButtonStates( mFailedJettisonButtons, buttonInteractionFilterResult.FailedInteractionState );
			}

			if( mPassedEndTurnButtons.Length > 0 || mFailedEndTurnButtons.Length > 0 )
			{
				EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = ArrayExtensions.GetObjectOfType< EndTurnButtonInteractionFilterResult >( mInteractionFilterResults );

				//Button entities have passed filter
				ApplyButtonStates( mPassedEndTurnButtons, buttonInteractionFilterResult.PassedInteractionState );

				//Button entities have failed filter
				ApplyButtonStates( mFailedEndTurnButtons, buttonInteractionFilterResult.FailedInteractionState );
			}
		}

		//Removes the filter
		public void Remove()
		{
			foreach( BattleEntity battleEntity in mPrefilteredBattleEntities )
			{
				battleEntity.OnInsertTargetableState( TargetableLogicController.TargetableState.None );
			}

			foreach( Socket socket in mPrefilteredSockets )
			{
				socket.OnInsertTargetableState( TargetableLogicController.TargetableState.None );
			}

			foreach( Ship ship in mPrefilteredShips )
			{
				ship.OnInsertTargetableState( TargetableLogicController.TargetableState.None );
			}

		}
		#endregion

		#region Private Methods
		private void GetPreFilteredData( PlayerBattleData playerBattleData, PlayerBattleData enemyPlayerBattleData, out BattleEntityData[] prefilteredEntityData, out ShipBattleData[] prefilteredShipData, out SocketData[] prefilteredSocketData, out BattleEntityData[] prefilteredBoardData )
		{
			prefilteredEntityData = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( BattleEntityInteractionFilterResult ) ) ? GetPreFilteredBattleEntityData( playerBattleData, enemyPlayerBattleData ) : new BattleEntityData[0];

			prefilteredShipData = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( ShipInteractionFilterResult ) ) ?  GetPreFilteredShipData( playerBattleData, enemyPlayerBattleData ) : new ShipBattleData[0];

			prefilteredSocketData = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( SocketInteractionFilterResult ) ) ?  GetPreFilteredSocketData( playerBattleData, enemyPlayerBattleData ) : new SocketData[0];

			prefilteredBoardData = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( SocketInteractionFilterResult ) ) || ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( BattleEntityInteractionFilterResult ) ) ?  GetPreFilteredBoardData( playerBattleData, enemyPlayerBattleData ) : new BattleEntityData[0];
		}

		private void GetPreFilteredEntities( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView, BattleEntity[] excludedEntities, out BattleEntity[] prefilteredComponents, out Ship[] prefilteredShips, out Socket[] prefilteredSockets, out RenderButton[] prefilteredEndTurnButtons, out RenderButton[] prefilteredJettisonButtons, out PlayerHUD[] prefilteredHUDs )
		{
			prefilteredComponents = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( BattleEntityInteractionFilterResult ) ) ? GetPreFilteredBattleEntities( playerBattleView, enemyBattleView, excludedEntities ) : new BattleEntity[0];

			prefilteredShips = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( ShipInteractionFilterResult ) ) ?  GetPreFilteredShips( playerBattleView, enemyBattleView ) : new Ship[0];

			prefilteredSockets = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( SocketInteractionFilterResult ) ) ?  GetPreFilteredSockets( playerBattleView, enemyBattleView ) : new Socket[0];

			prefilteredEndTurnButtons = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( EndTurnButtonInteractionFilterResult ) ) ?  GetPreFilteredButtons( playerBattleView, enemyBattleView ) : new RenderButton[0];
		
			prefilteredJettisonButtons = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( JettisonButtonInteractionFilterResult ) ) ?  GetPreFilteredButtons( playerBattleView, enemyBattleView ) : new RenderButton[0];

			prefilteredHUDs = ArrayExtensions.ContainsType( mInteractionFilterResults, typeof( HUDInteractionFilterResult ) ) ?  GetPreFilteredHUDs( playerBattleView, enemyBattleView ) : new PlayerHUD[0];

		}

		private BattleEntityData[] GetPreFilteredBattleEntityData( PlayerBattleData playerBattleData, PlayerBattleData enemyBattleData )
		{
			//Add battle entities
			List< BattleEntityData > possibleBattleEntityDataList = new List< BattleEntityData >();
			possibleBattleEntityDataList.AddRange( playerBattleData.HandCardData );
			possibleBattleEntityDataList.AddRange( enemyBattleData.HandCardData );
	
			possibleBattleEntityDataList.AddRange( playerBattleData.BoardCardData.Where( o => o != null ) );
			possibleBattleEntityDataList.AddRange( enemyBattleData.BoardCardData.Where( o => o != null ) );

			return possibleBattleEntityDataList.ToArray();
		}

		private ShipBattleData[] GetPreFilteredShipData( PlayerBattleData playerBattleData, PlayerBattleData enemyBattleData )
		{
			//Add battle entities
			ShipBattleData[] possibleShipDataList = new ShipBattleData[ 2 ];
			possibleShipDataList[ 0 ] = playerBattleData.ShipData;
			possibleShipDataList[ 1 ] = enemyBattleData.ShipData;
		
			return possibleShipDataList;
		}

		private SocketData[] GetPreFilteredSocketData( PlayerBattleData playerBattleData, PlayerBattleData enemyBattleData )
		{
			//Add battle entities
			List< SocketData > possibleSocketData = new List< SocketData >();
			possibleSocketData.AddRange( playerBattleData.SocketData );
			possibleSocketData.AddRange( enemyBattleData.SocketData );

			return possibleSocketData.ToArray();
		}

		private BattleEntityData[] GetPreFilteredBoardData( PlayerBattleData playerBattleData, PlayerBattleData enemyBattleData )
		{
			//Add battle entities
			List< BattleEntityData > possibleBoardData = new List< BattleEntityData >();
			possibleBoardData.AddRange( playerBattleData.BoardCardData );
			possibleBoardData.AddRange( enemyBattleData.BoardCardData );

			return possibleBoardData.ToArray();
		}

		private BattleEntity[] GetPreFilteredBattleEntities( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView, BattleEntity[] excludedEntities )
		{
			//Add battle entities
			List<BattleEntity> possibleBattleEntityTargetsList = new List<BattleEntity>();

			//Add the local players cards
			foreach( BattleEntity battleEntity in playerBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities )
			{
				//If the battle entity isnt null and isnt an excluded entity
				if( Array.IndexOf( excludedEntities, battleEntity ) == -1 )
				{
					possibleBattleEntityTargetsList.Add( battleEntity );
				}
			}

			//Add the remote players cards
			foreach( BattleEntity battleEntity in enemyBattleView.PlayerBattleSceneView.PlayerHandView.OrderedBattleEntities )
			{
				//If the battle entity isnt null and isnt an excluded entity
				if(  Array.IndexOf( excludedEntities, battleEntity ) == -1 )
				{
					possibleBattleEntityTargetsList.Add( battleEntity );
				}
			}

			//Add the local players components
			foreach( BattleEntity battleEntity in playerBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities )
			{
				//If the battle entity isnt null and isnt an excluded entity
				if( battleEntity != null && Array.IndexOf( excludedEntities, battleEntity ) == -1 )
				{
					possibleBattleEntityTargetsList.Add( battleEntity );
				}
			}

			//Add the remote players components
			foreach( BattleEntity battleEntity in enemyBattleView.PlayerBattleSceneView.PlayerBoardView.BattleEntities )
			{
				//If the battle entity isnt null and isnt an excluded entity
				if( battleEntity != null && Array.IndexOf( excludedEntities, battleEntity ) == -1 )
				{
					possibleBattleEntityTargetsList.Add( battleEntity );
				}
			}

			return possibleBattleEntityTargetsList.ToArray();
		}

		private Ship[] GetPreFilteredShips( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView )
		{
			//Add both players ships
			Ship[] prefilteredShips = new Ship[2];
			prefilteredShips[0] = playerBattleView.PlayerBattleSceneView.PlayerBoardView.Ship;
			prefilteredShips[1] = enemyBattleView.PlayerBattleSceneView.PlayerBoardView.Ship;

			return prefilteredShips;
		}

		private Socket[] GetPreFilteredSockets( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView )
		{
			//Add slot entities
			List<Socket> possibleSocketTargetsList = new List<Socket>();
			foreach( Socket socket in playerBattleView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets )
			{
				possibleSocketTargetsList.Add( socket );
			}

			foreach( Socket socket in enemyBattleView.PlayerBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets )
			{
				possibleSocketTargetsList.Add( socket );
			}

			return possibleSocketTargetsList.ToArray();
		}

		private RenderButton[] GetPreFilteredButtons( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView )
		{
			//Add both buttons
			RenderButton[] prefilteredButtons = new RenderButton[2];
			prefilteredButtons[0] = ((LocalPlayerHUD)playerBattleView.PlayerHUD).JettisonButton;
			prefilteredButtons[1] = ((LocalPlayerHUD)playerBattleView.PlayerHUD).EndTurnButton;

			return prefilteredButtons;
		}

		private PlayerHUD[] GetPreFilteredHUDs( PlayerBattleView playerBattleView, PlayerBattleView enemyBattleView )
		{
			//Add both huds
			PlayerHUD[] prefilteredHUDs = new PlayerHUD[2];
			prefilteredHUDs[0] = playerBattleView.PlayerHUD;
			prefilteredHUDs[1] = enemyBattleView.PlayerHUD;

			return prefilteredHUDs;
		}

		private void GetFilteredEntities( BattleEntity[] sourceBattleEntities, BattleEntityData[] masterEntityData, Ship[] sourceShips, ShipBattleData[] masterShipData, Socket[] sourceSockets, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, RenderButton[] sourceEndTurnButtons, RenderButton[] sourceJettisonButtons, PlayerHUD[] sourceHUDs, BattleFilter[] battleFilters, out BattleEntity[] passedBattleEntities, out Ship[] passedShips, out Socket[] passedSockets, out RenderButton[] passedEndTurnButtons, out RenderButton[] passedJettisonButtons, out PlayerHUD[] passedHUDs, out BattleEntity[] failedBattleEntities, out Ship[] failedShips, out Socket[] failedSockets, out RenderButton[] failedEndTurnButtons, out RenderButton[] failedJettisonButtons, out PlayerHUD[] failedHUDs )
		{
			passedSockets = sourceSockets;
			passedBattleEntities = sourceBattleEntities;
			passedShips = sourceShips;
			passedEndTurnButtons = sourceEndTurnButtons;
			passedJettisonButtons = sourceJettisonButtons;
			passedHUDs = sourceHUDs;

			failedSockets = new Socket[0];
			failedBattleEntities = new BattleEntity[0];
			failedShips = new Ship[0];
			failedEndTurnButtons = new RenderButton[0];
			failedJettisonButtons = new RenderButton[0];
			failedHUDs = new PlayerHUD[0];

			for( int i = 0; i < battleFilters.Length; i++ )
			{
				//Get passed/failed ship entities
				int failedShipCount = failedShips.Length;

				Ship[] passedCurrentShipFilter;
				Ship[] failedCurrentShipFilter;
				battleFilters[i].GetShips( masterShipData, masterSocketData, masterBoardData, passedShips, out passedCurrentShipFilter, out failedCurrentShipFilter );

				passedShips = passedCurrentShipFilter;
				Array.Resize<Ship>(ref failedShips, failedShipCount + failedCurrentShipFilter.Length);
				Array.Copy(failedCurrentShipFilter, 0, failedShips, failedShipCount, failedCurrentShipFilter.Length);


				//Get passed/failed slot entities
				int failedSocketCount = failedSockets.Length;

				Socket[] passedCurrentSocketFilter;
				Socket[] failedCurrentSocketFilter;
				battleFilters[i].GetSockets( masterSocketData, masterBoardData, passedSockets, out passedCurrentSocketFilter, out failedCurrentSocketFilter );

				passedSockets = passedCurrentSocketFilter;
				Array.Resize<Socket>(ref failedSockets, failedSocketCount + failedCurrentSocketFilter.Length);
				Array.Copy(failedCurrentSocketFilter, 0, failedSockets, failedSocketCount, failedCurrentSocketFilter.Length);



				//Get passed/failed battle entities
				int failedBattleEntityCount = failedBattleEntities.Length;

				BattleEntity[] passedCurrentBattleEntityFilter;
				BattleEntity[] failedCurrentBattleEntityFilter;
				battleFilters[i].GetBattleEntities( masterEntityData, masterBoardData, passedBattleEntities, out passedCurrentBattleEntityFilter, out failedCurrentBattleEntityFilter );

				passedBattleEntities = passedCurrentBattleEntityFilter;
				Array.Resize<BattleEntity>(ref failedBattleEntities, failedBattleEntityCount + failedCurrentBattleEntityFilter.Length);
				Array.Copy(failedCurrentBattleEntityFilter, 0, failedBattleEntities, failedBattleEntityCount, failedCurrentBattleEntityFilter.Length);
			


				//Get passed/failed buttons
				int failedJettisonButtonCount = failedJettisonButtons.Length;

				RenderButton[] passedCurrentJettisonButtonFilter;
				RenderButton[] failedCurrentJettisonButtonFilter;
				battleFilters[i].GetJettisonButton( passedJettisonButtons, out passedCurrentJettisonButtonFilter, out failedCurrentJettisonButtonFilter );

				passedJettisonButtons = passedCurrentJettisonButtonFilter;
				Array.Resize<RenderButton>(ref failedJettisonButtons, failedJettisonButtonCount + failedCurrentJettisonButtonFilter.Length);
				Array.Copy(failedCurrentJettisonButtonFilter, 0, failedJettisonButtons, failedJettisonButtonCount, failedCurrentJettisonButtonFilter.Length);


				//Get passed/failed buttons
				int failedEndTurnButtonCount = failedEndTurnButtons.Length;

				RenderButton[] passedCurrentEndTurnButtonFilter;
				RenderButton[] failedCurrentEndTurnButtonFilter;
				battleFilters[i].GetEndTurnButton( passedEndTurnButtons, out passedCurrentEndTurnButtonFilter, out failedCurrentEndTurnButtonFilter );

				passedEndTurnButtons = passedCurrentEndTurnButtonFilter;
				Array.Resize<RenderButton>(ref failedEndTurnButtons, failedEndTurnButtonCount + failedCurrentEndTurnButtonFilter.Length);
				Array.Copy(failedCurrentEndTurnButtonFilter, 0, failedEndTurnButtons, failedEndTurnButtonCount, failedCurrentEndTurnButtonFilter.Length);


				//Get passed/failed huds
				int failedHUDCount = failedHUDs.Length;

				PlayerHUD[] passedCurrentHUDFilter;
				PlayerHUD[] failedCurrentHUDFilter;
				battleFilters[i].GetHUDs( passedHUDs, out passedCurrentHUDFilter, out failedCurrentHUDFilter );

				passedHUDs = passedCurrentHUDFilter;
				Array.Resize< PlayerHUD >(ref failedHUDs, failedHUDCount + failedCurrentHUDFilter.Length);
				Array.Copy(failedCurrentHUDFilter, 0, failedHUDs, failedHUDCount, failedCurrentHUDFilter.Length);

			}
		}

		private void ApplyBattleEntityStates( BattleEntity[] battleEntities, BattleEntityController.TargetableState state )
		{
			foreach( BattleEntity battleEntity in battleEntities )
			{
				battleEntity.OnInsertTargetableState( state );
			}
		}

		private void ApplyShipStates( Ship[] shipEntities, TargetableLogicController.TargetableState state )
		{
			foreach( Ship ship in shipEntities )
			{
				ship.OnInsertTargetableState( state );
			}
		}

		private void ApplySocketStates( Socket[] slotEntities, SocketController.TargetableState targetableState )
		{
			foreach( Socket socket in slotEntities )
			{
				socket.OnInsertTargetableState( targetableState );
			}
		}

		private void ApplyButtonStates( RenderButton[] renderButtons, RenderButtonController.ButtonState state )
		{
			foreach( RenderButton renderButton in renderButtons )
			{
				renderButton.OnInsertState( state );
			}
		}

		private void ApplyHUDStates( PlayerHUD[] huds, PlayerHUDController.State state )
		{
			foreach( PlayerHUD playerHUD in huds )
			{
				playerHUD.OnInsertState( state );
			}
		}
		#endregion
	}
}