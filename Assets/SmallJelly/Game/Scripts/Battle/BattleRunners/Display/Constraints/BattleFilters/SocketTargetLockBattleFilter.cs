﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class SocketTargetLockBattleFilter : BattleFilter
	{
		#region Constructor
		public SocketTargetLockBattleFilter()
		{
		}
		#endregion

		#region Public Methods
		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			SocketData socketData;
			BattleEntityData battleEntityData;

			List< Socket > targetLockedSockets = new List< Socket >();
			foreach( Socket socket in sourceEntities )
			{
				socketData = GetMasterDataForSocket( masterSocketData, socket );
				battleEntityData = GetBattleEntityDataInSocket( socketData, masterBoardData );
				if( battleEntityData != null && battleEntityData.Buffs.Contains( BattleEntityController.Buff.TargetLock ) && ( (ComponentData) battleEntityData ).HP > 0 )
				{
					targetLockedSockets.Add( socket );
				}
			}


			if( targetLockedSockets.Count > 0)
			{
				passedConstraint = targetLockedSockets.ToArray();
				failedConstraint = sourceEntities.Where( x => !targetLockedSockets.Contains( x ) ).ToArray();
			}
			else
			{
				passedConstraint = sourceEntities;
				failedConstraint = new Socket[0];
			}
		}

		public override void GetShips( ShipBattleData[] masterShipData, SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Ship[] sourceEntities, out Ship[] passedConstraint, out Ship[] failedConstraint )
		{
			List< Ship > passedShipsList = new List<Ship>();
			List< Ship > failedShipsList = new List<Ship>();

			ShipBattleData shipBattleData;
			BattleEntityData battleEntityData;
			foreach( Ship ship in sourceEntities )
			{
				bool shipContainsTargetLock = false;
				shipBattleData = GetMasterDataForShip( masterShipData, ship );
				foreach( SocketData socketData in masterSocketData )
				{
					if( socketData.Player == shipBattleData.Player )
					{
						battleEntityData = GetBattleEntityDataInSocket( socketData, masterBoardData );
						if( battleEntityData != null && battleEntityData.Buffs.Contains( BattleEntityController.Buff.TargetLock ) && ( (ComponentData) battleEntityData ).HP > 0 )
						{
							shipContainsTargetLock = true;
							break;
						}
					}
				}

				if( shipContainsTargetLock )
				{
					failedShipsList.Add( ship );
				}
				else
				{
					passedShipsList.Add( ship );
				}
			}

			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = passedShipsList.ToArray();
			failedConstraint = failedShipsList.ToArray();
		}
		#endregion
	}
}