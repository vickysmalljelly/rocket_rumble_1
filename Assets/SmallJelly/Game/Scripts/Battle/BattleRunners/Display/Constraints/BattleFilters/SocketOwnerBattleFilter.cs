﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmallJelly
{
	public class SocketOwnerBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly bool mAllowFriendlyEntities;
		private readonly bool mAllowEnemyEntities;
		#endregion

		#region Constructor
		public SocketOwnerBattleFilter( bool allowFriendlyEntities, bool allowEnemyEnitites )
		{
			mAllowFriendlyEntities = allowFriendlyEntities;
			mAllowEnemyEntities = allowEnemyEnitites;
		}
		#endregion

		#region Public Methods
		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			List<Socket> passedConstraintList = new List<Socket>();
			List<Socket> failedConstraintList = new List<Socket>();

			foreach( Socket socket in sourceEntities )
			{
				SocketData socketData = GetMasterDataForSocket( masterSocketData, socket );

				if( ( socketData.Player == PlayerType.Local && mAllowFriendlyEntities ) || ( socketData.Player == PlayerType.Remote && mAllowEnemyEntities ) )
				{
					passedConstraintList.Add( socket );
				}
				else 
				{
					failedConstraintList.Add( socket );
				}
			}

			passedConstraint = passedConstraintList.ToArray();
			failedConstraint = failedConstraintList.ToArray();
		}
		#endregion
	}
}