﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SmallJelly
{
	//Accepts a predicate and evaluates it - splitting the results into true and false
	public class SocketPredicateBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly Func< Socket, bool > mSocketPredicate;
		#endregion

		#region Constructor
		public SocketPredicateBattleFilter( Func< Socket, bool> socketPredicate )
		{
			mSocketPredicate = socketPredicate;
		}
		#endregion

		#region Public Methods
		public override void GetSockets( SocketData[] masterSocketData, BattleEntityData[] masterBoardData, Socket[] sourceEntities, out Socket[] passedConstraint, out Socket[] failedConstraint )
		{
			Dictionary< bool, Socket[] > dictionary = sourceEntities.GroupBy( mSocketPredicate ).ToDictionary(g => g.Key, g => g.ToArray());

			//By default everything passes the constraint (unless this is overrided)
			passedConstraint = dictionary[ true ];
			failedConstraint = dictionary[ false ];
		}
		#endregion
	}
}