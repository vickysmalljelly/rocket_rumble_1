﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SmallJelly
{
	//Accepts a predicate and evaluates it - splitting the results into true and false
	public class BattleEntityPredicateBattleFilter : BattleFilter
	{
		#region Member Variables
		private readonly Func< BattleEntity, bool > mPredicate;
		#endregion

		#region Constructor
		public BattleEntityPredicateBattleFilter( Func< BattleEntity, bool> predicate )
		{
			mPredicate = predicate;
		}
		#endregion

		#region Public Methods
		public override void GetBattleEntities( BattleEntityData[] masterBattleEntityData, BattleEntityData[] masterBoardData, BattleEntity[] sourceEntities, out BattleEntity[] passedConstraint, out BattleEntity[] failedConstraint )
		{
			Dictionary< bool, BattleEntity[] > dictionary = sourceEntities.GroupBy( mPredicate ).ToDictionary(g => g.Key, g => g.ToArray());

			passedConstraint = dictionary.ContainsKey( true ) ? dictionary[ true ] : new BattleEntity[ 0 ];
			failedConstraint = dictionary.ContainsKey( false ) ? dictionary[ false ] : new BattleEntity[ 0 ];
		}
		#endregion
	}
}