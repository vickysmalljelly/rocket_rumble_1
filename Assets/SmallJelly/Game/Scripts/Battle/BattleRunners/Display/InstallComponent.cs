﻿using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class InstallComponent : PlayCardSuccess 
	{

		#region Constants
		private const string SOCKET_KEY = "Socket";
		#endregion

		#region Member Variables
		//TODO - It actually makes sense to move this up to the parent class - just not yet since the others runners arent moved over to the new system!
		private InstallComponentData mInstallComponentData;
		private BattleEntity mInstallationBattleEntity;
		private Socket mInstallationSocket;
		#endregion

		#region Constructor
		public InstallComponent( InstallComponentData installComponentData, BattleRunnerArgs args ) : base( BattleEventType.InstallComponentSuccess, args, installComponentData.PlayerId )
		{
			mInstallComponentData = installComponentData;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			base.OnStartRunning();

			SJLogger.LogMessage( MessageFilter.George, "1 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

			//Get the destroying players view
			PlayerBattleSceneView installationBattleSceneView = IsLocalPlayerId( mInstallComponentData.PlayerId ) ? LocalSceneView : RemoteSceneView;

			SJLogger.LogMessage( MessageFilter.George, "2 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

			mInstallationSocket = installationBattleSceneView.PlayerBoardView.Ship.BattleEntitySockets[ mInstallComponentData.SocketId ];

			SJLogger.LogMessage( MessageFilter.George, "3 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

			mInstallationBattleEntity = installationBattleSceneView.PlayerHandView.Get( mInstallComponentData.DrawNumber );

			SJLogger.LogMessage( MessageFilter.George, "4 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

			if( mInstallationBattleEntity != null )
			{
				SJLogger.LogMessage( MessageFilter.George, "5 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

				mInstallationBattleEntity.OnInsertState(BattleEntityController.State.ComponentInstallation, GetAnimationMetaData( ));

				RegisterBattleEntityListeners();	
			}
			else
			{
				SJLogger.LogMessage( MessageFilter.George, "6 Starting to install component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

				OnFinishRunning();
			}				
		}

		public override void OnFinishRunning( )
		{
			SJLogger.LogMessage( MessageFilter.George, "7 Finishing installing component with the draw number {0} {1}", mInstallComponentData.DrawNumber, mInstallComponentData.SocketId );

			if( mInstallationBattleEntity != null )
			{
				UnregisterBattleEntityListeners();
			}

			base.OnFinishRunning();
		}
		#endregion

		#region Private Methods
		private AnimationMetaData GetAnimationMetaData( )
		{
			Dictionary< string, NamedVariable > variables = new Dictionary< string, NamedVariable >();
			variables.Add( SOCKET_KEY, new FsmGameObject( mInstallationSocket.gameObject ) );

			return new AnimationMetaData( variables );
		}

		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mInstallationBattleEntity.AnimationFinished += HandleBattleEntityAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mInstallationBattleEntity.AnimationFinished -= HandleBattleEntityAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleBattleEntityAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			SJLogger.LogMessage( MessageFilter.George, "8 Starting to install component with the draw number {0}", mInstallComponentData.DrawNumber );

			OnFinishRunning();
		}
		#endregion
	}
}