﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
    [Serializable]
    public class ReplayOver : BattleDisplayEvent 
    {
        public ReplayOver(BattleRunnerArgs args) : base(BattleEventType.ReplayOver, args) 
        {
        }

        #region Public Methods
        public override void OnStartRunning( )
        {
            ReplayManager.Get.NotifyReplayOver();
            OnFinishRunning();
        }

        public override void OnFinishRunning()
        {   
            base.OnFinishRunning();
        }
        #endregion
    }
}