﻿using UnityEngine;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	[Serializable]
	public class DrawCard : BattleDisplayEvent 
	{
		#region Member Variables
		private readonly DrawCardData mDrawCardData;
		#endregion

		#region Constructors
		public DrawCard( DrawCardData drawCardData, BattleRunnerArgs args ) : base(BattleEventType.DrawCard, args) 
		{
			mDrawCardData = drawCardData;
		}
		#endregion

		#region Member Variables
		private BattleEntity mDrawingBattleEntity;
		#endregion

		#region Public Methods
		public override void OnStartRunning( )
		{
			bool isLocalPlayer = IsLocalPlayerId( mDrawCardData.DrawingPlayerId );
			PlayerBattleSceneView drawingCardView = isLocalPlayer ? LocalSceneView : RemoteSceneView;

			mDrawingBattleEntity = BattleEntityFactory.ConstructBattleEntity( mDrawCardData.EntityData, isLocalPlayer ? PlayerType.Local : PlayerType.Remote, drawingCardView.PlayerHandView.transform );
			mDrawingBattleEntity.OnRefreshEffectWithData( mDrawCardData.EntityData );
			mDrawingBattleEntity.OnRefreshSpriteWithData( mDrawCardData.EntityData );

			drawingCardView.PlayerHandView.Insert( new int[]{ drawingCardView.PlayerHandView.Count }, new BattleEntity[]{ mDrawingBattleEntity } );
			mDrawingBattleEntity.OnInsertState( BattleEntityController.State.CardDrawing );

			RegisterBattleEntityListeners();
		}

		public override void OnFinishRunning()
		{
			//Unregister the listeners
			UnregisterBattleEntityListeners();

			mDrawingBattleEntity.OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );

			base.OnFinishRunning();
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			mDrawingBattleEntity.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			mDrawingBattleEntity.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			OnFinishRunning();
		}
		#endregion
	}
}