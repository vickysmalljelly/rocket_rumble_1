﻿using System.Collections.Generic;
using System.Linq;
using System;

using SmallJelly.Framework;

using UnityEngine;


namespace SmallJelly
{
	/// <summary>
	/// Data to be displayed for a player during a battle.
	/// </summary>
	[Serializable]
	public class BattleRunnerData
	{
		#region Public Properties
		public string Id
		{
			get
			{
				return mId;
			}
		}

		public PlayerBattleData LocalPlayerBattleData
		{
			get
			{
				return mLocalPlayerBattleData;
			}
		}

		public PlayerBattleData RemotePlayerBattleData
		{
			get
			{
				return mRemotePlayerBattleData;
			}
		}

		public PlayerTargetingBattleData LocalPlayerTargetingBattleData
		{
			get
			{
				return mLocalPlayerTargetingBattleData;
			}
		}

		public PlayerTargetingBattleData RemotePlayerTargetingBattleData
		{
			get
			{
				return mRemotePlayerTargetingBattleData;
			}
		}

        /// <summary>
        /// Legacy member variable to support old way where json is parsed in BattleRunnerFactory
        /// </summary>
        /// <value>The runner JSO.</value>
		public SJJson LegacyRunnerJSON
		{
			get
			{
				return mLegacyRunnerJSON;
			}
		}
		#endregion

		#region Member Variables
		private readonly string mId;
		private readonly PlayerBattleData mLocalPlayerBattleData;
		private readonly PlayerBattleData mRemotePlayerBattleData;

		private readonly PlayerTargetingBattleData mLocalPlayerTargetingBattleData;
		private readonly PlayerTargetingBattleData mRemotePlayerTargetingBattleData;

        private readonly SJJson mLegacyRunnerJSON;
		#endregion

		#region Constructors
		public BattleRunnerData( string id, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, SJJson runnerJSON )
		{
			mId = id;

			mLocalPlayerBattleData = localPlayerBattleData;
			mRemotePlayerBattleData = remotePlayerBattleData;

			mLocalPlayerTargetingBattleData = localPlayerTargetingBattleData;
			mRemotePlayerTargetingBattleData = remotePlayerTargetingBattleData;

			mLegacyRunnerJSON = runnerJSON;
		}
		#endregion
	}
}
