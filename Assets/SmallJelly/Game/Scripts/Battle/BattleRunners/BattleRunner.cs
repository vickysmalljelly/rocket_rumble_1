﻿using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public enum BattleEventType
	{
		NotSet,
		DrawCard,
		InstallComponentSuccess,
		RemotePlayCard,
		PlayCardFail,
		FireComponent,
		TakeDamage,
		HullTakeDamage,
		HullDestroyed,
		UpdatePower,
		DestroyComponent,
		StateChange,
		JettisonSuccess,
		JettisonFail,
		Repair,
		Buff,
		Empty,
        ReplayOver
	}
		
	public abstract class BattleRunner 
	{
		#region Public Event Handlers
		public event EventHandler<BattleRunnerEventArgs> StartedRunning;
		public event EventHandler<BattleRunnerEventArgs> FinishedRunning;
		#endregion

		#region Public Properties
		public BattleEventType BattleEventType { get; private set; }

		public PlayerBattleSceneView LocalSceneView { get; private set; }
		public PlayerBattleData LocalBeforeData { get; private set; }
		public PlayerBattleData LocalAfterData { get; private set; }

		public PlayerBattleSceneView RemoteSceneView { get; private set; }
		public PlayerBattleData RemoteBeforeData { get; private set; }
		public PlayerBattleData RemoteAfterData { get; private set; }
		#endregion

		#region Constructors
		protected BattleRunner(BattleEventType eventType, BattleRunnerArgs args)
		{
			BattleEventType = eventType;

			LocalSceneView = args.LocalSceneView;
			LocalBeforeData = args.LocalDataBeforeEvent;
			LocalAfterData = args.LocalDataAfterEvent;

			RemoteSceneView = args.RemoteSceneView;
			RemoteBeforeData = args.RemoteDataBeforeEvent;
			RemoteAfterData = args.RemoteDataAfterEvent;
		}
		#endregion

		#region Public Methods
		public virtual void OnStartRunning()
		{
			FireStartedRunning();
		}

		public virtual void OnFinishRunning()
		{
			FireFinishedRunning();
		}

		public override string ToString()
		{
			return string.Format("[BattleEvent: BattleEventType={0}]", BattleEventType);
		}
		#endregion

		#region Protected Methods
		protected bool IsLocalPlayerId( string playerId )
		{
			return playerId == ChallengeManager.Get.LocalPlayer.UserId;
		}
		#endregion

		#region Event Firing
		public void FireStartedRunning()
		{	
			if( StartedRunning != null )
			{
				StartedRunning( this, new BattleRunnerEventArgs( this ) );
			}
		}

		public void FireFinishedRunning()
		{	
			if( FinishedRunning != null )
			{
				FinishedRunning( this, new BattleRunnerEventArgs( this ) );
			}
		}
		#endregion
	}
}
