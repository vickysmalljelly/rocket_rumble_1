﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace SmallJelly
{
	public class BattleRunnerFactory
	{
		#region Public Constants
		public const string HULL_TAKE_DAMAGE = "HULL_TAKE_DAMAGE";
		public const string REPLAY_OVER = "REPLAY_OVER";
		public const string MULLIGAN_OVER = "MULLIGAN_OVER";
		public const string REPAIR_COMPONENT = "REPAIR_COMPONENT";
		public const string REPAIR_HULL = "REPAIR_HULL";
		public const string JETTISON = "JETTISON";
		public const string JETTISON_FAIL_POWER = "JETTISON_FAIL_POWER";
		public const string STEAL_COMPONENT_TO_HAND = "STEAL_COMPONENT_TO_HAND";
		public const string REMOTE_PLAY_CARD = "REMOTE_PLAY_CARD";
		public const string NANOTECH_PLAYED = "CREW_CARD_PLAYED";
		public const string WEAPON_CANT_FIRE = "WEAPON_CANT_FIRE";
		public const string PLAY_CARD_FAIL_POWER = "PLAY_CARD_FAIL_POWER";
		public const string COMPONENT_INSTALL_FAIL_SLOT_IS_FULL = "COMPONENT_INSTALL_FAIL_SLOT_IS_FULL";
		public const string POWER_UPDATED = "POWER_UPDATED";
		public const string COMPONENT_INSTALL_SUCCESS = "COMPONENT_INSTALL_SUCCESS";
		public const string COMPONENT_SPAWN_SUCCESS = "COMPONENT_SPAWN_SUCCESS";
		public const string BATTLE_DRAW_CARD = "BATTLE_DRAW_CARD";
		public const string FIRE_AT_COMPONENT = "FIRE_AT_COMPONENT";
		public const string FIRE_AT_HULL = "FIRE_AT_HULL";
        public const string TARGET_LOCK_PREVENTS_FIRE = "TARGET_LOCK_PREVENTS_FIRE";
		public const string COMPONENT_TAKE_DAMAGE = "COMPONENT_TAKE_DAMAGE";
		public const string COMPONENT_DESTROYED = "COMPONENT_DESTROYED";
		public const string HULL_DESTROYED = "HULL_DESTROYED";
		public const string COMPONENT_BUFF_ATTACK = "COMPONENT_BUFF_ATTACK";
		public const string COMPONENT_BUFF_HP = "COMPONENT_BUFF_HP";
		public const string COMPONENT_BUFF_HP_REMOVED = "COMPONENT_BUFF_HP_REMOVED";
		public const string WEAPON_HAS_NO_ATTACK = "WEAPON_HAS_NO_ATTACK";
		public const string BUFF_POWER_COST = "BUFF_POWER_COST";
		public const string ADD_DOUBLE_SHOT = "ADD_DOUBLE_SHOT";
		public const string ADD_SHIELD = "ADD_SHIELD";
		public const string REMOVE_SHIELDS = "REMOVE_SHIELDS";
		public const string DESTROY_SHIELD = "DESTROY_SHIELD";
		public const string OUT_OF_CARDS = "OUT_OF_CARDS";
		public const string CARD_TEXT_FIRE_AT_HULL = "CARD_TEXT_FIRE_AT_HULL";
		public const string CARD_TEXT_FIRE_AT_SOCKET = "CARD_TEXT_FIRE_AT_SOCKET";
        public const string ADD_TARGET_LOCK = "ADD_TARGET_LOCK";

		#endregion

		#region Public Methods
		public static Queue< BattleRunner > GetBattleRunnerQueue ( BattleRunnerData battleRunnerData, BattleRunnerArgs args )
		{
			string id = battleRunnerData.Id;
			SJJson properties = battleRunnerData.LegacyRunnerJSON;

			switch (id) 
			{
			//Moved over to the system of just accepting battleRunnerData
			case HULL_TAKE_DAMAGE:
				return GetHullTakeDamage( battleRunnerData, args );

			case COMPONENT_INSTALL_SUCCESS:
				return GetInstallComponent( battleRunnerData, args );

			case BATTLE_DRAW_CARD:
				return GetBattleDrawCard ( battleRunnerData, args );

			case COMPONENT_TAKE_DAMAGE:
				return GetComponentTakeDamage ( battleRunnerData, args );

			case JETTISON:
				return GetJettisonSuccess ( battleRunnerData, args );

			case NANOTECH_PLAYED:
				return GetNanotechPlayed( battleRunnerData, args );

			case HULL_DESTROYED:
				return GetHullDestroyed ( battleRunnerData, args);

			//TODO - Yet to move over to the system of just accepting battle runner data
            case ADD_TARGET_LOCK:
                    return GetAddTargetLock (id, properties, args);

            case REPLAY_OVER:
                    return GetReplayOver (id, properties, args);

			case MULLIGAN_OVER:
				return GetMulliganOver (id, properties, args);

			case REPAIR_COMPONENT:
				return GetRepairComponent (id, properties, args);

			case REPAIR_HULL:
				return GetRepairHull (id, properties, args);

			case JETTISON_FAIL_POWER:
				return GetJettisonFailPower (id, properties, args);

			case STEAL_COMPONENT_TO_HAND:
				return GetStealComponentToHand (id, properties, args);

			case REMOTE_PLAY_CARD:
				return GetRemotePlayCard (id, properties, args);

			case WEAPON_CANT_FIRE:
				return GetWeaponCantFire (id, properties, args);

			case PLAY_CARD_FAIL_POWER:
			case COMPONENT_INSTALL_FAIL_SLOT_IS_FULL:
				return GetPlayCardFail (id, properties, args);

			case POWER_UPDATED:
				return GetPowerUpdated (id, properties, args);

			case COMPONENT_SPAWN_SUCCESS:
				return GetSpawnComponentSuccess (id, properties, args);

			case FIRE_AT_COMPONENT:
				// TODO distinct runner for firing at a component
				return GetFireAtComponent (id, properties, args);

			case FIRE_AT_HULL:
				// TODO distinct runner for firing at the hull
				return GetFireAtHull (id, properties, args);

            case TARGET_LOCK_PREVENTS_FIRE:
                return GetTargetLockPreventsFire (id, properties, args);

			case COMPONENT_DESTROYED:
				return GetComponentDestroyed (id, properties, args);

			case COMPONENT_BUFF_ATTACK:
				return GetBuffAttack (id, properties, args);

			case COMPONENT_BUFF_HP:
				return GetBuffHp (id, properties, args);

			case COMPONENT_BUFF_HP_REMOVED:
				return GetBuffHpRemoved (id, properties, args);

			case WEAPON_HAS_NO_ATTACK:
				return GetHasNoAttack (id, properties, args);

			case BUFF_POWER_COST:
				return GetBuffPowerCost (id, properties, args);

			case ADD_DOUBLE_SHOT:
				return GetAddDoubleShot (id, properties, args);

			case ADD_SHIELD:
				return GetAddShield (id, properties, args);

			case REMOVE_SHIELDS:
				return GetRemoveShields (id, properties, args);

			case DESTROY_SHIELD:
				return GetDestroyShield (id, properties, args);

            case OUT_OF_CARDS:
                return GetOutOfCards (id, properties, args);

            case CARD_TEXT_FIRE_AT_HULL:
                return GetCardTextFireAtHull (id, properties, args);

            case CARD_TEXT_FIRE_AT_SOCKET:
                return GetCardTextFireAtSocket (id, properties, args);

			default:
				SJLogger.LogError ( "Failed to recognise player battle event {0}", id);
				return new Queue< BattleRunner > ( new []{ new EmptyRunner( args ) } );
			}
		}

		#endregion

		#region Private Methods
		private static Queue< BattleRunner > GetJettisonSuccess ( BattleRunnerData battleRunnerData, BattleRunnerArgs args )
		{
			return new Queue< BattleRunner > ( new []{ new ComponentJettisonSuccess ( ( ComponentJettisonData )battleRunnerData,  args ) } );
		}

		private static Queue< BattleRunner > GetNanotechPlayed( BattleRunnerData battleRunnerData, BattleRunnerArgs args )
		{
			return new Queue< BattleRunner > ( new []{ new NanotechPlayed( ( NanotechPlayedData ) battleRunnerData, args ) } );
		}


		private static Queue< BattleRunner > GetAddShield (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "AddShield: player {0}, slot {1}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1));

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetRemoveShields (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			List<SJJson> shields = properties.GetSJJsonList ( "shields" );

			SJLogger.LogMessage (MessageFilter.Battle, "RemoveShield: player {0}, shields {1}",
				playerId,
				shields.ToString ());

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetDestroyShield (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "DestroyShield: player {0}, slot {1}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1));

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetAddDoubleShot (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Component has been given DoubleShot: player {0}, slot {1}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1));

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}


		private static Queue< BattleRunner > GetBuffPowerCost (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Power cost buff: player {0}", playerId);

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		// The player has tried to fire a weapon that has no attack value
		private static Queue< BattleRunner > GetHasNoAttack (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Component has no attack: player {0}, slot {1}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1)
			);

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetBuffAttack (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );
			int? buffAmount = (int)properties.GetInt ( "buffAmount" );

			SJLogger.LogMessage (MessageFilter.Battle, "Component buff attack player {0}, slot {1}, amount {2}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1),
				(buffAmount.HasValue ? buffAmount.GetValueOrDefault () : -1)
			);

			return new Queue< BattleRunner > ( new []{ new ComponentBuff ( args, playerId, slotId.GetValueOrDefault() ) } );
		}

		private static Queue< BattleRunner > GetBuffHp (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );
			int? buffAmount = (int)properties.GetInt ( "buffAmount" );

			SJLogger.LogMessage (MessageFilter.Battle, "Component buff hp player {0}, slot {1}, amount {2}",
				playerId,
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1),
				(buffAmount.HasValue ? buffAmount.GetValueOrDefault () : -1)
			);

			return new Queue< BattleRunner > ( new []{ new ComponentBuff ( args, playerId, slotId.GetValueOrDefault() ) } );
		}

		private static Queue< BattleRunner > GetBuffHpRemoved (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Component buff hp player {0}",
				playerId
			);

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetMulliganOver (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Mulligan over: player {0}", playerId);

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}
            
        private static Queue< BattleRunner > GetAddTargetLock (string id, SJJson properties, BattleRunnerArgs args)
        {
            int drawNumber = properties.GetInt ( "drawNumber" ).GetValueOrDefault();

            SJLogger.LogMessage (MessageFilter.Battle, "Add target lock: drawNumber {0}", drawNumber);

            return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
        }

        private static Queue< BattleRunner > GetTargetLockPreventsFire (string id, SJJson properties, BattleRunnerArgs args)
        {
            // This event is triggered whenenver the player tries to fire at anything when there is a component 
            // with target lock present on the board.
            // If the player is firing at the ship, socketId will be -1
            string playerId = properties.GetString ( "playerId" );
            int socketId = properties.GetInt ( "socketId" ).GetValueOrDefault();

            SJLogger.LogMessage (MessageFilter.Battle, "Target lock prevents fire: socketId {0}, playerId {1}", socketId, playerId);

            return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
        }

        private static Queue< BattleRunner > GetReplayOver (string id, SJJson properties, BattleRunnerArgs args)
        {
            SJLogger.LogMessage (MessageFilter.Battle, "Replay over");

            return new Queue< BattleRunner > ( new []{ new ReplayOver ( args ) } );
        }

		private static Queue< BattleRunner > GetRepairComponent (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Repair component {0}, player {1}",
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1), playerId);


			return new Queue< BattleRunner > ( new []{ new ComponentRepair ( args, playerId, slotId.GetValueOrDefault() ) } );
		}

		private static Queue< BattleRunner > GetRepairHull (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			SJLogger.LogMessage (MessageFilter.Battle, "Repair hull, player {0}", playerId);

			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetJettisonFailPower (string id, SJJson properties, BattleRunnerArgs args)
		{
			int? slotId = (int)properties.GetInt ( "slotId" );

			SJLogger.LogMessage (MessageFilter.Battle, "Jettison fail power for slot {0}",
				(slotId.HasValue ? slotId.GetValueOrDefault () : -1));

			return new Queue< BattleRunner > ( new []{ new ComponentJettisonFail (args, ComponentJettisonFail.Reason.NotEnoughPower) } );
		}

		private static Queue< BattleRunner > GetStealComponentToHand (string id, SJJson properties, BattleRunnerArgs args)
		{
			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

        private static Queue< BattleRunner > GetCardTextFireAtHull (string id, SJJson properties, BattleRunnerArgs args)
        {
			string sourcePlayerId = properties.GetString ( "attackingPlayerId" );
			string defendingPlayerId = properties.GetString ( "defendingPlayerId" );

			int? sourceSocketId = (int)properties.GetInt ( "fromSocketId" );

            if(sourceSocketId == -1)
            {
                SJLogger.LogWarning("Server was not able to identify the source component, If the source of the card text was NOT nanotech PLEASE SUBMIT THIS AS A BUG along with details of the card text that was activated.");
                return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
            }

			return new Queue< BattleRunner > ( new []{ new CardTextFireAtHullSuccess ( args, sourcePlayerId, defendingPlayerId, sourceSocketId.GetValueOrDefault() ) } );
        }

        private static Queue< BattleRunner > GetCardTextFireAtSocket (string id, SJJson properties, BattleRunnerArgs args)
        {
			string sourcePlayerId = properties.GetString ( "attackingPlayerId" );
			string defendingPlayerId = properties.GetString ( "defendingPlayerId" );

			int? sourceSocketId = (int)properties.GetInt ( "fromSocketId" );

            if(sourceSocketId == -1)
            {
                SJLogger.LogWarning("Server was not able to identify the source component, If the source of the card text was NOT nanotech PLEASE SUBMIT THIS AS A BUG along with details of the card text that was activated.");
                return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
            }

			int? destinationSocketId = (int)properties.GetInt ( "toSocketId" );

			return new Queue< BattleRunner > ( new []{ new CardTextFireAtComponentSuccess ( args, sourcePlayerId, sourceSocketId.GetValueOrDefault(), defendingPlayerId, destinationSocketId.GetValueOrDefault() ) } );
        }

        private static Queue< BattleRunner > GetOutOfCards (string id, SJJson properties, BattleRunnerArgs args)
        {
            string playerId = properties.GetString ( "playerId" );

            SJLogger.LogMessage (MessageFilter.Battle, "Player {0} out of cards", playerId);
            
			return new Queue< BattleRunner > ( new []{ new OutOfCards ( args, playerId ) } );
        }

		private static Queue< BattleRunner > GetRemotePlayCard (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
            int? drawNumber = (int)properties.GetInt ( "drawNumber" );

            SJLogger.LogMessage (MessageFilter.Battle, "playerId:{0}, drawNumber:{1}", playerId, drawNumber.GetValueOrDefault()); 

			// TODO Add client side logic for a "playing" animation

			return new Queue< BattleRunner > ( new BattleDisplayEvent[]{ new EmptyRunner ( args ), new RemotePlayCardRunner( drawNumber.GetValueOrDefault(), args ) } );
		}
          
		private static Queue< BattleRunner > GetWeaponCantFire (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			int slotId = (int)properties.GetInt ( "slotId" );
			SJLogger.LogMessage (MessageFilter.Battle, "playerId:{0}, slotId:{1}", playerId, slotId); 

			// TODO Add client side logic
			return new Queue< BattleRunner > ( new []{ new EmptyRunner( args ) } );
		}

		private static Queue< BattleRunner > GetPlayCardFail( string id, SJJson properties, BattleRunnerArgs args )
		{
			int drawNumber = (int)properties.GetInt( "drawNumber" );
          	string playerId = properties.GetString( "installedOnPlayerId" );

			switch(id) 
            {
			    case PLAY_CARD_FAIL_POWER:
                {
					return new Queue< BattleRunner > ( new []{ new PlayCardFail( args, playerId, drawNumber, -1, PlayCardFail.Reason.NotEnoughPower ) } );
                }
			    case COMPONENT_INSTALL_FAIL_SLOT_IS_FULL:
                {
                    int slotId = (int)properties.GetInt( "slotId" );
					return new Queue< BattleRunner > ( new []{ new PlayCardFail( args, playerId, drawNumber, slotId, PlayCardFail.Reason.SlotIsFull ) } );
                }
                default:
                {
                    SJLogger.LogError( "PlayCardFail.Reason {0} not recognised", id );
                    return null;
                }
			}
		}

		private static Queue< BattleRunner > GetPowerUpdated (string id, SJJson properties, BattleRunnerArgs args)
		{
			string playerId = properties.GetString ( "playerId" );
			return new Queue< BattleRunner > ( new []{ new UpdatePower (args, playerId) } );
		}

		private static Queue< BattleRunner > GetInstallComponent ( BattleRunnerData battleRunnerData, BattleRunnerArgs args)
		{
			SJLogger.LogMessage( MessageFilter.George, "Create Runner" );

			return new Queue< BattleRunner > ( new []{ new InstallComponent( (InstallComponentData)battleRunnerData, args ) } );
		}

		private static Queue< BattleRunner > GetSpawnComponentSuccess (string id, SJJson properties, BattleRunnerArgs args)
		{
			return new Queue< BattleRunner > ( new []{ new EmptyRunner ( args ) } );
		}

		private static Queue< BattleRunner > GetBattleDrawCard( BattleRunnerData battleRunnerData, BattleRunnerArgs args )
		{			
			return new Queue< BattleRunner > ( new []{ new DrawCard( (DrawCardData)battleRunnerData, args ) } );
		}

		private static Queue< BattleRunner > GetFireAtComponent (string id, SJJson properties, BattleRunnerArgs args)
		{
			string attackingPlayerId = properties.GetString ( "attackingPlayerId" );
			int fromSlotId = (int)properties.GetInt ( "fromSlotId" );
			string defendingPlayerId = properties.GetString ( "defendingPlayerId" );
			int toSlotId = (int)properties.GetInt ( "toSlotId" );

			SJJson hitPos = properties.GetObject ( "hitPos" );

			SJLogger.LogMessage (MessageFilter.Battle, "hitPos: {0}, defendingPlayer {1}, toSlotId {2}", hitPos.JSON, defendingPlayerId, toSlotId);

			float xCord = float.Parse (hitPos.GetString ( "x" ));
			float yCord = float.Parse (hitPos.GetString ( "y" ));
			float zCord = float.Parse (hitPos.GetString ( "z" ));

			Vector3 hitVector = new Vector3 (xCord, yCord, zCord);

			return new Queue< BattleRunner > ( new []{ new ComponentFireAtComponentSuccess (args, attackingPlayerId, fromSlotId, defendingPlayerId, toSlotId, hitVector) } );
		}

		private static Queue< BattleRunner > GetFireAtHull (string id, SJJson properties, BattleRunnerArgs args)
		{
			string attackingPlayerId = properties.GetString ( "attackingPlayerId" );
			int fromSlotId = (int)properties.GetInt ( "fromSlotId" );

			SJJson hitPos = properties.GetObject ( "hitPos" );
			float xCord = float.Parse (hitPos.GetString ( "x" ));
			float yCord = float.Parse (hitPos.GetString ( "y" ));
			float zCord = float.Parse (hitPos.GetString ( "z" ));

			Vector3 hitVector = new Vector3 (xCord, yCord, zCord);

			SJLogger.LogMessage (MessageFilter.Battle, "hitPos: {0}", hitPos.JSON);

			return new Queue< BattleRunner > ( new []{ new ComponentFireAtHullSuccess (args, attackingPlayerId, fromSlotId, hitVector) } );
		}
			
		private static Queue< BattleRunner > GetComponentDestroyed (string id, SJJson properties, BattleRunnerArgs args)
		{
			string defendingPlayerId = properties.GetString ( "defendingPlayerId" );
			int targetSlotId = (int)properties.GetInt ( "targetSlotId" );

			return new Queue< BattleRunner > ( new []{ new ComponentDestroy (args, defendingPlayerId, targetSlotId) } );
		}

		private static Queue< BattleRunner > GetHullTakeDamage ( BattleRunnerData battleRunnerData, BattleRunnerArgs args )
		{
			return new Queue< BattleRunner > ( new []{ new HullTakeDamage ( (HullTakeDamageData) battleRunnerData, args ) } );
		}


		private static Queue< BattleRunner > GetComponentTakeDamage ( BattleRunnerData battleRunnerData, BattleRunnerArgs args)
		{
			return new Queue< BattleRunner > ( new []{ new TakeDamage ( (ComponentTakeDamageData) battleRunnerData, args ) } );
		}

		private static Queue< BattleRunner > GetHullDestroyed ( BattleRunnerData battleRunnerData, BattleRunnerArgs args)
		{
			return new Queue< BattleRunner > ( new []{ new HullDestroyed ( (HullDestroyedData)battleRunnerData, args ) } );
		}

		#endregion
	}
}
