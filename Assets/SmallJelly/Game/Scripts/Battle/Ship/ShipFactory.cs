﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.IO;

namespace SmallJelly
{
	public class ShipFactory
	{
		#region Public Methods
		public static Ship ConstructShip( PlayerType playerType, string shipClass )
		{
			GameObject view = LoadShipView( playerType, shipClass );

			Ship ship = AssignComponents( view );
			AdjustScale( playerType,  ship );

			ship.OnInsertState( ShipController.ShipState.ShipIdle );

			return ship;
		}
		#endregion

		#region Private Methods
		private static GameObject LoadShipView( PlayerType playerType, string shipClass )
		{
			GameObject resource = GetShipResource( playerType, shipClass );
			return UnityEngine.Object.Instantiate( resource );

		}

		private static GameObject GetShipResource( PlayerType playerType, string shipClass )
		{
			//TODO - This method is only temporary - ships won't be hardcoded but will be loaded from the data
			//once we have multiple ships within each class (different skins etc)
			return Resources.Load< GameObject >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyShipPrefabs, Path.DirectorySeparatorChar, GetShipName( playerType, shipClass ) ) );
		}

		private static string GetShipName( PlayerType playerType, string shipClass )
		{
			if( playerType == PlayerType.Remote )
			{
				switch(BattleModeManager.Get.CurrentBattleId)
				{
					case BattleId.Tutorial1:
						return "ship_tutorialDrone01";

					case BattleId.Tutorial2:
						return "ship_tutorialDrone02";

					case BattleId.Tutorial3:
						return "ship_tutorialDrone03";
				}
			}

			switch( shipClass )
			{
				case ShipClass.Ancient: 
					return "ship_anc_tenebrae";

				case ShipClass.Enforcer: 
					return "ship_enf_prowler";

				case ShipClass.Smuggler: 
					return "ship_smu_jackrabbit";

				case ShipClass.Debug: 
					return "ship_anc_tenebrae";

				default:
					SJLogger.LogError("Ship class {0} not recognised", shipClass);
					return "ship_anc_tenebrae";
			}
		}

		private static void AdjustScale( PlayerType playerType, Ship ship )
		{
			switch( playerType )
			{
				case PlayerType.Local:
					//No change to scale
					break;

				case PlayerType.Remote:
					ship.transform.localScale = new Vector3( -1, 1, 1 );
					ship.SocketTemplate.transform.localScale = new Vector3( -1, 1, 1 );
					break;

				default:
					SJLogger.Assert( "Player is of an undefined type" );
					break;
			}
		}

		private static Ship AssignComponents( GameObject view )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the ship (the ship handles the views)
			view.gameObject.SetActive ( false );

			AssignAnimationControllers( view );
			AssignViews( view );
			AssignShipController( view );
			Ship ship = AssignShip( view );

			view.gameObject.SetActive ( true );

			return ship;
		}

		private static void AssignAnimationControllers( GameObject view )
		{
			view.AddComponent< ShipAnimationController > ();
		}

		private static void AssignViews( GameObject view )
		{
			ShipView shipView = view.AddComponent< ShipView > ();
			shipView.ShipAnimationController = view.GetComponent< ShipAnimationController > ();		
		}

		private static void AssignShipController( GameObject view )
		{
			ShipController shipController = view.AddComponent< ShipController >();
			shipController.ShipView = view.GetComponent< ShipView >();
		}

		private static Ship AssignShip( GameObject view )
		{
			//Add ship component
			Ship ship = view.AddComponent< Ship >();
			ship.ShipController = view.GetComponent< ShipController >();

			return ship;
		}
		#endregion
	}
}
