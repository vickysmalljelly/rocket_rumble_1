﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ShipController : TargetableLogicController 
	{
		#region Enums
		public enum ShipState
		{
			None,
			ShipIdle,
			ShipTakeDamage,
			ShipWon,
			ShipLost
		}
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished;
		#endregion

		#region Public Properties
		public ShipView ShipView
		{
			private get;
			set;
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private ShipState mCurrentShipState;
		#endregion


		#region Member Variables
		private Stack<ShipState> mPreviousStates;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			mPreviousStates = new Stack<ShipState>();
		}

		public void OnEnable()
		{
			RegisterListeners();
		}

		public void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( ShipState shipState  )
		{
			OnInsertState( shipState, new AnimationMetaData( new Dictionary< string, NamedVariable >() ) );
		}

		public void OnInsertState( ShipState shipState, AnimationMetaData animationMetaData )
		{
			UpdateState( shipState, animationMetaData );
		}

		public virtual void OnLeaveState( ShipState shipState )
		{
			
		}

		public virtual void OnReturnToPreviousState()
		{
			mPreviousStates.Pop();

			SJLogger.AssertCondition( mPreviousStates.Count > 0, "The battle entities previous state stack should never have 0 elements in it at this point in time" );

			OnInsertState( mPreviousStates.Pop() );
		}
		#endregion


		#region Mouse Controls
		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		#endregion



		#region Touch Controls
		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDraggedOver( elementsHit, movementDuringFrame );
		}

		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDragged( elementsHit, movementDuringFrame );
		}

		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDraggedOff( elementsHit, movementDuringFrame );
		}


		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMousePressedOver( elementsHit, movementDuringFrame );
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseRolledOver( elementsHit, movementDuringFrame );
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseReleasedOff( elementsHit, movementDuringFrame );
		}
		#endregion

		#region Private Methods
		private void UpdateState( ShipController.ShipState shipState, AnimationMetaData animationMetaData )
		{
			//If we're leaving a state
			if( mCurrentShipState != ShipState.None )
			{
				OnLeaveState( mCurrentShipState );
			}

			mCurrentShipState = shipState;

			mPreviousStates.Push( shipState );

			ShipView.OnPlayAnimation( shipState, animationMetaData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ShipView.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			ShipView.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			FireAnimationFinished();
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}