﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class Ship : Targetable 
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				ShipController.AnimationFinished += value;
			}

			remove
			{
				ShipController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public Transform SocketTemplate
		{
			get
			{
				return mSocketTemplate;
			}
		}

		public Socket[] BattleEntitySockets
		{
			get
			{
				return mBattleEntitySlots;
			}
		}

		public ShipBattleData ShipData
		{
			get 
			{
				return mShipData;
			}
		}

		public ShipController ShipController
		{
			private get
			{
				return (ShipController)TargetableLogicController;
			}

			set
			{
				TargetableLogicController = value;
			}
		}
		#endregion

		#region Member Properties
		private Transform mSocketTemplate;
		#endregion

		#region Constants
		private const string SOCKET_PARENT_TRANSFORM_NAME = "SocketsPosition";
		private const string SOCKET_POSITION_TRANSFORM_NAME = "Positions";
		private const string TEMPLATE_PREFAB_RESOURCE_LOCATION = "Prefabs/Sockets/socket_grid";
		private const string SLOT_PREFAB_RESOURCE_LOCATION = "Prefabs/Sockets/socket_socket";
		#endregion

		#region Member Variables
		private ShipBattleData mShipData;
		private Socket[] mBattleEntitySlots;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			mBattleEntitySlots = LoadSlots();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( ShipController.ShipState shipState )
		{
			ShipController.OnInsertState( shipState );
		}

		public void OnInsertState( ShipController.ShipState shipState, AnimationMetaData animationMetaData )
		{
			ShipController.OnInsertState( shipState, animationMetaData );
		}

		public void OnReturnToPreviousState( )
		{
			ShipController.OnReturnToPreviousState( );
		}

		public void Refresh( ShipBattleData shipData )
		{
			mShipData = shipData;
		}
		#endregion

		#region Private Methods
		private Socket[] LoadSlots(  )
		{
			GameObject templateResource = Resources.Load<GameObject>( TEMPLATE_PREFAB_RESOURCE_LOCATION );
			mSocketTemplate = Instantiate( templateResource ).transform;

			Transform socketParent = SJRenderTransformExtensions.FindDeepChild( transform, SOCKET_PARENT_TRANSFORM_NAME );

			SJLogger.AssertCondition( socketParent != null, "Socket parent transform wasn't found in the ship, it should have the name {0}", SOCKET_PARENT_TRANSFORM_NAME );

			SJRenderTransformExtensions.AddChild( socketParent, mSocketTemplate, true, true, false );

			Transform[] socketParents = mSocketTemplate.FindChild( SOCKET_POSITION_TRANSFORM_NAME ).Cast<Transform>().OrderBy(t=>t.GetSiblingIndex()).ToArray();
			Socket[] sockets = new Socket[ socketParents.Length ];

			GameObject socketResource = Resources.Load<GameObject>( SLOT_PREFAB_RESOURCE_LOCATION );
			for( int i = 0; i < socketParents.Length; i++ )
			{
				Socket socket = ( Instantiate( socketResource, Vector3.zero, Quaternion.identity ) as GameObject ).GetComponent<Socket>();
				SJRenderTransformExtensions.AddChild( socketParents[ i ], socket.transform, true, true, false );

				socket.transform.name = string.Format("Socket{0}", i);

				sockets[i] = socket;
			}

			return sockets;
		}
		#endregion
	}
}