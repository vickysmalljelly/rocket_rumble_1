﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ShipAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Ships/"; } }
		#endregion

		#region Constants
		private const string IDLE_ANIMATION_TEMPLATE_NAME = "IdleAnimation";
		private const string TAKE_DAMAGE_ANIMATION_TEMPLATE_NAME = "TakeDamageAnimation";
		private const string WIN_ANIMATION_TEMPLATE_NAME = "WinAnimation";
		private const string LOSE_ANIMATION_TEMPLATE_NAME = "LoseAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( ShipController.ShipState shipState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(shipState)
			{
				case ShipController.ShipState.ShipIdle:
					PlayAnimation( IDLE_ANIMATION_TEMPLATE_NAME );
					break;

				case ShipController.ShipState.ShipTakeDamage:
					PlayAnimation( TAKE_DAMAGE_ANIMATION_TEMPLATE_NAME );
					break;

				case ShipController.ShipState.ShipWon:
					PlayAnimation( WIN_ANIMATION_TEMPLATE_NAME );
					break;

				case ShipController.ShipState.ShipLost:
					PlayAnimation( LOSE_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
