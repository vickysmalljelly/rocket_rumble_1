﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ShipView : SJMonoBehaviour 
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				ShipAnimationController.AnimationFinished += value;
			}

			remove
			{
				ShipAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public ShipAnimationController ShipAnimationController
		{
			private get;
			set;
		}
		#endregion

		#region Member Variables
		private Collider[] mColliders;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
			//Add the animation controller
			ShipAnimationController = gameObject.AddComponent<ShipAnimationController>();

			mColliders = GetComponentsInChildren<Collider>();
		}

		public virtual void OnEnable()
		{
			EnableColliders();
		}

		public virtual void OnDisable()
		{
			DisableColliders();
		}
		#endregion

		#region Methods
		public void OnPlayAnimation( ShipController.ShipState shipState, AnimationMetaData animationMetaData )
		{
			ShipAnimationController.UpdateAnimations( shipState, animationMetaData );
		}
	
		public void EnableColliders()
		{
			foreach( Collider collider in mColliders )
			{
				collider.enabled = true;
			}
		}

		public void DisableColliders()
		{
			foreach( Collider collider in mColliders )
			{
				collider.enabled = false;
			}
		}
		#endregion
	}
}