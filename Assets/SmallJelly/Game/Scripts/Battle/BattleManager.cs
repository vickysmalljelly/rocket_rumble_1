using SmallJelly.Framework;
using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Timers;

namespace SmallJelly
{
	/// <summary>
	/// Singleton that exists for the lifetime of a battle.
	/// Contains the data to be displayed, and listens for updates.
	/// </summary>
	public class BattleManager : MonoBehaviourSingleton<BattleManager>
	{
		#region Public Properties
		public PlayerType CurrentTurn
		{
			get
			{
				return mCurrentTurn;
			}
		}
            
		public BattleConfig BattleConfig
		{
			get
			{
                return mChallenge.BattleConfig;
			}
		}

		public int TurnCount
		{
			get
			{
				return mTurnCount;
			}
		}

        /// <summary>
        /// True when there are no more animations and effects to be processed in a battle
        /// </summary>
		public bool AnimationsHaveFinished
		{
			get
			{
                return mBattleGameplayController.AnimationsHaveFinished;
			}
		}

		public BattleResult BattleResult
		{
			get
			{
				return BattleResult;
			}
		}

		public BattleIntroController BattleIntroController
		{
			get
			{
				return mBattleIntroController;
			}
		}

		public BattleMulliganController BattleMulliganController
		{
			get
			{
				return mBattleMulliganController;
			}
		}

		public BattleGameplayController BattleGameplayController
		{
			get
			{
				return mBattleGameplayController;
			}
		}

        public RRChallenge Challenge
        {
            get {
                return mChallenge;
            }
        }

		#endregion

		#region Public Events	
		public event EventHandler<SocketInformationEventArgs> TargetJettison
		{
			add
			{
				mBattleGameplayController.TargetJettison += value;
			}

			remove
			{
				mBattleGameplayController.TargetJettison -= value;
			}
		}

		public event EventHandler<BattleEntityAndStateTransitionEventArgs> BattleEntityStateTransitioned
		{
			add
			{
				mBattleGameplayController.BattleEntityStateTransitioned += value;
			}

			remove
			{
				mBattleGameplayController.BattleEntityStateTransitioned -= value;
			}
		}

		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> BattleEntityTargetableStateTransitioned
		{
			add
			{
				mBattleGameplayController.BattleEntityTargetableStateTransitioned += value;
			}

			remove
			{
				mBattleGameplayController.BattleEntityTargetableStateTransitioned -= value;
			}
		}
			
		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> BattleEntitySelectionStateTransitioned
		{
			add
			{
				mBattleGameplayController.BattleEntitySelectionStateTransitioned += value;
			}

			remove
			{
				mBattleGameplayController.BattleEntitySelectionStateTransitioned -= value;
			}
		}

		public event Action TutorialConnectionError;
		public event EventHandler ConnectionError;

		public event EventHandler< MulliganEventDataEventArgs > MulliganResponseSuccess;
		public event EventHandler< StringEventArgs > MulliganResponseFailure;

		public event EventHandler< PlayerBattleDataEventArgs > GetLatestBattleDataSuccess;

		public event EventHandler EndTurnSuccess;
		public event EventHandler EndTurnError;

        public Action MulliganSkippedSuccess;
        public Action MulliganSkippedError;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private SJRenderCanvas mCanvas;
		#endregion

		#region Private Variables
		private bool mHasRolledBack;
		private BattleIntroController mBattleIntroController;
		private BattleMulliganController mBattleMulliganController;
		private BattleGameplayController mBattleGameplayController;

		private int mTurnCount;
		private bool mTurnActive;

		private PlayerType mCurrentTurn;
        private RRChallenge mChallenge;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake ();

			UIManager.Get.AddMenu< BattleResult > ( FileLocations.BattleResultPrefab, mCanvas.transform );
			mBattleGameplayController = UIManager.Get.AddMenu< BattleGameplayController >( FileLocations.BattleGameplayPrefab, mCanvas.transform );
			mBattleMulliganController = UIManager.Get.AddMenu< BattleMulliganController >( FileLocations.BattleMulliganPrefab, mCanvas.transform );
			mBattleIntroController = UIManager.Get.AddMenu< BattleIntroController >( FileLocations.BattleIntroPrefab, mCanvas.transform );
		}

		protected void OnDestroy()
		{
            if(UIManager.Get != null)
            {
				UIManager.Get.RemoveMenu< BattleResult > ();
				UIManager.Get.RemoveMenu< BattleIntroController > ();
				UIManager.Get.RemoveMenu< BattleMulliganController >();
			    UIManager.Get.RemoveMenu< BattleGameplayController >();
            }
		}
			
		private void OnDisable()
		{
			UnRegisterListeners();
		}
		#endregion

		#region Public Methods
		public void Initialise( RRChallenge challenge )
		{
            mChallenge = challenge;

            AiDebugManager.Get.Initialise(mChallenge);

            RegisterListeners();

			//Show the battle controller
			UIManager.Get.ShowMenu<BattleGameplayController>();
			UIManager.Get.ShowMenu<BattleIntroController>();

            mBattleIntroController.BattleIntroCutsceneController.OnConstruct( mChallenge.BattleConfig );
            mBattleGameplayController.OnConstruct( mChallenge.BattleConfig );

			UIManager.Get.HideMenu<BattleGameplayController>();
			UIManager.Get.HideMenu<BattleIntroController>();
		}

		public void OnRefreshConnectionProblems( bool isExperiencingProblems )
		{
			mBattleGameplayController.OnRefreshConnectionProblems( isExperiencingProblems );

			ClientBattle.IsExperiencingConnectionProblems = isExperiencingProblems;
		}
			
		//Notifies the battle system of a turn switch
		public void OnStartTurn( PlayerType newPlayersTurn )
		{
			mCurrentTurn = newPlayersTurn;
			mBattleGameplayController.OnStartTurn( newPlayersTurn );

			if( newPlayersTurn == PlayerType.Local )
			{
				mTurnCount++;
			}
		}
			
		public void OnEndTurn( PlayerType turnToEnd )
		{
			mBattleGameplayController.OnEndTurn( turnToEnd );
			mCurrentTurn = default( PlayerType );
		}

		//Applys a filter to the scenes battle entities - determining who can be interacted with
		public void OnApplyInteractionFilter( InteractionFilter interactionFilter )
		{
			mBattleGameplayController.OnApplyInteractionFilter( interactionFilter );
		}

		//Removes the interaction filter
		public void OnRemoveInteractionFilter( )
		{
			mBattleGameplayController.OnRemoveInteractionFilter();
		}


		//Returns the information about a given ship - this is useful for sending information to the server
		public void GetShipInformation( Ship ship, out PlayerType owner )
		{
			mBattleGameplayController.GetShipInformation( ship, out owner );
		}

		//Returns the information about a given battle entity - this is useful for sending information to the server
		public void GetBattleEntityInformation( BattleEntity battleEntity, out PlayerType owner, out bool installed, out int index )
		{
			mBattleGameplayController.GetBattleEntityInformation( battleEntity, out owner, out installed, out index );
		}

		//Returns the information about a given socket entity - this is useful for sending information to the server
		public void GetSocketInformation( Socket socket, out PlayerType owner, out int slotIndex )
		{
			mBattleGameplayController.GetSocketInformation( socket, out owner, out slotIndex );
		}

		//Returns the information about a given button - this is useful for filtering interaction
		public void GetRenderButtonInformation( RenderButton renderButton, out bool isJettison, out bool isEndTurn )
		{
			mBattleGameplayController.GetButtonInformation( renderButton, out isJettison, out isEndTurn );
		}

		public bool IsTutorial()
		{
			return IsScriptedTutorial() || IsFreeplayTutorial();
		}

		public bool IsScriptedTutorial()
		{
			switch( BattleModeManager.Get.CurrentBattleId )
			{
				case BattleId.Tutorial1:
				case BattleId.Tutorial2:
				case BattleId.Tutorial3:
					return true;

				default:
					return false;
			}
		}

		public bool IsFreeplayTutorial()
		{
			switch( BattleModeManager.Get.CurrentBattleId )
			{
				case BattleId.Tutorial4:
					return true;

				default:
					return false;
			}
		}
		#endregion

		#region Server Interactions
		public void GetOptions()
		{
			ClientMulligan.GetOptions( HandleGetMulliganOptions, HandleMulliganServerError );
		}

		public void SetOptions( List< bool > selectedCards )
		{
			ClientMulligan.SetOptions( selectedCards, HandleSetMulliganOptions, HandleMulliganServerError );
		}

		public void NotifyMulliganComplete()
		{
			ClientMulligan.NotifyComplete( HandleNotifyMulliganComplete, HandleMulliganServerError );
		}

        public void NotifyMulliganSkipped()
        {
			ClientMulligan.NotifySkipped( HandleNotifyMulliganSkipped, HandleMulliganServerError );
        }

		public void EndTurn()
		{
			BattleManager.Get.OnEndTurn( PlayerType.Local );

			ClientBattle.EndTurn( HandleEndTurnSuccess, HandleEndTurnServerError );
		}

        public void ConcedeBattle()
        {
            ClientBattle.ConcedeBattle(HandleConcedeSuccess, HandleConcedeError);
        }

		public void JettisonEntity( int socketId )
		{
			ClientBattle.JettisonEntity( socketId, HandleJettisonComponentSuccess, HandleServerError );
		}

		public void InstallComponent( int drawNumber, int socketId )
		{
			SJLogger.LogMessage( MessageFilter.George, "ZZZ Starting to install component with the draw number {0}", drawNumber );

			SJLogger.LogMessage( MessageFilter.George, "Installing component" );
			ClientBattle.InstallComponent( drawNumber, socketId, HandleInstallComponentSuccess, HandleInstallServerError );
		}

		public void InstallComponentWithTarget( int drawNumber, int socketId, TargetData targetData )
		{
			SJLogger.LogMessage( MessageFilter.George, "PPP Starting to install component with the draw number {0}", drawNumber );

			ClientBattle.InstallComponentWithTarget( drawNumber, socketId, targetData, HandleSocketTargetingSuccess, HandleServerError );
		}

		public void PlayCrewCard( int drawNumber )
		{
			ClientBattle.PlayCrewCard( drawNumber, HandleCrewCardPlaySuccess, HandleServerError);
		}

		public void PlayCrewCardWithTarget( int drawNumber, TargetData targetData )
		{
			ClientBattle.PlayCrewCardWithTarget( drawNumber, targetData, HandleHullTargetingSuccess, HandleServerError );
		}

		public void FireComponentAtComponent( int sourceSocketIndex, int targetableSocketIndex, Vector3 position)
		{
			ClientBattle.FireComponentAtComponent( sourceSocketIndex, targetableSocketIndex, position, HandleSocketTargetingSuccess, HandleServerError );
		}

		public void FireComponentAtHull( int sourceSocketIndex, Vector3 hitPosition )
		{
			ClientBattle.FireComponentAtHull( sourceSocketIndex, hitPosition, HandleHullTargetingSuccess, HandleServerError );
		}

		public void GetLatestBattleData()
		{
			ClientBattle.GetLatestBattleData( HandleGetLatestBattleDataSuccess, HandleServerError );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{         
			mChallenge.BattleUpdated += mBattleGameplayController.HandleViewDataRecieved;
			mChallenge.TurnNearlyOver += HandleTurnNearlyOver;
		}

		private void UnRegisterListeners()
		{
			mChallenge.BattleUpdated -= mBattleGameplayController.HandleViewDataRecieved;
			mChallenge.TurnNearlyOver -= HandleTurnNearlyOver;
		}
		#endregion

		#region Event Handlers
		private void HandleJettisonComponentSuccess()
		{
		}

		private void HandleInstallComponentSuccess()
		{
			SJLogger.LogMessage( MessageFilter.Client, "HandleInstallComponentSuccess" );
		}

		private void HandleSocketTargetingSuccess()
		{
		}

		private void HandleCrewCardPlaySuccess()
		{
		}

		private void HandleHullTargetingSuccess()
		{
		}

		private void HandleInstallServerError(ServerError error)
		{
			SJLogger.LogMessage( MessageFilter.Client, "HandleInstallServerError" );
			HandleServerError( error );
		}

		private void HandleServerError(ServerError error)
		{
            /*
             * The analytics show that the most common error is the game is a nullrefexception from this method.  
             * Have added some more logging to diagnose.
             * 
             */

            if(error == null)
            {
                SJLogger.LogError("error is null");
            }
            else
            {
                if(error.ErrorType == ServerErrorType.ChallengeComplete)
                {
                    SJLogger.LogWarning("Ignoring actions as the battle is already over");
                    return;
                }

    			if(error.ErrorType == ServerErrorType.Timeout)
    			{
    				//In the case of tutorials we don't rollback when an occur errors - we fire a connection error
                    if(BattleManager.Get.IsScriptedTutorial())
    				{
    					FireTutorialConnectionError();
    					return;
    				}
    			}
            }
                
            if(mBattleGameplayController == null)
            {
                SJLogger.LogError("mBattleGameplayController is null");
            }
            else 
            {
				FireConnectionError();
            }
		}

		private void HandleTurnNearlyOver( string playerId, int secondsRemaining )
		{
			//If a turn is active then note that the turn is nearly over
			if( mCurrentTurn != default( PlayerType ) )
			{
	            SJLogger.LogMessage(MessageFilter.Gameplay, "HandleTurnNearlyOver - seconds remaining {0}", secondsRemaining);
				mBattleGameplayController.OnTurnNearlyOver( mCurrentTurn, secondsRemaining );
			}
		}

		private void HandleSetMulliganOptions( BattleEntityData[] battleEntityData )
		{
			FireMulliganResponseSuccess( new MulliganOptionsEventData( battleEntityData ) );
		}

		private void HandleGetMulliganOptions( BattleEntityData[] battleEntityData )
		{
			FireMulliganResponseSuccess( new MulliganOptionsEventData( battleEntityData ) );
		}

		private void HandleGetLatestBattleDataSuccess( SJJson eventJson )
		{
			SJJson localAfterJson = eventJson.GetObject( "local" );
			SJJson remoteAfterJson = eventJson.GetObject( "remote" );
			SJJson runnerJSON = eventJson.GetObject( "properties" );
            int issueNumber = eventJson.GetInt("issueNumber").GetValueOrDefault();                       
            string nextPlayerId = eventJson.GetString("nextPlayer");

			BattleRunnerData battleRunnerData = BattleRunnerDataFactory.GetBattleRunnerData( string.Empty, localAfterJson, remoteAfterJson, runnerJSON ); 

			FireGetLatestBattleDataSuccess( nextPlayerId, battleRunnerData.LocalPlayerBattleData, battleRunnerData.RemotePlayerBattleData, battleRunnerData.LocalPlayerTargetingBattleData, battleRunnerData.RemotePlayerTargetingBattleData, issueNumber );
		}

		private void HandleNotifyMulliganComplete( )
		{
			//Lets just use the default data - we have no arguments to supply
			FireMulliganResponseSuccess( new MulliganEventData( ) );
		}

        private void HandleNotifyMulliganSkipped( )
        {
            FireMulliganSkippedSuccess();
        }

		private void HandleMulliganServerError( ServerError serverError )
		{
			if( serverError.ErrorType == ServerErrorType.Timeout )
			{
				//In the case of tutorials we don't rollback when an occur errors - we fire a connection error
				if( BattleManager.Get.IsTutorial() )
				{
					FireTutorialConnectionError();
					return;
				}
			}

			FireMulliganResponseFailure( serverError.UserFacingMessage );
		}

		private void HandleEndTurnSuccess()
		{
			FireEndTurnSuccess();
		}

		private void HandleEndTurnServerError( ServerError serverError )
		{
			if( serverError.ErrorType == ServerErrorType.Timeout )
			{
				//In the case of tutorials we don't rollback when an occur errors - we fire a connection error
				if( BattleManager.Get.IsScriptedTutorial() )
				{
					FireTutorialConnectionError();
					return;
				}
				else
				{
					FireEndTurnError();
					FireConnectionError();
					return;
				}
			}

			FireEndTurnError();
		}

        private void HandleConcedeSuccess()
        {
            AnalyticsManager.Get.RecordGameProgress( AnalyticsManager.GameProgress.QuitBattle, 0 );
        }

        private void HandleConcedeError( ServerError serverError )
        {
            // TODO:
        }
		#endregion

		#region Event Firing
		private void FireConnectionError()
		{
			if( ConnectionError != null )
			{
				ConnectionError( this, EventArgs.Empty );
			}
		}
		private void FireTutorialConnectionError()
		{
			if( TutorialConnectionError != null )
			{
				TutorialConnectionError();
			}
		}

		private void FireMulliganResponseSuccess( MulliganEventData mulliganEventData )
		{
			if( MulliganResponseSuccess != null )
			{
				MulliganResponseSuccess( this, new MulliganEventDataEventArgs( mulliganEventData ) );
			}
		}

		private void FireMulliganResponseFailure( string errorMessage )
		{
			if( MulliganResponseFailure != null )
			{
				MulliganResponseFailure( this, new StringEventArgs( errorMessage ) );
			}
		}

		private void FireGetLatestBattleDataSuccess( string nextPlayerId, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, int issueNumber )
		{
			if( GetLatestBattleDataSuccess != null )
			{
				GetLatestBattleDataSuccess( this, new PlayerBattleDataEventArgs( nextPlayerId, localPlayerBattleData, remotePlayerBattleData, localPlayerTargetingBattleData, remotePlayerTargetingBattleData, issueNumber ) );
			}
		}

        private void FireMulliganSkippedSuccess()
        {
            if( MulliganSkippedSuccess != null )
            {                
                MulliganSkippedSuccess();
            }
        }

        private void FireMulliganSkippedError()
        {
            if( MulliganSkippedError != null )
            {
                MulliganSkippedError();
            }
        }

		private void FireEndTurnSuccess()
		{
			if( EndTurnSuccess != null )
			{
				EndTurnSuccess( this, EventArgs.Empty );
			}
		}

		private void FireEndTurnError()
		{
			if( EndTurnError != null )
			{
				EndTurnError( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}

