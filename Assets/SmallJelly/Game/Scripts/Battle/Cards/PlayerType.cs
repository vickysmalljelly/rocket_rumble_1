﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public enum PlayerType
	{
		None,
		Local,
		Remote
	}
}