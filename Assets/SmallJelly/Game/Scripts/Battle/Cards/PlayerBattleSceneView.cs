using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
    /// <summary>
    /// GEORGE TODO
    /// </summary>
	public class PlayerBattleSceneView : SJMonoBehaviour
	{
		#region Public Event Handlers
		public event EventHandler<BattleEntityAndStateTransitionEventArgs> BattleEntityStateUpdated
		{
			add
			{
				mPlayerBoardView.BattleEntityStateUpdated += value;
				mPlayerHandView.BattleEntityStateUpdated += value;
			}

			remove
			{
				mPlayerBoardView.BattleEntityStateUpdated -= value;
				mPlayerHandView.BattleEntityStateUpdated -= value;
			}
		}


		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> BattleEntityTargetableStateUpdated
		{
			add
			{
				mPlayerBoardView.BattleEntityTargetableStateUpdated += value;
				mPlayerHandView.BattleEntityTargetableStateUpdated += value;
			}

			remove
			{
				mPlayerBoardView.BattleEntityTargetableStateUpdated -= value;
				mPlayerHandView.BattleEntityTargetableStateUpdated -= value;
			}
		}

		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> BattleEntitySelectionStateUpdated
		{
			add
			{
				mPlayerBoardView.BattleEntitySelectionStateUpdated += value;
				mPlayerHandView.BattleEntitySelectionStateUpdated += value;
			}

			remove
			{
				mPlayerBoardView.BattleEntitySelectionStateUpdated -= value;
				mPlayerHandView.BattleEntitySelectionStateUpdated -= value;
			}
		}
		#endregion

		#region Public Properties
		public PlayerHandView PlayerHandView
		{
			get
			{
				return mPlayerHandView;
			}
		}

		public PlayerBoardView PlayerBoardView
		{
			get
			{
				return mPlayerBoardView;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private PlayerType mOwner;

		[SerializeField]
		private PlayerHandView mPlayerHandView;

		[SerializeField]
		private PlayerBoardView mPlayerBoardView;
		#endregion

		#region Public Methods
		public void OnStartTurn( PlayerType playerType )
		{
			mPlayerHandView.OnSwitchTurn( playerType );
			mPlayerBoardView.OnSwitchTurn( playerType );
		}

		//Called when the object is created - just before OnEnable - used to construct the object with the
		//supplied configuration
		public void OnConstruct( BattleConfig battleConfig, PlayerType playerType )
		{
			mPlayerBoardView.OnConstruct( battleConfig, playerType );
		}

		public void OnRollback()
		{
			foreach( BattleEntity battleEntity in mPlayerHandView.OrderedBattleEntities )
			{
				battleEntity.OnRollback();
			}

			foreach( BattleEntity battleEntity in mPlayerBoardView.BattleEntities )
			{
				if( battleEntity == null )
				{
					continue;
				}

				battleEntity.OnRollback();
			}
		}

		public void OnHardReset( PlayerBattleData newBattleData )
		{
			OnRollback();

			foreach( BattleEntity battleEntity in mPlayerHandView.OrderedBattleEntities.ToArray() )
			{
				battleEntity.BattleEntityController.BattleEntityView.AnimationController.StopExistingAnimations();
			}
				
			foreach( BattleEntity battleEntity in mPlayerBoardView.BattleEntities.ToArray() )
			{
				if( battleEntity != null )
				{
					battleEntity.BattleEntityController.BattleEntityView.AnimationController.StopExistingAnimations();
				}
			}

			PlayerBattleData temporaryPlayerBattleData = new PlayerBattleData();
			temporaryPlayerBattleData.HandCardData = new List<BattleEntityData>();
			temporaryPlayerBattleData.BoardCardData = new BattleEntityData[ 9 ];

			OnRefreshBattleEntities( temporaryPlayerBattleData );
			OnRefresh( newBattleData );

		}

		public void OnRefresh( PlayerBattleData playerBattleData )
		{
			OnRefreshShip( playerBattleData );
			OnRefreshBattleEntities( playerBattleData );
		}

		public void OnRefreshShip( PlayerBattleData playerBattleData )
		{
			mPlayerBoardView.Refresh( playerBattleData.ShipData );
			mPlayerBoardView.Refresh( playerBattleData.SocketData );
		}

		public void OnRefreshBattleEntities( PlayerBattleData playerBattleData )
		{

			List<BattleEntityData> boardBattleEntityDataList = playerBattleData.BoardCardData.ToList();
			List<BattleEntity> battleEntitiesToReassign = GetBattleEntitiesToReassign( playerBattleData.HandCardData, boardBattleEntityDataList ) ;

			Refresh( mPlayerHandView, playerBattleData.HandCardData, battleEntitiesToReassign );
			Refresh( mPlayerBoardView, playerBattleData.BoardCardData.ToList(), battleEntitiesToReassign );

			//Destroy the battle entities which are no longer in use
			foreach( BattleEntity battleEntity in battleEntitiesToReassign )
			{
				Destroy( battleEntity.gameObject );
			}
		}

		public void UpdateTargetData( Dictionary< int, BattleEntityTargetingData > battleEntityTargetingDataDictionary )
		{
			foreach( KeyValuePair< int, BattleEntityTargetingData > keyValuePair in battleEntityTargetingDataDictionary )
			{
				BattleEntity battleEntity = mPlayerHandView.Get( keyValuePair.Key );

				if( battleEntity == null )
				{
					battleEntity = mPlayerBoardView.Get( keyValuePair.Key );
				}

				//Entity has not been created yet - we will update its data when its created
				if( battleEntity == null )
				{
					continue;
				}

				battleEntity.OnRefreshTargetData( keyValuePair.Value );
			}
				
		}
		#endregion

		#region Private Methods
		private void Refresh( BattleEntityContainerView playerCardContainerView, List<BattleEntityData> battleEntityDataList, List<BattleEntity> battleEntitiesToReassignList )
		{
			List< int > indexes = new List< int >();
			List< BattleEntity > battleEntities = new List< BattleEntity >();

			for( int battleEntityDataCount = 0; battleEntityDataCount < battleEntityDataList.Count; battleEntityDataCount++ )
			{
				BattleEntityData battleEntityData = battleEntityDataList[battleEntityDataCount];

				if( battleEntityData == null )
				{
					continue;
				}
					
				//If the view doesn't contain the current battle data then we need to insert it
				if(playerCardContainerView.Get( battleEntityData.DrawNumber ) == null)
				{
					bool reassignedEntity = false;
					//If the battle data refers to a battle entity in another view - lets take that entity
					for( int reassignedEntitiesCount = 0; reassignedEntitiesCount < battleEntitiesToReassignList.Count; reassignedEntitiesCount++ )
					{
						BattleEntity battleEntityToReassign = battleEntitiesToReassignList[reassignedEntitiesCount];
						if( battleEntityToReassign.Data.DrawNumber == battleEntityData.DrawNumber )
						{
							reassignedEntity = true;
							indexes.Add( battleEntityDataCount );
							battleEntities.Add( battleEntityToReassign );

							battleEntitiesToReassignList.RemoveAt(reassignedEntitiesCount);
							break;
						}
					}

					//Card was not taken from the reassigned list so create a new card
					if( !reassignedEntity )
					{
						BattleEntity battleEntity = BattleEntityFactory.ConstructBattleEntity( battleEntityData, mOwner, playerCardContainerView.transform );

						indexes.Add( battleEntityDataCount );
						battleEntities.Add( battleEntity );
					}


				}
			}

			playerCardContainerView.Insert( indexes.ToArray(), battleEntities.ToArray() );
			playerCardContainerView.Refresh( battleEntityDataList );
		}

		private List<BattleEntity> GetBattleEntitiesToReassign( List< BattleEntityData > handBattleEntityData, List< BattleEntityData > boardBattleEntityData )
		{
			//Remove all elements from each view that are not in their supplied lists
			List<BattleEntity> handCardsToReassign = mPlayerHandView.RemoveExcluding( handBattleEntityData );
			List<BattleEntity> boardCardsToReassign = mPlayerBoardView.RemoveExcluding( boardBattleEntityData );

			//Concat the resultant lists to form all of the cards that we wish to reassign
			int numberOfCardsToReassign = handCardsToReassign.Count + boardCardsToReassign.Count;
			List<BattleEntity> cardsToReassign = new List<BattleEntity>( numberOfCardsToReassign );

			//Block copy the list contents across for speed
			cardsToReassign.AddRange( handCardsToReassign );
			cardsToReassign.AddRange( boardCardsToReassign );

			return cardsToReassign;
		}


		#endregion
	}
}
