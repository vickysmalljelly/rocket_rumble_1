﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class Targetable : SJMonoBehaviour
	{
		#region Protected Properties
		protected TargetableLogicController TargetableLogicController
		{
			get
			{
				return mTargetableLogicController;
			}

			set
			{
				mTargetableLogicController = value;
			}
		}
		#endregion

		#region Public Properties

		public TargetableEffectController EffectController
		{
			protected get;
			set;
		}

		public TargetableLogicController.TargetableState TargetableState
		{
			get
			{
				return mTargetableLogicController.CurrentTargetableState;
			}
		}

		public TargetableLogicController.SelectionState SelectionState
		{
			get
			{
				return mTargetableLogicController.CurrentSelectionState;
			}
		}
		#endregion

		#region Member Variables
		private TargetableLogicController mTargetableLogicController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		public void OnInsertTargetableState( TargetableLogicController.TargetableState targetableState )
		{
			TargetableLogicController.OnInsertTargetableState( targetableState );
		}

		public void OnInsertSelectionState( TargetableLogicController.SelectionState selectionState )
		{
			TargetableLogicController.OnInsertSelectionState( selectionState );
		}
		#endregion


		#region Event Handlers
		public void HandleMouseDraggedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseDraggedOver(o, eventArgs);
		}

		public void HandleMouseDragged( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseDragged(o, eventArgs);
		}

		public void HandleMouseDraggedOff( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseDraggedOff(o, eventArgs);
		}


		public void HandleMouseRolledOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseRolledOver(o, eventArgs);
		}

		public void HandleMouseHovered( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseHovered(o, eventArgs);
		}

		public void HandleMouseRolledOff( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseRolledOff(o, eventArgs);
		}

		public void HandleMousePressedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMousePressedOver(o, eventArgs);
		}

		public void HandleMouseReleasedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseReleasedOver(o, eventArgs);
		}

		public void HandleMouseReleasedOff( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleMouseReleasedOff(o, eventArgs);
		}



		public void HandleTouchDraggedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchDraggedOver(o, eventArgs);
		}

		public void HandleTouchDragged( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchDragged(o, eventArgs);
		}

		public void HandleTouchDraggedOff( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchDraggedOff(o, eventArgs);
		}

		public void HandleTouchPressedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchPressedOver(o, eventArgs);
		}

		public void HandleTouchReleasedOver( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchReleasedOver(o, eventArgs);
		}

		public void HandleTouchReleasedOff( object o, InputEventArgs eventArgs )
		{
			TargetableLogicController.HandleTouchReleasedOff(o, eventArgs);
		}
		#endregion

	}
}