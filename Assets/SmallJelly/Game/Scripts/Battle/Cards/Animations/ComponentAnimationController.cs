﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ComponentAnimationController : BattleEntityAnimationController
	{
		#region Constants
		private const string COMPONENT_IDLE_ANIMATION_TEMPLATE_NAME = "ComponentIdleAnimation";
		private const string COMPONENT_FIRE_ANIMATION_TEMPLATE_NAME = "ComponentFireAnimation";
		private const string COMPONENT_DEACTIVATE_ANIMATION_TEMPLATE_NAME = "ComponentDestroyAnimation";
		private const string COMPONENT_TAKE_DAMAGE_ANIMATION_TEMPLATE_NAME = "ComponentTakeDamageAnimation";
		private const string COMPONENT_INACTIVE_ANIMATION_TEMPLATE_NAME = "ComponentInactiveAnimation";
		private const string COMPONENT_JETTISON_ANIMATION_TEMPLATE_NAME = "ComponentJettisonStage1Animation";
		private const string COMPONENT_REPAIR_ANIMATION_TEMPLATE_NAME = "ComponentRepairAnimation";
		private const string COMPONENT_BUFF_ANIMATION_TEMPLATE_NAME = "ComponentBuffAnimation";

		private const string COMPONENT_ANIMATION_SUBFOLDER = "Components";

		private const string DRAGGING_ANIMATION_TEMPLATE_NAME = "DraggingAnimation";
		#endregion

		#region Protected Methods
		//Play the animations specifically for the local player
		protected override void PlayLocalAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayLocalAnimations( cardState, animationMetaData );
		}

		//Play the animations specifically for the remote player
		protected override void PlayRemoteAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayRemoteAnimations( cardState, animationMetaData );
		}


		//Play the animations shared by both players
		protected override void PlayGenericAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayGenericAnimations( cardState, animationMetaData );

			switch(cardState)
			{
				case BattleEntityController.State.CardDragging:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, DRAGGING_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentAddingToBoard:
				case BattleEntityController.State.ComponentRemovingFromBoard:
				case BattleEntityController.State.ComponentIdle:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_IDLE_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentDestroying:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_DEACTIVATE_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentTakeDamage:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_TAKE_DAMAGE_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentInactive:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_INACTIVE_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentJettison:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_JETTISON_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentBuff:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_BUFF_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentRepair:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_REPAIR_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
