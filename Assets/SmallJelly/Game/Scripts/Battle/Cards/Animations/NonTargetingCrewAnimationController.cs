﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class NonTargetingCrewAnimationController : CrewAnimationController
	{
		#region Constants
		private const string DISSAPEARING_ANIMATION_TEMPLATE_NAME = "DissapearingAnimation";
		private const string DRAGGING_ANIMATION_TEMPLATE_NAME = "DraggingAnimation";
		private const string NON_TARGETING_ANIMATION_SUBFOLDER = "NonTargeting";
		#endregion

		#region Protected Methods
		//Play the animations shared by both players
		protected override void PlayGenericAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayGenericAnimations( cardState, animationMetaData );

			switch(cardState)
			{
				case BattleEntityController.State.CardDissapearing:
					//Attempt to play animation dependent upon the id, if it doesnt exist then use the fallback
					BattleEntityData data = GetComponentInParent< BattleEntity >().Data;

					string[] pathElements = { GENERIC_ANIMATION_SUBFOLDER, CREW_ANIMATION_SUBFOLDER, NON_TARGETING_ANIMATION_SUBFOLDER, string.Format( "{0}_{1}", data.Id, DISSAPEARING_ANIMATION_TEMPLATE_NAME )  };
					if( !AnimationExists( pathElements ) )
					{
						pathElements = new string[]{ GENERIC_ANIMATION_SUBFOLDER, CREW_ANIMATION_SUBFOLDER, NON_TARGETING_ANIMATION_SUBFOLDER, DISSAPEARING_ANIMATION_TEMPLATE_NAME };
					}

					PlayAnimation( pathElements );
				break;

				case BattleEntityController.State.CardDragging:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, CREW_ANIMATION_SUBFOLDER, NON_TARGETING_ANIMATION_SUBFOLDER, DRAGGING_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
