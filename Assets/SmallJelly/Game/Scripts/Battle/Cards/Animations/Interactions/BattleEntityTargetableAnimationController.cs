﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public abstract class BattleEntityTargetableAnimationController : AnimationController
	{
		#region Constants
		protected const string GENERIC_ANIMATION_SUBFOLDER = "Generic";
		#endregion

		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Cards/"; } }
		#endregion

		#region Public Methods
		public abstract void UpdateAnimations( PlayerType playerType, BattleEntityController.TargetableState state, AnimationMetaData animationMetaData );
		#endregion
	}
}
