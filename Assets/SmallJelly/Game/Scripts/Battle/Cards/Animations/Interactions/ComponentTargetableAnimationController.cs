﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ComponentTargetableAnimationController : BattleEntityTargetableAnimationController
	{
		#region Constants
		private const string COMPONENT_DRAGGING_ANIMATION_TEMPLATE_NAME = "ComponentDraggingAnimation";

		private const string COMPONENT_ANIMATION_SUBFOLDER = "Components";
		#endregion

		#region Public Methods
		public override void UpdateAnimations( PlayerType playerType, BattleEntityController.TargetableState targetableState, AnimationMetaData animationMetaData )
		{				
			StopExistingAnimations();

			switch( targetableState )
			{
				case BattleEntityController.TargetableState.Targeting:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_DRAGGING_ANIMATION_TEMPLATE_NAME );
						break;
			}
		}
		#endregion
	}
}
