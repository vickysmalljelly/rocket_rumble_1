﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class CardTargetableAnimationController : BattleEntityTargetableAnimationController
	{
		#region Protected Methods
		public override void UpdateAnimations( PlayerType playerType, BattleEntityController.TargetableState targetableState, AnimationMetaData animationMetaData )
		{
		}
		#endregion
	}
}
