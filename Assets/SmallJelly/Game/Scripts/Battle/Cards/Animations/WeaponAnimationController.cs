﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class WeaponAnimationController : ComponentAnimationController
	{
		#region Constants
		private const string COMPONENT_PREFIRE_ANIMATION_TEMPLATE_NAME = "WeaponPrefireAnimation";
		private const string COMPONENT_POSTFIRE_ANIMATION_TEMPLATE_NAME = "WeaponPostfireAnimation";
		private const string COMPONENT_FIRE_ANIMATION_TEMPLATE_NAME = "WeaponFireAnimation";

		private const string COMPONENT_ANIMATION_SUBFOLDER = "Components";

		private const string WEAPON_ANIMATION_SUBFOLDER = "Weapons";
		#endregion

		#region Protected Methods
		//Play the animations specifically for the local player
		protected override void PlayLocalAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayLocalAnimations( cardState, animationMetaData );

			switch(cardState)
			{

				case BattleEntityController.State.ComponentFire:
					PlayAnimation( LOCAL_ANIMATION_SUBFOLDER, WEAPON_ANIMATION_SUBFOLDER, COMPONENT_FIRE_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}

		//Play the animations specifically for the remote player
		protected override void PlayRemoteAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayRemoteAnimations( cardState, animationMetaData );

			switch(cardState)
			{
				case BattleEntityController.State.ComponentFire:
					PlayAnimation( REMOTE_ANIMATION_SUBFOLDER, WEAPON_ANIMATION_SUBFOLDER, COMPONENT_FIRE_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}


		//Play the animations shared by both players
		protected override void PlayGenericAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayGenericAnimations( cardState, animationMetaData );

			switch(cardState)
			{
				case BattleEntityController.State.ComponentPrefire:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_PREFIRE_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.ComponentPostfire:
					PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, COMPONENT_ANIMATION_SUBFOLDER, COMPONENT_POSTFIRE_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
