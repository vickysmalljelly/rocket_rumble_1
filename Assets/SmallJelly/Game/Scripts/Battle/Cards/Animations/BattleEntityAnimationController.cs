﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	//TO DO - Split into a "Card" and "Component" Battle Entity Animation Controller - it's non-essential because the class
	//isn't huge (yet!) but the animations aren't shared anymore so there's no real need for them to be fired from the same class!
	public class BattleEntityAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Cards/"; } }
		#endregion

		#region Constants
		private const string INITIALIZATION_ANIMATION_TEMPLATE_NAME = "Initialization";
		private const string DEALING_ANIMATION_TEMPLATE_NAME = "DealingAnimation";
		private const string ROLLOVER_ANIMATION_TEMPLATE_NAME = "RolloverAnimation";
		private const string HOVER_ANIMATION_TEMPLATE_NAME = "HoverAnimation";
		private const string ROLLOFF_ANIMATION_TEMPLATE_NAME = "RolloffAnimation";
		private const string RETURN_TO_HAND_ANIMATION_TEMPLATE_NAME = "ReturnToHandAnimation";
		private const string ADD_TO_HAND_ANIMATION_TEMPLATE_NAME = "AddToHandAnimation";
		private const string INSTALLATION_ANIMATION_TEMPLATE_NAME = "InstallationAnimation";
		private const string PLAY_CARD_ANIMATION_TEMPLATE_NAME = "PlayCardAnimation";
		private const string ADDING_TO_BOARD_ANIMATION_TEMPLATE_NAME = "AddingToBoardAnimation";
		private const string MOVE_TO_SOCKET_ANIMATION_TEMPLATE_NAME = "MoveToSocketAnimation";

		protected const string LOCAL_ANIMATION_SUBFOLDER = "Local";
		protected const string REMOTE_ANIMATION_SUBFOLDER = "Remote";
		protected const string GENERIC_ANIMATION_SUBFOLDER = "Generic";
		#endregion

		#region Methods
		public void UpdateAnimations( PlayerType playerType, BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			if( playerType == PlayerType.Local )
			{
				//Play the animations specifically for the local player
				PlayLocalAnimations( cardState, animationMetaData );
			}
			else if( playerType == PlayerType.Remote )
			{
				//Play the animations specifically for the remote player
				PlayRemoteAnimations( cardState, animationMetaData );
			}

			//Play the animations shared by both players
			PlayGenericAnimations( cardState, animationMetaData );
		}
		#endregion

		#region Protected Methods
		//Play the animations specifically for the local player
		protected virtual void PlayLocalAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			switch(cardState)
			{
	
				case BattleEntityController.State.CardDrawing:
					PlayAnimation(LOCAL_ANIMATION_SUBFOLDER, DEALING_ANIMATION_TEMPLATE_NAME );
				break;
			}
		}

		//Play the animations specifically for the remote player
		protected virtual void PlayRemoteAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			switch(cardState)
			{
				case BattleEntityController.State.CardDrawing:
					PlayAnimation(REMOTE_ANIMATION_SUBFOLDER, DEALING_ANIMATION_TEMPLATE_NAME );
					break;

				case BattleEntityController.State.CardPlaying:
					PlayAnimation(REMOTE_ANIMATION_SUBFOLDER, PLAY_CARD_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}

		//Play the animations shared by both players
		protected virtual void PlayGenericAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			switch(cardState)
			{
			case BattleEntityController.State.CardInitialization:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, INITIALIZATION_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.CardRollover:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, ROLLOVER_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.CardHover:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, HOVER_ANIMATION_TEMPLATE_NAME );
				break;
						
			case BattleEntityController.State.CardRolloff:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, ROLLOFF_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.CardAddingToHand:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, RETURN_TO_HAND_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.ComponentMoveToSocket:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, MOVE_TO_SOCKET_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.ComponentInstallation:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, INSTALLATION_ANIMATION_TEMPLATE_NAME );
				break;

			case BattleEntityController.State.ComponentAddingToBoard:
				PlayAnimation( GENERIC_ANIMATION_SUBFOLDER, ADDING_TO_BOARD_ANIMATION_TEMPLATE_NAME );
				break;
			}
		}
		#endregion
	}
}
