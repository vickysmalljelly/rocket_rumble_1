﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class UtilityAnimationController : ComponentAnimationController
	{
		#region Protected Methods
		//Play the animations specifically for the local player
		protected override void PlayLocalAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayLocalAnimations( cardState, animationMetaData );
		}

		//Play the animations specifically for the remote player
		protected override void PlayRemoteAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayRemoteAnimations( cardState, animationMetaData );
		}

		//Play the animations shared by both players
		protected override void PlayGenericAnimations(BattleEntityController.State cardState, AnimationMetaData animationMetaData )
		{
			base.PlayGenericAnimations( cardState, animationMetaData );
		}
		#endregion
	}
}
