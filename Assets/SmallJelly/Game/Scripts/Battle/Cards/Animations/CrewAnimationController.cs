﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class CrewAnimationController : BattleEntityAnimationController
	{
		#region Constants
		protected const string CREW_ANIMATION_SUBFOLDER = "Crew";
		#endregion
	}
}
