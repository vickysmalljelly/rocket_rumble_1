﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class CardController : BattleEntityController
	{
		#region Constants
		private const float DISTANCE_THRESHOLD_FOR_TOUCH_BECOMING_DRAG = 0.015f;
		#endregion

		#region Member Variables
		[SerializeField]
		private float mDistanceFingerHasBeenDraggingUp;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
			BattleEntityView = GetComponent<CardView>();
		}
		#endregion

		#region Public Methods
		public override void OnInsertTargetableState( TargetableState targetableState )
		{
			//When the targetable state is initiated then any cards currently being rolled over should act
			//as if they've been rolled off.
			OnMouseRolledOff( LayersHitBehindEntityThisFrame, MovementDuringFrame );
			OnTouchDraggedOff( LayersHitBehindEntityThisFrame, MovementDuringFrame );

			//We only then allow the game to update the state to targetable!
			base.OnInsertTargetableState( targetableState );
		}

		public override void OnInsertState( State cardState, AnimationMetaData animationMetaData )
		{
			base.OnInsertState( cardState, animationMetaData );

			//If we enter a state while rolled over then force rolloff!

			switch( CurrentState )
			{
				case State.CardRollover:
				case State.CardHover:
					if( cardState != State.CardRollover && cardState != State.CardHover && cardState != State.CardRolloff )
					{
						RolloffCard();
					}
					break;
			}
		}

		public override void OnRollback()
		{
			base.OnRollback();

			switch( CurrentState )
			{
				case State.CardRollover:
				case State.CardHover:
					RolloffCard();
					break;
			}
		}
		#endregion

		#region Mouse Controls
		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
			//If the card is rolled over over then we move to rollover state
			case State.CardIdle:
				RolloverCard();
				break;
			}
		}

		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
				//The card can be rolledover whilst in a different state and then change to idle state whilst being rolled over - let's handle that case
				case State.CardIdle:
					RolloverCard();
					break;

				//If the card is continued to be hovered over then we move to hover state
				case State.CardRollover:
					HoverCard();
					break;
			}
		}

		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
				case State.CardRollover:
				case State.CardHover:
					RolloffCard();
					break;
			}
		}


		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}

		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
				case State.CardHover:
					DragCard();
				break;
			}
		}

		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}


		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			
		}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
				case State.CardHover:
					DragCard();
					break;

				case State.CardDragging:
					DropCard();
					break;
			}
		}

		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			//The released off functionality mimics the released over functionality
			OnMouseReleasedOver( elementsHit, movementDuringFrame );
		}
		#endregion

		#region Touch Controls
		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}

		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			//The finger must be moving more up than sideways and more than 1/50 of the screen per second in order to count
			//as a slide upwards.

			if( movementDuringFrame.y > Mathf.Abs(movementDuringFrame.x) && movementDuringFrame.y > ( Time.deltaTime / 100.0f ) )
			{
				mDistanceFingerHasBeenDraggingUp += movementDuringFrame.y;
			}
			else
			{
				mDistanceFingerHasBeenDraggingUp = 0.0f;
			}

			//If the touch is moved up then start dragging the card, or if the card is currently dragging then keep dragging!
			if( mDistanceFingerHasBeenDraggingUp > DISTANCE_THRESHOLD_FOR_TOUCH_BECOMING_DRAG || CurrentState == State.CardDragging )
			{
				//Drag the card
				DragCard();
			}
			else if( CurrentState == State.CardIdle )
			{
				RolloverCard();
			}
			else if( CurrentState == State.CardRollover )
			{
				HoverCard();
			}
		}

		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			mDistanceFingerHasBeenDraggingUp = 0.0f;

			switch(CurrentState)
			{
				case State.CardRollover:
				case State.CardHover:
					RolloffCard();
				break;
			}
		}


		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentState)
			{
				//If the card is rolled over over then we move to rollover state
				case State.CardIdle:
					RolloverCard();
					break;
			}
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			mDistanceFingerHasBeenDraggingUp = 0.0f;

			switch(CurrentState)
			{
				case State.CardRollover:
				case State.CardHover:
					RolloffCard();
					break;

				case State.CardDragging:
					DropCard();
					break;
			}
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnTouchReleasedOver( elementsHit, movementDuringFrame );
		}
		#endregion

		#region Protected Methods
		protected override void OnAnimationsFinished()
		{
			switch(CurrentState)
			{
				case State.CardAddingToHand:
					OnInsertState(State.CardIdle);
					break;

				case State.CardRolloff:
					OnInsertState(State.CardIdle);
				break;
			}
		}
		#endregion

		#region Private Methods
		private void RolloverCard()
		{
			if( CurrentTargetableState != TargetableState.None )
			{
				return;
			}

			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			OnInsertState(State.CardRollover);
		}

		private void HoverCard()
		{
			if( CurrentTargetableState != TargetableState.None )
			{
				return;
			}

			//If this card belongs to the local player
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			OnInsertState(State.CardHover);
		}

		private void RolloffCard()
		{
			//If this card belongs to the local player
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			OnInsertState(State.CardRolloff);
		}

		private void DragCard()
		{
			if( CurrentTargetableState != TargetableState.None )
			{
				return;
			}

			//Only continue if this card belongs to the local player
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			//Only continue if it's currently the local players turn
			if( BattleManager.Get.CurrentTurn != PlayerType.Local)
			{
				return;
			}

			//Only continue if card is playable
			if( CurrentPlayableState != PlayableState.Playable )
			{
				return;
			}

			OnInsertState(State.CardDragging);
		}

		private void DropCard()
		{
			OnInsertState( State.CardDraggingReleased );
			OnInsertTargetableState( TargetableState.Targeted );
		}
		#endregion
	}
}