﻿using UnityEngine;
using System.Collections;
using TMPro;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class TemplateBattleEntityParts : SJMonoBehaviour 
	{
		#region Exposed To The Inspector
		public Collider[] CardColliders;
		public Collider[] ComponentColliders;
		#endregion
	}
}
