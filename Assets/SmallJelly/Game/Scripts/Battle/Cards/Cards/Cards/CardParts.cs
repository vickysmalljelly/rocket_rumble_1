﻿using UnityEngine;
using System.Collections;
using TMPro;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class CardParts : BattleEntityParts 
	{
		#region Exposed To The Inspector
		public Color RegularTextColour;
		public Color LegendaryTextColour;

		public GameObject Effects;

		public TextMeshPro PowerCost;

		public TextMeshPro DisplayName;
		public TextMeshPro DisplayDescription;

		public TextMeshPro CardHP;
		public TextMeshPro CardDamage;
		#endregion

		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			base.OnRefresh( battleEntityData );

			DisplayName.text = battleEntityData.DisplayName;
			DisplayDescription.text = battleEntityData.CardDisplayText;
			PowerCost.text = string.Format( "{0}", battleEntityData.Cost );

			switch( battleEntityData.EntityType )
			{
				case BattleEntityType.Weapon:
					RefreshWeapon( (WeaponData) battleEntityData );
					break;

				case BattleEntityType.TargetingCrew:
				case BattleEntityType.NonTargetingCrew:
					RefreshCrew( (CrewData) battleEntityData );
					break;

				case BattleEntityType.Utility:
					RefreshUtility( (UtilityData) battleEntityData );
					break;

				case BattleEntityType.Virus:
					RefreshVirus( (VirusData) battleEntityData );
					break;

				case BattleEntityType.Unknown:
					RefreshUnknown( (UnknownData) battleEntityData );
					break;

				default:
					SJLogger.LogError( "Entity type {0} not recognised", battleEntityData.EntityType );
					break;
			}

			RefreshTextColor( battleEntityData );
		}
		#endregion

		#region Private Methods
		private void RefreshWeapon( WeaponData weaponData )
		{
			//Refresh the card
			HP.text = string.Format( "{0}", weaponData.HP );
			Damage.text = string.Format( "{0}", weaponData.Attack );

			//Refresh the component
			CardHP.text = string.Format( "{0}", weaponData.HP );
			CardDamage.text = string.Format( "{0}", weaponData.Attack );
		}

		private void RefreshCrew( CrewData crewData ){}

		private void RefreshUtility( UtilityData utilityData )
		{
			HP.text = string.Format( "{0}", utilityData.HP );
			CardHP.text = string.Format( "{0}", utilityData.HP );
		}

		private void RefreshVirus( VirusData virusData ){}

		private void RefreshUnknown( UnknownData unknownData ){}

		private void RefreshTextColor( BattleEntityData battleEntityData )
		{
			Color textColour;
			switch( battleEntityData.Rarity )
			{
				case BattleEntityRarity.Legendary:
					textColour = LegendaryTextColour;
					break;

				default:
					textColour = RegularTextColour;
					break;
			}

			DisplayDescription.color = textColour;
		}
		#endregion
	}
}
