﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;


namespace SmallJelly
{
	public class CardView : BattleEntityView
	{
		#region Public Properties
		public override BattleEntityParts Parts
		{
			get
			{
				return Template.CardParts;
			}
		}

		public CardParts CardParts
		{
			get
			{
				return (CardParts) Parts;
			}
		}
		#endregion

		#region MonoBehaviour Methods
		protected override void OnEnable()
		{
			base.OnEnable();

			foreach( Collider collider in TemplateBattleEntityParts.CardColliders )
			{
				collider.enabled = true;
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();

			foreach( Collider collider in TemplateBattleEntityParts.CardColliders )
			{
				collider.enabled = false;
			}
		}
		#endregion

		#region Public Methods
		public override void OnRefreshModelWithData( BattleEntityData battleEntityData )
		{
			base.OnRefreshModelWithData( battleEntityData );
		}
		#endregion

		#region Protected Methods
		protected override void EnableParts()
		{
			if( Parts == null )
			{
				return;
			}

			base.EnableParts();

			SetActiveRecursively( Parts.CardText, true );
			SetActiveRecursively( Parts.Card, true );
			SetActiveRecursively( Parts.Component, true );
			SetActiveRecursively( Parts.ComponentHousing, true );
			SetActiveRecursively( Parts.Mask, true );
			SetActiveRecursively( Parts.BasePlate, false );

			//TODO - We could instantiate the jettison jets at runtime as they're only needed during installation
			SetActiveRecursively( Parts.InstallationJets, false );
			SetActiveRecursively( Parts.InstallationBeam, false );
		}

		protected override void DisableParts()
		{
			if( Parts == null )
			{
				return;
			}

			base.DisableParts();

			SetActiveRecursively( Parts.CardText, false );
			SetActiveRecursively( Parts.Card, false );
			SetActiveRecursively( Parts.Component, false );
			SetActiveRecursively( Parts.ComponentHousing, false );
			SetActiveRecursively( Parts.Mask, false );
			SetActiveRecursively( Parts.BasePlate, false );

			//TODO - We could instantiate the jettison jets at runtime as they're only needed during installation
			SetActiveRecursively( Parts.InstallationJets, false );
			SetActiveRecursively( Parts.InstallationBeam, false );

		}
		#endregion
	}
}