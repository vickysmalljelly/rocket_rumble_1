﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;

namespace SmallJelly
{
	//TODO - I'd like to split this into "BattleEntityText", "BattleEntityParts" and "BattleEntityModels"
	public class BattleEntityParts : SJMonoBehaviour 
	{
		#region Exposed To The Inspector
		public GameObject Card;
		public GameObject CardText;
		public GameObject Component;
		public GameObject ComponentHousing;
		public GameObject BasePlate;
		public GameObject Mask;

		public GameObject InstallationJets;
		public GameObject InstallationBeam;

		public TextMeshPro HP;
		public TextMeshPro Damage;

		public Transform ModelContainer;

		public MaskBattleEntityModel MaskBattleEntityModel;

		public ComponentBattleEntityModel ComponentBattleEntityModel;
		public BodyBattleEntityModel BodyBattleEntityModel;

		public StatBackgroundBattleEntityModel[] StatBackgroundBattleEntityModels;
		public StatFrameBattleEntityModel[] StatFrameBattleEntityModels;

		public InteriorBattleEntityModel InteriorBattleEntityModel;
		public RarityBattleEntityModel RarityBattleEntityModel;

		public BackBattleEntityModel BackBattleEntityModel;

		public CostBattleEntityModel CostBattleEntityModel;

		public CardTextBattleEntityModel CardTextBattleEntityModel;
		public SpritedBattleEntity SpritedBattleEntity;
		#endregion

		#region Public Methods
		public virtual void OnRefresh( BattleEntityData battleEntityData )
		{
			BodyBattleEntityModel.OnRefresh( battleEntityData );
			ComponentBattleEntityModel.OnRefresh( battleEntityData );

			InteriorBattleEntityModel.OnRefresh( battleEntityData );
			RarityBattleEntityModel.OnRefresh( battleEntityData );

			BackBattleEntityModel.OnRefresh( battleEntityData );

			MaskBattleEntityModel.OnRefresh( battleEntityData );
			CardTextBattleEntityModel.OnRefresh( battleEntityData );
			CostBattleEntityModel.OnRefresh( battleEntityData );

			foreach( StatFrameBattleEntityModel statFrameBattleEntityModel in StatFrameBattleEntityModels )
			{
				statFrameBattleEntityModel.OnRefresh( battleEntityData );
			}

			foreach( StatBackgroundBattleEntityModel statBackgroundBattleEntityModel in StatBackgroundBattleEntityModels )
			{
				statBackgroundBattleEntityModel.OnRefresh( battleEntityData );
			}
		}
		#endregion
	}
}