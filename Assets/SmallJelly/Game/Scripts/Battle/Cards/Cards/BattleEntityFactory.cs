﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the battle entities (cards/components)
	public class BattleEntityFactory : CardFactory
	{
		#region Public Methods
		public static BattleEntity ConstructBattleEntity( BattleEntityData battleEntityData, PlayerType owner, Transform parent )
		{
			GameObject view = LoadCardView( CardType.BattleEntity );

			//Set the insantiated entities parent to the container it's being added to and re-orientate it to 0,0,0
			view.transform.parent = parent;
			view.transform.localPosition = Vector3.zero;

			view.name = string.Format( "Card{0}", battleEntityData.DrawNumber );

			AssignComponents (view, battleEntityData );

			BattleEntity battleEntity = view.GetComponent<BattleEntity> ();

			battleEntity.OnInsertState( BattleEntityController.State.CardSetup );
			battleEntity.OnInsertOwner( owner );

			battleEntity.OnRefreshModelWithData( battleEntityData );

			return battleEntity;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, BattleEntityData battleEntityData )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the battle entity (the battle entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationControllers( view );
			AssignViews (view);
			AssignBattleEntityComponent( view, battleEntityData);

			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationControllers( GameObject view )
		{
			view.AddComponent< UnknownAnimationController > ();
			view.AddComponent< CardTargetableAnimationController > ();
			view.AddComponent< ComponentTargetableAnimationController > ();
			view.AddComponent< BattleEntityEffectAnimationController > ();
		}

		private static void AssignViews( GameObject view )
		{
			
			CardView cardView = view.AddComponent< CardView > ();
			cardView.TargetableAnimationController = view.GetComponent< CardTargetableAnimationController > ();
			cardView.Template = view.GetComponent< TemplateBattleEntityModel >();
			cardView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();

			ComponentView componentView = view.AddComponent< ComponentView >();
			componentView.enabled = false;
			componentView.Template = view.GetComponent< TemplateBattleEntityModel >();
			componentView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();

			componentView.TargetableAnimationController = view.GetComponent< ComponentTargetableAnimationController > ();		
		}
			
		private static void AssignBattleEntityComponent( GameObject view, BattleEntityData battleEntityData )
		{
			//Add battle entity component
			BattleEntity battleEntity = view.AddComponent< BattleEntity >();
			battleEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();

			TargetableEffectController effectController = view.AddComponent< BattleEntityEffectController > ();
			effectController.AnimationController = view.GetComponent< BattleEntityEffectAnimationController >();
			battleEntity.EffectController = effectController;
		}
		#endregion
	}
}
