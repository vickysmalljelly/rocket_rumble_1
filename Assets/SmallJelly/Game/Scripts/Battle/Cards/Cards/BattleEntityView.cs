﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
	public abstract class BattleEntityView : SJMonoBehaviour 
	{
		#region Constants
		public const int MAXIMUM_TEXT_LENGTH = 20;
		#endregion

		#region Public Event Handlers
		public event EventHandler TemplateRefreshed;
		public event EventHandler AnimationFinished;
		#endregion

		#region Public Properties
		public TemplateBattleEntityModel Template
		{ 
			get;
			set;
		}

		public TemplateBattleEntityParts TemplateBattleEntityParts
		{ 
			get;
			set;
		}

		public abstract BattleEntityParts Parts
		{
			get;
		}
			
		//TODO - Make this a property that is assigned rather than constantly using "Get"
		public BattleEntityAnimationController AnimationController 
		{ 
			get 
			{
				return GetComponents< BattleEntityAnimationController >().First( x => x != null && x.gameObject != null );
			}
		}

		public BattleEntityTargetableAnimationController TargetableAnimationController 
		{ 
			protected get; 
			set; 
		}
		#endregion

		#region Member Variables
		private PlayerType mOwner;
		private BattleEntityController.State mState;
		private bool mIsSprited;

		private bool mTemplateRefreshed;
		private BattleEntityType mPreviousType;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			EnableParts();

		}

		private void OnDestroy()
		{
			//Remove the animation controller
			Destroy( AnimationController );
		}

		protected virtual void OnEnable()
		{
			TargetableAnimationController.enabled = true;
			RegisterAnimationListeners( AnimationController );
			RegisterListeners();
		}

		protected virtual void OnDisable()
		{
			UnregisterListeners();
			UnregisterAnimationListeners( AnimationController );
			TargetableAnimationController.enabled = false;
		}
		#endregion

		#region Abstract Methods
		public virtual void OnRefreshModelWithData( BattleEntityData battleEntityData )
		{
			Template.OnRefresh( battleEntityData );
			Parts.OnRefresh( battleEntityData );
				
			Refresh( battleEntityData );

			//The template has been refreshed - let's notify listeners now all the parts of that template have also been refreshed!
			if( mTemplateRefreshed )
			{
				FireTemplateRefreshed();
				mTemplateRefreshed = false;
			}
		}

		public virtual void OnRefreshSpriteWithData( BattleEntityData battleEntityData )
		{
			if( Parts.SpritedBattleEntity.ShouldRepaint( battleEntityData ) )
			{
				if( mIsSprited )
				{
					//Temporarily disable spriting so we can force a repaint!
					DisableSpriting();
					Parts.SpritedBattleEntity.Repaint( battleEntityData );
					EnableSpriting();
				}
				else
				{
					Parts.SpritedBattleEntity.Repaint( battleEntityData );
				}
			}
		}
		#endregion

		#region Methods
		public void OnInsertOwner( PlayerType owner )
		{
			mOwner = owner;
		}

		public void OnInsertState( PlayerType playerType, BattleEntityController.State state, AnimationMetaData animationMetaData )
		{
			mState = state;
			RefreshSpriting();

			AnimationController.UpdateAnimations( playerType, state, animationMetaData );


		}

		public void OnPlayTargetableAnimation( PlayerType playerType, BattleEntityController.TargetableState state, AnimationMetaData animationMetaData )
		{
			TargetableAnimationController.UpdateAnimations( playerType, state, animationMetaData );
		}

		protected void SetActiveRecursively(GameObject rootObject, bool active)
		{
			rootObject.SetActive(active);

			foreach (Transform childTransform in rootObject.transform)
			{
				SetActiveRecursively(childTransform.gameObject, active);
			}
		}

		public void Refresh( BattleEntityData battleEntityData )
		{
			List< PlayMakerFSM > activeFSMs = new List< PlayMakerFSM >();
			List< PlayMakerFSM > pooledInactiveFSMs = new List< PlayMakerFSM >();

			BattleEntityAnimationController currentAnimationController = AnimationController;

			//If the type has not changed then do not refresh the animator
			if( mPreviousType == battleEntityData.EntityType )
			{
				return;
			}

			mPreviousType = battleEntityData.EntityType; 


			if( currentAnimationController != null )
			{
				//We get the currently active FSMs on the current animation controller
				activeFSMs = AnimationController.ActiveFSMs;
				pooledInactiveFSMs = AnimationController.PooledInactiveFSMs;

				currentAnimationController.enabled = false;

				UnregisterAnimationListeners( currentAnimationController );
				DestroyImmediate( currentAnimationController );
			}

			//Add the relevant animation controller
			switch( battleEntityData.EntityType )
			{
				case BattleEntityType.Weapon:
					gameObject.AddComponent< WeaponAnimationController >();
					break;

				case BattleEntityType.TargetingCrew:
					gameObject.AddComponent< TargetingCrewAnimationController >();
					break;

				case BattleEntityType.NonTargetingCrew:
					gameObject.AddComponent< NonTargetingCrewAnimationController >();
					break;

				case BattleEntityType.Utility:
					gameObject.AddComponent< UtilityAnimationController >();
					break;

				default:
					gameObject.AddComponent< UnknownAnimationController >();
					break;
			}
				
			//We set the active FSMs of the new animation controller to the ones of the recently destroyed animation controller
			//ensuring that the FSMs get carried across
			AnimationController.ActiveFSMs = activeFSMs;
			AnimationController.PooledInactiveFSMs = pooledInactiveFSMs;
			RegisterAnimationListeners( AnimationController );

		}
		#endregion

		#region Protected Methods
		protected virtual void EnableParts()
		{
			Parts.ComponentBattleEntityModel.OnInsertOwner( mOwner );
		}

		protected virtual void DisableParts()
		{
		}
		#endregion

		#region Private Methods
		private void RefreshSpriting()
		{

			if( Parts == null )
			{
				return;
			}

			switch( mState )
			{
				case BattleEntityController.State.CardIdle:
					EnableSpriting();
					break;

				default:
					DisableSpriting();
					break;
			}
		}

		private void EnableSpriting()
		{
			mIsSprited = true;

			DisableParts();
			SetActiveRecursively( Parts.SpritedBattleEntity.gameObject, true );
		}

		private void DisableSpriting()
		{
			mIsSprited = false;

			EnableParts();
			SetActiveRecursively( Parts.SpritedBattleEntity.gameObject, false );
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}

		private void FireTemplateRefreshed()
		{
			if( TemplateRefreshed != null )
			{
				TemplateRefreshed( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterAnimationListeners( BattleEntityAnimationController battleEntityAnimationController )
		{
			battleEntityAnimationController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterAnimationListeners( BattleEntityAnimationController battleEntityAnimationController )
		{
			battleEntityAnimationController.AnimationFinished -= HandleAnimationFinished;
		}

		private void RegisterListeners()
		{
			Template.TemplateRefreshed += HandleTemplateRefreshed;
		}

		private void UnregisterListeners()
		{
			Template.TemplateRefreshed -= HandleTemplateRefreshed;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}

		private void HandleTemplateRefreshed( object o, EventArgs args )
		{
			mTemplateRefreshed = true;
		}
		#endregion
	}
}