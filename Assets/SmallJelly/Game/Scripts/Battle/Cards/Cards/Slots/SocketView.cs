﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class SocketView : SJMonoBehaviour
	{
		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				mSocketAnimationController.AnimationFinished += value;
			}

			remove
			{
				mSocketAnimationController.AnimationFinished -= value;
			}
		}

		public event EventHandler GameplayFireAnimationFinished
		{
			add
			{
				mSocketGameplayAnimationController.AnimationFinished += value;
			}

			remove
			{
				mSocketGameplayAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public Collider[] Colliders
		{
			get
			{
				return mColliders;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private Collider[] mColliders;

		[SerializeField]
		private SocketAnimationController mSocketAnimationController;

		[SerializeField]
		private SocketGameplayAnimationController mSocketGameplayAnimationController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		public virtual void OnEnable()
		{
			EnableColliders();
		}

		public virtual void OnDisable()
		{
			DisableColliders();
		}
		#endregion

		#region Methods
		public void OnPlayAnimation( SocketController.State state )
		{
			mSocketAnimationController.UpdateAnimations( state );
		}

		public void OnPlayAnimation( SocketController.GameplayState gameplayState, AnimationMetaData animationMetaData )
		{
			mSocketGameplayAnimationController.UpdateAnimations( gameplayState, animationMetaData );
		}

		public void EnableColliders()
		{
			foreach( Collider collider in mColliders )
			{
				collider.enabled = true;
			}
		}

		public void DisableColliders()
		{
			foreach( Collider collider in mColliders )
			{
				collider.enabled = false;
			}
		}
		#endregion

		#region Virtual Methods
		public void OnRefreshData( SocketData socketData )
		{
		}
		#endregion
	}
}