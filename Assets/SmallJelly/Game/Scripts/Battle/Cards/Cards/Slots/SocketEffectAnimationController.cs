﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System.IO;

namespace SmallJelly
{
	public class SocketEffectAnimationController : TargetableEffectAnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Effects/"; } }
		#endregion

		#region Constants
		private const string SHIELD_ANIMATION_TEMPLATE_NAME = "ShieldBuff";

		private const string ADD_ANIMATION_TEMPLATE_NAME_EXTENSION = "Add";
		private const string REMOVE_ANIMATION_TEMPLATE_NAME_EXTENSION = "Remove";
		#endregion

		#region Public Methods
		public void PlayRemoveAnimations( SocketEffectController.SocketEffect[] effectsToRemove )
		{
			foreach( SocketEffectController.SocketEffect effectToRemove in effectsToRemove )
			{
				string path = GetPath( effectToRemove );
				string finalPath = string.Format( "{0}{1}", path, REMOVE_ANIMATION_TEMPLATE_NAME_EXTENSION );

				PlayAnimation( finalPath );
			}
		}

		public void PlayAddAnimations( SocketEffectController.SocketEffect[] effectsToAdd )
		{
			foreach( SocketEffectController.SocketEffect effectToAdd in effectsToAdd )
			{
				string path = GetPath( effectToAdd );
				string finalPath = string.Format( "{0}{1}", path, ADD_ANIMATION_TEMPLATE_NAME_EXTENSION );

				PlayAnimation( finalPath );
			}
		}
		#endregion

		#region Private Methods
		private string GetPath( SocketEffectController.SocketEffect effect )
		{
			switch( effect )
			{
				case SocketEffectController.SocketEffect.Shield:
					return SHIELD_ANIMATION_TEMPLATE_NAME;
			}
				
			SJLogger.Assert( "No animation was found for the supplied type {0}", effect.ToString() );

			return string.Empty;
		}
		#endregion
	}
}
