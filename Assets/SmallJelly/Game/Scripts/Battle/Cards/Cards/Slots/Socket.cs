﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class Socket : Targetable 
	{
		#region Public Events
		public event EventHandler< SocketEventArgs > AnimationFinished;
		public event EventHandler< SocketEventArgs > HingeAnimationFinished;
		public event EventHandler< SocketEventArgs > GameplayAnimationFinished;
		#endregion

		#region Public Properties
		public SocketController SocketController
		{
			get
			{
				return mSocketController;
			}
		}

		public SocketData SocketData
		{
			get
			{
				return mSocketData;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private SocketController mSocketController;

		[SerializeField]
		private SocketEffectController mSocketEffectController;

		[SerializeField]
		private RenderCollisionGroup mRenderCollisionGroup;

		[SerializeField]
		private ObjectInputListener mObjectInputListener;
		#endregion

		#region Member Variables
		private SocketData mSocketData;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			TargetableLogicController = mSocketController;

			mSocketController.OnInsertState( SocketController.State.Initialization );
			mSocketController.OnInsertState( SocketController.State.Idle );
		}

		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterInputListeners();
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertGameplayState( SocketController.GameplayState firingState )
		{
			OnInsertGameplayState( firingState, new AnimationMetaData( new System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertGameplayState( SocketController.GameplayState gameplayState, AnimationMetaData animationMetaData )
		{
			mSocketController.OnInsertGameplayState( gameplayState, animationMetaData );
		}


		public void OnInsertComponent( BattleEntity component )
		{
			//Once a component is placed in a socket the socket will steal all input from that component - this is
			//important because interactions (targeting etc) happen to the socket and are influenced by the sockets
			//state and not the component itself.
			mRenderCollisionGroup.AddColliders( component.GetComponentsInChildren< Collider >() );

			//Disable input of the component - relinquishing full control to the socket
			component.ObjectInputListener.enabled = false;

			//The socket then forwards input on to the component so it can do whatever visual thing it needs to.
			RegisterComponentListeners( component );
			mSocketController.OnInsertComponent( component );
		}

		public void OnRemoveComponent( BattleEntity component )
		{
			//Once the component has been removed from the socket it will regain full input control back
			mSocketController.OnRemoveComponent( component );
			UnregisterComponentListeners( component );

			//Enable input of the component
			component.ObjectInputListener.enabled = true;

			mRenderCollisionGroup.RemoveColliders( component.GetComponentsInChildren< Collider >() );
		}

		public void OnRefreshData( SocketData socketData )
		{
			mSocketData = socketData;

			//Refresh the view
			mSocketController.OnRefreshData( socketData );
			mSocketEffectController.OnRefreshData( socketData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mSocketController.AnimationFinished += HandleAnimationFinished;
			mSocketController.GameplayFireAnimationFinished += HandleGameplayFireAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mSocketController.AnimationFinished -= HandleAnimationFinished;
			mSocketController.GameplayFireAnimationFinished -= HandleGameplayFireAnimationFinished;
		}

		private void RegisterComponentListeners( BattleEntity component )
		{
			component.StateTransitioned += mSocketController.HandleComponentStateTransitioned;
			component.SelectionStateTransitioned += mSocketController.HandleComponentSelectionStateTransitioned;
			component.TargetableStateTransitioned += mSocketController.HandleComponentTargetableStateTransitioned;
			component.HighlightedStateTransitioned += mSocketController.HandleComponentHighlightedStateTransitioned;

			//Mouse
			mObjectInputListener.MouseRolledOver += component.HandleMouseRolledOver;
			mObjectInputListener.MouseHovered += component.HandleMouseHovered;
			mObjectInputListener.MouseRolledOff += component.HandleMouseRolledOff;

			mObjectInputListener.MouseDraggedOver += component.HandleMouseDraggedOver;
			mObjectInputListener.MouseDragged += component.HandleMouseDragged;
			mObjectInputListener.MouseDraggedOff += component.HandleMouseDraggedOff;

			mObjectInputListener.MousePressed += component.HandleMousePressedOver;
			mObjectInputListener.MouseReleasedOver += component.HandleMouseReleasedOver;
			mObjectInputListener.MouseReleasedOff += component.HandleMouseReleasedOff;


			//Touch
			mObjectInputListener.TouchDraggedOver += component.HandleTouchDraggedOver;
			mObjectInputListener.TouchDragged += component.HandleTouchDragged;
			mObjectInputListener.TouchDraggedOff += component.HandleTouchDraggedOff;

			mObjectInputListener.TouchPressed += component.HandleTouchPressedOver;
			mObjectInputListener.TouchReleasedOver += component.HandleTouchReleasedOver;
			mObjectInputListener.TouchReleasedOff += component.HandleTouchReleasedOff;	
		}

		private void UnregisterComponentListeners( BattleEntity component )
		{
			component.StateTransitioned -= mSocketController.HandleComponentStateTransitioned;
			component.SelectionStateTransitioned -= mSocketController.HandleComponentSelectionStateTransitioned;
			component.TargetableStateTransitioned -= mSocketController.HandleComponentTargetableStateTransitioned;
			component.HighlightedStateTransitioned -= mSocketController.HandleComponentHighlightedStateTransitioned;


			//Mouse
			mObjectInputListener.MouseRolledOver -= component.HandleMouseRolledOver;
			mObjectInputListener.MouseHovered -= component.HandleMouseHovered;
			mObjectInputListener.MouseRolledOff -= component.HandleMouseRolledOff;

			mObjectInputListener.MouseDraggedOver -= component.HandleMouseDraggedOver;
			mObjectInputListener.MouseDragged -= component.HandleMouseDragged;
			mObjectInputListener.MouseDraggedOff -= component.HandleMouseDraggedOff;

			mObjectInputListener.MousePressed -= component.HandleMousePressedOver;
			mObjectInputListener.MouseReleasedOver -= component.HandleMouseReleasedOver;
			mObjectInputListener.MouseReleasedOff -= component.HandleMouseReleasedOff;


			//Touch
			mObjectInputListener.TouchDraggedOver -= component.HandleTouchDraggedOver;
			mObjectInputListener.TouchDragged -= component.HandleTouchDragged;
			mObjectInputListener.TouchDraggedOff -= component.HandleTouchDraggedOff;

			mObjectInputListener.TouchPressed -= component.HandleTouchPressedOver;
			mObjectInputListener.TouchReleasedOver -= component.HandleTouchReleasedOver;
			mObjectInputListener.TouchReleasedOff -= component.HandleTouchReleasedOff;	
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, new SocketEventArgs( this ) );
			}
		}

		private void FireSocketAnimationFinished()
		{
			if( HingeAnimationFinished != null )
			{
				HingeAnimationFinished( this, new SocketEventArgs( this ) );
			}
		}

		private void FireFiringAnimationFinished()
		{
			if( GameplayAnimationFinished != null )
			{
				GameplayAnimationFinished( this, new SocketEventArgs( this ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}

		private void HandleSocketAnimationFinished( object o, EventArgs args )
		{
			FireSocketAnimationFinished();
		}

		private void HandleGameplayFireAnimationFinished( object o, EventArgs args )
		{
			FireFiringAnimationFinished();
		}
		#endregion

		#region Event Registration
		private void RegisterInputListeners()
		{
			//Mouse
			mObjectInputListener.MouseRolledOver += HandleMouseRolledOver;
			mObjectInputListener.MouseHovered += HandleMouseHovered;
			mObjectInputListener.MouseRolledOff += HandleMouseRolledOff;

			mObjectInputListener.MouseDraggedOver += HandleMouseDraggedOver;
			mObjectInputListener.MouseDragged += HandleMouseDragged;
			mObjectInputListener.MouseDraggedOff += HandleMouseDraggedOff;

			mObjectInputListener.MousePressed += HandleMousePressedOver;
			mObjectInputListener.MouseReleasedOver += HandleMouseReleasedOver;
			mObjectInputListener.MouseReleasedOff += HandleMouseReleasedOff;


			//Touch
			mObjectInputListener.TouchDraggedOver += HandleTouchDraggedOver;
			mObjectInputListener.TouchDragged += HandleTouchDragged;
			mObjectInputListener.TouchDraggedOff += HandleTouchDraggedOff;

			mObjectInputListener.TouchPressed += HandleTouchPressedOver;
			mObjectInputListener.TouchReleasedOver += HandleTouchReleasedOver;
			mObjectInputListener.TouchReleasedOff += HandleTouchReleasedOff;
		}

		private void UnregisterInputListeners()
		{
			//Mouse
			mObjectInputListener.MouseRolledOver -= HandleMouseRolledOver;
			mObjectInputListener.MouseHovered -= HandleMouseHovered;
			mObjectInputListener.MouseRolledOff -= HandleMouseRolledOff;

			mObjectInputListener.MouseDraggedOver -= HandleMouseDraggedOver;
			mObjectInputListener.MouseDragged -= HandleMouseDragged;
			mObjectInputListener.MouseDraggedOff -= HandleMouseDraggedOff;

			mObjectInputListener.MousePressed -= HandleMousePressedOver;
			mObjectInputListener.MouseReleasedOver -= HandleMouseReleasedOver;
			mObjectInputListener.MouseReleasedOff -= HandleMouseReleasedOff;


			//Touch
			mObjectInputListener.TouchDraggedOver -= HandleTouchDraggedOver;
			mObjectInputListener.TouchDragged -= HandleTouchDragged;
			mObjectInputListener.TouchDraggedOff -= HandleTouchDraggedOff;

			mObjectInputListener.TouchPressed -= HandleTouchPressedOver;
			mObjectInputListener.TouchReleasedOver -= HandleTouchReleasedOver;
			mObjectInputListener.TouchReleasedOff -= HandleTouchReleasedOff;
		}
		#endregion

	}
}