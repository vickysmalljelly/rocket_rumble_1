﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class SocketGameplayAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  FileLocations.SmallJellySocketsPlayMakerTemplates; } }
		#endregion

		#region Constants
		private const string SOCKET_FIRING_ANIMATION = "SocketFireAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( SocketController.GameplayState gameplayState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch( gameplayState )
			{
				case SocketController.GameplayState.Firing:
					PlayAnimation( SOCKET_FIRING_ANIMATION );
					break;
			}
		}
		#endregion
	}
}
