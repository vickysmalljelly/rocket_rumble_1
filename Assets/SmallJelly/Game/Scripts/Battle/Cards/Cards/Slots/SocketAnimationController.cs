﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class SocketAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Sockets/"; } }
		#endregion

		#region Constants
		private const string SOCKET_INITIALIZATION_ANIMATION_TEMPLATE_NAME = "SocketInitialization";
		private const string SOCKET_IDLE_ANIMATION_TEMPLATE_NAME = "SocketIdle";
		private const string SOCKET_HOVERED_ANIMATION_TEMPLATE_NAME = "SocketHovered";
		private const string SOCKET_HOVERED_TARGETABLE_ANIMATION_TEMPLATE_NAME = "SocketHoveredTargetable";
		private const string SOCKET_HOVERED_NON_TARGETABLE_ANIMATION_TEMPLATE_NAME = "SocketHoveredNonTargetable";
		private const string SOCKET_SELECTED_ANIMATION_TEMPLATE_NAME = "SocketSelected";
		#endregion

		#region Methods
		public void UpdateAnimations( SocketController.State socketState )
		{
			StopExistingAnimations();

			switch(socketState)
			{
				case SocketController.State.Initialization:
					PlayAnimation( SOCKET_INITIALIZATION_ANIMATION_TEMPLATE_NAME );
					break;

				case SocketController.State.Idle:
					PlayAnimation( SOCKET_IDLE_ANIMATION_TEMPLATE_NAME );
					break;

				case SocketController.State.Hovered:
					PlayAnimation(SOCKET_HOVERED_ANIMATION_TEMPLATE_NAME);
					break;

				case SocketController.State.HoveredTargetable:
					PlayAnimation(SOCKET_HOVERED_TARGETABLE_ANIMATION_TEMPLATE_NAME);
					break;

				case SocketController.State.HoveredNonTargetable:
					PlayAnimation(SOCKET_HOVERED_NON_TARGETABLE_ANIMATION_TEMPLATE_NAME);
					break;

				case SocketController.State.Selected:
					PlayAnimation(SOCKET_SELECTED_ANIMATION_TEMPLATE_NAME);
					break;
			}
		}
		#endregion
	}
}
