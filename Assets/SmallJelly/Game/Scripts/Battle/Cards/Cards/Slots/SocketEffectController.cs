﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class SocketEffectController : TargetableEffectController
	{
		#region Battle Entity Effect State
		public enum SocketEffect
		{
			Shield,
		}
		#endregion

		#region Member Variables
		private PlayerType mTurn;

		private SocketEffect[] mCurrentEffects = new SocketEffect[ 0 ];

		private SocketEffect[] mBuffEffects;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake ()
		{
			base.Awake ();

			AnimationController = GetComponent< SocketEffectAnimationController >();
			mBuffEffects = new SocketEffect[ 0 ];
		}
		#endregion

		#region Public Methods

		public void OnRefreshData( SocketData socketData )
		{
			mBuffEffects = socketData.Effects;

			Refresh();
		}

		#endregion

		#region Protected Methods
		protected void Refresh( SocketEffect[] newEffects )
		{
			SocketEffect[] effectsToRemove = mCurrentEffects.Where( o => !newEffects.Contains( o ) ).ToArray();
			SocketEffect[] effectsToAdd = newEffects.Where( o => !mCurrentEffects.Contains( o ) ).ToArray();

			SocketEffectAnimationController socketEffectAnimationController = (SocketEffectAnimationController) AnimationController;
			socketEffectAnimationController.PlayRemoveAnimations( effectsToRemove );
			socketEffectAnimationController.PlayAddAnimations( effectsToAdd );

			mCurrentEffects = newEffects;
		}
		#endregion


		#region Private Methods
		private void Refresh()
		{
			List< SocketEffect > effects = new List< SocketEffect >();
			effects.AddRange( mBuffEffects );

			Refresh( effects.ToArray() );
		}

		#endregion
	}
}