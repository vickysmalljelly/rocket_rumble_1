﻿
using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	public class SocketController : TargetableLogicController
	{
		#region Enums
		public enum State
		{
			None,
			Initialization,
			Idle,
			Hovered,
			HoveredTargetable,
			HoveredNonTargetable,
			Selected
		}

		//At some point we might need to generalise this out to include o
		public enum GameplayState
		{
			None,
			Firing
		}
		#endregion

		#region Public Properties
		public BattleEntity Component
		{
			get
			{
				return mComponent;
			}
		}
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mSocketView.AnimationFinished += value;
			}

			remove
			{
				mSocketView.AnimationFinished -= value;
			}
		}

		public event EventHandler GameplayFireAnimationFinished
		{
			add
			{
				mSocketView.GameplayFireAnimationFinished += value;
			}

			remove
			{
				mSocketView.GameplayFireAnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private State mState;

		[SerializeField]
		private BattleEntity mComponent;

		[SerializeField]
		private SocketView mSocketView;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		public void OnEnable()
		{
			//Enable the view
			mSocketView.enabled = true;
		}

		public void OnDisable()
		{
			//Disable the view
			mSocketView.enabled = false;
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			if( mState != state )
			{
				OnLeaveState( mState );
				mState = state;

				mSocketView.OnPlayAnimation( mState );
			}
		}

		public void OnInsertGameplayState( GameplayState gameplayState, AnimationMetaData animationMetaData )
		{
			mSocketView.OnPlayAnimation( gameplayState, animationMetaData );
		}

		public void OnInsertComponent( BattleEntity component )
		{
			mComponent = component;
		}

		public void OnRemoveComponent( BattleEntity component )
		{
			SJLogger.AssertCondition( component == mComponent, "Component mismatch in slot!" );
			mComponent = null;
		}

		public void OnRefreshData( SocketData socketData )
		{
			mSocketView.OnRefreshData( socketData );
		}
		#endregion

		#region Overrided Methods
		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			StartHover();
		}

		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			StopHover();
		}

		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		#endregion

		#region Touch Controls
		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDraggedOver( elementsHit, movementDuringFrame );

			StartHover();
		}

		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDragged( elementsHit, movementDuringFrame );
		}

		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseDraggedOff( elementsHit, movementDuringFrame );

			StopHover();
		}
			
		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMousePressedOver( elementsHit, movementDuringFrame );

			StartHover();
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseReleasedOver( elementsHit, movementDuringFrame );

			StopHover();
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			OnMouseReleasedOff( elementsHit, movementDuringFrame );

			StopHover();
		}
		#endregion

		#region Battle Entity State Changes
		private void OnBattleEntityStateTransitioned( BattleEntityController.State previousState, BattleEntityController.State newState )
		{
			if( previousState == BattleEntityController.State.ComponentPostfire )
			{
				OnInsertState( State.Idle );
			}
		}

		private void OnBattleEntitySelectionStateTransitioned( BattleEntityController.SelectionState previousSelectionState, BattleEntityController.SelectionState newSelectionState )
		{
			switch( newSelectionState )
			{
				case BattleEntityController.SelectionState.Selected:
					OnInsertState( State.Selected );
					break;
			}

			switch( previousSelectionState )
			{
				case BattleEntityController.SelectionState.Selected:
					OnInsertState( State.Idle );
					break;
			}
		}

		private void OnBattleEntityHighlightedStateTransitioned( ComponentController.HighlightedState previousHighlightedState, ComponentController.HighlightedState newHighlightedState )
		{
			switch( newHighlightedState )
			{
				case ComponentController.HighlightedState.Highlighted:
					if( mState == State.Idle )
					{
						StartHover ();
					}
					break;
			}

			switch( previousHighlightedState )
			{
				case ComponentController.HighlightedState.Highlighted:
					if( mState == State.Hovered || mState == State.HoveredNonTargetable || mState == State.HoveredTargetable )
					{
						OnInsertState( State.Idle );
					}
					break;
			}
		}

		private void OnBattleEntityTargetableStateTransitioned( BattleEntityController.TargetableState prevTargetableState, BattleEntityController.TargetableState newTargetableState )
		{
			switch( newTargetableState )
			{
				case BattleEntityController.TargetableState.Targeting:
					OnInsertState( State.Selected );
					break;
			}

			switch( prevTargetableState )
			{
				case BattleEntityController.TargetableState.Targeting:
					StopHover();
					break;
			}
		}
		#endregion


		#region Protected Methods
		protected virtual void OnLeaveState( State state )
		{
		}
		#endregion

		#region Private Methods
		private void StartHover()
		{
			//We can only hover from idle state
			if( mState != State.Idle )
			{
				return;
			}

			switch( CurrentTargetableState )
			{				
				case TargetableState.Targetable:
					OnInsertState( State.HoveredTargetable );
					break;

				case TargetableState.NonTargetable:
					OnInsertState( State.HoveredNonTargetable );
					break;

				default:
					OnInsertState( State.Hovered );
					break;
			}
		}

		private void StopHover()
		{
			//If the state isnt hovered, hovered targetable or hovered non targetable then we cant stop hovering
			if( !( mState == State.Hovered || mState == State.HoveredTargetable || mState == State.HoveredNonTargetable ) )
			{
				return;
			}

			//If the component is handling control of the socket then dont deselect
			if( Component != null && Component.BattleEntityController.CurrentSelectionState == SelectionState.Selected )
			{
				return;
			}

			OnInsertState( State.Idle );
		}
		#endregion

		#region Event Handlers
		public void HandleComponentStateTransitioned( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			OnBattleEntityStateTransitioned( battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState );
		}

		public void HandleComponentSelectionStateTransitioned( object o, BattleEntityAndSelectionStateTransitionEventArgs battleEntityAndSelectionStateTransitionEventArgs )
		{
			OnBattleEntitySelectionStateTransitioned( battleEntityAndSelectionStateTransitionEventArgs.PreviousState, battleEntityAndSelectionStateTransitionEventArgs.NewState );
		}

		public void HandleComponentTargetableStateTransitioned( object o, BattleEntityAndTargetableStateTransitionEventArgs battleEntityAndTargetableStateTransitionEventArgs )
		{
			OnBattleEntityTargetableStateTransitioned( battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState );
		}

		public void HandleComponentHighlightedStateTransitioned( object o, BattleEntityAndHighlightedStateTransitionEventArgs battleEntityAndHighlightedStateTransitionEventArgs )
		{
			OnBattleEntityHighlightedStateTransitioned( battleEntityAndHighlightedStateTransitionEventArgs.PreviousState, battleEntityAndHighlightedStateTransitionEventArgs.NewState );
		}
		#endregion
	}
}