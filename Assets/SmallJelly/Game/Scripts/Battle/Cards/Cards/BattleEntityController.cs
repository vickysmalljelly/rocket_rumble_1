using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public abstract class BattleEntityController : TargetableLogicController
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			//Cards
			CardSetup,
			CardInitialization,
			CardDrawing,


			CardAddingToHand,
			CardRemovingFromHand,

			CardIdle,
			CardRollover,
			CardHover,
			CardRolloff,
			CardDragging,
			CardDraggingReleased,
			CardReadyToReturnToHand,
			CardDissapearing,
			CardPlaying,

			//Components
			ComponentAddingToBoard,
			ComponentRemovingFromBoard,

			ComponentMoveToSocket,
			ComponentInstallation,
			ComponentInactive,
			ComponentPrefire,
			ComponentFire,
			ComponentPostfire,


			ComponentIdle,
			ComponentDestroying,
			ComponentDestroyed,
			ComponentTakeDamage,
			ComponentJettison,
			ComponentRepair,
			ComponentBuff
		}

		//The playable state of the object - this is an enum rather than a bool for consistency
		public enum PlayableState
		{
			NonPlayable,
			Playable
		}

		public enum Buff
		{
			TargetLock,
			DamageBuff,
			HealthBuff
		}
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished;

		public event EventHandler< BattleEntityStateTransitionEventArgs > BattleEntityStateUpdated;
		public event EventHandler< BattleEntityTargetableStateTransitionEventArgs > BattleEntityTargetableStateUpdated;
		public event EventHandler< BattleEntityPlayableStateTransitionEventArgs > PlayableStateUpdated;
		#endregion

		#region Public Properties
		public PlayerType CurrentOwner
		{
			get
			{
				return mCurrentOwner;
			}
		}

		public Stack<State> PreviousStates
		{
			get
			{
				return mPreviousStates;
			}
		}

		public BattleEntityView BattleEntityView
		{
			get
			{
				return mBattleEntityView;
			}

			set
			{
				mBattleEntityView = value;
			}
		}

		public PlayableState CurrentPlayableState
		{
			get
			{
				return mPlayableState;
			}

			set
			{
				mPlayableState = value;
			}
		}
		#endregion

		#region Protected Properties
		protected State CurrentState
		{
			get
			{
				return mState;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private State mState;


		[SerializeField]
		private PlayableState mPlayableState;

		[SerializeField]
		private Stack< State > mPreviousStates;
		#endregion

		#region Member Variables
		private PlayerType mCurrentOwner;
		private BattleEntityView mBattleEntityView;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			mPreviousStates = new Stack< State >();
		}

		public void OnEnable()
		{
			RegisterListeners();

			//Enable the view
			BattleEntityView.enabled = true;
		}
		
		public void OnDisable()
		{
			//Disable the view
			BattleEntityView.enabled = false;

			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshModelWithData( BattleEntityData battleEntityData )
		{
			mBattleEntityView.OnRefreshModelWithData( battleEntityData );
		}

		public void OnRefreshSpriteWithData( BattleEntityData battleEntityData )
		{
			mBattleEntityView.OnRefreshSpriteWithData( battleEntityData );
		}

		public virtual void OnInsertPreviousStates( Stack<State> previousStates )
		{
			mPreviousStates = previousStates;
		}

		public virtual void OnInsertOwner( PlayerType owner )
		{
			mCurrentOwner = owner;
			mBattleEntityView.OnInsertOwner( owner );
		}
			
		public virtual void OnInsertState( State cardState )
		{		
			OnInsertState( cardState, new AnimationMetaData( new Dictionary< string, NamedVariable >() ) );
		}

		public virtual void OnRollback()
		{
		}

		//Allows the card to respond to global state changes - sometimes these state changes might actually be in response to card actions e.g. 
		//Card dealing animation finishing.
		public virtual void OnInsertState( State cardState, AnimationMetaData animationMetaData )
		{
			SJLogger.LogMessage( MessageFilter.George, "Change State " + gameObject.name + ", " + cardState );

			UpdateState( cardState, animationMetaData );
		}

		public override void OnInsertTargetableState( TargetableState targetableState )
		{				
			OnInsertTargetableState( targetableState, new AnimationMetaData( new Dictionary< string, NamedVariable >() ) );
		}

		//Allows the card to respond to global state changes - sometimes these state changes might actually be in response to card actions e.g. 
		//Card dealing animation finishing.
		public void OnInsertTargetableState( TargetableState targetableState, AnimationMetaData animationMetaData )
		{
			//We play animations before triggering the parent code which involves sending an event to notify other parties that the
			//state has changed, thus all interactions should happen before that happens.
			BattleEntityView.OnPlayTargetableAnimation( mCurrentOwner, targetableState, animationMetaData );

			base.OnInsertTargetableState( targetableState );
		}

		public void OnInsertPlayableState( PlayableState playableState )
		{
			FirePlayableStateUpdated( mPlayableState, playableState, LayersHitBehindEntityThisFrame );

			mPlayableState = playableState;
		}

		
		public virtual void OnReturnToPreviousState()
		{
			mPreviousStates.Pop();

			SJLogger.AssertCondition( mPreviousStates.Count > 0, "The battle entities previous state stack should never have 0 elements in it at this point in time" );

			OnInsertState( mPreviousStates.Pop() );
		}
		#endregion

		#region Protected Methods
		protected abstract void OnAnimationsFinished();


		protected virtual void OnLeaveState( State cardState )
		{
		}

		protected override void OnLeaveTargetableState( TargetableState targetableState )
		{
			base.OnLeaveTargetableState( targetableState );
		}


		protected void UpdateState( State newBattleEntityState, AnimationMetaData animationMetaData )
		{
			State previousState = mState;
			mState = newBattleEntityState;

			//If we're leaving a state
			OnLeaveState( previousState );

			mPreviousStates.Push( newBattleEntityState );

			BattleEntityView.OnInsertState( mCurrentOwner, newBattleEntityState, animationMetaData );

			FireStateUpdated( previousState, newBattleEntityState, LayersHitBehindEntityThisFrame );
		}
		#endregion


		#region Event Registration
		private void RegisterListeners()
		{
			mBattleEntityView.AnimationFinished += HandleAnimationFinished;
			mBattleEntityView.TemplateRefreshed += HandleTemplateRefreshed;
		}

		private void UnregisterListeners()
		{
			mBattleEntityView.AnimationFinished -= HandleAnimationFinished;
			mBattleEntityView.TemplateRefreshed -= HandleTemplateRefreshed;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			OnAnimationsFinished();
			FireAnimationFinished();
		}

		private void HandleTemplateRefreshed( object o, EventArgs args )
		{
			//The templates being refreshed so we need to re-intialise the card!
			OnInsertState( State.CardInitialization );
			OnReturnToPreviousState();
		}
		#endregion

		#region Event Firing
		protected void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( BattleEntityStateUpdated != null )
			{
				BattleEntityStateUpdated( this, new BattleEntityStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState, layersHitBehindEntity ) );
			}
		}

		protected override void FireTargetableStateUpdated( TargetableState previousSelectionState, TargetableState newSelectionState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if (BattleEntityTargetableStateUpdated != null) 
			{
				BattleEntityTargetableStateUpdated( this, new BattleEntityTargetableStateTransitionEventArgs( CurrentState, previousSelectionState, newSelectionState, layersHitBehindEntity ) );

			}
		}

		protected void FirePlayableStateUpdated( PlayableState previousPlayableState, PlayableState newPlayableState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( PlayableStateUpdated != null )
			{
				PlayableStateUpdated( this, new BattleEntityPlayableStateTransitionEventArgs( previousPlayableState, newPlayableState, layersHitBehindEntity ) );
			}
		}

		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}
