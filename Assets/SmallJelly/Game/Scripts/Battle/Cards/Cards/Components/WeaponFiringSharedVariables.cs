﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker.Actions;
using HutongGames.PlayMaker;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class WeaponFiringSharedVariables : SJMonoBehaviour
	{
		#region Public Properties
		public GameObject Projectile
		{
			get
			{
				return mProjectile;
			}
		}


		public GameObject ProjectileImpact
		{
			get
			{
				return mProjectileImpact;
			}
		}

		public GameObject ProjectileTrail
		{
			get
			{
				return mProjectileTrail;
			}
		}

		public GameObject MuzzleFlash
		{
			get
			{
				return mMuzzleFlash;
			}
		}


		public string FireSoundEffectName
		{
			get
			{
				return mFireSoundEffectName;
			}
		}

		public string ImpactSoundEffectName
		{
			get
			{
				return mImpactSoundEffectName;
			}
		}

		public string ExplosionSoundEffectName
		{
			get
			{
				return mExplosionSoundEffectName;
			}
		}



		public string WeaponType
		{
			get
			{
				return mWeaponType;
			}
		}

		public int NumberOfProjectiles
		{
			get
			{
				return mNumberOfProjectiles;
			}
		}

		public float ProjectileSpeed
		{
			get
			{
				return mProjectileSpeed;
			}
		}

		public float ProjectileFireDelay
		{
			get
			{
				return mProjectileFireDelay;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private GameObject mProjectile;

		[SerializeField]
		private GameObject mProjectileImpact;

		[SerializeField]
		private GameObject mProjectileTrail;

		[SerializeField]
		private GameObject mMuzzleFlash;


		[SerializeField]
		private string mFireSoundEffectName;

		[SerializeField]
		private string mImpactSoundEffectName;

		[SerializeField]
		private string mExplosionSoundEffectName;


		[SerializeField]
		private string mWeaponType;

		[SerializeField]
		private int mNumberOfProjectiles;

		[SerializeField]
		private float mProjectileSpeed;

		[SerializeField]
		private float mProjectileFireDelay;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}
		#endregion
	}
}