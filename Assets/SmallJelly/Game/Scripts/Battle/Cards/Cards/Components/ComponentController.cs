﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class ComponentController : BattleEntityController
	{
		#region Enums
		public enum BasePlateState
		{
			Lowered,
			HalfRaised,
			FullyRaised
		}

		public enum HighlightedState
		{
			NotHighlighted,
			Highlighted
		}
		#endregion

		#region Public Event Handlers
		public event EventHandler<BattleEntityHighlightedStateTransitionEventArgs> HighlightedStateUpdated;
		#endregion


		#region Member Variables
		private bool mPressedComponent;
		#endregion

		#region Protected Properties
		protected BasePlateState CurrentBasePlateState 
		{ 
			get 
			{
				return mCurrentBasePlateState;
			}
		}

		protected HighlightedState CurrentHighlightedState 
		{ 
			get 
			{
				return mCurrentHighlightedState;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private BasePlateState mCurrentBasePlateState; 

		[SerializeField]
		private HighlightedState mCurrentHighlightedState;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
			BattleEntityView = GetComponent<ComponentView>();
		}
		#endregion

		#region Public Methods
		public override void OnInsertTargetableState( TargetableState targetableState )
		{
			base.OnInsertTargetableState( targetableState );

			//Whenever we change targetable state we want to cancel all component highlights
			StopHighlightingComponent();

			switch( targetableState )
			{
				case TargetableState.Targeting:
					StartRaiseComponent();
					break;
			}
		}

		public void OnInsertBasePlateState( BasePlateState basePlateState )
		{				
			mCurrentBasePlateState = basePlateState;
			ComponentView componentBattleEntityView = (ComponentView) BattleEntityView;

			Dictionary< string, NamedVariable > animationMetaDataDictionary = new Dictionary< string, NamedVariable >();

			AnimationMetaData animationMetaData = new AnimationMetaData( animationMetaDataDictionary );
			componentBattleEntityView.OnPlayBasePlateAnimation( CurrentOwner, mCurrentBasePlateState, animationMetaData );
		}

		public void OnInsertHighlightedState( HighlightedState highlightedState )
		{
			if( mCurrentHighlightedState != highlightedState )
			{
				HighlightedState previousHighlightedState = mCurrentHighlightedState;
				FireHighlightedStateUpdated( previousHighlightedState, highlightedState, LayersHitBehindEntityThisFrame );
				mCurrentHighlightedState = highlightedState;
			}
		}

		public override void OnInsertState( State cardState, AnimationMetaData animationMetaData )
		{
			base.OnInsertState( cardState, animationMetaData );

			//If we're about to leave the board then let's stop highlighting the component and deselect it!
			switch( cardState )
			{
				case State.ComponentJettison:
				case State.ComponentRemovingFromBoard:
					StopHighlightingComponent();
					DeselectComponent();
					StopTargetingComponent();
					break;
			}
		}

		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			StartHighlightingComponent();
			StartHalfRaiseComponent();
		}

		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			
		}

		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			StopHalfRaiseComponent();
			StopHighlightingComponent();
		}
			

		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}

		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}

		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
		}


		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			mPressedComponent = true;
		}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			if( mPressedComponent )
			{
				switch(CurrentState)
				{
					//Player has clicked the component
					case State.ComponentDestroyed:
					case State.ComponentDestroying:
					case State.ComponentIdle:
					case State.ComponentInactive:
						SelectComponent();
						break;
				}	
			}

			mPressedComponent = false;
		}

		//If player hits an object other than this one
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentTargetableState)
			{
				//Player is no longer dragging
				case TargetableState.Targeting:
					StopTargetingComponent();
					break;
			}

			//We should be able to deselect the component in any state
			DeselectComponent();

			mPressedComponent = false;
		}
		#endregion

		#region Touch Controls
		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			StopHighlightingComponent();
		}
		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			StartHighlightingComponent();

			mPressedComponent = true;
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			if( mPressedComponent )
			{
				switch(CurrentState)
				{
					//Player has clicked the component
					case State.ComponentDestroyed:
					case State.ComponentDestroying:
					case State.ComponentIdle:
					case State.ComponentInactive:
						SelectComponent();
						break;
				}	
			}

			StopHighlightingComponent();

			mPressedComponent = false;
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch(CurrentTargetableState)
			{
				//Player is no longer dragging
				case TargetableState.Targeting:
					StopTargetingComponent();
					break;
			}

			//We should be able to deselect the component in any state
			DeselectComponent();
			StopHighlightingComponent();

			mPressedComponent = false;
		}
		#endregion

		#region Protected Methods
		protected override void OnLeaveState( State cardState )
		{
			base.OnLeaveState( cardState );
		}

		protected override void OnLeaveTargetableState( TargetableState targetableState )
		{
			base.OnLeaveTargetableState( targetableState );

			switch(targetableState)
			{
				//Player is no longer dragging
				case TargetableState.Targeting:
					StopRaiseComponent();
					break;
			}

		}

		protected override void OnAnimationsFinished(){}

		protected void StartHalfRaiseComponent()
		{
			if( CurrentTargetableState == TargetableState.NonTargetable )
			{
				return;
			}

			//Only raise local components unless its a targetable enemy component
			if( CurrentOwner != PlayerType.Local && CurrentTargetableState != TargetableState.Targetable )
			{
				return;
			}

			if( CurrentBasePlateState != BasePlateState.Lowered )
			{
				return;
			}

			OnInsertBasePlateState( BasePlateState.HalfRaised );
		}

		protected void StopHalfRaiseComponent()
		{
			if( CurrentBasePlateState != BasePlateState.HalfRaised )
			{
				return;
			}

			OnInsertBasePlateState( BasePlateState.Lowered );
		}

		protected void StartRaiseComponent()
		{
			OnInsertBasePlateState( BasePlateState.FullyRaised );
		}

		protected void StopRaiseComponent()
		{
			if( CurrentBasePlateState != BasePlateState.FullyRaised )
			{
				return;
			}

			OnInsertBasePlateState( BasePlateState.Lowered );
		}
		#endregion

		#region Private Methods
		private void SelectComponent()
		{
			if( CurrentTargetableState != TargetableState.None )
			{
				return;
			}

			//If this card belongs to the local player
			if( BattleManager.Get.CurrentTurn != PlayerType.Local )
			{
				return;
			}

			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			OnInsertSelectionState( SelectionState.Selected );
			StartRaiseComponent();
		}

		private void DeselectComponent()
		{
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}
				
			if( CurrentState == State.ComponentPrefire )
			{
				return;
			}

			OnInsertSelectionState( SelectionState.NotSelected );
			StopRaiseComponent();
		}

		private void StopTargetingComponent()
		{
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			if( CurrentTargetableState != TargetableState.Targeting )
			{
				return;
			}

			OnInsertTargetableState( BattleEntityController.TargetableState.Targeted );
		}

		private void StartHighlightingComponent()
		{
			OnInsertHighlightedState( HighlightedState.Highlighted );
		}

		private void StopHighlightingComponent()
		{
			OnInsertHighlightedState( HighlightedState.NotHighlighted );
		}
		#endregion

		#region Event Firing
		protected void FireHighlightedStateUpdated( HighlightedState previousHighlightedState, HighlightedState newHighlightedState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( HighlightedStateUpdated != null )
			{
				HighlightedStateUpdated( this, new BattleEntityHighlightedStateTransitionEventArgs( previousHighlightedState, newHighlightedState, layersHitBehindEntity ) );
			}
		}
		#endregion
	}
}