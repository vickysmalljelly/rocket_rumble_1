﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	 public class ComponentView : BattleEntityView 
	{
		#region Public Properties
		public override BattleEntityParts Parts
		{
			get
			{
				return Template.ComponentParts;
			}
		}
		#endregion

		#region MonoBehaviour Methods
		protected override void OnEnable()
		{
			base.OnEnable();

			foreach( Collider collider in TemplateBattleEntityParts.ComponentColliders )
			{
				collider.enabled = true;
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();

			foreach( Collider collider in TemplateBattleEntityParts.ComponentColliders )
			{
				collider.enabled = false;
			}
		}
		#endregion

		#region Public Methods
		public override void OnRefreshModelWithData( BattleEntityData battleEntityData )
		{
			base.OnRefreshModelWithData( battleEntityData );

			switch( battleEntityData.EntityType )
			{
			case BattleEntityType.Weapon:
				RefreshWeapon( (WeaponData) battleEntityData );
				break;

			case BattleEntityType.TargetingCrew:
			case BattleEntityType.NonTargetingCrew:
				RefreshCrew( (CrewData) battleEntityData );
				break;

			case BattleEntityType.Utility:
				RefreshUtility( (UtilityData) battleEntityData );
				break;

			case BattleEntityType.Virus:
				RefreshVirus( (VirusData) battleEntityData );
				break;

			case BattleEntityType.Unknown:
				RefreshUnknown( (UnknownData) battleEntityData );
				break;

			default:
				SJLogger.LogError( "Entity type {0} not recognised", battleEntityData.EntityType );
				break;

			}
		}

		public void OnPlayBasePlateAnimation( PlayerType playerType, ComponentController.BasePlateState state, AnimationMetaData animationMetaData )
		{
			//BasePlateAnimationController.UpdateAnimations( playerType, state, animationMetaData );
		}
		#endregion

		#region Protected Methods
		protected override void EnableParts()
		{
			if( Parts == null )
			{
				return;
			}

			base.EnableParts();

			SetActiveRecursively( Parts.CardText, false );
			SetActiveRecursively( Parts.Card, false );
			SetActiveRecursively( Parts.Component, true );
			SetActiveRecursively( Parts.ComponentHousing, false );
			SetActiveRecursively( Parts.Mask, false );
			SetActiveRecursively( Parts.BasePlate, true );

			//TODO - We could instantiate the jettison jets at runtime as they're only needed during installation
			SetActiveRecursively( Parts.InstallationJets, false );
			SetActiveRecursively( Parts.InstallationBeam, false );
		}

		protected override void DisableParts()
		{
			if( Parts == null )
			{
				return;
			}

			base.DisableParts();

			SetActiveRecursively( Parts.CardText, false );
			SetActiveRecursively( Parts.Card, false );
			SetActiveRecursively( Parts.Component, false );
			SetActiveRecursively( Parts.ComponentHousing, false );
			SetActiveRecursively( Parts.Mask, false );
			SetActiveRecursively( Parts.BasePlate, false );

			//TODO - We could instantiate the jettison jets at runtime as they're only needed during installation
			SetActiveRecursively( Parts.InstallationJets, false );
			SetActiveRecursively( Parts.InstallationBeam, false );
		}
		#endregion

		#region Private Methods
		private void RefreshWeapon( WeaponData weaponData )
		{
			Parts.HP.text = string.Format( "{0}", weaponData.HP );
			Parts.Damage.text = string.Format( "{0}", weaponData.Attack );
		}

		private void RefreshCrew( CrewData crewData ){}

		private void RefreshUtility( UtilityData utilityData )
		{
			Parts.HP.text = string.Format( "{0}", utilityData.HP );
		}

		private void RefreshVirus( VirusData virusData ){}

		private void RefreshUnknown( UnknownData unknownData ){}
		#endregion
	}
}