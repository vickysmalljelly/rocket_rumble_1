﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WeaponController : ComponentController
	{
		#region Member Variables
		private bool mPressedComponent;
		#endregion

		#region Public Methods
		public override void OnInsertState( State cardState, AnimationMetaData animationMetaData )
		{
			base.OnInsertState( cardState, animationMetaData );
		}
		#endregion

		#region Mouse Controls
		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnMouseDraggedOff( elementsHit, movementDuringFrame );

			switch(CurrentState)
			{
				case State.ComponentIdle:
					StartDraggingComponent();
					break;
			}
		}


		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			base.OnMousePressedOver( elementsHitBehind, movementDuringFrame );

			mPressedComponent = true;
		}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHitBehind, Vector2 movementDuringFrame )
		{
			base.OnMouseReleasedOver( elementsHitBehind, movementDuringFrame );

			switch(CurrentState)
			{
				//Player has clicked the component
				case State.ComponentIdle:
					StartDraggingComponent();
					break;
			}

			mPressedComponent = false;
		}

		//If player hits an object other than this one
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnMouseReleasedOff( elementsHit, movementDuringFrame );

			mPressedComponent = false;
		}

		public override void OnRollback()
		{
			base.OnRollback();

			switch( CurrentState )
			{
				case State.ComponentPrefire:
					OnInsertState( State.ComponentPostfire );
					break;
			}
		}
		#endregion

		#region Touch Controls
		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnTouchDraggedOff( elementsHit, movementDuringFrame );

			switch(CurrentState)
			{
				//Player has clicked the component
				case State.ComponentIdle:
					StartDraggingComponent();
					break;
			}
		}

		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnTouchPressedOver( elementsHit, movementDuringFrame );

			mPressedComponent = true;
		}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnTouchReleasedOver( elementsHit, movementDuringFrame );

			switch(CurrentState)
			{
				//Player has clicked the component
				case State.ComponentIdle:
					StartDraggingComponent();
					break;
			}

			mPressedComponent = false;
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			base.OnTouchReleasedOff( elementsHit, movementDuringFrame );

			mPressedComponent = false;
		}
		#endregion

		#region Protected Methods
		protected override void OnAnimationsFinished()
		{
			base.OnAnimationsFinished();

			switch(CurrentState)
			{
				case State.ComponentPostfire:
					OnInsertState(State.ComponentIdle);
					break;
			}
		}
		#endregion

		#region Private Methods
		private void StartDraggingComponent()
		{
			if( CurrentTargetableState != TargetableState.None )
			{
				return;
			}

			//If this card belongs to the local player
			if( CurrentOwner != PlayerType.Local )
			{
				return;
			}

			//If it's the players turn and the component was pressed then start dragging
			if( !( BattleManager.Get.CurrentTurn == PlayerType.Local && mPressedComponent ) )
			{
				return;
			}

			OnInsertTargetableState( BattleEntityController.TargetableState.Targeting );
		}
		#endregion
	}
}