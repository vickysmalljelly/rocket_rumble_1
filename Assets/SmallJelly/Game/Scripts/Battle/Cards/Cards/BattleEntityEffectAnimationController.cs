﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System.IO;

namespace SmallJelly
{
	public class BattleEntityEffectAnimationController : TargetableEffectAnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Effects/"; } }
		#endregion

		#region Constants
		private const string DESTROYED_ANIMATION_TEMPLATE_NAME = "Destroyed";
		private const string ONLINE_ANIMATION_TEMPLATE_NAME = "Online";
		private const string OFFLINE_ANIMATION_TEMPLATE_NAME = "Offline";
		private const string PLAYABLE_ANIMATION_TEMPLATE_NAME = "Playable";
		private const string TARGET_LOCK_ANIMATION_TEMPLATE_NAME = "TargetLock";
		private const string DAMAGE_BUFF_ANIMATION_TEMPLATE_NAME = "DamageBuff";
		private const string HEALTH_BUFF_ANIMATION_TEMPLATE_NAME = "HealthBuff";

		private const string ADD_ANIMATION_TEMPLATE_NAME_EXTENSION = "Add";
		private const string REMOVE_ANIMATION_TEMPLATE_NAME_EXTENSION = "Remove";
		#endregion

		#region Public Methods
		public void PlayRemoveAnimations( BattleEntityEffectController.BattleEntityEffect[] effectsToRemove )
		{
			foreach( BattleEntityEffectController.BattleEntityEffect effectToRemove in effectsToRemove )
			{
				string path = GetPath( effectToRemove );
				string finalPath = string.Format( "{0}{1}", path, REMOVE_ANIMATION_TEMPLATE_NAME_EXTENSION );

				PlayAnimation( finalPath );
			}
		}

		public void PlayAddAnimations( BattleEntityEffectController.BattleEntityEffect[] effectsToAdd )
		{
			foreach( BattleEntityEffectController.BattleEntityEffect effectToAdd in effectsToAdd )
			{
				string path = GetPath( effectToAdd );
				string finalPath = string.Format( "{0}{1}", path, ADD_ANIMATION_TEMPLATE_NAME_EXTENSION );

				PlayAnimation( finalPath );
			}
		}
		#endregion

		#region Private Methods
		private string GetPath( BattleEntityEffectController.BattleEntityEffect effect )
		{
			switch( effect )
			{
				case BattleEntityEffectController.BattleEntityEffect.Destroyed:
					return DESTROYED_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.Online:
					return ONLINE_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.Offline:
					return OFFLINE_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.Playable:
					return PLAYABLE_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.TargetLock:
					return TARGET_LOCK_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.DamageBuff:
					return DAMAGE_BUFF_ANIMATION_TEMPLATE_NAME;

				case BattleEntityEffectController.BattleEntityEffect.HealthBuff:
					return HEALTH_BUFF_ANIMATION_TEMPLATE_NAME;
			}
				
			SJLogger.Assert( "No animation was found for the supplied type {0}", effect.ToString() );

			return string.Empty;
		}
		#endregion
	}
}
