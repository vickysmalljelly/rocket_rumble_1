using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntity : Targetable
	{
		#region Public Events
		public event EventHandler<BattleEntityEventArgs> AnimationFinished;
		public event EventHandler<BattleEntityAndStateTransitionEventArgs> StateTransitioned;
		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> TargetableStateTransitioned;
		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> SelectionStateTransitioned;
		public event EventHandler<BattleEntityAndHighlightedStateTransitionEventArgs> HighlightedStateTransitioned;
		#endregion

		#region Public Properties
		//The data of the card right now in the flow - used for stat displays etc
		public BattleEntityData Data
		{
			get;
			private set;
		}

		public BattleEntityTargetingData TargetingData
		{
			get 
			{
				return mTargetingData;
			}
		}

		public ObjectInputListener ObjectInputListener { get; set; }

		public BattleEntityController BattleEntityController
		{
			get
			{
				return (BattleEntityController)TargetableLogicController;
			}

			set
			{
				TargetableLogicController = value;
			}
		}

		public BattleEntityEffectController BattleEntityEffectController
		{
			get
			{
				return (BattleEntityEffectController)EffectController;
			}

			set
			{
				EffectController = value;
			}
		}
		#endregion

		#region Member Variables
		private string mServerState;

		private BattleEntityTargetingData mTargetingData;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnStartTurn( PlayerType playerType )
		{
			//It's not my turn!
			if( Data.State == BattleEntityState.InHandPlayable && playerType != PlayerType.Local )
			{
				OnRefreshLogicState( BattleEntityState.InHandNotMyTurn );
			}
			else
			{
				OnRefreshLogicState( Data.State );
			}

			OnRefreshModelWithData( Data );
			OnRefreshEffectWithData( Data );
			OnRefreshSpriteWithData( Data );
		}

		public void OnRefreshLogicState( string serverState )
		{
			RefreshLogicState( serverState );
		}

		public void OnRefreshModelWithData( BattleEntityData battleEntityData )
		{
			Data = battleEntityData;

			//Refresh the model
			BattleEntityController.OnRefreshModelWithData( battleEntityData );
		}

		public void OnRefreshEffectWithData( BattleEntityData battleEntityData )
		{
			Data = battleEntityData;

			//Refresh the effects
			BattleEntityEffectController.OnRefreshData( battleEntityData );
		}

		public void OnRefreshSpriteWithData( BattleEntityData battleEntityData )
		{
			Data = battleEntityData;

			//Refresh the sprite
			BattleEntityController.OnRefreshSpriteWithData( battleEntityData );
		}

		public void OnRefreshTargetData( BattleEntityTargetingData battleEntityTargetingData )
		{
			mTargetingData = battleEntityTargetingData;
		}

		public void OnInsertState( BattleEntityController.State battleEntityState )
		{
			RefreshController( battleEntityState );
			BattleEntityController.OnInsertState( battleEntityState );
		}

		public void OnInsertPlayableState( CardController.PlayableState playableState )
		{
			BattleEntityController.OnInsertPlayableState( playableState );
		}

		public void OnInsertState( BattleEntityController.State battleEntityState, AnimationMetaData animationMetaData )
		{
			RefreshController( battleEntityState );
			BattleEntityController.OnInsertState( battleEntityState, animationMetaData );
		}

		public void OnReturnToPreviousState()
		{
			BattleEntityController.OnReturnToPreviousState();
		}

		public void OnInsertOwner( PlayerType owner )
		{
			BattleEntityController.OnInsertOwner( owner );
		}

		public void OnRollback()
		{
			BattleEntityController.OnRollback();
		}
		#endregion

		#region Private Methods
		//Some states have some kind of "logic" attached to them - so for example if a component is destroyed then
		//it moves to a state where it cannot be used etc. This is updated here.
		private bool RefreshLogicState( string serverState )
		{
			//If the state hasn't changed then do nothing
			if( mServerState == serverState )
			{
				return false;
			}

			//Switch to the new logic state
			mServerState = serverState;

			switch( serverState )
			{
				case BattleEntityState.InHandPlayable:
					OnInsertPlayableState( CardController.PlayableState.Playable );
					break;

				default:
					OnInsertPlayableState( CardController.PlayableState.NonPlayable );
					break;
			}

			switch( serverState )
			{
                case BattleEntityState.InHandPlayable:
					OnInsertState( BattleEntityController.State.CardIdle );
					return true;

				case BattleEntityState.InHandNotEnoughPower:
				case BattleEntityState.InHandNoTarget:
                case BattleEntityState.InHandNotMyTurn:
					OnInsertState( BattleEntityController.State.CardIdle );
					return true;

                case BattleEntityState.Online:
                case BattleEntityState.OnlineDoubleShot:
					OnInsertState( BattleEntityController.State.ComponentIdle );
					return true;

				case BattleEntityState.Offline:
				case BattleEntityState.Spawning:
                case BattleEntityState.Installing:
                case BattleEntityState.Fired:
					OnInsertState( BattleEntityController.State.ComponentInactive );
					return true;

                case BattleEntityState.Destroyed:
					OnInsertState( BattleEntityController.State.ComponentDestroyed );
					return true;

				//TODO - Initailized shouldnt even do anything but most cards erroneously set in initialization
				//state so let's allow them to be playable
				case BattleEntityState.Initialised:
					OnInsertPlayableState( CardController.PlayableState.Playable );
					return true;

				case BattleEntityState.Jettisoned:
				case BattleEntityState.Unknown:
                case BattleEntityState.Playing:
					return false;
					
				default:
					SJLogger.Assert( "No case was defined for the state {0}", serverState );
					return false;
			}
		}
			
		private bool RefreshController( BattleEntityController.State battleEntityState )
		{
			BattleEntityType battleEntityType = default( BattleEntityType );

			if( Data != null )
			{
				battleEntityType = Data.EntityType;
			}
				
			//TODO - I would love to change this so the card controller is added on "AddingToHand" and removed on "RemovingFromHand"
			//and the ComponentController is added on "AddingToBoard" and removed on "RemovingFromBoard". It's a much neater pattern
			bool updated = false;
			switch( battleEntityState ) 
			{
				case BattleEntityController.State.CardInitialization:
					break;

				//Behaviours to do with the hand
				case BattleEntityController.State.CardSetup:
				case BattleEntityController.State.CardAddingToHand:
				case BattleEntityController.State.CardRemovingFromHand:
				case BattleEntityController.State.CardDrawing:
				case BattleEntityController.State.CardIdle:
				case BattleEntityController.State.CardRollover:
				case BattleEntityController.State.CardHover:
				case BattleEntityController.State.CardRolloff:
				case BattleEntityController.State.CardDragging:
				case BattleEntityController.State.CardDraggingReleased:
				case BattleEntityController.State.CardReadyToReturnToHand:
				case BattleEntityController.State.CardDissapearing:
				case BattleEntityController.State.CardPlaying:
					updated = UpdateController< CardController >();
				break;

				//Behaviours to do with played components
				case BattleEntityController.State.ComponentAddingToBoard:
				case BattleEntityController.State.ComponentRemovingFromBoard:
				case BattleEntityController.State.ComponentMoveToSocket:
				case BattleEntityController.State.ComponentInstallation:
				case BattleEntityController.State.ComponentInactive:
				case BattleEntityController.State.ComponentPrefire:
				case BattleEntityController.State.ComponentFire:
				case BattleEntityController.State.ComponentPostfire:
				case BattleEntityController.State.ComponentIdle:
				case BattleEntityController.State.ComponentDestroying:
				case BattleEntityController.State.ComponentDestroyed:
				case BattleEntityController.State.ComponentTakeDamage:
				case BattleEntityController.State.ComponentJettison:
				case BattleEntityController.State.ComponentRepair:
				case BattleEntityController.State.ComponentBuff:
					switch( battleEntityType)
					{
						case BattleEntityType.Weapon:
							updated = UpdateController<WeaponController>();
							break;
						
						default:
							updated = UpdateController<ComponentController>();
							break;
					}
				break;

				default:
					SJLogger.Assert( "The controller type was not defined for {0}", battleEntityState );
				break;
			}

			return updated;
		}

		private bool UpdateController<T>() where T : BattleEntityController
		{
			PlayerType currentOwner = default(PlayerType);
			Stack< BattleEntityController.State > previousStates = new Stack<BattleEntityController.State>();
			TargetableLogicController.SelectionState battleEntitySelectionState = default( TargetableLogicController.SelectionState );
			TargetableLogicController.TargetableState battleEntityTargetableState = default( TargetableLogicController.TargetableState );
			BattleEntityController.PlayableState battleEntityPlayableState = default( BattleEntityController.PlayableState );

			if( BattleEntityController )
			{
				//Controller is already of the specified type
				if( BattleEntityController is T )
				{
					return false;
				}
					
				currentOwner = BattleEntityController.CurrentOwner;
				previousStates = BattleEntityController.PreviousStates;
				battleEntitySelectionState = BattleEntityController.CurrentSelectionState;
				battleEntityTargetableState = BattleEntityController.CurrentTargetableState;
				battleEntityPlayableState = BattleEntityController.CurrentPlayableState;

				//Unregister listeners for existing controller
				UnregisterListeners();

				//Remove existing controller immediately
				DestroyImmediate( BattleEntityController );
			}
				
			//Add new controller
			BattleEntityController = gameObject.AddComponent<T>();

			SJLogger.LogMessage( MessageFilter.George, "Assigning {0} the battle entity controller with the type {1}", gameObject.name, BattleEntityController.GetType().ToString() );

			//Register listeners for new controller
			RegisterListeners();

			BattleEntityController.OnInsertOwner( currentOwner );
			BattleEntityController.OnInsertPreviousStates( previousStates );
			BattleEntityController.OnInsertSelectionState( battleEntitySelectionState );
			BattleEntityController.OnInsertTargetableState( battleEntityTargetableState );
			BattleEntityController.OnInsertPlayableState( battleEntityPlayableState );
			return true;
		}
		#endregion

		#region Event Registration
		private void RegisterInputListeners()
		{
			//We register the input listeners with methods in this class opposed to directly with the controller
			//because the controller often changes - it's often swapped out
		
			//Mouse
			ObjectInputListener.MouseRolledOver += HandleMouseRolledOver;
			ObjectInputListener.MouseHovered += HandleMouseHovered;
			ObjectInputListener.MouseRolledOff += HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver += HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged += HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff += HandleMouseDraggedOff;

			ObjectInputListener.MousePressed += HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver += HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff += HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver += HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged += HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff += HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed += HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver += HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff += HandleTouchReleasedOff;
		}

		private void UnregisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver -= HandleMouseRolledOver;
			ObjectInputListener.MouseHovered -= HandleMouseHovered;
			ObjectInputListener.MouseRolledOff -= HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver -= HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged -= HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff -= HandleMouseDraggedOff;

			ObjectInputListener.MousePressed -= HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver -= HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff -= HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver -= HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged -= HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff -= HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed -= HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver -= HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff -= HandleTouchReleasedOff;
		}

		private void RegisterListeners()
		{
			BattleEntityController.AnimationFinished += HandleAnimationFinished;

			BattleEntityController.BattleEntityStateUpdated += HandleStateUpdated;
			BattleEntityController.BattleEntityTargetableStateUpdated += HandleTargetableStateUpdated;

			BattleEntityController.SelectionStateUpdated += HandleSelectionStateUpdated;

			if( BattleEntityController is ComponentController )
			{
				ComponentController componentController = (ComponentController)BattleEntityController;
				componentController.HighlightedStateUpdated += HandleHighlightedStateUpdated;
			}
		}

		private void UnregisterListeners()
		{
			BattleEntityController.AnimationFinished -= HandleAnimationFinished;

			BattleEntityController.BattleEntityStateUpdated -= HandleStateUpdated;
			BattleEntityController.BattleEntityTargetableStateUpdated -= HandleTargetableStateUpdated;

			BattleEntityController.SelectionStateUpdated -= HandleSelectionStateUpdated;

			if( BattleEntityController is ComponentController )
			{
				ComponentController componentController = (ComponentController)BattleEntityController;
				componentController.HighlightedStateUpdated -= HandleHighlightedStateUpdated;
			}
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, new BattleEntityEventArgs( this ) );
			}
		}

		private void FireStateTransitioned( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new BattleEntityAndStateTransitionEventArgs(battleEntity, previousState, newState, hitLayers) );
			}
		}

		private void FireTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.State state, BattleEntityController.TargetableState previousTargetableState, BattleEntityController.TargetableState newTargetableState, SJRenderRaycastHit[] hitLayers )
		{
			if( TargetableStateTransitioned != null )
			{
				TargetableStateTransitioned( this, new BattleEntityAndTargetableStateTransitionEventArgs(battleEntity, state, previousTargetableState, newTargetableState, hitLayers) );
			}
		}

		private void FireSelectionStateTransitioned( BattleEntity battleEntity, BattleEntityController.SelectionState previousState, BattleEntityController.SelectionState newState, SJRenderRaycastHit[] hitLayers )
		{
			if( SelectionStateTransitioned != null )
			{
				SelectionStateTransitioned( this, new BattleEntityAndSelectionStateTransitionEventArgs(battleEntity, previousState, newState, hitLayers) );
			}
		}

		private void FireHighlightedStateTransitioned( BattleEntity battleEntity, ComponentController.HighlightedState previousState, ComponentController.HighlightedState newState, SJRenderRaycastHit[] renderRaycastHitLayers )
		{
			if( HighlightedStateTransitioned != null )
			{
				HighlightedStateTransitioned( this, new BattleEntityAndHighlightedStateTransitionEventArgs( battleEntity, previousState, newState, renderRaycastHitLayers ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			FireAnimationFinished();
		}

		private void HandleStateUpdated( object o, BattleEntityStateTransitionEventArgs battleEntityStateEventArgs )
		{
			bool refreshed = RefreshController( battleEntityStateEventArgs.NewState );

			if( refreshed )
			{
				OnInsertState( battleEntityStateEventArgs.NewState );
			}
			else
			{
				FireStateTransitioned( this, battleEntityStateEventArgs.PreviousState, battleEntityStateEventArgs.NewState, battleEntityStateEventArgs.EntityHitLayers );
			}
		}

		private void HandleSelectionStateUpdated( object o, SelectionStateTransitionEventArgs eventArgs )
		{
			FireSelectionStateTransitioned( this, eventArgs.PreviousState, eventArgs.NewState, eventArgs.EntityHitLayers );
		}

		private void HandleTargetableStateUpdated( object o, BattleEntityTargetableStateTransitionEventArgs eventArgs )
		{
			FireTargetableStateTransitioned( this, eventArgs.State, eventArgs.PreviousState, eventArgs.NewState, eventArgs.EntityHitLayers );
		}

		private void HandleHighlightedStateUpdated( object o, BattleEntityHighlightedStateTransitionEventArgs eventArgs )
		{
			FireHighlightedStateTransitioned( this, eventArgs.PreviousState, eventArgs.NewState, eventArgs.EntityHitLayers );
		}
		#endregion
	}
}