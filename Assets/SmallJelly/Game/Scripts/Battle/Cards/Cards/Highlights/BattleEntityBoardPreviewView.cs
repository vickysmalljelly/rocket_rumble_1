﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	//Preview view of the battle entity, as displayed when the player hovers over a component on the board
	public class BattleEntityBoardPreviewView : SJMonoBehaviour 
	{
		#region Constants
		private const int MAXIMUM_TEXT_LENGTH = 20;
		#endregion

		#region Public Event Handlers
		public event EventHandler AnimationFinished
		{
			add
			{
				AnimationController.AnimationFinished += value;
			}

			remove
			{
				AnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public TemplateBattleEntityModel Template
		{ 
			get;
			set;
		}

		public TemplateBattleEntityParts TemplateBattleEntityParts
		{ 
			get;
			set;
		}

		public CardParts Parts
		{
			get
			{
				return Template.CardParts;
			}
		}

		public BattleEntityBoardPreviewAnimationController AnimationController { protected get; set; }
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}

		private void OnEnable()
		{
			
		}

		private void OnDestroy()
		{
			//Remove the animation controller
			Destroy( AnimationController );
		}
		#endregion

		#region Public Methods
		public void OnPlayAnimation( BattleEntityBoardPreviewController.State state )
		{
			AnimationController.UpdateAnimations( state );
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			Template.OnRefresh( battleEntityData );
			Parts.OnRefresh( battleEntityData );

			DisableParts();
		}
		#endregion

		#region Private Methods
		private void DisableParts()
		{
			//The board preview is neither interested in colliders - nor in the base plate
			foreach( Collider collider in TemplateBattleEntityParts.CardColliders )
			{
				collider.enabled = false;
			}

			foreach( Collider collider in TemplateBattleEntityParts.ComponentColliders )
			{
				collider.enabled = false;
			}

			Parts.BasePlate.SetActive (false);
		}
		#endregion
	}
}