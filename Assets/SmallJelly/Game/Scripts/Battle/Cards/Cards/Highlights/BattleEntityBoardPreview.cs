﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	//Preview of the battle entity, as displayed when the player hovers over a component on the board
	public class BattleEntityBoardPreview : SJMonoBehaviour 
	{
		#region Public Events
		public event EventHandler<BattleEntityBoardPreviewAndStateTransitionEventArgs> StateTransitioned;
		#endregion

		#region Member Variables
		private BattleEntityBoardPreviewController mBattleEntityBoardPreviewController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			mBattleEntityBoardPreviewController = gameObject.AddComponent<BattleEntityBoardPreviewController>();
			RegisterListeners();
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			mBattleEntityBoardPreviewController.OnRefreshData( battleEntityData );
		}

		public void OnInsertState( BattleEntityBoardPreviewController.State battleEntityBoardPreviewState )
		{
			mBattleEntityBoardPreviewController.OnInsertState( battleEntityBoardPreviewState );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mBattleEntityBoardPreviewController.StateTransitioned += HandleStateTransitioned;
		}

		private void UnregisterListeners()
		{
			mBattleEntityBoardPreviewController.StateTransitioned -= HandleStateTransitioned;
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( BattleEntityBoardPreview battleEntityBoardPreview, BattleEntityBoardPreviewController.State previousState, BattleEntityBoardPreviewController.State newState )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new BattleEntityBoardPreviewAndStateTransitionEventArgs(battleEntityBoardPreview, previousState, newState ) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, BattleEntityBoardPreviewStateTransitionEventArgs battleEntityBoardPreviewStateTransitionEventArgs )
		{
			FireStateTransitioned( this, battleEntityBoardPreviewStateTransitionEventArgs.PreviousState, battleEntityBoardPreviewStateTransitionEventArgs.NewState );
		}
		#endregion
	}
}