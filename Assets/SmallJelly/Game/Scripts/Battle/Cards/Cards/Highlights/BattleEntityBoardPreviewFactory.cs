﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the previews of battle entities when the player hovers over one on the board.
	public class BattleEntityBoardPreviewFactory : CardFactory
	{
		#region Public Methods
		public static BattleEntityBoardPreview ConstructBattleEntity( BattleEntityData battleEntityData )
		{
			GameObject view = LoadCardView( CardType.BattleEntity );
			AssignComponents (view, battleEntityData );

			BattleEntityBoardPreview battleEntityBoardPreview = view.GetComponent<BattleEntityBoardPreview> ();

			//We don't want the passed in data to have a state so we change it and then change it back
			string currentState = battleEntityData.State;
			battleEntityData.State = string.Empty;

			battleEntityBoardPreview.OnRefreshData (battleEntityData);

			battleEntityData.State = currentState;

			return battleEntityBoardPreview;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, BattleEntityData battleEntityData )
		{
			view.SetActive( false );

			AssignAnimationControllers( view );
			AssignViews( view );
			AssignBattleEntityBoardPreview( view, battleEntityData );

			view.SetActive( true );
		}

		private static BattleEntityBoardPreviewAnimationController AssignAnimationControllers( GameObject view )
		{
			BattleEntityBoardPreviewAnimationController battleEntityBoardPreviewAnimationController = view.AddComponent< BattleEntityBoardPreviewAnimationController > ();
			return battleEntityBoardPreviewAnimationController;
		}

		private static BattleEntityBoardPreviewView AssignViews( GameObject view )
		{
			BattleEntityBoardPreviewView battleEntityBoardPreviewView = view.AddComponent< BattleEntityBoardPreviewView > ();
			battleEntityBoardPreviewView.Template = view.GetComponent< TemplateBattleEntityModel >();
			battleEntityBoardPreviewView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();

			battleEntityBoardPreviewView.AnimationController = view.GetComponent< BattleEntityBoardPreviewAnimationController > ();

			return battleEntityBoardPreviewView;
		}

		private static BattleEntityBoardPreview AssignBattleEntityBoardPreview( GameObject view, BattleEntityData battleEntityData )
		{
			BattleEntityBoardPreview battleEntityBoardPreview = view.AddComponent< BattleEntityBoardPreview > ();

			return battleEntityBoardPreview;
		}
		#endregion
	}
}
