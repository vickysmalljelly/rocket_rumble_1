﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class BattleEntityBoardPreviewAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/CardPreviews/"; } }
		#endregion

		#region Constants
		private const string ROLLOVER_ANIMATION_TEMPLATE_NAME = "RolloverAnimation";
		private const string HOVER_ANIMATION_TEMPLATE_NAME = "HoverAnimation";
		private const string ROLLOFF_ANIMATION_TEMPLATE_NAME = "RolloffAnimation";
		#endregion

		#region Methods
		public void UpdateAnimations( BattleEntityBoardPreviewController.State state )
		{
			StopExistingAnimations();

			switch(state)
			{
				case BattleEntityBoardPreviewController.State.Showing:
					PlayAnimation(ROLLOVER_ANIMATION_TEMPLATE_NAME);
				break;

				case BattleEntityBoardPreviewController.State.Shown:
					PlayAnimation(HOVER_ANIMATION_TEMPLATE_NAME);
				break;

				case BattleEntityBoardPreviewController.State.Hiding:
					PlayAnimation(ROLLOFF_ANIMATION_TEMPLATE_NAME);
				break;
			}
		}
		#endregion
	}
}
