﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class BattleEntityBoardPreviewController : SJMonoBehaviour 
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			Hidden,
			Showing,
			Shown,
			Hiding
		}
		#endregion

		#region Public Events
		public event EventHandler<BattleEntityBoardPreviewStateTransitionEventArgs> StateTransitioned;
		public event EventHandler AnimationFinished;
		#endregion

		#region Member Variables
		[SerializeField]
		private State mState;

		[SerializeField]
		private BattleEntityBoardPreviewView mBattleEntityBoardPreviewView;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			mBattleEntityBoardPreviewView = GetComponent<BattleEntityBoardPreviewView>();
			RegisterListeners();
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			mBattleEntityBoardPreviewView.OnRefreshData( battleEntityData );
		}

		public void OnInsertState( State previewState )
		{
			State previousState = mState;
			OnLeaveState( previousState );

			mState = previewState;
			mBattleEntityBoardPreviewView.OnPlayAnimation( previewState );
			FireStateTransitioned( previousState, mState );

		}
		#endregion

		#region Private Methods
		private void OnAnimationsFinished()
		{
			switch( mState )
			{
				case State.Showing:
					OnInsertState( State.Shown );
				break;

				case State.Hiding:
					OnInsertState( State.Hidden );
				break;
			}
		}


		private void OnLeaveState( State previewState )
		{
			
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mBattleEntityBoardPreviewView.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mBattleEntityBoardPreviewView.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			OnAnimationsFinished();
			FireAnimationFinished();
		}
		#endregion

		#region Event Firing
		protected void FireStateTransitioned( State previousState, State newState )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new BattleEntityBoardPreviewStateTransitionEventArgs( previousState, newState ) );
			}
		}

		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}