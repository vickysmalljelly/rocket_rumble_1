﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BattleEntityEffectController : TargetableEffectController
	{
		#region Battle Entity Effect State
		public enum BattleEntityEffect
		{
			Destroyed,

			Online,
			Offline,

			Playable,

			TargetLock,
			DamageBuff,
			HealthBuff
		}
		#endregion

		#region Member Variables
		private BattleEntityEffect[] mCurrentEffects = new BattleEntityEffect[ 0 ];

		private List< BattleEntityEffect > mPlayableEffects;
		private List< BattleEntityEffect > mBuffEffects;
		private List< BattleEntityEffect > mStateEffects;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake ()
		{
			base.Awake ();

			mPlayableEffects = new List< BattleEntityEffect >();
			mBuffEffects = new List< BattleEntityEffect >();
			mStateEffects = new List< BattleEntityEffect >();
		}
		#endregion

		#region Public Methods
		public void OnSwitchTurn( PlayerType playerType )
		{
			Refresh();
		}

		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			mBuffEffects = GetBuffEffects( battleEntityData.Buffs );
			mStateEffects = GetStateEffects( battleEntityData.State );
			mPlayableEffects = GetPlayableEffects( battleEntityData.State );

			Refresh();
		}
		#endregion

		#region Protected Methods
		protected void Refresh( BattleEntityEffect[] newEffects )
		{
			BattleEntityEffect[] effectsToRemove = mCurrentEffects.Where( o => !newEffects.Contains( o ) ).ToArray();
			BattleEntityEffect[] effectsToAdd = newEffects.Where( o => !mCurrentEffects.Contains( o ) ).ToArray();

			BattleEntityEffectAnimationController battleEntityEffectAnimationController = (BattleEntityEffectAnimationController) AnimationController;
			battleEntityEffectAnimationController.PlayRemoveAnimations( effectsToRemove );
			battleEntityEffectAnimationController.PlayAddAnimations( effectsToAdd );

			mCurrentEffects = newEffects;
		}
		#endregion


		#region Private Methods
		private void Refresh()
		{
			List< BattleEntityEffect > effects = new List< BattleEntityEffect >();
			effects.AddRange( mPlayableEffects );
			effects.AddRange( mBuffEffects );
			effects.AddRange( mStateEffects );

			//If the effects array contains destroyed the destroyed will be the only effect!
			if( effects.Contains( BattleEntityEffect.Destroyed ) )
			{
				Refresh( new BattleEntityEffect[]{ BattleEntityEffect.Destroyed } );
				return;
			}

			Refresh( effects.ToArray() );
		}


		private List< BattleEntityEffect > GetBuffEffects( BattleEntityController.Buff[] buffs )
		{
			List< BattleEntityEffect > effects = new List< BattleEntityEffect >();

			foreach( BattleEntityController.Buff buff in buffs )
			{
				switch( buff )
				{
					case BattleEntityController.Buff.TargetLock:
						effects.Add( BattleEntityEffect.TargetLock );
						break;

					case BattleEntityController.Buff.DamageBuff:
						effects.Add( BattleEntityEffect.DamageBuff );
						break;

					case BattleEntityController.Buff.HealthBuff:
						effects.Add( BattleEntityEffect.HealthBuff );
						break;
				}
			}

			return effects;
		}

		private List< BattleEntityEffect > GetPlayableEffects( string state )
		{
			List< BattleEntityEffect > effectData = new List< BattleEntityEffect >();

			//Entities are considered to be "Offline" whenever they're not "Online"
			switch( state )
			{
				case BattleEntityState.InHandPlayable:
					effectData.Add( BattleEntityEffect.Playable );
					break;
			}

			return effectData;
		}
		private List< BattleEntityEffect > GetStateEffects( string state )
		{
			List< BattleEntityEffect > effectData = new List<BattleEntityEffect>();

			//Entities are considered to be "Offline" whenever they're not "Online"
			switch( state )
			{
				case BattleEntityState.Destroyed:
					effectData.Add( BattleEntityEffect.Destroyed );
					break;

				case BattleEntityState.Online:
				case BattleEntityState.OnlineDoubleShot:
					effectData.Add( BattleEntityEffect.Online );
					break;

				case BattleEntityState.Installing:
				case BattleEntityState.Fired:
				case BattleEntityState.Offline:
					effectData.Add( BattleEntityEffect.Offline );
					break;
			}

			return effectData;
		}
		#endregion
	}
}