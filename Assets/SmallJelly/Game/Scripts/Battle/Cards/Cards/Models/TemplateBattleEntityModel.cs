﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class TemplateBattleEntityModel : BattleEntityModel 
	{
		#region Constants
		private const string INSTANTIATED_PREFAB_NAME = "View";
		#endregion

		#region Public Event Handlers
		public event EventHandler TemplateRefreshed;
		#endregion

		#region Properties
		public CardParts CardParts
		{
			get;
			private set;
		}

		public ComponentParts ComponentParts
		{
			get;
			private set;
		}
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			string resourcePath = string.Format( FileLocations.CardTemplateModel, GetTypeCode( battleEntityData ) );

			//If the prefab has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}

			GameObject loadedPrefab = LoadPrefab( resourcePath, transform );
			loadedPrefab.name = INSTANTIATED_PREFAB_NAME;

			if( loadedPrefab != null )
			{
				CardParts = loadedPrefab.GetComponent< CardParts >();
				ComponentParts = loadedPrefab.GetComponent< ComponentParts >();
			}

			FireTemplateRefreshed();
		}
		#endregion

		#region Event Firing
		private void FireTemplateRefreshed()
		{
			if( TemplateRefreshed != null )
			{
				TemplateRefreshed( this, EventArgs.Empty );
			}
		}
		#endregion
	}
}