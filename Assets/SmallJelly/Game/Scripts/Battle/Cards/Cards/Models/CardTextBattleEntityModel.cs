﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;

namespace SmallJelly
{
	public class CardTextBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			//TODO - Intergrate CardParts text into this

			//Dont show card text when the entity type is not known!
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				gameObject.SetActive( false );
			}
		}
		#endregion
	}
}