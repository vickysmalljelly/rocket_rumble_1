﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the component prefab depending upon the supplied component id
	/// </summary>
	public class ComponentBattleEntityModel : BattleEntityModel 
	{
		#region Constants
		public const string CARD_ALIGN_POSITION = "CardAlignPosition";
		public const string COMPONENT_ALIGN_POSITION = "ComponentAlignPosition";
		private const string BATTLE_ENTITY_PATH = "Prefabs/Components/";
		private const string BATTLE_ENTITY_PREFIX = "comp";
		private const string BATTLE_FALLBACK_ENTITY = "Fallback";
		private const string EMPTY_STRING = "";
		#endregion

		#region Public Properties
		public WeaponFiringSharedVariables WeaponFiringSharedVariables
		{
			get;
			private set;
		}
		#endregion

		#region Member Variables
		private bool mIsAlignedToCard;
		private bool mIsAlignedToBoard;


		private GameObject mInstantiatedPrefab;
		#endregion

		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			//If type is unknown then return!
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format("{0}{1}_{2}", BATTLE_ENTITY_PATH, BATTLE_ENTITY_PREFIX, battleEntityData.Id );

			bool modelWasRefreshed = false;

			//If the prefab is already loaded then return
			if( !IsLoaded( resourcePath ) )
			{
				mInstantiatedPrefab = LoadPrefab( resourcePath, transform );

				modelWasRefreshed = true;

				//Load the fallback where no mesh has been loaded!
				if( mInstantiatedPrefab == null )
				{
					string fallbackResourcePath = string.Format("{0}{1}_{2}_{3}", BATTLE_ENTITY_PATH, BATTLE_ENTITY_PREFIX, battleEntityData.EntityType.ToString().ToLower(), BATTLE_FALLBACK_ENTITY);

					if( !IsLoaded( fallbackResourcePath ) )
					{
						SJLogger.LogWarning( "No resource could be found at the path {0}, loading fallback!", resourcePath );

						mInstantiatedPrefab = LoadPrefab( fallbackResourcePath, transform );

						modelWasRefreshed = true;
					}
				}

				WeaponFiringSharedVariables = mInstantiatedPrefab.GetComponent< WeaponFiringSharedVariables >();

				//Where no weapon firing shared variables are available we use the fallback one!
				if( WeaponFiringSharedVariables == null )
				{
					//Add a weapon firing shared variables component - it will then use the default values for
					//firing a weapon
					WeaponFiringSharedVariables = gameObject.AddComponent< WeaponFiringSharedVariables >();
				}
			}

			AlignComponentToCard( mInstantiatedPrefab, battleEntityData, modelWasRefreshed );
		}

		public void AlignComponentToCard( GameObject loadedComponent, BattleEntityData battleEntityData, bool modelWasRefreshed )
		{
			bool alignedToCard = default( bool );
			bool alignedToBoard = default( bool );

			//If the card is in the players hand or is empty (i.e not in the battle) then align the component to the specified position
			switch( battleEntityData.State )
			{
			case BattleEntityState.Initialised:
			case BattleEntityState.InHandPlayable:
			case BattleEntityState.InHandNoTarget:
			case BattleEntityState.InHandNotEnoughPower:
			case BattleEntityState.InHandNotMyTurn:
			case BattleEntityState.Playing:
			case EMPTY_STRING:
			case null:
				alignedToCard = true;
				alignedToBoard = false;
				break;

			case BattleEntityState.Destroyed:
				alignedToCard = mIsAlignedToCard;
				alignedToBoard = mIsAlignedToBoard;
				break;

			default:
				alignedToCard = false;
				alignedToBoard = true;
				break;
			}

			//The alignment hasnt changed - leave it be
			if( alignedToCard == mIsAlignedToCard && alignedToBoard == mIsAlignedToBoard && !modelWasRefreshed )
			{
				return;
			}

			if( alignedToCard )
			{
				Transform cardAlignPosition = SJRenderTransformExtensions.FindDeepChild( GetComponentInParent< CardParts >().transform, CARD_ALIGN_POSITION );

				Transform componentAlignPosition = SJRenderTransformExtensions.FindDeepChild( loadedComponent.transform, COMPONENT_ALIGN_POSITION );
					
				Vector3 positionOffset = cardAlignPosition.transform.position - componentAlignPosition.transform.position;
				Vector3 rotationOffset = cardAlignPosition.transform.eulerAngles - componentAlignPosition.transform.eulerAngles;

				loadedComponent.transform.position += positionOffset;
				loadedComponent.transform.eulerAngles += rotationOffset;
			}

			if( alignedToBoard )
			{
				loadedComponent.transform.localPosition = Vector3.zero;
			}

			modelWasRefreshed = false;
			mIsAlignedToCard = alignedToCard;
			mIsAlignedToBoard = alignedToBoard;
		}

		public void OnInsertOwner( PlayerType playerType )
		{
			switch( playerType )
			{
			case PlayerType.Local:
				transform.localScale = new Vector3(1,1,1);
				break;

			case PlayerType.Remote:
				transform.localScale = new Vector3(-1,1,1);
				break;
			}
		}
		#endregion
	}
}