﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class MaskBattleEntityModel : BattleEntityModel 
	{
		#region Member Variables
		private MeshRenderer mMeshRenderer;
		#endregion

		#region Public Methods
		protected override void Awake()
		{
			base.Awake();

			mMeshRenderer = GetComponent< MeshRenderer >();
		}

		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			mMeshRenderer.enabled = battleEntityData.EntityType != BattleEntityType.Unknown;
		}
		#endregion
	}
}