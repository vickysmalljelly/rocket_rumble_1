﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the stat (health/damage) background model/material depending upon the class of the supplied battle entity data
	/// </summary>
	public class StatBackgroundBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format( FileLocations.CardStatBackgroundModel, GetClassCode( battleEntityData ) );
			string materialPath = string.Format( FileLocations.CardStatBackgroundMaterial );

			//If the mesh has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}

			LoadMesh( resourcePath, materialPath );
		}
		#endregion
	}
}