﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class SpritedBattleEntity : SJMonoBehaviour 
	{
		#region Member Variables
		private Texture2D mRenderTexture;
		private BattleEntityData mPreviousPaintedData;
		#endregion

		#region Public Methods
		public void Repaint( BattleEntityData battleEntityData )
		{

			Vector3 cardRotation;
			Vector3 cameraRotation;
			Vector3 cameraPosition;

			Shader shader;

			if( battleEntityData.Player == PlayerType.Local )
			{
				cameraRotation = Vector3.zero;
				cameraPosition = new Vector3( 0, 0, -5 );

				cardRotation = Vector3.zero;

				//We need to use a layered sprite because of highlights etc
				shader = Shader.Find( "SmallJelly/Sprite (Layered)" );
			}
			else
			{
				cameraRotation = new Vector3( 0, 180, 0 );
				cameraPosition = new Vector3( 0, 0, 5 );		

				cardRotation = new Vector3( 0, -180, 0 );

				//We can use an unlayered sprite to save on draw calls
				shader = Shader.Find( "SmallJelly/Sprite" );
			}

			CardParts cardParts = gameObject.GetComponentInParent< CardParts >();
			cardParts.Effects.SetActive( false );

			GameObject parent = cardParts.gameObject;
			mRenderTexture = SJRenderExtensions.RenderToTexture( parent, cameraPosition, cameraRotation );
			Material material = gameObject.GetComponent< MeshRenderer>().material;
			material.shader = shader;
			material.mainTexture = mRenderTexture;

			cardParts.Effects.SetActive( true );

			transform.localEulerAngles = cardRotation;
		}

		public bool ShouldRepaint( BattleEntityData battleEntityData )
		{
			BattleEntityData previousPaintedData = mPreviousPaintedData;
			mPreviousPaintedData = battleEntityData;

			if( previousPaintedData == null )
			{
				return true;
			}

			if( battleEntityData.Cost != previousPaintedData.Cost )
			{
				return true;
			}

			if( battleEntityData.State != previousPaintedData.State )
			{
				return true;
			}
				
			return false;
		}
		#endregion
	}
}