﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the back of the card - this is only a stub at the moment - we dont supply card back data!
	/// </summary>
	public class BackBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			string resourcePath = string.Format( FileLocations.CardBackModel );
			string materialPath = string.Format( FileLocations.CardBackMaterial );
				
			//If the mesh has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}

			LoadMesh( resourcePath, materialPath );
		}
		#endregion
	}
}