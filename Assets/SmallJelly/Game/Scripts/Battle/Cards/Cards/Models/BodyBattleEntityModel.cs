﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the body model/material depending upon the class/rarity of the supplied battle entity data
	/// </summary>
	public class BodyBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format( FileLocations.CardBodyModel, GetClassCode( battleEntityData ) );
			string materialPath = string.Format( FileLocations.CardBodyMaterial, GetClassCode( battleEntityData ), GetRarityCode( battleEntityData ) );
				
			//If the mesh has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}
				
			LoadMesh( resourcePath, materialPath );
		}
		#endregion
	}
}