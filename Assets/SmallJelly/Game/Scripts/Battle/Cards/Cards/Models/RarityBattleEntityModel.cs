﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the rarity indicator (bottom of the card) model/material depending upon the class/rarity of the supplied battle entity data
	/// </summary>
	public class RarityBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format( FileLocations.CardRarityModel, GetClassCode( battleEntityData ) );
			string materialPath = string.Format( FileLocations.CardRarityMaterial, GetClassCode( battleEntityData ), GetRarityCode( battleEntityData ));

			//If the mesh has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}

			LoadMesh( resourcePath, materialPath );
		}
		#endregion
	}
}