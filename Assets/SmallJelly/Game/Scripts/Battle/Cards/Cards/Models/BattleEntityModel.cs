﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Abstract class that allows the refreshing of battle entity models & materials - either from a prefab or a mesh. For example - 
	/// we might want to refresh the background mesh/material of a card to whatever the type of the card is (weapon, unknown etc) - this 
	/// allows us to do this at runtime as many times as we want. Each "BattleEntityModel" object handles the refreshing of one model.
	/// </summary>
	public abstract class BattleEntityModel : SJMonoBehaviour 
	{
		#region Member Variables
		private string mLoadedModelPath;
		private GameObject mLoadedPrefab;
		#endregion

		#region Public Methods
		public abstract void OnRefresh( BattleEntityData battleEntityData );

		protected bool IsLoaded( string modelPath )
		{
			return mLoadedModelPath == modelPath;
		}

		protected GameObject LoadPrefab( string modelPath, Transform parent )
		{
			SJLogger.AssertCondition( modelPath != mLoadedModelPath, "This prefab is already loaded" );

			GameObject gameObject = Resources.Load< GameObject >( modelPath );

			//If prefab could not be found then keep existing prefab!
			if( gameObject == null )
			{
				return mLoadedPrefab;
			}

			if( mLoadedPrefab != null )
			{
				DestroyImmediate( mLoadedPrefab );
			}

			GameObject prefab = CreateModel( gameObject, parent );

			mLoadedModelPath = modelPath;
			mLoadedPrefab = prefab;

			return prefab;
		}

		protected void LoadMesh( string modelPath, string materialPath )
		{
			SJLogger.AssertCondition( modelPath != mLoadedModelPath, "This mesh is already loaded" );

			mLoadedModelPath = modelPath;

			Mesh mesh = Resources.Load< Mesh >( modelPath );
			SJLogger.AssertCondition( mesh != null, "Mesh could not be found at {0}", modelPath );

			Material material = Resources.Load< Material >( materialPath );
			SJLogger.AssertCondition( material != null, "Material could not be found at {0}", materialPath );

			CreateModel( mesh, material );
		}

		protected void ForceLayeringRefresh()
		{
			SJRenderObject[] sjRenderObjects = gameObject.GetComponentsInChildren< SJRenderObject >();
			foreach( SJRenderObject sjRenderObject in sjRenderObjects )
			{
				DestroyImmediate( sjRenderObject );
			}
		}

		protected string GetTypeCode( BattleEntityData battleEntityData )
		{
			switch( battleEntityData.EntityType )
			{
				case BattleEntityType.Weapon:
					return FileLocations.WeaponFileCode;

				case BattleEntityType.Utility:
					return FileLocations.UtilityFileCode;

				default:
					return FileLocations.NanotechFileCode;
			}
		}

		protected string GetClassCode( BattleEntityData battleEntityData )
		{
			switch( battleEntityData.Class )
			{
				case ShipClass.Ancient:
					return FileLocations.AncientClassFileCode;

				case ShipClass.Enforcer:
					return FileLocations.EnforcerClassFileCode;

				case ShipClass.Smuggler:
					return FileLocations.SmugglerClassFileCode;

				default:
					return FileLocations.NeutralClassFileCode;
			}
		}

		protected string GetRarityCode( BattleEntityData battleEntityData )
		{
			switch( battleEntityData.Rarity )
			{
				case BattleEntityRarity.Common:
					return FileLocations.CommonRarityFileCode;

				case BattleEntityRarity.Rare:
					return FileLocations.RareRarityFileCode;

				case BattleEntityRarity.Epic:
					return FileLocations.EpicRarityFileCode;

				case BattleEntityRarity.Legendary:
					return FileLocations.LegendaryRarityFileCode;

				default:
					return FileLocations.CommonRarityFileCode;
			}
		}
		#endregion

		#region Private Methods
		private void CreateModel( Mesh mesh, Material material )
		{
			//We disable the gameobject as we create the model to avoid it being impacted by the render system
			gameObject.SetActive( false );

			MeshFilter meshFilter = gameObject.GetComponent< MeshFilter >();
			if( meshFilter == null )
			{
				meshFilter = gameObject.AddComponent< MeshFilter >();
			}

			meshFilter.mesh = mesh;

			MeshRenderer meshRenderer = gameObject.GetComponent< MeshRenderer >();
			if( meshRenderer == null )
			{
				meshRenderer = gameObject.AddComponent< MeshRenderer >();
			}

			ForceLayeringRefresh();

			meshRenderer.sharedMaterial = material;

			//We re-enable the gameobject now the model's been loaded. It will now be picked up by the render system!
			gameObject.SetActive( true );
		}

		private GameObject CreateModel( GameObject resource, Transform parent )
		{
			GameObject componentModel = Instantiate( resource ); 
			SJRenderTransformExtensions.AddChild( parent, componentModel.transform, true, true, true );
			return componentModel;
		}
		#endregion
	}
}