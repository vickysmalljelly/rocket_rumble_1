﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the interior (where the component sits in the card) model/material depending upon the class of the supplied battle entity data
	/// </summary>
	public class InteriorBattleEntityModel : BattleEntityModel 
	{
		#region Member Variables
		private string mLoadedInteriorColour;
		#endregion

		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format( FileLocations.CardInteriorModel, GetClassCode( battleEntityData ) );
			string materialPath = string.Format( FileLocations.CardInteriorMaterial, GetClassCode( battleEntityData ) );

			//If the mesh has already been loaded then we dont need to reload it
			if( !IsLoaded( resourcePath ) )
			{
				LoadMesh( resourcePath, materialPath );
				mLoadedInteriorColour = default( string );
			}

			if( battleEntityData.InteriorColour != mLoadedInteriorColour )
			{
				MeshRenderer meshRenderer = GetComponent< MeshRenderer >();
				meshRenderer.sharedMaterial = new Material( meshRenderer.sharedMaterial );

				Color color;
				ColorUtility.TryParseHtmlString( battleEntityData.InteriorColour, out color );

				SJLogger.AssertCondition( color != default( Color ), "No color was found with the specified hex {0}", battleEntityData.InteriorColour );

				meshRenderer.sharedMaterial.color = color;

				mLoadedInteriorColour = battleEntityData.InteriorColour;

				ForceLayeringRefresh();
			}
		}
		#endregion
	}
}