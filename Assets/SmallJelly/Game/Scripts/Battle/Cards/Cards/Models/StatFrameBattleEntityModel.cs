﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Refreshes the stat (health/damage) frame model/material depending upon the class/rarity of the supplied battle entity data
	/// </summary>
	public class StatFrameBattleEntityModel : BattleEntityModel 
	{
		#region Public Methods
		public override void OnRefresh( BattleEntityData battleEntityData )
		{
			if( battleEntityData.EntityType == BattleEntityType.Unknown )
			{
				return;
			}

			string resourcePath = string.Format( FileLocations.CardStatFrameModel, GetClassCode( battleEntityData ) );
			string materialPath = string.Format( FileLocations.CardStatFrameMaterial, GetClassCode( battleEntityData ), GetRarityCode( battleEntityData ) );

			//If the mesh has already been loaded then return
			if( IsLoaded( resourcePath ) )
			{
				return;
			}

			LoadMesh( resourcePath, materialPath );
		}
		#endregion
	}
}