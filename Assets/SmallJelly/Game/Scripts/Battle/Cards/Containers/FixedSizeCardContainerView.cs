﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class FixedSizeCardContainerView : BattleEntityContainerView
	{
		#region Properties
		public override int Count 
		{ 
			get 
			{
				return BattleEntities.Length;
			}
		}

		public BattleEntity[] BattleEntities { get; set; }
		#endregion

		#region Methods
		public override void OnSwitchTurn( PlayerType playerType )
		{
			for( int count = 0; count < BattleEntities.Length; count++ )
			{
				if( BattleEntities[ count ] != null )
				{
					BattleEntities[ count ].OnStartTurn( playerType );
				}
			}
		}

		public override void Refresh( List<BattleEntityData> battleEntityData )
		{
			SJLogger.AssertCondition( battleEntityData.Count == BattleEntities.Length, "The number of created cards and the amount of received data must be equal : BattleEntityData {0} : OrderedBattleEntities {1}", battleEntityData.Count, BattleEntities.Length );

			for( int count = 0; count < BattleEntities.Length; count++ )
			{
				if( BattleEntities[ count ] )
				{
					BattleEntityData entityData = battleEntityData[ count ];

					BattleEntities[ count ].OnRefreshLogicState( entityData.State );
					BattleEntities[ count ].OnRefreshModelWithData( entityData );
					BattleEntities[ count ].OnRefreshEffectWithData( entityData );
					BattleEntities[ count ].OnRefreshSpriteWithData( entityData );
				}
			}
		}

		public override void Insert( int[] indexes, BattleEntity[] battleEntities )
		{
			for( int i = 0; i < indexes.Length; i++ )
			{
				int index = indexes[ i ];
				BattleEntity battleEntity = battleEntities [ i ];

				RegisterEntityListeners( battleEntity );

				BattleEntities[index] = battleEntity;

				SJRenderTransformExtensions.AddChild( transform, battleEntity.transform, false, false, false );
			}
		}

		public override int IndexOf( BattleEntity battleEntity )
		{
			return Array.IndexOf( BattleEntities, battleEntity );
		}

		public override bool Remove( BattleEntity battleEntity )
		{
			UnregisterEntityListeners( battleEntity );

			SJRenderTransformExtensions.AddChild( transform.parent, battleEntity.transform, false, false, false );

			for( int i = 0; i < BattleEntities.Length; i++ )
			{
				if( BattleEntities[i] == battleEntity )
				{
					BattleEntities[i] = null;
					return true;
				}
			}
			return false;
		}

		public override BattleEntity Get( int drawNumber )
		{
			foreach( BattleEntity card in BattleEntities )
			{
				if( card && card.Data.DrawNumber == drawNumber )
				{
					return card;
				}
			}

			return null;
		}

		public override List<BattleEntity> RemoveExcluding( List<BattleEntityData > cardData )
		{
			List<BattleEntity> cards = new List<BattleEntity>();
			for( int count = 0; count < BattleEntities.Length; count++ )
			{
				BattleEntity battleEntity = BattleEntities[ count ];

				//If no battle entity occupies this slot then continue
				if( !battleEntity )
				{
					continue;
				}

				bool contains = false;
				for( int innerCount = 0; innerCount < cardData.Count; innerCount++ )
				{
					//If the supplied card data slot is not empty and the dat inside matches the battle entity data
					if(cardData[innerCount] != null && cardData[innerCount].DrawNumber == battleEntity.Data.DrawNumber)
					{
						contains = true;
						break;
					}
				}
				if( !contains )
				{
					//Remove card from this list
					Remove( battleEntity );
					count--;

					//Add the card to the returned list
					cards.Add(battleEntity);
				}
			}

			return cards;
		}
		#endregion
	}
}
