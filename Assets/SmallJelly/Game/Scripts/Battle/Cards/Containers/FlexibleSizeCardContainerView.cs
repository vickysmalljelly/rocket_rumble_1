﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class FlexibleSizeCardContainerView : BattleEntityContainerView
	{		
		#region Properties
		public override int Count 
		{ 
			get 
			{
				return mOrderedBattleEntities.Count;
			}
		}

		public List< BattleEntity > OrderedBattleEntities 
		{ 
			get
			{
				return mOrderedBattleEntities;
			}
		}
		#endregion

		#region Member Variables
		private List< BattleEntity > mOrderedBattleEntities;
		#endregion


		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();
			mOrderedBattleEntities = new List< BattleEntity >();
		}
		#endregion

		#region Methods
		public override void OnSwitchTurn( PlayerType playerType )
		{
			for( int count = 0; count < mOrderedBattleEntities.Count; count++ )
			{
				mOrderedBattleEntities[ count ].OnStartTurn( playerType );
			}
		}

		public override void Refresh( List<BattleEntityData> battleEntityData )
		{
			SJLogger.AssertCondition( battleEntityData.Count == mOrderedBattleEntities.Count, "The number of created cards and the amount of received data must be equal : BattleEntityData {0} : OrderedBattleEntities {1}", battleEntityData.Count, mOrderedBattleEntities.Count );

			for( int count = 0; count < mOrderedBattleEntities.Count; count++ )
			{
				BattleEntityData entityData = battleEntityData[ count ];

				mOrderedBattleEntities[ count ].OnRefreshLogicState( entityData.State );
				mOrderedBattleEntities[ count ].OnRefreshModelWithData( entityData );
				mOrderedBattleEntities[ count ].OnRefreshEffectWithData( entityData );
				mOrderedBattleEntities[ count ].OnRefreshSpriteWithData( entityData );
			}
		}

		public override void Insert( int[] indexes, BattleEntity[] battleEntities )
		{
			for( int i = 0; i < indexes.Length; i++ )
			{
				int index = indexes[ i ];
				BattleEntity battleEntity = battleEntities[ i ];
				RegisterEntityListeners( battleEntity );

				mOrderedBattleEntities.Insert( index, battleEntity );

				SJRenderTransformExtensions.AddChild( transform, battleEntity.transform, false, false, false );
			}
		}

		public override int IndexOf( BattleEntity battleEntity )
		{
			return OrderedBattleEntities.IndexOf( battleEntity );
		}

		public override bool Remove( BattleEntity battleEntity )
		{
			UnregisterEntityListeners( battleEntity );

			SJRenderTransformExtensions.AddChild( transform.parent, battleEntity.transform, false, false, false );

			return mOrderedBattleEntities.Remove( battleEntity );
		}

		public override BattleEntity Get( int drawNumber )
		{
			foreach( BattleEntity card in mOrderedBattleEntities )
			{
				if( card.Data.DrawNumber == drawNumber )
				{
					return card;
				}
			}

			return null;
		}

		public override List<BattleEntity> RemoveExcluding( List<BattleEntityData > cardData )
		{
			List<BattleEntity> cards = new List<BattleEntity>();
			for( int count = 0; count < mOrderedBattleEntities.Count; count++ )
			{
				BattleEntity card = mOrderedBattleEntities[ count ];
				bool contains = false;
				for( int innerCount = 0; innerCount < cardData.Count; innerCount++ )
				{
					if(cardData[innerCount].DrawNumber == card.Data.DrawNumber)
					{
						contains = true;
						break;
					}
				}
				if( !contains )
				{
					//Remove card from this list
					Remove( card );
					count--;

					//Add the card to the returned list
					cards.Add(card);
				}
			}

			return cards;
		}
		#endregion
	}
}
