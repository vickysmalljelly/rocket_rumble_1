﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;
using System;
using System.Linq;

namespace SmallJelly
{
	public abstract class BattleEntityContainerView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler<BattleEntityAndStateTransitionEventArgs> BattleEntityStateUpdated;
		public event EventHandler<BattleEntityAndTargetableStateTransitionEventArgs> BattleEntityTargetableStateUpdated;
		public event EventHandler<BattleEntityAndSelectionStateTransitionEventArgs> BattleEntitySelectionStateUpdated;
		#endregion

		#region Properties
		public abstract int Count { get; }
		#endregion

		#region Methods
		public abstract void OnSwitchTurn( PlayerType playerType );
		public abstract void Refresh(List<BattleEntityData> battleEntityData );
		public abstract void Insert( int[] indexes, BattleEntity[] battleEntities );
		public abstract int IndexOf( BattleEntity battleEntity );
		public abstract bool Remove( BattleEntity battleEntity );

		public abstract BattleEntity Get( int drawNumber );
		public abstract List<BattleEntity> RemoveExcluding( List<BattleEntityData > cardData );

		public virtual void Rollback(){}
		#endregion

		#region Protected Methods
		protected virtual void RegisterEntityListeners( BattleEntity battleEntity )
		{
			battleEntity.StateTransitioned += HandleBattleEntityStateUpdated;
			battleEntity.TargetableStateTransitioned += HandleTargetableStateTransitioned;
			battleEntity.SelectionStateTransitioned += HandleBattleEntitySelectionStateUpdated;
			battleEntity.HighlightedStateTransitioned += HandleHighlightedStateTransitioned;
		}

		protected virtual void UnregisterEntityListeners( BattleEntity battleEntity )
		{
			battleEntity.StateTransitioned -= HandleBattleEntityStateUpdated;
			battleEntity.TargetableStateTransitioned -= HandleTargetableStateTransitioned;
			battleEntity.SelectionStateTransitioned -= HandleBattleEntitySelectionStateUpdated;
			battleEntity.HighlightedStateTransitioned -= HandleHighlightedStateTransitioned;
		}
		#endregion

		#region Private Methods
		private void BattleEntityTargetableStateTransitioned( BattleEntity battleEntity, TargetableLogicController.TargetableState previousState, TargetableLogicController.TargetableState newState, SJRenderRaycastHit[] renderRaycastHit )
		{
			//TODO - I'd like to simplify this stuff into a generic effects class with parameters on the situations where the effects happen
			//Now is not the time to do it!
			if( newState != TargetableLogicController.TargetableState.Targeted )
			{
				return;
			}

			if( renderRaycastHit.Length == 0 )
			{
				return;
			}

			//If the battle entity is still a card
			if( battleEntity.BattleEntityController is CardController )
			{
				switch( battleEntity.Data.EntityType )
				{
					//If the card is a weapon or a utility then it must be targeting an installation!
					case BattleEntityType.Weapon:
					case BattleEntityType.Utility:					
						return;

					//If the card is a non targeting crew card then we don't want anything relating to targeting to be processed
					case BattleEntityType.NonTargetingCrew:
						return;
				}
			}

			//Select all layers with targetable components where the targetable component isnt the source entity
			Targetable[] targetables = renderRaycastHit.Select( o => o.GroupGameObject.GetComponent< Targetable >() ).Where( o => ( o != null ) && ( o != battleEntity )  ).ToArray();

			if( targetables.Length > 0 )
			{
				Targetable elementOfInterest = targetables[ 0 ];

				//We are not interested in cards!
				if( elementOfInterest is BattleEntity )
				{
					BattleEntity battleEntityOfInterest = (BattleEntity)elementOfInterest;
					if( battleEntityOfInterest.BattleEntityController is CardController )
					{
						return;
					}
				}

				BattleEffectsFactory.EffectType effectType = elementOfInterest.TargetableState == TargetableLogicController.TargetableState.Targetable ? BattleEffectsFactory.EffectType.ValidTargetSelected :  BattleEffectsFactory.EffectType.InvalidTargetSelected;

				//TODO - Optimise the below line
				Vector3 hitPoint = renderRaycastHit.Where( o => o.GroupGameObject == elementOfInterest.gameObject ).First().Point;

				GameObject effect = BattleEffectsFactory.ConstructEffect( effectType );

				SJRenderTransformExtensions.AddChild( elementOfInterest.transform, effect.transform, false, true, false );
				effect.transform.position = hitPoint;
			}

		}
		#endregion

		#region Protected Event Handlers
		protected virtual void HandleBattleEntityStateUpdated( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			FireBattleEntityStateUpdated( battleEntityAndStateTransitionEventArgs.BattleEntity, battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState, battleEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		protected virtual void HandleTargetableStateTransitioned( object o, BattleEntityAndTargetableStateTransitionEventArgs battleEntityAndTargetableStateTransitionEventArgs )
		{
			BattleEntityTargetableStateTransitioned( battleEntityAndTargetableStateTransitionEventArgs.BattleEntity, battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState, battleEntityAndTargetableStateTransitionEventArgs.EntityHitLayers );

			FireBattleEntityTargetableStateUpdated( battleEntityAndTargetableStateTransitionEventArgs.BattleEntity, battleEntityAndTargetableStateTransitionEventArgs.State, battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState, battleEntityAndTargetableStateTransitionEventArgs.EntityHitLayers );
		}

		protected virtual void HandleBattleEntitySelectionStateUpdated( object o, BattleEntityAndSelectionStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			FireBattleEntitySelectionStateUpdated( battleEntityAndStateTransitionEventArgs.BattleEntity, battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState, battleEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		protected virtual void HandleHighlightedStateTransitioned( object o, BattleEntityAndHighlightedStateTransitionEventArgs battleEntityAndHightlightedStateTransitionEventArgs )
		{
		}
		#endregion

		#region Event Firing
		private void FireBattleEntityStateUpdated( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] entityHitLayers )
		{
			if( BattleEntityStateUpdated != null )
			{
				BattleEntityStateUpdated( this, new BattleEntityAndStateTransitionEventArgs( battleEntity, previousState, newState, entityHitLayers ) );
			}
		}

		private void FireBattleEntityTargetableStateUpdated( BattleEntity battleEntity, BattleEntityController.State state, BattleEntityController.TargetableState previousTargetableState, BattleEntityController.TargetableState newTargetableState, SJRenderRaycastHit[] entityHitLayers )
		{
			if( BattleEntityTargetableStateUpdated != null )
			{
				BattleEntityTargetableStateUpdated( this, new BattleEntityAndTargetableStateTransitionEventArgs( battleEntity, state, previousTargetableState, newTargetableState, entityHitLayers ) );
			}
		}

		private void FireBattleEntitySelectionStateUpdated( BattleEntity battleEntity, BattleEntityController.SelectionState previousState, BattleEntityController.SelectionState newState, SJRenderRaycastHit[] entityHitLayers )
		{
			if( BattleEntitySelectionStateUpdated != null )
			{
				BattleEntitySelectionStateUpdated( this, new BattleEntityAndSelectionStateTransitionEventArgs( battleEntity, previousState, newState, entityHitLayers ) );
			}
		}
		#endregion
	}
}