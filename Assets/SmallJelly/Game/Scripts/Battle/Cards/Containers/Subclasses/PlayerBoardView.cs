using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class PlayerBoardView : FixedSizeCardContainerView
	{
		#region Constants
		private const string SOCKET_KEY = "Socket";
		#endregion

        #region Public Properties
        public Ship Ship
		{
			get
			{
				return mShip;
			}
		}
		#endregion

		#region Private Properties
		private Socket[] SocketEntities
		{
			get
			{
				return mShip.BattleEntitySockets;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private Transform mBattleEntityBoardPreviewContainer;
		#endregion

		#region Member Variables
		private Ship mShip;
		private BattleEntityBoardPreview mBattleEntityBoardPreview;
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			BattleEntities = new BattleEntity[9];
		}

		private void OnEnable()
		{
			//Enable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = true;
		}

		private void OnDisable()
		{
			//Disable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = false;
		}
		#endregion

		#region Public Methods
		//Called when the object is created - just before OnEnable - used to construct the object with the
		//supplied configuration
		public void OnConstruct( BattleConfig battleConfig, PlayerType playerType )
		{
            string playerId = playerType == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId;

			BattleConfig.PlayerBattleConfig playerBattleConfig;
			if( playerId == battleConfig.Players[0].PlayerId  )
			{
				playerBattleConfig = battleConfig.Players[0];
			}
			else if( playerId == battleConfig.Players[1].PlayerId  )
			{
				playerBattleConfig = battleConfig.Players[1];
			}
			else
			{
				SJLogger.Assert("No config found for the player {0}", playerId);
				playerBattleConfig = default( BattleConfig.PlayerBattleConfig );
			}

			ConstructShip( playerBattleConfig.ShipClass, playerType );

            if(playerType == PlayerType.Local)
            {
                // Record the choice of ship
                AnalyticsManager.Get.RecordBattleEvent("class", playerBattleConfig.ShipClass);
            }
		}

		public override void Refresh( List<BattleEntityData> battleEntityData )
		{
			for( int count = 0; count < BattleEntities.Length; count++ )
			{
				BattleEntityData entityData = battleEntityData[ count ];

				//If the entry is null then skip (the battle entity slot might not be occupied)
				if( entityData == null )
				{
					continue;
				}

				BattleEntities[ count ].OnRefreshLogicState( entityData.State );
				BattleEntities[ count ].OnRefreshModelWithData( entityData );
				BattleEntities[ count ].OnRefreshEffectWithData( entityData );
				BattleEntities[ count ].OnRefreshSpriteWithData( entityData );
			}
		}

		public override void Rollback()
		{
			base.Rollback();

			foreach( BattleEntity battleEntity in BattleEntities )
			{
				if( battleEntity != null )
				{
					battleEntity.OnRollback();
				}
			}
		}

		public void Refresh( SocketData[] socketData )
		{
			for( int count = 0; count < SocketEntities.Length; count++ )
			{
				SocketEntities[ count ].OnRefreshData( socketData[ count ] );
			}
		}

		public void Refresh( ShipBattleData shipData )
		{
			//Refresh the ship
			mShip.Refresh( shipData );
		}

		public override void Insert( int[] indexes, BattleEntity[] battleEntities )
		{
			base.Insert( indexes, battleEntities );

			for( int i = 0; i < indexes.Length; i++ )
			{
				int index = indexes[ i ];
				BattleEntity battleEntity = battleEntities [ i ];

				SocketEntities[index].OnInsertComponent( battleEntity );


				Dictionary< string, NamedVariable > variables = new Dictionary< string, NamedVariable >();
				variables.Add( SOCKET_KEY, new FsmGameObject( SocketEntities[ index ].gameObject ) );

				//Let the component know its about to be added to the board
				battleEntity.OnInsertState( BattleEntityController.State.ComponentAddingToBoard, new AnimationMetaData( variables ) );
			}
		}

		public override bool Remove( BattleEntity battleEntity )
		{
			//Let the component know its about to be removed from the board
			battleEntity.OnInsertState( BattleEntityController.State.ComponentRemovingFromBoard );

			int index = Array.IndexOf( BattleEntities, battleEntity );

			bool removed = base.Remove( battleEntity );

			SocketEntities[index].OnRemoveComponent( battleEntity );
			return removed;
		}

		public int IndexOf( Socket socket )
		{
			return Array.IndexOf( SocketEntities, socket );
		}
		#endregion

		#region Private Methods
		private void ConstructShip( string shipClass, PlayerType playerType )
		{
			mShip = ShipFactory.ConstructShip( playerType, shipClass );				
			SJRenderTransformExtensions.AddChild( transform, mShip.transform, true, true, false );				
			mShip.OnInsertState( ShipController.ShipState.ShipIdle );
		}

		private void TestBattleEntityHighlightedStateTransitioned( BattleEntity battleEntity, ComponentController.HighlightedState highlightedState )
		{
			//TODO - Once we have the card creation code in - offering proper card creation of different types - this should
			//be taken out

			//Handle rolloff (whenever a state is changed - no matter if it's an "unhighlight" or another component has been highlighted)
			if( mBattleEntityBoardPreview != null )
			{
				mBattleEntityBoardPreview.OnInsertState( BattleEntityBoardPreviewController.State.Hiding );
			}

			//Handle rollover - we may only display "card previews" where an entity is highlighted and is not involved in
			//a targeting situation
			if( highlightedState == ComponentController.HighlightedState.Highlighted && battleEntity.TargetableState == TargetableLogicController.TargetableState.None )
			{
				BattleEntityBoardPreview battleEntityBoardPreview = BattleEntityBoardPreviewFactory.ConstructBattleEntity ( battleEntity.Data );
				battleEntityBoardPreview.transform.position = battleEntity.transform.position;

				//Add the preview to the board
				SJRenderTransformExtensions.AddChild( transform, battleEntityBoardPreview.transform, false, true, false );

				//Add the preview to the HUD - we do this immediately after adding it to the board to transform it between the
				//board and HUD cameras whilst being in the correct position!
				SJRenderTransformExtensions.AddChild( mBattleEntityBoardPreviewContainer, battleEntityBoardPreview.transform, false, true, true );

				AddBattleEntityPreview( battleEntityBoardPreview );
			}
		}

		private void AddBattleEntityPreview( BattleEntityBoardPreview battleEntityBoardPreview ) 
		{
			mBattleEntityBoardPreview = battleEntityBoardPreview;
			RegisterEntityBoardPreviewListeners( mBattleEntityBoardPreview );

			mBattleEntityBoardPreview.OnInsertState(BattleEntityBoardPreviewController.State.Showing);
		}

		private void RemoveBattleEntityPreview( BattleEntityBoardPreview battleEntityBoardPreview )
		{
			UnregisterEntityBoardPreviewListeners( battleEntityBoardPreview );
			Destroy( battleEntityBoardPreview.gameObject );
		}



		#endregion

		#region Event Handlers
		protected override void HandleHighlightedStateTransitioned( object o, BattleEntityAndHighlightedStateTransitionEventArgs battleEntityAndHighlightedStateTransitionEventArgs )
		{
			base.HandleHighlightedStateTransitioned( o, battleEntityAndHighlightedStateTransitionEventArgs );

			TestBattleEntityHighlightedStateTransitioned( battleEntityAndHighlightedStateTransitionEventArgs.BattleEntity, battleEntityAndHighlightedStateTransitionEventArgs.NewState );
		}

		protected virtual void HandleBattleEntityPreviewStateTransitioned( object o, BattleEntityBoardPreviewAndStateTransitionEventArgs battleEntityBoardPreviewAndStateTransitionEventArgs )
		{
			switch( battleEntityBoardPreviewAndStateTransitionEventArgs.NewState )
			{
				case BattleEntityBoardPreviewController.State.Hidden:
					RemoveBattleEntityPreview( battleEntityBoardPreviewAndStateTransitionEventArgs.BattleEntityBoardPreview );
				break;
			}
		}
		#endregion

		#region Event Registration
		protected override void RegisterEntityListeners( BattleEntity battleEntity )
		{
			base.RegisterEntityListeners( battleEntity );
		}

		protected override void UnregisterEntityListeners( BattleEntity battleEntity )
		{
			base.UnregisterEntityListeners( battleEntity );
		}

		private void RegisterEntityBoardPreviewListeners( BattleEntityBoardPreview battleEntityBoardPreview )
		{
			battleEntityBoardPreview.StateTransitioned += HandleBattleEntityPreviewStateTransitioned;
		}

		private void UnregisterEntityBoardPreviewListeners( BattleEntityBoardPreview battleEntityBoardPreview )
		{
			battleEntityBoardPreview.StateTransitioned -= HandleBattleEntityPreviewStateTransitioned;
		}
		#endregion
	}
}
