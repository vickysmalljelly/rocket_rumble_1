using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class LocalPlayerHandView : PlayerHandView
	{
		#region Protected Methods
		protected override CircleDistributionShape GetDistributionShape( Vector3 orientationPosition, float circleRadius, float startDegree, float endDegree, float maximumDegreesBetweenPoints, Vector3 pivotPoint )
		{
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );

			return new FaceGameObjectCircleDistributionShape( orientationPosition, camera.gameObject.transform.position,  circleRadius, startDegree, endDegree, maximumDegreesBetweenPoints, pivotPoint );
		}
		#endregion
	}
}