using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;

namespace SmallJelly
{
	public abstract class PlayerHandView : FlexibleSizeCardContainerView
	{
		#region Exposed To The Inspector
		[SerializeField]
		private float mCircleRadius;

		[SerializeField]
		private float mStartDegree;

		[SerializeField]
		private float mEndDegree;

		[SerializeField]
		private float mMaximumDegreesBetweenPoints;

		[SerializeField]
		private float mReadjustTime;

		[SerializeField]
		private Vector3 mPivotPoint;


		[SerializeField]
		private Transform mCardDrawn;

		[SerializeField]
		private Transform mCardHand;

		[SerializeField]
		private Transform mCardOther;

		[SerializeField]
		private Vector3 mLocationOffset;
		#endregion

		#region Member Variables
		private ObjectDistributor mObjectDistributor;
		#endregion

		#region Unity Methods
		private void Start()
		{
			Vector3 orientationPosition = transform.position + mLocationOffset;
			CircleDistributionShape circleDistributionShape = GetDistributionShape( orientationPosition, mCircleRadius, mStartDegree, mEndDegree, mMaximumDegreesBetweenPoints, mPivotPoint );
			mObjectDistributor = new ObjectDistributor( circleDistributionShape, mReadjustTime );

			RegisterListeners();
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}

		private void OnEnable()
		{
			//Enable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = true;
		}

		private void Update()
		{
		}
		private void OnDisable()
		{
			//Disable camera for this gameObject
			Camera camera = SJRenderTransformExtensions.GetCamera( gameObject.layer );
			camera.enabled = false;
		}
		#endregion

		#region Methods
		public override void Insert( int[] indexes, BattleEntity[] battleEntities )
		{
			base.Insert( indexes, battleEntities );

			AddCardsToHand( battleEntities );
		}
		
		public override bool Remove( BattleEntity battleEntity )
		{
			//Let the card know it's about to be removed from the hand
			battleEntity.OnInsertState( BattleEntityController.State.CardRemovingFromHand );

			bool removed = base.Remove( battleEntity );

			RemoveCardFromHand( battleEntity );

			return removed;
		}

		public override void Rollback()
		{
			base.Rollback();

			foreach( BattleEntity battleEntity in OrderedBattleEntities )
			{
				battleEntity.OnRollback();
			}
		}
		#endregion

		#region Protected Methods
		protected abstract CircleDistributionShape GetDistributionShape( Vector3 orientationPosition, float circleRadius, float startDegree, float endDegree, float maximumDegreesBetweenPoints, Vector3 pivotPoint );
		#endregion

		#region Private Methods
		private void AddCardsToHand( BattleEntity[] battleEntities )
		{
			List< int > indexList = new List< int >();
			List< BattleEntity > battleEntityList = new List< BattleEntity >();

			foreach( BattleEntity battleEntity in battleEntities )
			{
				//If the distributor already contains the card then we do not need to re-add it!
				if( mObjectDistributor.Contains( battleEntity.transform ) )
				{
					continue;
				}

				indexList.Add( OrderedBattleEntities.IndexOf(battleEntity) );
				battleEntityList.Add( battleEntity );


				//Make entity a child of this
				SJRenderTransformExtensions.AddChild( mCardHand, battleEntity.transform, false, true, false );

				battleEntity.OnInsertState( BattleEntityController.State.CardAddingToHand );
			}

			//Redistribute the children
			mObjectDistributor.Insert( indexList.ToArray(), battleEntityList.Select( x => x.transform ).ToArray() );
		}

		private void RemoveCardFromHand( BattleEntity battleEntity )
		{
			mObjectDistributor.Remove( battleEntity.transform );

			//Remove entity from this subheirarchy
			battleEntity.transform.parent = mCardOther;
		}

		private void FinishedAddingCardToHand( BattleEntity battleEntity )
		{
			battleEntity.OnInsertState( BattleEntityController.State.CardIdle );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mObjectDistributor.Added += HandleBattleEntityAdded;
		}

		private void UnregisterListeners()
		{
			mObjectDistributor.Added -= HandleBattleEntityAdded;
		}
		#endregion

		#region Protected Event Handlers
		protected override void HandleBattleEntityStateUpdated( object o, BattleEntityAndStateTransitionEventArgs cardStateEventArgs )
		{
			base.HandleBattleEntityStateUpdated( o, cardStateEventArgs );
			switch( cardStateEventArgs.NewState )
			{

			case BattleEntityController.State.CardDrawing:
				RemoveCardFromHand( cardStateEventArgs.BattleEntity );
				cardStateEventArgs.BattleEntity.transform.parent = mCardDrawn;
				break;

				//Where we get a notification that a card is dragging let's remove it from the hand
				//card distributor
			case BattleEntityController.State.CardDragging:
				RemoveCardFromHand( cardStateEventArgs.BattleEntity );
				break;

				//Were we get a notification that a card has returned to the hand let's readd it to the hand
			case BattleEntityController.State.CardReadyToReturnToHand:
				AddCardsToHand( new BattleEntity[] { cardStateEventArgs.BattleEntity } );
				break;
			}
		}
		#endregion

		#region Private Event Handlers

		private void HandleEntitySelected( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
		}

		private void HandleBattleEntityAdded( object o, TransformEventArgs transformEventArgs )
		{
			BattleEntity battleEntity = transformEventArgs.Transform.GetComponent<BattleEntity>();
			SJLogger.AssertCondition( battleEntity != null, "Objects added to the distributor must have a BattleEntity component" );
			FinishedAddingCardToHand( battleEntity );
		}

		public void HandleAbandonCardPlace( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			battleEntityEventArgs.BattleEntity.OnInsertState(BattleEntityController.State.CardReadyToReturnToHand);
		}
		#endregion
	}
}