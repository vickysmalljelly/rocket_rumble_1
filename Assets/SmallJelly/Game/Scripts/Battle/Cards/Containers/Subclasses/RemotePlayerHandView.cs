using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class RemotePlayerHandView : PlayerHandView
	{
		#region Protected Methods
		protected override CircleDistributionShape GetDistributionShape( Vector3 orientationPosition, float circleRadius, float startDegree, float endDegree, float maximumDegreesBetweenPoints, Vector3 pivotPoint )
		{
			return new CircleDistributionShape( circleRadius, startDegree, endDegree, maximumDegreesBetweenPoints, pivotPoint );
		}
		#endregion
	}
}