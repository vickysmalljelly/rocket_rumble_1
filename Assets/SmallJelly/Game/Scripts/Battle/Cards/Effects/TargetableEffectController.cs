﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Linq;

namespace SmallJelly
{
	public class TargetableEffectController : SJMonoBehaviour
	{
		#region Exposed To Inspector
		public TargetableEffectAnimationController AnimationController
		{
			protected get;
			set;
		}
		#endregion
	}
}