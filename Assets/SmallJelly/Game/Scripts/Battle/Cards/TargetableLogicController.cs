﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class TargetableLogicController : SJMonoBehaviour
	{
		#region Enums
		//Whether the object is allowed to be targeted or not (In a card text phase for example this is updated from
		//"None" to either "Targetable" or "NonTargetable")
		public enum TargetableState
		{
			None,
			Targeting,
			Targeted,
			Targetable,
			NonTargetable
		}

		public enum SelectionState
		{
			NotSelected,
			Selected
		}
		#endregion

		#region Public Event Handlers
		public event EventHandler<SelectionStateTransitionEventArgs> SelectionStateUpdated;
		public event EventHandler<TargetableStateTransitionEventArgs> TargetableStateUpdated;
		#endregion

		#region Public Properties
		public SelectionState CurrentSelectionState
		{
			get
			{
				return mSelectionState;
			}
		}

		public TargetableState CurrentTargetableState
		{
			get
			{
				return mTargetableState;
			}
		}
		#endregion

		#region Protected Properties
		protected SJRenderRaycastHit[] LayersHitBehindEntityThisFrame
		{
			get
			{
				return mLayersHitBehindEntityThisFrame;
			}
		}

		public Vector2 MovementDuringFrame
		{
			get
			{
				return mMovementDuringFrame;
			}
		}
		#endregion

		#region Member Properties
		protected bool IsInteractable
		{
			get
			{
				//Interact with this component if it's targeting or has no targeting state
				return mTargetableState != TargetableState.NonTargetable;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private SelectionState mSelectionState;

		[SerializeField]
		private TargetableState mTargetableState;
		#endregion

		#region Member Variables
		private SJRenderRaycastHit[] mLayersHitBehindEntityThisFrame;
		private Vector2 mMovementDuringFrame;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake ()
		{
			base.Awake ();

			mLayersHitBehindEntityThisFrame = new SJRenderRaycastHit[0];
		}

		public void LateUpdate()
		{
			//Refresh ready for the next frame
			mLayersHitBehindEntityThisFrame = new SJRenderRaycastHit[0];
		}
		#endregion

		#region Public Methods
		public virtual void OnInsertSelectionState( SelectionState selectionState )
		{
			if( mSelectionState != selectionState )
			{
				SelectionState previousSelectionState = mSelectionState;
				OnLeaveSelectionState( previousSelectionState );
				mSelectionState = selectionState;

				FireSelectionStateUpdated( previousSelectionState, selectionState, mLayersHitBehindEntityThisFrame );
			}
		}

		public virtual void OnInsertTargetableState( TargetableState targetableState )
		{
			if( mTargetableState != targetableState )
			{
				TargetableState previousTargetableState = mTargetableState;

				OnLeaveTargetableState( mTargetableState );
				mTargetableState = targetableState;

				FireTargetableStateUpdated( previousTargetableState, targetableState, mLayersHitBehindEntityThisFrame );
			}
		}
		#endregion

		#region Abstract Methods
		public abstract void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnMouseRolledOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

		public abstract void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

		public abstract void OnMousePressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

		public abstract void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

	
		public abstract void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

		public abstract void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );

		public abstract void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		public abstract void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame );
		#endregion

		#region Protected Methods
		protected virtual void OnLeaveSelectionState( SelectionState selectionState )
		{
		}

		protected virtual void OnLeaveTargetableState( TargetableState targetableState )
		{
		}


		protected virtual void FireSelectionStateUpdated( SelectionState previousSelectionState, SelectionState newSelectionState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( SelectionStateUpdated != null )
			{
				SelectionStateUpdated( this, new SelectionStateTransitionEventArgs( previousSelectionState, newSelectionState, layersHitBehindEntity ) );
			}
		}

		protected virtual void FireTargetableStateUpdated( TargetableState previousSelectionState, TargetableState newSelectionState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( TargetableStateUpdated != null )
			{
				TargetableStateUpdated( this, new TargetableStateTransitionEventArgs( previousSelectionState, newSelectionState, layersHitBehindEntity ) );
			}
		}
		#endregion

		#region Mouse Event Handlers
		public void HandleMouseRolledOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseRolledOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseHovered(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseHovered( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseRolledOff(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseRolledOff( eventArgs.Array, eventArgs.MovementDuringFrame );
		}


		public void HandleMouseDraggedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseDraggedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseDragged(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseDragged( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseDraggedOff(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseDraggedOff( eventArgs.Array, eventArgs.MovementDuringFrame );
		}


		public void HandleMousePressedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMousePressedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseReleasedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseReleasedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleMouseReleasedOff(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnMouseReleasedOff( eventArgs.Array, eventArgs.MovementDuringFrame );
		}
		#endregion



		#region Touch Event Handlers
		public void HandleTouchDraggedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchDraggedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleTouchDragged(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchDragged( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleTouchDraggedOff(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchDraggedOff( eventArgs.Array, eventArgs.MovementDuringFrame );
		}


		public void HandleTouchPressedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchPressedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleTouchReleasedOver(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchReleasedOver( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		public void HandleTouchReleasedOff(object o, InputEventArgs eventArgs)
		{
			mMovementDuringFrame = eventArgs.MovementDuringFrame;
			mLayersHitBehindEntityThisFrame = eventArgs.Array;
			OnTouchReleasedOff( eventArgs.Array, eventArgs.MovementDuringFrame );
		}

		#endregion
	}
}