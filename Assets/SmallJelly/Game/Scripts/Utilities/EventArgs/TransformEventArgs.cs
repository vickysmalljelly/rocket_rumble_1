﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class TransformEventArgs : EventArgs
	{
		#region Public Properties
		public Transform Transform 
		{ 
			get 
			{ 
				return mTransform; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly Transform mTransform;
		#endregion

		#region Constructors
		public TransformEventArgs(Transform transform)
		{
			mTransform = transform;
		}
		#endregion
	}
}