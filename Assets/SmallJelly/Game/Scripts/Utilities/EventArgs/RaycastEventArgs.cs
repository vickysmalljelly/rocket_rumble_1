﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class RenderRaycastEventArgs : EventArgs
	{
		#region Public Properties
		public Vector2 ScreenPosition
		{
			get
			{
				return mScreenPosition;
			}
		}

		public SJRenderRaycastHit[] Array 
		{ 
			get 
			{ 
				return mArray; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly Vector2 mScreenPosition;
		private readonly SJRenderRaycastHit[] mArray;
		#endregion

		#region Constructors
		public RenderRaycastEventArgs(Vector2 screenPosition, SJRenderRaycastHit[] array)
		{
			mScreenPosition = screenPosition;
			mArray = array;
		}
		#endregion
	}
}