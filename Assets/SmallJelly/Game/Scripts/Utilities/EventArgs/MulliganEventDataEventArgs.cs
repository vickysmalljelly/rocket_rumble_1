﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class MulliganEventDataEventArgs : EventArgs
	{
		#region Public Properties
		public MulliganEventData MulliganEventData 
		{ 
			get 
			{ 
				return mMulliganEventData; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly MulliganEventData mMulliganEventData;
		#endregion

		#region Constructors
		public MulliganEventDataEventArgs( MulliganEventData mulliganEventData )
		{
			mMulliganEventData = mulliganEventData;
		}
		#endregion
	}
}