﻿using UnityEngine;
using System.Collections;
using System;

public class GameObjectEventArgs : EventArgs
{
	#region Public Properties
	public GameObject GameObject 
	{ 
		get 
		{ 
			return mGameObject; 
		} 
	}
	#endregion

	#region Member Variables
	private readonly GameObject mGameObject;
	#endregion

	#region Constructors
	public GameObjectEventArgs(GameObject gameObject)
	{
		mGameObject = gameObject;
	}
	#endregion
}
