﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class InboxEventDataEventArgs : EventArgs
	{
		#region Public Properties
		public InboxEventData InboxEventData 
		{ 
			get 
			{ 
				return mInboxEventData; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly InboxEventData mInboxEventData;
		#endregion

		#region Constructors
		public InboxEventDataEventArgs( InboxEventData inboxEventData )
		{
			mInboxEventData = inboxEventData;
		}
		#endregion
	}
}