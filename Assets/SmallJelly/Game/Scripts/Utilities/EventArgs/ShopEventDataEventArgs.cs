﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ShopEventDataEventArgs : EventArgs
	{
		#region Public Properties
		public ShopEventData ShopEventData 
		{ 
			get 
			{ 
				return mShopEventData; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly ShopEventData mShopEventData;
		#endregion

		#region Constructors
		public ShopEventDataEventArgs( ShopEventData inboxEventData )
		{
			mShopEventData = inboxEventData;
		}
		#endregion
	}
}