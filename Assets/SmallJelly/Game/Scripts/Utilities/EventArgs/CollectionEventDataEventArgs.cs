﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class CollectionEventDataEventArgs : EventArgs
	{
		#region Public Properties
		public CollectionEventData CollectionEventData 
		{ 
			get 
			{ 
				return mCollectionEventData; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly CollectionEventData mCollectionEventData;
		#endregion

		#region Constructors
		public CollectionEventDataEventArgs( CollectionEventData collectionEventData )
		{
			mCollectionEventData = collectionEventData;
		}
		#endregion
	}
}