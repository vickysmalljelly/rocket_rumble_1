﻿
using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class CardSelectionStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public CardSelectionController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public CardSelectionController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly CardSelectionController.State mPreviousState;
		private readonly CardSelectionController.State mNewState;
		#endregion

		#region Constructors
		public CardSelectionStateTransitionEventArgs( CardSelectionController.State previousState, CardSelectionController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
