﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityBoardPreviewAndStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntityBoardPreview BattleEntityBoardPreview 
		{ 
			get 
			{ 
				return mBattleEntityBoardPreview; 
			} 
		}

		public BattleEntityBoardPreviewController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public BattleEntityBoardPreviewController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityBoardPreview mBattleEntityBoardPreview;
		private readonly BattleEntityBoardPreviewController.State mPreviousState;
		private readonly BattleEntityBoardPreviewController.State mNewState;
		#endregion

		#region Constructors
		public BattleEntityBoardPreviewAndStateTransitionEventArgs(BattleEntityBoardPreview battleEntityBoardPreview, BattleEntityBoardPreviewController.State previousState, BattleEntityBoardPreviewController.State newState)
		{
			mBattleEntityBoardPreview = battleEntityBoardPreview;
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
