﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityPlayableStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public CardController.PlayableState PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public CardController.PlayableState NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly CardController.PlayableState mPreviousState;
		private readonly CardController.PlayableState mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityPlayableStateTransitionEventArgs(CardController.PlayableState previousState, CardController.PlayableState newState, SJRenderRaycastHit[] entityHitLayers)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
