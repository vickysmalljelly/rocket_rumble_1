﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class SocketInformationEventArgs : EventArgs
	{
		#region Public Properties
		public int DrawNumber 
		{ 
			get 
			{ 
				return mDrawNumber; 
			} 
		}

		public int SlotId 
		{
			get 
			{ 
				return mSlotId; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly int mDrawNumber;
		private readonly int mSlotId;
		#endregion
		
		#region Constructors
		public SocketInformationEventArgs(int drawNumber, int slotId)
		{
			mDrawNumber = drawNumber;
			mSlotId = slotId;
		}
		#endregion
	}
}