﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntity BattleEntity 
		{ 
			get 
			{ 
				return mBattleEntity; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly BattleEntity mBattleEntity;
		#endregion
		
		#region Constructors
		public BattleEntityEventArgs(BattleEntity battleEntity)
		{
			mBattleEntity = battleEntity;
		}
		#endregion
	}
}