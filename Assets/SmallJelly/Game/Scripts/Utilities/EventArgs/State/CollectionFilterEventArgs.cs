﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class CollectionFilterEventArgs : EventArgs
	{
		#region Public Properties
		public CollectionFilterData CollectionFilterData 
		{ 
			get 
			{ 
				return mCollectionFilterData; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly CollectionFilterData mCollectionFilterData;
		#endregion
		
		#region Constructors
		public CollectionFilterEventArgs( CollectionFilterData collectionFilterData )
		{
			mCollectionFilterData = collectionFilterData;
		}
		#endregion
	}
}