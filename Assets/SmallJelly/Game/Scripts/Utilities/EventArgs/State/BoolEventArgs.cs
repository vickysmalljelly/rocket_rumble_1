﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BoolEventArgs : EventArgs
	{
		#region Public Properties
		public bool Bool 
		{ 
			get 
			{ 
				return mBool; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly bool mBool;
		#endregion
		
		#region Constructors
		public BoolEventArgs(bool boolArg)
		{
			mBool = boolArg;
		}
		#endregion
	}
}