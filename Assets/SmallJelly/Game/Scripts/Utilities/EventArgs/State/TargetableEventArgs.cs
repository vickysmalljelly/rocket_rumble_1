﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class TargetableEventArgs : EventArgs
	{
		#region Public Properties
		public Targetable Targetable 
		{ 
			get 
			{ 
				return mTargetable; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly Targetable mTargetable;
		#endregion
		
		#region Constructors
		public TargetableEventArgs(Targetable battleEntity)
		{
			mTargetable = battleEntity;
		}
		#endregion
	}
}