﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntityController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public BattleEntityController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.State mPreviousState;
		private readonly BattleEntityController.State mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityStateTransitionEventArgs(BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] entityHitLayers)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
