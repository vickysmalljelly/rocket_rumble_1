﻿
using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class DeckSelectionStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public DeckListController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public DeckListController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly DeckListController.State mPreviousState;
		private readonly DeckListController.State mNewState;
		#endregion

		#region Constructors
		public DeckSelectionStateTransitionEventArgs( DeckListController.State previousState, DeckListController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
