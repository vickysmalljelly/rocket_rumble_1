﻿
using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class DeckContentsStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public DeckContentsController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public DeckContentsController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly DeckContentsController.State mPreviousState;
		private readonly DeckContentsController.State mNewState;
		#endregion

		#region Constructors
		public DeckContentsStateTransitionEventArgs( DeckContentsController.State previousState, DeckContentsController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
