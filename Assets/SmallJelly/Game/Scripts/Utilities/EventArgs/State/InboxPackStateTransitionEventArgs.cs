﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class InboxPackStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public  InboxPackController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public  InboxPackController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly InboxPackController.State mPreviousState;
		private readonly InboxPackController.State mNewState;
		#endregion

		#region Constructors
		public InboxPackStateTransitionEventArgs( InboxPackController.State previousState, InboxPackController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
