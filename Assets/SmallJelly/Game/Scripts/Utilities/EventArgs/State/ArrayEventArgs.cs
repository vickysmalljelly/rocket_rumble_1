﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class ArrayEventArgs<T> : EventArgs
	{
		#region Public Properties
		public T[] Array
		{ 
			get 
			{ 
				return mArray; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly T[] mArray;
		#endregion

		#region Constructors
		public ArrayEventArgs(T[] array)
		{
			mArray = array;
		}
		#endregion
	}
}