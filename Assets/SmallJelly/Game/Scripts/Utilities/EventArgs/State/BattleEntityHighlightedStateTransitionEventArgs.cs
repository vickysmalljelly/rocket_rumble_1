﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityHighlightedStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public ComponentController.HighlightedState PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public ComponentController.HighlightedState NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly ComponentController.HighlightedState mPreviousState;
		private readonly ComponentController.HighlightedState mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityHighlightedStateTransitionEventArgs(ComponentController.HighlightedState previousState, ComponentController.HighlightedState newState, SJRenderRaycastHit[] entityHitLayers)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
