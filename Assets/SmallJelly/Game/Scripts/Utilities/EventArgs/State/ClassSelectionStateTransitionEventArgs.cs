﻿
using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ClassSelectionStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public ClassSelectionController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public ClassSelectionController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly ClassSelectionController.State mPreviousState;
		private readonly ClassSelectionController.State mNewState;
		#endregion

		#region Constructors
		public ClassSelectionStateTransitionEventArgs( ClassSelectionController.State previousState, ClassSelectionController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
