﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityTargetableStateTransitionEventArgs : TargetableStateTransitionEventArgs
	{
		#region Public Properties
		public BattleEntityController.State State 
		{ 
			get 
			{ 
				return mState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.State mState;
		#endregion

		#region Constructors
		public BattleEntityTargetableStateTransitionEventArgs( BattleEntityController.State state, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] entityHitLayers ) : base( previousState, newState, entityHitLayers )
		{
			mState = state;
		}
		#endregion
	}
}
