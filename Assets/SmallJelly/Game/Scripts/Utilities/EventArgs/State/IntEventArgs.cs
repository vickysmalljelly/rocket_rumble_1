﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class IntEventArgs : EventArgs
	{
		#region Public Properties
		public int Int 
		{ 
			get 
			{ 
				return mInt; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly int mInt;
		#endregion
		
		#region Constructors
		public IntEventArgs(int intArg)
		{
			mInt = intArg;
		}
		#endregion
	}
}