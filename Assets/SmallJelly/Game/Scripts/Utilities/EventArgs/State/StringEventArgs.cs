﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class StringEventArgs: EventArgs
	{
		#region Public Properties
		public string String
		{ 
			get 
			{ 
				return mString; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly string mString;
		#endregion

		#region Constructors
		public StringEventArgs( string str )
		{
			mString = str;
		}
		#endregion
	}
}