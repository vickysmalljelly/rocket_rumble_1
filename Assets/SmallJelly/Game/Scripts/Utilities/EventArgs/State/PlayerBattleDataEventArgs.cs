﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class PlayerBattleDataEventArgs : EventArgs
	{
		#region Public Properties
		public string NextPlayerId 
		{ 
			get 
			{ 
				return mNextPlayerId; 
			} 
		}

		public PlayerBattleData LocalPlayerBattleData 
		{ 
			get 
			{ 
				return mLocalPlayerBattleData; 
			} 
		}

		public PlayerBattleData RemotePlayerBattleData 
		{ 
			get 
			{ 
				return mRemotePlayerBattleData; 
			} 
		}

		public PlayerTargetingBattleData LocalPlayerTargetingBattleData
		{
			get
			{
				return mLocalPlayerTargetingBattleData;
			}
		}

		public PlayerTargetingBattleData RemotePlayerTargetingBattleData
		{
			get
			{
				return mRemotePlayerTargetingBattleData;
			}
		}

		public int IssueNumber
		{
			get
			{
				return mIssueNumber;
			}
		}
		#endregion
		
		#region Member Variables
		private readonly string mNextPlayerId;

		private readonly PlayerBattleData mLocalPlayerBattleData;
		private readonly PlayerBattleData mRemotePlayerBattleData;

		private readonly PlayerTargetingBattleData mLocalPlayerTargetingBattleData;
		private readonly PlayerTargetingBattleData mRemotePlayerTargetingBattleData;

		private readonly int mIssueNumber;
		#endregion
		
		#region Constructors
		public PlayerBattleDataEventArgs( string nextPlayerId, PlayerBattleData localPlayerBattleData, PlayerBattleData remotePlayerBattleData, PlayerTargetingBattleData localPlayerTargetingBattleData, PlayerTargetingBattleData remotePlayerTargetingBattleData, int issueNumber )
		{
			mNextPlayerId = nextPlayerId;

			mLocalPlayerBattleData = localPlayerBattleData;
			mRemotePlayerBattleData = remotePlayerBattleData;

			mLocalPlayerTargetingBattleData = localPlayerTargetingBattleData;
			mRemotePlayerTargetingBattleData = remotePlayerTargetingBattleData;

			mIssueNumber = issueNumber;
		}
		#endregion
	}
}