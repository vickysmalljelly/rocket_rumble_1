﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleRunnerEventArgs : EventArgs
	{
		#region Public Properties
		public BattleRunner BattleRunner 
		{ 
			get 
			{ 
				return mBattleRunner; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly BattleRunner mBattleRunner;
		#endregion
		
		#region Constructors
		public BattleRunnerEventArgs(BattleRunner battleRunner)
		{
			mBattleRunner = battleRunner;
		}
		#endregion
	}
}