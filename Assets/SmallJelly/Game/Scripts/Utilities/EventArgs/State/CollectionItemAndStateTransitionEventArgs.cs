﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class CollectionItemAndStateTransitionEventArgs : CollectionItemEventArgs
	{
		#region Public Properties
		public  CollectionItemController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public  CollectionItemController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly  CollectionItemController.State mPreviousState;
		private readonly  CollectionItemController.State mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public CollectionItemAndStateTransitionEventArgs(CollectionItem card,  CollectionItemController.State previousState,  CollectionItemController.State newState, SJRenderRaycastHit[] entityHitLayers) : base(card)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
