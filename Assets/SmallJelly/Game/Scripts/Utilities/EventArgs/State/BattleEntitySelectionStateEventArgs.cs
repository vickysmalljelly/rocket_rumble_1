﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntitySelectionStateEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntityController.SelectionState State 
		{ 
			get 
			{ 
				return mState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.SelectionState mState;
		#endregion

		#region Constructors
		public BattleEntitySelectionStateEventArgs(BattleEntityController.SelectionState state)
		{
			mState = state;
		}
		#endregion
	}
}
