﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityTargetingEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntity SourceBattleEntity 
		{ 
			get 
			{ 
				return mSourceEntity; 
			} 
		}

		public Targetable Target 
		{ 
			get 
			{ 
				return mTarget; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntity mSourceEntity;
		private readonly Targetable mTarget;
		#endregion

		#region Constructors
		public BattleEntityTargetingEventArgs(BattleEntity sourceEntity, Targetable targetEntity)
		{
			mSourceEntity = sourceEntity;
			mTarget = targetEntity;
		}
		#endregion
	}
}