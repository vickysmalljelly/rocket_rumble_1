﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class InputEventArgs : ArrayEventArgs< SJRenderRaycastHit >
	{
		#region Public Properties
		public Vector2 MovementDuringFrame
		{ 
			get 
			{ 
				return mMovementDuringFrame; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly Vector2 mMovementDuringFrame;
		#endregion

		#region Constructors
		public InputEventArgs( SJRenderRaycastHit[] raycastHit, Vector2 movementDuringFrame ) : base( raycastHit )
		{
			mMovementDuringFrame = movementDuringFrame;
		}
		#endregion
	}
}