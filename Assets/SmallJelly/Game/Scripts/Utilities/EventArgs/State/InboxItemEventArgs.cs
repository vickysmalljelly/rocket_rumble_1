﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class InboxItemEventArgs : EventArgs
	{
		#region Public Properties
		public InboxItem InboxItem 
		{ 
			get 
			{ 
				return mInboxItem; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly InboxItem mInboxItem;
		#endregion
		
		#region Constructors
		public InboxItemEventArgs(InboxItem inboxItem)
		{
			mInboxItem = inboxItem;
		}
		#endregion
	}
}