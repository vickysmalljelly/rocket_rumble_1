﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class MulliganEntityAndSelectionStateTransitionEventArgs : MulliganEntityEventArgs
	{
		#region Public Properties
		public MulliganEntityController.SelectionState PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public MulliganEntityController.SelectionState NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly MulliganEntityController.SelectionState mPreviousState;
		private readonly MulliganEntityController.SelectionState mNewState;
		#endregion

		#region Constructors
		public MulliganEntityAndSelectionStateTransitionEventArgs( MulliganEntity card, MulliganEntityController.SelectionState previousState, MulliganEntityController.SelectionState newState ) : base( card )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
