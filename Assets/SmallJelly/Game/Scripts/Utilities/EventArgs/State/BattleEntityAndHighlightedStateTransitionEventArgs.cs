﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityAndHighlightedStateTransitionEventArgs : BattleEntityEventArgs
	{
		#region Public Properties
		public ComponentController.HighlightedState PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public ComponentController.HighlightedState NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly ComponentController.HighlightedState mPreviousState;
		private readonly ComponentController.HighlightedState mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityAndHighlightedStateTransitionEventArgs(BattleEntity battleEntity, ComponentController.HighlightedState previousState, ComponentController.HighlightedState newState, SJRenderRaycastHit[] entityHitLayers) : base(battleEntity)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
