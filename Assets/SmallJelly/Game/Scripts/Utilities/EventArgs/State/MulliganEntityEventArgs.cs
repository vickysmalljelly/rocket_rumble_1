﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class MulliganEntityEventArgs : EventArgs
	{
		#region Public Properties
		public MulliganEntity MulliganEntity 
		{ 
			get 
			{ 
				return mMulliganEntity; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly MulliganEntity mMulliganEntity;
		#endregion
		
		#region Constructors
		public MulliganEntityEventArgs( MulliganEntity mulliganEntity )
		{
			mMulliganEntity = mulliganEntity;
		}
		#endregion
	}
}