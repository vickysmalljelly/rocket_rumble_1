﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class CollectionItemEventArgs : EventArgs
	{
		#region Public Properties
		public CollectionItem CollectionEntity 
		{ 
			get 
			{ 
				return mCollectionEntity; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly CollectionItem mCollectionEntity;
		#endregion
		
		#region Constructors
		public CollectionItemEventArgs(CollectionItem battleEntity)
		{
			mCollectionEntity = battleEntity;
		}
		#endregion
	}
}