﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityAndSelectionStateEventArgs : BattleEntityEventArgs
	{
		#region Public Properties
		public BattleEntityController.SelectionState State 
		{ 
			get 
			{ 
				return mState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.SelectionState mState;
		#endregion

		#region Constructors
		public BattleEntityAndSelectionStateEventArgs(BattleEntity card, BattleEntityController.SelectionState state) : base(card)
		{
			mState = state;
		}
		#endregion
	}
}
