﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ShopItemAndStateTransitionEventArgs : ShopItemEventArgs
	{
		#region Public Properties
		public  ShopItemController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public  ShopItemController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly  ShopItemController.State mPreviousState;
		private readonly  ShopItemController.State mNewState;
		#endregion

		#region Constructors
		public ShopItemAndStateTransitionEventArgs(ShopItem inboxItem,  ShopItemController.State previousState,  ShopItemController.State newState ) : base(inboxItem)
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
