﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ShopProductInformationEventArgs : EventArgs
	{
		#region Public Properties
		public ShopProductInformation ShopProductInformation 
		{ 
			get 
			{ 
				return mShopProductInformation; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly ShopProductInformation mShopProductInformation;
		#endregion
		
		#region Constructors
		public ShopProductInformationEventArgs(ShopProductInformation inboxItem)
		{
			mShopProductInformation = inboxItem;
		}
		#endregion
	}
}