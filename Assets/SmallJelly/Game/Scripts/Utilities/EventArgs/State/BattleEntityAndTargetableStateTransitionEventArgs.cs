﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityAndTargetableStateTransitionEventArgs : BattleEntityEventArgs
	{
		#region Public Properties
		public BattleEntityController.State State
		{
			get
			{
				return mState;
			}
		}

		public BattleEntityController.TargetableState PreviousTargetableState 
		{ 
			get 
			{ 
				return mPreviousTargetableState; 
			} 
		}

		public BattleEntityController.TargetableState NewTargetableState 
		{ 
			get 
			{ 
				return mNewTargetableState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.State mState;
		private readonly BattleEntityController.TargetableState mPreviousTargetableState;
		private readonly BattleEntityController.TargetableState mNewTargetableState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityAndTargetableStateTransitionEventArgs(BattleEntity card, BattleEntityController.State state, BattleEntityController.TargetableState previousTargetableState, BattleEntityController.TargetableState newTargetableState, SJRenderRaycastHit[] entityHitLayers) : base(card)
		{
			mState = state;
			mPreviousTargetableState = previousTargetableState;
			mNewTargetableState = newTargetableState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
