﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ShopItemEventArgs : EventArgs
	{
		#region Public Properties
		public ShopItem ShopItem 
		{ 
			get 
			{ 
				return mShopItem; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly ShopItem mShopItem;
		#endregion
		
		#region Constructors
		public ShopItemEventArgs(ShopItem inboxItem)
		{
			mShopItem = inboxItem;
		}
		#endregion
	}
}