﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class SocketEventArgs : EventArgs
	{
		#region Public Properties
		public Socket Socket 
		{ 
			get 
			{ 
				return mSocket; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly Socket mSocket;
		#endregion
		
		#region Constructors
		public SocketEventArgs(Socket battleEntity)
		{
			mSocket = battleEntity;
		}
		#endregion
	}
}