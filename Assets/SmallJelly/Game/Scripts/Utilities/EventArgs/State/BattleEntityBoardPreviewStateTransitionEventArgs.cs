﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityBoardPreviewStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public BattleEntityBoardPreviewController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public BattleEntityBoardPreviewController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityBoardPreviewController.State mPreviousState;
		private readonly BattleEntityBoardPreviewController.State mNewState;
		#endregion

		#region Constructors
		public BattleEntityBoardPreviewStateTransitionEventArgs(BattleEntityBoardPreviewController.State previousState, BattleEntityBoardPreviewController.State newState)
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
