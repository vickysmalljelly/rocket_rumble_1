﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class RenderButtonEventArgs : EventArgs
	{
		#region Public Properties
		public RenderButton RenderButton 
		{ 
			get 
			{ 
				return mRenderButton; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly RenderButton mRenderButton;
		#endregion
		
		#region Constructors
		public RenderButtonEventArgs(RenderButton renderButton)
		{
			mRenderButton = renderButton;
		}
		#endregion
	}
}