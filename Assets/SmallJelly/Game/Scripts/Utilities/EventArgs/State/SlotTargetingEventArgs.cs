﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class SlotTargetingEventArgs : EventArgs
	{
		#region Public Properties
		public int SourceSlot 
		{ 
			get 
			{ 
				return mSourceSlot; 
			} 
		}

		public int TargetSlot 
		{ 
			get 
			{ 
				return mTargetSlot; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly int mSourceSlot;
		private readonly int mTargetSlot;
		#endregion

		#region Constructors
		public SlotTargetingEventArgs(int sourceSlot, int targetSlot)
		{
			mSourceSlot = sourceSlot;
			mTargetSlot = targetSlot;
		}
		#endregion
	}
}