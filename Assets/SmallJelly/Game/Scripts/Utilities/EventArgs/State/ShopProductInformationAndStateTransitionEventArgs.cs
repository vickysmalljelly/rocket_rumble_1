﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ShopProductInformationAndStateTransitionEventArgs : ShopProductInformationEventArgs
	{
		#region Public Properties
		public  ShopProductInformationController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public  ShopProductInformationController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly  ShopProductInformationController.State mPreviousState;
		private readonly  ShopProductInformationController.State mNewState;
		#endregion

		#region Constructors
		public ShopProductInformationAndStateTransitionEventArgs(ShopProductInformation inboxItem,  ShopProductInformationController.State previousState,  ShopProductInformationController.State newState ) : base(inboxItem)
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
