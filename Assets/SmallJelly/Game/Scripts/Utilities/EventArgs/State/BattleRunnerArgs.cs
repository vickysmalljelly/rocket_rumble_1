﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public struct BattleRunnerArgs
	{
		#region Public Properties
		public PlayerBattleSceneView LocalSceneView { get; private set; }
		public PlayerBattleData LocalDataBeforeEvent { get; private set; }
		public PlayerBattleData LocalDataAfterEvent { get; private set; }

		public PlayerBattleSceneView RemoteSceneView { get; private set; }
		public PlayerBattleData RemoteDataBeforeEvent { get; private set; }
		public PlayerBattleData RemoteDataAfterEvent { get; private set; }
		#endregion

		#region Constructor
		public BattleRunnerArgs(PlayerBattleSceneView localSceneView, PlayerBattleSceneView remoteSceneView, PlayerBattleData localDataBeforeEvent, PlayerBattleData remoteDataBeforeEvent, PlayerBattleData localDataAfterEvent, PlayerBattleData remoteDataAfterEvent) 
		{
			LocalSceneView = localSceneView;
			RemoteSceneView = remoteSceneView;

			LocalDataBeforeEvent = localDataBeforeEvent;
			RemoteDataBeforeEvent = remoteDataBeforeEvent;

			LocalDataAfterEvent = localDataAfterEvent;
			RemoteDataAfterEvent = remoteDataAfterEvent;
		}
		#endregion
	}
}