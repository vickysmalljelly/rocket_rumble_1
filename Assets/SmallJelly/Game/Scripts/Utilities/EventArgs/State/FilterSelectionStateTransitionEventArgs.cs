﻿
using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class FilterSelectionStateTransitionEventArgs : EventArgs
	{
		#region Public Properties
		public CollectionFilterListController.State PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public CollectionFilterListController.State NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly CollectionFilterListController.State mPreviousState;
		private readonly CollectionFilterListController.State mNewState;
		#endregion

		#region Constructors
		public FilterSelectionStateTransitionEventArgs( CollectionFilterListController.State previousState, CollectionFilterListController.State newState )
		{
			mPreviousState = previousState;
			mNewState = newState;
		}
		#endregion
	}
}
