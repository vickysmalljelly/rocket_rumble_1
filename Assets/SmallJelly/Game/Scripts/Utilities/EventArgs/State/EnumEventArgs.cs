﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class EnumEventArgs< T > : EventArgs where T : struct, IConvertible
	{
		#region Public Properties
		public T Value 
		{ 
			get 
			{ 
				return mValue; 
			} 
		}
		#endregion
		
		#region Member Variables
		private readonly T mValue;
		#endregion

		#region Constructors
		public EnumEventArgs( T valueArg )
		{
			if( !typeof( T ).IsEnum ) 
			{
				SJLogger.Assert( "T must be an enumerated type" );
			}

			mValue = valueArg;
		}
		#endregion
	}
}