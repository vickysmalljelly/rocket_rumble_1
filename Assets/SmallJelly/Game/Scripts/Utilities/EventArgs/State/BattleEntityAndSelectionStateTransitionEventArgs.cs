﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityAndSelectionStateTransitionEventArgs : BattleEntityEventArgs
	{
		#region Public Properties
		public BattleEntityController.SelectionState PreviousState 
		{ 
			get 
			{ 
				return mPreviousState; 
			} 
		}

		public BattleEntityController.SelectionState NewState 
		{ 
			get 
			{ 
				return mNewState; 
			} 
		}

		//Often state transitions are triggered by hitting battle entities, these are the layers that were hit (if any were hit)
		public SJRenderRaycastHit[] EntityHitLayers
		{
			get
			{
				return mEntityHitLayers;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntityController.SelectionState mPreviousState;
		private readonly BattleEntityController.SelectionState mNewState;
		private readonly SJRenderRaycastHit[] mEntityHitLayers;
		#endregion

		#region Constructors
		public BattleEntityAndSelectionStateTransitionEventArgs(BattleEntity card, BattleEntityController.SelectionState previousState, BattleEntityController.SelectionState newState, SJRenderRaycastHit[] entityHitLayers) : base(card)
		{
			mPreviousState = previousState;
			mNewState = newState;
			mEntityHitLayers = entityHitLayers;
		}
		#endregion
	}
}
