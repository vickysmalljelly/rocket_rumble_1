﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class ChallengeSetupEventDataEventArgs : EventArgs
	{
		#region Public Properties
		public ChallengeSetupEventData ChallengeSetupEventData 
		{ 
			get 
			{ 
				return mChallengeSetupEventData; 
			} 
		}
		#endregion

		#region Member Variables
		private readonly ChallengeSetupEventData mChallengeSetupEventData;
		#endregion

		#region Constructors
		public ChallengeSetupEventDataEventArgs( ChallengeSetupEventData inboxEventData )
		{
			mChallengeSetupEventData = inboxEventData;
		}
		#endregion
	}
}