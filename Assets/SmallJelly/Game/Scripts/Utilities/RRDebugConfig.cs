﻿using System;
using System.IO;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    [Serializable]
    public class RRDebugConfig 
    {
        // These should be written so that defaulting to false gives the non-debug behaviour
        public bool AiDebug;
        public bool DontMatchWithAI;
        public bool DisableOnboarding;
		public bool DisableInboxOnboarding;
		public bool SkipTutorial1;
		public bool SkipTutorial2;
		public bool SkipTutorial3;
		public bool SkipTutorial4;       

        public static RRDebugConfig ReadFromFile()
        {
            if(File.Exists(FileLocations.DebugConfigFile)) 
            {
                return XmlSerialization.FromFile<RRDebugConfig>(FileLocations.DebugConfigFile);
            }
        
            return null;
        }

        public static RRDebugConfig SaveToFile()
        {
            // Create a new file and save
            RRDebugConfig config = new RRDebugConfig();
            XmlSerialization.SerializationStatus status = XmlSerialization.ToFile<RRDebugConfig>(config, FileLocations.DebugConfigFile);

            if(status.Status == XmlSerialization.Status.Success)
            {
                Debug.Log("Succesful write of " + FileLocations.DebugConfigFile);
            }
            else
            {
                Debug.LogError("Failed to write " + FileLocations.DebugConfigFile);
                Debug.LogError("Exception " + status.Exception.Message + " " + status.Exception.GetType());
            }

            return config;
        }

        public void ForceSkipTutorials()
        {
            Debug.Log("Forcing tutorials to be skipped");
            DisableOnboarding = true;
            DisableInboxOnboarding = true;
            SkipTutorial1 = true;
            SkipTutorial2 = true;
            SkipTutorial3 = true;
            SkipTutorial4 = true;
        }
    }
}