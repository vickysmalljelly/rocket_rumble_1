﻿
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Refreshes the REwards in the database
    /// Copied and pasted from ReplaysREfreshRunner, whole systm could do with an overhaul.
    /// </summary>
    public class RewardsRefreshRunner
    {
        private enum ReplayExportState
        {
            WAITING,
            DELETING_REPLAYS,
            EXPORTING_REPLAYS,
            FINISHED,
            FAILED
        }
        private ReplayExportState mCurrentReplayState;
        private bool mCurrentStateFinished;

        /// <summary>
        /// Method called in order to set the correct initial state for the runner.
        /// </summary>
        public void InitialiseRefreshReplays()
        {
            mCurrentReplayState = ReplayExportState.WAITING;
            mCurrentStateFinished = true;
        }

        /// <summary>
        /// Method called (ideally in an update method) to perform the right calls in order and progress
        /// correctly through the states.
        /// </summary>
        public void RefreshReplays()
        {
            if(mCurrentStateFinished)
            {
                mCurrentStateFinished = false;
                switch(mCurrentReplayState)
                {
                    case ReplayExportState.WAITING:
                        mCurrentReplayState = ReplayExportState.DELETING_REPLAYS;
                        RewardsExporter.DeleteRewardsCollection(HandleDeleteAllItemsSuccess, HandleDeleteAllItemsError);
                        //MongoDbCollections.DeleteAllItems(ReplayExporter.ReplaysCollectionName, HandleDeleteAllItemsSuccess, HandleDeleteAllItemsError);
                        break;
                    case ReplayExportState.DELETING_REPLAYS:
                        mCurrentReplayState = ReplayExportState.EXPORTING_REPLAYS;
                        RewardsExporter.ExportRewardsToCollection(HandleExportAllItemsSuccess, HandleExportAllItemsError);
                        break;
                    case ReplayExportState.EXPORTING_REPLAYS:
                        mCurrentReplayState = ReplayExportState.FINISHED;
                        break;
                    default:
                        mCurrentReplayState = ReplayExportState.FAILED;
                        break;
                }
            }
        }

        /// <summary>
        /// Returns the current state.
        /// </summary>
        /// <returns>String of current state.</returns>
        public string GetCurrentState()
        {
            return mCurrentReplayState.ToString();
        }

        private void ReplaysHandleSuccess()
        {
            mCurrentStateFinished = true;
        }

        private void ReplaysHandleError()
        {
            mCurrentReplayState = ReplayExportState.FAILED;
        }

        private void HandleDeleteAllItemsSuccess(string collectionName, int numDeleted)
        {
            Debug.Log("Deleted " + numDeleted + " from " + collectionName);
            mCurrentStateFinished = true;
        }

        private void HandleDeleteAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to delete items from collection " + collectionName);
            mCurrentReplayState = ReplayExportState.FAILED;
        }

        private void HandleExportAllItemsSuccess(string collectionName, int numItems)
        {
            Debug.Log("Exported " + numItems + " to " + collectionName);
            mCurrentStateFinished = true;
        }

        private void HandleExportAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to export items to collection " + collectionName);
            mCurrentReplayState = ReplayExportState.FAILED;
        }
    }
}