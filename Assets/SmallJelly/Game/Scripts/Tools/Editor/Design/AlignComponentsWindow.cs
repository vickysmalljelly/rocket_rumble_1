﻿using UnityEngine;
using SmallJelly;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace SmallJelly
{
	public class AlignComponentsWindow : EditorWindow
	{
		#region Constants
		private const string SCENE_PATH = "Assets/SmallJelly/Game/Scenes/Design/TestAlignComponents.unity";
		#endregion

		#region Member Variables
		private Vector2 mScrollPosition;
		private GameObject mTestCard;
		private GameObject mLoadedPrefab;
		private GameObject[] mAvailablePrefabs;
		#endregion

		#region Static Methods
		[MenuItem("Small Jelly/Design/Align Components")]
		private static void Init()
		{
			EditorSceneManager.OpenScene( SCENE_PATH );

			// Get existing open window or if none, make a new one:
			AlignComponentsWindow window = ( AlignComponentsWindow ) EditorWindow.GetWindow( typeof( AlignComponentsWindow ) );
			window.Show();
		}
		#endregion

		#region MonoBehaviour Methods
		public void Awake()
		{
			mTestCard = GameObject.Find("TestCard");
			mAvailablePrefabs = Resources.LoadAll< GameObject >( string.Format("{0}{1}", FileLocations.SmallJellyPrefabs, "/Components" ) );
		}

		public void OnGUI()
		{
			mScrollPosition = GUILayout.BeginScrollView( mScrollPosition );

			foreach( GameObject availablePrefab in mAvailablePrefabs )
			{
				if( GUILayout.Button( availablePrefab.name ) )
				{
					GenerateComponent( availablePrefab );
				}
			}

			GUILayout.EndScrollView();

			GUILayout.Space( 20 );

			if( GUILayout.Button( "Add Indicators To All Components" ) )
			{
				foreach( GameObject availablePrefab in mAvailablePrefabs )
				{
					GenerateComponent( availablePrefab );
					DestroyImmediate( mLoadedPrefab );
				}
			}
				
			if( GUILayout.Button( "Save Component" ) )
			{
				AlignIndicatorToComponent();

				Object prefab = PrefabUtility.GetPrefabParent( mLoadedPrefab );
				PrefabUtility.ReplacePrefab(mLoadedPrefab, prefab, ReplacePrefabOptions.ConnectToPrefab | ReplacePrefabOptions.ReplaceNameBased );
			}
		}

		private void GenerateComponent( GameObject availablePrefab )
		{
			if( mLoadedPrefab != null )
			{
				DestroyImmediate( mLoadedPrefab );
			}

			mLoadedPrefab = PrefabUtility.InstantiatePrefab( availablePrefab ) as GameObject;

			if( SJRenderTransformExtensions.FindDeepChild( mLoadedPrefab.transform, ComponentBattleEntityModel.COMPONENT_ALIGN_POSITION ) == null )
			{
				GameObject componentAlignPosition = new GameObject( ComponentBattleEntityModel.COMPONENT_ALIGN_POSITION );
				componentAlignPosition.transform.parent = mLoadedPrefab.transform;
				componentAlignPosition.transform.localEulerAngles = Vector3.zero;
				componentAlignPosition.transform.localPosition = Vector3.zero;

				Object prefab = PrefabUtility.GetPrefabParent( mLoadedPrefab );
				PrefabUtility.ReplacePrefab(mLoadedPrefab, prefab, ReplacePrefabOptions.ConnectToPrefab | ReplacePrefabOptions.ReplaceNameBased );
			}

			AlignComponentToIndicator();
		}

		private void AlignComponentToIndicator()
		{
			Transform cardAlignPosition = SJRenderTransformExtensions.FindDeepChild( mTestCard.transform, ComponentBattleEntityModel.CARD_ALIGN_POSITION );
			Transform componentAlignPosition = SJRenderTransformExtensions.FindDeepChild( mLoadedPrefab.transform, ComponentBattleEntityModel.COMPONENT_ALIGN_POSITION );

			Vector3 positionOffset = cardAlignPosition.transform.position - componentAlignPosition.transform.position;
			Vector3 rotationOffset = cardAlignPosition.transform.eulerAngles - componentAlignPosition.transform.eulerAngles;

			mLoadedPrefab.transform.position += positionOffset;
			mLoadedPrefab.transform.eulerAngles += rotationOffset;
		}

		private void AlignIndicatorToComponent()
		{
			Transform cardAlignPosition = SJRenderTransformExtensions.FindDeepChild( mTestCard.transform, ComponentBattleEntityModel.CARD_ALIGN_POSITION );
			Transform componentAlignPosition = SJRenderTransformExtensions.FindDeepChild( mLoadedPrefab.transform, ComponentBattleEntityModel.COMPONENT_ALIGN_POSITION );

			componentAlignPosition.transform.position = cardAlignPosition.position;
			componentAlignPosition.transform.eulerAngles = cardAlignPosition.eulerAngles;
		}
		#endregion
	}
}