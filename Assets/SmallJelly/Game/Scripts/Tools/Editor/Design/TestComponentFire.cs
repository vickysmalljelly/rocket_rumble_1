﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;
using SmallJelly;
using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEditor;

public class TestComponentFire 
{
	[MenuItem("Small Jelly/Design/Test Fire Component %#f")]
	public static void CreateCollection()
	{
		Fire();
	}

	#region Private Methods
	private static void Fire()
	{
		//Find the object in the scene which we're interested in!
		GameObject source = GameObject.Find("Source");
		GameObject target = GameObject.Find("Target");

		WeaponFiringSharedVariables weaponFiringSharedVariables = source.GetComponentInChildren< WeaponFiringSharedVariables >();
		WeaponAnimationController weaponAnimationController = source.GetComponent< WeaponAnimationController >();

		SJLogger.AssertCondition( weaponFiringSharedVariables != null, "Source object must have a weapon firing shared variables component" );
		SJLogger.AssertCondition( weaponAnimationController != null, "Source object must have a weapon animation controller component" );

		weaponAnimationController.UpdateAnimations( PlayerType.Local, BattleEntityController.State.CardInitialization, new AnimationMetaData( new Dictionary<string, NamedVariable>() ) );
			
		AnimationMetaData animationMetaData = GetAnimationMetaData( weaponFiringSharedVariables, target );
		weaponAnimationController.UpdateAnimations( PlayerType.Local, BattleEntityController.State.ComponentFire, animationMetaData );
				
	}

	private static AnimationMetaData GetAnimationMetaData( WeaponFiringSharedVariables weaponFiringSharedVariables, GameObject target )
	{
		Dictionary< string, NamedVariable > animationVariables = new Dictionary< string, NamedVariable >();

		//Firing target object
		animationVariables.Add( ComponentFireSuccess.FIRING_TARGET_OBJECT, new FsmGameObject( target ) );

		//Firing target point
		animationVariables.Add( ComponentFireSuccess.FIRING_TARGET_POINT, new FsmVector3( Vector3.zero ) );


		//Add data from the WeaponFiringSharedVariables component
		//Weapon type - currently unused in the graph but will be in the near future
		FsmString weaponTypeString = new FsmString(  );
		weaponTypeString.Value = weaponFiringSharedVariables.WeaponType;
		animationVariables.Add( ComponentFireSuccess.WEAPON_TYPE_SHARED_VARIABLE_ID, weaponTypeString );



		//Number of projectiles
		animationVariables.Add( ComponentFireSuccess.NUMBER_OF_PROJECTILES_SHARED_VARIABLE_ID, new FsmInt( weaponFiringSharedVariables.NumberOfProjectiles ) );

		//Projectile speed
		animationVariables.Add( ComponentFireSuccess.PROJECTILE_SPEED_SHARED_VARIABLE_ID, new FsmFloat( weaponFiringSharedVariables.ProjectileSpeed ) );

		//Projectile file display
		animationVariables.Add( ComponentFireSuccess.PROJECTILE_FIRE_DELAY_SHARED_VARIABLE_ID, new FsmFloat( weaponFiringSharedVariables.ProjectileFireDelay ) );



		//Projectile
		if( weaponFiringSharedVariables.Projectile != default( GameObject ) )
		{
			//Projectile itself
			animationVariables.Add( ComponentFireSuccess.PROJECTILE_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.Projectile ) );
		}


		//Projectile impact
		if( weaponFiringSharedVariables.ProjectileImpact != default( GameObject ) )
		{
			animationVariables.Add( ComponentFireSuccess.PROJECTILE_IMPACT_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.ProjectileImpact ) );
		}

		//Projectile trail
		if( weaponFiringSharedVariables.ProjectileTrail != default( GameObject ) )
		{
			animationVariables.Add( ComponentFireSuccess.PROJECTILE_TRAIL_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.ProjectileTrail ) );
		}

		//Muzzle flash
		if( weaponFiringSharedVariables.MuzzleFlash != default( GameObject ) )
		{
			animationVariables.Add( ComponentFireSuccess.MUZZLE_FLASH_SHARED_VARIABLE_ID, new FsmGameObject( weaponFiringSharedVariables.MuzzleFlash ) );
		}


		//Fire sound effect
		if( weaponFiringSharedVariables.FireSoundEffectName != string.Empty )
		{
			FsmString weaponFireSoundEffect = new FsmString(  );
			weaponFireSoundEffect.Value = weaponFiringSharedVariables.FireSoundEffectName;
			animationVariables.Add( ComponentFireSuccess.WEAPON_FIRE_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponFireSoundEffect );
		}

		//Impact sound effect
		if( weaponFiringSharedVariables.ImpactSoundEffectName != string.Empty )
		{
			FsmString weaponImpactSoundEffect = new FsmString(  );
			weaponImpactSoundEffect.Value = weaponFiringSharedVariables.ImpactSoundEffectName;
			animationVariables.Add( ComponentFireSuccess.WEAPON_IMPACT_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponImpactSoundEffect );
		}

		//Explosion sound effect
		if( weaponFiringSharedVariables.ExplosionSoundEffectName != string.Empty )
		{
			FsmString weaponExplosionSoundEffect = new FsmString(  );
			weaponExplosionSoundEffect.Value = weaponFiringSharedVariables.ExplosionSoundEffectName;
			animationVariables.Add( ComponentFireSuccess.WEAPON_EXPLOSION_SOUND_EFFECT_SHARED_VARIABLE_ID, weaponExplosionSoundEffect );
		}

		return new AnimationMetaData( animationVariables );
	}
	#endregion
}
