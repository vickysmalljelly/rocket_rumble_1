﻿using System;
using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Contains configuration data for manipulating Gamespark's Snapshots
    /// </summary>
    [Serializable]
    public class SnapshotConfig  
    {
        public string ConfigName;

        /// <summary>
        /// The GameSparks environment that we are copying from
        /// </summary>
        public GameSparksEnvironment Source;  

        /// <summary>
        /// List of GameSparks environments that we are going to copy to.
        /// </summary>
        public List<GameSparksEnvironment> DestinationList = new List<GameSparksEnvironment>();

        public static SnapshotConfig ReadFromFile()
        {
            return XmlSerialization.FromFile<SnapshotConfig>(FileLocations.SnapshotConfigFile);
        }
    }
}