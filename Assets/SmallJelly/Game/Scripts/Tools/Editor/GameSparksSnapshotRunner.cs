﻿using System;

namespace SmallJelly
{
    /// <summary>
    /// Class used to call GameSparks Snaphost functions in order to create and copy snapshots.
    /// </summary>
    public class GameSparksSnapshotRunner
    {
        private enum SnapshotState
        {
            WAITING,
            CREATING_SNAPSHOT,
            COPYING_SNAPSHOT,
            FINISHED,
            FAILED
        }
        private static SnapshotState mCurrentSnapshotState;

        private SnapshotConfig mSnapshotConfig;

        public GameSparksSnapshotRunner(SnapshotConfig snapshotConfig)
        {
            mSnapshotConfig = snapshotConfig;
        }

        /// <summary>
        /// Method used to create a snapshot of a GameSparks environment.
        /// </summary>
        /// <param name="snapshotDescription">Description of the new snapshot. Will default to Time/Date if null.</param>
        public void CreateSnapshot(string snapshotDescription, Action successHandler, Action errorHandler)
        {
            ClientGameSparksSnapshot.CreateSnapshot(mSnapshotConfig.Source.Key, snapshotDescription, successHandler, errorHandler);
        }

        /// <summary>
        /// Method used to copy the latest snapshot of the soruce environment over to all the destination environments.
        /// </summary>
        public void CopySnapshot(Action successHandler, Action errorHandler)
        {
            ClientGameSparksSnapshot.CopySnapshotToListOfEnvironments(mSnapshotConfig, successHandler, errorHandler);
        }

        /// <summary>
        /// Returns the current state.
        /// </summary>
        /// <returns>String of current state.</returns>
        public string GetCurrentState()
        {
            return mCurrentSnapshotState.ToString();
        }

        private void SnapshotHandleSuccess()
        {
            mCurrentSnapshotState = SnapshotState.FINISHED;
        }

        private void SnapshotHandleError()
        {
            mCurrentSnapshotState = SnapshotState.FAILED;
        }
    }
}