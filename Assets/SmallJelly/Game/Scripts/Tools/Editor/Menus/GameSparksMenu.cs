﻿using UnityEngine;
using UnityEditor;

using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Menu commands for GameSparks Snapshot functionality.
    /// </summary>
    public class GameSparksMenu
    {
        [MenuItem("Small Jelly/GameSparks/Snapshot Wizard", false, 1)]
        public static void SnapshotWizard()
        {
            EditorWindow.GetWindow(typeof(SnapshotWindow));
        }

        [MenuItem("Small Jelly/GameSparks/GameSparks Account Details", false, 2)]
        public static void SetGamesparksAccountDetails()
        {
            EditGameSparksAccountWindow.Init();
        }

        [MenuItem("Small Jelly/GameSparks/Register System Users", false, 3)]
        public static void RegisterSystemUsers()
        {
            SnapshotConfig snapshotConfig = XmlSerialization.FromFile<SnapshotConfig>(FileLocations.SnapshotConfigFile);
            ClientReservedUser.RegisterReservedUsers(snapshotConfig, successHandler, errorHandler);
        }

        private static void successHandler()
        {

        }

        private static void errorHandler(string errorMessage)
        {
            Debug.LogError(errorMessage);
        }
    }
}