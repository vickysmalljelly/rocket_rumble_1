﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    public static class FeatureFlagsExporter
    {
        private const string mFeatureFlagsCollectionName = "featureFlags";
        private const string mFeatureFlagsDirectoryName = "FeatureFlags";

        #region Menu Items
        [MenuItem("Small Jelly/Export/Feature Flags/Create Collections in Database")]
        public static void CreateCollections()
        {
            MongoDbCollections.CreateCollection(mFeatureFlagsCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Feature Flags/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteFeatureFlagsCollection);
            exporter.AddExport(ExportFeatureFlagsToCollection);

            return exporter;
        }

        public static void DeleteFeatureFlagsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mFeatureFlagsCollectionName, successHandler, errorHandler);
        }          

        public static void ExportFeatureFlagsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mFeatureFlagsDirectoryName, mFeatureFlagsCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }                   

        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }
    }
}


