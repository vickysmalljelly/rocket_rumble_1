﻿using System;
using SmallJelly.Framework;
using UnityEditor;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Menu commands for the Replay Functionality.
    /// </summary>
    public class ReplayExporter
    {
        public const string ReplaysCollectionName = "replays";
        private const string mRewardsDirectoryName = "AutomatedTests";

        [MenuItem("Small Jelly/Export/Replays/Create Collection In Database")]
        public static void CreateCollection()
        {
            MongoDbCollections.CreateCollection(ReplaysCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Replays/Import From Database To Local Json")]
        public static void ImportAllReplays()
        {
			MongoDbReplays.ImportAllReplays(HandleSuccess, HandleError);
        }

        public static string ExtractValueToUseAsId(string itemJson)
        {
            int replayNameIndex = itemJson.IndexOf("\"replayName\"");
            int sliceStart = replayNameIndex + 15;
            string replayName = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex = replayName.IndexOf("\"");
            replayName = replayName.Substring(0, endIndex);
            return replayName;
        }

        [MenuItem("Small Jelly/Export/Replays/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
            
        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteReplayCollection);
            exporter.AddExport(ExportReplaysToCollection);

            return exporter;
        }

        public static void DeleteReplayCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(ReplaysCollectionName, successHandler, errorHandler);
        }

        public static void ExportReplaysToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllItemsFromLocation(FileLocations.AutomatedTestsDirectory, ReplaysCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }
            
		private static void HandleSuccess()
		{
		}

		private static void  HandleError()
		{
			Debug.Log("Error!");
		}

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }

        private static void HandleDeleteAllItemsSuccess(string collectionName, int numDeleted)
        {
            Debug.Log("Deleted " + numDeleted + " from " + collectionName);
        }

        private static void HandleDeleteAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to delete items from collection " + collectionName);
        }

        private static void HandleExportAllItemsSuccess(string collectionName, int numItems)
        {
            Debug.Log("Exported " + numItems + " to " + collectionName);
        }

        private static void HandleExportAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to export items to collection " + collectionName);
        }
    }
}