﻿using System.Diagnostics;
using System.IO;
using SmallJelly.Framework;
using UnityEditor;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Debug menu for Rocket Rumble
    /// </summary>
    public static class DebugMenu
    {
        //#if SJ_DEBUG
        [MenuItem("Small Jelly/RR Debug/Open Server Debug Config")]
        public static void OpenServerConfigDebug()
        {
            string filename = FileLocations.SmallJellyResources + Path.DirectorySeparatorChar + FileLocations.DebugServerConfig + ".txt";

            if(!File.Exists(filename))
            {
                UnityEngine.Debug.LogError("Server debug config file does not exist: " + filename);
                return;
            }              

            Process.Start(filename);
        } 

        [MenuItem("Small Jelly/RR Debug/Open AI Debug")]
        public static void OpenAIDebug()
        {
            if(!File.Exists(FileLocations.AiDebugFile))
            {
                UnityEngine.Debug.LogError("AI debug file does not exist");
                return;
            }              

            Process.Start(FileLocations.AiDebugFile);
        }   

        [MenuItem("Small Jelly/RR Debug/Open Debug Config")]
        public static void OpenDebugConfig()
        {
            if(!File.Exists(FileLocations.DebugConfigFile))
            {
                UnityEngine.Debug.LogError("Log file does not exist yet, reset to create it");
                return;
            }              

            Process.Start(FileLocations.DebugConfigFile);
        }          

        [MenuItem("Small Jelly/RR Debug/Reset Debug Config")]
        public static void ResetDebugConfig()
        {
            if(File.Exists(FileLocations.DebugConfigFile))
            {
                // Delete any existing file
                File.Delete(FileLocations.DebugConfigFile);
            }

            RRDebugConfig.SaveToFile();
        }
        //#endif
    }
}