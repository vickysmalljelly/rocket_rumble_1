﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    /// <summary>
    /// Exports data from all tabs in the RocketRumbleBattleConfig spreadsheet; BattleConfig, PredefinedDecks, StarterDecks
    /// </summary>
    public static class BattleConfigExporter
    {
        private const string mBattleConfigCollectionName = "battleConfig";
        private const string mBattleConfigDirectoryName = "BattleConfig";
        private const string mPredefinedDecksCollectionName = "predefinedDecks";
        private const string mPredefinedDecksDirectoryName = "PredefinedDecks";
        private const string mStarterDecksCollectionName = "starterDecks";
        private const string mStarterDecksDirectoryName = "StarterDecks";
        private const string mAiDecksCollectionName = "aiDecks";
        private const string mAiDecksDirectoryName = "AIDecks";

        #region Menu Items
        [MenuItem("Small Jelly/Export/BattleConfig/Create Collections in Database")]
        public static void CreateCollection()
        {
            MongoDbCollections.CreateCollection(mBattleConfigCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mPredefinedDecksCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mStarterDecksCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mAiDecksCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/BattleConfig/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();
                      
            exporter.AddExport(DeleteBattleConfigCollection);
            exporter.AddExport(ExportBattleConfigToCollection);

            exporter.AddExport(DeletePredefinedDecksCollection);
            exporter.AddExport(ExportPredefinedDecksToCollection);

            exporter.AddExport(DeleteStarterDecksCollection);
            exporter.AddExport(ExportStarterDecksToCollection);

            exporter.AddExport(DeleteAiDecksCollection);
            exporter.AddExport(ExportAiDecksToCollection);

            return exporter;
        }

        public static void DeleteAiDecksCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mAiDecksCollectionName, successHandler, errorHandler);
        }          

        public static void ExportAiDecksToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mAiDecksDirectoryName, mAiDecksCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        } 

        public static void DeleteStarterDecksCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mStarterDecksCollectionName, successHandler, errorHandler);
        }          

        public static void ExportStarterDecksToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mStarterDecksDirectoryName, mStarterDecksCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        } 

        public static void DeleteBattleConfigCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mBattleConfigCollectionName, successHandler, errorHandler);
        }          

        public static void ExportBattleConfigToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mBattleConfigDirectoryName, mBattleConfigCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }       

        public static void DeletePredefinedDecksCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mPredefinedDecksCollectionName, successHandler, errorHandler);
        }          

        public static void ExportPredefinedDecksToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mPredefinedDecksDirectoryName, mPredefinedDecksCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        } 
        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }
    }
}


