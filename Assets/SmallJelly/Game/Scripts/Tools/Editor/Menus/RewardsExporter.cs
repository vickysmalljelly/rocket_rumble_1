﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    public static class RewardsExporter
    {
        private const string mRewardEventsCollectionName = "rewardEvents";
        private const string mRewardsCollectionName = "rewards";
        private const string mRewardEventsDirectoryName = "RewardEvents";
        private const string mRewardsDirectoryName = "Rewards";
        private const string mSimplePacksCollectionName = "simplePacks";
        private const string mSimplePacksDirectoryName = "SimplePacks";
        private const string mSimpleGroupsCollectionName = "simpleGroups";
        private const string mSimpleGroupsDirectoryName = "SimpleGroups";

        #region Menu Items
        [MenuItem("Small Jelly/Export/Rewards/Create Collections in Database", false, 2)]
        public static void CreateCollection()
        {
            MongoDbCollections.CreateCollection(mRewardEventsCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mRewardsCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mSimplePacksCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mSimpleGroupsCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Rewards/Refresh", false, 1)]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteRewardEventsCollection);
            exporter.AddExport(DeleteRewardsCollection);

            exporter.AddExport(ExportRewardEventsToCollection);
            exporter.AddExport(ExportRewardsToCollection);

            exporter.AddExport(DeletePacksCollection);
            exporter.AddExport(DeleteGroupsCollection);

            exporter.AddExport(ExportPacksToCollection);
            exporter.AddExport(ExportGroupsToCollection);

            return exporter;
        }

        public static void DeleteRewardEventsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mRewardEventsCollectionName, successHandler, errorHandler);
        }

        public static void DeleteRewardsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mRewardsCollectionName, successHandler, errorHandler);
        }

        public static void ExportRewardEventsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mRewardEventsDirectoryName, mRewardEventsCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }

        public static void ExportRewardsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mRewardsDirectoryName, mRewardsCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }
        public static void DeletePacksCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mSimplePacksCollectionName, successHandler, errorHandler);
        }

        public static void ExportPacksToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mSimplePacksDirectoryName, mSimplePacksCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }

        public static void DeleteGroupsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mSimpleGroupsCollectionName, successHandler, errorHandler);
        }

        public static void ExportGroupsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mSimpleGroupsDirectoryName, mSimpleGroupsCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }
        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }
    }
}

