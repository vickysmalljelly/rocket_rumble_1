﻿using System.Diagnostics;
using System.IO;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    /// <summary>
    /// Menu commands for Deck tools
    /// </summary>
    public static class DeckTools
    {
        [MenuItem("Small Jelly/Collection/New Collection Config", false, 5)]
        public static void NewCollectionConfig()
        {
            CollectionManager.CreateNewCollectionConfig();
        }

        [MenuItem("Small Jelly/Collection/Open Collection Config", false, 5)]
        public static void OpenCollectionConfig()
        {
            if(!File.Exists(FileLocations.DebugCollectionFile))
            {
                UnityEngine.Debug.LogError("Can't find " + FileLocations.DebugCollectionFile);
                return;
            }

            Process.Start(FileLocations.DebugCollectionFile);
        }    
    }
}
