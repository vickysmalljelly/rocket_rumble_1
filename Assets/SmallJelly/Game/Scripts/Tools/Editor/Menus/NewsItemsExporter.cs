﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
	public static class NewsItemsExporter
	{
        private const string mNewsItemsCollectionName = "newsItems";

		[MenuItem("Small Jelly/Export/News Items/Create Collection in Database")]
		public static void CreateCollection()
		{
			MongoDbNews.CreateCollection(HandleSuccess, HandleError);
		}

        [MenuItem("Small Jelly/Export/News Items/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteNewsItemsCollection);
            exporter.AddExport(ExportNewsItemsToCollection);

            return exporter;
        }


        public static void DeleteNewsItemsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mNewsItemsCollectionName, successHandler, errorHandler);
        }

        public static void ExportNewsItemsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbNews.ExportAllNewsItems(successHandler, errorHandler);          
        }
        private static void HandleSuccess(string name, int coun)
		{
		}

        private static void  HandleError(string name)
		{
			Debug.Log("Error!");
		}
	}
}

