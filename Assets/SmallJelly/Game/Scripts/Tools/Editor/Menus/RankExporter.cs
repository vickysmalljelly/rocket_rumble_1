﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    public static class RankExporter
    {
        private const string mRankCollectionName = "ranks";
        private const string mRankDirectoryName = "Ranks";
        private const string mTierNamesCollectionName = "tiers";
        private const string mTierNamesDirectoryName = "Tiers";

        #region Menu Items
        [MenuItem("Small Jelly/Export/Rank/Create Collections in Database")]
        public static void CreateCollections()
        {
            MongoDbCollections.CreateCollection(mRankCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
            MongoDbCollections.CreateCollection(mTierNamesCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Rank/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteRankCollection);
            exporter.AddExport(ExportRanksToCollection);

            exporter.AddExport(DeleteTierNamesCollection);
            exporter.AddExport(ExportTierNamesToCollection);

            return exporter;
        }

        public static void DeleteRankCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mRankCollectionName, successHandler, errorHandler);
        }          

        public static void ExportRanksToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mRankDirectoryName, mRankCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }

        public static void DeleteTierNamesCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mTierNamesCollectionName, successHandler, errorHandler);
        }          

        public static void ExportTierNamesToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mTierNamesDirectoryName, mTierNamesCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }         
           
        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }
    }
}


