﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    public static class ProfanitiesExporter
    {
        private const string mProfanitiesCollectionName = "profanities";
        private const string mProfanitiesDirectoryName = "Profanities";

        #region Menu Items
        [MenuItem("Small Jelly/Export/Profanities/Create Collections in Database")]
        public static void CreateCollections()
        {
            MongoDbCollections.CreateCollection(mProfanitiesCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Profanities/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods

        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteCollection);
            exporter.AddExport(ExportToCollection);

            return exporter;
        }

        public static void DeleteCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mProfanitiesCollectionName, successHandler, errorHandler);
        }          

        public static void ExportToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mProfanitiesDirectoryName, mProfanitiesCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }                   

        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }
    }
}



