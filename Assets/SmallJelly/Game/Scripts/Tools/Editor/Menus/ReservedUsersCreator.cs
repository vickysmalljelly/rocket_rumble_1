﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    /// <summary>
    /// Creates the reserved users on the environments specified in the SnapshotConfig
    /// </summary>
    public static class ReservedUsersCreator
    {
        [MenuItem("Small Jelly/Reserved Users/Create Reserved Users", false, 1)]
        public static void CreateReservedUsersMenuItem()
        {
            CreateReservedUsers(SuccessHandler, ErrorHandler);
        }

        public static void CreateReservedUsers(Action successHandler, Action<string> errorHandler)
        {
            SnapshotConfig config = SnapshotConfig.ReadFromFile();

            ClientReservedUser.RegisterReservedUsers(config, () => 
            {
                Debug.Log("Created reserved users");
                successHandler();
            }, 
                // TODO: Should be ServerError not string
                (string error) => 
            {
                Debug.Log("Failed to create reserved users"); 
                SJLogger.LogError("The server encountered an error. {0}", error);
                errorHandler(error);
            }
            );
        }

        private static void SuccessHandler()
        {
        }

        private static void ErrorHandler(string error)
        {
        }
    }
}

