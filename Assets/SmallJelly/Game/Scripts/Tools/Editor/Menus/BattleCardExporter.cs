﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    /// <summary>
    /// Menu commands for exporting battle cards.
    /// </summary>
	public class BattleCardExporter  
	{
        private const string mBattleEntitiesCollectionName = "battleEntities";

        [MenuItem("Small Jelly/Export/Battle Cards/Create Collection in Database")]
		public static void CreateCollection()
		{
            MongoDbCollections.CreateCollection(mBattleEntitiesCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
		}

        [MenuItem("Small Jelly/Export/Battle Cards/Export Test Cards")]
        public static void ExportTestCards()
        {
            MongoDbBattleEntities.ExportTestCards(HandleSuccess, HandleError);
        }

        [MenuItem("Small Jelly/Export/Battle Cards/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }


        #region Public methods
        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteBattleEntitiesCollection);
            exporter.AddExport(MongoDbBattleEntities.ExportAllCards);

            return exporter;
        }

        public static void DeleteBattleEntitiesCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mBattleEntitiesCollectionName, successHandler, errorHandler);
        }
        #endregion

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }

        private static void HandleSuccess(string name, int count)
        {
        }

        private static void HandleError(string name)
        {
            Debug.Log("Error!");
        }
    }
}