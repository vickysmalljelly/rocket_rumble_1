﻿using System;
using SmallJelly.Framework;
using UnityEditor;

namespace SmallJelly
{
    public static class ShipCardExporter
    {
        private const string mShipsCollectionName = "ships";
        private const string mShipsDirectoryName = "Ships";

        #region Menu Items
        [MenuItem("Small Jelly/Export/Ships/Create Collection in Database")]
        public static void CreateCollection()
        {
            MongoDbCollections.CreateCollection(mShipsCollectionName, HandleCreateCollectionSuccess, HandleCreateCollectionError);
        }

        [MenuItem("Small Jelly/Export/Ships/Refresh")]
        public static void Refresh()
        {
            Exporter exporter = GetExporter();
            exporter.Run();
        }
        #endregion

        #region Public methods
        public static Exporter GetExporter()
        {
            Exporter exporter = new Exporter();

            exporter.AddExport(DeleteShipsCollection);
            exporter.AddExport(ExportShipsToCollection);

            return exporter;
        }

        public static void DeleteShipsCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.DeleteAllItems(mShipsCollectionName, successHandler, errorHandler);
        }

        public static void ExportShipsToCollection(Action<string, int> successHandler, Action<string> errorHandler)
        {
            MongoDbCollections.ExportAllIG2Utems(mShipsDirectoryName, mShipsCollectionName, ExtractValueToUseAsId, successHandler, errorHandler);
        }
        #endregion

        private static string ExtractValueToUseAsId(string itemJson)
        {
            int idIndex = itemJson.IndexOf("\"Id\"");
            int sliceStart = idIndex + 6;
            string idString = itemJson.Substring(sliceStart, itemJson.Length - sliceStart);
            int endIndex  = idString.IndexOf("\"");
            idString = idString.Substring(0, endIndex);
            // Debug.Log("Extracted: " + idString);
            return idString;
        }

        private static void HandleCreateCollectionSuccess(string collectionName)
        {
            Debug.Log("Created " + collectionName);
        }

        private static void HandleCreateCollectionError(string collectionName)
        {
            Debug.LogError("Failed to create collection " + collectionName);
        }

        private static void HandleDeleteAllItemsSuccess(string collectionName, int numDeleted)
        {
            Debug.Log("Deleted " + numDeleted + " from " + collectionName);
        }

        private static void HandleDeleteAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to delete items from collection " + collectionName);
        }

        private static void HandleExportAllItemsSuccess(string collectionName, int numItems)
        {
            Debug.Log("Exported " + numItems + " to " + collectionName);
        }

        private static void HandleExportAllItemsError(string collectionName)
        {
            Debug.LogError("Failed to export items to collection " + collectionName);
        }
    }
}


