﻿using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Class used to call User Export functions in order to create reserved users on the server.
    /// </summary>
    public class UserExportRunner
    {
        private enum ExportState
        {
            WAITING,
            EXPORTING_USERS,
            SETTING_DECKS,
            FINISHED,
            FAILED
        }
        private static ExportState mCurrentExportState;

        public UserExportRunner()
        {
        }
             
        /// <summary>
        /// Method used to create users on the server.
        /// </summary>
        public void ExportUsers()
        {
            mCurrentExportState = ExportState.EXPORTING_USERS;
            ReservedUsersCreator.CreateReservedUsers(SnapshotHandleSuccess, SnapshotHandleError);
        }

        /// <summary>
        /// Returns the current state.
        /// </summary>
        /// <returns>String of current state.</returns>
        public string GetCurrentState()
        {
            return mCurrentExportState.ToString();
        }

        private void SnapshotHandleSuccess()
        {
            mCurrentExportState = ExportState.FINISHED;
        }

        private void SnapshotHandleError(string errorMessage)
        {
            UnityEngine.Debug.LogError(errorMessage);
            mCurrentExportState = ExportState.FAILED;
        }
    }
}