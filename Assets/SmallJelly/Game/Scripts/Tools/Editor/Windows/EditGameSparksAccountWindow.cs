﻿using UnityEngine;
using UnityEditor;

namespace SmallJelly
{
    /// <summary>
    /// Editor window for editing the email and pw of the Google account used for downloading data from the MuonData spreadsheet.
    /// </summary>
    public class EditGameSparksAccountWindow : EditorWindow
    {
    	private static string mEmail = string.Empty;
    	private static string mPw = string.Empty;
        private static string mKey = string.Empty;

    	void OnEnable()
    	{
    		this.titleContent.text = "GameSparks";
    	}
    		
    	public static void Init()
    	{
    		
    		mEmail = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksEmailAddress);
    		mPw = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksPw);
            mKey = PlayerPrefs.GetString(PlayerPrefsKeys.GameSparksKey);

    		var window = EditorWindow.CreateInstance<EditGameSparksAccountWindow>();
    		window.Show();
    	}
    		
    	void OnGUI()
    	{
    		if(GUILayout.Button("Clear"))
    		{
    			Debug.Log("Clear GameSparks account");
    			mEmail = string.Empty;
    			mPw = string.Empty;
                mKey = string.Empty;

    			PlayerPrefs.DeleteKey(PlayerPrefsKeys.GameSparksEmailAddress);
    			PlayerPrefs.DeleteKey(PlayerPrefsKeys.GameSparksPw);
                PlayerPrefs.DeleteKey(PlayerPrefsKeys.GameSparksKey);
    		}

    		string newEmail = string.Empty;
    		string newPw = string.Empty;
            string newKey = string.Empty;

    		newEmail = EditorGUILayout.TextField("Email: ", mEmail);
    		newPw = EditorGUILayout.PasswordField("Pw: ", mPw);
            newKey = EditorGUILayout.PasswordField("Key: ", mKey);

    		if(newEmail != mEmail)
    		{
    			mEmail = newEmail;
    			PlayerPrefs.SetString(PlayerPrefsKeys.GameSparksEmailAddress, mEmail);
    			PlayerPrefs.Save();
    		}

    		if(newPw != mPw)
    		{
    			mPw = newPw;
    			PlayerPrefs.SetString(PlayerPrefsKeys.GameSparksPw, mPw);
    			PlayerPrefs.Save();
    		}

            if(newKey != mKey)
            {
                mKey = newKey;
                PlayerPrefs.SetString(PlayerPrefsKeys.GameSparksKey, mKey);
                PlayerPrefs.Save();
            }
    	}
    }
}