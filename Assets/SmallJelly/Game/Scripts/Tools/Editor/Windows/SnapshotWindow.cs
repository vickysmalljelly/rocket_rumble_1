﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Editor window for performing a complete snapshot copy.
    /// Handles card refresh, news items, snapshot creation and snapshot copy.
    /// </summary>
    public class SnapshotWindow : EditorWindow
    {
        private static ExportOptions Options = new ExportOptions();
        private static string SnapshotName;

        // TODO:
        /*
         * Send messages back from exporter to wizard during exports
         * 
         * ExportTestCards
         * 
         * */

        #region Options classes
        private class ExportOption
        {
            public ExportOption(string name)
            {
                OptionName = name;
            }

            public string OptionName;
            public bool Export;
        }
            
        /// <summary>
        /// Allows the user to specify what data they want to export to the database.
        /// </summary>
        private class ExportOptions
        {
            public ExportOptions()
            {
                Options.Add(new ExportOption("Rewards"));
                Options.Add(new ExportOption("Ships"));
                Options.Add(new ExportOption("Battle Cards"));
                Options.Add(new ExportOption("News"));
                Options.Add(new ExportOption("Replays"));
                Options.Add(new ExportOption("Battle Config"));
                Options.Add(new ExportOption("Ranks"));
                Options.Add(new ExportOption("Feature Flags"));
            }

            public bool IsOptionSet(string optionName)
            {
                foreach(ExportOption option in Options)
                {
                    if(string.Compare(option.OptionName, optionName) == 0)
                    {
                        return option.Export;
                    }
                }

                Debug.LogError("Option " + optionName + " not found");
                return false;
            }

            public List<ExportOption> Options = new List<ExportOption>();
        }
        #endregion

        #region Panel Classes
        private abstract class SnapshotWindowPanel
        {
            public event Action RepaintNeeded;

            public string Title { get; set; }
            public string Description { get; set; }

            public SnapshotWindowPanel(string title, string description)
            {
                Title = title;
                Description = description;
            }

            public virtual bool HasFinished() 
            {
                return true;
            }

            public virtual void Initialise() {}

            public virtual void Update() {}

            public virtual void PanelOnGui() {}

            protected void FireRepaintNeeded()
            {
                if(RepaintNeeded != null)
                {
                    RepaintNeeded();
                }
            }
        }

        private class OptionsPanel : SnapshotWindowPanel
        {
            public OptionsPanel(string title, string description)
                : base(title, description) {}
            
            public override void PanelOnGui()
            {
                base.PanelOnGui();

                foreach(ExportOption option in SnapshotWindow.Options.Options)
                {
                    option.Export = EditorGUILayout.Toggle(option.OptionName, option.Export);
                }
            }
        }

        /// <summary>
        /// Exports json from the project into the database.
        /// </summary>
        private class ExportPanel : SnapshotWindowPanel
        {
            public ExportPanel(string title, string description)
                : base(title, description) {}

            protected bool mHasFinished;
            private Exporter mExporter;

            private List<Exporter> mExporters = new List<Exporter>();
            private List<Exporter>.Enumerator mEnumerator;

            public override void Initialise() 
            {
                if(Options.IsOptionSet("Rewards"))
                {
                    mExporters.Add(RewardsExporter.GetExporter());
                }
                if(Options.IsOptionSet("Ships"))
                {
                    mExporters.Add(ShipCardExporter.GetExporter());
                }
                if(Options.IsOptionSet("Battle Cards"))
                {
                    mExporters.Add(BattleCardExporter.GetExporter());
                }
                if(Options.IsOptionSet("News"))
                {
                    mExporters.Add(NewsItemsExporter.GetExporter());
                }
                if(Options.IsOptionSet("Replays"))
                {
                    Debug.Log("Adding replays to exporter");
                    mExporters.Add(ReplayExporter.GetExporter());
                }
                if(Options.IsOptionSet("Battle Config"))
                {
                    Debug.Log("Adding replays to exporter");
                    mExporters.Add(BattleConfigExporter.GetExporter());
                }
                if(Options.IsOptionSet("Ranks"))
                {
                    Debug.Log("Adding ranks to exporter");
                    mExporters.Add(RankExporter.GetExporter());
                }
                if(Options.IsOptionSet("Feature Flags"))
                {
                    Debug.Log("Adding feature flags to exporter");
                    mExporters.Add(FeatureFlagsExporter.GetExporter());
                }

                if(mExporters.Count == 0)
                {
                    // Nothing to export
                    mHasFinished = true;
                    return;
                }

                mEnumerator = mExporters.GetEnumerator();
                mEnumerator.MoveNext();

                foreach(Exporter exporter in mExporters)
                {
                    exporter.ExportFinished += () => 
                    {
                        Debug.Log("export - finished");

                        if(mEnumerator.MoveNext())
                        {
                            // Start the next export
                            mEnumerator.Current.Run();
                        }
                        else
                        {
                            mHasFinished = true;
                        }

                        FireRepaintNeeded();
                    };
                }
                    
                // Start first run
                mEnumerator.Current.Run();
            }

            public override bool HasFinished()
            {
                return mHasFinished;
            }
        }

        private class SnapshotNamePanel : SnapshotWindowPanel
        {
            public SnapshotNamePanel(string title, string description)
                : base(title, description)
            {}

            public override void Initialise()
            {
                SnapshotName = string.Empty;
            }

            public override bool HasFinished()
            {
                return SnapshotName.Length > 0;
            }

            public override void PanelOnGui()
            {
                base.PanelOnGui();     

                EditorGUILayout.LabelField("Snapshot name:");
                SnapshotWindow.SnapshotName = GUILayout.TextField(SnapshotWindow.SnapshotName);
            }
        }

        private class SnapshotCreatePanel : SnapshotWindowPanel
        {
            private GameSparksSnapshotRunner mSnapshotRunner;

            private bool mHasFinished;

            public SnapshotCreatePanel(string title, string description, SnapshotConfig snapshotConfig)
                : base(title, description)
            {
                mSnapshotRunner = new GameSparksSnapshotRunner(snapshotConfig);
            }

            public override void Initialise()
            {
                mSnapshotRunner.CreateSnapshot(SnapshotWindow.SnapshotName, SuccessHandler, ErrorHandler);
            }

            public override bool HasFinished()
            {
                return mHasFinished;
            }

            public override void PanelOnGui()
            {
                base.PanelOnGui();

                EditorGUILayout.LabelField("Snapshot name:");
                EditorGUILayout.LabelField(SnapshotWindow.SnapshotName);
            }

            private void SuccessHandler()
            {
                mHasFinished = true;

                FireRepaintNeeded();
            }

            private void ErrorHandler()
            {
                Debug.LogError("Failed to create Snapshot");
            }
        }

        private class SnapshotCopyPanel : SnapshotWindowPanel
        {
            private GameSparksSnapshotRunner mSnapshotRunner;

            private bool mHasFinished;

            public SnapshotCopyPanel(string title, string description, SnapshotConfig snapshotConfig)
                : base(title, description)
            {
                mSnapshotRunner = new GameSparksSnapshotRunner(snapshotConfig);
            }

            public override void Initialise()
            {
                mSnapshotRunner.CopySnapshot(SuccessHandler, ErrorHandler);
            }

            public override bool HasFinished()
            {
                return mHasFinished;
            }

            public override void PanelOnGui()
            {
                base.PanelOnGui();

                EditorGUILayout.LabelField("Snapshot name:");
                EditorGUILayout.LabelField(SnapshotWindow.SnapshotName);
            }

            private void SuccessHandler()
            {
                mHasFinished = true;

                FireRepaintNeeded();
            }

            private void ErrorHandler()
            {
                Debug.LogError("Failed to copy Snapshot");
            }
        }

        private class UserCreatePanel : SnapshotWindowPanel
        {
            private UserExportRunner mUserExportRunner;

            public UserCreatePanel(string title, string description, SnapshotConfig snapshotConfig)
                : base(title, description)
            {
                mUserExportRunner = new UserExportRunner();
            }

            public override void Initialise()
            {
                // TODO: add success / error handlers
                mUserExportRunner.ExportUsers();
            }
        }

        private class FinishedPanel : SnapshotWindowPanel
        {
            public FinishedPanel(string title, string description)
                : base(title, description)
            {
            }
        }
        #endregion

        private static List<SnapshotWindowPanel> mPanels;
        private List<SnapshotWindowPanel>.Enumerator mEnumerator;
		private static SnapshotConfig mSnapshotConfig;
        private GUIStyle mTitleStyle = new GUIStyle();  
        private bool mInitialised;

        private void Init()
        {
            mSnapshotConfig = SnapshotConfig.ReadFromFile();

            InitialisePanels();

            mTitleStyle.fontSize = 16;
            mTitleStyle.normal.textColor = Color.blue;

        }
            
        private void OnGUI()
        {
            SnapshotWindowPanel panel = mEnumerator.Current;

            if(panel == null)
            {
                return;
            }

            EditorStyles.label.wordWrap = true;

            GUILayout.FlexibleSpace();

            // Show the title and the description
            GUILayout.Label(panel.Title, mTitleStyle);
            GUILayout.Label(panel.Description);

            // Let the panel render any custom items
            panel.PanelOnGui();

            GUILayout.FlexibleSpace();

            // If the panel has finished, show the next button
            if(panel.HasFinished())
            {
                if (GUILayout.Button("Next"))
                {
                    // Button clicked
                    MoveToNextPanel();
                }
            }
        }

        private void Update()
        {
            if(!mInitialised)
            {
                Init();
                mInitialised = true;
            }
        }     
            
        private void InitialisePanels()
        {
            mPanels = new List<SnapshotWindowPanel>();

            mPanels.Add(new OptionsPanel("Options", "Select data to export"));
            mPanels.Add(new ExportPanel("Export", "Exporting to database"));

            mPanels.Add(new SnapshotNamePanel("Snapshot Name", "Enter a name for the snapshot"));
            mPanels.Add(new SnapshotCreatePanel("Snapshot Create", "Creating the snapshot", mSnapshotConfig));
            mPanels.Add(new SnapshotCopyPanel("Snapshot Copy", "Copying the snapshot to destination environments set in SnapshotConfig", mSnapshotConfig));

            mPanels.Add(new UserCreatePanel("Creating Users", "Click next to create all reserved users on all your environments", mSnapshotConfig));
            mPanels.Add(new FinishedPanel("All done!", "Click finish to close the wizard."));

            // Listen for repaint events
            foreach(SnapshotWindowPanel panel in mPanels)
            {
                panel.RepaintNeeded += () => 
                {
                    Debug.Log("Doing repaint");
                    Repaint();
                };
            }

            // Initialise enumerator
            mEnumerator = mPanels.GetEnumerator();
            // Move to the first panel
            mEnumerator.MoveNext();
        }

        private void MoveToNextPanel()
        {
            if(mEnumerator.MoveNext())
            {
                mEnumerator.Current.Initialise();
            }
            else
            {
                // Close the wizard
                Close();
            }
        }
    }
}