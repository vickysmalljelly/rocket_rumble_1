﻿using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using SmallJelly.Framework;
using System.Collections.Generic;
using SmallJelly;
using System.Collections;

public class ReflectionViewer : EditorWindow
{
	#region Initialization
	// Add menu named "My Window" to the Window menu
	[MenuItem ("Small Jelly/Unity Tools/Stack Viewer")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		ReflectionViewer window = (ReflectionViewer)EditorWindow.GetWindow (typeof (ReflectionViewer));
		window.Show();
	}
	#endregion

	private Vector2 mScrollPosition;
	private string mAllowedNamespace = "System";

	void OnGUI () 
	{		
		GameObject activeGameObject = Selection.activeGameObject;

		if( activeGameObject != null )
		{
			GUILayout.Label("Reflection Viewer " + activeGameObject.name);

			mScrollPosition = GUILayout.BeginScrollView( mScrollPosition );

			mAllowedNamespace = GUILayout.TextField( mAllowedNamespace );

			SJMonoBehaviour[] monoBehaviours = activeGameObject.GetComponents<SJMonoBehaviour>();
			foreach( SJMonoBehaviour sjMonoBehaviour in monoBehaviours )
			{
				PrintMonoBehaviour( sjMonoBehaviour );
				GUILayout.Space(30);
			}
			GUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Label("Please select an object in the heirarchy");
		}
	}

	private void PrintMonoBehaviour(SJMonoBehaviour monoBehaviour)
	{
		GUILayout.Label (monoBehaviour.GetType().ToString(), EditorStyles.boldLabel);

		Type t = monoBehaviour.GetType();

		GUILayout.Label("Properties");
		foreach (PropertyInfo info in t.GetProperties())
		{
			if( !info.PropertyType.Namespace.Contains( mAllowedNamespace ) )
				continue;

			object value = info.GetValue( monoBehaviour, null );
			string valueString = value == null ? "null" : value.ToString();

			//TODO - Take out this special case crap
			if( monoBehaviour is BattleEntityController && info.Name == "PreviousStates" )
			{
				valueString = CustomStackString<BattleEntityController.State>( (Stack<BattleEntityController.State>)info.GetValue( monoBehaviour, null ) );
			}
			GUILayout.TextArea( string.Format("{0} {1} : {2}", info.PropertyType, info.Name, valueString ) );
		}

		GUILayout.Label("Fields");
		foreach( FieldInfo info in t.GetFields( BindingFlags.Instance | BindingFlags.Static |BindingFlags.NonPublic |BindingFlags.Public) )
		{
			if( !info.FieldType.Namespace.Contains( mAllowedNamespace ) )
				continue;

			object value = info.GetValue( monoBehaviour );
			string valueString = value == null ? "null" : value.ToString();
			GUILayout.TextArea( string.Format("{0} {1} : {2}", info.FieldType, info.Name, valueString ) );
		}
	}

	//TODO: Make a generic system for custom output types
	private string CustomStackString<T>( Stack<T> stack )
	{
		T[] array = stack.ToArray();

		string stackString = string.Empty;
		foreach( T t in array )
		{
			stackString = string.Format( "{0}{1}{2}", stackString, t.ToString(), "/n" );
		}
		return stackString;
	}

}