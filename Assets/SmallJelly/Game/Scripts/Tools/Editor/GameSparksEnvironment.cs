﻿using System;

namespace SmallJelly
{
    /// <summary>
    /// Encapsulates information identifying a GameSparks environment (a GameSparks "game").
    /// </summary>
    [Serializable]
    public class GameSparksEnvironment
    {
        /// <summary>
        /// The name of the environment
        /// </summary>
        public string Name;

        /// <summary>
        /// The key for the environment
        /// </summary>
        public string Key;

        /// <summary>
        /// The secret for the callback to register the reserved users.
        /// Find in the GameSparks console under Configurator->Integrations->Credentials
        /// </summary>
        public string ReservedUsersRegistrationCallbackSecret;

        /// <summary>
        /// Copy the setup of your game
        /// </summary>
		public bool IncludeGameConfig;

        /// <summary>
        /// Copy the contents of the meta data collections along with the snapshot.
        /// </summary>
		public bool IncludeMetadata;

        /// <summary>
        /// The include binaries.
        /// </summary>
		public bool IncludeBinaries;

        /// <summary>
        /// Include the collaborators that have access to the project.
        /// </summary>
        public bool IncludeCollaborators;
    }
}