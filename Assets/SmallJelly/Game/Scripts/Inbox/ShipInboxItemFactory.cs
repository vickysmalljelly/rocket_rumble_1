﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the ship collection items
	public class ShipInboxItemFactory : CardFactory
	{
		#region Public Methods
		public static ShipInboxItem ConstructInboxItem( ShipCardData shipCardData )
		{
			GameObject view = LoadCardView( CardType.Ship );
			view.name = string.Format( "Card{0}", shipCardData.DisplayName );

			AssignComponents (view, shipCardData );

			ShipInboxItem shipInboxItem = view.GetComponent< ShipInboxItem > ();
			shipInboxItem.OnRefreshData( shipCardData );
			shipInboxItem.OnInsertState( ShipInboxItemController.State.Initialisation );
			shipInboxItem.OnInsertState( ShipInboxItemController.State.Hiding );

			return shipInboxItem;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, ShipCardData shipCardData )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the collection entity (the collection entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationController( view );
			AssignView( view );
			AssignController( view );
			AssignInboxItemComponent( view, shipCardData);

			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< InboxItemAnimationController > ();
		}

		private static void AssignView( GameObject view )
		{
			ShipInboxItemView shipInboxItemView = view.AddComponent< ShipInboxItemView > ();
			shipInboxItemView.Parts = view.GetComponent< ShipCardParts > ();
		}

		private static void AssignController( GameObject view )
		{
			ShipInboxItemController collectionEntityController = view.AddComponent< ShipInboxItemController > ();
			collectionEntityController.View = view.GetComponent< ShipInboxItemView >();
		}

		private static void AssignInboxItemComponent( GameObject view, ShipCardData shipCardData )
		{
			//Add battle entity component
			ShipInboxItem collectionEntity = view.AddComponent< ShipInboxItem >();
			collectionEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			collectionEntity.InboxItemController = view.GetComponent< ShipInboxItemController >();
		}
		#endregion
	}
}
