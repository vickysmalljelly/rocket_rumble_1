﻿using UnityEngine;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// Manages a player's inbox
    /// </summary>
    public class InboxManager : MonoBehaviourSingleton<InboxManager>
    {
		#region Public Properties
		public InboxMenuController InboxMenuController
		{
			get
			{
				return mInboxMenuController;
			}
		}
		#endregion

		#region Public Event Handlers
		public event Action InboxConnectionFailed;

		public event EventHandler< InboxEventDataEventArgs > InboxEventDataReceived;

		//TODO - We need to take a second look at inbox server error handling but this will be sufficient for now
		public event EventHandler< StringEventArgs > InboxResponseFailure;
		#endregion

		#region Member Variables
		private InboxMenuController mInboxMenuController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake ();

			mInboxMenuController = UIManager.Get.GetMenuController< InboxMenuController >();
		}
		#endregion

		#region Public Methods
		public void ListInbox()
		{
			ClientInbox.ListInbox( HandleListInboxSuccess, HandleServerError );
		}

		public void ConsumeItem( string uniqueItem )
		{
			ClientInbox.ConsumeItem( uniqueItem, HandleConsumeItemSuccess,  HandleServerError );
		}
		#endregion

		#region Event Firing
		private void FireInboxEventDataReceived( InboxEventData inboxEventData )
		{
			if( InboxEventDataReceived != null )
			{
				InboxEventDataReceived( this, new InboxEventDataEventArgs( inboxEventData ) );
			}
		}

		private void FireInboxResponseFailure( string text )
		{
			if( InboxResponseFailure != null  )
			{
				InboxResponseFailure( this, new StringEventArgs( text ) );
			}
		}

		private void FireInboxConnectionFailed()
		{
			if( InboxConnectionFailed != null )
			{
				InboxConnectionFailed();
			}
		}
		#endregion

		#region Event Handlers
		private void HandleListInboxSuccess( List< InboxItemData > inboxItemData )
		{	
			FireInboxEventDataReceived( new ListItemsInboxEventData( inboxItemData ) );
		}

		private void HandleConsumeItemSuccess( RewardData rewardData )
		{	
			FireInboxEventDataReceived( new ConsumeItemInboxEventData( rewardData ) );

            if(rewardData.NewCards)
            {
                GameManager.Get.UpdateNewCardsOnClient();
            }
		}

		private void HandleServerError( ServerError error )
		{
			switch(error.ErrorType)
			{
				case ServerErrorType.Timeout:
					FireInboxConnectionFailed();
					break;

				default:
					FireInboxResponseFailure( error.UserFacingMessage ); 
					break;
				}
		}
		#endregion
    }
}