﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the ship collection items
	public class ResourceInboxItemFactory : CardFactory
	{
		#region Public Methods
		public static ResourceInboxItem ConstructInboxItem( string resourceName, int amount )
		{
			GameObject view;

            if(resourceName == "Crystals")
            {
                // Temporary hack to get crystals working
                resourceName = RewardData.FLUOREX_CRYSTALS;
            }

			CardType cardType = resourceName == RewardData.CREDITS ? CardType.Currency : CardType.Resource;
			view = LoadCardView( cardType );
			view.name = string.Format( "Card{0}", resourceName );

			AssignComponents (view );

			ResourceInboxItem shipInboxItem = view.GetComponent< ResourceInboxItem > ();
			shipInboxItem.OnRefreshData( resourceName, amount );
			shipInboxItem.OnInsertState( ResourceInboxItemController.State.Initialisation );
			shipInboxItem.OnInsertState( ResourceInboxItemController.State.Hiding );

			return shipInboxItem;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the collection entity (the collection entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationController( view );
			AssignView( view );
			AssignController( view );
			AssignInboxItemComponent( view );

			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< InboxItemAnimationController > ();
		}

		private static void AssignView( GameObject view )
		{
			ResourceInboxItemView shipInboxItemView = view.AddComponent< ResourceInboxItemView > ();
			shipInboxItemView.Parts = view.GetComponent< ResourceCardParts > ();
		}

		private static void AssignController( GameObject view )
		{
			ResourceInboxItemController collectionEntityController = view.AddComponent< ResourceInboxItemController > ();
			collectionEntityController.View = view.GetComponent< ResourceInboxItemView >();
		}

		private static void AssignInboxItemComponent( GameObject view )
		{
			//Add battle entity component
			ResourceInboxItem collectionEntity = view.AddComponent< ResourceInboxItem >();
			collectionEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			collectionEntity.InboxItemController = view.GetComponent< ResourceInboxItemController >();
		}
		#endregion
	}
}
