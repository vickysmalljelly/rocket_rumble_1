﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class InboxItem : Targetable
	{
		#region Public Events
		public event EventHandler< InboxItemAndStateTransitionEventArgs > StateTransitioned;

		public event EventHandler AnimationFinished
		{
			add
			{
				InboxItemController.AnimationFinished += value;
			}

			remove
			{
				InboxItemController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Public Properties
		public InboxItemController InboxItemController
		{
			get
			{
				return (InboxItemController)TargetableLogicController;
			}

			set
			{
				TargetableLogicController = value;
			}
		}

		public ObjectInputListener ObjectInputListener { get; set; }
		#endregion

		#region Unity Methods
		private void OnEnable()
		{
			RegisterListeners();
			RegisterInputListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
			UnregisterInputListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState(  InboxItemController.State state )
		{
			InboxItemController.OnInsertState( state );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			InboxItemController.InboxItemStateUpdated += HandleStateUpdated;
		}

		private void UnregisterListeners()
		{
			InboxItemController.InboxItemStateUpdated -= HandleStateUpdated;
		}

		private void RegisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver += InboxItemController.HandleMouseRolledOver;
			ObjectInputListener.MouseHovered += InboxItemController.HandleMouseHovered;
			ObjectInputListener.MouseRolledOff += InboxItemController.HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver += InboxItemController.HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged += InboxItemController.HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff += InboxItemController.HandleMouseDraggedOff;

			ObjectInputListener.MousePressed += InboxItemController.HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver += InboxItemController.HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff += InboxItemController.HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver += InboxItemController.HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged += InboxItemController.HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff += InboxItemController.HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed += InboxItemController.HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver += InboxItemController.HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff += InboxItemController.HandleTouchReleasedOff;
		}

		private void UnregisterInputListeners()
		{
			//Mouse
			ObjectInputListener.MouseRolledOver -= InboxItemController.HandleMouseRolledOver;
			ObjectInputListener.MouseHovered -= InboxItemController.HandleMouseHovered;
			ObjectInputListener.MouseRolledOff -= InboxItemController.HandleMouseRolledOff;

			ObjectInputListener.MouseDraggedOver -= InboxItemController.HandleMouseDraggedOver;
			ObjectInputListener.MouseDragged -= InboxItemController.HandleMouseDragged;
			ObjectInputListener.MouseDraggedOff -= InboxItemController.HandleMouseDraggedOff;

			ObjectInputListener.MousePressed -= InboxItemController.HandleMousePressedOver;
			ObjectInputListener.MouseReleasedOver -= InboxItemController.HandleMouseReleasedOver;
			ObjectInputListener.MouseReleasedOff -= InboxItemController.HandleMouseReleasedOff;


			//Touch
			ObjectInputListener.TouchDraggedOver -= InboxItemController.HandleTouchDraggedOver;
			ObjectInputListener.TouchDragged -= InboxItemController.HandleTouchDragged;
			ObjectInputListener.TouchDraggedOff -= InboxItemController.HandleTouchDraggedOff;

			ObjectInputListener.TouchPressed -= InboxItemController.HandleTouchPressedOver;
			ObjectInputListener.TouchReleasedOver -= InboxItemController.HandleTouchReleasedOver;
			ObjectInputListener.TouchReleasedOff -= InboxItemController.HandleTouchReleasedOff;
		}
		#endregion

		#region Event Firing
		private void FireStateTransitioned( InboxItem collectionItem, InboxItemController.State previousState, InboxItemController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			if( StateTransitioned != null )
			{
				StateTransitioned( this, new InboxItemAndStateTransitionEventArgs(collectionItem, previousState, newState, hitLayers) );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateUpdated( object o, BattleEntityInboxItemStateTransitionEventArgs battleEntityStateEventArgs )
		{
			FireStateTransitioned( this, battleEntityStateEventArgs.PreviousState, battleEntityStateEventArgs.NewState, battleEntityStateEventArgs.EntityHitLayers );
		}
		#endregion

	}
}