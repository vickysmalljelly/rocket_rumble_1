﻿using UnityEngine;
using System.Collections;
using System;

namespace SmallJelly
{
	public class BattleEntityInboxItemController : InboxItemController
	{
		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			( ( BattleEntityInboxItemView ) View ).OnRefreshData( battleEntityData );
		}
		#endregion

	}
}