﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class BattleEntityInboxItemView : InboxItemView
	{
		#region Public Properties
		public TemplateBattleEntityModel Template
		{ 
			get;
			set;
		}

		public TemplateBattleEntityParts TemplateBattleEntityParts
		{ 
			get;
			set;
		}

		public CardParts Parts
		{
			get
			{
				return Template.CardParts;
			}
		}
		#endregion

		#region MonoBehaviour Methods
		private void OnDestroy()
		{
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( BattleEntityData battleEntityData )
		{
			Template.OnRefresh( battleEntityData );
			Parts.OnRefresh( battleEntityData );

			EnableParts();
		}
		#endregion

		#region Private Methods
		private void EnableParts()
		{
			SetActiveRecursively( Parts.Card, true );
			SetActiveRecursively( Parts.Component, true );
			SetActiveRecursively( Parts.ComponentHousing, true );
			SetActiveRecursively( Parts.Mask, true );
			SetActiveRecursively( Parts.BasePlate, false );
		}

		private void RefreshWeapon( WeaponData weaponData )
		{
			//Refresh the card
			Parts.HP.text = string.Format( "{0}", weaponData.HP );
			Parts.Damage.text = string.Format( "{0}", weaponData.Attack );
		}

		private void RefreshCrew( CrewData crewData ){}

		private void RefreshUtility( UtilityData utilityData )
		{
			Parts.HP.text = string.Format( "{0}", utilityData.HP );
		}

		private void RefreshVirus( VirusData virusData ){}

		private void RefreshUnknown( UnknownData unknownData ){}
		#endregion
	}
}