﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using HutongGames.PlayMaker;
using System;

namespace SmallJelly
{
	public class InboxItemController : TargetableLogicController
	{
		#region Enums
		public enum State
		{
			None,
			Initialisation,
			Hiding,
			Reveal,
			Showing
		}
		#endregion

		#region Exposed To Inspector
		//Display in the inspector
		[SerializeField]
		private State mState;
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished;

		public event EventHandler< BattleEntityInboxItemStateTransitionEventArgs > InboxItemStateUpdated;
		#endregion

		#region Public Properties
		public InboxItemView View
		{
			protected get;
			set;
		}
		#endregion

		#region Unity Methods
		public void OnEnable()
		{
			RegisterListeners();
		}

		public void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new System.Collections.Generic.Dictionary<string, NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			UpdateState( state, animationMetaData );
		}

		public override void OnMousePressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch( mState )
			{
				//Player has clicked the component
				case State.Hiding:
					StartRevealing();
					break;
			}
		}

		public override void OnMouseReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch( mState )
			{
			//Player has clicked the component
			case State.Hiding:
				StartRevealing();
				break;
			}
		}
		public override void OnMouseReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseRolledOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseHovered( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseRolledOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnMouseDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnMouseDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnTouchDraggedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDragged( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		public override void OnTouchDraggedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnTouchPressedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}

		public override void OnTouchReleasedOver( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame )
		{
			switch( mState )
			{
			//Player has clicked the component
			case State.Hiding:
				StartRevealing();
				break;
			}
		}

		public override void OnTouchReleasedOff( SJRenderRaycastHit[] elementsHit, Vector2 movementDuringFrame ){}
		#endregion

		#region Private Methods
		protected void UpdateState( State newBattleEntityState, AnimationMetaData animationMetaData )
		{
			State previousState = mState;
			mState = newBattleEntityState;

			View.OnPlayAnimation( newBattleEntityState, animationMetaData );

			FireStateUpdated( previousState, newBattleEntityState, LayersHitBehindEntityThisFrame );
		}

		private void StartRevealing()
		{
			OnInsertState( State.Reveal );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			View.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			View.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			//When showing animation has finished, switch to idle state
			switch( mState )
			{
				case State.Reveal:
					OnInsertState( State.Showing );
					break;
			}

			FireAnimationFinished();
		}
		#endregion

		#region Event Firing
		private void FireAnimationFinished()
		{
			if( AnimationFinished != null )
			{
				AnimationFinished( this, EventArgs.Empty );
			}
		}
		#endregion


		#region Event Firing
		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState, SJRenderRaycastHit[] layersHitBehindEntity )
		{
			if( InboxItemStateUpdated != null )
			{
				InboxItemStateUpdated( this, new BattleEntityInboxItemStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState, layersHitBehindEntity ) );
			}
		}
		#endregion
	}
}