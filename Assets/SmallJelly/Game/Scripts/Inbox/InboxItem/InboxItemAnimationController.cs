﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class InboxItemAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Inbox/InboxItem/"; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string REVEAL_ANIMATION_TEMPLATE_NAME = "RevealAnimation";
		#endregion

		#region Public Methods
		public void UpdateAnimations( InboxItemController.State cardState, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			switch(cardState)
			{
				case InboxItemController.State.Initialisation:
					PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case InboxItemController.State.Reveal:
					PlayAnimation( REVEAL_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}