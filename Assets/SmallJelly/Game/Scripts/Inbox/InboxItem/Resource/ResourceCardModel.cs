﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.IO;

namespace SmallJelly
{
	public class ResourceCardModel : SJMonoBehaviour 
	{

		#region Exposed To Inspector
		[SerializeField]
		private Transform mComponentPosition;
		#endregion

		#region Member Variables
		private string mLoadedComponentName;
		private GameObject mComponentModel;
		#endregion

		#region Public Methods
		public virtual void OnRefreshData(string resourceName, int amount )
		{
			string componentName = string.Format( "{0}", resourceName );

			//The model does not need to change - return!
			if( componentName == mLoadedComponentName )
			{
				return;
			}
				
			mLoadedComponentName = componentName;

			//A model already exists so let's remove it
			if( mComponentModel != null )
			{
				Destroy( mComponentModel );
			}

			GameObject componentModelResource = GetResourceResource( resourceName );
			mComponentModel = CreateModel( componentModelResource );
		}
		#endregion

		#region Private Methods
		private static GameObject GetResourceResource( string resourceName )
		{
			//TODO - This method is only temporary - I dont want to hardcode resource stuff - we need a proper resource system

			string prefabName;
			switch( resourceName )
			{
			case RewardData.XELION_GAS: 
				prefabName = "res_xelionGas";
				break;

			case RewardData.FLUOREX_CRYSTALS: 
				prefabName = "res_fluorexCrystals";
				break;

			case RewardData.AZOMITE: 
				prefabName = "res_azomite";
				break;

			case RewardData.CREDITS: 
				prefabName = "res_currency";
				break;

			default:
				SJLogger.LogError("Resource class {0} not recognised", resourceName);
				prefabName = "res_rareCircuitboard";
				break;
			}


			return Resources.Load< GameObject >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyResourcePrefabs, Path.DirectorySeparatorChar, prefabName ) );
		}


		private GameObject CreateModel( GameObject componentModelResource )
		{
			GameObject componentModel = Instantiate( componentModelResource ); 
			SJRenderTransformExtensions.AddChild( mComponentPosition, componentModel.transform, true, true, true );
			return componentModel;
		}
		#endregion
	}
}