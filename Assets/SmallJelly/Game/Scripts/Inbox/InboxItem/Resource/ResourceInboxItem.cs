﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ResourceInboxItem : InboxItem
	{
		#region Public Methods
		public void OnRefreshData( string resourceName, int amount )
		{
			ResourceInboxItemController shipInboxItemController = ( ResourceInboxItemController )InboxItemController;
			shipInboxItemController.OnRefreshData( resourceName, amount );
		}
		#endregion

	}
}