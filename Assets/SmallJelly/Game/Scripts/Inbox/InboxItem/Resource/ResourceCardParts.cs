﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;

namespace SmallJelly
{
	public class ResourceCardParts : SJMonoBehaviour
	{
		#region Exposed To The Inspector
		public TextMeshPro ResourceName;
		public TextMeshPro ResourceAmount;

		public ResourceCardModel ResourceCardModel;
		#endregion
	}
}