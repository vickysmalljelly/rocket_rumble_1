﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ResourceInboxItemController : InboxItemController
	{
		#region Public Methods
		public void OnRefreshData( string resourceName, int amount )
		{
			( ( ResourceInboxItemView ) View ).OnRefreshData( resourceName, amount );
		}
		#endregion
	}
}