﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ResourceInboxItemView : InboxItemView
	{
		#region Public Properties
		public ResourceCardParts Parts { get; set; }
		#endregion

		#region MonoBehaviour Methods
		protected override void OnEnable()
		{
			base.OnEnable();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( string resourceName, int amount )
		{
			Parts.ResourceName.text = resourceName;
			Parts.ResourceAmount.text = string.Format( "{0}", amount );

			Parts.ResourceCardModel.OnRefreshData( resourceName, amount );
		}
		#endregion
	}
}