﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShipInboxItemController : InboxItemController
	{
		#region Public Methods
		public void OnRefreshData( ShipCardData shipCardData )
		{
			( ( ShipInboxItemView ) View ).OnRefreshData( shipCardData );
		}
		#endregion
	}
}