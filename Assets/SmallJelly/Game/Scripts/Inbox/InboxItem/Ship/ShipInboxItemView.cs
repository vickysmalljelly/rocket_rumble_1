﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShipInboxItemView : InboxItemView
	{
		#region Public Properties
		public ShipCardParts Parts { get; set; }
		#endregion

		#region MonoBehaviour Methods
		protected override void OnEnable()
		{
			base.OnEnable();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
		}
		#endregion

		#region Public Methods
		public void OnRefreshData( ShipCardData shipCardData )
		{
			Parts.ShipName.text = shipCardData.DisplayName;
			Parts.ShipClass.text = shipCardData.Class;

			Parts.ShipCardModel.OnRefreshData( shipCardData );
		}
		#endregion
	}
}