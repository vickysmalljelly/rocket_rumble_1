﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.IO;

namespace SmallJelly
{
	public class ShipCardModel : SJMonoBehaviour 
	{

		#region Exposed To Inspector
		[SerializeField]
		private Transform mComponentPosition;
		#endregion

		#region Member Variables
		private string mLoadedComponentName;
		private GameObject mComponentModel;
		#endregion

		#region Public Methods
		public virtual void OnRefreshData( ShipCardData shipCardData )
		{
			string componentName = string.Format( "{0}", shipCardData.Id );

			//The model does not need to change - return!
			if( componentName == mLoadedComponentName )
			{
				return;
			}
				
			mLoadedComponentName = componentName;

			//A model already exists so let's remove it
			if( mComponentModel != null )
			{
				Destroy( mComponentModel );
			}

			GameObject componentModelResource = GetShipResource( shipCardData.Class );
			mComponentModel = CreateModel( componentModelResource );
		}
		#endregion

		#region Private Methods
		private static GameObject GetShipResource( string shipClass )
		{
			//TODO - This method is only temporary - ships won't be hardcoded but will be loaded from the data
			//once we have multiple ships within each class (different skins etc)
			string shipName;
			switch( shipClass )
			{
			case ShipClass.Ancient: 
				shipName = "ship_anc_tenebrae_preview";
				break;

			case ShipClass.Enforcer: 
				shipName = "ship_enf_prowler_preview";
				break;

			case ShipClass.Smuggler: 
				shipName = "ship_smu_jackrabbit_preview";
				break;

			case ShipClass.Debug: 
				shipName = "ship_anc_tenebrae_preview";
				break;

			default:
				SJLogger.LogError("Ship class {0} not recognised", shipClass);
				shipName = "ship_anc_tenebrae";
				break;
			}


			return Resources.Load< GameObject >( string.Format( "{0}{1}{2}", FileLocations.SmallJellyShipPrefabs, Path.DirectorySeparatorChar, shipName ) );
		}


		private GameObject CreateModel( GameObject componentModelResource )
		{
			GameObject componentModel = Instantiate( componentModelResource ); 
			SJRenderTransformExtensions.AddChild( mComponentPosition, componentModel.transform, true, true, true );
			return componentModel;
		}
		#endregion
	}
}