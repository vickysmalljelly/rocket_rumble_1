﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class ShipInboxItem : InboxItem
	{
		#region Public Properties
		//The data of the card right now in the flow - used for stat displays etc
		public ShipCardData Data
		{
			get 
			{
				return mData;
			}
		}
		#endregion

		#region Member Variables
		private ShipCardData mData;
		#endregion

		#region Public Methods
		public void OnRefreshData( ShipCardData shipCardData )
		{
			mData = shipCardData;
			ShipInboxItemController shipInboxItemController = ( ShipInboxItemController )InboxItemController;
			shipInboxItemController.OnRefreshData( shipCardData );
		}
		#endregion

	}
}