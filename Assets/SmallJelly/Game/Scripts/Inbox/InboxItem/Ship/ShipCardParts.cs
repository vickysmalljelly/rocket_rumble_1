﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using TMPro;

namespace SmallJelly
{
	public class ShipCardParts : SJMonoBehaviour
	{
		#region Exposed To The Inspector
		public TextMeshPro ShipName;
		public TextMeshPro ShipClass;

		public ShipCardModel ShipCardModel;
		#endregion
	}
}