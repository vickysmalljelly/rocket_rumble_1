﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	//A factory class, used to construct the battle entities (cards/components)
	public class BattleEntityInboxItemFactory : CardFactory
	{
		#region Public Methods
		public static BattleEntityInboxItem ConstructInboxItem( BattleEntityData battleEntityData )
		{
			GameObject view = LoadCardView( CardType.BattleEntity );
			view.name = string.Format( "Card{0}", battleEntityData.DrawNumber );

			AssignComponents (view, battleEntityData );

			BattleEntityInboxItem battleEntityInboxItem = view.GetComponent< BattleEntityInboxItem > ();
			battleEntityInboxItem.OnRefreshData( battleEntityData );
			battleEntityInboxItem.OnInsertState( BattleEntityInboxItemController.State.Initialisation );
			battleEntityInboxItem.OnInsertState( BattleEntityInboxItemController.State.Hiding );

			return battleEntityInboxItem;
		}
		#endregion

		#region Private Methods
		private static void AssignComponents( GameObject view, BattleEntityData battleEntityData )
		{
			//We work top to bottom - first we assign the animation controllers, then we assign the views (the views handle the animation
			//controllers) and then we assign the collection entity (the collection entity handles the views)

			view.gameObject.SetActive ( false );

			AssignAnimationController( view );
			AssignView(view);
			AssignController( view );
			AssignInboxItemComponent( view, battleEntityData);

			view.gameObject.SetActive ( true );
		}

		private static void AssignAnimationController( GameObject view )
		{
			view.AddComponent< InboxItemAnimationController > ();
		}

		private static void AssignView( GameObject view )
		{
			BattleEntityInboxItemView collectionEntityView = view.AddComponent< BattleEntityInboxItemView > ();
			collectionEntityView.Template = view.GetComponent< TemplateBattleEntityModel >();
			collectionEntityView.TemplateBattleEntityParts = view.GetComponent< TemplateBattleEntityParts >();
		}

		private static void AssignController( GameObject view )
		{
			BattleEntityInboxItemController collectionEntityController = view.AddComponent< BattleEntityInboxItemController > ();
			collectionEntityController.View = view.GetComponent< BattleEntityInboxItemView >();
		}

		private static void AssignInboxItemComponent( GameObject view, BattleEntityData battleEntityData )
		{
			//Add battle entity component
			BattleEntityInboxItem collectionEntity = view.AddComponent< BattleEntityInboxItem >();
			collectionEntity.ObjectInputListener = view.GetComponent< PlayerObjectInputListener > ();
			collectionEntity.InboxItemController = view.GetComponent< BattleEntityInboxItemController >();
		}
		#endregion
	}
}
