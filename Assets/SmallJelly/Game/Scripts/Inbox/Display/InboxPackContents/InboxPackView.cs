﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of the deck list panel (see DeckListController.cs)
	/// </summary>
	public class InboxPackView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler< InboxItemAndStateTransitionEventArgs > InboxItemStateTransitioned;

		public event EventHandler AnimationFinished
		{
			add
			{
				mInboxPackAnimationController.AnimationFinished += value;
			}

			remove
			{
				mInboxPackAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private Transform[] mCardParents;

		[SerializeField]
		private InboxPackAnimationController mInboxPackAnimationController;
		#endregion

		#region Member Variables
		private List< InboxItem > mInboxItems;
		#endregion

		#region Public Methods
		protected override void Awake()
		{
			base.Awake();

			mInboxItems = new List< InboxItem >();
		}

		public void Refresh( RewardData rewardData )
		{
			UnregisterListeners();

            int numCards = 0;

			foreach( BattleEntityData battleEntityData in rewardData.BattleEntityCards )
			{
				mInboxItems.Add( BattleEntityInboxItemFactory.ConstructInboxItem( battleEntityData ) );
                numCards++;

				int index = mInboxItems.Count - 1;
				SJRenderTransformExtensions.AddChild( mCardParents[ index ], mInboxItems[ index ].transform, true, true, true );
			}


			foreach( ShipCardData shipCardData in rewardData.ShipCards )
			{
				mInboxItems.Add( ShipInboxItemFactory.ConstructInboxItem( shipCardData ) );

				int index = mInboxItems.Count - 1;
				SJRenderTransformExtensions.AddChild( mCardParents[ index ], mInboxItems[ index ].transform, true, true, true );
			}

			foreach( KeyValuePair< string, int > resource in rewardData.Resources )
			{
				mInboxItems.Add( ResourceInboxItemFactory.ConstructInboxItem( resource.Key, resource.Value ) );

				int index = mInboxItems.Count - 1;
				SJRenderTransformExtensions.AddChild( mCardParents[ index ], mInboxItems[ index ].transform, true, true, true );
			}

            foreach(int amount in rewardData.Credits)
            {
                mInboxItems.Add( ResourceInboxItemFactory.ConstructInboxItem( "Credits", amount ) );
                numCards++;

                int index = mInboxItems.Count - 1;
                SJRenderTransformExtensions.AddChild( mCardParents[ index ], mInboxItems[ index ].transform, true, true, true );
            }

            if( rewardData.Crystals > 0 )
            {
                mInboxItems.Add( ResourceInboxItemFactory.ConstructInboxItem( "Crystals", rewardData.Crystals ) );

                int index = mInboxItems.Count - 1;
                SJRenderTransformExtensions.AddChild( mCardParents[ index ], mInboxItems[ index ].transform, true, true, true );
            }

			RegisterListeners();

            mInboxPackAnimationController.SetNumberOfCards(numCards);
		}

		public void OnPlayAnimation( InboxPackController.State state, AnimationMetaData animationMetaData )
		{
			mInboxPackAnimationController.UpdateAnimations( state, animationMetaData );
		}

		public void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			foreach( InboxItem inboxItem in mInboxItems )
			{
				inboxItem.StateTransitioned += HandleStateTransitioned;
			}
		}

		private void UnregisterListeners()
		{
			foreach( InboxItem inboxItem in mInboxItems )
			{
				inboxItem.StateTransitioned -= HandleStateTransitioned;
			}
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, InboxItemAndStateTransitionEventArgs inboxItemAndStateTransitionEventArgs )
		{
			FireInboxItemStateTransitioned( inboxItemAndStateTransitionEventArgs.InboxItem, inboxItemAndStateTransitionEventArgs.PreviousState, inboxItemAndStateTransitionEventArgs.NewState, inboxItemAndStateTransitionEventArgs.EntityHitLayers );
		}
		#endregion

		#region Event Firing
		private void FireInboxItemStateTransitioned(InboxItem inboxItem,  InboxItemController.State previousState,  InboxItemController.State newState, SJRenderRaycastHit[] entityHitLayers)
		{
			if( InboxItemStateTransitioned != null )
			{
				InboxItemStateTransitioned( this, new InboxItemAndStateTransitionEventArgs( inboxItem, previousState, newState, entityHitLayers ) );
			}
		}
		#endregion
	}
}