﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	/// <summary>
	/// Menu class which controls the display and selection of the players inbox items
	/// </summary>
	public class InboxPack : MenuController
	{
		#region Public Event Handlers
		public event EventHandler< InboxItemAndStateTransitionEventArgs > InboxItemStateTransitioned
		{
			add
			{
				mInboxPackController.InboxItemStateTransitioned += value;
			}

			remove
			{
				mInboxPackController.InboxItemStateTransitioned -= value;
			}
		}

		public event EventHandler< InboxPackStateTransitionEventArgs > InboxPackStateUpdated
		{
			add
			{
				mInboxPackController.InboxPackStateUpdated += value;
			}

			remove
			{
				mInboxPackController.InboxPackStateUpdated -= value;
			}
		}

		public event EventHandler AnimationFinished
		{
			add
			{
				mInboxPackController.AnimationFinished += value;
			}

			remove
			{
				mInboxPackController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private InboxPackController mInboxPackController;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();

			OnInsertState( InboxPackController.State.Initialisation );
		}
		#endregion

		#region Public Methods
		public void Refresh( RewardData rewardData )
		{
			mInboxPackController.Refresh( rewardData );
		}

		public void OnInsertState( InboxPackController.State state )
		{
			mInboxPackController.OnInsertState( state );
		}
		#endregion
	}
}