﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	/// <summary>
	/// Menu class which controls the display and selection of the inbox items
	/// </summary>
	public class InboxPackController : SJMonoBehaviour
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,
			Initialisation,
			MovingIn,
			Opening,
			ConfirmCompletion,
			MovingOut
		}
		#endregion

		#region Public Event Handlers
		public event EventHandler< InboxItemAndStateTransitionEventArgs > InboxItemStateTransitioned
		{
			add
			{
				mInboxPackView.InboxItemStateTransitioned += value;
			}

			remove
			{
				mInboxPackView.InboxItemStateTransitioned -= value;
			}
		}

		public event EventHandler< InboxPackStateTransitionEventArgs > InboxPackStateUpdated;

		public event EventHandler AnimationFinished
		{
			add
			{
				mInboxPackView.AnimationFinished += value;
			}

			remove
			{
				mInboxPackView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private InboxPackView mInboxPackView;
		#endregion

		#region Member Variables
		private State mState;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();
		}

		public void OnEnable()
		{
			RegisterListeners();
		}

		public void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void Refresh( RewardData rewardData )
		{
			mInboxPackView.Refresh( rewardData );
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			UpdateState( state, animationMetaData );
		}
		#endregion

		#region Private Methods
		private void UpdateState( State newBattleEntityState, AnimationMetaData animationMetaData )
		{
			State previousState = mState;
			mState = newBattleEntityState;

			mInboxPackView.OnPlayAnimation( newBattleEntityState, animationMetaData );

			FireStateUpdated( previousState, newBattleEntityState );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		public void HandleAnimationFinished(object o, EventArgs args)
		{
			//When showing animation has finished, switch to idle state
			switch( mState )
			{
				case State.ConfirmCompletion:
					OnInsertState( State.MovingOut );
					break;

				case State.MovingOut:
					OnInsertState( State.None );
					break;
			}
		}
		#endregion

		#region Event Firing
		private void FireStateUpdated( State previousBattleEntityState, State newBattleEntityState )
		{
			if( InboxPackStateUpdated != null )
			{
				InboxPackStateUpdated( this, new InboxPackStateTransitionEventArgs( previousBattleEntityState, newBattleEntityState ) );
			}
		}
		#endregion
	}
}