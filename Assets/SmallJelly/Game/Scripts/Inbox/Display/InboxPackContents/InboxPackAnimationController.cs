﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	/// <summary>
	/// Controls the animations of the pack opening in the Inbox.
	/// </summary>
	public class InboxPackAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Inbox/"; } }
		#endregion

		#region Constants
		private const string INITIALISATION_ANIMATION_TEMPLATE_NAME = "InitialisationAnimation";
		private const string MOVE_IN_ANIMATION_TEMPLATE_NAME = "MoveInAnimation";
		private const string MOVE_OUT_ANIMATION_TEMPLATE_NAME = "MoveOutAnimation";
		private const string OPENING_ANIMATION_TEMPLATE_NAME = "OpenAnimation";
		private const string CONFIRM_COMPLETION_ANIMATION_TEMPLATE_NAME = "ConfirmCompletionAnimation";
		#endregion

		#region Methods
        public void SetNumberOfCards(int numCards)
        {
            SJLogger.LogMessage(MessageFilter.UI, "Number of cards to animate = {0}", numCards);
            mPlayMakerSharedVariables.Add("NumCards", new FsmInt(numCards));
        }

		public void UpdateAnimations( InboxPackController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch(state)
			{
				case InboxPackController.State.Initialisation:
					PlayAnimation( INITIALISATION_ANIMATION_TEMPLATE_NAME );
					break;

				case InboxPackController.State.MovingIn:
					PlayAnimation( MOVE_IN_ANIMATION_TEMPLATE_NAME );
					break;

				case InboxPackController.State.MovingOut:
					PlayAnimation( MOVE_OUT_ANIMATION_TEMPLATE_NAME );
					break;

				case InboxPackController.State.Opening:
					PlayAnimation( OPENING_ANIMATION_TEMPLATE_NAME );
					break;

				case InboxPackController.State.ConfirmCompletion:
					PlayAnimation( CONFIRM_COMPLETION_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}
