﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Menu class which controls the display and selection of the players inbox items
	/// </summary>
	public class InboxList : MenuController
	{
		#region Public Event Handlers
		public event EventHandler< StringEventArgs > PickedInboxItem;
        public event Action<int> InboxUpdated;
		#endregion

		#region Public Properties
		public InboxItemData[] InboxItemData
		{
			get
			{
				return mInboxItemData;
			}
		}
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private InboxListController mInboxListController;
		#endregion

		#region Member Variables
		private InboxItemData[] mInboxItemData;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void Refresh( InboxItemData[] inboxItemData )
		{
			mInboxListController.Refresh( inboxItemData );

			mInboxItemData = inboxItemData;

            FireInboxUpdated(inboxItemData.Count());
		}
		#endregion

		#region Event Firing
        private void FireInboxUpdated(int numItems)
        {
            if(InboxUpdated != null)
            {
                InboxUpdated(numItems);
            }
        }

		private void FirePickedInboxItem( string uniqueId )
		{
			if( PickedInboxItem != null )
			{
				PickedInboxItem( this, new StringEventArgs( uniqueId ) );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInboxListController.PickedInboxIndex += HandlePickedInboxIndex;
		}

		private void UnregisterListeners()
		{
			mInboxListController.PickedInboxIndex -= HandlePickedInboxIndex;
		}
		#endregion

		#region Event Handlers
		private void HandlePickedInboxIndex( object o, IntEventArgs intEventArgs )
		{
			//Throw the unique id
			FirePickedInboxItem( mInboxItemData[ intEventArgs.Int ].UniqueId );
		}
		#endregion
	}
}