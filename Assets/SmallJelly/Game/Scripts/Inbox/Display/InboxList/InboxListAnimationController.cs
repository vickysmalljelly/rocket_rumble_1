﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SmallJelly.Framework;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	/// <summary>
	/// Controls the animations of the deck list panel (see DeckListController.cs)
	/// </summary>
	public class InboxListAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return string.Empty; } }
		#endregion

		#region Methods
		public void UpdateAnimations( InboxListController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

		}
		#endregion
	}
}
