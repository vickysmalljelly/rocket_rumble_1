﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;
using HutongGames.PlayMaker;
using UnityEngine.UI;
using System.Linq;

namespace SmallJelly
{
	/// <summary>
	/// Menu class which controls the display and selection of the inbox items
	/// </summary>
	public class InboxListController : SJMonoBehaviour
	{
		#region Enums
		//The core state of the object
		public enum State
		{
			None,

			Downloading,
			Downloaded,

			Idle,
			MoveIn,
			MoveOut
		}
		#endregion

		#region Public Event Handlers
		public event EventHandler< IntEventArgs > PickedInboxIndex
		{
			add
			{
				mInboxListView.PickedInboxIndex += value;
			}

			remove
			{
				mInboxListView.PickedInboxIndex -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private InboxListView mInboxListView;
		#endregion

		#region Unity Methods
		protected override void Awake ()
		{
			base.Awake();
		}
		#endregion

		#region Public Methods
		public void Refresh( InboxItemData[] inboxItemData )
		{
			mInboxListView.Refresh( inboxItemData );
		}

		public void OnInsertState( State state )
		{
			OnInsertState( state, new AnimationMetaData( new Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( State state, AnimationMetaData animationMetaData )
		{
			mInboxListView.OnPlayAnimation( state, animationMetaData );
		}
		#endregion
	}
}