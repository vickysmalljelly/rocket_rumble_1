﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace SmallJelly
{
	/// <summary>
	/// Controls the view of the deck list panel (see DeckListController.cs)
	/// </summary>
	public class InboxListView : SJMonoBehaviour 
	{
		#region Public Event Handlers
		public event EventHandler< IntEventArgs > PickedInboxIndex;

		public event EventHandler AnimationFinished
		{
			add
			{
				mInboxListAnimationController.AnimationFinished += value;
			}

			remove
			{
				mInboxListAnimationController.AnimationFinished += value;
			}
		}
		#endregion

		#region Exposed To The Inspector
		[SerializeField]
		private Transform mScrollView;

		[SerializeField]
		private InboxListAnimationController mInboxListAnimationController;
		#endregion

		#region Member Variables
		private List< UIButton > mButtons;
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{
			mButtons = new List< UIButton >();
		}

		private void OnDisable()
		{
			mButtons.Clear();
		}
		#endregion

		#region Public Methods
		public void Refresh( InboxItemData[] inboxItemDataArray )
		{

			foreach( UIButton uiButton in mButtons )
			{
				UnregisterButtonListeners( uiButton );
				Destroy( uiButton.gameObject );
			}

			mButtons.Clear();

			foreach( InboxItemData inboxItemData in inboxItemDataArray )
			{
				UIButton button = UIButtonFactory.ConstructInboxButton( inboxItemData.DisplayName );
				SJRenderTransformExtensions.AddChild( mScrollView, button.transform, false, false, true );

				mButtons.Add( button );
			}


			foreach( UIButton uiButton in mButtons )
			{
				RegisterButtonListeners( uiButton );
			}
		}

		public void OnPlayAnimation( InboxListController.State state, AnimationMetaData animationMetaData )
		{
			mInboxListAnimationController.UpdateAnimations( state, animationMetaData );
		}
		#endregion

		#region Event Firing
		private void FirePickedInboxIndex( int index )
		{
			if( PickedInboxIndex != null )
			{
				PickedInboxIndex( this, new IntEventArgs( index ) );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterButtonListeners( UIButton button )
		{
			button.Clicked += HandleButtonClicked;
		}

		private void UnregisterButtonListeners( UIButton button )
		{
			button.Clicked -= HandleButtonClicked;
		}
		#endregion

		#region Event Handlers
		private void HandleButtonClicked( object o, GameObjectEventArgs args )
		{
			UIButton uiButton = args.GameObject.GetComponent< UIButton >();
			int index = mButtons.IndexOf( uiButton );

			//Send the ID of the clicked item
			FirePickedInboxIndex( index );
		}
		#endregion
	}
}