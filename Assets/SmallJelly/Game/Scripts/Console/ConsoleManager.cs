﻿using System.Collections.Generic;
using SmallJelly.Framework;
using WellFired.DevelopmentConsole;

//using GetSocialSdk.Core;

namespace SmallJelly
{
	/// <summary>
	/// Manager for debug commands added to the Development Console
	/// </summary>
	public class ConsoleManager : MonoBehaviourSingleton<GameManager>
	{
        //#if SJ_DEBUG
		private void OnEnable()
		{
			// Register this class as a source of console commands
			DevelopmentCommands.Register(this);
		}

		private void OnDisable()
		{
			// Unregister this class as a source of console commands
			DevelopmentCommands.Unregister(this);
		}          

        static List<IapData> sVirtualGoods = new List<IapData>();

        [ConsoleCommand]
        private void ListLeaderboards()
        {
            ClientLeaderboard.ListLeaderboards(
                
                (List<LeaderboardData> list) => 
            {
                Debug.Log("ListLeaderboards success");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("ListLeaderboards failed");
            }
            );
        }

        [ConsoleCommand]
        private void FakeRumbleWin()
        {
            ClientDebug.FakeWin("Smuggler", 
                () => 
            {
                Debug.Log("FakeRumbleWin success");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("FakeRumbleWin failed");
            }
            );
        }

        [ConsoleCommand]
        private void TestGetLatestBattleData()
        {
            ClientBattle.GetLatestBattleData(
                (SJJson battleView) => 
            {
                Debug.Log(battleView.JSON);
                Debug.Log("GetLatestBattleData success");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("GetLatestBattleData failed");
            }
            );
        }

        [ConsoleCommand]
        private void TestDurable()
        {
            ClientDebug.TestDurable(
                () => 
            {
                Debug.Log("TestDurable success");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("TestDurable failed");
            }
            );
        }

        [ConsoleCommand]
        private void SetCheckpoint(string checkpoint)
        {
            ClientOnboarding.SetCheckpoint(checkpoint, 
                () => 
            {
                Debug.Log("SetCheckpoint response");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("SetCheckpoint failed");
            }
            );
        }

        [ConsoleCommand]
        private void Ping()
        {
            ClientPing.Ping(
                () => 
            {
                Debug.Log("Ping response");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Ping failed");
            }
            );
        }

        [ConsoleCommand]
        private void GiveCredits(int numCredits)
        {
            ClientDebug.GiveCredits(numCredits, 
                () => 
            {
                Debug.Log("Given " + numCredits + " credits to player");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to give credits to player");
            }
            );
        }

        [ConsoleCommand]
        private void GiveCrystals(int numCrystals)
        {
            ClientDebug.GiveCrystals(numCrystals, 
                () => 
            {
                Debug.Log("Given " + numCrystals + " crystals to player");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to give crystals to player");
            }
            );
        }

        [ConsoleCommand]
        private void BuyVirtualGoodWithCash(string shortCode)
        {
            Debug.Log("BuyVirtualGoodWithCash: " + shortCode);
            IapData itemToBuy = sVirtualGoods.Find(x => x.ShortCode == shortCode);

            if(itemToBuy != default(IapData))
            {
                Debug.Log("We are buying item " + itemToBuy.ShortCode);

                ProductManager.Get.BuyConsumable(itemToBuy.GooglePlayProductId);
            }
            else
            {
                Debug.Log("Item " + shortCode + " not found");
            }              
        }


        [ConsoleCommand]
        private void BuyVirtualGoodWithCredits(string shortCode)
        {
            IapData itemToBuy = sVirtualGoods.Find(x => x.ShortCode == shortCode);

            if(itemToBuy != default(IapData))
            {
                Debug.Log("We are buying item " + itemToBuy.ShortCode);
                           
                ClientShop.BuyVirtualGoodWithCredits(itemToBuy,
                    (code, currency, price) => 
                {
                    Debug.Log("Successful purchase: " + itemToBuy.Name);
                }, 
                    (ServerError error) => 
                {
                    switch(error.ErrorType)
                    {
                        case ServerErrorType.NotEnoughCredits:
                            Debug.Log(error.UserFacingMessage);
                            break;
                        default:                                
                            Debug.LogError(error.UserFacingMessage);
                            break;
                    }
                }
                );
            }
            else
            {
                Debug.Log("Item " + shortCode + " not found");
            }              
        }

        [ConsoleCommand]
        private void ListVirtualGoods()
        {
            ClientShop.ListVirtualGoods(
                (List<IapData> list) => 
            {
                sVirtualGoods.Clear();

                Debug.Log("Number of virtual goods = " + list.Count);
                foreach(IapData item in list)
                {
                    Debug.Log(item.ToString());
                    sVirtualGoods.Add(item);
                }

            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to list virtual goods");
            }
            );
        }

        [ConsoleCommand]
        private void ConsumeAllInboxItems()
        {
            ClientDebug.ConsumeAllInboxItems(
                () => 
            {
                Debug.Log("Consumed all inbox items");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to consume all inbox items");
            }
            );
        }

        [ConsoleCommand]
        private void GivePacks(int numPacks)
        {
            ClientDebug.GivePacks(numPacks, 
                () => 
            {
                Debug.Log("Given " + numPacks + " to player");
            }, 
            (ServerError error) => 
            {
                Debug.LogError("Failed to give packs to player");
            }
            );
        }

        /*
        [ConsoleCommand]
        private void TestGetSocial()
        {
            GetSocial.SendInvite(InviteChannelIds.Email,
                onComplete: () => Debug.Log("Invitation via EMAIL was sent"),
                onCancel: () => Debug.Log("Invitation via EMAIL was cancelled"),
                onFailure: (error) => Debug.LogError("Invitation via EMAIL failed, error: " + error.Message)
            );
        }
*/

        [ConsoleCommand]
        private void AddAllToCollection()
        {
            ClientDebug.AddAllToCollection(
                () => 
            {
                Debug.Log("Added all cards to collection");
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to add all cards to collection");
            }
            );
        }

        [ConsoleCommand]
        private void AddToCollection(string cardId)
        {

            ClientDebug.AddBattleCardToCollection(cardId,
                () => 
            {
                Debug.Log("Forced add of " + cardId);
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to force add of " + cardId);
            }
            );
        }

        [ConsoleCommand]
        private void UpdatePlayerData()
        {
            GameManager.Get.RefreshPlayerDetails();
        }

        [ConsoleCommand]
        private void ConsumeInboxItem(string uniqueId)
        {
            ClientInbox.ConsumeItem(uniqueId, 
                (RewardData reward) => 
            {
                DebugOutputReward(reward);
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to give reward. Message: " + error.UserFacingMessage);
            }
            );
        }

        [ConsoleCommand]
        private void ListInbox()
        {
            ClientInbox.ListInbox(
                (List<InboxItemData> list) => 
            {
                Debug.Log("Inbox Contents:");

                foreach(InboxItemData item in list)
                {
                    Debug.Log("Name: " + item.DisplayName + ", " + item.Description + ", " + item.ItemType + ", " + item.UniqueId);
                    Debug.Log("Comparing " + item.ItemType);

                    if(string.Compare(item.ItemType, "Pack") == 0)
                    {
                        InboxPackData pack = (InboxPackData)item;
                        Debug.Log("Type: " + pack.PackType + ", " + pack.PackId);
                    }
                }
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to give reward. Message: " + error.UserFacingMessage);
            }
            );
        }

        [ConsoleCommand]
        private void GiveReward(RewardEvent rewardEvent)
        {
            ClientRewards.DebugGiveReward(rewardEvent, 
                (RewardData reward) => 
            {
                DebugOutputReward(reward);
            }, 
                (ServerError error) => 
            {
                Debug.LogError("Failed to give reward. Message: " + error.UserFacingMessage);
            }
            );
        }
            
        [ConsoleCommand]
        private void SetCurrentDeck()
        {
            // Hard code to test
            string deckId = "585a4b6008987a04a4141a00";
            ClientDeck.SetCurrentDeck(deckId,
                () => 
            {
                Debug.Log("Deck set");
            }, 
                (ServerError error) => 
            {
                Debug.LogError(error.ErrorType);
            }
            );
        }

        /// <summary>
        /// Get cards from the player's collection
        /// </summary>
        [ConsoleCommand]
        private void SetEmail(string emailAddress)
        {
            ClientUser.SetUserEmail(emailAddress,
                () => 
            {
                Debug.Log("Email set");
            }, 
                (ServerError error) => 
            {
                Debug.LogError(error.ErrorType);
            }
            );
        }

        /// <summary>
        /// Force a specific card to be drawn for the current player
        /// </summary>
		[ConsoleCommand]
        private void DrawCard(string cardId)
		{
            
            ClientBattle.DebugForceCardDraw(cardId,
                () => 
                {
                    Debug.Log("Forced draw of " + cardId);
                }, 
                (ServerError error) => 
                {
                    Debug.LogError("Failed to force draw of " + cardId);
                }
            );
		}

		/// <summary>
		/// Saves a replay on the server.
		/// </summary>
		/// <param name="replayName"></param>
		[ConsoleCommand]
		private void SaveReplay(string replayName, string replayDescription)
		{
			ClientReplay.SaveReplay(replayName, replayDescription, 
                () =>
    			{
    				Debug.Log(replayName + " saved successfully");
    			}, 
                (ServerError error) =>
    			{
    				Debug.LogError(replayName + " failed to save with error: " + error.ToString());
    			}
            );
		}

		[ConsoleCommand]
		private void RecordBug(string bugName, string bugDescription)
		{
			ClientReplay.RecordBug(bugName, bugDescription, 
                () =>
    			{
    				Debug.Log(bugName + " recorded successfully");
    			}, 
                (ServerError error) =>
    			{
    				Debug.LogError(bugName + " failed to save with error: " + error.ToString());
    			}
            );
		}

		[ConsoleCommand]
		private void GetNews()
		{
			ClientNews.GetNewsItems((newsItemList) =>
			{
				Debug.Log("Retrieved news items: " + newsItemList.Count);
                foreach(NewsItem item in newsItemList)
                {
                    Debug.Log(item.Id + ", " + item.Date + ", " + item.Title  + ", " + item.Content);
                }
			},
			(ServerError error)=>
			{
				Debug.LogError("Failed to retrieve news items: " + error.TechnicalDetails);
			});
		}

		[ConsoleCommand]
		private void DeckUpdateDisplayName(string deckId, string displayName)
		{
			ClientDeck.DeckUpdateDisplayName(deckId, displayName, () =>
			{
				Debug.Log(string.Format("Updated deck {0} displayName to: {1}", deckId, displayName));
			},
				(ServerError error) =>
			{
				Debug.LogError(string.Format("Failed to update displayName for deck {0}, with error: {1}", deckId, error.TechnicalDetails));
			});
		}

		[ConsoleCommand]
		private void ListDecks()
		{
			ClientDeck.ListDecks((List<Deck> decks) =>
			{
				Debug.Log("Retrieved list of decks for local player: ");
				foreach(Deck deck in decks)
				{
					Debug.Log("deckId: " + deck.DeckId + ", displayName: " + deck.DisplayName);
				}
			},
			(ServerError error) =>
			{
				Debug.LogError("Failed to get decks for local player, with error: " + error.TechnicalDetails);
			});
		}

		[ConsoleCommand]
		private void GetDeck(string deckId)
		{
			ClientDeck.GetDeck(deckId, (Deck deck) =>
			{
				Debug.Log(string.Format("Retrieved deck with id {0}, displayName {1} and cards:", deck.DeckId, deck.DisplayName));
				foreach(BattleEntityData entity in deck.BattleEntities)
				{
					Debug.Log(entity.Id);
				}
			},
			(ServerError error) =>
			{
				Debug.LogError("Failed to get deck " + deckId + " for local player, with error: " + error.TechnicalDetails);
			});
		}

        private void DebugOutputReward(RewardData reward)
        {
            Debug.Log("Reward =========================================");

            foreach(BattleEntityData entity in reward.BattleEntityCards)
            {
                Debug.Log("CardID = " + entity.Id);
            }

            foreach(ShipCardData ship in reward.ShipCards)
            {
                Debug.Log("Ship = " + ship.Id + ", " + ship.DisplayName + ", " + ship.Class);
            }

            foreach(InboxItemData pack in reward.Packs)
            {
                Debug.Log("Pack = " + pack.ItemType + ", " + pack.DisplayName + ", " + pack.Description);
            }

            if(reward.GetTotalCredits() != 0)
            {
                Debug.Log("Credits = " + reward.Credits);
            }

            foreach(KeyValuePair<string, int> entry in reward.Resources)
            {
                Debug.Log("Resource: " + entry.Key + " " + entry.Value);
            }
            Debug.Log("=============================================");
        }

        //#endif
	}
}
