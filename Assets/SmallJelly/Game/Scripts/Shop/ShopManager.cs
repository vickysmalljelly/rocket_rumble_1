﻿using UnityEngine;
using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace SmallJelly
{
	/// <summary>
	/// Manages the shop
	/// </summary>
	public class ShopManager : MonoBehaviourSingleton< ShopManager >
	{
		#region Public Properties
		public ShopMenuController ShopMenuController
		{
			get
			{
				return mShopMenuController;
			}
		}

        public GameObject CurrentShopItem { get; set; }

		#endregion

		#region Public Event Handlers
		public event Action ShopConnectionFailed;

		public event EventHandler< ShopEventDataEventArgs > ShopEventDataReceived;
		public event EventHandler< StringEventArgs > ShopResponseFailure;
		#endregion

		#region Member Variables
		private ShopMenuController mShopMenuController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake ();

			mShopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
		}

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
        public static long GetNumCrystalsFromProduct( Product product ) 
        {
            string productName = product.metadata.localizedTitle; 
            productName = productName.Substring(0, productName.IndexOf(" "));
            long numCrystals = 0;
            if(long.TryParse(productName, out numCrystals))
            {
                return numCrystals;
            }

            return -1;
        }

		public void BuyVirtualGoodWithCrystals( IapData data ) 
		{
			ClientShop.BuyVirtualGoodWithCrystals( data, HandleBuyVirtualGoodSuccess, HandleServerError );
		}

		public void BuyVirtualGoodWithCredits( IapData data ) 
		{
			ClientShop.BuyVirtualGoodWithCredits( data, HandleBuyVirtualGoodSuccess, HandleServerError );
		}

		public void BuyConsumable( IapData data )
		{
			ProductManager.Get.BuyConsumable( data.GooglePlayProductId );
		}

		public void ListVirtualGoods() 
		{
			ClientShop.ListVirtualGoods( HandleListVirtualGoodsSuccess, HandleServerError );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ProductManager.Get.PurchaseSuccess += HandlePurchaseSuccess;
			ProductManager.Get.PurchaseFailed += HandlePurchaseFailed;
		}

		private void UnregisterListeners()
		{
            if(ProductManager.Get != null)
            {
    			ProductManager.Get.PurchaseSuccess -= HandlePurchaseSuccess;
    			ProductManager.Get.PurchaseFailed -= HandlePurchaseFailed;
            }
		}
		#endregion

		#region Event Firing
		private void FireShopEventDataReceived( ShopEventData shopEventData )
		{
			if( ShopEventDataReceived != null )
			{
				ShopEventDataReceived( this, new ShopEventDataEventArgs( shopEventData ) );
			}
		}

		private void FireShopResponseFailure( string text )
		{
			if( ShopResponseFailure != null  )
			{
				ShopResponseFailure( this, new StringEventArgs( text ) );
			}
		}

		public void FireShopConnectionFailed()
		{
			if( ShopConnectionFailed != null )
			{
				ShopConnectionFailed();
			}
		}
		#endregion

		#region Event Handlers
        private void HandleBuyVirtualGoodSuccess( string shortCode, IapData.Currency currency, long amount )
		{	
            SJLogger.LogMessage(MessageFilter.Gameplay, "Bought virtual good, shortCode = {0}", shortCode);

            // Need to update the currency spent
            switch(currency)
            {
                case IapData.Currency.Credits:
                    GameManager.Get.SpendCredits(amount, PlayerData.SinkOfResource.SpentInShop);
                    break;
                case IapData.Currency.Crystals:
                    GameManager.Get.SpendCrystals(amount, PlayerData.SinkOfResource.SpentInShop);
                    break;
                default: 
                    SJLogger.LogError("Currency not recognised: {0}", currency);
                    break;
            }               

            // Update that we have a new item in the inbox
            BuyVirtualGoodSuccessEventData eventData = new BuyVirtualGoodSuccessEventData(shortCode);

            if(eventData.NumPacks > 0)
            {
                // Note that we have bought a pack
                AnalyticsManager.Get.RecordPacksBought(eventData.NumPacks);

                // Update that we have a new item in the inbox
                GameManager.Get.UpdateNewInboxItemOnClient(true);
            }

            FireShopEventDataReceived( eventData );
		}

		private void HandleListVirtualGoodsSuccess( List< IapData > iapData )
		{	
			FireShopEventDataReceived( new ListVirtualGoodsSuccessEventData( iapData ) );
		}

		private void HandlePurchaseSuccess( Product product )
		{
            long numCrystals = GetNumCrystalsFromProduct(product);
            if(numCrystals == -1)
            {
                SJLogger.LogWarning("Failed to get num crystals from product");
            }
            else
            {
                GameManager.Get.GainCrystals(GetNumCrystalsFromProduct(product), PlayerData.SourceOfResource.BoughtInShop);
            }

            FireShopEventDataReceived( new BuyVirtualGoodFromGooglePlaySuccessEventData(product.definition.id, numCrystals) );
		}

		private void HandlePurchaseFailed( string productId, PurchaseFailureReason purchaseFailureReason )
		{
            string errorMessage = string.Empty;

            switch(purchaseFailureReason)
            {
                case PurchaseFailureReason.DuplicateTransaction:
                    errorMessage = "Purchase failed: duplicate transation.";
                    break;
                case PurchaseFailureReason.ExistingPurchasePending:
                    errorMessage = "Purchase failed: existing purchase pending.";
                    break;
                case PurchaseFailureReason.PaymentDeclined:
                    errorMessage = "Purchase failed: payment declined.";
                    break;
                case PurchaseFailureReason.ProductUnavailable:
                    errorMessage = "Purchase failed: product unavailable.";
                    break;
                case PurchaseFailureReason.PurchasingUnavailable:
                    errorMessage = "Purchase failed: purchasing unavailable.";
                    break;
                case PurchaseFailureReason.SignatureInvalid:
                    errorMessage = "Purchase failed: signature invalid.";
                    break;
                case PurchaseFailureReason.Unknown:
                default:
                    errorMessage = "Purchase failed: reason unknown.";
                    break;
            }

            FireShopResponseFailure( errorMessage );
		}

        private void HandleServerError( ServerError error )
		{
			switch( error.ErrorType)
			{
				case ServerErrorType.Timeout:
					FireShopConnectionFailed();
					break;

				default:
					FireShopResponseFailure( error.UserFacingMessage );
					break;
			}
		}
		#endregion
	}
}