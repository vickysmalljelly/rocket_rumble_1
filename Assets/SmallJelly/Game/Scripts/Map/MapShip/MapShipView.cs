﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class MapShipView : SJMonoBehaviour 
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mMapShipAnimationController.AnimationFinished += value;
			}

			remove
			{
				mMapShipAnimationController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private MapShipAnimationController mMapShipAnimationController;
		#endregion

		#region Public Methods
		public void OnInsertState( MapShipController.State state, AnimationMetaData animationMetaData )
		{
			mMapShipAnimationController.OnInsertState( state, animationMetaData );
		}
		#endregion
	}
}