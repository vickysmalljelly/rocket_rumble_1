﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class MapShipAnimationController : AnimationController
	{
		#region Protected Properties
		protected override string ResourcePath { get{ return  "PlayMaker/Templates/Map/Ships/"; } }
		#endregion

		#region Constants
		private const string MOVE_TO_PLANET_ANIMATION_TEMPLATE_NAME = "MoveToPlanetAnimation";
		private const string MOVE_AROUND_PLANET_ANIMATION_TEMPLATE_NAME = "MoveAroundPlanetAnimation";
		#endregion

		#region Public Methods
		public void OnInsertState( MapShipController.State state, AnimationMetaData animationMetaData )
		{
			StopExistingAnimations();

			SetMetaData( animationMetaData );

			switch( state )
			{
				case MapShipController.State.MoveToPlanet:
					PlayAnimation( MOVE_TO_PLANET_ANIMATION_TEMPLATE_NAME );
					break;

				case MapShipController.State.MoveAroundPlanet:
					PlayAnimation( MOVE_AROUND_PLANET_ANIMATION_TEMPLATE_NAME );
					break;
			}
		}
		#endregion
	}
}