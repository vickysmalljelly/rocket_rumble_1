﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class MapShip : SJMonoBehaviour
	{
		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mMapShipController.AnimationFinished += value;
			}

			remove
			{
				mMapShipController.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private MapShipController mMapShipController;
		#endregion

		#region Public Methods
		public void OnInsertState( MapShipController.State state )
		{
			OnInsertState( state, new AnimationMetaData( new System.Collections.Generic.Dictionary<string, HutongGames.PlayMaker.NamedVariable>() ) );
		}

		public void OnInsertState( MapShipController.State state, AnimationMetaData animationMetaData )
		{
			mMapShipController.OnInsertState( state, animationMetaData );
		}
		#endregion
	}
}