﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class MapShipController : SJMonoBehaviour 
	{
		#region Enums
		public enum State
		{
			MoveToPlanet,
			MoveAroundPlanet
		}
		#endregion

		#region Public Events
		public event EventHandler AnimationFinished
		{
			add
			{
				mMapShipView.AnimationFinished += value;
			}

			remove
			{
				mMapShipView.AnimationFinished -= value;
			}
		}
		#endregion

		#region Member Variables
		[SerializeField]
		private MapShipView mMapShipView;
		#endregion

		#region Public Methods
		public void OnInsertState( MapShipController.State state, AnimationMetaData animationMetaData )
		{
			mMapShipView.OnInsertState( state, animationMetaData );
		}
		#endregion
	}
}