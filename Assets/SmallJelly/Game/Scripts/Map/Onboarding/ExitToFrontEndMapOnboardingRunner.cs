﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ExitToFrontEndMapOnboardingRunner : MapOnboardingRunner
	{

		#region Constructors
		public ExitToFrontEndMapOnboardingRunner( )
		{
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			FireExitToFrontEnd();

			OnFinishRunning();
		}
		#endregion
	}
}