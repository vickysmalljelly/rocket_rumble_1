﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public abstract class MapOnboardingRunner
	{
		#region Event Handlers
		public event Action< BattleId > RunBattleWithId;
		public event Action ExitToFrontEnd;

		public event Action FinishedRunning;
		#endregion

		#region Public Methods
		public virtual void OnStartRunning()
		{
		}

		public virtual void OnUpdate()
		{
		}

		public virtual void OnFinishRunning()
		{
			FireFinishedRunning();
		}
		#endregion

		#region Event Firing
		protected void FireRunBattleWithId( BattleId battleId )
		{
			if( RunBattleWithId != null )
			{
				RunBattleWithId( battleId );
			}
		}


		protected void FireExitToFrontEnd()
		{
			if( ExitToFrontEnd != null )
			{
				ExitToFrontEnd();
			}
		}

		private void FireFinishedRunning()
		{
			if( FinishedRunning != null )
			{
				FinishedRunning( );
			}
		}
		#endregion
	}
}