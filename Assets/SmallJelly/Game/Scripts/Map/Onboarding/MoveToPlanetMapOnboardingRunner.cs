﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class MoveToPlanetMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private readonly int mPlanetIndex;
		#endregion

		#region Constructors
		public MoveToPlanetMapOnboardingRunner( int planetIndex )
		{
			mPlanetIndex = planetIndex;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			Dictionary< string, NamedVariable> metaData = new Dictionary< string, NamedVariable>();
			metaData.Add("Planet", new FsmGameObject( MapManager.Get.PlanetListeners[ mPlanetIndex ].gameObject ) );

			AnimationMetaData animationMetaData = new AnimationMetaData( metaData );

			MapManager.Get.MapShip.OnInsertState( MapShipController.State.MoveToPlanet, animationMetaData );

			RegisterListeners();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			UnregisterListeners();

			MapManager.Get.MapShip.OnInsertState( MapShipController.State.MoveAroundPlanet );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			MapManager.Get.MapShip.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			MapManager.Get.MapShip.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			OnFinishRunning();
		}
		#endregion
	}
}