﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly.Framework
{
	/// <summary>
	/// Responsible for all dialogs relating specifically to the map onboarding process
	/// </summary>
	public class MapOnboardingDialogManager : MonoBehaviourSingleton< MapOnboardingDialogManager >
	{
		#region Inspector Variables
		[SerializeField]
		private GameObject mPopupDialog;
		#endregion

		#region Private Member Variables
		private PopupViewTMP mPopupView;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			SJLogger.AssertCondition(mPopupDialog != null, "mPopupMessage has not been set in the inspector");

			mPopupView = mPopupDialog.GetComponent<PopupViewTMP>();

			SJLogger.AssertCondition(mPopupView != null, "mPopupMessage does not contain a PopupView component");
		}
		#endregion

		#region Public Methods
		public void ShowMessagePopup(string title, string message, params ButtonData[] buttons)
		{
			SJLogger.LogMessage(MessageFilter.UI, "Opening popup {0}, {1}", title, message);

			mPopupDialog.SetActive(true);

			PopupData data = new PopupData(title, message, buttons);
			mPopupView.OnRefresh(data);

			// Listen to all buttons so we know when to close the dialog
			RegisterPopupViewListeners( mPopupView );
		}

		public void HideMessagePopup()
		{
			SJLogger.LogMessage(MessageFilter.UI, "Hiding popup");

			// Stop listening to the buttons
			UnregisterPopupViewListeners( mPopupView );

			mPopupDialog.SetActive(false);
		}
		#endregion

		#region Event Registration
		private void RegisterPopupViewListeners( PopupViewTMP popupView )
		{
			// Listen to all buttons so we know when to close the dialog
			popupView.ButtonClicked += HandlePopupButtonClicked;
		}

		private void UnregisterPopupViewListeners( PopupViewTMP popupView )
		{
			popupView.ButtonClicked -= HandlePopupButtonClicked;
		}
		#endregion

		#region Event Handlers
		private void HandlePopupButtonClicked()
		{
			HideMessagePopup();
		}
		#endregion
	}
}
