﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DisplayPopupMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private string mTitle;
		private string mMessage;
		private string mButtonName;
		#endregion

		#region Constructor
		public DisplayPopupMapOnboardingRunner( string title, string message, string buttonName )
		{
			mTitle = title;
			mMessage = message;
			mButtonName = buttonName;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			MapOnboardingDialogManager.Get.ShowMessagePopup( mTitle, mMessage, new ButtonData( mButtonName, OnFinishRunning ) );
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();
		}
		#endregion
	}
}