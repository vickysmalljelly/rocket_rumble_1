﻿using SmallJelly.Framework;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SmallJelly
{
	public class MapOnboardingManager : MonoBehaviourSingleton< MapOnboardingManager >
	{
		#region Event Handlers
		public event Action< BattleId > RunBattleWithId;
		public event Action ExitToFrontEnd;
		#endregion

		#region Member Variables
		private Queue< MapOnboardingRunner > mCurrentRunnerQueue;
		private MapOnboardingRunner mCurrentRunner;
		private bool mRunnerFinished;
		#endregion

		#region Public Methods
		protected override void Awake()
		{
			base.Awake();
			mCurrentRunnerQueue = MapOnboardingRunnerFactory.GetMapOnboardingRunnerQueue( );
		}

		public void Start()
		{
			mRunnerFinished = true;

			Update();
		}

		public void OnStartRunning( MapOnboardingRunner mapOnboardingRunner )
		{
			mRunnerFinished = false;

			mCurrentRunner = mapOnboardingRunner;

			RegisterListeners( mapOnboardingRunner );

			mapOnboardingRunner.OnStartRunning();
		}

		public void Update()
		{
			if( mRunnerFinished )
			{
				if( mCurrentRunner != null )
				{
					OnFinishRunning( mCurrentRunner );
				}

				if( mCurrentRunnerQueue.Count > 0 )
				{
					OnStartRunning( mCurrentRunnerQueue.Dequeue() );
				}
				return;
			}
				
			mCurrentRunner.OnUpdate();
		}

		public void OnFinishRunning( MapOnboardingRunner mapOnboardingRunner )
		{
			UnregisterListeners( mapOnboardingRunner );

			mCurrentRunner = null;
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( MapOnboardingRunner mapOnboardingRunner )
		{
			mapOnboardingRunner.RunBattleWithId += HandleRunBattleWithId;
			mapOnboardingRunner.ExitToFrontEnd += HandleExitToFrontEnd;

			mapOnboardingRunner.FinishedRunning += HandleBattleOnboardingRunnerFinished;
		}

		private void UnregisterListeners( MapOnboardingRunner mapOnboardingRunner )
		{
			mapOnboardingRunner.RunBattleWithId -= HandleRunBattleWithId;
			mapOnboardingRunner.ExitToFrontEnd -= HandleExitToFrontEnd;

			mapOnboardingRunner.FinishedRunning -= HandleBattleOnboardingRunnerFinished;
		}
		#endregion

		#region Event Firing
		private void FireRunBattleWithId( BattleId battleId )
		{
			if( RunBattleWithId != null )
			{
				RunBattleWithId( battleId );
			}
		}

		private void FireExitToFrontEnd()
		{
			if( ExitToFrontEnd != null )
			{
				ExitToFrontEnd();
			}
		}
		#endregion

		#region Event Handlers
		private void HandleRunBattleWithId( BattleId battleId )
		{
			FireRunBattleWithId( battleId );
		}

		private void HandleExitToFrontEnd()
		{
			FireExitToFrontEnd();
		}

		private void HandleBattleOnboardingRunnerFinished( )
		{
			mRunnerFinished = true;
		}
		#endregion
	}

}