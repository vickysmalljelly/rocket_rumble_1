﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class WaitForPlaymakerPrefabMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private readonly string mPath;
		private PlayMakerFSM mActiveFsm;
		#endregion

		#region Constructors
		public WaitForPlaymakerPrefabMapOnboardingRunner( string path )
		{
			mPath = path;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			GameObject loadedGameObject = Resources.Load< GameObject >( mPath );

			SJLogger.AssertCondition( loadedGameObject != null, "A gameobject at the resource path {0} could not be found", mPath );

			GameObject instantiatedGameObject = UnityEngine.Object.Instantiate( loadedGameObject );
			mActiveFsm = instantiatedGameObject.GetComponent< PlayMakerFSM >();
		}

		public override void OnUpdate()
		{
			if( !mActiveFsm.Active )
			{
				OnFinishRunning();
			}
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			UnityEngine.Object.Destroy( mActiveFsm.gameObject );
		}
		#endregion
	}
}