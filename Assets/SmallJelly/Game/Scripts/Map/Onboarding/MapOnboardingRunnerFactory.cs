using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace SmallJelly
{
	public class MapOnboardingRunnerFactory
	{
		#region Public Methods
		public static Queue< MapOnboardingRunner > GetMapOnboardingRunnerQueue ( )
		{
			if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial1CheckPoint ) && !FeatureFlags.SkipTutorial1 )
			{
				return GetTutorialOne();
			}
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial2CheckPoint ) && !FeatureFlags.SkipTutorial2 )
			{
				return GetTutorialTwo();
			}
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial3CheckPoint ) && !FeatureFlags.SkipTutorial3 )
			{
				return GetTutorialThree();
			}
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial4CheckPoint ) && !FeatureFlags.SkipTutorial4 )
			{
				return GetTutorialFour();
			}
			else
			{
				return GetTutorialFive();
			}
		}

		#endregion

		#region Private Methods	
		private static Queue< MapOnboardingRunner > GetTutorialOne ()
		{
			Queue< MapOnboardingRunner > tutorialOne = new Queue< MapOnboardingRunner >();

			tutorialOne.Enqueue( new LoadTutorialMapOnboardingRunner( BattleId.Tutorial1 ) );

			return tutorialOne;
		}

		private static Queue< MapOnboardingRunner > GetTutorialTwo ()
		{
			Queue< MapOnboardingRunner > tutorialTwo = new Queue< MapOnboardingRunner >();

			tutorialTwo.Enqueue( new SetPlanetMapOnboardingRunner( 1, new Vector3( -10, 0, 0 ) ) );
			tutorialTwo.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 1 ) );

			tutorialTwo.Enqueue( new WaitForPlaymakerPrefabMapOnboardingRunner( string.Format( "{0}/{1}", FileLocations.SmallJellyMapPrefabs, "TurnOffLines" ) ) );

			tutorialTwo.Enqueue( new WaitForSecondsMapOnboardingRunner( 1.0f ) );
			tutorialTwo.Enqueue( new DisplayPopupMapOnboardingRunner( "Co-Bee", "That was a close scrape! I'm not sure how much longer we can survive out here without more firepower.", "" ) );
			tutorialTwo.Enqueue( new DisplayPopupMapOnboardingRunner( "Co-Bee", "I've plotted a course to the nearest spaceport to see what we can scavenge.", "" ) );

			tutorialTwo.Enqueue( new WaitForPlaymakerPrefabMapOnboardingRunner( string.Format( "{0}/{1}", FileLocations.SmallJellyMapPrefabs, "PlotCourseToSpaceStation" ) ) );

			tutorialTwo.Enqueue( new WaitForPlanetSelectionMapOnboardingRunner( 2 ) );

			tutorialTwo.Enqueue( new MoveToPlanetMapOnboardingRunner( 2 ) );
			tutorialTwo.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 2 ) );

			tutorialTwo.Enqueue( new WaitForSecondsMapOnboardingRunner( 1.5f ) );
			tutorialTwo.Enqueue( new DisplayPopupMapOnboardingRunner( "Co-Bee", "I'm detecting an old drone ship nearby. Maybe we should use it for battle practice.", "" ) );
			tutorialTwo.Enqueue( new LoadTutorialMapOnboardingRunner( BattleId.Tutorial2 ) );


			return tutorialTwo;
		}

		private static Queue< MapOnboardingRunner > GetTutorialThree ()
		{
			Queue< MapOnboardingRunner > tutorialThree = new Queue< MapOnboardingRunner >();

			tutorialThree.Enqueue( new SetPlanetMapOnboardingRunner( 2, new Vector3( -10, 0, 0 ) ) );
			tutorialThree.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 2 ) );
			tutorialThree.Enqueue( new WaitForPlanetSelectionMapOnboardingRunner( 3 ) );

			tutorialThree.Enqueue( new MoveToPlanetMapOnboardingRunner( 3 ) );
			tutorialThree.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 3 ) );

			tutorialThree.Enqueue( new WaitForSecondsMapOnboardingRunner( 1.5f ) );
			tutorialThree.Enqueue( new DisplayPopupMapOnboardingRunner( "Co-Bee", "Another drone ship! Let's take it out!", "" ) );
			tutorialThree.Enqueue( new LoadTutorialMapOnboardingRunner( BattleId.Tutorial3 ) );


			return tutorialThree;
		}

		private static Queue< MapOnboardingRunner > GetTutorialFour ()
		{
			Queue< MapOnboardingRunner > tutorialFour = new Queue< MapOnboardingRunner >();

			tutorialFour.Enqueue( new SetPlanetMapOnboardingRunner( 3, new Vector3( -10, 0, 0 ) ) );
			tutorialFour.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 3 ) );
			tutorialFour.Enqueue( new WaitForPlanetSelectionMapOnboardingRunner( 4 ) );
			tutorialFour.Enqueue( new MoveToPlanetMapOnboardingRunner( 4 ) );
			tutorialFour.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 4 ) );
			tutorialFour.Enqueue( new DisplayPopupMapOnboardingRunner( "Co-Bee", "Incoming enemy, Captain! Weapons live, hull at full strength. Brace for combat!", "" ) );			
			tutorialFour.Enqueue( new LoadTutorialMapOnboardingRunner( BattleId.Tutorial4 ) );


			return tutorialFour;
		}

		private static Queue< MapOnboardingRunner > GetTutorialFive ()
		{
			Queue< MapOnboardingRunner > tutorialFour = new Queue< MapOnboardingRunner >();

			tutorialFour.Enqueue( new SetPlanetMapOnboardingRunner( 4, new Vector3( -10, 0, 0 ) ) );
			tutorialFour.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 4 ) );
			tutorialFour.Enqueue( new WaitForPlanetSelectionMapOnboardingRunner( 5 ) );
			tutorialFour.Enqueue( new MoveToPlanetMapOnboardingRunner( 5 ) );
			tutorialFour.Enqueue( new MoveAroundPlanetMapOnboardingRunner( 5 ) );
			tutorialFour.Enqueue( new ExitToFrontEndMapOnboardingRunner( ) );


			return tutorialFour;
		}
		#endregion
	}
}
