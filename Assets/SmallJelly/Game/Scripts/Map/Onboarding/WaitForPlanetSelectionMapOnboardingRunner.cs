﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class WaitForPlanetSelectionMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private readonly int mPlanetIndex;
		#endregion

		#region Constructors
		public WaitForPlanetSelectionMapOnboardingRunner( int planetIndex )
		{
			mPlanetIndex = planetIndex;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			MapManager.Get.PlanetIndicator.transform.position = MapManager.Get.PlanetListeners[ mPlanetIndex ].transform.position;
			MapManager.Get.PlanetIndicator.SetActive( true );

			RegisterListeners();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();

			UnregisterListeners();

			MapManager.Get.PlanetIndicator.SetActive( false );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			MapManager.Get.PlanetListeners[ mPlanetIndex ].MouseReleasedOver += HandleMouseReleasedOver;
			MapManager.Get.PlanetListeners[ mPlanetIndex ].TouchReleasedOver += HandleTouchReleasedOver;
		}

		private void UnregisterListeners()
		{
			MapManager.Get.PlanetListeners[ mPlanetIndex ].MouseReleasedOver -= HandleMouseReleasedOver;
			MapManager.Get.PlanetListeners[ mPlanetIndex ].TouchReleasedOver -= HandleTouchReleasedOver;
		}
		#endregion

		#region Event Handler
		private void HandleMouseReleasedOver( object o, EventArgs args )
		{
			OnFinishRunning();
		}

		private void HandleTouchReleasedOver( object o, EventArgs args )
		{
			OnFinishRunning();
		}
		#endregion
	}
}