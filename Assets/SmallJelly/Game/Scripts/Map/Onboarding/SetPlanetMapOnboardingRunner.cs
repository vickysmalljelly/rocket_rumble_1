﻿using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	public class SetPlanetMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private readonly int mPlanetIndex;
		private readonly Vector3 mOffset;
		#endregion

		#region Constructors
		public SetPlanetMapOnboardingRunner( int planetIndex, Vector3 offset )
		{
			mPlanetIndex = planetIndex;
			mOffset = offset;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			MapManager.Get.MapShip.transform.position = MapManager.Get.PlanetListeners[ mPlanetIndex ].transform.position + mOffset;

			OnFinishRunning();
		}
		#endregion
	}
}
