﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class WaitForSecondsMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private float mSeconds;
		#endregion

		#region Constructor
		public WaitForSecondsMapOnboardingRunner( float seconds )
		{
			mSeconds = seconds;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			GameManager.Get.DoCoroutine( WaitForFinish() );
		}
		#endregion

		#region Private Methods
		private IEnumerator WaitForFinish()
		{
			yield return new WaitForSeconds( mSeconds );

			OnFinishRunning();
		}
		#endregion
	}
}