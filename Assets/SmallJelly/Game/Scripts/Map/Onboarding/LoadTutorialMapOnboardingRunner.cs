﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class LoadTutorialMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private BattleId mBattleId;
		#endregion

		#region Constructors
		public LoadTutorialMapOnboardingRunner( BattleId battleId )
		{
			mBattleId = battleId;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			FireRunBattleWithId( mBattleId );

			OnFinishRunning();
		}
		#endregion
	}
}