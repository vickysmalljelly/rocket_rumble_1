﻿using UnityEngine;
using System.Collections;
using System;
using SmallJelly.Framework;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
	public class MoveAroundPlanetMapOnboardingRunner : MapOnboardingRunner
	{
		#region Member Variables
		private readonly int mPlanetIndex;
		#endregion

		#region Constructors
		public MoveAroundPlanetMapOnboardingRunner( int planetIndex )
		{
			mPlanetIndex = planetIndex;
		}
		#endregion

		#region Public Methods
		public override void OnStartRunning()
		{
			base.OnStartRunning();

			Dictionary< string, NamedVariable> metaData = new Dictionary< string, NamedVariable>();
			metaData.Add("Planet", new FsmGameObject( MapManager.Get.PlanetListeners[ mPlanetIndex ].gameObject ) );

			AnimationMetaData animationMetaData = new AnimationMetaData( metaData );

			MapManager.Get.MapShip.OnInsertState( MapShipController.State.MoveAroundPlanet, animationMetaData );

			OnFinishRunning();
		}

		public override void OnFinishRunning()
		{
			base.OnFinishRunning();
		}
		#endregion
	}
}