﻿using SmallJelly.Framework;
using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

namespace SmallJelly
{
	//Temporary class to manage the map
	public class MapManager : MonoBehaviourSingleton< MapManager >
	{
		#region Public Properties
		public MapMenuController MapMenuController
		{
			get
			{
				return mMapMenuController;
			}
		}
		#endregion

		#region Public Variables
		public MapShip MapShip;
		public PlayerObjectInputListener[] PlanetListeners;
		public GameObject PlanetIndicator;
		#endregion

		#region Exposed To Inspector
		[SerializeField]
		private SJRenderCanvas mCanvas;

		[SerializeField]
		private MapMenuController mMapMenuController;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake ();

			mMapMenuController = UIManager.Get.AddMenu< MapMenuController >( FileLocations.MapMenuPrefab, mCanvas.transform );
			UIManager.Get.ShowMenu< MapMenuController >();
		}

		protected void OnDestroy()
		{
			if(UIManager.Get != null)
			{
				UIManager.Get.HideMenu< MapMenuController >();
				UIManager.Get.RemoveMenu< MapMenuController > ();;
			}
		}
		#endregion
	}
}

