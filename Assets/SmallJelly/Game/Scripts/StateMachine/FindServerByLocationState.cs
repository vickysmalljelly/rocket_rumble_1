using System.Collections;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Initial state of the game, currently contains UI, but will change later
	/// </summary>
	public class FindServerByLocationState : AbstractGameState
	{
		#region Constants
		private const string LOCATION_KEY = "ServerLocation";

		private const string USA_KEY = "USA";
		private const string EUROPE_KEY = "EUROPE";
		#endregion

        //private ServerConfig mEuropeServer;
        //private ServerConfig mUSServer;

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

            TransitionState( GameStateMachine.GAME_STATE_CONNECT_TO_GS );

            /*
            // Initialise servers
            mEuropeServer = new ServerConfig();
            mEuropeServer.Key = "v311721v2Y40";
            mEuropeServer.Secret = "fibPofoCB9mKqGdC3a5E8VtNKnRCyPLJ";
            mEuropeServer.Preview = false;
            mEuropeServer.Debug = false;
            mEuropeServer.Cluster = Cluster.Europe;

            mUSServer = new ServerConfig();
            mUSServer.Key = "n315451TyeMm";
            mUSServer.Secret = "W7MuwpUkcnuA3WrfDk98M7May5YrmCye";
            mUSServer.Preview = false;
            mUSServer.Debug = false;
            mUSServer.Cluster = Cluster.US;



            #if SERVER_OVERRIDE
            PlayerPrefs.DeleteKey(LOCATION_KEY);
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_AUTHTOKEN_KEY);
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_DEVICEID_KEY);
            PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_USERID_KEY);
            PlayerPrefs.Save();


            ServerConfig debugServerConfig = XmlSerialization.FromResources<ServerConfig>(FileLocations.DebugServerConfig);
            SJLogger.AssertCondition(debugServerConfig != null, "Failed to read debug server config from {0}", FileLocations.DebugServerConfig);
            SJLogger.LogMessage(MessageFilter.System, "Server config read from file {0}", debugServerConfig.ToString() );
            InitialiseGS( debugServerConfig );

            #else

            if(!PlayerPrefs.HasKey( LOCATION_KEY ))
            {
                // This is a new player
                SJLogger.LogMessage( MessageFilter.System, "New player - establishing location in order to choose cluster" );

                // Clear any cached GS auth data
                PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_AUTHTOKEN_KEY);
                PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_DEVICEID_KEY);
                PlayerPrefs.DeleteKey(GameSparks.Platforms.PlatformBase.PLAYER_PREF_USERID_KEY);

                PlayerPrefs.Save();

                // Establish which cluster to connect to 
                PollLocation();
                return;
            }

            // We've previously connected to a cluster, so choose the same one again
			SJLogger.LogMessage( MessageFilter.System, "Loading location from PlayerPrefs" );

            string locationKey = PlayerPrefs.GetString( LOCATION_KEY );

            SJLogger.LogMessage( MessageFilter.System, "LOCATION_KEY: {0}", locationKey );

            switch(locationKey)
            {
                case EUROPE_KEY:
                    InitialiseGS(mEuropeServer);
                    break;
                case USA_KEY:
                    InitialiseGS(mUSServer);
                    break;
                default:
                    SJLogger.LogError("Server location key not recognised: " + locationKey);
                    break;
            }
                    
            #endif
            */
		}

        public override void OnLeaving()
        {
        }
		#endregion

		#region Private Methods
		private void PollLocation()
		{
			SJLogger.LogMessage( MessageFilter.System, "Polling location from GPS" );

			ClientLocation.PollLocation( SuccessHandler, ErrorHandler );
		}

		private void InitialiseGS( ServerConfig config )
		{
            SJLogger.LogMessage( MessageFilter.System, "Server config =  {0}", config.ToString() );

			// Initialise GameSparks
            SJGameSparksManager.Get.Initialise( config );
			TransitionState( GameStateMachine.GAME_STATE_CONNECT_TO_GS );
        }

		private bool IsUK( Vector2 geolocation )
		{
			if( geolocation.x > 50.0f && geolocation.x < 60.0f )
			{
				if( geolocation.y < 3.0f && geolocation.y > -10.0f )
				{
					return true;
				}
			}

			return false;
		}

		private bool IsFrance( Vector2 geolocation )
		{
			if( geolocation.x > 43.0f && geolocation.x < 50.0f )
			{
				if( geolocation.y < 8.0f && geolocation.y > -5.0f )
				{
					return true;
				}
			}

			return false;
		}

		private bool IsNorthernEurope( Vector2 geolocation )
		{
            // Only bother to check longitude as we've taken the game off the store for all countries other than US / Canada / Northern Europe
			if( geolocation.y < 50.0f && geolocation.y > -20.0f )
			{
				return true;
			}
                
			return false;
		}

		private void SuccessHandler( Vector2 geolocation )
		{
            // We are only in the store for US / Canada / Northern European countries now
			if( IsNorthernEurope( geolocation ) )
			{
				PlayerPrefs.SetString( LOCATION_KEY, EUROPE_KEY );
                //InitialiseGS( mEuropeServer );
			}
			else
			{
				PlayerPrefs.SetString( LOCATION_KEY, USA_KEY );
                //InitialiseGS( mUSServer );
			}
		}

		private void ErrorHandler()
		{
            UnityEngine.Debug.LogError("FindServerByLocationState.ErrorHandler - defaulting to US");
			// Default to US in the event of an error - e.g. denied access to location data
			PlayerPrefs.SetString( LOCATION_KEY, USA_KEY );
            //InitialiseGS( mUSServer );
		}
		#endregion
	}
}
