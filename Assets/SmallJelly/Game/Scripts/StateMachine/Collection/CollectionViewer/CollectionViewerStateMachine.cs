﻿using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class CollectionViewerStateMachine : StateMachine
	{
		#region Event Handlers
		public event EventHandler ReadyToTransitionToPlay
		{
			add
			{
				mReadyToTransitionToPlay += value;

				mInitializationCollectionViewerState.ReadyToTransitionToPlay += value;
				mIdleCollectionViewerState.ReadyToTransitionToPlay += value;

				mExitToNewDeckState.ReadyToTransitionToPlay += value;
				mExitToEditDeckState.ReadyToTransitionToPlay += value;
				mExitToMainMenuState.ReadyToTransitionToPlay += value;
			}

			remove
			{
				mReadyToTransitionToPlay -= value;

				mInitializationCollectionViewerState.ReadyToTransitionToPlay -= value;
				mIdleCollectionViewerState.ReadyToTransitionToPlay -= value;

				mExitToNewDeckState.ReadyToTransitionToPlay -= value;
				mExitToEditDeckState.ReadyToTransitionToPlay -= value;
				mExitToMainMenuState.ReadyToTransitionToPlay -= value;
			}
		}

		public event EventHandler mReadyToTransitionToPlay;

		public event EventHandler ReadyToTransitionToShop
		{
			add
			{
				mInitializationCollectionViewerState.ReadyToTransitionToShop += value;
				mIdleCollectionViewerState.ReadyToTransitionToShop += value;

				mExitToNewDeckState.ReadyToTransitionToShop += value;
				mExitToEditDeckState.ReadyToTransitionToShop += value;
				mExitToMainMenuState.ReadyToTransitionToShop += value;
			}

			remove
			{
				mInitializationCollectionViewerState.ReadyToTransitionToPlay -= value;
				mIdleCollectionViewerState.ReadyToTransitionToPlay -= value;

				mExitToNewDeckState.ReadyToTransitionToPlay -= value;
				mExitToEditDeckState.ReadyToTransitionToPlay -= value;
				mExitToMainMenuState.ReadyToTransitionToPlay -= value;
			}
		}

		public event EventHandler ReadyToTransitionToInbox
		{
			add
			{
				mInitializationCollectionViewerState.ReadyToTransitionToInbox += value;
				mIdleCollectionViewerState.ReadyToTransitionToInbox += value;

				mExitToNewDeckState.ReadyToTransitionToInbox += value;
				mExitToEditDeckState.ReadyToTransitionToInbox += value;
				mExitToMainMenuState.ReadyToTransitionToInbox += value;
			}

			remove
			{
				mInitializationCollectionViewerState.ReadyToTransitionToInbox -= value;
				mIdleCollectionViewerState.ReadyToTransitionToInbox -= value;

				mExitToNewDeckState.ReadyToTransitionToInbox -= value;
				mExitToEditDeckState.ReadyToTransitionToInbox -= value;
				mExitToMainMenuState.ReadyToTransitionToInbox -= value;
			}
		}
		#endregion
		
		#region Constants
		// Game states
		public const string COLLECTION_VIEWER_INITIALIZATION_STATE = "COLLECTION_VIEWER_INITIALIZATION";

		public const string COLLECTION_VIEWER_ENTER_STATE = "COLLECTION_VIEWER_ENTER_STATE"; 
		public const string COLLECTION_VIEWER_IDLE_STATE = "COLLECTION_VIEWER_IDLE"; 
		public const string COLLECTION_VIEWER_EXIT_TO_MAIN_MENU_STATE = "COLLECTION_VIEWER_EXIT_TO_MAIN_MENU_STATE"; 
		public const string COLLECTION_VIEWER_EXIT_TO_NEW_DECK_STATE = "COLLECTION_VIEWER_EXIT_TO_NEW_DECK_STATE"; 
		public const string COLLECTION_VIEWER_EXIT_TO_EDIT_DECK_STATE = "COLLECTION_VIEWER_EXIT_TO_EDIT_DECK_STATE"; 
		public const string COLLECTION_VIEWER_CLASS_SELECTION_STATE = "COLLECTION_VIEWER_CLASS_SELECTION_STATE";
		#endregion

		#region Member Variables
		private WaitForDataCollectionViewerState mInitializationCollectionViewerState;

		private IdleCollectionViewerState mIdleCollectionViewerState;

		private ClassSelectionCollectionViewerState mClassSelectionCollectionViewerState;

		private ExitToNewDeckCollectionViewerState mExitToNewDeckState;
		private ExitToEditDeckCollectionViewerState mExitToEditDeckState;
		private ExitToMainMenuCollectionViewerState mExitToMainMenuState;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			// Initialise states
			mInitializationCollectionViewerState = AddState< WaitForDataCollectionViewerState >( COLLECTION_VIEWER_INITIALIZATION_STATE );

			mIdleCollectionViewerState = AddState< IdleCollectionViewerState >( COLLECTION_VIEWER_IDLE_STATE );

			mClassSelectionCollectionViewerState = AddState< ClassSelectionCollectionViewerState >( COLLECTION_VIEWER_CLASS_SELECTION_STATE );

			mExitToNewDeckState = AddState< ExitToNewDeckCollectionViewerState >( COLLECTION_VIEWER_EXIT_TO_NEW_DECK_STATE );
			mExitToEditDeckState = AddState< ExitToEditDeckCollectionViewerState >( COLLECTION_VIEWER_EXIT_TO_EDIT_DECK_STATE );
			mExitToMainMenuState = AddState< ExitToMainMenuCollectionViewerState >( COLLECTION_VIEWER_EXIT_TO_MAIN_MENU_STATE );


			AddTransition( mInitializationCollectionViewerState.Id, mIdleCollectionViewerState.Id );
			AddTransition( mIdleCollectionViewerState.Id, mClassSelectionCollectionViewerState.Id );

			AddTransition( mClassSelectionCollectionViewerState.Id, mExitToNewDeckState.Id );
			AddTransition( mIdleCollectionViewerState.Id, mExitToMainMenuState.Id );
			AddTransition( mIdleCollectionViewerState.Id, mExitToEditDeckState.Id );

			RegisterListeners();

			//Enter the first state
			OnEnterInitializationState();
			LateUpdate();
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void OnEnterInitializationState()
		{
			StateConstructionData = new StateConstructionData( COLLECTION_VIEWER_INITIALIZATION_STATE );
		}

		private void OnEnterIdleState()
		{
			StateConstructionData = new StateConstructionData( COLLECTION_VIEWER_IDLE_STATE );
		}

		private void OnEnterEnterState()
		{
			StateConstructionData = new StateConstructionData( COLLECTION_VIEWER_ENTER_STATE );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInitializationCollectionViewerState.StateCompleted += HandleStateFinished;
			mIdleCollectionViewerState.StateCompleted += HandleStateFinished;
			mClassSelectionCollectionViewerState.StateCompleted += HandleStateFinished;

			mExitToEditDeckState.StateCompleted += HandleExitStateFinished;
			mExitToNewDeckState.StateCompleted += HandleExitStateFinished;
			mExitToMainMenuState.StateCompleted += HandleExitStateFinished;

			CollectionManager.Get.CollectionEventDataReceived += HandleCollectionEventDataReceived;
			CollectionManager.Get.CollectionError += HandleCollectionError;
			CollectionManager.Get.CollectionMenuController.CardSelection.CollectionEntityStateTransitioned += HandleCollectionEntityStateTransitioned;
		}

		private void UnregisterListeners()
		{
			mInitializationCollectionViewerState.StateCompleted -= HandleStateFinished;
			mIdleCollectionViewerState.StateCompleted -= HandleStateFinished;
			mClassSelectionCollectionViewerState.StateCompleted -= HandleStateFinished;

			mExitToEditDeckState.StateCompleted -= HandleExitStateFinished;
			mExitToMainMenuState.StateCompleted -= HandleExitStateFinished;
			mExitToNewDeckState.StateCompleted -= HandleExitStateFinished;

			//Prevent erroneously throwing errors when the game is shutting down
			if( CollectionManager.Get != null )
			{
				CollectionManager.Get.CollectionEventDataReceived -= HandleCollectionEventDataReceived;
				CollectionManager.Get.CollectionError -= HandleCollectionError;
				CollectionManager.Get.CollectionMenuController.CardSelection.CollectionEntityStateTransitioned -= HandleCollectionEntityStateTransitioned;
			}
		}
		#endregion


		#region Event Handlers
		private void HandleCollectionEventDataReceived( object o, CollectionEventDataEventArgs collectionEventDataEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is CollectionManagementState, "State ({0}) must be a collection viewer state", CurrentState.GetType().ToString() );
			((CollectionManagementState)CurrentState).OnCollectionEventDataReceived( collectionEventDataEventArgs.CollectionEventData );

		}

		private void HandleCollectionError( object o, EventArgs args )
		{
			if( mReadyToTransitionToPlay != null )
			{
				mReadyToTransitionToPlay( this, EventArgs.Empty );
			}
		}

		private void HandleCollectionEntityStateTransitioned( object o, CollectionItemAndStateTransitionEventArgs collectionEntityAndStateTransitionEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is CollectionManagementState, "State ({0}) must be a collection viewer state", CurrentState.GetType().ToString() );
			((CollectionManagementState)CurrentState).OnCollectionEntityStateTransitioned( collectionEntityAndStateTransitionEventArgs.CollectionEntity, collectionEntityAndStateTransitionEventArgs.PreviousState, collectionEntityAndStateTransitionEventArgs.NewState, collectionEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		private void HandleStateFinished (State state, StateConstructionData messageData)
		{
			if( messageData is EmptyStateConstructionData )
			{
				OnEnterIdleState();
			}
			else
			{
				StateConstructionData = (StateConstructionData)messageData;
			}
		}

		private void HandleExitStateFinished ( State state, StateConstructionData stateConstructionData )
		{
			OnStateMachineCompleted( stateConstructionData );
		}
		#endregion
	}
}
