﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class WaitForDataCollectionViewerState : IdleCollectionViewerState
	{
		#region Member Variables
		private GetCardsCollectionEventData mGetCardsCollectionEventData;
		private GetDecksCollectionEventData mGetDecksCollectionEventData;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			//Show deck selection controller and card selection controller - ready for the pages/decks to be retrieved
			UIManager.Get.ShowMenu< DeckListController >();
			UIManager.Get.ShowMenu< CardSelection >();
			UIManager.Get.ShowMenu< CollectionFilterList >();

			//Filter the collection contents by class
			CollectionFilterList collectionFilterList = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = collectionFilterList.CollectionFilterData;
			collectionFilterListData.SelectedPageIndex = 0;

			collectionFilterListData.SelectedComponentTypeIndex = 0;
			collectionFilterListData.ComponentTypeOptions = new string[]{ "any", "Weapon", "Nanotech", "Utility" };

			collectionFilterListData.SelectedShipClassIndex = 0;
			collectionFilterListData.ShipClassOptions = new string[]{ ShipClass.Neutral, ShipClass.Ancient, ShipClass.Enforcer, ShipClass.Smuggler };

			collectionFilterListData.SelectedPowerCostIndex = 0;
			collectionFilterListData.PowerCostOptions = new string[]{ "any", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

			collectionFilterList.Refresh( collectionFilterListData, true );

			CollectionManager.Get.ListDecks();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		public override void OnCollectionEventDataReceived( CollectionEventData collectionEventData )
		{
			//Let's store the data, waiting for all of it to be reived before we trigger it. This will force the data to refresh the UI
			//all at the same time
			if( collectionEventData is GetCardsCollectionEventData )
			{
				mGetCardsCollectionEventData = (GetCardsCollectionEventData)collectionEventData;
			}

			if( collectionEventData is GetDecksCollectionEventData )
			{
				mGetDecksCollectionEventData = (GetDecksCollectionEventData)collectionEventData;
			}

			if( mGetCardsCollectionEventData != null && mGetDecksCollectionEventData != null )
			{
				base.OnCollectionEventDataReceived( mGetCardsCollectionEventData );
				base.OnCollectionEventDataReceived( mGetDecksCollectionEventData );

				DeckListController deckSelectionController = CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController;
				deckSelectionController.OnInsertState( DeckListController.State.MoveIn );

				CollectionFilterList collectionFilterList = CollectionManager.Get.CollectionMenuController.CollectionFilterList;
				collectionFilterList.OnInsertState( CollectionFilterListController.State.MoveIn );

				ExitToEnterState();
			}
		}
		#endregion

		#region Private Methods
		private void ExitToEnterState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( CollectionViewerStateMachine.COLLECTION_VIEWER_IDLE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}