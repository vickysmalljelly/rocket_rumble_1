﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ExitToEditDeckCollectionViewerState : ExitCollectionViewerState
	{
		#region Member Variables
		private string mDeckId;
        private string mClassId; 
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

            SJLogger.AssertCondition( stateConstructionData is TypedStateConstructionData<DeckIdAndClass>, "If we're transitioning to edit a deck then we need a supplied string so we know what the deck id is!" );
		
            TypedStateConstructionData<DeckIdAndClass> scd = (TypedStateConstructionData<DeckIdAndClass>)stateConstructionData;
            mDeckId = scd.Value.DeckId;
            mClassId = scd.Value.ClassName;
		}
		#endregion

		#region Event Handlers
		protected override void HandleAnimationFinished( object o, EventArgs args )
		{
            DeckIdAndClass diac = new DeckIdAndClass();
            diac.DeckId = mDeckId;
            diac.ClassName = mClassId;

            OnStateCompleted(new TypedStateConstructionData<DeckIdAndClass>(CollectionStateMachine.EDIT_DECK_BUILDER_STATE,diac));
		}
		#endregion
	}
}