﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ClassSelectionCollectionViewerState : CollectionManagementState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			UIManager.Get.ShowMenu< ClassSelectionController >();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			UIManager.Get.HideMenu< ClassSelectionController >();
		}
		#endregion

		#region Private Methods
		private void TransitionToExitToNewDeckBuildingState( string className )
		{
			StateConstructionData stateConstructionMessageData = new StringStateConstructionData( CollectionViewerStateMachine.COLLECTION_VIEWER_EXIT_TO_NEW_DECK_STATE, className );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ClassSelectionController classSelectionController = UIManager.Get.GetMenuController< ClassSelectionController >();
			classSelectionController.EnforcerClicked += HandleEnforcerClicked;
			classSelectionController.SmugglerClicked += HandleSmugglerClicked;
			classSelectionController.AncientClicked += HandleAncientClicked;
		}

		private void UnregisterListeners()
		{
			ClassSelectionController classSelectionController = UIManager.Get.GetMenuController< ClassSelectionController >();
			classSelectionController.EnforcerClicked -= HandleEnforcerClicked;
			classSelectionController.SmugglerClicked -= HandleSmugglerClicked;
			classSelectionController.AncientClicked -= HandleAncientClicked;
		}
		#endregion

		#region Event Handlers
		private void HandleEnforcerClicked( object o, EventArgs args )
		{
			TransitionToExitToNewDeckBuildingState( ShipClass.Enforcer );
		}

		private void HandleSmugglerClicked( object o, EventArgs args )
		{
			TransitionToExitToNewDeckBuildingState( ShipClass.Smuggler );
		}

		private void HandleAncientClicked( object o, EventArgs args )
		{
			TransitionToExitToNewDeckBuildingState( ShipClass.Ancient );
		}
		#endregion
	}
}