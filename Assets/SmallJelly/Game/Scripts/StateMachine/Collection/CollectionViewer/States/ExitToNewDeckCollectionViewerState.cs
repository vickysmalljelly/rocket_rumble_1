﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ExitToNewDeckCollectionViewerState : ExitCollectionViewerState
	{
		#region Member Variables
		private string mChosenClass;
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to edit a deck then we need a supplied string so we know what the deck id is!" );

			mChosenClass = ( ( StringStateConstructionData ) stateConstructionData ).String;
		}
		#endregion

		#region Event Handlers
		protected override void HandleAnimationFinished( object o, EventArgs args )
		{
			OnStateCompleted( new StringStateConstructionData( CollectionStateMachine.NEW_DECK_BUILDER_STATE, mChosenClass ) );
		}
		#endregion
	}
}