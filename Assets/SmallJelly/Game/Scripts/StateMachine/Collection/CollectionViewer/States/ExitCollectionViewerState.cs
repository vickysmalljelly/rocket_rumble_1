﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class ExitCollectionViewerState : CollectionManagementState
	{
		#region Member Variables
		private DeckListController mDeckSelectionController;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mDeckSelectionController = CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController;

			RegisterListeners();

			mDeckSelectionController.OnInsertState( DeckListController.State.MoveOut );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			//Hide deck selection controller and card selection controller
			UIManager.Get.HideMenu< DeckListController >();
			UIManager.Get.HideMenu< CardSelection >();
			UIManager.Get.HideMenu< CollectionFilterList >();

		}
		#endregion

		#region Event Registrations
		private void RegisterListeners()
		{
			mDeckSelectionController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mDeckSelectionController.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		protected abstract void HandleAnimationFinished( object o, EventArgs args );

		private void HandleServerError( ServerError error )
		{
			if( error.ErrorType == ServerErrorType.Timeout )
			{
				SJLogger.LogError("TODO: Handle timeout");
			}
		}
		#endregion
	}
}