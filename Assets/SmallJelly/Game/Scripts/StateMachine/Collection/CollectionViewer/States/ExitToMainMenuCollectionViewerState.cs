﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ExitToMainMenuCollectionViewerState : ExitCollectionViewerState
	{
		#region Event Handlers
		protected override void HandleAnimationFinished( object o, EventArgs args )
		{
			OnStateCompleted( new StateConstructionData( CollectionStateMachine.EXIT_STATE ) );
		}
		#endregion
	}
}