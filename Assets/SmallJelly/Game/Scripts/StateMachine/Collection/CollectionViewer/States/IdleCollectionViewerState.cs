﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class IdleCollectionViewerState : CollectionManagementState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Private Methods
        private void TransitionToExitToEditDeckBuildingState(string deckId, string className)
		{
            DeckIdAndClass diac = new DeckIdAndClass();
            diac.DeckId = deckId;
            diac.ClassName = className;

            TypedStateConstructionData<DeckIdAndClass> stateConstructionMessageData = new TypedStateConstructionData<DeckIdAndClass>(CollectionViewerStateMachine.COLLECTION_VIEWER_EXIT_TO_EDIT_DECK_STATE, diac);
			OnStateCompleted( stateConstructionMessageData );
		}

		private void TransitionToExitToDeckBuildingState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( CollectionViewerStateMachine.COLLECTION_VIEWER_CLASS_SELECTION_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void TransitionToExitToMainMenuState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( CollectionViewerStateMachine.COLLECTION_VIEWER_EXIT_TO_MAIN_MENU_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.NewDeckPressed += HandleNewDeckPressed;
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.EditDeckPressed += HandleEditDeckPressed;
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.BackPressed += HandleBackPressed;
		}

		private void UnregisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.NewDeckPressed -= HandleNewDeckPressed;
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.EditDeckPressed -= HandleEditDeckPressed;
			CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController.BackPressed -= HandleBackPressed;
		}
		#endregion

		#region Event Handlers
		private void HandleNewDeckPressed( object o, EventArgs eventArgs )
		{
			TransitionToExitToDeckBuildingState();
		}

        private void HandleEditDeckPressed(string deckId, string className)
		{
            TransitionToExitToEditDeckBuildingState(deckId, className);
		}

		private void HandleBackPressed( object o, EventArgs eventArgs )
		{
			TransitionToExitToMainMenuState();
		}
		#endregion
	}
}