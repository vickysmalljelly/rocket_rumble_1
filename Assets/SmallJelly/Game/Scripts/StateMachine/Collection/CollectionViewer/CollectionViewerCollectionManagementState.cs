﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	/// <summary>
	/// State for viewing the users current collection of cards
	/// </summary>
	public sealed class CollectionViewerState : AbstractCollectionState
	{	
		#region Member Variables
		private CollectionViewerStateMachine mCollectionViewerStateMachine;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mCollectionViewerStateMachine = this.AddChildStateMachine< CollectionViewerStateMachine >();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Register Listeners
		private void RegisterListeners()
		{
			mCollectionViewerStateMachine.StateMachineCompleted += HandleStateMachineCompleted;

			mCollectionViewerStateMachine.ReadyToTransitionToPlay += HandleReadyToTransitionToRumble;
			mCollectionViewerStateMachine.ReadyToTransitionToInbox += HandleReadyToTransitionToInbox;
			mCollectionViewerStateMachine.ReadyToTransitionToShop += HandleReadyToTransitionToShop;
		}

		private void UnregisterListeners()
		{
			mCollectionViewerStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			mCollectionViewerStateMachine.ReadyToTransitionToPlay -= HandleReadyToTransitionToRumble;
			mCollectionViewerStateMachine.ReadyToTransitionToInbox -= HandleReadyToTransitionToInbox;
			mCollectionViewerStateMachine.ReadyToTransitionToShop -= HandleReadyToTransitionToShop;
		}
		#endregion

		#region Event Handlers
		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
		{
			//Finish state
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}