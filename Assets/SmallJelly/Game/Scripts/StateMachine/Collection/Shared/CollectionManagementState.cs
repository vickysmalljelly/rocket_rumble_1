﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class CollectionManagementState : StateMachine.State
	{
		#region Event Handlers
		public event EventHandler ReadyToTransitionToPlay;
		public event EventHandler ReadyToTransitionToShop;
		public event EventHandler ReadyToTransitionToInbox;
		#endregion

        private int mPreviousClassIndex;

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public virtual void OnCollectionEventDataReceived( CollectionEventData collectionEventData )
		{
			if( collectionEventData is GetCardsCollectionEventData )
			{
				GetCardsCollectionEventData getCardsCollectionEventData = (GetCardsCollectionEventData) collectionEventData;

				//Refresh the card selection
				CardSelection cardSelection = CollectionManager.Get.CollectionMenuController.CardSelection;
                cardSelection.RefreshPage( getCardsCollectionEventData.Direction, getCardsCollectionEventData.Page, getCardsCollectionEventData.NumberOfPages, getCardsCollectionEventData.Cards );


				//Refresh the filter selection
				CollectionFilterList filterSelection = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

				CollectionFilterData collectionFilterListData = filterSelection.CollectionFilterData;
				collectionFilterListData.SelectedPageIndex = getCardsCollectionEventData.Page;
				collectionFilterListData.NumberOfPages = getCardsCollectionEventData.NumberOfPages;

				//We don't need to alert the server of any data change because the data change has been triggered by the server
				//not the client!
				filterSelection.Refresh( collectionFilterListData, false );
			}

			if( collectionEventData is GetDecksCollectionEventData )
			{
				GetDecksCollectionEventData getDecksCollectionEventData = (GetDecksCollectionEventData) collectionEventData;

				DeckListController deckSelectionController = CollectionManager.Get.CollectionMenuController.DeckSelectionCollectionDisplayController;
				deckSelectionController.OnInsertState( DeckListController.State.Downloaded );

				deckSelectionController.Refresh( getDecksCollectionEventData.Decks );
			}
		}

		//Allows the state to react in response to battle entity state transitions
		public virtual void OnCollectionEntityStateTransitioned( CollectionItem collectionEntity, CollectionItemController.State previousState, CollectionItemController.State newState, SJRenderRaycastHit[] hitLayers )
		{

		}
		#endregion

		#region Private Methods
		private void GetPageWithFilter( CollectionFilterData collectionFilterData )
		{
			string shipClass = collectionFilterData.ShipClassOptions[ collectionFilterData.SelectedShipClassIndex ];
			string componentType = collectionFilterData.ComponentTypeOptions[ collectionFilterData.SelectedComponentTypeIndex ];
		
			int numberOfElementsPerPage = collectionFilterData.NumberOfElementsPerPage;
			int pageIndex = collectionFilterData.SelectedPageIndex;

			//If there is no cost in the string then we consider it to be "all"
			string selectedPowerCost = collectionFilterData.PowerCostOptions[ collectionFilterData.SelectedPowerCostIndex ];
			int powerCost;
			if( !int.TryParse( selectedPowerCost, out powerCost ) )
			{
				powerCost = -1;	
			}

            CollectionManager.Get.GetCards( collectionFilterData.Direction, shipClass, componentType, powerCost, numberOfElementsPerPage, pageIndex );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();

			frontEndNavigationController.RumbleClicked += HandleRumbleClicked;
			frontEndNavigationController.ShopClicked += HandleShopClicked;
			frontEndNavigationController.InboxClicked += HandleInboxClicked;

			CollectionFilterList filterSelection = UIManager.Get.GetMenuController< CollectionFilterList >();
			filterSelection.FiltersUpdated += HandleRetrievePage;
			filterSelection.ClassFilterSelected += HandleClassFilterSelected;
			filterSelection.PowerCostFilterSelected += HandlePowerCostFilterSelected;

			filterSelection.PageLeft += HandlePageLeft;
			filterSelection.PageRight += HandlePageRight;
		}

		private void UnregisterListeners()
		{
			FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();
		
			frontEndNavigationController.RumbleClicked -= HandleRumbleClicked;
			frontEndNavigationController.ShopClicked -= HandleShopClicked;
			frontEndNavigationController.InboxClicked -= HandleInboxClicked;

			CollectionFilterList filterSelection = UIManager.Get.GetMenuController< CollectionFilterList >();
			filterSelection.FiltersUpdated -= HandleRetrievePage;
			filterSelection.ClassFilterSelected -= HandleClassFilterSelected;
			filterSelection.PowerCostFilterSelected -= HandlePowerCostFilterSelected;

			filterSelection.PageLeft -= HandlePageLeft;
			filterSelection.PageRight -= HandlePageRight;
		}
		#endregion

		#region Event Firing
		protected void FireReadyToTransitionToRumble()
		{
			if( ReadyToTransitionToPlay != null )
			{
				ReadyToTransitionToPlay( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToShop()
		{
			if( ReadyToTransitionToShop != null )
			{
				ReadyToTransitionToShop( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToInbox()
		{
			if( ReadyToTransitionToInbox != null )
			{
				ReadyToTransitionToInbox( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleRetrievePage( object o, CollectionFilterEventArgs collectionFilterEventArgs )
		{
			GetPageWithFilter( collectionFilterEventArgs.CollectionFilterData );
		}

        private void HandleClassFilterSelected( string className )
		{
			CollectionFilterList filterSelection = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = filterSelection.CollectionFilterData;
            int index = Array.IndexOf( collectionFilterListData.ShipClassOptions, className );
			collectionFilterListData.SelectedShipClassIndex = index;
			collectionFilterListData.SelectedPageIndex = 0;

            // Work out if we are moving right or left through the classes
            if(index == 0 && mPreviousClassIndex == (collectionFilterListData.ShipClassOptions.Length - 1))
            {
                collectionFilterListData.Direction = CollectionManager.Direction.Left;
            }
            else if(index < mPreviousClassIndex)
            {
                collectionFilterListData.Direction = CollectionManager.Direction.Left;
            }
            else
            {
                collectionFilterListData.Direction = CollectionManager.Direction.Right;
            }

            mPreviousClassIndex = index;

			filterSelection.Refresh( collectionFilterListData, true );
		}

		private void HandlePowerCostFilterSelected( object o, StringEventArgs args )
		{
			CollectionFilterList filterSelection = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = filterSelection.CollectionFilterData;
			int index = Array.IndexOf( collectionFilterListData.PowerCostOptions, args.String );
			collectionFilterListData.SelectedPowerCostIndex = index;
			collectionFilterListData.SelectedPageIndex = 0;
            collectionFilterListData.Direction = CollectionManager.Direction.Right;

			filterSelection.Refresh( collectionFilterListData, true );
		}

		private void HandlePageLeft( object o, EventArgs args )
		{

			CollectionFilterList filterSelection = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = filterSelection.CollectionFilterData;
			collectionFilterListData.SelectedPageIndex--;
            collectionFilterListData.Direction = CollectionManager.Direction.Left;

			if( collectionFilterListData.SelectedPageIndex < 0 )
			{
				//Access the last page of the next class
				collectionFilterListData.SelectedPageIndex = -1;
				collectionFilterListData.SelectedShipClassIndex = MathsExtensions.Modulo( collectionFilterListData.SelectedShipClassIndex - 1, Mathf.Max( 1, collectionFilterListData.ShipClassOptions.Length ) );
			}

			filterSelection.Refresh( collectionFilterListData, true );
		}

		private void HandlePageRight( object o, EventArgs args )
		{
			CollectionFilterList filterSelection = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = filterSelection.CollectionFilterData;
			collectionFilterListData.SelectedPageIndex++;
            collectionFilterListData.Direction = CollectionManager.Direction.Right;

			if( collectionFilterListData.SelectedPageIndex >= collectionFilterListData.NumberOfPages )
			{
				//Access the first page of the next class
				collectionFilterListData.SelectedPageIndex = 0;
				collectionFilterListData.SelectedShipClassIndex = MathsExtensions.Modulo( collectionFilterListData.SelectedShipClassIndex + 1, Mathf.Max( 1, collectionFilterListData.ShipClassOptions.Length ) );

			}

			filterSelection.Refresh( collectionFilterListData, true );
		}

		protected virtual void HandleRumbleClicked( object o, EventArgs args )
		{
			FireReadyToTransitionToRumble();
		}

		protected virtual void HandleShopClicked( object o, EventArgs args )
		{
			FireReadyToTransitionToShop();
		}

		protected virtual void HandleInboxClicked( object o, EventArgs args )
		{
			FireReadyToTransitionToInbox();
		}
		#endregion
	}
}