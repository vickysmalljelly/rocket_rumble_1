﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SmallJelly
{
	public class NewDeckCompleteDeckBuilderState : CompleteDeckBuilderState
	{
		#region Private Methods
		protected override void CreateDeck()
		{
			Deck deck = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.Deck;
			List< string > cards = deck.BattleEntities.Select( o => o.Id ).ToList();

			//Let's use an ancient class by default until we've got class selection etc in the deck building flow
			CollectionManager.Get.NewDeck( deck.DisplayName, deck.Class, cards );
		}
		#endregion
	}
}