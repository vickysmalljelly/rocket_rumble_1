﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class NewDeckBuilderStateMachine : DeckBuilderStateMachine
	{
		#region MonoBehaviour Methods
		protected override void Awake()
		{
			EnterDeckBuilderState = AddState< NewDeckEnterDeckBuilderState >( DECK_BUILDER_ENTER_STATE );
			CompleteDeckBuilderState = AddState< NewDeckCompleteDeckBuilderState >( DECK_BUILDER_COMPLETE_STATE );

			base.Awake();

			AddTransition( EnterDeckBuilderState.Id, IdleDeckBuilderState.Id );
		}
		#endregion
	}
}
