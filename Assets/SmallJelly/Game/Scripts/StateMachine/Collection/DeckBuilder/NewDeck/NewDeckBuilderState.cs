﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class NewDeckBuilderState : DeckBuilderState
	{
		#region Member Variables
		private string mClassId;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			DeckBuilderStateMachine = this.AddChildStateMachine< NewDeckBuilderStateMachine >();

			NewDeckBuilderStateMachine newDeckBuilderStateMachine = (NewDeckBuilderStateMachine) DeckBuilderStateMachine;
			newDeckBuilderStateMachine.OnConstruct( new StringStateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_ENTER_STATE, mClassId ) );

            // Lock the class tabs
            CollectionFilterList controller = UIManager.Get.GetMenuController<CollectionFilterList>();
            controller.SetEditingDeck(mClassId);           

			base.OnEntering();
		}

		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to create a deck then we need a supplied string so we know what the class is!" );

			mClassId = ( ( StringStateConstructionData ) stateConstructionData ).String;
		}

        public override void OnLeaving()
        {
            // Unlock the class tabs
            CollectionFilterList controller = UIManager.Get.GetMenuController<CollectionFilterList>();
            controller.ClearEditingDeck();

            base.OnLeaving();
        }
		#endregion
	}
}