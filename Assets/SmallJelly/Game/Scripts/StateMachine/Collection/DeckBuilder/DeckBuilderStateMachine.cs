﻿using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class DeckBuilderStateMachine : StateMachine
	{
		#region Event Handlers
		public event EventHandler ReadyToTransitionToPlay
		{
			add
			{
				mReadyToTransitionToPlay += value;

				EnterDeckBuilderState.ReadyToTransitionToPlay += value;
				IdleDeckBuilderState.ReadyToTransitionToPlay += value;

				ExitDeckBuilderState.ReadyToTransitionToPlay += value;
				CompleteDeckBuilderState.ReadyToTransitionToPlay += value;
				UploadingDeckBuilderState.ReadyToTransitionToPlay += value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToPlay += value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToPlay += value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToPlay += value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToPlay += value;
			}

			remove
			{
				mReadyToTransitionToPlay -= value;

				EnterDeckBuilderState.ReadyToTransitionToPlay -= value;
				IdleDeckBuilderState.ReadyToTransitionToPlay -= value;

				ExitDeckBuilderState.ReadyToTransitionToPlay -= value;
				CompleteDeckBuilderState.ReadyToTransitionToPlay -= value;
				UploadingDeckBuilderState.ReadyToTransitionToPlay -= value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToPlay -= value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToPlay -= value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToPlay -= value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToPlay -= value;
			}
		}

		public event EventHandler mReadyToTransitionToPlay;

		public event EventHandler ReadyToTransitionToShop
		{
			add
			{
				EnterDeckBuilderState.ReadyToTransitionToShop += value;
				IdleDeckBuilderState.ReadyToTransitionToShop += value;

				ExitDeckBuilderState.ReadyToTransitionToShop += value;
				CompleteDeckBuilderState.ReadyToTransitionToShop += value;
				UploadingDeckBuilderState.ReadyToTransitionToShop += value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToShop += value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToShop += value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToShop += value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToShop += value;
			}

			remove
			{
				EnterDeckBuilderState.ReadyToTransitionToShop -= value;
				IdleDeckBuilderState.ReadyToTransitionToShop -= value;

				ExitDeckBuilderState.ReadyToTransitionToShop -= value;
				CompleteDeckBuilderState.ReadyToTransitionToShop -= value;
				UploadingDeckBuilderState.ReadyToTransitionToShop -= value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToShop -= value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToShop -= value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToShop -= value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToShop -= value;
			}
		}

		public event EventHandler ReadyToTransitionToInbox
		{

			add
			{
				EnterDeckBuilderState.ReadyToTransitionToInbox += value;
				IdleDeckBuilderState.ReadyToTransitionToInbox += value;

				ExitDeckBuilderState.ReadyToTransitionToInbox += value;
				CompleteDeckBuilderState.ReadyToTransitionToInbox += value;
				UploadingDeckBuilderState.ReadyToTransitionToInbox += value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToInbox += value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToInbox += value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToInbox += value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToInbox += value;
			}

			remove
			{
				EnterDeckBuilderState.ReadyToTransitionToInbox -= value;
				IdleDeckBuilderState.ReadyToTransitionToInbox -= value;

				ExitDeckBuilderState.ReadyToTransitionToInbox -= value;
				CompleteDeckBuilderState.ReadyToTransitionToInbox -= value;
				UploadingDeckBuilderState.ReadyToTransitionToInbox -= value;

				ConfirmExitToCollectionViewerDeckBuilderState.ReadyToTransitionToInbox -= value;
				ConfirmExitToRumbleDeckBuilderState.ReadyToTransitionToInbox -= value;
				ConfirmExitToShopDeckBuilderState.ReadyToTransitionToInbox -= value;
				ConfirmExitToInboxDeckBuilderState.ReadyToTransitionToInbox -= value;
			}
		}
		#endregion

		#region Constants
		// Game states
		public const string DECK_BUILDER_INITIALIZATION_STATE = "DECK_BUILDER_INITIALIZATION"; 
		public const string DECK_BUILDER_ENTER_STATE = "DECK_BUILDER_ENTER_STATE"; 
		public const string DECK_BUILDER_IDLE_STATE = "DECK_BUILDER_IDLE"; 
		public const string DECK_BUILDER_COMPLETE_STATE = "DECK_BUILDER_COMPLETE"; 
		public const string DECK_BUILDER_EXIT_STATE = "DECK_BUILDER_EXIT_STATE"; 

		public const string DECK_BUILDER_CONFIRM_EXIT_TO_RUMBLE_STATE = "DECK_BUILDER_CONFIRM_EXIT_TO_RUMBLE_STATE"; 
		public const string DECK_BUILDER_CONFIRM_EXIT_TO_SHOP_STATE = "DECK_BUILDER_CONFIRM_EXIT_TO_SHOP_STATE"; 
		public const string DECK_BUILDER_CONFIRM_EXIT_TO_INBOX_STATE = "DECK_BUILDER_CONFIRM_EXIT_TO_INBOX_STATE"; 
		public const string DECK_BUILDER_CONFIRM_EXIT_TO_COLLECTION_VIEWER_STATE = "DECK_BUILDER_CONFIRM_EXIT_TO_COLLECTION_VIEWER_STATE";

		public const string DECK_BUILDER_UPLOADING_DECK_STATE = "DECK_BUILDER_UPLOADING_DECK_STATE"; 
		#endregion

		#region Protected Properties
		protected EnterDeckBuilderState EnterDeckBuilderState { get; set; }

		protected IdleDeckBuilderState IdleDeckBuilderState { get; set; }
		protected ExitDeckBuilderState ExitDeckBuilderState { get; set; }
		protected CompleteDeckBuilderState CompleteDeckBuilderState { get; set; }

		protected ConfirmExitToRumbleDeckBuilderState ConfirmExitToRumbleDeckBuilderState { get; set; }
		protected ConfirmExitToShopDeckBuilderState ConfirmExitToShopDeckBuilderState { get; set; }
		protected ConfirmExitToInboxDeckBuilderState ConfirmExitToInboxDeckBuilderState { get; set; }
		protected ConfirmExitToCollectionViewerDeckBuilderState ConfirmExitToCollectionViewerDeckBuilderState { get; set; }

		protected UploadingDeckBuilderState UploadingDeckBuilderState { get; set; }
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			// Initialise states
			IdleDeckBuilderState = AddState< IdleDeckBuilderState >( DECK_BUILDER_IDLE_STATE );
			ExitDeckBuilderState = AddState< ExitDeckBuilderState >( DECK_BUILDER_EXIT_STATE );

			UploadingDeckBuilderState = AddState< UploadingDeckBuilderState >( DECK_BUILDER_UPLOADING_DECK_STATE );

			ConfirmExitToRumbleDeckBuilderState = AddState< ConfirmExitToRumbleDeckBuilderState >( DECK_BUILDER_CONFIRM_EXIT_TO_RUMBLE_STATE );
			ConfirmExitToShopDeckBuilderState = AddState< ConfirmExitToShopDeckBuilderState >( DECK_BUILDER_CONFIRM_EXIT_TO_SHOP_STATE );
			ConfirmExitToInboxDeckBuilderState = AddState< ConfirmExitToInboxDeckBuilderState >( DECK_BUILDER_CONFIRM_EXIT_TO_INBOX_STATE );
			ConfirmExitToCollectionViewerDeckBuilderState = AddState< ConfirmExitToCollectionViewerDeckBuilderState >( DECK_BUILDER_CONFIRM_EXIT_TO_COLLECTION_VIEWER_STATE );

			AddTransition( IdleDeckBuilderState.Id, CompleteDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, IdleDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, ExitDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, UploadingDeckBuilderState.Id );

			AddTransition( IdleDeckBuilderState.Id, ConfirmExitToRumbleDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, ConfirmExitToRumbleDeckBuilderState.Id );
			AddTransition( ConfirmExitToRumbleDeckBuilderState.Id, IdleDeckBuilderState.Id );

			AddTransition( IdleDeckBuilderState.Id, ConfirmExitToShopDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, ConfirmExitToShopDeckBuilderState.Id );
			AddTransition( ConfirmExitToShopDeckBuilderState.Id, IdleDeckBuilderState.Id );

			AddTransition( IdleDeckBuilderState.Id, ConfirmExitToInboxDeckBuilderState.Id );
			AddTransition( CompleteDeckBuilderState.Id, ConfirmExitToInboxDeckBuilderState.Id );
			AddTransition( ConfirmExitToInboxDeckBuilderState.Id, IdleDeckBuilderState.Id );

			AddTransition( IdleDeckBuilderState.Id, ConfirmExitToCollectionViewerDeckBuilderState.Id );
			AddTransition( ConfirmExitToCollectionViewerDeckBuilderState.Id, IdleDeckBuilderState.Id );
			AddTransition( ConfirmExitToCollectionViewerDeckBuilderState.Id, ExitDeckBuilderState.Id );

			AddTransition( IdleDeckBuilderState.Id, ExitDeckBuilderState.Id );

			RegisterListeners();
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public void OnConstruct( StateConstructionData stateConstrucitonData )
		{
			StateConstructionData = stateConstrucitonData;
			LateUpdate();
		}
		#endregion

		#region Private Methods
		private void OnEnterIdleState()
		{
			StateConstructionData = new StateConstructionData( DECK_BUILDER_IDLE_STATE );
		}

		private void OnEnterExitToDeckBuilderState()
		{
			StateConstructionData = new StateConstructionData( DECK_BUILDER_UPLOADING_DECK_STATE );
		}
		#endregion

		#region Event Registration
		protected virtual void RegisterListeners()
		{
			EnterDeckBuilderState.StateCompleted += HandleStateFinished;
			IdleDeckBuilderState.StateCompleted += HandleStateFinished;
			CompleteDeckBuilderState.StateCompleted += HandleStateFinished;

			ConfirmExitToRumbleDeckBuilderState.StateCompleted += HandleStateFinished;
			ConfirmExitToShopDeckBuilderState.StateCompleted += HandleStateFinished;
			ConfirmExitToInboxDeckBuilderState.StateCompleted += HandleStateFinished;
			ConfirmExitToCollectionViewerDeckBuilderState.StateCompleted += HandleStateFinished;

			UploadingDeckBuilderState.StateCompleted += HandleUploadingDeckStateFinished;

			ExitDeckBuilderState.StateCompleted += HandleExitStateFinished;

			CollectionManager.Get.CollectionEventDataReceived += HandleCollectionEventDataReceived;
			CollectionManager.Get.CollectionError += HandleCollectionError;
			CollectionManager.Get.CollectionMenuController.CardSelection.CollectionEntityStateTransitioned += HandleCollectionEntityStateTransitioned;
		}

		protected virtual void UnregisterListeners()
		{
			EnterDeckBuilderState.StateCompleted -= HandleStateFinished;
			IdleDeckBuilderState.StateCompleted -= HandleStateFinished;
			CompleteDeckBuilderState.StateCompleted -= HandleStateFinished;

			ConfirmExitToRumbleDeckBuilderState.StateCompleted -= HandleStateFinished;
			ConfirmExitToShopDeckBuilderState.StateCompleted -= HandleStateFinished;
			ConfirmExitToInboxDeckBuilderState.StateCompleted -= HandleStateFinished;
			ConfirmExitToCollectionViewerDeckBuilderState.StateCompleted -= HandleStateFinished;

			UploadingDeckBuilderState.StateCompleted -= HandleUploadingDeckStateFinished;

			ExitDeckBuilderState.StateCompleted -= HandleExitStateFinished;

			//Prevent erroneously throwing errors when the game is shutting down
			if( CollectionManager.Get != null )
			{
				CollectionManager.Get.CollectionEventDataReceived -= HandleCollectionEventDataReceived;
				CollectionManager.Get.CollectionError -= HandleCollectionError;
				CollectionManager.Get.CollectionMenuController.CardSelection.CollectionEntityStateTransitioned -= HandleCollectionEntityStateTransitioned;
			}
		}
		#endregion


		#region Event Handlers
		private void HandleCollectionEventDataReceived( object o, CollectionEventDataEventArgs collectionEventDataEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is CollectionManagementState, "State ({0}) must be a collection viewer state", CurrentState.GetType().ToString() );
			((CollectionManagementState)CurrentState).OnCollectionEventDataReceived( collectionEventDataEventArgs.CollectionEventData );

		}

		private void HandleCollectionError( object o, EventArgs args )
		{
			if( mReadyToTransitionToPlay != null )
			{
				mReadyToTransitionToPlay( this, EventArgs.Empty );
			}
		}

		private void HandleCollectionEntityStateTransitioned( object o, CollectionItemAndStateTransitionEventArgs collectionEntityAndStateTransitionEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is CollectionManagementState, "State must be a collection viewer state");
			((CollectionManagementState)CurrentState).OnCollectionEntityStateTransitioned( collectionEntityAndStateTransitionEventArgs.CollectionEntity, collectionEntityAndStateTransitionEventArgs.PreviousState, collectionEntityAndStateTransitionEventArgs.NewState, collectionEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		protected void HandleStateFinished (State state, StateConstructionData messageData)
		{
			if( messageData is EmptyStateConstructionData )
			{
				OnEnterIdleState();
			}
			else
			{
				StateConstructionData = (StateConstructionData)messageData;
			}
		}

		private void HandleUploadingDeckStateFinished( State state, StateConstructionData stateConstructionData )
		{
			OnEnterExitToDeckBuilderState();
		}

		private void HandleExitStateFinished (State state, StateConstructionData messageData)
		{
			OnStateMachineCompleted();
		}
		#endregion
	}
}
