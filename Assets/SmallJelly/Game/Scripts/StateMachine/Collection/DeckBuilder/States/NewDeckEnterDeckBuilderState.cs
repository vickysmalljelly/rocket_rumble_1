﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class NewDeckEnterDeckBuilderState : EnterDeckBuilderState
	{
		#region Member Variables
		private string mShipClass;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			//Filter the collection contents by class
			CollectionFilterList collectionFilterList = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

			CollectionFilterData collectionFilterListData = collectionFilterList.CollectionFilterData;
			collectionFilterListData.SelectedPageIndex = 0;
			collectionFilterListData.SelectedShipClassIndex = 0;
			collectionFilterListData.ShipClassOptions = new string[]{ ShipClass.Neutral, mShipClass };

			collectionFilterList.Refresh( collectionFilterListData, true );

			//Set the decks class - ready for its upload
			DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
			deckContentsController.SetDisplayName( "NEW DECK" );
			deckContentsController.SetClass( mShipClass );
		}

		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to edit a deck then we need a supplied string so we know what the deck id is!" );

			mShipClass = ( ( StringStateConstructionData ) stateConstructionData ).String;
		}
		#endregion

		#region Protected Methods
		protected override void ExitToNextState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_IDLE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}