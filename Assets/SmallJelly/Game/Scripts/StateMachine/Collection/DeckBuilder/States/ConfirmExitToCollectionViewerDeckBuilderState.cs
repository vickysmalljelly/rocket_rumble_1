﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ConfirmExitToCollectionViewerDeckBuilderState : ConfirmExitDeckBuilderState
	{
		#region Protected Properties
		protected override string ExitDeckBuilderTitle 
		{ 
			get 
			{ 
				return "Exit Deck Building?"; 
			} 
		}

		protected override string ExitDeckBuilderMessage 
		{ 
			get
			{
				return "By exiting deck building you will lose your unfinished deck. Ok?";
			}
		}
		#endregion

		#region Protected Methods
		protected override void ConfirmExit()
		{
			OnStateCompleted( new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_EXIT_STATE ) );
		}

		protected override void CancelExit()
		{
			OnStateCompleted( new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_IDLE_STATE ) );
		}
		#endregion
	}
}