﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class ConfirmExitDeckBuilderState : CoreDeckBuilderState
	{
		#region Protected Properties
		protected abstract string ExitDeckBuilderTitle { get; }
		protected abstract string ExitDeckBuilderMessage { get; }
		#endregion

		#region Constants
		private const string CONFIRMATION_POSITIVE_TEXT = "Yes";
		private const string CONFIRMATION_NEGATIVE_TEXT = "No";
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ButtonData confirmExitButtonData = new ButtonData( CONFIRMATION_POSITIVE_TEXT, ConfirmExit );
			ButtonData cancelExitButtonData = new ButtonData( CONFIRMATION_NEGATIVE_TEXT, CancelExit );

			DialogManager.Get.ShowMessagePopup( ExitDeckBuilderTitle, ExitDeckBuilderMessage, confirmExitButtonData, cancelExitButtonData );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Abstracted Methods
		protected abstract void ConfirmExit();
		protected abstract void CancelExit();
		#endregion

		#region Event Handlers
		protected override void HandleRumbleClicked( object o, EventArgs args )
		{
		}

		protected override void HandleShopClicked( object o, EventArgs args )
		{
		}

		protected override void HandleInboxClicked( object o, EventArgs args )
		{
		}
		#endregion
	}
}