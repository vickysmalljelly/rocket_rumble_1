﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ExitDeckBuilderState : CoreDeckBuilderState
	{
		#region Member Variables
		private DeckContentsController mDeckSelectedCardsCollectionDisplayController;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mDeckSelectedCardsCollectionDisplayController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;

			RegisterListeners();

			mDeckSelectedCardsCollectionDisplayController.OnInsertState( DeckContentsController.State.MoveOut );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			//Hide deck contents controller and card selection controller
			UIManager.Get.HideMenu< DeckContentsController >();
			UIManager.Get.HideMenu< CardSelection >();
			UIManager.Get.HideMenu< CollectionFilterList >();
		}
		#endregion

		#region Event Registrations
		private void RegisterListeners()
		{
			mDeckSelectedCardsCollectionDisplayController.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mDeckSelectedCardsCollectionDisplayController.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleAnimationFinished( object o, EventArgs args )
		{
			OnStateCompleted();
		}

		private void HandleServerError( ServerError error )
		{
			if( error.ErrorType == ServerErrorType.Timeout )
			{
				SJLogger.LogError("TODO: Handle timeout");
			}
		}
	
		protected override void HandleRumbleClicked( object o, EventArgs args ){}

		protected override void HandleShopClicked( object o, EventArgs args ){}

		protected override void HandleInboxClicked( object o, EventArgs args ){}
		#endregion
	}
}