﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ConfirmExitToInboxDeckBuilderState : ConfirmExitDeckBuilderState
	{
		#region Protected Properties
		protected override string ExitDeckBuilderTitle 
		{ 
			get 
			{ 
				return "Exit Deck Building?"; 
			} 
		}

		protected override string ExitDeckBuilderMessage 
		{ 
			get
			{
				return "By leaving your collection you will lose your deck creation progress. Ok?";
			}
		}
		#endregion

		#region Protected Methods
		protected override void ConfirmExit()
		{
			FireReadyToTransitionToInbox();
		}

		protected override void CancelExit()
		{
			OnStateCompleted( new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_IDLE_STATE ) );
		}
		#endregion
	}
}