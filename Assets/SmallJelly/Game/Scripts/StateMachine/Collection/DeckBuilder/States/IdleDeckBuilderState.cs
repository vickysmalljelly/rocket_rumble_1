﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SmallJelly
{
	public class IdleDeckBuilderState : CoreDeckBuilderState
	{
		#region Constants
		private const int MAX_NUMBER_OF_DUPLICATE_CARDS = 2;
		private const int MAX_NUMBER_OF_DUPLICATE_LEGENDARY_CARDS = 1;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			//Check deck completion criteria
			CheckForDeckCompletion();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity state transitions
		public override void OnCollectionEntityStateTransitioned( CollectionItem collectionItem, CollectionItemController.State previousState, CollectionItemController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnCollectionEntityStateTransitioned( collectionItem, previousState, newState, hitLayers );

			//TODO - I'd like to intergrate this below flew UI lines with the 3D input system directly but it's the last day so there's not time!
			//If we're touching the UI then return
			if( EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent< Button >() != null )
			{
				return;
			}

			switch( previousState )
			{
				//If entity was pressed
				case CollectionItemController.State.Pressing:
					DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
					CardSelection cardSelection = CollectionManager.Get.CollectionMenuController.CardSelection;

					ButtonData messageClose = new ButtonData( "Close", HandleMessageClose );

					int cardCount = deckContentsController.Deck.BattleEntities.Count;
					if( cardCount == CollectionConstants.MAXIMUM_CARDS )
					{
						return;
					}

					if( collectionItem is BattleEntityCollectionItem )
					{
						BattleEntityCollectionItem battleEntityCollectionItem = (BattleEntityCollectionItem)collectionItem;
						BattleEntityData battleEntityData = battleEntityCollectionItem.Data.Card;
						
						//If we've already exceeded the number of cards of this type we're allowed then we can't have any more!
						int count = deckContentsController.Deck.BattleEntities.Count( x => x.Id == battleEntityData.Id );

						//We have a different max count for legendary and non-legendary!
						if( ( battleEntityData.Rarity == BattleEntityRarity.Legendary && count >= MAX_NUMBER_OF_DUPLICATE_LEGENDARY_CARDS ) || ( battleEntityData.Rarity != BattleEntityRarity.Legendary && count >= MAX_NUMBER_OF_DUPLICATE_CARDS ) )
						{
							DialogManager.Get.ShowMessagePopup( "Error", "You can only have one copy of each legendary card and two copies of each non-legendary card in your deck", messageClose );
							return;
						}

						//Try and find the card in the current collection screen
						CollectionBattleEntityData collectionBattleEntityData = cardSelection.Data.FirstOrDefault( x => x.Card.Id == battleEntityData.Id );

						//If we dont have the card in the current card selection screen then there's not a whole lot we can do! - we just have to accept it
						//because it will have come from a downloaded deck - if it does exist in the current card selection screen then we need to verify
						//that we have enough cards to add it!
						if( collectionBattleEntityData.Card != null && count >= collectionBattleEntityData.Count )
						{
							DialogManager.Get.ShowMessagePopup( "Error", "You've already put all of the copies of this card in your deck!", messageClose );
							return;
						}

						deckContentsController.AddCard( battleEntityData );
					}
					break;
			}
		}
		#endregion

		#region Private Methods
		private void CheckForDeckCompletion()
		{
			int cardCount = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.Deck.BattleEntities.Count;
			SJLogger.AssertCondition( cardCount <= CollectionConstants.MAXIMUM_CARDS, "The card count should never exceed the number of cards specified in collection constants. Current card count {0}, Maximum card count {1}", cardCount, CollectionConstants.MAXIMUM_CARDS );

			//Check if deck is complete
			if( cardCount == CollectionConstants.MAXIMUM_CARDS )
			{
				ExitToCompleteDeckState();
			}
		}

		private void ExitToCompleteDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_COMPLETE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToRumbleDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_RUMBLE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToShopDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_SHOP_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToInboxDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_INBOX_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToCollectionViewerDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_COLLECTION_VIEWER_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.DeckUpdated += HandleDeckUpdated;
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.CompletePressed += HandleCompletePressed;
		}

		private void UnregisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.DeckUpdated -= HandleDeckUpdated;
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.CompletePressed -= HandleCompletePressed;
		}
		#endregion

		#region Event Handlers
		private void HandleMessageClose()
		{
			DialogManager.Get.HideMessagePopup();
		}

		private void HandleDeckUpdated( object o, EventArgs eventArgs )
		{
			//Check deck completion criteria
			CheckForDeckCompletion();
		}

		private void HandleCompletePressed( object o, EventArgs eventArgs )
		{
			ExitToConfirmExitToCollectionViewerDeckState();
		}

		protected override void HandleRumbleClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToRumbleDeckState();
		}

		protected override void HandleShopClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToShopDeckState();
		}

		protected override void HandleInboxClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToInboxDeckState();	
		}
		#endregion
	}
}