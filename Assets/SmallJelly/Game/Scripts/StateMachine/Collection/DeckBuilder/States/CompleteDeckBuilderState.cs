﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SmallJelly
{
	public abstract class CompleteDeckBuilderState : CoreDeckBuilderState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Protected Methods
		protected abstract void CreateDeck();
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity state transitions
		public override void OnCollectionEntityStateTransitioned( CollectionItem collectionItem, CollectionItemController.State previousState, CollectionItemController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnCollectionEntityStateTransitioned( collectionItem, previousState, newState, hitLayers );

			//TODO - I'd like to intergrate this below flew UI lines with the 3D input system directly but it's the last day so there's not time!
			//If we're touching the UI then return
			if( EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent< Button >() != null )
			{
				return;
			}

			ButtonData messageClose = new ButtonData( "Close", HandleMessageClose );

			switch( previousState )
			{
			//If entity was pressed
			case CollectionItemController.State.Pressing:
				DialogManager.Get.ShowMessagePopup( "Error", "Your deck is already full!", messageClose );
				break;
			}
		}
		#endregion

		#region Private Methods
		private void ExitToIdleDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_IDLE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToExitDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_EXIT_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToRumbleDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_RUMBLE_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToShopDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_SHOP_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}

		private void ExitToConfirmExitToInboxDeckState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_CONFIRM_EXIT_TO_INBOX_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.DeckUpdated += HandleDeckUpdated;
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.CompletePressed += HandleCompletePressed;
		}

		private void UnregisterListeners()
		{
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.DeckUpdated -= HandleDeckUpdated;
			CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.CompletePressed -= HandleCompletePressed;
		}
		#endregion
			
		#region Event Handlers
		private void HandleMessageClose()
		{
			DialogManager.Get.HideMessagePopup();
		}

		private void HandleDeckUpdated( object o, EventArgs eventArgs )
		{
			int cardCount = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.Deck.BattleEntities.Count;
			SJLogger.AssertCondition( cardCount < CollectionConstants.MAXIMUM_CARDS, "The deck has been updated whilst it was already complete, it thus should no longer be complete" );

			//The deck has been updated - since it was full it must now no longer be full (its impossible to add more cards - as asserted above)
			ExitToIdleDeckState();
		}

		private void HandleCompletePressed( object o, EventArgs eventArgs )
		{
			CreateDeck();
			ExitToExitDeckState();
		}

		protected override void HandleRumbleClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToRumbleDeckState();
		}

		protected override void HandleShopClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToShopDeckState();
		}

		protected override void HandleInboxClicked( object o, EventArgs args )
		{
			ExitToConfirmExitToInboxDeckState();	
		}
		#endregion
	}
}