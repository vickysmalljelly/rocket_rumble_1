﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class CoreDeckBuilderState : CollectionManagementState
	{

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion
	}
}