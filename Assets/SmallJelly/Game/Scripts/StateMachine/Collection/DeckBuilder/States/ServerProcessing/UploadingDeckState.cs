﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class UploadingDeckBuilderState : ServerProcessingCollectionManagementState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
			deckContentsController.OnInsertState( DeckContentsController.State.Downloading );
		}

		public override void OnCollectionEventDataReceived( CollectionEventData collectionEventData )
		{
			base.OnCollectionEventDataReceived( collectionEventData );

			DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
			deckContentsController.OnInsertState( DeckContentsController.State.Downloaded );

			if( collectionEventData is NewDeckCollectionEventData )
			{
				OnStateCompleted();
			}
		}
		#endregion
	}
}