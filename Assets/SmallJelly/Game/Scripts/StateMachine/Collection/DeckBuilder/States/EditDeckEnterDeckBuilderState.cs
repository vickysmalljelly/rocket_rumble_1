﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class EditDeckEnterDeckBuilderState : EnterDeckBuilderState
	{
		#region Member Variables
		private string mClassId;
		#endregion

		#region Public Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to create a deck then we need a supplied string so we know what the class is!" );

			mClassId = ( ( StringStateConstructionData ) stateConstructionData ).String;
		}
		#endregion

		#region Protected Methods
		protected override void ExitToNextState()
		{
			StateConstructionData stateConstructionMessageData = new StringStateConstructionData( EditDeckBuilderStateMachine.DECK_BUILDER_DOWNLOADING_DECK_STATE, mClassId );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}