﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class EnterDeckBuilderState : CoreDeckBuilderState
	{
		#region Member Variables
		private DeckContentsController mDeckContentsController;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();


			//Show deck contents controller and card selection controller - ready for the page to be displayed
			DeckContentsController deckContentsController = UIManager.Get.ShowMenu< DeckContentsController >();
			UIManager.Get.ShowMenu< CardSelection >();
			UIManager.Get.ShowMenu< CollectionFilterList >();

			deckContentsController.OnInsertState( DeckContentsController.State.MoveIn );

			ExitToNextState();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Protected Methods
		protected abstract void ExitToNextState();
		#endregion
	}
}