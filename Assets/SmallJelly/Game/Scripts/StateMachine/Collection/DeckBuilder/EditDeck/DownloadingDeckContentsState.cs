﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class DownloadingDeckContentsState : ServerProcessingCollectionManagementState
	{
		#region Member Variables
		private string mDeckId;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			CollectionManager.Get.GetDeck( mDeckId );

			DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
			deckContentsController.OnInsertState( DeckContentsController.State.Downloading );
		}

		public override void OnCollectionEventDataReceived( CollectionEventData collectionEventData )
		{
			base.OnCollectionEventDataReceived( collectionEventData );

			if( collectionEventData is GetDeckCollectionEventData )
			{
				GetDeckCollectionEventData getDeckCollectionEventData = (GetDeckCollectionEventData) collectionEventData;

				DeckContentsController deckContentsController = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController;
				deckContentsController.OnInsertState( DeckContentsController.State.Downloaded );

				Deck deck = getDeckCollectionEventData.Deck;

				SJLogger.AssertCondition( deckContentsController.Deck.BattleEntities.Count == 0, "The current deck should be empty - a new one has just been downloaded!" );

				deckContentsController.SetId( deck.DeckId );
				deckContentsController.SetDisplayName( deck.DisplayName );
				deckContentsController.SetClass( deck.Class );

				foreach( BattleEntityData battleEntityData in deck.BattleEntities )
				{
					deckContentsController.AddCard( battleEntityData );
				}

				CollectionFilterList collectionFilterList = CollectionManager.Get.CollectionMenuController.CollectionFilterList;

				CollectionFilterData collectionFilterListData = collectionFilterList.CollectionFilterData;
				collectionFilterListData.SelectedPageIndex = 0;
				collectionFilterListData.SelectedShipClassIndex = 0;
				collectionFilterListData.ShipClassOptions = new string[]{ ShipClass.Neutral, deck.Class };

				collectionFilterList.Refresh( collectionFilterListData, true );

				//We've finished downloading the relevant data so let's exit the state
				OnStateCompleted( new StateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_IDLE_STATE ) );
			}
		}
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to edit a deck then we need a supplied string so we know what the deck id is!" );

			mDeckId = ( ( StringStateConstructionData ) stateConstructionData ).String;
		}
		#endregion

	}
}