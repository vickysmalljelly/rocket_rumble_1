﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SmallJelly
{
	public class EditDeckCompleteDeckBuilderState : CompleteDeckBuilderState
	{
		#region Private Methods
		protected override void CreateDeck()
		{
			Deck deck = CollectionManager.Get.CollectionMenuController.DeckContentsDisplayController.Deck;
			List< string > cards = deck.BattleEntities.Select( o => o.Id ).ToList();

            CollectionManager.Get.UpdateDeck( deck.DeckId, cards, deck.DisplayName );
		}
		#endregion
	}
}