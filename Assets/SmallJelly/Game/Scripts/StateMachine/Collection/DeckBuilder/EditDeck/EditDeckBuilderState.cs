﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class EditDeckBuilderState : DeckBuilderState
	{
		#region Member Variables
		private string mDeckId;
        private string mClassName;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			DeckBuilderStateMachine = this.AddChildStateMachine< EditDeckBuilderStateMachine >();

			EditDeckBuilderStateMachine editDeckBuilderStateMachine = (EditDeckBuilderStateMachine) DeckBuilderStateMachine;
			editDeckBuilderStateMachine.OnConstruct( new StringStateConstructionData( DeckBuilderStateMachine.DECK_BUILDER_ENTER_STATE, mDeckId ) );

            CollectionFilterList controller = UIManager.Get.GetMenuController<CollectionFilterList>();
            controller.SetEditingDeck(mClassName);

			base.OnEntering();
		}

		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

            SJLogger.AssertCondition( stateConstructionData is TypedStateConstructionData<DeckIdAndClass>, "If we're transitioning to edit a deck then we need a supplied string so we know what the deck id is!" );

            TypedStateConstructionData<DeckIdAndClass> scd = stateConstructionData as TypedStateConstructionData<DeckIdAndClass>;

            mDeckId = scd.Value.DeckId;
            mClassName = scd.Value.ClassName;
		}

        public override void OnLeaving()
        {
            CollectionFilterList controller = UIManager.Get.GetMenuController<CollectionFilterList>();
            controller.ClearEditingDeck();

            base.OnLeaving();
        }
		#endregion
	}
}