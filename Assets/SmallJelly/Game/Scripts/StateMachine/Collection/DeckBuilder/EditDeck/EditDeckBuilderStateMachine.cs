﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class EditDeckBuilderStateMachine : DeckBuilderStateMachine
	{
		#region Constants
		// Game states
		public const string DECK_BUILDER_DOWNLOADING_DECK_STATE = "DECK_BUILDER_DOWNLOADING_DECK_STATE";
		#endregion

		#region Member Variables
		private DownloadingDeckContentsState mDownloadingDeckContentsState;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			mDownloadingDeckContentsState = AddState< DownloadingDeckContentsState >( DECK_BUILDER_DOWNLOADING_DECK_STATE );
			EnterDeckBuilderState = AddState< EditDeckEnterDeckBuilderState >( DECK_BUILDER_ENTER_STATE );
			CompleteDeckBuilderState = AddState< EditDeckCompleteDeckBuilderState >( DECK_BUILDER_COMPLETE_STATE );

			base.Awake();

			AddTransition( EnterDeckBuilderState.Id, mDownloadingDeckContentsState.Id );
			AddTransition( mDownloadingDeckContentsState.Id, IdleDeckBuilderState.Id );
		}
		#endregion

		#region Event Registration
		protected override void RegisterListeners()
		{
			base.RegisterListeners();

			mDownloadingDeckContentsState.StateCompleted += HandleStateFinished;
		}

		protected override void UnregisterListeners()
		{
			base.UnregisterListeners();

			mDownloadingDeckContentsState.StateCompleted -= HandleStateFinished;
		}
		#endregion
	}
}
