﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	/// <summary>
	/// State for viewing the users current collection of cards
	/// </summary>
	public abstract class DeckBuilderState : AbstractCollectionState
	{	
		#region Protected Properties
		protected DeckBuilderStateMachine DeckBuilderStateMachine { get; set; }
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Register Listeners
		private void RegisterListeners()
		{
			DeckBuilderStateMachine.StateMachineCompleted += HandleStateMachineCompleted;

			DeckBuilderStateMachine.ReadyToTransitionToPlay += HandleReadyToTransitionToRumble;
			DeckBuilderStateMachine.ReadyToTransitionToInbox += HandleReadyToTransitionToInbox;
			DeckBuilderStateMachine.ReadyToTransitionToShop += HandleReadyToTransitionToShop;
		}

		private void UnregisterListeners()
		{
			DeckBuilderStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			DeckBuilderStateMachine.ReadyToTransitionToPlay -= HandleReadyToTransitionToRumble;
			DeckBuilderStateMachine.ReadyToTransitionToInbox -= HandleReadyToTransitionToInbox;
			DeckBuilderStateMachine.ReadyToTransitionToShop -= HandleReadyToTransitionToShop;
		}
		#endregion

		#region Event Handlers
		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData messageData )
		{
			//Finish state
			StateConstructionData stateConstructionMessageData = new StateConstructionData( CollectionStateMachine.COLLECTION_VIEWER_STATE );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}