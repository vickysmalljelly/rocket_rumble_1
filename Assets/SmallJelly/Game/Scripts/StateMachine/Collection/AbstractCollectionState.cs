﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class AbstractCollectionState : StateMachine.State
	{
		#region Public Events
		public event EventHandler ReadyToTransitionToRumble;
		public event EventHandler ReadyToTransitionToShop;
		public event EventHandler ReadyToTransitionToInbox;
		#endregion

		#region Event Firing
		protected void FireReadyToTransitionToRumble()
		{
			if( ReadyToTransitionToRumble != null )
			{
				ReadyToTransitionToRumble( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToShop()
		{
			if( ReadyToTransitionToShop != null )
			{
				ReadyToTransitionToShop( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToInbox()
		{
			if( ReadyToTransitionToInbox != null )
			{
				ReadyToTransitionToInbox( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		protected void HandleReadyToTransitionToRumble( object o, EventArgs args )
		{
			FireReadyToTransitionToRumble();
		}

		protected void HandleReadyToTransitionToShop( object o, EventArgs args )
		{
			FireReadyToTransitionToShop();
		}

		protected void HandleReadyToTransitionToInbox( object o, EventArgs args )
		{
			FireReadyToTransitionToInbox();
		}
		#endregion
	}
}