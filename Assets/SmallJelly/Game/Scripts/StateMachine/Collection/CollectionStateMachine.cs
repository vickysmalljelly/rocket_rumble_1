using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class CollectionStateMachine : StateMachine
	{
		#region Event Handlers
		public event EventHandler ReadyToTransitionToRumble
		{
			add
			{
				mCollectionViewerState.ReadyToTransitionToRumble += value;
				mEditDeckBuilderState.ReadyToTransitionToRumble += value;
				mNewDeckBuilderState.ReadyToTransitionToRumble += value;
			}

			remove
			{
				mCollectionViewerState.ReadyToTransitionToRumble -= value;
				mEditDeckBuilderState.ReadyToTransitionToRumble -= value;
				mNewDeckBuilderState.ReadyToTransitionToRumble -= value;
			}
		}

		public event EventHandler ReadyToTransitionToShop
		{
			add
			{
				mCollectionViewerState.ReadyToTransitionToShop += value;
				mEditDeckBuilderState.ReadyToTransitionToShop += value;
				mNewDeckBuilderState.ReadyToTransitionToShop += value;
			}

			remove
			{
				mCollectionViewerState.ReadyToTransitionToShop -= value;
				mEditDeckBuilderState.ReadyToTransitionToShop -= value;
				mNewDeckBuilderState.ReadyToTransitionToShop -= value;
			}
		}

		public event EventHandler ReadyToTransitionToInbox
		{
			add
			{
				mCollectionViewerState.ReadyToTransitionToInbox += value;
				mEditDeckBuilderState.ReadyToTransitionToInbox += value;
				mNewDeckBuilderState.ReadyToTransitionToInbox += value;
			}

			remove
			{
				mCollectionViewerState.ReadyToTransitionToInbox -= value;
				mEditDeckBuilderState.ReadyToTransitionToInbox -= value;
				mNewDeckBuilderState.ReadyToTransitionToInbox -= value;
			}
		}
		#endregion

		#region Constants
		public const string ENTER_STATE = "ENTER";
		public const string COLLECTION_VIEWER_STATE = "COLLECTION_VIEWER";
		public const string EDIT_DECK_BUILDER_STATE = "EDIT_DECK_BUILDER"; 
		public const string NEW_DECK_BUILDER_STATE = "NEW_DECK_BUILDER"; 

		public const string EXIT_STATE = "EXIT";
		#endregion

		#region Member Variables
		private CollectionViewerState mCollectionViewerState;
		private EditDeckBuilderState mEditDeckBuilderState;
		private NewDeckBuilderState mNewDeckBuilderState;
		#endregion

		#region MonoBehaviour
		protected override void Awake()
		{
			base.Awake();


			mCollectionViewerState = AddState<CollectionViewerState>( COLLECTION_VIEWER_STATE );
			mNewDeckBuilderState = AddState<NewDeckBuilderState>( NEW_DECK_BUILDER_STATE );
			mEditDeckBuilderState = AddState<EditDeckBuilderState>( EDIT_DECK_BUILDER_STATE );


			AddTransition( mCollectionViewerState.Id, mEditDeckBuilderState.Id );
			AddTransition( mCollectionViewerState.Id, mNewDeckBuilderState.Id );

			AddTransition( mEditDeckBuilderState.Id, mCollectionViewerState.Id );
			AddTransition( mNewDeckBuilderState.Id, mCollectionViewerState.Id );

			//By default let's move to the colleciton viewer
			StateConstructionData = new StateConstructionData( mCollectionViewerState.Id );

			RegisterListeners();
        }

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mCollectionViewerState.StateCompleted += HandleStateCompleted;
			mNewDeckBuilderState.StateCompleted += HandleStateCompleted;
			mEditDeckBuilderState.StateCompleted += HandleStateCompleted;
		}

		private void UnregisterListeners()
		{
			mCollectionViewerState.StateCompleted -= HandleStateCompleted;
			mNewDeckBuilderState.StateCompleted -= HandleStateCompleted;
			mEditDeckBuilderState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handlers
		private void HandleStateCompleted( State state, StateConstructionData stateConstructionMessageData )
		{
			StateConstructionData = stateConstructionMessageData;
		}
		#endregion
    }
}