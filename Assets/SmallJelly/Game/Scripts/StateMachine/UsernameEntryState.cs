﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
    /// <summary>
    /// Don't know if it should be an AbstractFrontEndState as it is only used in the onboarding.
    /// </summary>
    public sealed class UsernameEntryState : AbstractFrontEndState< PlayMenuController >
    {
        protected override string FrontEndPath 
        {
            get
            {
                return FileLocations.PlayMenuPrefab;
            }
        }

        private UsernameEntryStateMachine mChildStateMachine;

        #region State Machine Methods
        public override void OnEntering()
        {
            base.OnEntering();

            mChildStateMachine = AddChildStateMachine<UsernameEntryStateMachine>();

            RegisterListeners();
        }

        public override void OnLeaving()
        {
            base.OnLeaving();

            UnregisterListeners();

        }
        #endregion

        #region Event Registration
        private void RegisterListeners()
        {
            mChildStateMachine.StateMachineCompleted += HandleChildStateMachineCompleted;
        }

        private void UnregisterListeners()
        {
            mChildStateMachine.StateMachineCompleted -= HandleChildStateMachineCompleted;
        }
        #endregion

        #region Event Handlers
        private void HandleChildStateMachineCompleted(StateMachine arg1, StateConstructionData arg2)
        {
            TransitionState(FrontEndStateMachine.PLAY_STATE);
        }
        #endregion
    }
}
