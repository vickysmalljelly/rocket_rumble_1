﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// State to update player's data. Exits when complete.
    /// We would like to be able to use this from anywhere in the game.
    /// </summary>
	public class UpdatePlayerDataState : AbstractGameState
    {
        // The reward we are expecting the player to be given
        private RewardData mRewardData;

        public override void OnEntering()
        {
            base.OnEntering();

            DialogManager.Get.ShowSpinner();

            ClientAuthentication.GetAccountDetails(HandleAccountDetailsSuccess, HandleAccountDetailsError);
        }

        public void SetRewardData(RewardData rewardData)
        {
            mRewardData = rewardData;
        }

        private void HandleAccountDetailsSuccess(VirtualGoods goods, SJJson scriptData)
        {
            DialogManager.Get.HideSpinner();

            // We know the player is logged in.  Do some housekeeping for other services.
            #if KT_PLAY
            KTPlayManager.Get.Login(ClientAuthentication.PlayerAccountDetails.DisplayName);
            #endif

            //SJFirebaseManager.Get.Register();

            // Store the player data
            GameManager.Get.UpdatePlayerData(new PlayerData(goods, scriptData));

            #if UNITY_ANDROID
            // Check if we need to update
            if(FeatureFlags.NeedAppUpdate())
            {
                TransitionState(GameStateMachine.GAME_STATE_FORCE_UPDATE);
                return;
            }               
            #endif

            OnStateCompleted();
        }

        private void HandleAccountDetailsError(ServerError error)
        {
            DialogManager.Get.HideSpinner();

            if(mRewardData != null) 
            { 
                // Failed to retrieve the data from the server, try to update the UI with what we expect the changes to be.
                GameManager.Get.UpdateRewardsOnClient(mRewardData);
            }
            else
            {
                SJLogger.LogWarning("UpdatePlayerDataState - account details update failed and we have no reward data");
            }

            OnStateCompleted();
        }
    }
}

