﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class AbstractGameState : StateMachine.State
	{
		#region MonoBehaviour Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			//Whenever we switch to a new major part of the game - unload those assets not currently in usage!
			GameManager.Get.CleanupUnusedAssets();
		}
		#endregion
		
		#region Event Handlers
		private void HandleConnectionFailed( object o, EventArgs args )
		{
			TransitionState( GameStateMachine.GAME_STATE_CONNECTION_LOST );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			GameManager.Get.ConnectionFailed += HandleConnectionFailed;
		}

		private void UnregisterListeners()
		{
			GameManager.Get.ConnectionFailed -= HandleConnectionFailed;
		}
		#endregion
	}
}