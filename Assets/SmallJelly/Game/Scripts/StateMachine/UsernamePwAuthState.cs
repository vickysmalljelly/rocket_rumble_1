﻿using SmallJelly.Framework;
using UnityEngine;
using System.Collections;

namespace SmallJelly
{
	/// <summary>
	/// Shows the authentication menu which has options to login or register
	/// </summary>
	public sealed class UsernamePwAuthState : AbstractGameState
	{
		AuthenticationMenuController mMenuController;

		public override void OnEntering()
		{
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Authentication, null, null);
			base.OnEntering();

			// Show the menu
			mMenuController = UIManager.Get.ShowMenu<AuthenticationMenuController>();
			mMenuController.AuthenticationCompleted += HandleAuthenicationCompleted;
		}
		
		public override void OnLeaving()
		{
			base.OnLeaving();

            mMenuController.AuthenticationCompleted -= HandleAuthenicationCompleted;
			
			// Hide the menu
			UIManager.Get.HideMenu<AuthenticationMenuController>();

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Authentication, null, null);
		}

		private void HandleAuthenicationCompleted()
		{
			mMenuController.AuthenticationCompleted -= HandleAuthenicationCompleted;

            TransitionState(GameStateMachine.GAME_STATE_PLAYER_UPDATE, new StateConstructionData(GameStateMachine.GAME_STATE_ONBOARDING));
		}           
	}
}
