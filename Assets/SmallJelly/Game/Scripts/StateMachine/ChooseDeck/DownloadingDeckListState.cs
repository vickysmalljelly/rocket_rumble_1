﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SmallJelly
{
	public sealed class DownloadingDeckListState : StateMachine.State
	{
		#region Constants
		private const string SERVER_DOWNLOADING_TITLE = "Downloading";
		private const string SERVER_DOWNLOADING_MESSAGE= "Downloading custom decks.";

		private const string SERVER_ERROR_MESSAGE_TITLE_TEXT = "Error";
		private const string SERVER_ERROR_MESSAGE_BUTTON_TEXT = "Ok";
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			UIManager.Get.ShowMenu< ChooseCustomDeckDisplayController >();

			ChallengeSetupManager.Get.ListDecks();
		}

		public override void OnLeaving()
		{
			UIManager.Get.HideMenu< ChooseCustomDeckDisplayController >();

			UnregisterListeners();

			base.OnLeaving();
		}
		#endregion

		#region Register Listeners
		private void RegisterListeners()
		{
			ChallengeSetupManager.Get.Success += HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;

		}

		private void UnregisterListeners()
		{
			ChallengeSetupManager.Get.Success -= HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;

		}
		#endregion

		#region Event Handlers
		private void HandleChallengeSetupEventDataReceived( object o, ChallengeSetupEventDataEventArgs challengeSetupEventDataEventArgs )
		{
			if( challengeSetupEventDataEventArgs.ChallengeSetupEventData is ListDecksEventData )
			{
				ListDecksEventData listDecksEventData = (ListDecksEventData)challengeSetupEventDataEventArgs.ChallengeSetupEventData;

				//We've finished downloading the relevant data so let's exit the state
				OnStateCompleted( new TypedStateConstructionData< Deck[] >( ChooseDeckStateMachine.CHOOSE_CUSTOM_DECK_STATE, listDecksEventData.Decks.ToArray() ) );
			}
		}

        private void HandleChallengeSetupResponseFailure( ServerError error )
		{

		}
		#endregion
	}
}