﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class SinglePlayerSetCurrentDeckState : SetCurrentDeckState
	{
		#region Protected Methods
		protected override void TransitionToNextState()
		{
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_SINGLE_PLAYER_MENU ) );
		}
		#endregion
	}
}