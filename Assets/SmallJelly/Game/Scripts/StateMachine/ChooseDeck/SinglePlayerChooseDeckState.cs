﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	public class SinglePlayerChooseDeckState : ChooseDeckState
	{	
		#region State Machine Methods
		public override void OnEntering()
		{
			ChooseDeckStateMachine = AddChildStateMachine< SinglePlayerChooseDeckStateMachine >();

			base.OnEntering();
		}
		#endregion
	}
}