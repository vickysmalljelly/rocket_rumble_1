using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class SinglePlayerChooseDeckStateMachine : ChooseDeckStateMachine
	{
		#region Private Methods
		protected override void Awake()
		{
			// Initialise states
			SetCurrentDeckState = AddState< SinglePlayerSetCurrentDeckState >( SET_CURRENT_DECK_STATE );

			base.Awake();
		}
		#endregion
	}
}