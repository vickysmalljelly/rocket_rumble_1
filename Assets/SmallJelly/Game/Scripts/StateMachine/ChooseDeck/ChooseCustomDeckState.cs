﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class ChooseCustomDeckState : StateMachine.State 
	{
		//TODO - This is temporary-ish code until we've worked out what we're doing with deck selection in the final game
		#region Member Variables
		private Deck[] mDecks;
		private ChooseCustomDeckDisplayController mChooseDeckDisplayController;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mChooseDeckDisplayController = UIManager.Get.ShowMenu< ChooseCustomDeckDisplayController >();

            mChooseDeckDisplayController.Refresh( mDecks );

			RegisterListeners( mChooseDeckDisplayController );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners( mChooseDeckDisplayController );
		}

		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is TypedStateConstructionData< Deck[] >, "If we're transitioning to select a deck then we need some decks to list!" );

			mDecks = ( ( TypedStateConstructionData< Deck[] > ) stateConstructionData ).Value;
		}
		#endregion


		#region Event Registration
		private void RegisterListeners( ChooseCustomDeckDisplayController chooseDeckDisplayController )
		{
			chooseDeckDisplayController.PressedBack += HandlePressedBack;
			chooseDeckDisplayController.PressedCustomDeck += HandlePressedCustomDeck;
		}

		private void UnregisterListeners( ChooseCustomDeckDisplayController chooseDeckDisplayController )
		{
			chooseDeckDisplayController.PressedBack -= HandlePressedBack;
			chooseDeckDisplayController.PressedCustomDeck -= HandlePressedCustomDeck;
		}
		#endregion

		#region Event Handlers
		private void HandlePressedBack( object o, EventArgs args )
		{
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_FRONT_END ) );
		}

		private void HandlePressedCustomDeck( object o, StringEventArgs stringEventArgs )
		{
			OnStateCompleted( new StringStateConstructionData( ChooseDeckStateMachine.SET_CURRENT_DECK_STATE, stringEventArgs.String ) );
		}
		#endregion
	}
}