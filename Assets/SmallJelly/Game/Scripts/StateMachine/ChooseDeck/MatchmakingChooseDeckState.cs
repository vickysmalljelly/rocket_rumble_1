﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	public class MatchmakingChooseDeckState : ChooseDeckState
	{	
		#region State Machine Methods
		public override void OnEntering()
		{
			ChooseDeckStateMachine = AddChildStateMachine< MatchmakingChooseDeckStateMachine >();

			base.OnEntering();
		}
		#endregion
	}
}