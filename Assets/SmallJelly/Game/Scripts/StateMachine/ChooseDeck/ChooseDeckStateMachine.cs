using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// State machine which controls the selection of the deck to be used in the battle
	/// </summary>
	public class ChooseDeckStateMachine : StateMachine
	{
		#region Constants
		// States
		public const string DOWNLOADING_DECK_LIST = "DOWNLOADING_DECK_LIST";
		public const string CHOOSE_CUSTOM_DECK_STATE = "CHOOSE_CUSTOM_DECK_STATE";
		public const string SET_CURRENT_DECK_STATE = "SET_CURRENT_DECK_STATE";
		#endregion

		#region Protected Properties
		protected SetCurrentDeckState SetCurrentDeckState
		{
			set
			{
				mSetCurrentDeckState = value;
			}
		}
		#endregion

		#region Member Variables
		private ChooseCustomDeckDisplayController mChooseCustomDeckDisplayController;
		private DownloadingDeckListState mDownloadingDeckListState;
		private ChooseCustomDeckState mChooseCustomDeckState;
		private SetCurrentDeckState mSetCurrentDeckState;
		#endregion

		#region Private Methods
		protected override void Awake()
		{
			base.Awake();

			// Initialise states
			mDownloadingDeckListState = AddState< DownloadingDeckListState >( DOWNLOADING_DECK_LIST );
			mChooseCustomDeckState = AddState< ChooseCustomDeckState >( CHOOSE_CUSTOM_DECK_STATE );

			AddTransition( DOWNLOADING_DECK_LIST, CHOOSE_CUSTOM_DECK_STATE );
			AddTransition( CHOOSE_CUSTOM_DECK_STATE, SET_CURRENT_DECK_STATE );
			AddTransition( SET_CURRENT_DECK_STATE, CHOOSE_CUSTOM_DECK_STATE );

			StateConstructionData = new StateConstructionData( DOWNLOADING_DECK_LIST );
		}

		private void OnEnable()
		{
			mChooseCustomDeckDisplayController = CreateDisplayController();

			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();

			UIManager.Get.HideMenu< ChooseCustomDeckDisplayController >();
			UIManager.Get.RemoveMenu< ChooseCustomDeckDisplayController >();

			Destroy( mChooseCustomDeckDisplayController.gameObject );
		}
		#endregion

		#region Private Methods
		private ChooseCustomDeckDisplayController CreateDisplayController()
		{
			return UIManager.Get.AddMenu< ChooseCustomDeckDisplayController >( FileLocations.ChooseCustomDeckMenuPrefab, UIManager.Get.transform );
		}
		#endregion


		#region Event Registration
		private void RegisterListeners()
		{
			mDownloadingDeckListState.StateCompleted += HandleStateCompleted;
			mChooseCustomDeckState.StateCompleted += HandleStateCompleted;
			mSetCurrentDeckState.StateCompleted += HandleStateCompleted;
		}

		private void UnregisterListeners()
		{
			mDownloadingDeckListState.StateCompleted -= HandleStateCompleted;
			mChooseCustomDeckState.StateCompleted -= HandleStateCompleted;
			mSetCurrentDeckState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handlers
		private void HandleStateCompleted (State state, StateConstructionData messageData)
		{
			SJLogger.AssertCondition( messageData != null, "Message data must not be null!");

			StateConstructionData = (StateConstructionData)messageData;
		}
		#endregion
	}
}