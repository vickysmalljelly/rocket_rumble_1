﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public abstract class SetCurrentDeckState : StateMachine.State 
	{
		#region Constants
		private const string SERVER_ERROR_MESSAGE_TITLE_TEXT = "Error";
		private const string SERVER_ERROR_MESSAGE_BUTTON_TEXT = "Ok";
		#endregion

		#region Member Variables
		private StringStateConstructionData mStringStateConstructionData;
		#endregion

		#region Public Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.LogMessage(MessageFilter.GameState, "Constructing {0}", this.GetType());
			mStringStateConstructionData = ( StringStateConstructionData ) stateConstructionData;
		}

		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			string chosenDeck = mStringStateConstructionData.String;
			ChallengeSetupManager.Get.SetCurrentDeck( chosenDeck );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
			UnregisterListeners();
		}
		#endregion

		#region Protected Methods
		protected abstract void TransitionToNextState();
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ChallengeSetupManager.Get.Success += HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;
		}

		private void UnregisterListeners()
		{
			ChallengeSetupManager.Get.Success -= HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;
		}
		#endregion

		#region Event Handlers
		private void HandleChallengeSetupEventDataReceived( object o, ChallengeSetupEventDataEventArgs challengeSetupEventDataEventArgs )
		{
			TransitionToNextState();
		}

        private void HandleChallengeSetupResponseFailure( ServerError error )
		{
			ButtonData buttonData = new ButtonData( SERVER_ERROR_MESSAGE_BUTTON_TEXT, HandleErrorDismissed );
            DialogManager.Get.ShowMessagePopup( SERVER_ERROR_MESSAGE_TITLE_TEXT, error.UserFacingMessage, buttonData );
		}

		private void HandleErrorDismissed()
		{
			OnStateCompleted( new StateConstructionData( ChooseDeckStateMachine.CHOOSE_CUSTOM_DECK_STATE ) );
		}
		#endregion
	}
}