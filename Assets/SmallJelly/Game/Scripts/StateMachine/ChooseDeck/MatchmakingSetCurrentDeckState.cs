﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class MatchmakingSetCurrentDeckState : SetCurrentDeckState
	{
		#region Protected Methods
		protected override void TransitionToNextState()
		{
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_FIND_MATCH ) );
		}
		#endregion
	}
}