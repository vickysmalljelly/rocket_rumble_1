using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	public abstract class ChooseDeckState : AbstractGameState
	{	
		#region Protected Properties
		protected ChooseDeckStateMachine ChooseDeckStateMachine
		{
			set
			{
				mChooseDeckStateMachine = value;
			}
		}
		#endregion

		#region Member Variables
		private ChooseDeckStateMachine mChooseDeckStateMachine;
		private ChooseCustomDeckDisplayController mChooseDeckDisplayController;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners ( mChooseDeckStateMachine );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners ( mChooseDeckStateMachine );
        }
		#endregion


		#region Event Registration
		private void RegisterListeners( ChooseDeckStateMachine chooseDeckStateMachine )
		{
			chooseDeckStateMachine.StateMachineCompleted += HandleStateMachineCompleted;
		}

		private void UnregisterListeners( ChooseDeckStateMachine chooseDeckStateMachine )
		{
			chooseDeckStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;
		}
		#endregion



		#region Event Handlers
		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionData )
		{
			OnStateCompleted( stateConstructionData );
		}
		#endregion
    }
}