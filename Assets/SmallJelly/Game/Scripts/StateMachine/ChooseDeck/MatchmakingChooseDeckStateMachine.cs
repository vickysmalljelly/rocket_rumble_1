using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class MatchmakingChooseDeckStateMachine : ChooseDeckStateMachine
	{
		#region Private Methods
		protected override void Awake()
		{
			// Initialise states
			SetCurrentDeckState = AddState< MatchmakingSetCurrentDeckState >( SET_CURRENT_DECK_STATE );

			base.Awake();
		}
		#endregion
	}
}