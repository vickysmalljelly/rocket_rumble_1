﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls the replay state of the game
    /// </summary>
	public sealed class ReplayState : AbstractGameState
    {   
        public override void OnEntering()
        {
            base.OnEntering();

            ReplayStateMachine stateMachine = this.AddChildStateMachine<ReplayStateMachine>();
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;

        }

        public override void OnLeaving()
        {
            base.OnLeaving();
        }

		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
        {
            stateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			OnStateCompleted( stateConstructionMessageData );
        }

    }
}