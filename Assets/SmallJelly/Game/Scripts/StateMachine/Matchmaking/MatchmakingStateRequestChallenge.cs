﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Requests a new challenge and responds to subsequent events.
	/// TODO: Move all gamesparks code out of state classes
	/// </summary>
	public sealed class MatchmakingStateRequestChallenge : StateMachine.State 
	{
		private User mUserToChallenge;
        private string mChallengeId;

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			// Initialise data
			MatchmakingStateMachine mmsm = this.GetParentStateMachine<MatchmakingStateMachine>();
            mUserToChallenge = mmsm.UserToChallenge;
            
			// Send challenge request
			List<string> users = new List<string>();
			users.Add(mUserToChallenge.UserId);

			ChallengeSetupManager.Get.RequestChallenge( new Dictionary<string, string>(), users );

            ButtonData cancelButton = new ButtonData("Cancel", HandleCancelButton);
            DialogManager.Get.ShowMessagePopup("Waiting for opponent", "Waiting for your opponent to accept the challenge", cancelButton);
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
        }
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			// Listen for challenge started / declined messages
			ServerMessageManager.Get.ChallengeDeclined += HandleChallengeDeclinedMessage;
			ChallengeManager.Get.ChallengeCreated += HandleChallengeStartedMessage;

            ChallengeSetupManager.Get.RequestChallengeSuccess += HandleRequestChallengeSuccess;
			ChallengeSetupManager.Get.Success += HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;

		}            

		private void UnregisterListeners()
		{
			ServerMessageManager.Get.ChallengeDeclined -= HandleChallengeDeclinedMessage;
			ChallengeManager.Get.ChallengeCreated -= HandleChallengeStartedMessage;

            ChallengeSetupManager.Get.RequestChallengeSuccess -= HandleRequestChallengeSuccess;
			ChallengeSetupManager.Get.Success -= HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;

		}
		#endregion

		#region Event Handlers
		private void HandleCancelButton()
		{
			ChallengeSetupManager.Get.WithdrawChallenge( mChallengeId );
		}            

		private void HandleChallengeDeclinedMessage(string nameOfDecliningPlayer)
		{
			DialogManager.Get.HideMessagePopup();

			ButtonData buttonData = new ButtonData("Ok", HandleChallengeFailed);
			DialogManager.Get.ShowMessagePopup("Challenge declined", string.Format("{0} declined your challenge", nameOfDecliningPlayer), buttonData);
		}

		private void HandleChallengeStartedMessage(SJChallengeCreatedMessage message)
		{
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge started");

			DialogManager.Get.HideMessagePopup();

			// We have a match
			OnStateCompleted( new BattleStateConstructionData( (RRChallenge)message.Challenge ) );
		}

		private void HandleChallengeFailed()
		{
			DialogManager.Get.HideMessagePopup();

			// Go back to idle state
			OnStateCompleted( new StateConstructionData( MatchmakingStateMachine.MATCHMAKING_STATE_IDLE ) );
		}

        private void HandleRequestChallengeSuccess(string challengeId)
        {
            SJLogger.LogMessage(MessageFilter.Challenge, "Challenge request sent successfully");
            // Do nothing as we are waiting for the other user to accept or decline the challenge
        }

		private void HandleChallengeSetupEventDataReceived( object o, ChallengeSetupEventDataEventArgs challengeSetupEventDataEventArgs )
		{
			if( challengeSetupEventDataEventArgs.ChallengeSetupEventData is WithdrawChallengeEventData )
			{
				DialogManager.Get.HideMessagePopup();

				mChallengeId = string.Empty;

				// Go back to idle state
				OnStateCompleted( new StateConstructionData( MatchmakingStateMachine.MATCHMAKING_STATE_IDLE ) );
			}
		}

        private void HandleChallengeSetupResponseFailure( ServerError error )
		{
            SJLogger.LogMessage( MessageFilter.UI, "ChallengeSetupError - {0}", error.TechnicalDetails);

			DialogManager.Get.HideMessagePopup();

			ButtonData buttonData = new ButtonData("Ok", HandleChallengeFailed);
            DialogManager.Get.ShowMessagePopup( "Error", error.UserFacingMessage, buttonData);
		}

		private void HandleErrorDismissed()
		{
			OnStateCompleted( new StateConstructionData( ChooseDeckStateMachine.CHOOSE_CUSTOM_DECK_STATE ) );
		}
		#endregion
    }
}