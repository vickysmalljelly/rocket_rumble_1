﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls finding a match for the player
    /// </summary>
    public sealed class FindMatchStateMachine : StateMachine
    {
        // States
        public const string MATCHMAKING_STATE_FIND_MATCH = "MS_FIND_MATCH"; 

        private State mFindMatchState;
        private bool mStartedRunning = false;


        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mFindMatchState = AddState<MatchmakingStateFindMatch>(MATCHMAKING_STATE_FIND_MATCH);

            mFindMatchState.StateCompleted += HandleStateCompleted;
        }

        private void Update()
        {
            if( !mStartedRunning )
            {
                mStartedRunning = true;

                // Move to first state
                TransitionState( MATCHMAKING_STATE_FIND_MATCH );
            }
        }

        private void HandleStateCompleted(State state, StateConstructionData nextStateConstructionData)
        {
            mFindMatchState.StateCompleted -= HandleStateCompleted;

            OnStateMachineCompleted(nextStateConstructionData);
        }
    }
}