﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls single player matchmaking states.  When any of the states complete, a match has been found and we can move on to mulligan phase.
    /// </summary>
    public sealed class SinglePlayerStateMachine : StateMachine
    {
        // States
        public const string SINGLE_PLAYER_STATE_REQUESTED_CHALLENGE = "SP_REQUESTED_CHALLENGE"; 
        public const string SINGLE_PLAYER_STATE_IDLE = "SP_IDLE"; 

        private State mRequestChallengeState;
        private State mIdleState;

        private bool mStartedRunning = false;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mIdleState = AddState<SinglePlayerStateIdle>(SINGLE_PLAYER_STATE_IDLE);
            mRequestChallengeState = AddState<SinglePlayerStateRequestChallenge>(SINGLE_PLAYER_STATE_REQUESTED_CHALLENGE);

            // Initialise transitions
            AddTransition(SINGLE_PLAYER_STATE_IDLE, SINGLE_PLAYER_STATE_REQUESTED_CHALLENGE);
            AddTransition(SINGLE_PLAYER_STATE_REQUESTED_CHALLENGE, SINGLE_PLAYER_STATE_IDLE);      

            mIdleState.StateCompleted += HandleStateCompleted;
            mRequestChallengeState.StateCompleted += HandleStateCompleted;

        }

        private void Update()
        {
            if( !mStartedRunning )
            {
                mStartedRunning = true;

                //Move to first state
                TransitionState( SINGLE_PLAYER_STATE_IDLE );
            }
        }
           
        private void HandleStateCompleted(State state, StateConstructionData nextStateConstructionData)
        {
            mIdleState.StateCompleted -= HandleStateCompleted;
            mRequestChallengeState.StateCompleted -= HandleStateCompleted;

			OnStateMachineCompleted( nextStateConstructionData );
        }
    }
}