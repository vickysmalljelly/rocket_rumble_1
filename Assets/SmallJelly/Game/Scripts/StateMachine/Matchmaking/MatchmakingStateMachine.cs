﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Controls matchmaking states.  When any of the states complete, a match has been found and we can move on to mulligan phase.
	/// </summary>
	public sealed class MatchmakingStateMachine : StateMachine
	{
		#region Constants
		// States
		public const string MATCHMAKING_STATE_IDLE = "MS_IDLE"; 
		public const string MATCHMAKING_STATE_REQUESTED_CHALLENGE = "MS_REQUESTED_CHALLENGE"; 
		#endregion

		#region Public Properties
		public User UserToChallenge { get; set; }
		#endregion

		#region Member Variables
		private State mIdleState;
		private State mRequestChallengeState;

		private bool mStartedRunning = false;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();
			
			// Initialise states

			mIdleState = AddState<MatchmakingStateIdle>(MATCHMAKING_STATE_IDLE);
			mRequestChallengeState = AddState<MatchmakingStateRequestChallenge>(MATCHMAKING_STATE_REQUESTED_CHALLENGE);


			// Initialise transitions

			AddTransition(MATCHMAKING_STATE_IDLE, MATCHMAKING_STATE_REQUESTED_CHALLENGE);
			AddTransition(MATCHMAKING_STATE_REQUESTED_CHALLENGE, MATCHMAKING_STATE_IDLE);      


        }

		private void OnEnable()
		{
			RegisterListeners();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}

		private void Update()
		{
			if( !mStartedRunning )
			{
				mStartedRunning = true;

				//Move to first state
				TransitionState( MATCHMAKING_STATE_IDLE );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mIdleState.StateCompleted += HandleStateCompleted;
			mRequestChallengeState.StateCompleted += HandleStateCompleted;
		}

		private void UnregisterListeners()
		{
			mIdleState.StateCompleted -= HandleStateCompleted;
			mRequestChallengeState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handlers
		private void HandleStateCompleted(State state, StateConstructionData nextStateConstructionData)
		{
			if( nextStateConstructionData is BattleStateConstructionData )
			{
				//Terminate if we're ready to enter the battle
				OnStateMachineCompleted( nextStateConstructionData );
			}
			else
			{
				//Otherwise we should go wherever the state tells us to
				TransitionState( nextStateConstructionData.StateId, nextStateConstructionData );
			}
		}
		#endregion
    }
}