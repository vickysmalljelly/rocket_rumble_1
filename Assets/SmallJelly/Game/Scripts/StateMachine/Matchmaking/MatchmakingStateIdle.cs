﻿using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// The player is in the matchmaking menu
	/// </summary>
	public sealed class MatchmakingStateIdle : StateMachine.State 
	{
		private MatchmakingMenuController mMenuController;

		protected override void Awake()
		{
			base.Awake();

			mMenuController = UIManager.Get.GetMenuController<MatchmakingMenuController>();
			SJLogger.AssertCondition(mMenuController != null, "Can't find MatchmakingMenuController");
		}

		public override void OnEntering()
		{
			base.OnEntering();
			
			// Listen for challenges made by other players
			ServerMessageManager.Get.ChallengeIssued += HandleChallengeIssued;

			// Listen for the player issuing a challenge
			mMenuController.OnlineUserClicked += HandleOnlineUserClicked;
		}
		
		public override void OnLeaving()
        {
            base.OnLeaving();

			// Detach from events
			mMenuController.OnlineUserClicked -= HandleOnlineUserClicked;
			ServerMessageManager.Get.ChallengeIssued -= HandleChallengeIssued;
            ChallengeManager.Get.ChallengeCreated -= HandleChallengeStarted;
        }

		private void HandleOnlineUserClicked(User user)
		{
			SJLogger.LogMessage(MessageFilter.GameState, "User {0} clicked", user.DisplayName);

			MatchmakingStateMachine stateMachine = this.GetParentStateMachine<MatchmakingStateMachine>();
			stateMachine.UserToChallenge = user;
			TransitionState(MatchmakingStateMachine.MATCHMAKING_STATE_REQUESTED_CHALLENGE);
		}
            
        private void HandleChallengeIssued(SJChallengeIssuedMessage message)
		{
            SJLogger.LogMessage(MessageFilter.Challenge, "Challenge issued by {0}", message.ChallengerName);
			
            string popupMessage = string.Format("{0} has challenged you", message.ChallengerName);

			
			ButtonData acceptButton = new ButtonData("Accept", HandleChallengeAccepted);
			ButtonData declineButton = new ButtonData("Decline", HandleChallengeDeclined);
			DialogManager.Get.ShowMessagePopup("Challenge issued", popupMessage, declineButton, acceptButton);
		}
		
		private void HandleChallengeAccepted()
		{
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge Accepted");

			// Send message that the challenge has been accepted
			ClientChallenge.AcceptChallenge(HandleChallengeAcceptedSuccess, HandleChallengeAcceptedError);
        }
		
		private void HandleChallengeDeclined()
		{
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge Declined");
			
			// Send message that the challenge has been declined
			ClientChallenge.DeclineChallenge(HandleChallengeDeclinedSuccess, HandleChallengeDeclinedError);
		}

		private void HandleChallengeAcceptedSuccess()
		{
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge Accepted message sent successfully");

            // Make sure we don't listen to this challenge more than once
            ChallengeManager.Get.ChallengeCreated -= HandleChallengeStarted;

            // Listen for the challenge starting
            ChallengeManager.Get.ChallengeCreated += HandleChallengeStarted;
        }

        private void HandleChallengeStarted(SJChallengeCreatedMessage message)
        {
            // The matchmaking stage is completed
            OnStateCompleted( new BattleStateConstructionData( (RRChallenge)message.Challenge ) );
        }
		
		private void HandleChallengeAcceptedError(ServerError error)
		{
            if(error.ErrorType == ServerErrorType.ChallengeCancelled)
            {
                ButtonData okButton = new ButtonData("Ok", HandleChallengeErrorOkButton);
                DialogManager.Get.ShowMessagePopup("Challenge cancelled", error.UserFacingMessage, okButton);
                return;
            }

            SJLogger.LogError("Challenge Accepted message failed: {0}", error.TechnicalDetails);
        }
            
		private void HandleChallengeDeclinedSuccess()
		{
            ChallengeManager.Get.ResetChallenge();
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge Declined message sent successfully");
		}
		
		private void HandleChallengeDeclinedError(ServerError error)
		{
            ChallengeManager.Get.ResetChallenge();

            if(error.ErrorType == ServerErrorType.ChallengeCancelled)
            {
                ButtonData okButton = new ButtonData("Ok", HandleChallengeErrorOkButton);
                DialogManager.Get.ShowMessagePopup("Challenge cancelled", error.UserFacingMessage, okButton);
                return;
            }

            SJLogger.LogError("Challenge Declined message failed: {0}", error.TechnicalDetails);
		}

        private void HandleChallengeErrorOkButton()
        {
            ChallengeManager.Get.ResetChallenge();
        }
	}
}