﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// The player is in the single player menu
    /// N.B. This is currently just a placeholder, the UI will change shortly
    /// </summary>
    public sealed class SinglePlayerStateIdle : StateMachine.State 
    {
		#region State Machine Methods
        public override void OnEntering()
        {
            base.OnEntering();

			RegisterListeners();

            // TODO: The id of the AI player is now stored in PlayerData
            // Find the system_ai player to battle
			ChallengeSetupManager.Get.FindUserByName( "system_ai" );
        }

        public override void OnLeaving()
        {
            base.OnLeaving();

			UnregisterListeners();
        }
		#endregion


		#region Event Registration
		private void RegisterListeners()
		{
            ChallengeSetupManager.Get.FindUserSuccess += HandleFindUserSuccess;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;
		}

		private void UnregisterListeners()
		{
            ChallengeSetupManager.Get.FindUserSuccess -= HandleFindUserSuccess;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;
		}
		#endregion

		#region Event Handlers
        private void HandleFindUserSuccess(User user)
        {           
            // Set the user we want to challenge
            ChallengeManager.Get.UserToChallenge = user;
			TransitionState(SinglePlayerStateMachine.SINGLE_PLAYER_STATE_REQUESTED_CHALLENGE);			
        }

        private void HandleChallengeSetupResponseFailure( ServerError error ) 
        {
            SJLogger.LogError( "{0}", error.TechnicalDetails );
        }
		#endregion
    }
}