﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Requests a new challenge with an AI and responds to subsequent events.
    /// TODO: Move all gamesparks code out of state classes
    /// </summary>
    public sealed class SinglePlayerStateRequestChallenge : StateMachine.State 
    {
		#region State Machine Methods
        public override void OnEntering()
        {
            base.OnEntering();

            // TODO: Add message - waiting for response from...
            DialogManager.Get.ShowSpinner();

			RegisterListeners();

            // Send challenge request
            List<string> users = new List<string>();
            users.Add(ChallengeManager.Get.UserToChallenge.UserId);

            Dictionary<string, string> scriptData = new Dictionary<string, string>();
            scriptData.Add("battleId", BattleModeManager.Get.CurrentBattleId.ToString());

			ChallengeSetupManager.Get.RequestChallenge( scriptData, users );
        }

        public override void OnLeaving()
        {
            base.OnLeaving();

			UnregisterListeners();
        }
		#endregion

		#region Register Listeners
		private void RegisterListeners()
		{
			// Listen for challenge started / declined messages
			ServerMessageManager.Get.ChallengeDeclined += HandleChallengeDeclinedMessage;
			ChallengeManager.Get.ChallengeCreated += HandleChallengeStartedMessage;

			ChallengeSetupManager.Get.Success += HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;
		}

		private void UnregisterListeners()
		{
			ServerMessageManager.Get.ChallengeDeclined -= HandleChallengeDeclinedMessage;
			ChallengeManager.Get.ChallengeCreated -= HandleChallengeStartedMessage;

			ChallengeSetupManager.Get.Success -= HandleChallengeSetupEventDataReceived;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;
		}
		#endregion

		#region Event Handlers
		private void HandleChallengeSetupEventDataReceived( object o, ChallengeSetupEventDataEventArgs challengeSetupEventDataEventArgs )
        {
            SJLogger.LogMessage(MessageFilter.AI, "Challenge request sent successfully - waiting for other user to accept or decline");

            // Do nothing as we are waiting for the other user to accept or decline the challenge
        }

        private void HandleChallengeSetupResponseFailure( ServerError error )
        {
            SJLogger.LogMessage(MessageFilter.AI, "ChallengeRequestError - {0}", error.TechnicalDetails );
            DialogManager.Get.HideSpinner();

            ButtonData buttonData = new ButtonData("Ok", HandleChallengeFailed);
            DialogManager.Get.ShowMessagePopup( "Error", error.UserFacingMessage, buttonData);
        }
            
        private void HandleChallengeDeclinedMessage(string nameOfDecliningPlayer)
        {
            DialogManager.Get.HideSpinner();

            ButtonData buttonData = new ButtonData("Ok", HandleChallengeFailed);
            DialogManager.Get.ShowMessagePopup("Challenge declined", string.Format("{0} declined your challenge", nameOfDecliningPlayer), buttonData);
        }

        private void HandleChallengeStartedMessage(SJChallengeCreatedMessage message)
        {
            SJLogger.LogMessage(MessageFilter.GameState, "Challenge started");

            DialogManager.Get.HideSpinner();

            // We have a match
            OnStateCompleted( new BattleStateConstructionData( (RRChallenge)message.Challenge ) );
        }

        private void HandleChallengeFailed()
        {
            // Go back to idle state
            TransitionState(MatchmakingStateMachine.MATCHMAKING_STATE_IDLE);
        }
		#endregion
    }
}