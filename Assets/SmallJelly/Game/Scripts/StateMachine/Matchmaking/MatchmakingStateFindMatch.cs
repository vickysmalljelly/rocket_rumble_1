﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// The player is looking for a match
    /// </summary>
    public sealed class MatchmakingStateFindMatch : StateMachine.State 
    {
        public override void OnEntering()
        {
            base.OnEntering();

            AttachToEvents();

            ChallengeSetupManager.Get.FindMatch(BattleModeManager.Get.GetRandomBattleMatchType());
        }

        public override void OnLeaving()
        {
            base.OnLeaving();

            DetachFromEvents();
        }

        private void AttachToEvents()
        {
            // Listen for a successful match
            ChallengeManager.Get.ChallengeCreated += HandleChallengeCreated;

            // Listen for not finding a match
            ServerMessageManager.Get.MatchNotFound += HandleMatchNotFound;

            ChallengeSetupManager.Get.RequestChallengeSuccess += HandleRequestChallengeSuccess;
            ChallengeSetupManager.Get.Error += HandleChallengeSetupResponseFailure;
        }
            
        private void DetachFromEvents()
        {
            // Detach from events
            ChallengeManager.Get.ChallengeCreated -= HandleChallengeCreated;
            ServerMessageManager.Get.MatchNotFound -= HandleMatchNotFound;

            ChallengeSetupManager.Get.RequestChallengeSuccess -= HandleRequestChallengeSuccess;
            ChallengeSetupManager.Get.Error -= HandleChallengeSetupResponseFailure;
        }            

        private void HandleMatchNotFound()
        {
            SJLogger.LogMessage(MessageFilter.Gameplay, "HandleMatchNotFound: Attempt to match with an AI player instead");

            if(GameManager.Get.DebugConfig.DontMatchWithAI)
            {
                HandleChallengeSetupResponseFailure(new ServerError(ServerErrorType.NotSet, "Forcing error for debug", "Forcing error for debug"));
                return;                    
            }

            // Human player not found, match with an AI player instead.
            BattleModeManager.Get.SetBattleConfig(BattleMode.SinglePlayer, BattleId.SinglePlayer, MatchmakingMode.NotSet);

            List<string> users = new List<string>();
            users.Add(GameManager.Get.PlayerData.AiPlayerId);

            Dictionary<string, string> scriptData = new Dictionary<string, string>();
            scriptData.Add("battleId", BattleModeManager.Get.CurrentBattleId.ToString());

            ChallengeSetupManager.Get.RequestChallenge(scriptData, users);
        }

        private void HandleRequestChallengeSuccess(string challengeId)
        {
            // We have matched with the AI, no need to do anything, ChallengeCreated will be fired by the server
            SJLogger.LogMessage(MessageFilter.Gameplay, "We have matched with the AI");
        }
                   
        private void HandleChallengeSetupResponseFailure( ServerError error )
        {
            SJLogger.LogError("We have failed to match with the AI. Error type: {0}, {1}", error.ErrorType, error.TechnicalDetails);
           
            // DO NOT show the user facing message, as this would give away that we are using bots.
            ButtonData okButton = new ButtonData( "Ok", HandleAcknowledgmentOfMatchFail );
            DialogManager.Get.ShowMessagePopup( "Error", "An error occurred during matchmaking. Please try again.", okButton );
        }

        private void HandleAcknowledgmentOfMatchFail()
        {
            // We failed to find a match, go back to the main menu
			OnStateCompleted(new StateConstructionData(GameStateMachine.GAME_STATE_FRONT_END));
        }
            
        private void HandleChallengeCreated(SJChallengeCreatedMessage message)
        {
            // The matchmaking stage is completed, we are ready for battle
            OnStateCompleted( new BattleStateConstructionData((RRChallenge)message.Challenge) );
        }                   
    }
}