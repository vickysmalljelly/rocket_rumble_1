﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	
	/// <summary>
	/// Parent for the state machine that controls the navigation buttons.
	/// </summary>
	public sealed class FrontEndState : AbstractGameState
	{
		#region Member Variables
		private FrontEndStateMachine mFrontEndStateMachine;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			UIManager.Get.ShowMenu< FrontEndNavigationController >();

           	mFrontEndStateMachine = this.AddChildStateMachine< FrontEndStateMachine >();
							
			RegisterListeners();
		}

		public override void OnLeaving()
		{
            UnregisterListeners();

            UIManager.Get.HideMenu< FrontEndNavigationController >();

			base.OnLeaving();
		}
		#endregion

		#region Event Handlers
        private void RegisterListeners()
		{
			//Register rumble state events
			mFrontEndStateMachine.StateMachineCompleted += HandleStateMachineCompleted;

			mFrontEndStateMachine.LogoutClicked += HandleLogoutClicked;
			mFrontEndStateMachine.MapModeClicked += HandleMapModeClicked;
			mFrontEndStateMachine.MultiplayerClicked += HandleMultiplayerClicked;
			mFrontEndStateMachine.RumbleClicked += HandleRumbleClicked;
			mFrontEndStateMachine.SinglePlayerClicked += HandleSinglePlayerClicked;
        }

        private void UnregisterListeners()
        {
			//Register rumble state events
			mFrontEndStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			mFrontEndStateMachine.LogoutClicked -= HandleLogoutClicked;
			mFrontEndStateMachine.MapModeClicked -= HandleMapModeClicked;
			mFrontEndStateMachine.MultiplayerClicked -= HandleMultiplayerClicked;
			mFrontEndStateMachine.RumbleClicked -= HandleRumbleClicked;
			mFrontEndStateMachine.SinglePlayerClicked -= HandleSinglePlayerClicked;
        }
		#endregion

		#region Event Handlers
		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionData )
		{
			OnStateCompleted( stateConstructionData );
		}

		private void HandleMapModeClicked( object o, EventArgs args )
		{
            BattleModeManager.Get.SetBattleConfig(BattleMode.TwoPlayer, BattleId.PvP, MatchmakingMode.Random);			
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_MAP ) );
		}

		private void HandleReplayClicked( object o, EventArgs args )
		{
		}

		private void HandleLogoutClicked( object o, EventArgs args )
        {
            GameManager.Get.Logout();

            #if USERNAME_LOGIN_ALLOWED
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_USERNAME_PW_AUTH ) );
            #endif
		}

		private void HandleMultiplayerClicked( object o, EventArgs args )
		{
            BattleModeManager.Get.SetBattleConfig(BattleMode.TwoPlayer, BattleId.PvP, MatchmakingMode.NotSet);

            if(SJGameSparksManager.Get.PlayerAuthenticated)
			{
				OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_MATCHMAKING_MENU ) );
				return;
			}
            #if USERNAME_LOGIN_ALLOWED
			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_USERNAME_PW_AUTH ) );
            #endif
		}

		private void HandleRumbleClicked( object o, EventArgs args )
		{
			FireRumbleAnalytic();

            BattleModeManager.Get.SetBattleConfig(BattleMode.TwoPlayer, BattleId.PvP, MatchmakingMode.Random);

			OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_MATCHMAKING_CHOOSE_DECK ) );
		}

        private void HandleSinglePlayerClicked(BattleId battleId)
		{
            BattleModeManager.Get.SetBattleConfig(BattleMode.SinglePlayer, battleId, MatchmakingMode.NotSet);

            if(SJGameSparksManager.Get.PlayerAuthenticated)
			{
				OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK ) );
				return;
			}               
		}           
		#endregion

		#region Analytics
		private void FireRumbleAnalytic()
		{
			//Log analytics for rumble
			AnalyticsManager.Get.RecordGameProgress(AnalyticsManager.GameProgress.Rumble, 0);
		}
		#endregion
	}
}
