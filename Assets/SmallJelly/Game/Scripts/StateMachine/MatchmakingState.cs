using SmallJelly.Framework;
using System.Collections.Generic;


namespace SmallJelly
{
	
	/// <summary>
	/// Shows the matchmaking menu and holds the MatchMakingStateMachine
    /// VJS This is now more of a "Challenge a friend" state.
	/// </summary>
	public sealed class MatchmakingState : AbstractGameState 
	{
		private MatchmakingMenuController mMatchmakingMenuController;

		public override void OnEntering()
		{
			base.OnEntering();

			// Show the menu
			mMatchmakingMenuController = UIManager.Get.ShowMenu<MatchmakingMenuController>();
			SJLogger.AssertCondition(mMatchmakingMenuController != null, "Don't have a MatchmakingMenuController");

            // Listen for button clicks
			mMatchmakingMenuController.LogoutClicked += HandleLogoutClicked;
            mMatchmakingMenuController.BackButtonClicked += HandleBackButtonClicked;

			MatchmakingStateMachine stateMachine = this.AddChildStateMachine<MatchmakingStateMachine>();
			stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
		}

		public override void OnLeaving()
		{
            mMatchmakingMenuController.LogoutClicked -= HandleLogoutClicked;
            mMatchmakingMenuController.BackButtonClicked -= HandleBackButtonClicked;
	  
            // Hide the menu
			UIManager.Get.HideMenu<MatchmakingMenuController>();

            base.OnLeaving();
		}

		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
		{
			stateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			TransitionState( stateConstructionMessageData.StateId, stateConstructionMessageData);
		}

        private void HandleBackButtonClicked()
        {
            TransitionState(GameStateMachine.GAME_STATE_FRONT_END);
        }

		private void HandleLogoutClicked()
		{
			mMatchmakingMenuController.LogoutClicked -= HandleLogoutClicked;

            /*
             *  Removing as it's not possible to logout when using device id for auth.
             * 

            GameManager.Get.Logout();

			TransitionState(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH);
            */         
		}

        private void HandleDeckSuccess()
        {
            // Wait for turn taken event
        }

        private void HandleDeckError(ServerError error)
        {
            SJLogger.LogError("Deck error: {0}", error.TechnicalDetails);
        }

	}
}