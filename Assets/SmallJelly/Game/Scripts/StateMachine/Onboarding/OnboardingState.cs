﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Top level state used to trigger appropriate onboarding.  
    /// </summary>
	public class OnboardingState : AbstractGameState
    {
        private StateMachine mActiveOnboardingStateMachine;

        public override void OnEntering()
        {
            base.OnEntering();

			//For the initial release we don't want to show the disclaimer screens - I (George) have spoken to Ben and he thinks they may
			//wish to be re-added in the future so I shall leave all of the code as-is.
			TransitionState(GameStateMachine.GAME_STATE_FRONT_END);
			return;
			/**
			if( OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.WelcomeCheckpoint ) || GameManager.Get.DebugConfig.DisableInboxOnboarding )
            {
                // We have already shown the tutorial
                TransitionState(GameStateMachine.GAME_STATE_START_MENU);
                return;
            }

            // Need to show the demo warning screen, the sell screen then the tutorial
            mActiveOnboardingStateMachine = this.AddChildStateMachine<OnboardingWelcomeStateMachine>();
            mActiveOnboardingStateMachine.StateMachineCompleted += HandleStateMachineCompleted;
            **/
        }

		/**
        public override void OnLeaving()
        {
            if(mActiveOnboardingStateMachine != null)
            {
                mActiveOnboardingStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;
            }

            base.OnLeaving();
        }
            
        private void HandleStateMachineCompleted(StateMachine stateMachine, StateConstructionData constructionData)
        {
            TransitionState(GameStateMachine.GAME_STATE_START_MENU);
        }
        **/
    }
}

