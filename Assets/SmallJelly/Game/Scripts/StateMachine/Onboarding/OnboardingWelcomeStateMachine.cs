﻿
using SmallJelly.Framework;


namespace SmallJelly
{
    /// <summary>
    /// State machine to control showing welcome screens at the start of the game 
    /// and the first part of the tutorial
    /// </summary>
    public sealed class OnboardingWelcomeStateMachine : StateMachine
    {
        // Game states
        public const string ON_DISCLAIMER = "ON_DISCLAIMER";  

        private DisclaimerState mDisclaimerState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mDisclaimerState = AddState<DisclaimerState>(ON_DISCLAIMER);

            mDisclaimerState.StateCompleted += HandleStateCompleted;

            TransitionState(ON_DISCLAIMER);
        }

        private void OnDestroy()
        {
            mDisclaimerState.StateCompleted -= HandleStateCompleted;
        }

        private void HandleStateCompleted(State state, StateConstructionData constructionData)
        {
            this.OnStateMachineCompleted();
        }
    }
}
