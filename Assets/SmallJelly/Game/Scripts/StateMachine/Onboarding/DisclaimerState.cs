﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Shows a few screens via a playmaker graph in the prefab 
    /// </summary>
    public sealed class DisclaimerState : StateMachine.State 
    {
        private List<GameObject> mLoadedPrefabs = new List<GameObject>();
        private List<PlayMakerFSM> mPlayerMakerGraphs = new List<PlayMakerFSM>();

        public override void OnEntering()
        {
            base.OnEntering();

            // Load the prefab containing the screens
            GameObject prefab = this.LoadPrefab(FileLocations.DemoIntroPrefab);
            GameObject instantiated = this.InstantiatePrefab(prefab, mLoadedPrefabs, UIManager.Get.UIRoot);

            // Find the Playmaker graph in the prefab
            instantiated.GetComponentsInChildren<PlayMakerFSM>(true, mPlayerMakerGraphs);
        }

        public override void OnLeaving()
        {
            foreach(GameObject item in mLoadedPrefabs)
            {
                Destroy(item.gameObject);
            }

            base.OnLeaving();
        }

        private void Update()
        {
            foreach(PlayMakerFSM graph in mPlayerMakerGraphs)
            {
                if(graph.Active) 
                {
                    // Not finished yet
                    return;
                }
            }

            // All finished
            OnStateCompleted();
        }
    }
}
