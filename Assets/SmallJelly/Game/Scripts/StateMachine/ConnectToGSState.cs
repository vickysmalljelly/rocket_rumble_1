using System.Collections;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{

	/// <summary>
	/// Initial state of the game, currently contains UI, but will change later
	/// </summary>
	public class ConnectToGSState : AbstractGameState
	{
		#region Constants
		private const int SECONDS_UNTIL_SIMULATED_TIMEOUT = 10;
		#endregion

		#region Member Variables
        private bool mFirstUpdate = true;
        private bool mTransitioningToAuth;
		private IEnumerator mEnumerator;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

            #if UNITY_IOS
            Application.targetFrameRate = 60;
            #endif

			//From Unity 5.3 and onwards you have to store a reference to an enumerator in order to stop it!
			mEnumerator = HandleSimulatedTimeout();
			GameManager.Get.DoCoroutine( mEnumerator );

			DialogManager.Get.ShowSpinner();

			InitialiseMenus();

			InitialiseChallenges();          
		}

        public override void OnLeaving()
        {
            SJGameSparksManager.Get.SdkBecameAvailable -= HandleSdkBecameAvailable; 

			GameManager.Get.StopCoroutine( mEnumerator );

			DialogManager.Get.HideSpinner();
			DialogManager.Get.HideSpinnerWithMessage();

            base.OnLeaving();

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Launch, null, null);
        }
		#endregion

		#region MonoBehaviour Methods
		private void Update()
		{
            if(mFirstUpdate) 
            {
                StartGame();
                mFirstUpdate = false;
            }
		}
		#endregion

		#region Private Methods
        private void StartGame()
        {
            if(SJGameSparksManager.Get.SdkAvailable)
            {
                // We are connected to the server
                CheckAuthentication();
                return;
            }

            // Wait for the server to be available
            SJGameSparksManager.Get.SdkBecameAvailable += HandleSdkBecameAvailable; 
        }

		private void CheckAuthentication()
		{
			if( SJGameSparksManager.Get.PlayerAuthenticated )
			{
				TransitionState(GameStateMachine.GAME_STATE_PLAYER_UPDATE, new StateConstructionData(GameStateMachine.GAME_STATE_ONBOARDING));
			}
			else
			{
                mTransitioningToAuth = true;

                #if UNITY_EDITOR
                   
					#if USERNAME_LOGIN_ALLOWED
    				TransitionState(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH);
                    #else
                    TransitionState(GameStateMachine.GAME_STATE_DEVICE_AUTH);
                   	#endif

                #else
                TransitionState(GameStateMachine.GAME_STATE_DEVICE_AUTH);
                #endif
			}
		}

		private static void InitialiseMenus()
		{
			GameObject authenticationMenu = UIManager.Get.FindChildObjectByName("ui_authenticationMenu");
			SJLogger.AssertCondition(authenticationMenu != null, "Can't find AuthenticationMenu object");
			UIManager.Get.AddMenu< AuthenticationMenuController >(authenticationMenu);

			GameObject matchmakingMenu = UIManager.Get.FindChildObjectByName("ui_matchmakingMenu");
			SJLogger.AssertCondition(matchmakingMenu != null, "Can't find MatchmakingMenu object");
			UIManager.Get.AddMenu< MatchmakingMenuController >(matchmakingMenu);

			GameObject replayMenu = UIManager.Get.FindChildObjectByName("ui_replayMenu");
			SJLogger.AssertCondition(replayMenu != null, "Can't find ReplayMenu object");
			UIManager.Get.AddMenu< ReplayMenuController >(replayMenu);

			GameObject demoUpsell = UIManager.Get.FindChildObjectByName( "ui_demoUpsell" );
			SJLogger.AssertCondition( demoUpsell != null, "Can't find DemoUpsell object" );
			UIManager.Get.AddMenu< DemoUpsellDisplayController >( demoUpsell );

			GameObject frontEndNavigation = UIManager.Get.FindChildObjectByName( "ui_frontEndNavigation" );
			SJLogger.AssertCondition( frontEndNavigation != null, "Can't find FrontEndNavigation object" );
			FrontEndNavigationController controller = UIManager.Get.AddMenu< FrontEndNavigationController >( frontEndNavigation );
			controller.ListenToCreditsUpdated();
		}

		private static void InitialiseChallenges()
		{
			const string challengeRequestMessage = "Let's see what your ship is made of.";
			ChallengeManager.Get.SetChallengeRequestMessage(challengeRequestMessage);

		}
		#endregion

		#region Event Handlers
		private IEnumerator HandleSimulatedTimeout()
		{
			yield return new WaitForSeconds( SECONDS_UNTIL_SIMULATED_TIMEOUT );

            if(mTransitioningToAuth)
            {
                yield break;
            }
                
            AnalyticsManager.Get.RecordConnectionLossEvent("initial_state");

			DialogManager.Get.HideSpinner();
            string title = "Trying to Connect...";
            string message = "Failed to connect to our servers, please check your internet connection. \nIf you still get this error, we'd really appreciate an email to support@smalljelly.com, thanks!";
			DialogManager.Get.ShowSpinnerWithMessage( title, message );
		}

        private void HandleSdkBecameAvailable()
        {
            CheckAuthentication();
        }
		#endregion
	}
}
