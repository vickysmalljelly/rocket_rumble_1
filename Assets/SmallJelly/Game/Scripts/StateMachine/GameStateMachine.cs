using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls the state of the game.
    /// </summary>
    public sealed class GameStateMachine : StateMachine
    {
		#region Constants
        // Game states
		public const string GAME_STATE_FIND_SERVER_BY_LOCATION = "GAME_STATE_FIND_SERVER_BY_LOCATION";
		public const string GAME_STATE_CONNECT_TO_GS = "GAME_STATE_CONNECT_TO_GS";
        public const string GAME_STATE_CONNECTION_LOST = "GAME_STATE_CONNECTION_LOST"; 
		public const string GAME_STATE_FRONT_END = "GAME_STATE_FRONT_END"; 
        #if USERNAME_LOGIN_ALLOWED
        public const string GAME_STATE_USERNAME_PW_AUTH = "GAME_STATE_USERNAME_PW_AUTH"; 
        #endif
        public const string GAME_STATE_DEVICE_AUTH = "GAME_STATE_DEVICE_AUTH";  
        public const string GAME_STATE_PLAYER_UPDATE = "GAME_STATE_PLAYER_UPDATE"; 
        public const string GAME_STATE_FORCE_UPDATE = "GAME_STATE_FORCE_UPDATE";
        public const string GAME_STATE_ONBOARDING = "GAME_STATE_ONBOARDING"; 

        public const string GAME_STATE_MATCHMAKING_MENU = "GAME_STATE_MATCHMAKING_MENU"; 
        public const string GAME_STATE_FIND_MATCH = "GAME_STATE_FIND_MATCH"; 
        public const string GAME_STATE_SINGLE_PLAYER_MENU = "GAME_STATE_SINGLE_PLAYER_MENU"; 

        public const string GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK = "GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK"; 
		public const string GAME_STATE_MATCHMAKING_CHOOSE_DECK = "GAME_STATE_MATCHMAKING_CHOOSE_DECK"; 

        public const string GAME_STATE_MAP = "GAME_STATE_MAP"; 
        public const string GAME_STATE_PLAY_BATTLE = "GAME_STATE_PLAY_BATTLE"; 
        public const string GAME_STATE_DEMO_UPSELL = "GAME_STATE_DEMO_UPSELL"; 
        public const string GAME_STATE_REPLAY = "GAME_STATE_REPLAY"; 
		#endregion

		#region Member Variables
		private bool mStartedRunning = false;

		private SinglePlayerChooseDeckState mSinglePlayerChooseDeckState;
		private MatchmakingChooseDeckState mMatchmakingChooseDeckState;

		private FrontEndState mFrontEndState;
		private ReplayState mReplayState;
		private SinglePlayerState mSinglePlayerState;
		private FindMatchState mFindMatchState;
		private MatchmakingState mMatchmakingState;
        private UpdatePlayerDataState mUpdatePlayerDataState;
		#endregion

		#region MonoBehaviour Methods
        protected override void Awake()
        {
			base.Awake();

            // Initialise states
			AddState< FindServerByLocationState >( GAME_STATE_FIND_SERVER_BY_LOCATION );
            AddState< ConnectToGSState >( GAME_STATE_CONNECT_TO_GS );

            #if USERNAME_LOGIN_ALLOWED
            AddState< UsernamePwAuthState >( GAME_STATE_USERNAME_PW_AUTH );
            #endif
            AddState< DeviceAuthState >( GAME_STATE_DEVICE_AUTH );

			AddState< MapState >( GAME_STATE_MAP );
            mUpdatePlayerDataState = AddState< UpdatePlayerDataState > ( GAME_STATE_PLAYER_UPDATE );
            AddState< ForceGameUpdateState > ( GAME_STATE_FORCE_UPDATE );
            AddState< OnboardingState > ( GAME_STATE_ONBOARDING );

			AddState< BattleState >( GAME_STATE_PLAY_BATTLE );
			AddState< DemoUpsellState >( GAME_STATE_DEMO_UPSELL );
			AddState< ConnectionLostState >( GAME_STATE_CONNECTION_LOST );

			mFrontEndState = AddState< FrontEndState >( GAME_STATE_FRONT_END );

			mSinglePlayerChooseDeckState = AddState< SinglePlayerChooseDeckState >( GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK );
			mMatchmakingChooseDeckState = AddState< MatchmakingChooseDeckState >( GAME_STATE_MATCHMAKING_CHOOSE_DECK );

			mReplayState = AddState< ReplayState >( GAME_STATE_REPLAY );
			mSinglePlayerState = AddState< SinglePlayerState >( GAME_STATE_SINGLE_PLAYER_MENU );
			mFindMatchState = AddState< FindMatchState >( GAME_STATE_FIND_MATCH );
			mMatchmakingState = AddState< MatchmakingState >( GAME_STATE_MATCHMAKING_MENU );


            // Initialise transitions
			AddTransition(GAME_STATE_FIND_SERVER_BY_LOCATION, GAME_STATE_CONNECT_TO_GS);
            AddTransition(GAME_STATE_CONNECT_TO_GS, GAME_STATE_PLAYER_UPDATE);
            #if USERNAME_LOGIN_ALLOWED
			AddTransition(GAME_STATE_CONNECT_TO_GS, GAME_STATE_USERNAME_PW_AUTH);
            #endif
            AddTransition(GAME_STATE_CONNECT_TO_GS, GAME_STATE_DEVICE_AUTH);

            // Transitions out of auth states
            #if USERNAME_LOGIN_ALLOWED
            AddTransition(GAME_STATE_USERNAME_PW_AUTH, GAME_STATE_PLAYER_UPDATE);
            #endif
            AddTransition(GAME_STATE_DEVICE_AUTH, GAME_STATE_PLAYER_UPDATE);
            AddTransition(GAME_STATE_DEVICE_AUTH, GAME_STATE_CONNECTION_LOST);

            AddTransition(GAME_STATE_PLAYER_UPDATE, GAME_STATE_ONBOARDING);
            AddTransition(GAME_STATE_ONBOARDING, GAME_STATE_FRONT_END);

			AddTransition( GAME_STATE_FRONT_END, GAME_STATE_MATCHMAKING_MENU );
            AddTransition( GAME_STATE_FRONT_END, GAME_STATE_SINGLE_PLAYER_MENU );
			AddTransition( GAME_STATE_FRONT_END, GAME_STATE_MAP );
            AddTransition( GAME_STATE_FRONT_END, GAME_STATE_REPLAY );
            #if USERNAME_LOGIN_ALLOWED
            AddTransition( GAME_STATE_FRONT_END, GAME_STATE_USERNAME_PW_AUTH );
            #endif
			AddTransition( GAME_STATE_FRONT_END, GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK );
			AddTransition( GAME_STATE_FRONT_END, GAME_STATE_MATCHMAKING_CHOOSE_DECK );

            AddTransition( GAME_STATE_MAP, GAME_STATE_PLAY_BATTLE );
            AddTransition( GAME_STATE_MAP, GAME_STATE_FRONT_END );
			AddTransition( GAME_STATE_MAP, GAME_STATE_SINGLE_PLAYER_MENU );

			AddTransition( GAME_STATE_MATCHMAKING_CHOOSE_DECK, GAME_STATE_FIND_MATCH );
			AddTransition( GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK, GAME_STATE_SINGLE_PLAYER_MENU );

			AddTransition( GAME_STATE_MATCHMAKING_CHOOSE_DECK, GAME_STATE_FRONT_END );
			AddTransition( GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK, GAME_STATE_FRONT_END );

            #if USERNAME_LOGIN_ALLOWED
			AddTransition( GAME_STATE_MATCHMAKING_MENU, GAME_STATE_USERNAME_PW_AUTH );
            #endif
			AddTransition( GAME_STATE_MATCHMAKING_MENU, GAME_STATE_PLAY_BATTLE );
            AddTransition( GAME_STATE_MATCHMAKING_MENU, GAME_STATE_FRONT_END );

            #if USERNAME_LOGIN_ALLOWED
            AddTransition( GAME_STATE_SINGLE_PLAYER_MENU, GAME_STATE_USERNAME_PW_AUTH );
            #endif
            AddTransition( GAME_STATE_SINGLE_PLAYER_MENU, GAME_STATE_PLAY_BATTLE );

			AddTransition( GAME_STATE_PLAY_BATTLE, GAME_STATE_FRONT_END );
			AddTransition( GAME_STATE_PLAY_BATTLE, GAME_STATE_MAP );

            AddTransition( GAME_STATE_REPLAY, GAME_STATE_PLAY_BATTLE );

            AddTransition( GAME_STATE_FIND_MATCH, GAME_STATE_PLAY_BATTLE );
            AddTransition( GAME_STATE_FIND_MATCH, GAME_STATE_FRONT_END );

			//Handle state transitions to connection lost state
			AddTransition( GAME_STATE_CONNECT_TO_GS, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_CONNECTION_LOST, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_FRONT_END, GAME_STATE_CONNECTION_LOST );
            #if USERNAME_LOGIN_ALLOWED
			AddTransition( GAME_STATE_USERNAME_PW_AUTH, GAME_STATE_CONNECTION_LOST );
            #endif
			AddTransition( GAME_STATE_PLAYER_UPDATE, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_ONBOARDING, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_MATCHMAKING_MENU, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_FIND_MATCH, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_SINGLE_PLAYER_MENU, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_SINGLE_PLAYER_CHOOSE_DECK, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_MATCHMAKING_CHOOSE_DECK, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_MAP, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_PLAY_BATTLE, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_DEMO_UPSELL, GAME_STATE_CONNECTION_LOST );
			AddTransition( GAME_STATE_REPLAY, GAME_STATE_CONNECTION_LOST );

			AddTransition( GAME_STATE_CONNECTION_LOST, GAME_STATE_PLAYER_UPDATE );
            AddTransition( GAME_STATE_PLAYER_UPDATE, GAME_STATE_FORCE_UPDATE);

            // For reset
            AddTransition(GAME_STATE_FRONT_END, GAME_STATE_CONNECT_TO_GS);

			RegisterListeners();
        }

		private void Update()
		{
			if( !mStartedRunning )
			{
				mStartedRunning = true;

				//Move to first state
				TransitionState( GAME_STATE_FIND_SERVER_BY_LOCATION );
			}
		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

        #region Public Methods
        #if USERNAME_LOGIN_ALLOWED
        public void ForceTransitionToAuthenticationState()
        {
            TransitionState(GameStateMachine.GAME_STATE_USERNAME_PW_AUTH);
        }
        #endif
        #endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mFrontEndState.StateCompleted += HandleStateCompleted;
			mSinglePlayerChooseDeckState.StateCompleted += HandleStateCompleted;
			mMatchmakingChooseDeckState.StateCompleted += HandleStateCompleted;
			mReplayState.StateCompleted += HandleStateCompleted;
			mSinglePlayerState.StateCompleted += HandleStateCompleted;
			mFindMatchState.StateCompleted += HandleStateCompleted;
			mMatchmakingState.StateCompleted += HandleStateCompleted;
            mUpdatePlayerDataState.StateCompleted += HandleUpdatePlayerStateCompleted;
		}

		private void UnregisterListeners()
		{
			mFrontEndState.StateCompleted -= HandleStateCompleted;
            mUpdatePlayerDataState.StateCompleted -= HandleUpdatePlayerStateCompleted;
			mSinglePlayerChooseDeckState.StateCompleted -= HandleStateCompleted;
			mMatchmakingChooseDeckState.StateCompleted -= HandleStateCompleted;
			mReplayState.StateCompleted -= HandleStateCompleted;
			mSinglePlayerState.StateCompleted -= HandleStateCompleted;
			mFindMatchState.StateCompleted -= HandleStateCompleted;
			mMatchmakingState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handlers
        private void HandleUpdatePlayerStateCompleted( State state, StateConstructionData stateConstructionData )
        {
            TransitionState( GAME_STATE_ONBOARDING );
        }

		private void HandleStateCompleted( State state, StateConstructionData stateConstructionData )
		{
			StateConstructionData = stateConstructionData;
		}
		#endregion
    }
}