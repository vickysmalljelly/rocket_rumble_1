﻿using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	public sealed class ConnectionLostState : AbstractFrontEndState< PlayMenuController >
	{
		#region Protected Properties
		protected override string FrontEndPath 
		{
			get
			{
				return FileLocations.PlayMenuPrefab;
			}
		}
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

            AnalyticsManager.Get.RecordConnectionLossEvent("connection_loss_state");

			RegisterListeners();

			FrontEndNavigationController.gameObject.SetActive( false );
			MenuController.HideNavigationButtons();
            			
            DialogManager.Get.ShowConnectionLossPopup();

			Ping();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
            			
            DialogManager.Get.HideConnectionLossPopup();

			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void Ping()
		{
			GameManager.Get.Ping();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			GameManager.Get.ConnectionReestablished += HandleConnectionReestablished;
			GameManager.Get.ConnectionFailed += HandleConnectionFailed;
		}

		private void UnregisterListeners()
		{
			GameManager.Get.ConnectionReestablished -= HandleConnectionReestablished;
			GameManager.Get.ConnectionFailed -= HandleConnectionFailed;
		}
		#endregion

		#region Event Handlers
		private void HandleConnectionReestablished( object o, EventArgs args )
		{
			TransitionState( GameStateMachine.GAME_STATE_PLAYER_UPDATE );
		}

		private void HandleConnectionFailed( object o, EventArgs args )
		{
			Ping();
		}
		#endregion
	}
}
