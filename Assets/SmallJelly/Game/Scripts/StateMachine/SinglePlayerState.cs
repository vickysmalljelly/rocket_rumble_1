﻿using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// Shows the single player menu and holds the SinglePlayerStateMachine
    /// </summary>
	public sealed class SinglePlayerState : AbstractGameState
    {
        private SinglePlayerStateMachine mSinglePlayerStateMachine;

        public override void OnEntering()
        {
            base.OnEntering();		

            // Add the child state machine
            mSinglePlayerStateMachine = this.AddChildStateMachine<SinglePlayerStateMachine>();

            RegisterListeners();
        }

        public override void OnLeaving()
        {
            base.OnLeaving();

            UnregisterListeners();
        }

        private void RegisterListeners()
        {
            mSinglePlayerStateMachine.StateMachineCompleted += HandleStateMachineCompleted;
        }

        private void UnregisterListeners() 
        {
            mSinglePlayerStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;
        }

		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
        {
			OnStateCompleted( stateConstructionMessageData );
        }            
    }
}