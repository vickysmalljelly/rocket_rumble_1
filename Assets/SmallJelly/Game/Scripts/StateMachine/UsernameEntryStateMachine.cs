﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls entering a username after authenticating with the device id
    /// </summary>
    public sealed class UsernameEntryStateMachine : StateMachine
    {
        // Game states
        public const string USERNAME_ENTRY_TEXT_ENTRY = "TEXT_ENTRY"; 
        public const string USERNAME_ENTRY_UPDATE_PLAYER_DETAILS = "UPDATE"; 

        private UpdatePlayerDataState mUpdateState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            AddState<UsernameTextEntryState>(USERNAME_ENTRY_TEXT_ENTRY);
            mUpdateState = AddState<UpdatePlayerDataState>(USERNAME_ENTRY_UPDATE_PLAYER_DETAILS);

            // Initialise transitions
            AddTransition(USERNAME_ENTRY_TEXT_ENTRY, USERNAME_ENTRY_UPDATE_PLAYER_DETAILS);

            // Enter the first state
            StateConstructionData = new StateConstructionData( UsernameEntryStateMachine.USERNAME_ENTRY_TEXT_ENTRY );
        }

        private void OnEnable()
        {
            mUpdateState.StateCompleted += HandleUpdateCompleted;

			UIManager.Get.HideMenu< FrontEndNavigationController >();
        }

        private void OnDisable()
        {
            mUpdateState.StateCompleted -= HandleUpdateCompleted;

			UIManager.Get.ShowMenu< FrontEndNavigationController >();
        }

        void HandleUpdateCompleted(State arg1, StateConstructionData arg2)
        {
            OnStateMachineCompleted();
        }
    }
}

