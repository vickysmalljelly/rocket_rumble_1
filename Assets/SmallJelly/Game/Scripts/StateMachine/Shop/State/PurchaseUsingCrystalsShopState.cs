﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class PurchaseUsingCrystalsShopState : PurchaseShopState
	{
		#region Private Constants
		private const string MESSAGE_TITLE = "Failed Purchase";
		private const string MESSAGE_BODY = "Your purchase has failed! Please try again";
		#endregion

		#region Public Properties
		//TODO - relook at this - for now if you purchase something using crystals then it definitely will be a pack - but that may not always be the case!
		protected override string PurchaseCompleteEffectPrefabPath
		{
			get
			{
				return string.Format( "{0}/{1}", FileLocations.SmallJellyShopParticleEffectPrefabs, "pfx_awardPack");
			}
		}
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

            ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
            shopMenuController.TogglePurchaseInProgressSpinner(true);

			ShopManager.Get.BuyVirtualGoodWithCrystals( IapData );
		}

        public override void OnLeaving()
        {
            ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
            shopMenuController.TogglePurchaseInProgressSpinner(false);

            base.OnLeaving();
        }
		#endregion

		#region Event Handlers
		private void HandleClosed()
		{
			OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_BROWSING ) );
		}
		#endregion
	}
}