﻿using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	public abstract class PurchaseShopState : AbstractShopState
	{
		#region Public Properties
		protected abstract string PurchaseCompleteEffectPrefabPath
		{
			get;
		}

		protected IapData IapData { get; private set; }
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			IapData = ( ( TypedStateConstructionData< IapData > )stateConstructionData ).Value;
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Event Handlers
		public override void OnShopEventDataReceived( ShopEventData shopEventData )
		{
			base.OnShopEventDataReceived( shopEventData );

			if( shopEventData is BuyVirtualGoodSuccessEventData || shopEventData is BuyVirtualGoodFromGooglePlaySuccessEventData )
			{
				//Instantiate the prefab which displays the purchase effect - e.g. dragging a card pack to the inbox
				GameObject rewardPrefab = Instantiate( Resources.Load< GameObject >( PurchaseCompleteEffectPrefabPath ) );
				ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();

                // Set the game object from which the effect should originate
                PlayMakerSharedVariables psv = rewardPrefab.GetComponent<PlayMakerSharedVariables>();
                psv.Add("from", new FsmGameObject(ShopManager.Get.CurrentShopItem));

                if(shopEventData is BuyVirtualGoodFromGooglePlaySuccessEventData)
                {
                    BuyVirtualGoodFromGooglePlaySuccessEventData args = shopEventData as BuyVirtualGoodFromGooglePlaySuccessEventData;
                    psv.Add("numCrystals", new FsmInt((int)args.NumCrystals));
                }
                else if (shopEventData is BuyVirtualGoodSuccessEventData)
                {
                    BuyVirtualGoodSuccessEventData args = shopEventData as BuyVirtualGoodSuccessEventData;
                    psv.Add("numPacks", new FsmInt((int)args.NumPacks));
                }
   
				SJRenderTransformExtensions.AddChild( shopMenuController.transform, rewardPrefab.transform, true, true, true );

				OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_COMPLETED_PURCHASE ) );
			}
		}            
		#endregion
	}
}