﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class BrowsingShopState : AbstractShopState
	{
		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
			shopMenuController.ShopItemStateTransitioned += HandleShopItemStateTransitioned;
		}

		private void UnregisterListeners()
		{
			ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
			shopMenuController.ShopItemStateTransitioned -= HandleShopItemStateTransitioned;
		}
		#endregion

		#region Event Handlers
		private void HandleShopItemStateTransitioned( object o, ShopItemAndStateTransitionEventArgs shopItemAndStateTransitionEventArgs )
		{
			if( shopItemAndStateTransitionEventArgs.NewState == ShopItemController.State.Selected )
			{
				OnStateCompleted( new TypedStateConstructionData< ShopItem >( ShopStateMachine.SHOP_STATE_PRODUCT_INFORMATION, shopItemAndStateTransitionEventArgs.ShopItem ) );
			}
		}
		#endregion
	}
}