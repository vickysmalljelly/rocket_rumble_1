﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class DownloadingShopState : AbstractShopState
	{
		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ShopManager.Get.ListVirtualGoods( );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Event Handlers
		public override void OnShopEventDataReceived( ShopEventData shopEventData )
		{
			base.OnShopEventDataReceived( shopEventData );

			if( shopEventData is ListVirtualGoodsSuccessEventData )
			{
				ListVirtualGoodsSuccessEventData listVirtualGoodsSuccessEventData = (ListVirtualGoodsSuccessEventData) shopEventData;
				ShopMenuController shopMenuController = UIManager.Get.GetMenuController< ShopMenuController >();
				shopMenuController.OnRefreshData( listVirtualGoodsSuccessEventData.Data );

				OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_BROWSING ) );
			}
		}

		private void HandleError( ServerError serverError )
		{
			ButtonData buttonData = new ButtonData( "Ok", HandleErrorDismissed );
			DialogManager.Get.ShowMessagePopup( "Connection Problems", "Could not connect to the shop, please check your internet conection and try again", new ButtonData[]{ buttonData } );
		}

		private void HandleErrorDismissed()
		{
			OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_DOWNLOADING ) );
		}
		#endregion
	}
}