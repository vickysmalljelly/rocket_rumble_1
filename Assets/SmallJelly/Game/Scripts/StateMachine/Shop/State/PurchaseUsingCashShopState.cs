﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace SmallJelly
{
	public class PurchaseUsingCashShopState : PurchaseShopState
	{
		#region Public Properties
		//TODO - relook at this - for now if you purchase something using credits then it definitely will be a pack - but that may not always be the case!
		protected override string PurchaseCompleteEffectPrefabPath
		{
			get
			{
				return string.Format( "{0}/{1}", FileLocations.SmallJellyShopParticleEffectPrefabs, "pfx_awardCrystals");
			}
		}
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ShopManager.Get.BuyConsumable( IapData );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Event Handlers
		private void HandleOkButton()
		{
		}
		#endregion
	}
}