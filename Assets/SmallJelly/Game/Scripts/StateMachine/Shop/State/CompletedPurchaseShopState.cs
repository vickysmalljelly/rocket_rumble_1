﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class CompletedPurchaseShopState : AbstractShopState
	{
		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_BROWSING ) );
		}
		#endregion
	}
}