﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class ProductInformationShopState : AbstractShopState
	{
		#region Member Variables
		private ShopItem mShopItem;
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			TypedStateConstructionData< ShopItem > shopItemTypedStateConstructionData = ( TypedStateConstructionData< ShopItem > )stateConstructionData;
			mShopItem = shopItemTypedStateConstructionData.Value;
		}

		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			ShopProductInformation shopProductInformation = UIManager.Get.ShowMenu< ShopProductInformation >();
			shopProductInformation.OnRefreshData( mShopItem.Data );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UIManager.Get.HideMenu< ShopProductInformation >();

			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			ShopProductInformation shopProductInformation = UIManager.Get.GetMenuController< ShopProductInformation >();
			shopProductInformation.StateTransitioned += HandleStateTransitioned;
		}

		private void UnregisterListeners()
		{
			ShopProductInformation shopProductInformation = UIManager.Get.GetMenuController< ShopProductInformation >();
			shopProductInformation.StateTransitioned -= HandleStateTransitioned;
		}
		#endregion

		#region Event Handlers
		private void HandleStateTransitioned( object o, ShopProductInformationAndStateTransitionEventArgs shopProductInformationAndStateTransitionEventArgs )
		{
			IapData iapData = UIManager.Get.GetMenuController< ShopProductInformation >().Data;
			switch( shopProductInformationAndStateTransitionEventArgs.NewState )
			{
				case ShopProductInformationController.State.PurchaseUsingCredits:
					OnStateCompleted( new TypedStateConstructionData< IapData >( ShopStateMachine.SHOP_STATE_PURCHASE_USING_CREDITS, iapData ) );
					break;

				case ShopProductInformationController.State.PurchaseUsingCrystals:
					OnStateCompleted( new TypedStateConstructionData< IapData >( ShopStateMachine.SHOP_STATE_PURCHASE_USING_CRYSTALS, iapData ) );
					break;

				case ShopProductInformationController.State.PurchaseUsingCash:
					OnStateCompleted( new TypedStateConstructionData< IapData >( ShopStateMachine.SHOP_STATE_PURCHASE_USING_CASH, iapData) );
					break;

				case ShopProductInformationController.State.Back:
					OnStateCompleted( new StateConstructionData( ShopStateMachine.SHOP_STATE_BROWSING ) );
					break;
			}
		}
		#endregion
	}
}