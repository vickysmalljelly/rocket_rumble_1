﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class AbstractShopState : StateMachine.State
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}

		public virtual void OnShopEventDataReceived( ShopEventData shopEventData )
		{
            // What do to here?
		}

		public virtual void OnShopResponseFailure( string displayMessage )
		{
            ButtonData messageClose = new ButtonData( "Ok", HandleMessageClose );
            DialogManager.Get.ShowMessagePopup( "Error", displayMessage, messageClose );
		}
		#endregion

        private void HandleMessageClose()
        {
            TransitionState( ShopStateMachine.SHOP_STATE_BROWSING );
        }
	}
}