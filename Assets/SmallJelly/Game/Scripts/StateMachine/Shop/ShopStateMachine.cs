using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class ShopStateMachine : StateMachine
	{
		#region Constants
		public const string SHOP_STATE_DOWNLOADING = "SHOP_STATE_DOWNLOADING";
		public const string SHOP_STATE_BROWSING = "SHOP_STATE_BROWSING";
		public const string SHOP_STATE_PRODUCT_INFORMATION = "SHOP_STATE_PRODUCT_INFORMATION";

		public const string SHOP_STATE_PURCHASE_USING_CREDITS = "SHOP_STATE_PURCHASE_USING_CREDITS";
		public const string SHOP_STATE_PURCHASE_USING_CRYSTALS = "SHOP_STATE_PURCHASE_USING_CRYSTALS";
		public const string SHOP_STATE_PURCHASE_USING_CASH = "SHOP_STATE_PURCHASE_USING_CASH";

		public const string SHOP_STATE_COMPLETED_PURCHASE = "SHOP_STATE_COMPLETED_PURCHASE";
		#endregion

		#region Member Variables
		private DownloadingShopState mDownloadingShopState;
		private BrowsingShopState mBrowsingShopState;
		private ProductInformationShopState mProductInformationShopState;

		private PurchaseUsingCreditsShopState mPurchaseUsingCreditsShopState;
		private PurchaseUsingCrystalsShopState mPurchaseUsingCrystalsShopState;
		private PurchaseUsingCashShopState mPurchaseUsingCashShopState;

		private CompletedPurchaseShopState mCompletedPurchaseShopState;
		#endregion

		#region MonoBehaviour
		protected override void Awake()
		{
			base.Awake();

			mDownloadingShopState = AddState< DownloadingShopState >( SHOP_STATE_DOWNLOADING );
			mBrowsingShopState = AddState< BrowsingShopState >( SHOP_STATE_BROWSING );
			mProductInformationShopState = AddState< ProductInformationShopState >( SHOP_STATE_PRODUCT_INFORMATION );

			mPurchaseUsingCreditsShopState = AddState< PurchaseUsingCreditsShopState >( SHOP_STATE_PURCHASE_USING_CREDITS );
			mPurchaseUsingCrystalsShopState = AddState< PurchaseUsingCrystalsShopState >( SHOP_STATE_PURCHASE_USING_CRYSTALS );
			mPurchaseUsingCashShopState = AddState< PurchaseUsingCashShopState >( SHOP_STATE_PURCHASE_USING_CASH );

			mCompletedPurchaseShopState = AddState< CompletedPurchaseShopState >( SHOP_STATE_COMPLETED_PURCHASE );

			AddTransition( SHOP_STATE_DOWNLOADING, SHOP_STATE_BROWSING );
			AddTransition( SHOP_STATE_BROWSING, SHOP_STATE_PRODUCT_INFORMATION );
			AddTransition( SHOP_STATE_PRODUCT_INFORMATION, SHOP_STATE_BROWSING );

			AddTransition( SHOP_STATE_PRODUCT_INFORMATION, SHOP_STATE_PURCHASE_USING_CREDITS );
			AddTransition( SHOP_STATE_PRODUCT_INFORMATION, SHOP_STATE_PURCHASE_USING_CRYSTALS );
			AddTransition( SHOP_STATE_PRODUCT_INFORMATION, SHOP_STATE_PURCHASE_USING_CASH );

			AddTransition( SHOP_STATE_PURCHASE_USING_CREDITS, SHOP_STATE_COMPLETED_PURCHASE );
			AddTransition( SHOP_STATE_PURCHASE_USING_CRYSTALS, SHOP_STATE_COMPLETED_PURCHASE );
			AddTransition( SHOP_STATE_PURCHASE_USING_CASH, SHOP_STATE_COMPLETED_PURCHASE );

			AddTransition( SHOP_STATE_PURCHASE_USING_CREDITS, SHOP_STATE_BROWSING );
			AddTransition( SHOP_STATE_PURCHASE_USING_CRYSTALS, SHOP_STATE_BROWSING );
			AddTransition( SHOP_STATE_PURCHASE_USING_CASH, SHOP_STATE_BROWSING );

			AddTransition( SHOP_STATE_COMPLETED_PURCHASE, SHOP_STATE_BROWSING );

			//By default let's move to the downloading shop state
			HandleStateCompleted( default( State ), new StateConstructionData( mDownloadingShopState.Id ) );

			RegisterListeners();
        }

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mDownloadingShopState.StateCompleted += HandleStateCompleted;
			mBrowsingShopState.StateCompleted += HandleStateCompleted;
			mProductInformationShopState.StateCompleted += HandleStateCompleted;

			mPurchaseUsingCreditsShopState.StateCompleted += HandleStateCompleted;
			mPurchaseUsingCrystalsShopState.StateCompleted += HandleStateCompleted;
			mPurchaseUsingCashShopState.StateCompleted += HandleStateCompleted;

			mCompletedPurchaseShopState.StateCompleted += HandleStateCompleted;

			ShopManager.Get.ShopEventDataReceived += HandleShopEventDataReceived;
			ShopManager.Get.ShopResponseFailure += HandleShopResponseFailure;
		}

		private void UnregisterListeners()
		{
			mDownloadingShopState.StateCompleted -= HandleStateCompleted;
			mBrowsingShopState.StateCompleted -= HandleStateCompleted;
			mProductInformationShopState.StateCompleted -= HandleStateCompleted;

			mPurchaseUsingCreditsShopState.StateCompleted -= HandleStateCompleted;
			mPurchaseUsingCrystalsShopState.StateCompleted -= HandleStateCompleted;
			mPurchaseUsingCashShopState.StateCompleted -= HandleStateCompleted;

			mCompletedPurchaseShopState.StateCompleted -= HandleStateCompleted;

			if( ShopManager.Get != null )
			{
				ShopManager.Get.ShopEventDataReceived -= HandleShopEventDataReceived;
				ShopManager.Get.ShopResponseFailure -= HandleShopResponseFailure;
			}
		}
		#endregion

		#region Event Handlers
        protected virtual void HandleStateCompleted( State state, StateConstructionData stateConstructionMessageData )
		{
			StateConstructionData = stateConstructionMessageData;
		}

		private void HandleShopEventDataReceived( object o, ShopEventDataEventArgs shopEventDataEventArgs )
		{
			( ( AbstractShopState )CurrentState ).OnShopEventDataReceived( shopEventDataEventArgs.ShopEventData );
		}

		private void HandleShopResponseFailure( object o, StringEventArgs stringEventArgs )
		{
			( ( AbstractShopState )CurrentState ).OnShopResponseFailure( stringEventArgs.String );
		}
		#endregion
    }
}