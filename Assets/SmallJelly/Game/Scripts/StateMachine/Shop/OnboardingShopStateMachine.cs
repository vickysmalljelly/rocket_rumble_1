using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class OnboardingShopStateMachine : ShopStateMachine
	{
		#region Member Variables
		private bool mCompletedPurchase;
		#endregion

		#region MonoBehaviour
		private void OnEnable()
		{
			mCompletedPurchase = false;
        }

		private void OnDisable()
		{
			//Cleanup onboarding when we exit this state
			if( OnboardingManager.Get.OnboardingIsActive() )
			{
				OnboardingManager.Get.StopOnboardingSection();
			}
		}
		#endregion

		#region Event Handlers
		protected override void HandleStateCompleted( State state, StateConstructionData stateConstructionData )
		{
			base.HandleStateCompleted( state, stateConstructionData );

			if( OnboardingManager.Get.OnboardingIsActive() )
			{
				OnboardingManager.Get.StopOnboardingSection();
			}

			if( !mCompletedPurchase )
			{
				
				switch( stateConstructionData.StateId ) 
				{
					case ShopStateMachine.SHOP_STATE_DOWNLOADING:
						OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.BuyPackStepOne );
						break;

					case ShopStateMachine.SHOP_STATE_BROWSING:
						OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.BuyPackStepTwo );
						break;

					case ShopStateMachine.SHOP_STATE_PRODUCT_INFORMATION:
						OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.BuyPackStepThree );
						break;

					case ShopStateMachine.SHOP_STATE_COMPLETED_PURCHASE:
						GameManager.Get.PlayerData.SetCheckpointOnClient( OnboardingManager.StandardCardPackBoughtCheckpoint );
						mCompletedPurchase = true;
						break;
				}
			}
			else
			{
				switch( stateConstructionData.StateId ) 
				{
					case ShopStateMachine.SHOP_STATE_BROWSING:
					OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.DirectUserFromShopScreenToInbox );
						break;
				}
			}
		}
		#endregion
    }
}