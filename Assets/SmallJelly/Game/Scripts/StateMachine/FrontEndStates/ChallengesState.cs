﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ChallengesState : StateMachine.State 
    {
        ChallengesMenuController mController;

        public override void OnEntering()
        {
            base.OnEntering();

            mController = UIManager.Get.GetMenuController<ChallengesMenuController>();

            mController.BackButtonClicked += HandleBackButtonClicked;
        }            

        public override void OnLeaving()
        {
            mController.BackButtonClicked -= HandleBackButtonClicked;

            base.OnLeaving();
        }

        private void HandleBackButtonClicked()
        {
            // Notify parent that we are done
            OnStateCompleted();
        }
    }
}
