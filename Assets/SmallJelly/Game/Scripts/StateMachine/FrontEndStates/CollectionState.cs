using SmallJelly.Framework;
using UnityEngine;
using System;

namespace SmallJelly
{
	/// <summary>
	/// State for viewing a players collection, decks and creating new decks
	/// </summary>
	public sealed class CollectionState : AbstractFrontEndState< CollectionMenuController >
	{	
		#region Protected Properties
		protected override string FrontEndPath 
		{
			get
			{
				return FileLocations.CollectionsPrefab;
			}
		}
		#endregion

		#region Member Variables
		private CollectionStateMachine mCollectionStateMachine;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			GameManager.Get.AddCollectionManager();

			mCollectionStateMachine = this.AddChildStateMachine< CollectionStateMachine >();

			FrontEndNavigationController.CollectionButton.interactable = false;
            // Hide Collection red dot while viewing the collection
            FrontEndNavigationController.CollectionRedDot.SetActive(false);

			RegisterListeners ();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners ();

			FrontEndNavigationController.CollectionButton.interactable = true;
            // Re-enable the collection red dot now we are leaving the collection
            FrontEndNavigationController.CollectionRedDot.SetActive(true);

			GameManager.Get.RemoveCollectionManager();
        }
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mCollectionStateMachine.ReadyToTransitionToRumble += HandleReadyToTransitionToRumble;
			mCollectionStateMachine.ReadyToTransitionToShop += HandleReadyToTransitionToShop;
			mCollectionStateMachine.ReadyToTransitionToInbox += HandleReadyToTransitionToInbox;
		}

		private void UnregisterListeners()
		{
			mCollectionStateMachine.ReadyToTransitionToRumble -= HandleReadyToTransitionToRumble;
			mCollectionStateMachine.ReadyToTransitionToShop -= HandleReadyToTransitionToShop;
			mCollectionStateMachine.ReadyToTransitionToInbox -= HandleReadyToTransitionToInbox;
		}
		#endregion
    }
}