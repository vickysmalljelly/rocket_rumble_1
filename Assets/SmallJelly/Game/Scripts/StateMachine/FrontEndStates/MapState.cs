﻿using System;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	public sealed class MapState : AbstractGameState
	{
		#region Member Variables
		private MapManager mMapManager;
		private MapOnboardingManager mMapOnboardingManager;
		#endregion

		#region State Machine Overrides
		public override void OnEntering()
		{	
			base.OnEntering();

			//TODO - Take this out! It's just temporarily in until we get the checkpoint correctly set from the server
			OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.FirstRumbleCheckpoint );
			OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.RumbleIntroCheckpoint );

			mMapManager = CreateMapManager();
			mMapOnboardingManager = CreateMapOnboardingManager();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
	
			UnregisterListeners();

			Destroy( mMapManager.gameObject );
			Destroy( mMapOnboardingManager.gameObject );
		}
		#endregion

		#region Private Methods
		private MapManager CreateMapManager()
		{
			MapManager mapManagerResource = Resources.Load< MapManager >( FileLocations.MapPrefab );
			MapManager mapManager = Instantiate( mapManagerResource );
			mapManager.transform.parent = transform;
			mapManager.transform.localPosition = Vector3.zero;
			mapManager.transform.localEulerAngles = Vector3.zero;

			return mapManager;
		}

		private MapOnboardingManager CreateMapOnboardingManager()
		{
			MapOnboardingManager mapOnboardingResource = Resources.Load< MapOnboardingManager >( FileLocations.MapOnboardingPrefab );
			MapOnboardingManager mapOnboardingManager = Instantiate( mapOnboardingResource );
			mapOnboardingManager.transform.parent = transform;
			mapOnboardingManager.transform.localPosition = Vector3.zero;
			mapOnboardingManager.transform.localEulerAngles = Vector3.zero;

			return mapOnboardingManager;
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mMapManager.MapMenuController.Back += HandleBack;
			mMapOnboardingManager.RunBattleWithId += HandleRunBattleWithId;
			mMapOnboardingManager.ExitToFrontEnd += HandleExitToFrontEnd;
		}

		private void UnregisterListeners()
		{
			mMapManager.MapMenuController.Back -= HandleBack;
			mMapOnboardingManager.RunBattleWithId -= HandleRunBattleWithId;
			mMapOnboardingManager.ExitToFrontEnd -= HandleExitToFrontEnd;
		}
		#endregion

		#region Event Handlers
		private void HandleBack( object o, EventArgs args )
		{
			TransitionState( GameStateMachine.GAME_STATE_FRONT_END );
		}

		private void HandleRunBattleWithId( BattleId battleId )
		{
            BattleModeManager.Get.SetBattleConfig(BattleMode.SinglePlayer, battleId, MatchmakingMode.NotSet);

			TransitionState( GameStateMachine.GAME_STATE_SINGLE_PLAYER_MENU );
		}

		private void HandleExitToFrontEnd()
		{
			TransitionState( GameStateMachine.GAME_STATE_FRONT_END );
		}
		#endregion
	}
}
