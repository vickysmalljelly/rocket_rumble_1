﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class ShopState : AbstractFrontEndState< ShopMenuController >
	{
		#region Protected Properties
		protected override string FrontEndPath 
		{
			get
			{
				return FileLocations.ShopPrefab;
			}
		}
		#endregion


		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			GameManager.Get.AddShopManager();

			if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.StandardCardPackBoughtCheckpoint ) )
			{
				this.AddChildStateMachine< OnboardingShopStateMachine >();
			}
			else
			{
				this.AddChildStateMachine< ShopStateMachine >();
			}
				
			UIManager.Get.AddMenu< ShopProductInformation >( FileLocations.ShopProductInformationPrefab, UIManager.Get.transform );

			FrontEndNavigationController.ShopButton.interactable = false;
            // Hide the shop red dot while we are viewing the shop
            FrontEndNavigationController.ShopRedDot.SetActive(false);

            FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();
            frontEndNavigationController.ToggleCrystalsBuyButton(false);

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

            FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();
            frontEndNavigationController.ToggleCrystalsBuyButton(true);

			FrontEndNavigationController.ShopButton.interactable = true;
            // Re-enable the shop red dot now we are leaving the shop
            FrontEndNavigationController.ShopRedDot.SetActive(true);

			UIManager.Get.RemoveMenu< ShopProductInformation >();

			GameManager.Get.RemoveShopManager();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			FrontEndNavigationController.RumbleClicked += HandleReadyToTransitionToRumble;
			FrontEndNavigationController.CollectionClicked += HandleReadyToTransitionToCollection;
			FrontEndNavigationController.InboxClicked += HandleReadyToTransitionToInbox;
		}

		private void UnregisterListeners()
		{
			FrontEndNavigationController.RumbleClicked -= HandleReadyToTransitionToRumble;
			FrontEndNavigationController.CollectionClicked -= HandleReadyToTransitionToCollection;
			FrontEndNavigationController.InboxClicked -= HandleReadyToTransitionToInbox;
		}
		#endregion
	}
}