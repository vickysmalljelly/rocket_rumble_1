﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Controls the state of the front end.
	/// </summary>
	public class FrontEndStateMachine : StateMachine
	{
		#region Event Handlers
		public event EventHandler MultiplayerClicked
		{
			add
			{
				mPlayState.MultiplayerClicked += value;
			}

			remove
			{
				mPlayState.MultiplayerClicked -= value;
			}
		}

        public event Action<BattleId> SinglePlayerClicked
		{
			add
			{
				mPlayState.SinglePlayerClicked += value;
			}

			remove
			{
				mPlayState.SinglePlayerClicked -= value;
			}
		}

		public event EventHandler MapModeClicked
		{
			add
			{
				mPlayState.MapModeClicked += value;
			}

			remove
			{
				mPlayState.MapModeClicked -= value;
			}
		}

		public event EventHandler LogoutClicked
		{
			add
			{
				mPlayState.LogoutClicked += value;
			}

			remove
			{
				mPlayState.LogoutClicked -= value;
			}
		}

		public event EventHandler RumbleClicked
		{
			add
			{
				mPlayState.RumbleClicked += value;
			}

			remove
			{
				mPlayState.RumbleClicked -= value;
			}
		}
		#endregion

		#region Constants
		// Game states
		public const string PLAY_STATE = "PLAY_STATE"; 
		public const string USERNAME_ENTRY_STATE = "USERNAME_ENTRY_STATE"; 
		public const string COLLECTION_STATE = "COLLECTION_STATE"; 
		public const string INBOX_STATE = "INBOX_STATE"; 
		public const string SHOP_STATE = "SHOP_STATE"; 
		#endregion

		#region Member Variables
		private PlayState mPlayState;
		private UsernameEntryState mUsernameEntryState;
		private CollectionState mCollectionState;
		private InboxState mInboxState;
		private ShopState mShopState;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			// Initialise states
			mPlayState = AddState< PlayState >( PLAY_STATE );
			mUsernameEntryState = AddState< UsernameEntryState >( USERNAME_ENTRY_STATE );
			mCollectionState = AddState< CollectionState >( COLLECTION_STATE );
			mInboxState = AddState< InboxState >( INBOX_STATE );
			mShopState = AddState< ShopState >( SHOP_STATE );

			AddTransition( PLAY_STATE, USERNAME_ENTRY_STATE );
			AddTransition( USERNAME_ENTRY_STATE, PLAY_STATE );
			AddTransition( PLAY_STATE, COLLECTION_STATE );
			AddTransition( PLAY_STATE, INBOX_STATE );
			AddTransition( PLAY_STATE, SHOP_STATE );

			AddTransition( COLLECTION_STATE, PLAY_STATE );
			AddTransition( COLLECTION_STATE, INBOX_STATE );
			AddTransition( COLLECTION_STATE, SHOP_STATE );

			AddTransition( INBOX_STATE, PLAY_STATE );
			AddTransition( INBOX_STATE, COLLECTION_STATE );
			AddTransition( INBOX_STATE, SHOP_STATE );

			AddTransition( SHOP_STATE, PLAY_STATE );
			AddTransition( SHOP_STATE, COLLECTION_STATE );
			AddTransition( SHOP_STATE, INBOX_STATE );

			RegisterListeners();

			//Enter the first state
			OnEnterPlayState();
			LateUpdate();
		}

		private void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void OnEnterPlayState()
		{
			StateConstructionData = new StateConstructionData( FrontEndStateMachine.PLAY_STATE );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mPlayState.StateCompleted += HandleStateCompleted;
			mUsernameEntryState.StateCompleted += HandleStateCompleted;
			mCollectionState.StateCompleted += HandleStateCompleted;
			mInboxState.StateCompleted += HandleStateCompleted;
			mShopState.StateCompleted += HandleStateCompleted;
		}

		private void UnregisterListeners()
		{
			mPlayState.StateCompleted -= HandleStateCompleted;
			mUsernameEntryState.StateCompleted -= HandleStateCompleted;
			mCollectionState.StateCompleted -= HandleStateCompleted;
			mInboxState.StateCompleted -= HandleStateCompleted;
			mShopState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handlers
		private void HandleStateCompleted( State state, StateConstructionData stateConstructionData )
		{
			StateConstructionData = (StateConstructionData)stateConstructionData;
		}

		private void HandleLogoutClicked( object o, EventArgs args )
		{
		}

		private void HandleMapModeClicked( object o, EventArgs args )
		{
		}

		private void HandleMultiplayerClicked( object o, EventArgs args )
		{
		}

		private void HandleReplayClicked( object o, EventArgs args )
		{
		}

		private void HandleRumbleClicked( object o, EventArgs args )
		{
		}

		private void HandleSinglePlayerClicked( object o, EventArgs args )
		{
		}

		#endregion
	}
}