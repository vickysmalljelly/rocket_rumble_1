﻿using System;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{

	/// <summary>
	/// State when the RR Home button is selected.
	/// </summary>
	public sealed class PlayState : AbstractFrontEndState< PlayMenuController >
	{
		#region Protected Properties
		protected override string FrontEndPath 
		{
			get
			{
				return FileLocations.PlayMenuPrefab;
			}
		}
		#endregion

		#region Event Handlers
		public event EventHandler MultiplayerClicked;
        public event Action<BattleId> SinglePlayerClicked;
		public event EventHandler MapModeClicked;
		public event EventHandler LogoutClicked;
		public event EventHandler RumbleClicked;
		#endregion

		#region Member Variables
		private GameObject mCurrentTutorialPrefab;
		#endregion

		#region State Machine Overrides
		public override void OnEntering()
		{	
			base.OnEntering();

			RegisterNavigationListeners();
            RegisterRumbleListeners();

            // Always listen to the logout button
            MenuController.LogoutClicked += HandleLogoutClicked;

            FrontEndNavigationController.RumbleButton.interactable = false;

            // Wnat to wait until the player has finished the onboarding before enabling
            if(OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.FinishOnboardingCheckpoint ))
            {
                KTPlayManager.Get.Refresh();
            }

			MenuController.ShowLogo();
			MenuController.ShowNavigationButtons();

			//If we havent completed the first tutorial then run the intro sequence
			if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial1CheckPoint ) )
			{
                // Record the start of the onboarding
                AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Onboarding, null, null);

				OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.StartScreen );
				UIManager.Get.HideMenu< FrontEndNavigationController >();
			}
			//If we havent completed the tutorial then run that
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.Tutorial4CheckPoint ) )
			{
				UIManager.Get.HideMenu< FrontEndNavigationController >();

				OnStateCompleted( new StateConstructionData( GameStateMachine.GAME_STATE_MAP ) );
			}
			//If we havent completed the username entry then move to that state
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.UsernameCheckPoint ) )
			{
				MenuController.HideLogo();
				MenuController.HideNavigationButtons();

				OnStateCompleted( new StateConstructionData( FrontEndStateMachine.USERNAME_ENTRY_STATE ) );
			}
			//Then run the pack opening
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.StandardCardPackBoughtCheckpoint ) )
			{
				OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.DirectUserFromPlayScreenToShop );
			}
			//Then run the pack opening
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.PackOpenedCheckpoint ) && !GameManager.Get.DebugConfig.DisableInboxOnboarding )
			{
				OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.DirectUserFromPlayScreenToInbox );
			}
			else if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.FinishOnboardingCheckpoint ) )
			{
				OnboardingManager.Get.SetCheckpointOnServer( OnboardingManager.FinishOnboardingCheckpoint );
				OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.FinishOnboardingCheckpoint );

				OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.FinishOnboarding );

                // Note that we have finished the onboarding
                AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Onboarding, null, null);
			}
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
	
			UnregisterNavigationListeners();
            UnregisterRumbleListeners();

			FrontEndNavigationController.RumbleButton.interactable = true;

			if( OnboardingManager.Get.OnboardingIsActive() )
			{
				OnboardingManager.Get.StopOnboardingSection();
			}

            // If any of the submenus are open, remove them
            if(this.HasChildStateMachine())
            {
                RemoveChildStateMachine();
            }
		}
		#endregion

		#region Event Registration
		private void RegisterNavigationListeners()
        {
			FrontEndNavigationController.CollectionClicked += HandleReadyToTransitionToCollection;
			FrontEndNavigationController.ShopClicked += HandleReadyToTransitionToShop;
			FrontEndNavigationController.InboxClicked += HandleReadyToTransitionToInbox;
		}

        private void RegisterRumbleListeners()
        {
            MenuController.MultiplayerClicked += HandleMultiplayerClicked;
            MenuController.SinglePlayerClicked += HandleSinglePlayerClicked;
            MenuController.MapModeClicked += HandleMapModeClicked;
            MenuController.LogoutClicked += HandleLogoutClicked;
            MenuController.RumbleClicked += HandleRumbleClicked;
            MenuController.ChallengesClicked += HandleChallengesClicked;
            MenuController.LootRunsClicked += HandleLootRunsClicked;
            MenuController.LeaderboardsClicked += HandleLeaderboardsClicked;
        } 

		private void UnregisterNavigationListeners()
		{
			FrontEndNavigationController.CollectionClicked -= HandleReadyToTransitionToCollection;
			FrontEndNavigationController.ShopClicked -= HandleReadyToTransitionToShop;
			FrontEndNavigationController.InboxClicked -= HandleReadyToTransitionToInbox;
		}

        private void UnregisterRumbleListeners()
        {
            if(MenuController != null)
            {
                MenuController.MultiplayerClicked -= HandleMultiplayerClicked;
                MenuController.SinglePlayerClicked -= HandleSinglePlayerClicked;
                MenuController.MapModeClicked -= HandleMapModeClicked;
                MenuController.LogoutClicked -= HandleLogoutClicked;
                MenuController.RumbleClicked -= HandleRumbleClicked;
                MenuController.ChallengesClicked -= HandleChallengesClicked;
                MenuController.LootRunsClicked -= HandleLootRunsClicked;
                MenuController.LeaderboardsClicked -= HandleLeaderboardsClicked;
            }
        }
		#endregion

		#region Event Firing
		private void FireMultiplayerClicked()
		{
			if( MultiplayerClicked != null )
			{
				MultiplayerClicked( this, EventArgs.Empty );
			}
		}

        private void FireSinglePlayerClicked(BattleId battleId)
		{
			if( SinglePlayerClicked != null )
			{
				SinglePlayerClicked(battleId);
			}
		}

		private void FireMapModeClicked()
		{
			if( MapModeClicked != null )
			{
				MapModeClicked( this, EventArgs.Empty );
			}
		}

		private void FireLogoutClicked()
		{
			if( LogoutClicked != null )
			{
				LogoutClicked( this, EventArgs.Empty );
			}
		}

		private void FireRumbleClicked()
		{
			if( RumbleClicked != null )
			{
				RumbleClicked( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleAccoutDetailsError(ServerError error)
		{
            // TODO: Need to handle this properly or there is a chance of the game hanging
			SJLogger.LogError("Failed to retrieve account details: {0}", error.TechnicalDetails);
		}

		private void HandleMultiplayerClicked( object o, EventArgs args )
		{
			FireMultiplayerClicked();
		}

        private void HandleSinglePlayerClicked(BattleId battleId)
		{
            FireSinglePlayerClicked(battleId);
		}

		private void HandleMapModeClicked( object o, EventArgs args )
		{
			FireMapModeClicked();
		}           

		private void HandleLogoutClicked( object o, EventArgs args )
		{
			FireLogoutClicked();
		}

		private void HandleRumbleClicked( object o, EventArgs args )
		{
			FireRumbleClicked();
		}

        private void HandleChallengesClicked()
        {
            UnregisterRumbleListeners();
                
            // Remove stuff on RR Home menu
            RemoveFrontEndMenuController();

            // Create a state machine to handle the challenges screen
            ChallengesStateMachine stateMachine = this.AddChildStateMachine<ChallengesStateMachine>();

            // Listen for when the StateMachine has completed
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
        }

        private void HandleLootRunsClicked()
        {
            UnregisterRumbleListeners();

            // Remove stuff on RR Home menu
            RemoveFrontEndMenuController();

            // Create a state machine to handle the challenges screen
            LootRunsStateMachine stateMachine = this.AddChildStateMachine<LootRunsStateMachine>();

            // Listen for when the StateMachine has completed
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
        }    
            
        private void HandleLeaderboardsClicked()
        {
            UnregisterRumbleListeners();

            // Remove stuff on RR Home menu
            RemoveFrontEndMenuController();

            // Create a state machine to handle the challenges screen
            LeaderboardsStateMachine stateMachine = this.AddChildStateMachine<LeaderboardsStateMachine>();

            // Listen for when the StateMachine has completed
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
        }  
		#endregion

        private void HandleStateMachineCompleted(StateMachine stateMachine, StateConstructionData stateConstructionData)
        {
            // Add the RR Home menu again
            PlayMenuController controller = AddFrontEndMenuController();
            controller.ShowNavigationButtons();

            RegisterRumbleListeners();

            RemoveChildStateMachine();
        }
	}
}
