﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class AbstractFrontEndState< T > : StateMachine.State where T : FrontEndMenuController 
	{
		#region Protected Properties
		protected T MenuController
		{
			get
			{
				return mT;
			}
		}

		protected FrontEndNavigationController FrontEndNavigationController
		{
			get
			{
				return mFrontEndNavigationController;
			}
		}

		protected abstract string FrontEndPath { get; }
		#endregion

		#region Member Variables
		private T mT;
		private FrontEndNavigationController mFrontEndNavigationController;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			AddFrontEndMenuController();

            mFrontEndNavigationController = UIManager.Get.GetMenuController<FrontEndNavigationController>();

			FrontEndNavigationController.RumbleButton.interactable = true;
			FrontEndNavigationController.CollectionButton.interactable = true;
			FrontEndNavigationController.ShopButton.interactable = true;
			FrontEndNavigationController.InboxButton.interactable = true;
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			FrontEndNavigationController.RumbleButton.interactable = false;
			FrontEndNavigationController.CollectionButton.interactable = false;
			FrontEndNavigationController.ShopButton.interactable = false;
			FrontEndNavigationController.InboxButton.interactable = false;

			RemoveFrontEndMenuController();
		}
		#endregion

		#region Private Methods
        protected T AddFrontEndMenuController()
		{
			UIManager.Get.AddMenu< T >( FrontEndPath, UIManager.Get.transform );
            mT = UIManager.Get.ShowMenu< T >();
            mT.OnInsertState( FrontEndMenuController.State.MovingIn );
            return mT;
		}

		protected void RemoveFrontEndMenuController()
		{
            if(mT != null)
            {
    			UIManager.Get.HideMenu< T >();
    			UIManager.Get.RemoveMenu< T >();
    			Destroy( mT.gameObject );
                mT = null;
            }
		}
		#endregion

		#region Event Handlers
		protected virtual void HandleReadyToTransitionToRumble( object o, EventArgs args )
		{
            SJLogger.LogMessage(MessageFilter.UI, "start transition");
			OnStateCompleted( new StateConstructionData( FrontEndStateMachine.PLAY_STATE ) );
		}

		protected virtual void HandleReadyToTransitionToCollection( object o, EventArgs args )
		{
            SJLogger.LogMessage(MessageFilter.UI, "start transition");
			OnStateCompleted( new StateConstructionData( FrontEndStateMachine.COLLECTION_STATE ) );
		}

		protected virtual void HandleReadyToTransitionToShop( object o, EventArgs args )
		{
            SJLogger.LogMessage(MessageFilter.UI, "start transition");
			OnStateCompleted( new StateConstructionData( FrontEndStateMachine.SHOP_STATE ) );
		}

		protected virtual void HandleReadyToTransitionToInbox( object o, EventArgs args )
		{
            SJLogger.LogMessage(MessageFilter.UI, "start transition");
			OnStateCompleted( new StateConstructionData( FrontEndStateMachine.INBOX_STATE ) );
		}
		#endregion
	}
}