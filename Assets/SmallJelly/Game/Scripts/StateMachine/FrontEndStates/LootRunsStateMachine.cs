﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// State machine for the challenges screen
    /// </summary>
    public sealed class LootRunsStateMachine : StateMachine
    {
        // Game states
        public const string LOOT_RUNS = "LOOT_RUNS"; 

        private LootRunsState mState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mState = AddState<LootRunsState>(LOOT_RUNS);


            // Initialise transitions
            // n/a
        }

        private void OnEnable()
        {
            UIManager.Get.AddMenu<LootRunsMenuController>(FileLocations.LootRunsMenuPrefab, UIManager.Get.transform);
            UIManager.Get.ShowMenu<LootRunsMenuController>();

            mState.StateCompleted += HandleStateCompleted;
            SetCurrentState(mState, null);
        }

        private void OnDisable()
        {
            mState.StateCompleted -= HandleStateCompleted;

            UIManager.Get.HideMenu<LootRunsMenuController>();
            UIManager.Get.RemoveMenu<LootRunsMenuController>();
        }

        private void HandleStateCompleted(State arg1, StateConstructionData arg2)
        {
            // Notify parent that we are done
            OnStateMachineCompleted();
        }
    }
}
