﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LeaderboardsState : StateMachine.State 
    {
        LeaderboardsMenuController mController;

        public override void OnEntering()
        {
            base.OnEntering();

            mController = UIManager.Get.GetMenuController<LeaderboardsMenuController>();

            mController.BackButtonClicked += HandleBackButtonClicked;
        }            

        public override void OnLeaving()
        {
            mController.BackButtonClicked -= HandleBackButtonClicked;

            base.OnLeaving();
        }

        private void HandleBackButtonClicked()
        {
            // Notify parent that we are done
            OnStateCompleted();
        }
    }
}
