﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// State machine for the challenges screen
    /// </summary>
    public sealed class ChallengesStateMachine : StateMachine
    {
        // Game states
        public const string CHALLENGES_COMING_SOON = "CHALLENGES_COMING_SOON"; 

        private ChallengesState mState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mState = AddState<ChallengesState>(CHALLENGES_COMING_SOON);


            // Initialise transitions
            // n/a
        }
            
        private void OnEnable()
        {
            UIManager.Get.AddMenu<ChallengesMenuController>(FileLocations.ChallengesMenuPrefab, UIManager.Get.transform);
            UIManager.Get.ShowMenu<ChallengesMenuController>();

            mState.StateCompleted += HandleStateCompleted;
            SetCurrentState(mState, null);
        }

        private void OnDisable()
        {
            mState.StateCompleted -= HandleStateCompleted;

            UIManager.Get.HideMenu<ChallengesMenuController>();
            UIManager.Get.RemoveMenu<ChallengesMenuController>();
        }

        private void HandleStateCompleted(State arg1, StateConstructionData arg2)
        {
            // Notify parent that we are done
            OnStateMachineCompleted();
        }
    }
}
