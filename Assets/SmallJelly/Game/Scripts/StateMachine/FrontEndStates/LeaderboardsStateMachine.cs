﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// State machine for the challenges screen
    /// </summary>
    public sealed class LeaderboardsStateMachine : StateMachine
    {
        // Game states
        public const string LEADERBOARDS = "LEADERBOARDS"; 

        private LeaderboardsState mState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            mState = AddState<LeaderboardsState>(LEADERBOARDS);


            // Initialise transitions
            // n/a
        }

        private void OnEnable()
        {
            UIManager.Get.AddMenu<LeaderboardsMenuController>(FileLocations.LeaderboardsMenuPrefab, UIManager.Get.transform);
            UIManager.Get.ShowMenu<LeaderboardsMenuController>();

            mState.StateCompleted += HandleStateCompleted;
            SetCurrentState(mState, null);
        }

        private void OnDisable()
        {
            mState.StateCompleted -= HandleStateCompleted;

            UIManager.Get.HideMenu<LeaderboardsMenuController>();
            UIManager.Get.RemoveMenu<LeaderboardsMenuController>();
        }

        private void HandleStateCompleted(State arg1, StateConstructionData arg2)
        {
            // Notify parent that we are done
            OnStateMachineCompleted();
        }
    }
}

