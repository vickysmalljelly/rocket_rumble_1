﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class InboxState : AbstractFrontEndState< InboxMenuController >
	{
		#region Protected Properties
		protected override string FrontEndPath 
		{
			get
			{
				return FileLocations.InboxPrefab;
			}
		}
		#endregion

		#region Member Variables
		private InboxStateMachine mInboxStateMachine;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			GameManager.Get.AddInboxManager();

			if( !OnboardingManager.Get.HasPassedCheckpoint( OnboardingManager.PackOpenedCheckpoint ) && !GameManager.Get.DebugConfig.DisableInboxOnboarding )
			{
				mInboxStateMachine = this.AddChildStateMachine< OnboardingInboxStateMachine >();
			}
			else
			{
				mInboxStateMachine = this.AddChildStateMachine< InboxStateMachine >();
			}

			FrontEndNavigationController.InboxButton.interactable = false;
            // Hide inbox red dot while viewing the inbox
            FrontEndNavigationController.InboxRedDot.SetActive(false);

			RegisterListeners();
		}

		public override void OnLeaving()
		{
            UnregisterListeners();

			if( OnboardingManager.Get.OnboardingIsActive() )
			{
				OnboardingManager.Get.StopOnboardingSection();
			}
                
            // Re-enable the inbox red dot now we are leaving the inbox
            FrontEndNavigationController.InboxRedDot.SetActive(true);
			FrontEndNavigationController.InboxButton.interactable = true;
            			
            base.OnLeaving();

            // Only remove the inbox manager once all sub-states are disposed
            GameManager.Get.RemoveInboxManager();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInboxStateMachine.ReadyToTransitionToRumble += HandleReadyToTransitionToRumble;
			mInboxStateMachine.ReadyToTransitionToShop += HandleReadyToTransitionToShop;
			mInboxStateMachine.ReadyToTransitionToCollection += HandleReadyToTransitionToCollection;
		}

		private void UnregisterListeners()
		{
			mInboxStateMachine.ReadyToTransitionToRumble -= HandleReadyToTransitionToRumble;
			mInboxStateMachine.ReadyToTransitionToShop -= HandleReadyToTransitionToShop;
			mInboxStateMachine.ReadyToTransitionToCollection -= HandleReadyToTransitionToCollection;
		}
		#endregion
	}
}