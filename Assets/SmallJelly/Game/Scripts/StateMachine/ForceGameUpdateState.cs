﻿using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
    /// <summary>
    /// Displays a popup asking the player to update the app.  There is intentionally no exit from this state.
    /// </summary>
    public class ForceGameUpdateState : AbstractGameState
    {
        private const string mButtonText = "Ok";
        private const string mPopupTitle = "Please Update";
        private const string mPopupMessage = "We've been busy scouring the galaxy for updates and bug fixes. Please download the latest version of Rocket Rumble.";

        private void Update()
        {
            if(!DialogManager.Get.IsMessagePopupActive())
            {
                ShowPopup();
            }
        }

        private void ShowPopup()
        {
            DialogManager.Get.ShowMessagePopup(mPopupTitle, mPopupMessage, new ButtonData(mButtonText, HandleVisitStore)); 
        }

        private void HandleVisitStore()
        {
            string url = "https://play.google.com/store/apps/details?id=com.smalljelly.rocketrumble&ah=hgZwZFevpVWtxzFBbLNie-yiCY0";
            Application.OpenURL(url);
        }          
    }
}

