﻿using SmallJelly.Framework;
using UnityEngine;
using System.Collections;

namespace SmallJelly
{
    /// <summary>
    /// Shows the authentication menu which has options to login or register
    /// </summary>
    public sealed class DeviceAuthState : AbstractGameState
    {       
        public override void OnEntering()
        {
            base.OnEntering();

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Start, ProgressArea.Authentication, null, null);
                     
            Authenticate();
        }
            
        public override void OnLeaving()
        {
            base.OnLeaving();

            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Complete, ProgressArea.Authentication, null, null);
        }

        private void Authenticate()
        {
            ClientAuthentication.DeviceAuthentication("deviceId", HandleAuthenicationCompleted, HandleAuthenticationError);
        }    

        private void HandleAuthenicationCompleted()
        {
			SJLogger.AssertCondition( SJGameSparksManager.Get.PlayerAuthenticated == true, "Player Authenticated is False, it MUST be true!" );
            TransitionState( GameStateMachine.GAME_STATE_PLAYER_UPDATE, new StateConstructionData( GameStateMachine.GAME_STATE_ONBOARDING ) );
        }           

        private void HandleAuthenticationError(ServerError error)
        {
            AnalyticsManager.Get.RecordGameProgress(ProgressStatus.Fail, ProgressArea.Authentication, error.ErrorType.ToString(), error.TechnicalDetails);

            ButtonData buttonData = new ButtonData( "OK", HandleOkClicked );

            switch(error.ErrorType)
            {
                case ServerErrorType.Timeout:                   
                    DialogManager.Get.ShowMessagePopup("Error", "We are having some communication issues while authenticating. Please check your internet connection", buttonData);
                    break;
                default:
                    string message = string.Format("Failed to authenticate, {0}", error.UserFacingMessage);
                    DialogManager.Get.ShowMessagePopup("Error", message, buttonData);                   
                    Debug.Log("DeviceAuthState.HandleError: " + error.TechnicalDetails);
                    break;
            }
        }

        private void HandleOkClicked()
        {
            Authenticate();
        }                    
    }
}
