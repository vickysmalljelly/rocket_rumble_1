using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class DemoUpsellState : AbstractGameState
	{	
		#region Member Variables
		private DemoUpsellDisplayController mDemoUpsellDisplayController;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mDemoUpsellDisplayController = UIManager.Get.ShowMenu<DemoUpsellDisplayController>();
			RegisterListeners( mDemoUpsellDisplayController );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UIManager.Get.HideMenu<DemoUpsellDisplayController>();
			UnregisterListeners( mDemoUpsellDisplayController );
        }
		#endregion

		#region Event Registration
		private void RegisterListeners( DemoUpsellDisplayController demoUpsellDisplayController )
		{
			demoUpsellDisplayController.Finished += HandleFinished;
		}

		private void UnregisterListeners( DemoUpsellDisplayController demoUpsellDisplayController )
		{
			demoUpsellDisplayController.Finished -= HandleFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleFinished( object o, EventArgs args )
		{
			TransitionState( GameStateMachine.GAME_STATE_FRONT_END );
		}
		#endregion
    }
}