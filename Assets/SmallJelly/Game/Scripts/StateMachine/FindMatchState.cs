﻿using SmallJelly.Framework;
using System.Collections.Generic;


namespace SmallJelly
{

    /// <summary>
    /// Finds a match with another player
    /// </summary>
	public sealed class FindMatchState : AbstractGameState
    {
        public override void OnEntering()
        {
            base.OnEntering();          

			//We've now selected a deck and thus passed the "HasSelectedDeck" checkpoint
			OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.HasSelectedDeckCheckpoint );

			DialogManager.Get.ShowMatchmakingSpinner();

            FindMatchStateMachine stateMachine = this.AddChildStateMachine<FindMatchStateMachine>();
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
        }

        public override void OnLeaving()
        {

            base.OnLeaving();

			DialogManager.Get.HideMatchmakingSpinner();
        }

		private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionData )
        {
            stateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

			SJLogger.AssertCondition(stateConstructionData != null, "Expect the FindMatchStateMachine to pass stateConstructionMessageData");

			OnStateCompleted( stateConstructionData );
        }


    }
}