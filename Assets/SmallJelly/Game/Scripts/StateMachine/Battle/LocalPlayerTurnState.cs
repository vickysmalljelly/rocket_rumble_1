using SmallJelly.Framework;
using System.Timers;

namespace SmallJelly
{
	/// <summary>
	/// Contains the state machine that controls the game during the local player's turn.
	/// </summary>
	public sealed class LocalPlayerTurnState : PlayerTurnState
	{
		#region Member Variables
		private bool mIsExperiencingConnectionProblems;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();
			SJLogger.LogMessage(MessageFilter.George, "Starting local players turn" );

			mIsExperiencingConnectionProblems = false;
			BattleManager.Get.OnRefreshConnectionProblems( false );

			//Add the interaction state machine
			this.AddChildStateMachine< LocalPlayerTurnInteractionStateMachine >();

			//Fire turn analytic - starting from index 0
			FireBattleTurnAnalytic( BattleManager.Get.TurnCount );

			//Alert the battle manager that it's now the local players turn
			BattleManager.Get.OnStartTurn( PlayerType.Local );

			// Start the replay if in the correct mode
			if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay) {
                ReplayManager.Get.StartReplay();
			}
		}

		public override void Update()
		{
			//Switch the the local players turn - the turn has changed!
			if( !ChallengeManager.Get.IsMyTurn() && NextStateConstructionData == null )
			{
				SJLogger.LogMessage(MessageFilter.Gameplay, "Recieved turn taken message");

				EndTurn();
				NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_REMOTE_PLAYER_TURN );
			}

			if( mIsExperiencingConnectionProblems != ClientBattle.IsExperiencingConnectionProblems )
			{
				BattleManager.Get.OnRefreshConnectionProblems( ClientBattle.IsExperiencingConnectionProblems );
				mIsExperiencingConnectionProblems = ClientBattle.IsExperiencingConnectionProblems;
			}

			base.Update();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Protected Methods
		protected override void EndTurn()
		{
			BattleManager.Get.OnEndTurn( PlayerType.Local );
		}

		public override void Ping()
		{
			//If the ping responds in less than "THRESHOLD_FOR_SLOW_CONNECTION" seconds then we're no longer experiencing connection problems,
			//otherwise we are
			Timer pingTimer = new Timer();
			pingTimer.AutoReset = false;
			pingTimer.Interval = ClientBattle.THRESHOLD_FOR_SLOW_CONNECTION * 1000.0f;
			pingTimer.Elapsed += new ElapsedEventHandler( 
				( object source, ElapsedEventArgs e ) => 
				{ 
					pingTimer.Stop();
					pingTimer.Dispose();

					Ping();
				} 
			);

			pingTimer.Start();

			ClientPing.Ping
			( 
				() => {
					if( pingTimer.Enabled )
					{
						pingTimer.Stop();
						pingTimer.Dispose();

						BattleManager.Get.OnRefreshConnectionProblems( false );
					}
				},

				( ServerError serverError ) =>
				{
					pingTimer.Stop();
					pingTimer.Dispose();
				}
			);
		}

		#endregion

		#region Event Handlers
		private void HandleStartReplaySuccess()
		{
			SJLogger.LogMessage(MessageFilter.Battle, "Local player - Replay started successfully");
		}

		private void HandleStartReplayError(ServerError error)
		{
			SJLogger.LogError("Local player - Failed to start replay: {0}", error.TechnicalDetails);
		}
		#endregion

		#region Analytics Methods
		private void FireBattleTurnAnalytic( int turn )
		{
			//Log analytics for battle turn - lets discuss how to deal with the turn counter
			AnalyticsManager.Get.RecordGameProgress( AnalyticsManager.GameProgress.BattleTurn, turn );
		}
		#endregion
    }
}