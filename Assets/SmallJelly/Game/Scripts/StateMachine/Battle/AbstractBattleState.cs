﻿using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	public abstract class AbstractBattleState : StateMachine.State 
	{
		#region Public Properties
		public StateConstructionData NextStateConstructionData { get; protected set; }
		#endregion

		#region Protected Properties
		protected BattleGameplayController BattleGameplayController;
		#endregion

		#region MonoBehaviour Methods
		public virtual void Update()
		{
			// Wait until all of the events have finished
			if(!BattleManager.Get.AnimationsHaveFinished)
			{
				return;
			}

			if( NextStateConstructionData != default( StateConstructionData ) )
			{
				OnStateCompleted( NextStateConstructionData );

				//Null the state construction data immediately after we've used it incase we need to re-enter the state
				NextStateConstructionData = null;
			}
		}
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			BattleGameplayController = UIManager.Get.GetMenuController<BattleGameplayController>();

			RegisterButtonListeners();
            RegisterServerMessageListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterButtonListeners();
            UnregisterServerMessageListeners();
		}
		#endregion

		#region Event Registration
        private void RegisterServerMessageListeners()
        {
            BattleManager.Get.Challenge.ChallengeWon += HandleChallengeWon;
            BattleManager.Get.Challenge.ChallengeLost += HandleChallengeLost;
        }

        private void UnregisterServerMessageListeners()
        {
            BattleManager.Get.Challenge.ChallengeWon -= HandleChallengeWon;
            BattleManager.Get.Challenge.ChallengeLost -= HandleChallengeLost;
        }

		private void RegisterButtonListeners()
		{
			BattleGameplayController.ExitClicked += HandleExitClicked;
		}

		private void UnregisterButtonListeners()
		{
			BattleGameplayController.ExitClicked -= HandleExitClicked;
		}
		#endregion

		#region Event Handlers
		private void HandleExitClicked()
		{
            // Ensure that this can only be triggered once
            BattleGameplayController.ExitClicked -= HandleExitClicked;

            BattleManager.Get.ConcedeBattle();
		}

        private void HandleChallengeWon(SJChallengeWonMessage message)
		{
            // Stop listening to won and lost messages in case we get them more than once
            UnregisterServerMessageListeners();

            SJJson rewardJson = message.ScriptData.GetObject("reward"); 
            RewardData reward = RewardData.FromJson(rewardJson);

			GameManager.Get.UpdateRewardsOnClient( reward );

			NextStateConstructionData = new NestedTypedStateConstructionData< bool, RewardData >( BattleStateMachine.BATTLE_STATE_REWARD, true, reward );
		}

        private void HandleChallengeLost(SJChallengeLostMessage message)
		{
            // Stop listening to won and lost messages in case we get them more than once
            UnregisterServerMessageListeners();

            SJJson rewardJson = message.ScriptData.GetObject("reward"); 
            RewardData reward = RewardData.FromJson(rewardJson);

			GameManager.Get.UpdateRewardsOnClient( reward );

			NextStateConstructionData = new NestedTypedStateConstructionData< bool, RewardData >( BattleStateMachine.BATTLE_STATE_REWARD, false, reward );
		}
		#endregion
	}
}
