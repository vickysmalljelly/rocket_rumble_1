﻿using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class ResultBattleState : StateMachine.State 
	{
		private BattleResult mBattleResult;

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			bool won = ( ( NestedTypedStateConstructionData< bool, RewardData > ) stateConstructionData ).Value;
			RewardData rewardData = ( ( NestedTypedStateConstructionData< bool, RewardData > ) stateConstructionData ).SecondValue;
            SJLogger.AssertCondition(rewardData != null, "RewardBattleState  OnConstruct Reward data cannot be null");

			BattleManager.Get.BattleGameplayController.LocalPlayerBattleView.PlayerHUD.OnInsertState( PlayerHUDController.State.Hiding );
			BattleManager.Get.BattleGameplayController.RemotePlayerBattleView.PlayerHUD.OnInsertState( PlayerHUDController.State.Hiding );

			mBattleResult = UIManager.Get.ShowMenu< BattleResult >();
			mBattleResult.Refresh( won, rewardData );

			if( won )
			{
				CompleteCheckpoint();
				FireWonAnalytic();

                LeaderboardManager.Get.SetLeaderboardPositionNeedsRefresh();
			}
			else
			{
				FireLostAnalytic();
			}
		}

		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{           
			UnregisterListeners();

			UIManager.Get.HideMenu< BattleResult >();

			base.OnLeaving();
		}
		#endregion

		#region Protected Methods
		private string GetLocalShipClass()
		{
			BattleConfig battleConfig = BattleManager.Get.BattleConfig;

			BattleConfig.PlayerBattleConfig playerBattleConfig;
			if( ChallengeManager.Get.LocalPlayer.UserId == battleConfig.Players[0].PlayerId  )
			{
				playerBattleConfig = battleConfig.Players[0];
			}
			else
			{
				playerBattleConfig = battleConfig.Players[1];
			}

			return playerBattleConfig.ShipClass;
		}
		#endregion


		#region Event Registration
		private void RegisterListeners()
		{
			mBattleResult.FlowFinished += HandleFlowFinished;
		}

		private void UnregisterListeners()
		{
			mBattleResult.FlowFinished -= HandleFlowFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleFlowFinished( object o, EventArgs args )
		{
			OnStateCompleted();
		}
		#endregion

		#region Private Methods
		private void CompleteCheckpoint()
		{
			//Complete the relevant checkpoints (where the checkpoints are completed by a battle win)
			switch( BattleModeManager.Get.CurrentBattleId )
			{
			case BattleId.Tutorial1:
				OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.Tutorial1CheckPoint );
				return;

			case BattleId.Tutorial2:
				OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.Tutorial2CheckPoint );
				return;

			case BattleId.Tutorial3:
				OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.Tutorial3CheckPoint );
				return;

			case BattleId.Tutorial4:
				OnboardingManager.Get.SetCheckpointOnClient( OnboardingManager.Tutorial4CheckPoint );
				OnboardingManager.Get.FinishOnboarding();
				return;
			}
		}
		#endregion

		#region Analytics Methods
		private void FireWonAnalytic()
		{
			AnalyticsManager.Get.RecordBattleResult(BattleModeManager.Get.CurrentBattleId, AnalyticsManager.BattleResult.Won, GetLocalShipClass());
		}

		private void FireLostAnalytic()
		{
			AnalyticsManager.Get.RecordBattleResult(BattleModeManager.Get.CurrentBattleId, AnalyticsManager.BattleResult.Lost, GetLocalShipClass());
		}

		private void FireCompletedBattleAnalytic()
		{
			//Log analytics for battle turn - lets discuss how to deal with the turn counter
			AnalyticsManager.Get.RecordGameProgress( AnalyticsManager.GameProgress.CompletedBattle, 0 );
		}
		#endregion

		#region Event Handlers
        private void HandleStateMachineCompleted( StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
        {
            stateMachine.StateMachineCompleted -= HandleStateMachineCompleted;

            TransitionState(BattleStateMachine.BATTLE_STATE_EXIT);
        }  
		#endregion
	}
}
