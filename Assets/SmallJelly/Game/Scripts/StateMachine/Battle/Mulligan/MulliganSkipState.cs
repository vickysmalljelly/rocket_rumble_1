﻿using SmallJelly.Framework;

namespace SmallJelly
{
    public class MulliganSkipState : StateMachine.State 
    {
        public override void OnEntering()
        {
            base.OnEntering();

            BattleManager.Get.MulliganSkippedSuccess += HandleSuccess;
            BattleManager.Get.NotifyMulliganSkipped();
        }           

        public override void OnLeaving()
        {
            BattleManager.Get.MulliganSkippedSuccess -= HandleSuccess;
            base.OnLeaving();
        }

        protected void HandleSuccess()
		{
			OnStateCompleted( new StateConstructionData( BattleStateMachine.BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA ) );
		}
    }
}
