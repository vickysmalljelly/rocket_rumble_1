﻿using System.Collections;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Controls the mulligan when the player is going first.
	/// </summary>
	public class WaitForInitialBattleDataState : AbstractBattleState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			//Ping every 10 seconds whilst it's the remote players turn
			InvokeRepeating( "Ping", 0.0f, 10.0f );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			CancelInvoke( "Ping" );
		}

		public override void Update()
		{
			base.Update();

			//If we've received data.
			if( BattleManager.Get.BattleGameplayController.Data.LocalLastRunData != null && BattleManager.Get.BattleGameplayController.Data.RemoteLastRunData != null )
			{
				//Finish
				OnStateCompleted( new StateConstructionData( ChallengeManager.Get.CurrentChallenge.NextPlayerId == ChallengeManager.Get.CurrentChallenge.User1.UserId ? BattleStateMachine.BATTLE_STATE_LOCAL_PLAYER_TURN : BattleStateMachine.BATTLE_STATE_REMOTE_PLAYER_TURN ) );
			}
		}
		#endregion
	
		#region Public Methods
		public void Ping()
		{
			Invoke( "DetectSlowConnection", ClientBattle.THRESHOLD_FOR_SLOW_CONNECTION );

			ClientPing.Ping
			( 
				() => {
					if( IsInvoking( "DetectSlowConnection" ) )
					{
						BattleManager.Get.OnRefreshConnectionProblems( false );
						CancelInvoke( "DetectSlowConnection" );
					}
				},

				( ServerError serverError ) =>
				{
					if( BattleManager.Get != null )
					{
						BattleManager.Get.OnEndTurn( BattleManager.Get.CurrentTurn );
					}

					//We've lost connection!
					NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_CONNECTION_LOSS );
				}
			);
		}

		private void DetectSlowConnection()
		{
			BattleManager.Get.OnRefreshConnectionProblems( true );
		}
		#endregion
	}
}