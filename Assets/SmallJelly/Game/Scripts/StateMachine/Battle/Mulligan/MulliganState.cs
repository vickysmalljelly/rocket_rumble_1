﻿using System.Collections;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Controls the mulligan when the player is going first.
	/// </summary>
	public class MulliganState : AbstractBattleState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

            UIManager.Get.ShowMenu< BattleMulliganController >();

			AddChildStateMachine< MulliganStateMachine >();
			RegisterListeners();
		}
		#endregion
 
	

		#region Private Methods
		public override void OnLeaving()
		{
			base.OnLeaving();

			UIManager.Get.HideMenu< BattleMulliganController >();
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		//Register for the message from the server denoting a complete mulligan
		private void RegisterListeners()
		{
            BattleManager.Get.Challenge.MulliganCompleted += HandleMulliganComplete;
		}

		private void UnregisterListeners()
		{
            BattleManager.Get.Challenge.MulliganCompleted -= HandleMulliganComplete;
		}
		#endregion

		#region Event Handlers
		private void HandleMulliganComplete( )
		{
			OnStateCompleted( new StateConstructionData( BattleStateMachine.BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA ) );
		}
		#endregion
	}
}