﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class MulliganStateMachine : StateMachine
	{		
		#region Public Constants
		public const string MULLIGAN_STATE_INITIALISE = "MULLIGAN_INITIALISE";
		public const string MULLIGAN_STATE_SELECT_CARDS = "MULLIGAN_STATE_SELECT_CARDS";
		public const string MULLIGAN_STATE_FINALISE_CARDS = "MULLIGAN_STATE_FINALISE_CARDS";
		public const string MULLIGAN_STATE_WAIT_FOR_COMPLETION = "MULLIGAN_STATE_WAIT_FOR_COMPLETION";

		public const string MULLIGAN_STATE_DOWNLOADING_OPTIONS = "MULLIGAN_STATE_DOWNLOADING_OPTIONS";
		public const string MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS = "MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS";
		#endregion

		#region Member Variables
		private InitialisationMulliganState mInitialisationMulliganState;
		private SelectCardsMulliganState mSelectCardsMulliganState;
		private FinaliseCardsMulliganState mFinaliseCardsMulliganState;
		private WaitForCompletionMulliganState mWaitForCompletionMulliganState;

		private DownloadingOptionsServerProcessingMulliganState mDownloadingOptionsServerProcessingMulliganState;
		private DownloadingFinalCardsServerProcessingMulliganState mDownloadingFinalCardsServerProcessingMulliganState;
		#endregion

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			mInitialisationMulliganState = AddState< InitialisationMulliganState >( MULLIGAN_STATE_INITIALISE );
			mSelectCardsMulliganState = AddState< SelectCardsMulliganState >( MULLIGAN_STATE_SELECT_CARDS );
			mFinaliseCardsMulliganState = AddState< FinaliseCardsMulliganState >( MULLIGAN_STATE_FINALISE_CARDS );
			mWaitForCompletionMulliganState = AddState< WaitForCompletionMulliganState >( MULLIGAN_STATE_WAIT_FOR_COMPLETION );

			mDownloadingOptionsServerProcessingMulliganState = AddState< DownloadingOptionsServerProcessingMulliganState >( MULLIGAN_STATE_DOWNLOADING_OPTIONS );
			mDownloadingFinalCardsServerProcessingMulliganState = AddState< DownloadingFinalCardsServerProcessingMulliganState >( MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS );

			AddTransition( MULLIGAN_STATE_INITIALISE, MULLIGAN_STATE_DOWNLOADING_OPTIONS );

			AddTransition( MULLIGAN_STATE_DOWNLOADING_OPTIONS, MULLIGAN_STATE_SELECT_CARDS );
			//Downloading options can return to the previous state on server error
			AddTransition( MULLIGAN_STATE_DOWNLOADING_OPTIONS, MULLIGAN_STATE_INITIALISE );

			AddTransition( MULLIGAN_STATE_SELECT_CARDS, MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS );

			AddTransition( MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS, MULLIGAN_STATE_FINALISE_CARDS );
			//Downloading options can return to the previous state on server error
			AddTransition( MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS, MULLIGAN_STATE_SELECT_CARDS );

			AddTransition( MULLIGAN_STATE_FINALISE_CARDS, MULLIGAN_STATE_WAIT_FOR_COMPLETION );
			//Waiting for completion can return to the previous state on server error
			AddTransition( MULLIGAN_STATE_WAIT_FOR_COMPLETION, MULLIGAN_STATE_FINALISE_CARDS );

			RegisterListeners();

			TransitionState( MULLIGAN_STATE_INITIALISE );

		}

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInitialisationMulliganState.StateCompleted += HandleStateCompleted;
			mSelectCardsMulliganState.StateCompleted += HandleStateCompleted;

			mFinaliseCardsMulliganState.StateCompleted += HandleStateCompleted;

			mDownloadingOptionsServerProcessingMulliganState.StateCompleted += HandleStateCompleted;
			mDownloadingFinalCardsServerProcessingMulliganState.StateCompleted += HandleStateCompleted;

			mWaitForCompletionMulliganState.StateCompleted += HandleStateCompleted;
		}

		private void UnregisterListeners()
		{
			mInitialisationMulliganState.StateCompleted -= HandleStateCompleted;
			mSelectCardsMulliganState.StateCompleted -= HandleStateCompleted;

			mFinaliseCardsMulliganState.StateCompleted -= HandleStateCompleted;

			mDownloadingOptionsServerProcessingMulliganState.StateCompleted -= HandleStateCompleted;
			mDownloadingFinalCardsServerProcessingMulliganState.StateCompleted -= HandleStateCompleted;

			mWaitForCompletionMulliganState.StateCompleted -= HandleStateCompleted;
		}
		#endregion

		#region Event Handler
		private void HandleStateCompleted( object o, StateConstructionData stateConstructionData )
		{
			TransitionState( stateConstructionData.StateId, stateConstructionData );
		}
		#endregion
	}
}