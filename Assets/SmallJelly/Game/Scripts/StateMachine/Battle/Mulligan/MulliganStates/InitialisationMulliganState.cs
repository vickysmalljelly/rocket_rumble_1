﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class InitialisationMulliganState : AbstractMulliganState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			BattleManager.Get.GetOptions();

			ExitToDownloadingOptionsState();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Private Methods
		private void ExitToDownloadingOptionsState()
		{
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_DOWNLOADING_OPTIONS ) );
		}
		#endregion
	}
}