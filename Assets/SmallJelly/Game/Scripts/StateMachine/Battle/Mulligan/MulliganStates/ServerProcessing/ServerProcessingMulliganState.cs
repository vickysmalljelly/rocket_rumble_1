﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class ServerProcessingMulliganState : AbstractMulliganState
	{
		#region Constants
		private const string SERVER_ERROR_MESSAGE_TITLE_TEXT = "Error";
		private const string SERVER_ERROR_MESSAGE_BUTTON_TEXT = "Ok";
		#endregion

		#region MonoBehaviour Methods
		public override void OnEntering()
		{
			base.OnEntering();
			RegisterListeners();

			UpdateMulliganState();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
			UnregisterListeners();
		}
		#endregion

		#region Protected Methods
		protected abstract void ServerProcessingSuccess();
		protected abstract void ServerProcessingFailure();
		#endregion

		#region Private Methods
		private void UpdateMulliganState()
		{
			BattleManager.Get.BattleMulliganController.Mulligan.OnInsertState( MulliganController.State.ServerProcessing );
			foreach( MulliganEntity mulliganEntity in BattleManager.Get.BattleMulliganController.MulliganEntities )
			{
				//Mulligan is in options idle state
				mulliganEntity.OnInsertState( MulliganEntityController.State.ServerProcessing );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleServerProcessingSuccess( MulliganEventData mulliganEventData )
		{
			if( mulliganEventData is MulliganOptionsEventData )
			{
				//Do something with the data
				MulliganOptionsEventData mulliganOptionsEventData = ( MulliganOptionsEventData ) mulliganEventData;
				BattleManager.Get.BattleMulliganController.Refresh( mulliganOptionsEventData.BattleEntityData );
			}

			ServerProcessingSuccess();
		}

		private void HandleServerProcessingFailure( string errorMessage )
		{
			ButtonData buttonData = new ButtonData( SERVER_ERROR_MESSAGE_BUTTON_TEXT, HandleErrorDismissed );
			DialogManager.Get.ShowMessagePopup( SERVER_ERROR_MESSAGE_TITLE_TEXT, errorMessage, buttonData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.MulliganResponseSuccess += HandleMulliganResponseSuccess;
			BattleManager.Get.MulliganResponseFailure += HandleMulliganResponseFailure;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.MulliganResponseSuccess -= HandleMulliganResponseSuccess;
			BattleManager.Get.MulliganResponseFailure -= HandleMulliganResponseFailure;
		}
		#endregion

		#region Event Handlers
		private void HandleMulliganResponseSuccess( object o, MulliganEventDataEventArgs mulliganEventDataEventArgs )
		{
			HandleServerProcessingSuccess( mulliganEventDataEventArgs.MulliganEventData );
		}

		private void HandleMulliganResponseFailure( object o, StringEventArgs stringEventArgs )
		{
			HandleServerProcessingFailure( stringEventArgs.String );
		}

		private void HandleErrorDismissed()
		{
			ServerProcessingFailure();
		}
		#endregion
	}
}