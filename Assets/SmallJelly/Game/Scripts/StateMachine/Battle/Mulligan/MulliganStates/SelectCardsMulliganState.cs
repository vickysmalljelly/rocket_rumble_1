﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;
using System.Linq;
using System;

namespace SmallJelly
{
	public class SelectCardsMulliganState : AbstractMulliganState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			UpdateState();

			FireMulliganPhase0Analytic();

            if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay) {
                
                MakeSelection();
            }
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void UpdateState()
		{
			BattleManager.Get.BattleMulliganController.Mulligan.OnInsertState( MulliganController.State.OptionsIdle );

			foreach( MulliganEntity mulliganEntity in BattleManager.Get.BattleMulliganController.MulliganEntities )
			{
				//Mulligan is in options idle state
				mulliganEntity.OnInsertState( MulliganEntityController.State.OptionsIdle );

				//Deselect any selected entities
				mulliganEntity.OnInsertSelectionState( TargetableLogicController.SelectionState.NotSelected );
			}
		}

		private void ExitToDownloadingFinalCardsState()
		{
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_DOWNLOADING_FINAL_CARDS ) );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.BattleMulliganController.CompleteClicked += HandleCompleteClicked;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.BattleMulliganController.CompleteClicked -= HandleCompleteClicked;
		}
		#endregion

		#region Event Handlers
		private void HandleCompleteClicked( object obj, EventArgs eventArgs )
		{
            MakeSelection();
		}

        private void MakeSelection()
        {
            List< bool > mulliganEntitiesSelected = BattleManager.Get.BattleMulliganController.MulliganEntitiesSelected;

            //Invert list because the options are those entities which are NOT selected!
            List< bool > mulliganEntitiesNotSelected = mulliganEntitiesSelected.Select( o => !o ).ToList();

            BattleManager.Get.SetOptions( mulliganEntitiesNotSelected );

            ExitToDownloadingFinalCardsState();
        }
		#endregion

		#region Analytics Methods
		private void FireMulliganPhase0Analytic()
		{
			//Log analytics for mulligan phase 0
			AnalyticsManager.Get.RecordGameProgress(AnalyticsManager.GameProgress.Mulligan, 0);
		}
		#endregion
	}
}