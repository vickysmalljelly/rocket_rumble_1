﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Sends a mulligan complete message to the server and waits for a response, it never moves to a next state
	/// because its the terminal element in the state machine. It terminates when the state machine is terminated
	/// by MulliganState.cs moving to the next state.
	/// </summary>
	public class WaitForCompletionMulliganState : ServerProcessingMulliganState
	{
		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			SendMulliganCompletionMessage();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Protected Methods
		protected override void ServerProcessingSuccess()
		{
			//Fire analytic
		}

		protected override void ServerProcessingFailure()
		{
			//Return to the previous state
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_FINALISE_CARDS ) );
		}
		#endregion

		#region Private Methods
		private void SendMulliganCompletionMessage()
		{
			BattleManager.Get.NotifyMulliganComplete( );
		}
		#endregion
	}
}