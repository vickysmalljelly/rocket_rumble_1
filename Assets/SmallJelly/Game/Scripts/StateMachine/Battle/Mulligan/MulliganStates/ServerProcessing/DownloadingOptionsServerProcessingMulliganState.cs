﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DownloadingOptionsServerProcessingMulliganState : ServerProcessingMulliganState
	{
		#region Protected Methods
		protected override void ServerProcessingSuccess()
		{
			//Move to next state
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_SELECT_CARDS ) );
		}

		protected override void ServerProcessingFailure()
		{
			//Return to previous state
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_INITIALISE ) );
		}
		#endregion
	}
}