﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class FinaliseCardsMulliganState : AbstractMulliganState
	{
		#region Constants
		private const float TIME_UNTIL_CARDS_AUTO_FINALISED = 5.0f;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			UpdateStates();

            if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay) 
			{
                ExitToWaitForCompletionState();
				return;
            }

			GameManager.Get.DoCoroutine( WaitForAutoFinalisation() );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Private Methods
		private void UpdateStates()
		{
			BattleManager.Get.BattleMulliganController.Mulligan.OnInsertState( MulliganController.State.FinalisedIdle );

			//TODO - We could move all of this mulligan eneity state stuff into the mulligan controller
			foreach( MulliganEntity mulliganEntity in BattleManager.Get.BattleMulliganController.MulliganEntities )
			{
				mulliganEntity.OnInsertState( MulliganEntityController.State.FinalisedIdle );

				//Deselect any selected entities
				mulliganEntity.OnInsertSelectionState( TargetableLogicController.SelectionState.NotSelected );
			}
		}
		
		private void ExitToWaitForCompletionState()
		{
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_WAIT_FOR_COMPLETION ) );
		}

		//Wait for TIME_UNTIL_CARDS_AUTO_FINALISED seconds until the finalisation autocompletes
		private IEnumerator WaitForAutoFinalisation()
		{
			yield return new WaitForSeconds( TIME_UNTIL_CARDS_AUTO_FINALISED );

			ExitToWaitForCompletionState();
		}
		#endregion
	}
}