﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class DownloadingFinalCardsServerProcessingMulliganState : ServerProcessingMulliganState
	{
		#region Protected Methods
		protected override void ServerProcessingSuccess()
		{
			//Fire analytic
			FireMulliganPhase1Analytic();

			//Move to next state
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_FINALISE_CARDS ) );
		}

		protected override void ServerProcessingFailure()
		{
			//Return to previous state
			OnStateCompleted( new StateConstructionData( MulliganStateMachine.MULLIGAN_STATE_SELECT_CARDS ) );
		}
		#endregion

		#region Analytics Methods
		private void FireMulliganPhase1Analytic()
		{
			//Log analytics for mulligan phase 1
			AnalyticsManager.Get.RecordGameProgress(AnalyticsManager.GameProgress.Mulligan, 1);
		}
		#endregion
	}
}