﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public abstract class AbstractMulliganState : StateMachine.State 
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion
	}
}
