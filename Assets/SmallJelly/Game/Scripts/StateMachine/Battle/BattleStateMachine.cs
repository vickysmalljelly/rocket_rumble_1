using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Controls the two player battle
	/// </summary>
	public sealed class BattleStateMachine : StateMachine
	{
		// States
		public const string BATTLE_STATE_INITIALISE = "BS_INITIALISE";

		public const string BATTLE_STATE_COIN_TOSS_FIRST = "BATTLE_STATE_COIN_TOSS_FIRST";
		public const string BATTLE_STATE_COIN_TOSS_SECOND = "BATTLE_STATE_COIN_TOSS_SECOND";

		public const string BATTLE_STATE_MULLIGAN = "BATTLE_STATE_MULLIGAN";
        public const string BATTLE_STATE_SKIP_MULLIGAN = "BATTLE_STATE_SKIP_MULLIGAN";
		public const string BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA = "BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA";

		public const string BATTLE_STATE_LOCAL_PLAYER_TURN = "BS_LOCAL_PLAYER_TURN";
		public const string BATTLE_STATE_REMOTE_PLAYER_TURN = "BS_REMOTE_PLAYER_TURN";

		public const string BATTLE_STATE_REWARD= "BS_REWARD";
		public const string BATTLE_STATE_CONNECTION_LOSS = "BS_CONNECTION_LOSS";
		public const string BATTLE_STATE_EXIT = "BS_EXIT";

		private InitialBattleState mInitialBattleState;

		private CoinTossFirstState mCoinTossFirstState;
		private CoinTossSecondState mCoinTossSecondState;

		private MulliganState mMulliganState;
		private MulliganSkipState mMulliganSkipState;
		private WaitForInitialBattleDataState mWaitForInitialBattleDataState;
		private LocalPlayerTurnState mLocalPlayerTurnState;
		private RemotePlayerTurnState mRemotePlayerTurnState;

		private ResultBattleState mRewardBattleState;
		private ConnectionLossBattleState mConnectionLossBattleState;
		private ExitState mExitState;

		#region MonoBehaviour Methods
		protected override void Awake()
		{
			base.Awake();

			// Initialise states
			mInitialBattleState = AddState< InitialBattleState >( BATTLE_STATE_INITIALISE );

			mCoinTossFirstState = AddState< CoinTossFirstState >( BATTLE_STATE_COIN_TOSS_FIRST );
			mCoinTossSecondState = AddState< CoinTossSecondState >( BATTLE_STATE_COIN_TOSS_SECOND );

			mMulliganState = AddState< MulliganState >( BATTLE_STATE_MULLIGAN );
			mWaitForInitialBattleDataState = AddState< WaitForInitialBattleDataState >( BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA );

			mMulliganSkipState = AddState< MulliganSkipState >( BATTLE_STATE_SKIP_MULLIGAN );

			mLocalPlayerTurnState= AddState< LocalPlayerTurnState >( BATTLE_STATE_LOCAL_PLAYER_TURN );
			mRemotePlayerTurnState = AddState< RemotePlayerTurnState >( BATTLE_STATE_REMOTE_PLAYER_TURN );

			mRewardBattleState = AddState< ResultBattleState >( BATTLE_STATE_REWARD );

			mConnectionLossBattleState = AddState< ConnectionLossBattleState >( BATTLE_STATE_CONNECTION_LOSS );

			mExitState = AddState< ExitState >( BATTLE_STATE_EXIT );

			// Initialise transitions
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_COIN_TOSS_FIRST);
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_COIN_TOSS_SECOND);
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_EXIT);

			//These two below are used for skipping mulligan in replays
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_LOCAL_PLAYER_TURN);
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_REMOTE_PLAYER_TURN);

			AddTransition(BATTLE_STATE_COIN_TOSS_FIRST, BATTLE_STATE_MULLIGAN);
			AddTransition(BATTLE_STATE_COIN_TOSS_SECOND, BATTLE_STATE_MULLIGAN);

            AddTransition(BATTLE_STATE_COIN_TOSS_FIRST, BATTLE_STATE_SKIP_MULLIGAN);
			AddTransition(BATTLE_STATE_COIN_TOSS_SECOND, BATTLE_STATE_SKIP_MULLIGAN);

			AddTransition(BATTLE_STATE_COIN_TOSS_FIRST, BATTLE_STATE_EXIT);
			AddTransition(BATTLE_STATE_COIN_TOSS_SECOND, BATTLE_STATE_EXIT);

			AddTransition(BATTLE_STATE_SKIP_MULLIGAN, BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA);
			AddTransition(BATTLE_STATE_SKIP_MULLIGAN, BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA);

			AddTransition(BATTLE_STATE_MULLIGAN, BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA);

			AddTransition(BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA, BATTLE_STATE_LOCAL_PLAYER_TURN);
			AddTransition(BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA, BATTLE_STATE_REMOTE_PLAYER_TURN);

			AddTransition(BATTLE_STATE_MULLIGAN, BATTLE_STATE_EXIT);

			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_LOCAL_PLAYER_TURN);
			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_REMOTE_PLAYER_TURN);

			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_REMOTE_PLAYER_TURN);
			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_LOCAL_PLAYER_TURN);

			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_EXIT);
			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_EXIT);

			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_REWARD);

			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_CONNECTION_LOSS);
			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_CONNECTION_LOSS);
			AddTransition(BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA, BATTLE_STATE_CONNECTION_LOSS);

			AddTransition( BATTLE_STATE_CONNECTION_LOSS, BATTLE_STATE_LOCAL_PLAYER_TURN );
			AddTransition( BATTLE_STATE_CONNECTION_LOSS, BATTLE_STATE_REMOTE_PLAYER_TURN );


			AddTransition(BATTLE_STATE_REWARD, BATTLE_STATE_EXIT);

			//Won lost states
			AddTransition(BATTLE_STATE_INITIALISE, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_COIN_TOSS_FIRST, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_COIN_TOSS_SECOND, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_MULLIGAN, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_WAIT_FOR_INITIAL_BATTLE_DATA, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_LOCAL_PLAYER_TURN, BATTLE_STATE_REWARD);
			AddTransition(BATTLE_STATE_REMOTE_PLAYER_TURN, BATTLE_STATE_REWARD);

			RegisterListeners();

			TransitionState(BATTLE_STATE_INITIALISE);
		}

		private void OnDestroy()
		{
			UnregisterListeners();

			// Clear the challenge data
			ChallengeManager.Get.ResetChallenge();
		}
		#endregion

		#region Event Registrations
		private void RegisterListeners()
		{
			mInitialBattleState.StateCompleted += HandleStateCompleted;

			mCoinTossFirstState.StateCompleted += HandleStateCompleted;
			mCoinTossSecondState.StateCompleted += HandleStateCompleted;

			mMulliganState.StateCompleted += HandleStateCompleted;
			mMulliganSkipState.StateCompleted += HandleStateCompleted;
			mWaitForInitialBattleDataState.StateCompleted += HandleStateCompleted;

			mLocalPlayerTurnState.StateCompleted += HandleStateCompleted;
			mRemotePlayerTurnState.StateCompleted += HandleStateCompleted;

			mRewardBattleState.StateCompleted += HandleStateCompleted;
			mConnectionLossBattleState.StateCompleted += HandleStateCompleted;

			mExitState.StateCompleted += HandleExitStateCompleted;
		}

		private void UnregisterListeners()
		{
			mInitialBattleState.StateCompleted -= HandleStateCompleted;

			mCoinTossFirstState.StateCompleted -= HandleStateCompleted;
			mCoinTossSecondState.StateCompleted -= HandleStateCompleted;

			mMulliganState.StateCompleted -= HandleStateCompleted;
			mMulliganSkipState.StateCompleted -= HandleStateCompleted;
			mWaitForInitialBattleDataState.StateCompleted -= HandleStateCompleted;

			mLocalPlayerTurnState.StateCompleted -= HandleStateCompleted;
			mRemotePlayerTurnState.StateCompleted -= HandleStateCompleted;

			mRewardBattleState.StateCompleted -= HandleStateCompleted;
			mConnectionLossBattleState.StateCompleted -= HandleStateCompleted;

			mExitState.StateCompleted -= HandleExitStateCompleted;		
		}
		#endregion

		#region Event Handlers
		private void HandleStateCompleted(State state, StateConstructionData nextStateConstructionData)
		{
			StateConstructionData = nextStateConstructionData;
		}

		private void HandleExitStateCompleted(State state, StateConstructionData nextStateConstructionData)
		{
			// The battle is over
			OnStateMachineCompleted();
		}
		#endregion
	}
}