﻿using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class ConnectionLossBattleState : AbstractBattleState
	{

		#region Member Variables
		private BattleResult mBattleResult;

		private bool mIsTransitioningIn;
		private bool mIsTransitoningOut;

		private string mNextPlayerId;

		private PlayerBattleData mLocalPlayerBattleData;
		private PlayerBattleData mRemotePlayerBattleData;

		private PlayerTargetingBattleData mLocalPlayerTargetingBattleData;
		private PlayerTargetingBattleData mRemotePlayerTargetingBattleData;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			GetLatestBattleData();

			RRChallenge rrChallenge = ( (RRChallenge) ChallengeManager.Get.CurrentChallenge );

			//Pause the queue
			rrChallenge.SetMinimumIssueNumber( int.MaxValue );
            			
            DialogManager.Get.ShowConnectionLossPopup();

			RegisterListeners();
		}

		public override void OnLeaving()
		{      
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Unity Methods
		public override void Update()
		{
			base.Update();

			if( mIsTransitioningIn && !TransitionManager.Get.IsTransitioning() )
			{
                DialogManager.Get.HideConnectionLossPopup();
                DialogManager.Get.HideSpinnerWithMessage();

				mIsTransitioningIn = false;
				mIsTransitoningOut = true;

				RRChallenge rrChallenge = ( (RRChallenge) ChallengeManager.Get.CurrentChallenge );
			
				//Fire turn taken!
				SJTurnTakenMessage sjTurnTakenMessage = new SJTurnTakenMessage();
				sjTurnTakenMessage.NextPlayerId = mNextPlayerId;
				sjTurnTakenMessage.ChallengeId = rrChallenge.ChallengeId;
				rrChallenge.FireTurnTaken( sjTurnTakenMessage );

				BattleManager.Get.BattleGameplayController.OnHardResetData( mLocalPlayerBattleData, mRemotePlayerBattleData, mLocalPlayerTargetingBattleData, mRemotePlayerTargetingBattleData );
				BattleManager.Get.OnRefreshConnectionProblems( false );

				TransitionManager.Get.StopFadeToBlackTransition();
				TransitionManager.Get.PlayFadeFromBlackTransition();
			}

			if( mIsTransitoningOut && !TransitionManager.Get.IsTransitioning() )
			{
				TransitionManager.Get.StopFadeFromBlackTransition();

				mIsTransitioningIn = false;
				mIsTransitoningOut = false;
				if( ChallengeManager.Get.IsMyTurn() )
				{
					NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_LOCAL_PLAYER_TURN );
				}
				else
				{
					NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_REMOTE_PLAYER_TURN );
				}
			}
		}
		#endregion

		#region Private Methods
		private void GetLatestBattleData()
		{
			BattleManager.Get.GetLatestBattleData();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.GetLatestBattleDataSuccess += HandleGetLatestBattleDataSuccess;
			BattleManager.Get.ConnectionError += HandleConnectionError;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.GetLatestBattleDataSuccess -= HandleGetLatestBattleDataSuccess;
			BattleManager.Get.ConnectionError -= HandleConnectionError;
		}
		#endregion

		#region Event Handlers
		private void HandleGetLatestBattleDataSuccess( object o, PlayerBattleDataEventArgs playerBattleDataEventArgs )
		{
			RRChallenge rrChallenge = ( (RRChallenge) ChallengeManager.Get.CurrentChallenge );

			//Resume the queue from supplied issue number
			rrChallenge.SetMinimumIssueNumber( playerBattleDataEventArgs.IssueNumber );

			DialogManager.Get.HideSpinnerWithMessage();
            DialogManager.Get.HideConnectionLossPopup();
			DialogManager.Get.ShowSpinnerWithMessage( "*Reconnecting*", "*Reconnecting*" );

			mIsTransitioningIn = true;
			mIsTransitoningOut = false;

			mNextPlayerId = playerBattleDataEventArgs.NextPlayerId;

			mLocalPlayerTargetingBattleData = playerBattleDataEventArgs.LocalPlayerTargetingBattleData;
			mRemotePlayerTargetingBattleData = playerBattleDataEventArgs.RemotePlayerTargetingBattleData;

			mLocalPlayerBattleData = playerBattleDataEventArgs.LocalPlayerBattleData;
			mRemotePlayerBattleData = playerBattleDataEventArgs.RemotePlayerBattleData;

			TransitionManager.Get.PlayFadeToBlackTransition();
		}

		private void HandleConnectionError( object o, EventArgs args )
		{
			GetLatestBattleData();
		}
		#endregion
	}
}
