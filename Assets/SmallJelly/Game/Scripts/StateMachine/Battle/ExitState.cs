﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class ExitState : StateMachine.State 
	{
		public override void OnEntering()
		{
			base.OnEntering();
		}

		public void Update()
		{
			//For now let's immediately finish the state, eventually we'll probably have an exit animation of some sort
			OnStateCompleted();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
	}
}
