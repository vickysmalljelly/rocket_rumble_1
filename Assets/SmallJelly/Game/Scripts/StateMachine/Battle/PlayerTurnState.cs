using SmallJelly.Framework;
using System.Linq;
using System;

namespace SmallJelly
{
	public abstract class PlayerTurnState : AbstractBattleState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Public Methods
		public abstract void Ping();
		#endregion

		#region Protected Methods
		protected abstract void EndTurn();
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			//BattleManager.Get.Challenge.TurnTaken += HandleChallengeTurnTaken;
			BattleManager.Get.ConnectionError += HandleConnectionError;
		}

		private void UnregisterListeners()
		{
			//BattleManager.Get.Challenge.TurnTaken -= HandleChallengeTurnTaken;
			BattleManager.Get.ConnectionError -= HandleConnectionError;
		}
		#endregion

		#region Event Handlers
		/**
		private void HandleChallengeTurnTaken( SJTurnTakenMessage turnTakenMessage )
		{
			EndTurn();

			SJLogger.LogMessage(MessageFilter.Gameplay, "Recieved turn taken message");
			NextStateConstructionData = (turnTakenMessage.NextPlayerId == ChallengeManager.Get.LocalPlayer.UserId) ? new StateConstructionData( BattleStateMachine.BATTLE_STATE_LOCAL_PLAYER_TURN ) : new StateConstructionData( BattleStateMachine.BATTLE_STATE_REMOTE_PLAYER_TURN );
		}**/

		private void HandleConnectionError( object o, EventArgs args )
		{
			EndTurn();

			NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_CONNECTION_LOSS );
		}
		#endregion
    }
}