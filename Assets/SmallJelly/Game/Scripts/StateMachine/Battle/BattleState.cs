using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Controls the battle state of the game
	/// </summary>
	public sealed class BattleState : AbstractGameState
	{	
		#region Member Variables
		private BattleStateConstructionData mBattleStateConstructionData;
		private BattleStateMachine mBattleStateMachine;
		private BattleManager mBattleManager;
		private BattleOnboardingManager mBattleOnboardingManager;
		#endregion

		#region State Machine Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
        {
			mBattleStateConstructionData = (BattleStateConstructionData) stateConstructionData;

            SJLogger.AssertCondition(mBattleStateConstructionData.Challenge != null, "Must have a challenge");
            SJLogger.AssertCondition(mBattleStateConstructionData.Challenge.BattleConfig != null, "Must have a battle config");
            SJLogger.AssertCondition(mBattleStateConstructionData.Challenge.BattleConfig.Players.Count == 2, "Must have two players in the config");
		}

		public override void OnEntering()
		{
			base.OnEntering();

			SJLogger.AssertCondition( mBattleStateConstructionData != null, "Battle state must be constructed with data - it needs a BattleConfig to initiate the battle" );
		
			RRChallenge challenge = mBattleStateConstructionData.Challenge;    

			GameManager.Get.AddBattleManager( challenge );

			mBattleOnboardingManager = CreateBattleOnboardingManager( challenge );

			//Create state machine
			mBattleStateMachine = this.AddChildStateMachine<BattleStateMachine>();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
            
			UnregisterListeners();

            // Hide the menu
			UIManager.Get.HideMenu<BattleGameplayController>();

			GameManager.Get.RemoveBattleManager();

			//Destroy the battle onboarding manager gameObject
			Destroy( mBattleOnboardingManager.gameObject );
        }
		#endregion

		#region Private Methods

		private BattleOnboardingManager CreateBattleOnboardingManager( RRChallenge challenge )
		{
			BattleOnboardingManager battleOnboardingManagerResource = Resources.Load< BattleOnboardingManager >( FileLocations.BattleOnboardingPrefab );
			BattleOnboardingManager battleOnboardingManager = Instantiate( battleOnboardingManagerResource );
			battleOnboardingManager.transform.parent = transform;
			battleOnboardingManager.transform.localPosition = Vector3.zero;
			battleOnboardingManager.transform.localEulerAngles = Vector3.zero;

			battleOnboardingManager.Initialise( challenge );

			return battleOnboardingManager;
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mBattleStateMachine.StateMachineCompleted += HandleStateMachineCompleted;
		}

		private void UnregisterListeners()
		{
			mBattleStateMachine.StateMachineCompleted -= HandleStateMachineCompleted;
		}
		#endregion

		#region Event Handlers
		private void HandleStateMachineCompleted(StateMachine stateMachine, StateConstructionData stateConstructionMessageData )
		{
			//If we've not passed all of the tutorial checkpoints then move back to the map
			if( BattleManager.Get.IsTutorial() )
			{
				TransitionState( GameStateMachine.GAME_STATE_MAP );
			}
			else
			{
				//If we've passed all of the tutorial checkpoints then go back to the main screen
				TransitionState( GameStateMachine.GAME_STATE_FRONT_END );
			}
		}
		#endregion
        
    }
}