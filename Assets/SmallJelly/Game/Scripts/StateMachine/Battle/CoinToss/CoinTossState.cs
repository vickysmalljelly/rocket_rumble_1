﻿using System.Collections;
using SmallJelly.Framework;
using UnityEngine;
using System;
using System.IO;

namespace SmallJelly
{
	public abstract class CoinTossState : AbstractBattleState
	{
		#region Protected Properties
		protected abstract BattleCoinController.State State { get; }		
		#endregion

		#region Member Variables
		private bool mIntroFinished;
		private bool mCoinTossFinished;

		private BattleIntroController mBattleIntro;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mBattleIntro = UIManager.Get.ShowMenu< BattleIntroController >();

			PlayIntro( );
			PlayCoinToss( );

			RegisterListeners();

			if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay)
			{
				OnStateCompleted( new StateConstructionData( BattleManager.Get.BattleConfig.MulliganEnabled ? BattleStateMachine.BATTLE_STATE_MULLIGAN : BattleStateMachine.BATTLE_STATE_SKIP_MULLIGAN ) );
				return;
			}
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			UIManager.Get.HideMenu< BattleIntroController >();

            //Show the gameplay
            UIManager.Get.ShowMenu< BattleGameplayController >();
		}
		#endregion

		#region Private Methods
		private void PlayIntro()
		{
			mBattleIntro.BattleIntroCutsceneController.OnInsertState( BattleIntroCutsceneController.State.Initialisation );
			mBattleIntro.BattleIntroCutsceneController.OnInsertState( BattleIntroCutsceneController.State.Playing );
		}

		private void PlayCoinToss()
		{
			mBattleIntro.BattleCoinController.OnInsertState( State );
		}

		private void CheckAnimationFinish()
		{
			if( mIntroFinished && mCoinTossFinished )
			{
				OnStateCompleted( new StateConstructionData( BattleManager.Get.BattleConfig.MulliganEnabled ? BattleStateMachine.BATTLE_STATE_MULLIGAN : BattleStateMachine.BATTLE_STATE_SKIP_MULLIGAN ) );
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mBattleIntro.BattleIntroCutsceneController.AnimationFinished += HandleBattleIntroControllerAnimationFinished;
			mBattleIntro.BattleCoinController.AnimationFinished += HandleCoinTossAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mBattleIntro.BattleIntroCutsceneController.AnimationFinished -= HandleBattleIntroControllerAnimationFinished;
			mBattleIntro.BattleCoinController.AnimationFinished -= HandleCoinTossAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleBattleIntroControllerAnimationFinished( object o, EventArgs eventArgs )
		{
			mIntroFinished = true;

			CheckAnimationFinish();
		}

		private void HandleCoinTossAnimationFinished( object o, EventArgs eventArgs )
		{
			mCoinTossFinished = true;

			CheckAnimationFinish();
		}
		#endregion
	}
}