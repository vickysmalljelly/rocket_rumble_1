﻿using System.Collections;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	public sealed class CoinTossFirstState : CoinTossState
	{
		#region Protected Properties
		protected override BattleCoinController.State State
		{ 
			get 
			{ 
				return BattleCoinController.State.FirstTurn; 
			} 
		}
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion
	}
}