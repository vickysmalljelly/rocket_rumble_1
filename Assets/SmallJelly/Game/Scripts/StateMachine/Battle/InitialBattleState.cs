using SmallJelly.Framework;

namespace SmallJelly
{
	/// <summary>
	/// Initialises the battle, depending on whose turn it is first and on the battle mode.
	/// </summary>
	public sealed class InitialBattleState : AbstractBattleState
	{
		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			bool isMyTurn;

			// If we are in replay mode check who the first player is and transition to that state
			if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay)
			{
                isMyTurn = BattleManager.Get.Challenge.ReplayFirstUserId == ChallengeManager.Get.LocalPlayer.UserId;
			}
			else
			{
				isMyTurn = ChallengeManager.Get.IsMyTurn();
			}

			SJLogger.LogMessage(MessageFilter.George, "Early In the Flow - Local Player User Id " + ChallengeManager.Get.LocalPlayer.UserId );
			SJLogger.LogMessage(MessageFilter.George, "Early In the Flow - Remote Player User Id " + ChallengeManager.Get.RemotePlayerId );
			SJLogger.LogMessage(MessageFilter.George, "Early In the Flow - Next Player Id " + ChallengeManager.Get.CurrentChallenge.NextPlayerId );

			// If we're in a non-replay mode check whose turn it is
			if(isMyTurn)
			{
				SJLogger.LogMessage(MessageFilter.George, "It's the local players turn first" );

                SJLogger.LogMessage(MessageFilter.Vicky, "It's my turn, go to mulligan first state");
				OnStateCompleted( new StateConstructionData( BattleStateMachine.BATTLE_STATE_COIN_TOSS_FIRST ) );
			}
			else
			{
				SJLogger.LogMessage(MessageFilter.George, "It's the remote players turn first" );

                SJLogger.LogMessage(MessageFilter.Vicky, "It's not my turn, go to mulligan second state");
				OnStateCompleted( new StateConstructionData( BattleStateMachine.BATTLE_STATE_COIN_TOSS_SECOND ) );
			}
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Analytics Methods
		private void FireStartBattleAnalytic()
		{
			//Log analytics for start battle
			AnalyticsManager.Get.RecordGameProgress(AnalyticsManager.GameProgress.StartBattle, 0);
		}
		#endregion
	}
}
