﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Initialises the replay
    /// </summary>
    public sealed class InitialReplayState : StateMachine.State 
    {
        public override void OnEntering()
        {
            base.OnEntering();

            DialogManager.Get.ShowSpinner();

            ReplayInitialiseStateMachine stateMachine = this.AddChildStateMachine<ReplayInitialiseStateMachine>();
            stateMachine.StateMachineCompleted += HandleStateMachineCompleted;
           
        }

        public override void OnLeaving()
        {
            DialogManager.Get.HideSpinner();

            base.OnLeaving();
        }

		private void HandleStateMachineCompleted(StateMachine stateMachine, StateConstructionData tateConstructionMessageData )
        {
            // Initialisation completed
            TransitionState(ReplayStateMachine.REPLAY_STATE_START);

        }
    }
}