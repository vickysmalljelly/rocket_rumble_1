﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Controls a battle replay
    /// </summary>
    public sealed class ReplayStateMachine : StateMachine
    {
        // States
        public const string REPLAY_STATE_INITIALISE = "RS_INITIALISE";
        public const string REPLAY_STATE_START = "RS_START";

        private bool mStartedRunning = false;

        private StartReplayState mStartReplayState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            AddState<InitialReplayState>(REPLAY_STATE_INITIALISE);
            mStartReplayState = AddState<StartReplayState>(REPLAY_STATE_START);
            mStartReplayState.StateCompleted += HandleStateCompleted;

            // Initialise transitions
            AddTransition(REPLAY_STATE_INITIALISE, REPLAY_STATE_START);
        }

        private void Update()
        {
            if( !mStartedRunning )
            {
                mStartedRunning = true;

                //Move to first state
                TransitionState(REPLAY_STATE_INITIALISE);
            }
        }

        private void HandleStateCompleted(State state, StateConstructionData data)
        {
            this.OnStateMachineCompleted( data );
        }
    }
}