﻿using System.Collections.Generic;
using SmallJelly.Framework;
using UnityEngine;

namespace SmallJelly
{
	/// <summary>
	/// Starts the replay
	/// </summary>
	public sealed class StartReplayState : StateMachine.State
	{
		#region Member Variables
		private ReplayMenuController mMenuController;
		#endregion

		#region State Machine Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mMenuController = UIManager.Get.ShowMenu<ReplayMenuController>();

			mMenuController.OkClicked += HandleOkClicked;
		}

		public override void OnLeaving()
		{
            ChallengeManager.Get.ChallengeCreated -= HandleChallengeCreatedMessage;
			mMenuController.OkClicked -= HandleOkClicked;

			UIManager.Get.HideMenu<ReplayMenuController>();

			base.OnLeaving();
		}
		#endregion

		#region Event Handlers
		private void HandleOkClicked()
		{
			DialogManager.Get.ShowSpinner();

            ChallengeManager.Get.ChallengeCreated += HandleChallengeCreatedMessage;

			// Send challenge request
			List<string> users = new List<string>();
			users.Add(ReplayManager.Get.UserToChallenge.UserId);

            string replayName = mMenuController.GetReplayName();
            ReplayManager.Get.SetReplayName(replayName);
            ClientReplay.RequestReplayChallenge(replayName, BattleModeManager.Get.GetChallengeShortCode(), users, HandleChallengeRequestSuccess, HandleChallengeRequestError);
		}

		private void HandleChallengeRequestSuccess()
		{
			SJLogger.LogMessage(MessageFilter.Challenge, "Challenge request sent successfully");

			// Do nothing as we are waiting for the other user to accept or decline the challenge
		}

		private void HandleChallengeRequestError(ServerError error)
		{
			SJLogger.LogError("RequestReplayChallenge - {0}, {1}, {2}",
				error.ErrorType, error.UserFacingMessage, error.TechnicalDetails);

			DialogManager.Get.HideSpinner();
		}

        private void HandleChallengeCreatedMessage(SJChallengeCreatedMessage message)
		{
			SJLogger.LogMessage(MessageFilter.GameState, "Challenge created");

            // Stop listening to event
            ChallengeManager.Get.ChallengeCreated -= HandleChallengeCreatedMessage;

			DialogManager.Get.HideSpinner();

			// We have finished the replay initialisation
            OnStateCompleted( new BattleStateConstructionData( (RRChallenge)message.Challenge ) );
		}
		#endregion
	}
}