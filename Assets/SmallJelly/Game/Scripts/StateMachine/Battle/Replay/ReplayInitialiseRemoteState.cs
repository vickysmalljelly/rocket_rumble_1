﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Finds and sets the id of the remote player to use in the replay
    /// </summary>
    public sealed class ReplayInitialiseRemoteState : StateMachine.State 
    {   
        public override void OnEntering()
        {
            base.OnEntering();

            // Find the system player that will act as the remote player in the challenge 
            ClientUser.FindUserByName("system_replay", HandleFindUserSuccess, HandleFindUserError);
        }

        public override void OnLeaving()
        {
            base.OnLeaving();
        }

        private void HandleFindUserSuccess(User user) 
        {
            SJLogger.LogMessage(MessageFilter.Challenge, "Found system_replay user");

            ReplayManager.Get.UserToChallenge = user;

            TransitionState(ReplayInitialiseStateMachine.REPLAY_STATE_INITIALISE_LOCAL);
        }

        private void HandleFindUserError(ServerError error) 
        {
            string message = string.Format("{0}, {1}", error.UserFacingMessage, error.TechnicalDetails);
            SJLogger.LogError(message);
        }
    }
}