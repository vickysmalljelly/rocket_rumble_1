﻿using UnityEngine;
using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Initialises a replay
    /// </summary>
    public sealed class ReplayInitialiseStateMachine : StateMachine
    {
        // States
        public const string REPLAY_STATE_INITIALISE_REMOTE = "RSI_GET_REPLAY_REMOTE_PLAYER";
        public const string REPLAY_STATE_INITIALISE_LOCAL = "RSI_GET_REPLAY_LOCAL_PLAYER";

        private bool mStartedRunning = false;
        private ReplayInitialiseLocalState mState;

        protected override void Awake()
        {
            base.Awake();

            // Initialise states
            AddState<ReplayInitialiseRemoteState>(REPLAY_STATE_INITIALISE_REMOTE);
            mState = AddState<ReplayInitialiseLocalState>(REPLAY_STATE_INITIALISE_LOCAL);
            mState.StateCompleted += HandleInitialisationCompleted;

            // Initialise transitions
            AddTransition(REPLAY_STATE_INITIALISE_REMOTE, REPLAY_STATE_INITIALISE_LOCAL);
     
        }

        private void Update()
        {
            if(!mStartedRunning)
            {
                mStartedRunning = true;

                //Move to first state
                TransitionState(REPLAY_STATE_INITIALISE_REMOTE);
            }
        }

        private void HandleInitialisationCompleted(State state, StateConstructionData data)
        {
            mState.StateCompleted -= HandleInitialisationCompleted;

            this.OnStateMachineCompleted();
        }
            
    }
}