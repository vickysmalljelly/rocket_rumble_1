﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Finds and sets the id of the local player to use in the replay, then logs us in as that player
    /// </summary>
    public sealed class ReplayInitialiseLocalState : StateMachine.State 
    {   
		#region Member Variables
        string mReplayPlayerName;
		#endregion

		#region State Machine Methods
        public override void OnEntering()
        {
            base.OnEntering();

            string currentPlayerName = ClientAuthentication.PlayerAccountDetails.DisplayName;
            mReplayPlayerName = string.Concat("system_replay_", currentPlayerName);

            ClientUser.FindUserByName(mReplayPlayerName, HandleFindUserSuccess, HandleFindUserError);

        }

        public override void OnLeaving()
        {
            base.OnLeaving();
        }
		#endregion

		#region Event Handlers
        private void HandleFindUserSuccess(User user) 
        {
            // The replay player exists in the database

            SJLogger.LogMessage(MessageFilter.Challenge, "Found {0} user", user.DisplayName);

            ReplayManager.Get.UserToRunReplay = user;

            GameManager.Get.Logout();

            ClientAuthentication.Login(user.DisplayName, "pw", HandleLoginSuccess, HandleLoginError);
        }

        private void HandleFindUserError(ServerError error) 
        {
            if(error.ErrorType == ServerErrorType.PlayerIdNotFound)
            {
                // The replay player doesn't exist in the database, need to create it
                SJLogger.LogMessage(MessageFilter.Challenge, "The replay player {0} doesn't exist in the database, need to create it", mReplayPlayerName);

                GameManager.Get.Logout();

                ClientAuthentication.Register(mReplayPlayerName, "pw", HandleRegistrationSuccess, HandleRegistrationError);

                return;
            }

            // This is an unexpected error
            string message = string.Format("{0}, {1}, {2}", error.ErrorType.ToString(), error.UserFacingMessage, error.TechnicalDetails);
            SJLogger.LogError("Failed to find user {0}. {1}", mReplayPlayerName, message);
        }

        private void HandleRegistrationSuccess()
        {
            SJLogger.LogMessage(MessageFilter.Challenge, "Player {0} registered", mReplayPlayerName);

            this.OnStateCompleted();
        }

        private void HandleRegistrationError(ServerError error)
        {
            string message = string.Format("{0}, {1}, {2}", error.ErrorType.ToString(), error.UserFacingMessage, error.TechnicalDetails);
            SJLogger.LogError("Failed to register as user {0}. {1}", mReplayPlayerName, message);
        }

        private void HandleLoginSuccess()
        {
            SJLogger.LogMessage(MessageFilter.Challenge, "Player {0} logged in", mReplayPlayerName);

            this.OnStateCompleted();
        }

        private void HandleLoginError(ServerError error)
        {
            string message = string.Format("{0}, {1}, {2}", error.ErrorType.ToString(), error.UserFacingMessage, error.TechnicalDetails);
            SJLogger.LogError("Failed to log in as user {0}. {1}", mReplayPlayerName, message);
        }
		#endregion
    }
}