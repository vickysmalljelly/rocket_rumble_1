using SmallJelly.Framework;
using System.Timers;

namespace SmallJelly
{
	/// <summary>
	/// Controls the gameplay when it is the remote player's turn.
	/// </summary>
	public sealed class RemotePlayerTurnState : PlayerTurnState
	{
		#region Member Variables
		private float mElapsedTime;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			SJLogger.LogMessage(MessageFilter.George, "Starting remote players turn" );

			//Add the interaction state machine
			this.AddChildStateMachine< RemotePlayerTurnInteractionStateMachine >();

			//Alert the battle manager that it's now the remote players turn
			BattleManager.Get.OnStartTurn(PlayerType.Remote);

            // Start the replay if in the correct mode
            if(BattleModeManager.Get.CurrentBattleMode == BattleMode.Replay) {
                ReplayManager.Get.StartReplay();
            }
				
			//Ping every 10 seconds whilst it's the remote players turn
			InvokeRepeating( "Ping", 0.0f, 10.0f );

			Update();
		}

		public override void Update()
		{
			//Switch the the local players turn - the turn has changed!
			if( ChallengeManager.Get.IsMyTurn() && NextStateConstructionData == null )
			{
				SJLogger.LogMessage(MessageFilter.Gameplay, "Recieved turn taken message");

				EndTurn();
				NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_LOCAL_PLAYER_TURN );
			}

			base.Update();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			CancelInvoke( "Ping" );
		}
		#endregion

		#region Public Methods
		public override void Ping()
		{
			Invoke( "DetectSlowConnection", ClientBattle.THRESHOLD_FOR_SLOW_CONNECTION );

			ClientPing.Ping
			( 
				() => {
					if( IsInvoking( "DetectSlowConnection" ) )
					{
						BattleManager.Get.OnRefreshConnectionProblems( false );
						CancelInvoke( "DetectSlowConnection" );
					}
				},

				( ServerError serverError ) =>
				{
					//We've lost connection!
					EndTurn();
					NextStateConstructionData = new StateConstructionData( BattleStateMachine.BATTLE_STATE_CONNECTION_LOSS );
				}
			);
		}

		private void DetectSlowConnection()
		{
			BattleManager.Get.OnRefreshConnectionProblems( true );
		}
		#endregion

		#region Protected Methods
		protected override void EndTurn()
		{
			BattleManager.Get.OnEndTurn( PlayerType.Remote );
		}
		#endregion

		#region Event Handlers       
		private void HandleStartReplaySuccess()
		{
			SJLogger.LogMessage(MessageFilter.Battle, "Remote player - Replay started successfully");
		}

		private void HandleStartReplayError(ServerError error)
		{
			SJLogger.LogError("Remote player - Failed to start replay: {0}", error.TechnicalDetails);
		}
		#endregion
	}
}