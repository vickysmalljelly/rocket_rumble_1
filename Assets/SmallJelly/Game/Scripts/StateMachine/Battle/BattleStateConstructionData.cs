﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class BattleStateConstructionData : StateConstructionData
	{
		#region Public Properties
        public RRChallenge Challenge
		{
			get
			{
                return mChallenge;
			}
		}
		#endregion

		#region Member Variables
        private readonly RRChallenge mChallenge;
		#endregion

		#region Constructors
		public BattleStateConstructionData(RRChallenge rrChallenge) : base(GameStateMachine.GAME_STATE_PLAY_BATTLE)
		{
            mChallenge = rrChallenge;
		}
		#endregion
	}
}