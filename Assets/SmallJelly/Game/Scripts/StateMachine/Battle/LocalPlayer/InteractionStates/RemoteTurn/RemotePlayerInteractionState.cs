﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class RemotePlayerInteractionState : InteractionState
	{
		#region Public Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			InteractionFilter interactionFilter = new InteractionFilter( InteractionStateConstructionData.BattleFilters, InteractionStateConstructionData.FilterStateResults, new BattleEntity[0] );
			BattleManager.Get.OnApplyInteractionFilter( interactionFilter );
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion
	}
}