﻿using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
	public class InteractionState : StateMachine.State
	{
		#region Protected Properties
		protected InteractionStateConstructionData InteractionStateConstructionData { get { return mInteractionStateConstructionData; } }
		#endregion

		#region Member Variables
		private InteractionStateConstructionData mInteractionStateConstructionData;
		#endregion

		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			mInteractionStateConstructionData = (InteractionStateConstructionData)stateConstructionData;
		}

		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}
			

		public override void OnLeaving()
		{
			UnregisterListeners();

			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity state transitions
		public virtual void OnBattleEntityStateTransitioned( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			
		}

		//Allows the state to react in response to battle entity targetable state transitions
		public virtual void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.TargetJettison += HandleTargetJettison;

			BattleManager.Get.BattleEntityStateTransitioned += HandleBattleEntityStateTransitioned;
			BattleManager.Get.BattleEntityTargetableStateTransitioned += HandleBattleEntityTargetableStateTransitioned;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.TargetJettison -= HandleTargetJettison;

			BattleManager.Get.BattleEntityStateTransitioned -= HandleBattleEntityStateTransitioned;
			BattleManager.Get.BattleEntityTargetableStateTransitioned -= HandleBattleEntityTargetableStateTransitioned;

		}
		#endregion

		#region Event Handlers
		private void HandleBattleEntityStateTransitioned( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			OnBattleEntityStateTransitioned( battleEntityAndStateTransitionEventArgs.BattleEntity, battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState, battleEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		private void HandleBattleEntityTargetableStateTransitioned( object o, BattleEntityAndTargetableStateTransitionEventArgs battleEntityAndTargetableStateTransitionEventArgs )
		{
			OnBattleEntityTargetableStateTransitioned( battleEntityAndTargetableStateTransitionEventArgs.BattleEntity, battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState, battleEntityAndTargetableStateTransitionEventArgs.EntityHitLayers );
		}

		private void HandleTargetJettison( object o, SocketInformationEventArgs socketInformationEventArgs )
		{
			BattleManager.Get.JettisonEntity( socketInformationEventArgs.SlotId );
		}
		#endregion

	}
}