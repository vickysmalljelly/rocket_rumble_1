﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class ComponentInstallationCardTextTargetingInteractionState : CardTextTargetingInteractionState
	{
		#region Member Variables
		private int mInstallationSocketId;
		#endregion

		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			mInstallationSocketId = ((TargetingComponentInstallationCardTextInteractionStateConstructionData)stateConstructionData).InstallationSocketId;
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity targetable state transitions
		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( battleEntity, previousState, newState, hitLayers );

			//Check whether we're interested in this entity
			if( battleEntity != SourceBattleEntity )
			{
				return;
			}

			switch( previousState )
			{
				//If the dragging was released
				case BattleEntityController.TargetableState.Targeting:
					bool hitTarget = FindTarget( hitLayers );
					if( hitTarget )
					{
						OnStateCompleted();
					}
					else
					{
						//We didnt hit anything so carry on targeting!
						SourceBattleEntity.OnInsertTargetableState( TargetableLogicController.TargetableState.Targeting );
					}
					break;
			}
		}
		#endregion

		#region Protected Methods
		protected override void TargetSocket( BattleEntity sourceBattleEntity, Socket targetableSocket )
		{
			PlayerType owner;
			int targetSlotIndex;
			BattleManager.Get.GetSocketInformation( targetableSocket, out owner, out targetSlotIndex );

			TargetData targetData = new TargetData();

			targetData.TypeOfTarget = TargetData.TargetType.Socket;
			targetData.PlayerId = owner == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId;
			targetData.SocketId = targetSlotIndex;

			BattleManager.Get.InstallComponentWithTarget( sourceBattleEntity.Data.DrawNumber, mInstallationSocketId, targetData );

		}

		protected override void TargetShip( BattleEntity sourceBattleEntity, Ship targetableShip )
		{
			PlayerType owner;
			BattleManager.Get.GetShipInformation( targetableShip, out owner );

			TargetData targetData = new TargetData();
			targetData.TypeOfTarget = TargetData.TargetType.Hull;
			targetData.PlayerId = owner == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId;

			BattleManager.Get.InstallComponentWithTarget( sourceBattleEntity.Data.DrawNumber, mInstallationSocketId, targetData );
		}
		#endregion
	}
}