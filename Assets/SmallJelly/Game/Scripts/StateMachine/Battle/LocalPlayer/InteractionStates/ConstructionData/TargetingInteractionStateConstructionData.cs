﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class TargetingInteractionStateConstructionData : InteractionStateConstructionData 
	{
		#region Public Properties
		public BattleEntity SourceBattleEntity 
		{ 
			get
			{
				return mSourceBattleEntity;
			}
		}
		#endregion

		#region Member Variables
		private readonly BattleEntity mSourceBattleEntity;
		#endregion

		#region Constructor
		public TargetingInteractionStateConstructionData( string stateId, BattleFilter[] battleFilters, FilterStateResult[] filterStateResults, BattleEntity sourceBattleEntity ) : base( stateId, battleFilters, filterStateResults )
		{
			mSourceBattleEntity = sourceBattleEntity;
		}
		#endregion
	}
}