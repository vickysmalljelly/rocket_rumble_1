﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class InteractionStateConstructionData : StateConstructionData 
	{
		#region Public Properties
		public BattleFilter[] BattleFilters
		{
			get;
			set;
		}

		//The new states as a result of filters
		public FilterStateResult[] FilterStateResults
		{
			get;
			set;
		}
		#endregion

		#region Constructor
		public InteractionStateConstructionData( string stateId, BattleFilter[] battleFilters, FilterStateResult[] filterStateResults ) : base( stateId )
		{
			BattleFilters = battleFilters;
			FilterStateResults = filterStateResults;
		}
		#endregion
	}
}