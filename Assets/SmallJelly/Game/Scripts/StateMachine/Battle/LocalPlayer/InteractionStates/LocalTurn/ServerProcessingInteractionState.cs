﻿using SmallJelly.Framework;
using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Controls the gameplay while in the play phase of the local player
	/// </summary>
	public sealed class ServerProcessingInteractionState : InteractionState 
	{
		#region MonoBehaviour Methods
		public void Update()
		{
			//Finish state when the server has stopped processing
			if( !ClientBattle.ServerIsProcessingGameplay() )
			{
				OnStateCompleted();
			}
		}
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			InteractionFilter interactionFilter = new InteractionFilter( InteractionStateConstructionData.BattleFilters, InteractionStateConstructionData.FilterStateResults, new BattleEntity[0] );
			BattleManager.Get.OnApplyInteractionFilter( interactionFilter );
		}
		
		public override void OnLeaving()
		{
			//Remove the filter on the games interaction
			BattleManager.Get.OnRemoveInteractionFilter();	

			base.OnLeaving();
		}
		#endregion
	}
}