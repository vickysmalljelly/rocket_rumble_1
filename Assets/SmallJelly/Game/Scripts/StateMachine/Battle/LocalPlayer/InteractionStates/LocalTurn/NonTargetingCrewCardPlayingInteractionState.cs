﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class NonTargetingCrewCardPlayingInteractionState : TargetingInteractionState
	{
		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity state transitions
		public override void OnBattleEntityStateTransitioned( BattleEntity battleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityStateTransitioned( battleEntity, previousState, newState, hitLayers );

			//Check whether we're interested in this entity
			if( battleEntity != SourceBattleEntity )
			{
				return;
			}

			switch( newState )
			{
				//If the dragging was released
				case BattleEntityController.State.CardDraggingReleased:
				
					foreach( SJRenderRaycastHit renderRaycastHit in hitLayers )
					{
						GameObject hitGroup = renderRaycastHit.GroupGameObject;

						//If the card is released over the hud then let's return it to the hand!
						bool hitHud = hitGroup.GetComponentInParent< PlayerHUD >();
						if( hitHud )
						{
							//The HUD was hit! - return to hand
							SourceBattleEntity.OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );

							OnStateCompleted();
							return;
						}
					}

					//The HUD was not hit so let's play it!
					PlayCrewCard();
					OnStateCompleted();
					break;
			}
		}
		#endregion

		#region Private Methods
		private void PlayCrewCard()
		{
			BattleManager.Get.PlayCrewCard( SourceBattleEntity.Data.DrawNumber );
       	}
		#endregion
	}
}