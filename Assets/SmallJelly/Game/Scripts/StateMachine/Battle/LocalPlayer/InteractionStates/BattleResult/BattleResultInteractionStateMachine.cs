﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class BattleResultTurnInteractionStateMachine : StateMachine
	{
		#region Constants
		// Game states
		public const string BATTLE_RESULT_INTERACTION_STATE = "BATTLE_RESULT_INTERACTION_STATE"; 
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{			
			// Initialise states
			AddState< BattleResultInteractionState >( BATTLE_RESULT_INTERACTION_STATE );

			//Enter the first state
			OnEnterResultInteractionState();
			LateUpdate();
		}
		#endregion

		#region Private Methods
		private void OnEnterResultInteractionState()
		{
			//Show the HUD
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( false, false );

			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			//Disable interaction with all entities
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );

			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.Showing, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );

			StateConstructionData = new InteractionStateConstructionData( BattleResultTurnInteractionStateMachine.BATTLE_RESULT_INTERACTION_STATE, new BattleFilter[]{ jettisonButtonBattleFilter, hudBattleFilter, buttonBattleFilter, battleEntityOwnerBattleFilter }, new FilterStateResult[]{ jettisonButtonInteractionFilterResult, battleEntityInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult } );
		}
		#endregion
    }
}
