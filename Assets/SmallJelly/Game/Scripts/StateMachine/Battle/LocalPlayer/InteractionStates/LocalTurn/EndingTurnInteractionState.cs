﻿using SmallJelly.Framework;
using UnityEngine;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Controls the gameplay while in the play phase of the local player
	/// </summary>
	public sealed class EndingTurnInteractionState : InteractionState 
	{
		#region Member Variables
		private bool mSentMessage;
		#endregion

		#region MonoBehaviour Methods
		public void Update()
		{
			//We never tell this state to terminate because that would relinquish control back to another interaction state
			//something that we don't want to do because once this state has been hit it should never be left until the
			//state machine is deconstructed. If we were to leave the state once the server had finished processing then
			//we would run the risk that ScriptMessageManager.EndTurn had not been fired yet. This could leave us in a period of
			//limbo where we can interact even though the turn has ended. Instead we will wait for the server to deconstruct and thus call "OnLeaving"
			if( BattleManager.Get.AnimationsHaveFinished && !mSentMessage )
			{
				BattleManager.Get.EndTurn();
				mSentMessage = true;
			}
		}
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			InteractionFilter interactionFilter = new InteractionFilter( InteractionStateConstructionData.BattleFilters, InteractionStateConstructionData.FilterStateResults, new BattleEntity[0] );
			BattleManager.Get.OnApplyInteractionFilter( interactionFilter );

			RegisterListeners();
		}
		
		public override void OnLeaving()
		{
			UnregisterListeners();

			//Remove the filter on the games interaction
			BattleManager.Get.OnRemoveInteractionFilter();	

			base.OnLeaving();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.EndTurnSuccess += HandleEndTurnSuccess;
			BattleManager.Get.EndTurnError += HandleEndTurnError;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.EndTurnSuccess -= HandleEndTurnSuccess;
			BattleManager.Get.EndTurnError -= HandleEndTurnError;
		}
		#endregion

		#region Event Handlers
		private void HandleEndTurnSuccess( object o, EventArgs args )
		{}

		private void HandleEndTurnError( object o, EventArgs args )
		{
			mSentMessage = false;
		}
		#endregion
	}
}