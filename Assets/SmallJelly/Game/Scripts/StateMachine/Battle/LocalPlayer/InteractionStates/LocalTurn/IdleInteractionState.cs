﻿using SmallJelly.Framework;
using UnityEngine;
using System.Collections.Generic;

namespace SmallJelly
{
	/// <summary>
	/// Controls the gameplay while in the play phase of the local player
	/// </summary>
	public sealed class IdleInteractionState : InteractionState 
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();

			InteractionFilter interactionFilter = new InteractionFilter( InteractionStateConstructionData.BattleFilters, InteractionStateConstructionData.FilterStateResults, new BattleEntity[0] );
			BattleManager.Get.OnApplyInteractionFilter( interactionFilter );
		}
		
		public override void OnLeaving()
		{						
			//Remove the filter on the games interaction
			BattleManager.Get.OnRemoveInteractionFilter();	

			UnregisterListeners();

			base.OnLeaving();
		}

		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity sourceBattleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( sourceBattleEntity, previousState, newState, hitLayers );
		
			switch( newState ) 
			{
			case BattleEntityController.TargetableState.Targeting:

				SJLogger.AssertCondition (sourceBattleEntity.Data is WeaponData, "The battle data of draggable components must be weapon data");
				WeaponData weaponData = (WeaponData)sourceBattleEntity.Data;

					//Do not allow components with 0 or less attack to be targeted!
				if (weaponData.Attack <= 0) {
					sourceBattleEntity.OnReturnToPreviousState ();
					return;
				}

					// Interested in remote players ships
					ShipOwnerBattleFilter draggingShipOwnerBattleFilter = new ShipOwnerBattleFilter (false, true);

					// Interested in remote players battle entities
					BattleEntityOwnerBattleFilter draggingBattleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter (false, true);

					//Interested in neither players slots
					SocketOwnerBattleFilter draggingSocketOwnerBattleFilter = new SocketOwnerBattleFilter (false, true);

					SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter ( true, false );

					// Only interested in ships/battle entities which are alive (hp > 0)
					AliveBattleFilter aliveBattleFilter = new AliveBattleFilter();

					SocketTargetLockBattleFilter socketTargetLockBattleFilter = new SocketTargetLockBattleFilter();
					JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( true );
					EndTurnButtonBattleFilter draggingButtonBattleFilter = new EndTurnButtonBattleFilter( false );

					ShipInteractionFilterResult draggingShipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
					BattleEntityInteractionFilterResult draggingBattleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
					SocketInteractionFilterResult draggingSocketInteractionFilterResult = new SocketInteractionFilterResult( SocketController.TargetableState.Targetable, SocketController.TargetableState.NonTargetable );
					JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.GreyedOut );
					EndTurnButtonInteractionFilterResult draggingButtonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.GreyedOut );

					//Target given locations
					StateConstructionData componentDraggingStateConstructionData = new TargetingInteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.TARGETING_ATTACK_INTERACTION_STATE, new BattleFilter[] { jettisonButtonBattleFilter, draggingShipOwnerBattleFilter, socketOccupiedBattleFilter, draggingBattleEntityOwnerBattleFilter, draggingSocketOwnerBattleFilter, aliveBattleFilter, draggingButtonBattleFilter, socketTargetLockBattleFilter },new FilterStateResult[]{ jettisonButtonInteractionFilterResult, draggingShipInteractionFilterResult, draggingBattleEntityInteractionFilterResult, draggingSocketInteractionFilterResult, draggingButtonInteractionFilterResult }, sourceBattleEntity );
					OnStateCompleted( componentDraggingStateConstructionData );
					break;
			}
		}

		//(Could it be the “Add online” thing making the card hand tiny since it touches "parent"?)
		public override void OnBattleEntityStateTransitioned( BattleEntity sourceBattleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityStateTransitioned( sourceBattleEntity, previousState, newState, hitLayers );

			switch( newState ) 
			{
				case BattleEntityController.State.CardDragging:
					switch( sourceBattleEntity.Data.EntityType )
					{
						case BattleEntityType.Weapon:
						case BattleEntityType.Utility:
							TargetingComponentCardInstallation( sourceBattleEntity, previousState, newState, hitLayers );
						break;

						case BattleEntityType.TargetingCrew:
							TargetingCrewCardEffect( sourceBattleEntity, previousState, newState, hitLayers );
						break;

						case BattleEntityType.NonTargetingCrew:
							PlayingNonTargetingCrewCard( sourceBattleEntity, previousState, newState, hitLayers );
						break;

						//TODO - Have set all others up as a "Weapon" for now as things are still being implemented
						default:
							TargetingComponentCardInstallation( sourceBattleEntity, previousState, newState, hitLayers );
						break;
					}
				break;
			}
		}
		#endregion

		#region Private Methods
		private void EndTurn()
		{
			//Show neither button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityListBattleFilter = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

			StateConstructionData stateConstructionData = new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.ENDING_TURN_INTERACTION_STATE, new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, battleEntityOwnerBattleFilter, shipOwnerBattleFilter }, new FilterStateResult[]{ buttonInteractionFilterResult, socketInteractionFilterResult, battleEntityListBattleFilter, shipInteractionFilterResult, jettisonButtonInteractionFilterResult } );
			OnStateCompleted( stateConstructionData );
		}

		private void TargetingCrewCardEffect( BattleEntity sourceBattleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			SJLogger.AssertCondition( sourceBattleEntity.Data is TargetingCrewData, "The supplied card MUST be a targetable crew card" );

			bool targetLocalHull = false;
			bool targetRemoteHull = false;

			List< int > targetLocalSockets = new List< int >();
			List< int > targetRemoteSockets = new List< int >();

			SJLogger.AssertCondition( sourceBattleEntity.TargetingData.TargetData != null, "Played crew card doesn't have any data on what it is able target" );

			foreach( SJJson targetJson in sourceBattleEntity.TargetingData.TargetData )
			{
				bool isLocalTarget = targetJson.GetString( "playerId" ) == ChallengeManager.Get.LocalPlayer.UserId;
				string targetType = targetJson.GetString( "type" );

				switch( targetType )
				{
				case "hull":
					if( isLocalTarget )
					{
						targetLocalHull = true;
					}
					else
					{
						targetRemoteHull = true;
					}
					break;

				case "socket":
					int socketId = targetJson.GetInt ( "socketId" ).GetValueOrDefault();
					List< int > targetSockets = isLocalTarget ? targetLocalSockets : targetRemoteSockets;
					targetSockets.Add( socketId );
					break;
				}
			}

			// Interested in neither players ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( targetLocalHull, targetRemoteHull );

			//Enable relevant sockets and battle entities in those sockets
			SocketListBattleFilter socketListBattleFilter = new SocketListBattleFilter ( targetLocalSockets, targetRemoteSockets );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );

			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( SocketController.TargetableState.Targetable, SocketController.TargetableState.NonTargetable );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );

			StateConstructionData stateConstructionData = new TargetingInteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.TARGETING_CREW_CARD_TEXT_INTERACTION_STATE, new BattleFilter[] { shipOwnerBattleFilter, socketListBattleFilter, buttonBattleFilter, jettisonButtonBattleFilter }, new FilterStateResult[]{ shipInteractionFilterResult, battleEntityInteractionFilterResult, socketInteractionFilterResult, buttonInteractionFilterResult, jettisonButtonInteractionFilterResult }, sourceBattleEntity );
			OnStateCompleted( stateConstructionData );
		}

		private void TargetingComponentCardInstallation( BattleEntity sourceBattleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			// Interested in neither players ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			//Interested in neither players battle entities
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );

			//Interested in local players slots
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( true, false );
			SocketOccupiedBattleFilter socketOccupiedBattleFilter = new SocketOccupiedBattleFilter( false, true );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );

			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( SocketController.TargetableState.Targetable, SocketController.TargetableState.NonTargetable );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );

			StateConstructionData stateConstructionData = new TargetingInteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.TARGETING_COMPONENT_CARD_INSTALLATION_INTERACTION_STATE, new BattleFilter[] { jettisonButtonBattleFilter, shipOwnerBattleFilter, battleEntityOwnerBattleFilter, socketOwnerBattleFilter, buttonBattleFilter, socketOccupiedBattleFilter }, new FilterStateResult[]{ shipInteractionFilterResult, battleEntityInteractionFilterResult, socketInteractionFilterResult, buttonInteractionFilterResult, jettisonButtonInteractionFilterResult }, sourceBattleEntity );
			OnStateCompleted( stateConstructionData );
		}

		private void PlayingNonTargetingCrewCard( BattleEntity sourceBattleEntity, BattleEntityController.State previousState, BattleEntityController.State newState, SJRenderRaycastHit[] hitLayers )
		{
			//Interested in neither players ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

			//Interested in neither players battle entities
			BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );

			//Interested in neither players slots
			SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );

			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( SocketController.TargetableState.Targetable, SocketController.TargetableState.NonTargetable );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );

			StateConstructionData stateConstructionData = new TargetingInteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.PLAYING_NON_TARGETING_CREW_CARD_INTERACTION_STATE, new BattleFilter[] { shipOwnerBattleFilter, battleEntityOwnerBattleFilter, socketOwnerBattleFilter, buttonBattleFilter, jettisonButtonBattleFilter }, new FilterStateResult[]{ jettisonButtonInteractionFilterResult, shipInteractionFilterResult, battleEntityInteractionFilterResult, socketInteractionFilterResult, buttonInteractionFilterResult }, sourceBattleEntity );
			OnStateCompleted( stateConstructionData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			BattleManager.Get.BattleGameplayController.EndTurnClicked += HandleEndTurn;
		}

		private void UnregisterListeners()
		{
			BattleManager.Get.BattleGameplayController.EndTurnClicked -= HandleEndTurn;
		}
		#endregion


		#region Event Handlers
		private void HandleEndTurn()
		{
			EndTurn();
		}

		private void HandleTestSuccess()
		{
			// Wait for turn taken event
		}
		
		private void HandleTestError(ServerError error)
		{
			SJLogger.LogError("Test funtionality failed: {0}", error.TechnicalDetails);
		}

		private void HandleSlotTargetingSuccess()
		{
		}

		private void HandleSlotTargetingError( ServerError error )
		{			
			SJLogger.LogError("Failed to target slot: {0}", error.TechnicalDetails);
		}
		#endregion
	}
}