﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class TargetingComponentInstallationCardTextInteractionStateConstructionData : TargetingInteractionStateConstructionData 
	{
		#region Public Properties
		public int InstallationSocketId 
		{ 
			get
			{
				return mInstallationSocketId;
			}
		}
		#endregion

		#region Member Variables
		private readonly int mInstallationSocketId;
		#endregion

		#region Constructor
		public TargetingComponentInstallationCardTextInteractionStateConstructionData( string stateId, BattleFilter[] battleFilters, FilterStateResult[] filterStateResults, BattleEntity sourceBattleEntity, int installationSocketId ) : base( stateId, battleFilters, filterStateResults, sourceBattleEntity )
		{
			mInstallationSocketId = installationSocketId;
		}
		#endregion
	}
}