﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public sealed class RemotePlayerTurnInteractionStateMachine : StateMachine
	{
		#region Constants
		// Game states
		public const string BATTLE_REMOTE_PLAYER_INTERACTION_STATE = "BATTLE_REMOTE_PLAYER_INTERACTION_STATE"; 
		#endregion

		#region MonoBehaviour Methods
		private void OnEnable()
		{			
			// Initialise states
			AddState< RemotePlayerInteractionState >( BATTLE_REMOTE_PLAYER_INTERACTION_STATE );

			//Enter the first state
			OnEnterRemotePlayerInteractionState();
			LateUpdate();
		}
		#endregion

		#region Private Methods
		private void OnEnterRemotePlayerInteractionState()
		{
			//Show the HUD
			HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

			//Show end turn button but not jettison button
			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( false );

			HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.Showing, PlayerHUDController.State.Hiding );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );

			StateConstructionData stateConstructionData = new InteractionStateConstructionData( RemotePlayerTurnInteractionStateMachine.BATTLE_REMOTE_PLAYER_INTERACTION_STATE, new BattleFilter[]{ hudBattleFilter, buttonBattleFilter, jettisonButtonBattleFilter }, new FilterStateResult[]{ jettisonButtonInteractionFilterResult, hudInteractionFilterResult, buttonInteractionFilterResult } );
	
			BattleOnboardingManager.Get.OnRemotePlayerInteractionStateFinished( default( State ), stateConstructionData );

			StateConstructionData = stateConstructionData;
		}
		#endregion
    }
}
