﻿using SmallJelly.Framework;

namespace SmallJelly
{
	public class LocalPlayerTurnInteractionStateMachine : StateMachine
	{
		#region Constants
		// Game states
		public const string BATTLE_IDLE_INTERACTION_STATE = "BATTLE_IDLE"; 
		public const string TARGETING_COMPONENT_CARD_INSTALLATION_INTERACTION_STATE = "TARGETINGCOMPONENTCARDINSTALLATION"; 
		public const string TARGETING_CREW_CARD_TEXT_INTERACTION_STATE = "TARGETINGCREWCARDTEXT"; 
		public const string TARGETING_COMPONENT_CARD_TEXT_INTERACTION_STATE = "TARGETINGCOMPONENTCARDTEXT"; 

		public const string PLAYING_NON_TARGETING_CREW_CARD_INTERACTION_STATE = "PLAYINGNONTARGETINGCREWCARD"; 
		public const string BATTLE_SERVER_PROCESSING_INTERACTION_STATE = "SERVERPROCESSING";
		public const string TARGETING_ATTACK_INTERACTION_STATE = "TARGETINGATTACK"; 
		public const string ENDING_TURN_INTERACTION_STATE = "ENDINGTURN";
		#endregion

		#region Member Variables
		private IdleInteractionState mIdleInteractionState;
		private AttackTargetingInteractionState mAttackTargetingInteractionState;
		private InstallationTargetingInteractionState mInstallationTargetingInteractionState;

		private CrewCardTextTargetingInteractionState mCrewCardTextTargetingInteractionState;
		private ComponentInstallationCardTextTargetingInteractionState mComponentCardTextTargetingInteractionState;

		private NonTargetingCrewCardPlayingInteractionState mNonTargetingCrewCardPlayingInteractionState;

		private ServerProcessingInteractionState mServerProcessingInteractionState;

		private EndingTurnInteractionState mEndingTurnInteractionState;
		#endregion

		#region MonoBehaviour Methods
		protected virtual void OnEnable()
		{			
			// Initialise states
			mIdleInteractionState = AddState< IdleInteractionState >( BATTLE_IDLE_INTERACTION_STATE );
			mAttackTargetingInteractionState = AddState< AttackTargetingInteractionState >( TARGETING_ATTACK_INTERACTION_STATE );
			mInstallationTargetingInteractionState = AddState< InstallationTargetingInteractionState >( TARGETING_COMPONENT_CARD_INSTALLATION_INTERACTION_STATE );
			mCrewCardTextTargetingInteractionState = AddState< CrewCardTextTargetingInteractionState >( TARGETING_CREW_CARD_TEXT_INTERACTION_STATE );
			mComponentCardTextTargetingInteractionState = AddState< ComponentInstallationCardTextTargetingInteractionState >( TARGETING_COMPONENT_CARD_TEXT_INTERACTION_STATE );
			mNonTargetingCrewCardPlayingInteractionState = AddState< NonTargetingCrewCardPlayingInteractionState >( PLAYING_NON_TARGETING_CREW_CARD_INTERACTION_STATE );

			mServerProcessingInteractionState = AddState< ServerProcessingInteractionState >( BATTLE_SERVER_PROCESSING_INTERACTION_STATE );
			mEndingTurnInteractionState = AddState< EndingTurnInteractionState >( ENDING_TURN_INTERACTION_STATE );

			// Initialise transitions
			AddTransition( mIdleInteractionState.Id, mIdleInteractionState.Id );
			AddTransition( mIdleInteractionState.Id, mAttackTargetingInteractionState.Id );
			AddTransition( mIdleInteractionState.Id, mInstallationTargetingInteractionState.Id );
			AddTransition( mIdleInteractionState.Id, mCrewCardTextTargetingInteractionState.Id );
			AddTransition( mIdleInteractionState.Id, mNonTargetingCrewCardPlayingInteractionState.Id );
			AddTransition( mIdleInteractionState.Id, mEndingTurnInteractionState.Id );

			AddTransition( mInstallationTargetingInteractionState.Id, mComponentCardTextTargetingInteractionState.Id );

			AddTransition( mInstallationTargetingInteractionState.Id, mServerProcessingInteractionState.Id );
			AddTransition( mAttackTargetingInteractionState.Id, mServerProcessingInteractionState.Id );
			AddTransition( mCrewCardTextTargetingInteractionState.Id, mServerProcessingInteractionState.Id );
			AddTransition( mNonTargetingCrewCardPlayingInteractionState.Id, mServerProcessingInteractionState.Id );
			AddTransition( mComponentCardTextTargetingInteractionState.Id, mServerProcessingInteractionState.Id );
			AddTransition( mEndingTurnInteractionState.Id, mServerProcessingInteractionState.Id );

			AddTransition( mInstallationTargetingInteractionState.Id, mIdleInteractionState.Id );
			AddTransition( mAttackTargetingInteractionState.Id, mIdleInteractionState.Id );
			AddTransition( mCrewCardTextTargetingInteractionState.Id, mIdleInteractionState.Id );
			AddTransition( mNonTargetingCrewCardPlayingInteractionState.Id, mIdleInteractionState.Id );
			AddTransition( mComponentCardTextTargetingInteractionState.Id, mIdleInteractionState.Id );

			AddTransition( mServerProcessingInteractionState.Id, mIdleInteractionState.Id );

			//Pretend an empty state has finished to allow the state machine to enter the first state via
			//the usual code path
			HandleInteractionStateFinished( null, new EmptyStateConstructionData() );

			LateUpdate();

			RegisterListeners();
        }


		protected virtual void OnDisable()
		{
			UnregisterListeners();
		}
		#endregion

		#region Protected Methods
		protected InteractionStateConstructionData GetNextState( State state, StateConstructionData stateConstructionData )
		{
			if( stateConstructionData is EmptyStateConstructionData )
			{
				if( ClientBattle.ServerIsProcessingGameplay() )
				{
					//Show end turn button but not jettison button
					JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
					EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );
					SocketOwnerBattleFilter socketOwnerBattleFilter = new SocketOwnerBattleFilter( false, false );
					BattleEntityOwnerBattleFilter battleEntityOwnerBattleFilter = new BattleEntityOwnerBattleFilter( false, false );
					ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( false, false );

					JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
					EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
					SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
					BattleEntityInteractionFilterResult battleEntityListBattleFilter = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
					ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );

					return new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_SERVER_PROCESSING_INTERACTION_STATE, new BattleFilter[]{ jettisonButtonBattleFilter, buttonBattleFilter, socketOwnerBattleFilter, battleEntityOwnerBattleFilter, shipOwnerBattleFilter }, new FilterStateResult[]{ jettisonButtonInteractionFilterResult, buttonInteractionFilterResult, socketInteractionFilterResult, battleEntityListBattleFilter, shipInteractionFilterResult } );
				}
				else
				{
					//Show the HUD
					HUDBattleFilter hudBattleFilter = new HUDBattleFilter( true, true );

					//Show end turn button but not jettison button
					EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );

					HUDInteractionFilterResult hudInteractionFilterResult = new HUDInteractionFilterResult( PlayerHUDController.State.Showing, PlayerHUDController.State.Hiding );
					EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.Showing, RenderButtonController.ButtonState.Hiding );

					return new InteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.BATTLE_IDLE_INTERACTION_STATE, new BattleFilter[]{ hudBattleFilter, buttonBattleFilter }, new FilterStateResult[]{ hudInteractionFilterResult, buttonInteractionFilterResult } );
				}
			}
			else
			{
				return (InteractionStateConstructionData)stateConstructionData;
			}
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mIdleInteractionState.StateCompleted += HandleInteractionStateFinished;

			mAttackTargetingInteractionState.StateCompleted += HandleInteractionStateFinished;
			mInstallationTargetingInteractionState.StateCompleted += HandleInteractionStateFinished;
			mCrewCardTextTargetingInteractionState.StateCompleted += HandleInteractionStateFinished;
			mNonTargetingCrewCardPlayingInteractionState.StateCompleted += HandleInteractionStateFinished;
			mComponentCardTextTargetingInteractionState.StateCompleted += HandleInteractionStateFinished;
			mServerProcessingInteractionState.StateCompleted += HandleInteractionStateFinished;

			//We treat a requested interaction state change as if it were another state terminating with some next state in mind
			BattleOnboardingManager.Get.RequestInteractionStateChange += HandleInteractionStateFinished;
		}

		private void UnregisterListeners()
		{
			mIdleInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mAttackTargetingInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mInstallationTargetingInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mCrewCardTextTargetingInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mNonTargetingCrewCardPlayingInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mComponentCardTextTargetingInteractionState.StateCompleted -= HandleInteractionStateFinished;
			mServerProcessingInteractionState.StateCompleted -= HandleInteractionStateFinished;

			//We treat a requested interaction state change as if it were another state terminating with some next state in mind
			BattleOnboardingManager.Get.RequestInteractionStateChange -= HandleInteractionStateFinished;
		}
		#endregion


		#region Event Handlers
		protected virtual void HandleInteractionStateFinished ( State state, StateConstructionData stateConstructionData )
		{
			//Let the state machine make any changes to the state construction data that it wants to!
			stateConstructionData = GetNextState( state, stateConstructionData );

			BattleOnboardingManager.Get.OnLocalPlayerInteractionStateFinished( state, stateConstructionData );

			StateConstructionData = stateConstructionData;
		}

		private void HandleBattleEntityStateTransitioned( object o, BattleEntityAndStateTransitionEventArgs battleEntityAndStateTransitionEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is InteractionState, "State must be an interaction state");
			((InteractionState)CurrentState).OnBattleEntityStateTransitioned( battleEntityAndStateTransitionEventArgs.BattleEntity, battleEntityAndStateTransitionEventArgs.PreviousState, battleEntityAndStateTransitionEventArgs.NewState, battleEntityAndStateTransitionEventArgs.EntityHitLayers );
		}

		private void HandleBattleEntityTargetableStateTransitioned( object o, BattleEntityAndTargetableStateTransitionEventArgs battleEntityAndTargetableStateTransitionEventArgs )
		{
			SJLogger.AssertCondition( CurrentState is InteractionState, "State must be an interaction state");
			((InteractionState)CurrentState).OnBattleEntityTargetableStateTransitioned( battleEntityAndTargetableStateTransitionEventArgs.BattleEntity, battleEntityAndTargetableStateTransitionEventArgs.PreviousTargetableState, battleEntityAndTargetableStateTransitionEventArgs.NewTargetableState, battleEntityAndTargetableStateTransitionEventArgs.EntityHitLayers );
		}
		#endregion
    }
}
