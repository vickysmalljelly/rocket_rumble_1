﻿using SmallJelly.Framework;
using System;
using System.Collections.Generic;

namespace SmallJelly
{
    /// <summary>
    /// GEORGE TODO
    /// </summary>
	public abstract class TargetingInteractionState : InteractionState
	{
		#region Protected Properties
		protected BattleEntity SourceBattleEntity
		{
			get
			{
				return mSourceBattleEntity;
			}
		}

		protected InteractionFilter InteractionFilter
		{
			get
			{
				return mInteractionFilter;
			}
		}
		#endregion

		#region Member Variables
		private InteractionFilter mInteractionFilter;
		private BattleEntity mSourceBattleEntity;
		#endregion

		#region Public Methods
        public override void OnConstruct( StateConstructionData messageData )
		{
			base.OnConstruct( messageData );

			SJLogger.AssertCondition( messageData is TargetingInteractionStateConstructionData, "Targeting state should always be constructed by data of type \"TargetingInteractionStateConstructionData\"");

			mSourceBattleEntity = ((TargetingInteractionStateConstructionData)messageData).SourceBattleEntity;
		}

		public override void OnEntering()
		{
			base.OnEntering();

			mInteractionFilter = new InteractionFilter( InteractionStateConstructionData.BattleFilters, InteractionStateConstructionData.FilterStateResults, new BattleEntity[]{ SourceBattleEntity } );
			BattleManager.Get.OnApplyInteractionFilter( mInteractionFilter );

			//This source entity is being targeted
			mSourceBattleEntity.OnInsertTargetableState( TargetableLogicController.TargetableState.Targeting );

		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			//Remove the filter on the games interaction
			BattleManager.Get.OnRemoveInteractionFilter();	

			//The source entity is no longer being targeted
			mSourceBattleEntity.OnInsertTargetableState( TargetableLogicController.TargetableState.Targeted );
			mSourceBattleEntity.OnInsertTargetableState( TargetableLogicController.TargetableState.None );

			mSourceBattleEntity = null;
		}
		#endregion
	}
}