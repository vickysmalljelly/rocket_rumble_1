﻿using SmallJelly.Framework;
using System;
using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace SmallJelly
{
    /// <summary>
    /// GEORGE TODO
    /// </summary>
	public class InstallationTargetingInteractionState : TargetingInteractionState
	{
		#region Constants
		private const string SOCKET_KEY = "Socket";
		#endregion

		#region Member Variables
		private Socket mSelectedSocket;
		#endregion

		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}
		
		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Private Methods
		private void StartPlayingInstallationAnimation()
		{
			SJLogger.LogMessage( MessageFilter.George, "StartPlayingInstallationAnimation " + SourceBattleEntity.gameObject );
			SourceBattleEntity.OnInsertState( BattleEntityController.State.ComponentMoveToSocket, GetAnimationMetaData() );
			SJLogger.LogMessage( MessageFilter.George, "Register Listeners" );

			RegisterBattleEntityListeners();
		}

		private void FinishedPlayingInstallationAnimation()
		{
			UnregisterBattleEntityListeners();

			PlayerType owner;
			int socketId;
			BattleManager.Get.GetSocketInformation( mSelectedSocket, out owner, out socketId );

			SJLogger.LogMessage( MessageFilter.George, "FinishedPlayingInstallationAnimation " + SourceBattleEntity.gameObject );

			//If the weapon card text has potential targets
			if( SourceBattleEntity.TargetingData.TargetData != null && SourceBattleEntity.TargetingData.TargetData.Count > 0 )
			{
				//Has some target data
				StartTargetingCardText( SourceBattleEntity, socketId );
			}
			else
			{
				StartInstallingBattleEntity( SourceBattleEntity, socketId ); 
			}
		}

		private void StartTargetingCardText( BattleEntity targetableBattleEntity, int socketId )
		{
			TargetCardText( targetableBattleEntity, socketId );
		}

		private void StartInstallingBattleEntity( BattleEntity targetableBattleEntity, int socketId )
		{
			SJLogger.LogMessage( MessageFilter.Battle, "Attempting to install {0} - {1}", targetableBattleEntity.Data.Id, targetableBattleEntity.Data.DisplayName );
			SJLogger.LogMessage( MessageFilter.George, "TTT Starting to install component" );

			BattleManager.Get.InstallComponent( targetableBattleEntity.Data.DrawNumber, socketId );
			OnStateCompleted();
		}

		protected void TargetCardText( BattleEntity sourceBattleEntity, int sourceSocketId )
		{
			bool targetLocalHull = false;
			bool targetRemoteHull = false;

			List< int > targetLocalSockets = new List< int >();
			List< int > targetRemoteSockets = new List< int >();

			BattleEntityTargetingData battleEntityTargetingData = (BattleEntityTargetingData)sourceBattleEntity.TargetingData;

			foreach( SJJson targetJson in battleEntityTargetingData.TargetData )
			{
				bool isLocalTarget = targetJson.GetString( "playerId" ) == ChallengeManager.Get.LocalPlayer.UserId;
				string targetType = targetJson.GetString( "type" );

				switch( targetType )
				{
				case "hull":
					if( isLocalTarget )
					{
						targetLocalHull = true;
					}
					else
					{
						targetRemoteHull = true;
					}
					break;

				case "socket":
					int socketId = targetJson.GetInt ( "socketId" ).GetValueOrDefault();

					List< int > targetSockets = isLocalTarget ? targetLocalSockets : targetRemoteSockets;
					targetSockets.Add( socketId );
					break;
				}
			}

			// Interested in neither players ships
			ShipOwnerBattleFilter shipOwnerBattleFilter = new ShipOwnerBattleFilter( targetLocalHull, targetRemoteHull );

			//Enable relevant sockets and battle entities in those sockets
			SocketListBattleFilter socketListBattleFilter = new SocketListBattleFilter ( targetLocalSockets, targetRemoteSockets );

			JettisonButtonBattleFilter jettisonButtonBattleFilter = new JettisonButtonBattleFilter( false );
			EndTurnButtonBattleFilter buttonBattleFilter = new EndTurnButtonBattleFilter( true );

			ShipInteractionFilterResult shipInteractionFilterResult = new ShipInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			BattleEntityInteractionFilterResult battleEntityInteractionFilterResult = new BattleEntityInteractionFilterResult( TargetableLogicController.TargetableState.Targetable, TargetableLogicController.TargetableState.NonTargetable );
			SocketInteractionFilterResult socketInteractionFilterResult = new SocketInteractionFilterResult( SocketController.TargetableState.Targetable, SocketController.TargetableState.NonTargetable );
			JettisonButtonInteractionFilterResult jettisonButtonInteractionFilterResult = new JettisonButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );
			EndTurnButtonInteractionFilterResult buttonInteractionFilterResult = new EndTurnButtonInteractionFilterResult( RenderButtonController.ButtonState.GreyedOut, RenderButtonController.ButtonState.Hiding );

			TargetingComponentInstallationCardTextInteractionStateConstructionData stateConstructionData = new TargetingComponentInstallationCardTextInteractionStateConstructionData( LocalPlayerTurnInteractionStateMachine.TARGETING_COMPONENT_CARD_TEXT_INTERACTION_STATE, new BattleFilter[] { shipOwnerBattleFilter, socketListBattleFilter, buttonBattleFilter, jettisonButtonBattleFilter }, new FilterStateResult[]{ shipInteractionFilterResult, battleEntityInteractionFilterResult, socketInteractionFilterResult, buttonInteractionFilterResult, jettisonButtonInteractionFilterResult }, sourceBattleEntity, sourceSocketId );
			OnStateCompleted( stateConstructionData );
		}

		private AnimationMetaData GetAnimationMetaData()
		{
			Dictionary< string, NamedVariable > variables = new Dictionary< string, NamedVariable >();
			variables.Add( SOCKET_KEY, new FsmGameObject( mSelectedSocket.gameObject ) );

			return new AnimationMetaData( variables );
		}

		#endregion


		#region Protected Methods
		//Allows the state to react in response to battle entity targetable state transitions
		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( battleEntity, previousState, newState, hitLayers );

			//Check whether we're interested in this entity
			if( battleEntity != SourceBattleEntity )
			{
				return;
			}

			switch( previousState )
			{
				//If the dragging was released
				case BattleEntityController.TargetableState.Targeting:

					Targetable targetable = default( Targetable );
					foreach( SJRenderRaycastHit renderRaycastHit in hitLayers )
					{
						//If the target is another entity then let us target it!
						targetable = renderRaycastHit.GroupGameObject.GetComponent< Targetable >();

						//If is a targetable object and it is a targetable state
						if( targetable && targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
						{
							if( targetable is Socket )
							{
								mSelectedSocket = (Socket) targetable;

							SJLogger.LogMessage( MessageFilter.George, "Weyo time to start playing installation animation " + SourceBattleEntity.gameObject );

								//Install the component
								StartPlayingInstallationAnimation();

								return;
							}
						}
					}

				SJLogger.LogMessage( MessageFilter.George, "Weyo time to start returning to hand " + SourceBattleEntity.gameObject );

					//No slot was found to drop in - return to hand
					SourceBattleEntity.OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );
					OnStateCompleted();

					break;
			}
		}
		#endregion

		#region Event Listeners
		private void RegisterBattleEntityListeners()
		{
			SourceBattleEntity.AnimationFinished += HandleBattleEntityAnimationFinished;
		}

		private void UnregisterBattleEntityListeners()
		{
			SourceBattleEntity.AnimationFinished -= HandleBattleEntityAnimationFinished;
		}
		#endregion

		#region Event Handlers
		private void HandleBattleEntityAnimationFinished( object o, BattleEntityEventArgs battleEntityEventArgs )
		{
			FinishedPlayingInstallationAnimation();
		}

		private void HandleInstallComponentSuccess()
		{
			// Wait for turn taken event
		}
            
        private void HandleServerError(ServerError error)
        {
            if(error.ErrorType == ServerErrorType.Timeout)
            {
                SJLogger.LogError("TODO: Handle timeout");
            }
        }
		#endregion
	}
}