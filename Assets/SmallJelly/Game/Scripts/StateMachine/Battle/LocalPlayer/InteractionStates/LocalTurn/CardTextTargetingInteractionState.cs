﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public abstract class CardTextTargetingInteractionState : TargetingInteractionState
	{
		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity targetable state transitions
		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( battleEntity, previousState, newState, hitLayers );
		}
		#endregion

		#region Protected Methods
		protected bool FindTarget( SJRenderRaycastHit[] hitLayers  )
		{
			Targetable targetable = default( Targetable );

			foreach( SJRenderRaycastHit renderRaycastHit in hitLayers )
			{
				//If the target is another entity then let us target it!
				targetable = renderRaycastHit.GroupGameObject.GetComponent<Targetable>();

				if(targetable)
				{
					if( targetable is Ship )
					{
						//Fire attack
						if( targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
						{
							TargetShip( SourceBattleEntity, (Ship)targetable );
							return true;
						}
						return false;
					}

					if( targetable is Socket )
					{
						//Fire attack
						if( targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
						{
							TargetSocket( SourceBattleEntity, (Socket)targetable );
							return true;
						}
						return false;
					}
				}
			}

			return false;
		}

		protected abstract void TargetShip( BattleEntity sourceBattleEntity, Ship targetableShip );
		protected abstract void TargetSocket( BattleEntity sourceBattleEntity, Socket targetableSocket );
		#endregion
	}
}