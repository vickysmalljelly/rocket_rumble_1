﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class CrewCardTextTargetingInteractionState : CardTextTargetingInteractionState
	{
		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );
		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity targetable state transitions
		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( battleEntity, previousState, newState, hitLayers );

			//Check whether we're interested in this entity
			if( battleEntity != SourceBattleEntity )
			{
				return;
			}

			switch( previousState )
			{
				//If the dragging was released
				case BattleEntityController.TargetableState.Targeting:

					//If the card is released over the hud then let's return it to the hand!
					foreach( SJRenderRaycastHit renderRaycastHit in hitLayers )
					{
						GameObject hitGroup = renderRaycastHit.GroupGameObject;

						bool hitHud = hitGroup.GetComponentInParent< PlayerHUD >();
						if( hitHud )
						{
							SourceBattleEntity.OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );
							OnStateCompleted();
							return;
						}
					}

					//Check if the card was released over a 'real' target!
					bool hitTarget = FindTarget( hitLayers );
					if( !hitTarget )
					{
						//Nothing of interest was hit - return to hand
						SourceBattleEntity.OnInsertState( BattleEntityController.State.CardReadyToReturnToHand );
					}

					OnStateCompleted();
					break;
			}
		}
		#endregion

		#region Protected Methods
		protected override void TargetShip( BattleEntity sourceBattleEntity, Ship targetableShip )
		{
			PlayerType owner;
			BattleManager.Get.GetShipInformation( targetableShip, out owner );

            TargetData target = new TargetData();
            target.PlayerId = (owner == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId);
            target.TypeOfTarget = TargetData.TargetType.Hull;

			BattleManager.Get.PlayCrewCardWithTarget( sourceBattleEntity.Data.DrawNumber, target );
		}

		protected override void TargetSocket( BattleEntity sourceBattleEntity, Socket targetableSocket )
		{
			PlayerType owner;
			int slotIndex;
			BattleManager.Get.GetSocketInformation( targetableSocket, out owner, out slotIndex );

            TargetData target = new TargetData();
            target.PlayerId = (owner == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId);
            target.TypeOfTarget = TargetData.TargetType.Socket;
            target.SocketId = slotIndex;

			BattleManager.Get.PlayCrewCardWithTarget( sourceBattleEntity.Data.DrawNumber, target );
		}
		#endregion

		#region Event Handlers
		private void HandleSlotTargetingSuccess()
		{
		}

		private void HandleHullTargetingSuccess()
		{
		}

        private void HandleServerError(ServerError error)
        {
            if(error.ErrorType == ServerErrorType.Timeout)
            {
                SJLogger.LogError("TODO: Handle timeout");
            }
        }
		#endregion
	}
}