﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;

namespace SmallJelly
{
	public class AttackTargetingInteractionState : TargetingInteractionState
	{
		#region Public Methods
        public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

		}

		public override void OnEntering()
		{
			base.OnEntering();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		//Allows the state to react in response to battle entity targetable state transitions
		public override void OnBattleEntityTargetableStateTransitioned( BattleEntity battleEntity, BattleEntityController.TargetableState previousState, BattleEntityController.TargetableState newState, SJRenderRaycastHit[] hitLayers )
		{
			base.OnBattleEntityTargetableStateTransitioned( battleEntity, previousState, newState, hitLayers );

			//Check whether we're interested in this entity
			if( battleEntity != SourceBattleEntity )
			{
				return;
			}

			switch( previousState )
			{
				//If the dragging was released
				case BattleEntityController.TargetableState.Targeting:

					Targetable targetable = default( Targetable );
					foreach( SJRenderRaycastHit renderRaycastHit in hitLayers )
					{
						//If the target is another entity then let us target it!
						targetable = renderRaycastHit.GroupGameObject.GetComponent<Targetable>();

						if( targetable )
						{
							if( targetable is Socket )
							{
								if( targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
								{
									//Fire attack
									TargetSocket( (Socket)targetable, renderRaycastHit );
								}
								break;
							}

							if( targetable is Ship )
							{
								if( targetable.TargetableState == TargetableLogicController.TargetableState.Targetable )
								{
									//Fire attack
									TargetShip( (Ship)targetable, renderRaycastHit );
								}
								break;
							}
						}
					}

					//Whether or not the attack has been targeted it's the end of the state
					OnStateCompleted();

					break;
			}
		}
		#endregion

		#region Event Handlers
		private void TargetSocket( Socket targetableSocket, SJRenderRaycastHit renderRaycastHitLayer )
		{
			SourceBattleEntity.OnInsertState( BattleEntityController.State.ComponentPrefire );

			PlayerType owner;
			int targetSlotIndex;
			BattleManager.Get.GetSocketInformation( targetableSocket, out owner, out targetSlotIndex );

			TargetData targetData = new TargetData();

			targetData.TypeOfTarget = TargetData.TargetType.Socket;
			targetData.PlayerId = owner == PlayerType.Local ? ChallengeManager.Get.LocalPlayer.UserId : ChallengeManager.Get.RemotePlayerId;
			targetData.SocketId = targetSlotIndex;

			PlayerType sourceOwner;
			bool installed;
			int sourceSlotIndex;
			BattleManager.Get.GetBattleEntityInformation( SourceBattleEntity, out sourceOwner, out installed, out sourceSlotIndex );

			BattleManager.Get.FireComponentAtComponent( sourceSlotIndex, targetSlotIndex, Vector3.zero );
		}

		private void TargetShip( Ship targetableShip, SJRenderRaycastHit renderRaycastHitLayer )
		{

			SourceBattleEntity.OnInsertState( BattleEntityController.State.ComponentPrefire );

			PlayerType sourceOwner;
			bool installed;
			int sourceSlotIndex;
			BattleManager.Get.GetBattleEntityInformation( SourceBattleEntity, out sourceOwner, out installed, out sourceSlotIndex );

			Transform modelContainer = targetableShip.transform;
			RaycastHit raycastHit;
			if( !RaycastExtensions.RaycastChildren( renderRaycastHitLayer.Ray, modelContainer, out raycastHit, Mathf.Infinity ) )
			{
				if( !RaycastExtensions.RaycastChildren( renderRaycastHitLayer.Ray, modelContainer, out raycastHit, Mathf.Infinity ) )
				{
					SJLogger.LogWarning("The raycast didn't hit any points on the components mesh - this is potentially erroneous!");
				}
			}

			Vector3 hitPosition = default( Vector3 );
			if( raycastHit.point != default( Vector3 ) )
			{
				//Transform hit point into local space!
				hitPosition = modelContainer.InverseTransformPoint( raycastHit.point );
			}
				

			BattleManager.Get.FireComponentAtHull( sourceSlotIndex, hitPosition );
		}
		#endregion
	}
}