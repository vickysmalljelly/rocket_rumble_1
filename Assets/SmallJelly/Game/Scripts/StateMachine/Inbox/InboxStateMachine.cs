using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public class InboxStateMachine : StateMachine
	{
		#region Event Handlers
		public event EventHandler ReadyToTransitionToRumble
		{
			add
			{
				mInitializationInboxState.ReadyToTransitionToRumble += value;
				mEnterInboxState.ReadyToTransitionToRumble += value;
				mDownloadingInboxListState.ReadyToTransitionToRumble += value;
				mIdleInboxState.ReadyToTransitionToRumble += value;
			}

			remove
			{
				mInitializationInboxState.ReadyToTransitionToRumble -= value;
				mEnterInboxState.ReadyToTransitionToRumble -= value;
				mDownloadingInboxListState.ReadyToTransitionToRumble -= value;
				mIdleInboxState.ReadyToTransitionToRumble -= value;
			}
		}

		public event EventHandler ReadyToTransitionToShop
		{
			add
			{
				mInitializationInboxState.ReadyToTransitionToShop += value;
				mEnterInboxState.ReadyToTransitionToShop += value;
				mDownloadingInboxListState.ReadyToTransitionToShop += value;
				mIdleInboxState.ReadyToTransitionToShop += value;
			}

			remove
			{
				mInitializationInboxState.ReadyToTransitionToShop -= value;
				mEnterInboxState.ReadyToTransitionToShop -= value;
				mDownloadingInboxListState.ReadyToTransitionToShop -= value;
				mIdleInboxState.ReadyToTransitionToShop -= value;
			}
		}

		public event EventHandler ReadyToTransitionToCollection
		{
			add
			{
				mInitializationInboxState.ReadToTransitionToCollection += value;
				mEnterInboxState.ReadToTransitionToCollection += value;
				mDownloadingInboxListState.ReadToTransitionToCollection += value;
				mIdleInboxState.ReadToTransitionToCollection += value;
			}

			remove
			{
				mInitializationInboxState.ReadToTransitionToCollection -= value;
				mEnterInboxState.ReadToTransitionToCollection -= value;
				mDownloadingInboxListState.ReadToTransitionToCollection -= value;
				mIdleInboxState.ReadToTransitionToCollection -= value;
			}
		}
		#endregion

		#region Constants
		public const string INBOX_STATE_INITIALIZATION = "INBOX_STATE_INITIALIZATION";
		public const string INBOX_STATE_ENTER = "INBOX_STATE_ENTER";
		public const string INBOX_STATE_DOWNLOADING_LIST = "INBOX_STATE_DOWNLOADING_LIST";
		public const string INBOX_STATE_IDLE = "INBOX_STATE_IDLE";
		public const string INBOX_STATE_DOWNLOADING_CONTENT = "INBOX_STATE_DOWNLOADING_CONTENT";
		public const string INBOX_STATE_OPENING_PACK = "INBOX_STATE_OPENING_PACK";
		public const string INBOX_STATE_COMPLETE_OPEN_PACK = "INBOX_STATE_COMPLETE_OPEN_PACK";
		#endregion

		#region Member Variables
		private InitializationInboxState mInitializationInboxState;
		private EnterInboxState mEnterInboxState;
		private DownloadingListInboxState mDownloadingInboxListState;
		private IdleInboxState mIdleInboxState;
		private DownloadingContentInboxState mDownloadingContentInboxState;
		private OpeningPackInboxState mOpeningPackInboxState;
		private CompleteOpenPackInboxState mCompleteOpenPackInboxState;
		#endregion

		#region MonoBehaviour
		protected override void Awake()
		{
			base.Awake();

			mInitializationInboxState = AddState< InitializationInboxState >( INBOX_STATE_INITIALIZATION );
			mEnterInboxState = AddState< EnterInboxState >( INBOX_STATE_ENTER );
			mDownloadingInboxListState = AddState< DownloadingListInboxState >( INBOX_STATE_DOWNLOADING_LIST );
			mIdleInboxState = AddState< IdleInboxState >( INBOX_STATE_IDLE );
			mDownloadingContentInboxState = AddState< DownloadingContentInboxState >( INBOX_STATE_DOWNLOADING_CONTENT );
			mOpeningPackInboxState = AddState< OpeningPackInboxState >( INBOX_STATE_OPENING_PACK );
			mCompleteOpenPackInboxState = AddState< CompleteOpenPackInboxState >( INBOX_STATE_COMPLETE_OPEN_PACK );

			AddTransition( INBOX_STATE_INITIALIZATION, INBOX_STATE_ENTER );
			AddTransition( INBOX_STATE_ENTER, INBOX_STATE_DOWNLOADING_LIST );
			AddTransition( INBOX_STATE_DOWNLOADING_LIST, INBOX_STATE_IDLE );
			AddTransition( INBOX_STATE_IDLE, INBOX_STATE_DOWNLOADING_CONTENT );
			AddTransition( INBOX_STATE_DOWNLOADING_CONTENT, INBOX_STATE_OPENING_PACK );
			AddTransition( INBOX_STATE_OPENING_PACK, INBOX_STATE_COMPLETE_OPEN_PACK );
			AddTransition( INBOX_STATE_COMPLETE_OPEN_PACK, INBOX_STATE_IDLE );

			//By default let's move to the colleciton viewer
			StateConstructionData = new StateConstructionData( mInitializationInboxState.Id );

			RegisterListeners();
        }

		private void OnDestroy()
		{
			UnregisterListeners();
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInitializationInboxState.StateCompleted += HandleStateCompleted;
			mEnterInboxState.StateCompleted += HandleStateCompleted;
			mDownloadingInboxListState.StateCompleted += HandleStateCompleted;
			mIdleInboxState.StateCompleted += HandleStateCompleted;
			mDownloadingContentInboxState.StateCompleted += HandleStateCompleted;
			mOpeningPackInboxState.StateCompleted += HandleStateCompleted;
			mCompleteOpenPackInboxState.StateCompleted += HandleStateCompleted;

			InboxManager.Get.InboxEventDataReceived += HandleInboxEventDataReceived;
		}

		private void UnregisterListeners()
		{
			mInitializationInboxState.StateCompleted -= HandleStateCompleted;
			mEnterInboxState.StateCompleted -= HandleStateCompleted;
			mDownloadingInboxListState.StateCompleted -= HandleStateCompleted;
			mIdleInboxState.StateCompleted -= HandleStateCompleted;
			mDownloadingContentInboxState.StateCompleted -= HandleStateCompleted;
			mOpeningPackInboxState.StateCompleted -= HandleStateCompleted;
			mCompleteOpenPackInboxState.StateCompleted -= HandleStateCompleted;

            if(InboxManager.Get != null)
            {
			    InboxManager.Get.InboxEventDataReceived -= HandleInboxEventDataReceived;
            }
		}
		#endregion

		#region Event Handlers
        protected virtual void HandleStateCompleted( State state, StateConstructionData stateConstructionMessageData )
		{
			StateConstructionData = stateConstructionMessageData;
		}

		private void HandleInboxEventDataReceived( object o, InboxEventDataEventArgs inboxEventDataEventArgs )
		{
			( ( AbstractInboxState )CurrentState ).OnInboxEventDataReceived( inboxEventDataEventArgs.InboxEventData );
		}
		#endregion
    }
}