﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public abstract class AbstractInboxState : StateMachine.State
	{
		#region Public Events
		public event EventHandler ReadyToTransitionToRumble;
		public event EventHandler ReadyToTransitionToShop;
		public event EventHandler ReadToTransitionToCollection;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}

		public virtual void OnInboxEventDataReceived( InboxEventData inboxEventData )
		{
		}

		public virtual void OnInboxResponseFailure( string message )
		{
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();

			frontEndNavigationController.RumbleClicked += HandleReadyToTransitionToRumble;
			frontEndNavigationController.ShopClicked += HandleReadyToTransitionToShop;
			frontEndNavigationController.CollectionClicked += HandleReadyToTransitionToCollection;

			InboxManager.Get.InboxResponseFailure += HandleInboxResponseFailure;
			InboxManager.Get.InboxConnectionFailed += HandleInboxConnectionFailed;
		}

		private void UnregisterListeners()
		{
			FrontEndNavigationController frontEndNavigationController = UIManager.Get.GetMenuController< FrontEndNavigationController >();

			frontEndNavigationController.RumbleClicked -= HandleReadyToTransitionToRumble;
			frontEndNavigationController.ShopClicked -= HandleReadyToTransitionToShop;
			frontEndNavigationController.CollectionClicked -= HandleReadyToTransitionToCollection;

			InboxManager.Get.InboxResponseFailure -= HandleInboxResponseFailure;
			InboxManager.Get.InboxConnectionFailed -= HandleInboxConnectionFailed;
		}
		#endregion

		#region Event Firing
		protected void FireReadyToTransitionToRumble()
		{
			if( ReadyToTransitionToRumble != null )
			{
				ReadyToTransitionToRumble( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToShop()
		{
			if( ReadyToTransitionToShop != null )
			{
				ReadyToTransitionToShop( this, EventArgs.Empty );
			}
		}

		protected void FireReadyToTransitionToInbox()
		{
			if( ReadToTransitionToCollection != null )
			{
				ReadToTransitionToCollection( this, EventArgs.Empty );
			}
		}
		#endregion

		#region Event Handlers
		private void HandleInboxResponseFailure( object o, StringEventArgs args )
		{
			OnInboxResponseFailure( args.String );
		}

		protected virtual void HandleInboxConnectionFailed()
		{
		}

		protected void HandleReadyToTransitionToRumble( object o, EventArgs args )
		{
			FireReadyToTransitionToRumble();
		}

		protected void HandleReadyToTransitionToShop( object o, EventArgs args )
		{
			FireReadyToTransitionToShop();
		}

		protected void HandleReadyToTransitionToCollection( object o, EventArgs args )
		{
			FireReadyToTransitionToInbox();
		}
		#endregion
	}
}