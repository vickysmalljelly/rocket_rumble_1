using UnityEngine;
using SmallJelly.Framework;
using System;

namespace SmallJelly
{
	public sealed class OnboardingInboxStateMachine : InboxStateMachine
	{
		#region Member Variables
		private int mLastTutorialIndex;
		#endregion

		#region MonoBehaviour
		protected override void Awake()
		{
			base.Awake();

			mLastTutorialIndex = 0;
			OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.OpenPackStepOne );
        }
		#endregion

		#region Event Handlers
		protected override void HandleStateCompleted( State state, StateConstructionData stateConstructionData )
		{
			base.HandleStateCompleted( state, stateConstructionData );

			switch( stateConstructionData.StateId ) 
			{
				case InboxStateMachine.INBOX_STATE_IDLE:
					if( mLastTutorialIndex == 0 )
					{
						mLastTutorialIndex++;
						OnboardingManager.Get.StopOnboardingSection();
						OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.OpenPackStepTwo );
					}
					else if( mLastTutorialIndex == 1 )
					{
						mLastTutorialIndex++;
						OnboardingManager.Get.StartOnboardingSection( OnboardingManager.OnboardingId.OpenPackStepThree );
						GameManager.Get.PlayerData.SetCheckpointOnClient( OnboardingManager.PackOpenedCheckpoint );
					}
					break;

				case InboxStateMachine.INBOX_STATE_DOWNLOADING_CONTENT:
					if( mLastTutorialIndex == 1 )
					{
						OnboardingManager.Get.StopOnboardingSection();
					}
					break;
			}
		}
		#endregion
    }
}