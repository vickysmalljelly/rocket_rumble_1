﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class IdleInboxState : AbstractInboxState
	{
		#region Member Variables
		private InboxItemData[] mInboxItemData;
		#endregion

		#region Public Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is TypedStateConstructionData< InboxItemData[] >, "If we're transitioning to select an inbox item then we need some inbox items to list!" );

			mInboxItemData = ( ( TypedStateConstructionData< InboxItemData[] > ) stateConstructionData ).Value;
		}

		public override void OnEntering()
		{
			base.OnEntering();

			InboxManager.Get.InboxMenuController.InboxList.Refresh( mInboxItemData );

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void ExitToDownloadingContentState( string inboxId )
		{
			StateConstructionData stateConstructionMessageData = new StringStateConstructionData( InboxStateMachine.INBOX_STATE_DOWNLOADING_CONTENT, inboxId );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			InboxManager.Get.InboxMenuController.InboxList.PickedInboxItem += HandlePickedInboxItem;
		}

		private void UnregisterListeners()
		{
			InboxManager.Get.InboxMenuController.InboxList.PickedInboxItem -= HandlePickedInboxItem;
		}
		#endregion

		#region Event Handlers
		private void HandlePickedInboxItem( object o, StringEventArgs args )
		{
			ExitToDownloadingContentState( args.String );
		}
		#endregion
	}
}