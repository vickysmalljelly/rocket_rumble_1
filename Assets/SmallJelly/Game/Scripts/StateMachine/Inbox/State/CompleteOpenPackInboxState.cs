﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	/// <summary>
	/// Used to the opening of the pack - i.e. hitting the "ok" button
	/// </summary>
	public class CompleteOpenPackInboxState : AbstractInboxState
	{
		#region Member Variables
		private InboxList mInboxList;
		private InboxPack mInboxPack;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mInboxList = UIManager.Get.GetMenuController< InboxList >();
			mInboxPack = UIManager.Get.GetMenuController< InboxPack >();
			mInboxPack.OnInsertState( InboxPackController.State.ConfirmCompletion );

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();
		}
		#endregion

		#region Private Methods
		private void ExitToNextState()
		{
			StateConstructionData stateConstructionMessageData = new TypedStateConstructionData< InboxItemData[] >( InboxStateMachine.INBOX_STATE_IDLE, mInboxList.InboxItemData );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInboxPack.InboxPackStateUpdated += HandleInboxPackStateUpdated;
		}

		private void UnregisterListeners()
		{
			mInboxPack.InboxPackStateUpdated -= HandleInboxPackStateUpdated;
		}
		#endregion

		#region Event Handlers
		protected override void HandleInboxConnectionFailed()
		{
			base.HandleInboxConnectionFailed();

			//Remove the inbox pack - we're prematurely terminating the opening content flow
			UIManager.Get.RemoveMenu< InboxPack >();
		}

		private void HandleInboxPackStateUpdated( object o, InboxPackStateTransitionEventArgs inboxPackStateTransitionEventArgs )
		{
			if( inboxPackStateTransitionEventArgs.NewState == InboxPackController.State.MovingOut )
			{
				//We're done with the inbox pack now - it's just starting moving out so let's dispose of it
				UIManager.Get.RemoveMenu< InboxPack >();

				//Exit to the next state
				ExitToNextState();
			}
		}
		#endregion
	}
}