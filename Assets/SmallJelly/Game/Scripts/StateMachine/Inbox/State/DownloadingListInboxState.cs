﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class DownloadingListInboxState : AbstractInboxState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ListInbox();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Public Methods
		public override void OnInboxEventDataReceived( InboxEventData inboxEventData )
		{
			if( inboxEventData is ListItemsInboxEventData )
			{
				InboxItemData[] inboxItemData = ( ( ListItemsInboxEventData ) inboxEventData ).InboxItemData.ToArray();
				ExitToEnterState( inboxItemData );
			}
		}

		public override void OnInboxResponseFailure( string message )
		{
			//TODO - We need to take a second look at inbox server error handling but this will be sufficient for now
			DialogManager.Get.ShowMessagePopup( "Error", message, new ButtonData( "Retry", HandleHiddenErrorMessage ) );
		}
		#endregion

		#region Private Methods
		private void ListInbox()
		{
			InboxManager.Get.ListInbox();
		}

		private void ExitToEnterState( InboxItemData[] inboxItemData )
		{
			StateConstructionData stateConstructionMessageData = new TypedStateConstructionData< InboxItemData[] >( InboxStateMachine.INBOX_STATE_IDLE, inboxItemData );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Handlers
		private void HandleHiddenErrorMessage()
		{
			DialogManager.Get.HideMessagePopup();

			ListInbox();
		}
		#endregion
	}
}