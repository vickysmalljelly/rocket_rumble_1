﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class InitializationInboxState : AbstractInboxState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ExitToEnterState();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Private Methods
		private void ExitToEnterState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( InboxStateMachine.INBOX_STATE_ENTER );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}