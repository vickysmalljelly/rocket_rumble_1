﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;

namespace SmallJelly
{
	public class EnterInboxState : AbstractInboxState
	{
		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			ExitToDownloadingState();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();
		}
		#endregion

		#region Private Methods
		private void ExitToDownloadingState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( InboxStateMachine.INBOX_STATE_DOWNLOADING_LIST );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion
	}
}