﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class OpeningPackInboxState : AbstractInboxState
	{
		#region Member Variables
		private RewardData mRewardData;
		private InboxPack mInboxPack;

		private int mNumberOfElementsRemaining;
		#endregion

		#region Public Methods
		public override void OnEntering()
		{
			base.OnEntering();

			mInboxPack = UIManager.Get.GetMenuController< InboxPack >();
			mInboxPack.Refresh( mRewardData );

			mInboxPack.OnInsertState( InboxPackController.State.Opening );

			RegisterListeners( mInboxPack );
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

            // Show the rewards in the rest of the UI
            GameManager.Get.UpdateRewardsOnClient(mRewardData);

			UnregisterListeners( mInboxPack );
		}

		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is TypedStateConstructionData< RewardData >, "If we're giving a reward then we need a reward to give!" );

			mRewardData = ( ( TypedStateConstructionData< RewardData > ) stateConstructionData ).Value;

            int creditsPackCount = ( mRewardData.Credits.Count );
			int crystalPackCount = ( mRewardData.Crystals > 0 ? 1 : 0 );

			mNumberOfElementsRemaining = mRewardData.ShipCards.Count + mRewardData.BattleEntityCards.Count + mRewardData.Resources.Count + creditsPackCount + crystalPackCount;
		}
		#endregion

		#region Private Methods
		private void CompleteOpenPackInboxState()
		{
			StateConstructionData stateConstructionMessageData = new StateConstructionData( InboxStateMachine.INBOX_STATE_COMPLETE_OPEN_PACK );
			OnStateCompleted( stateConstructionMessageData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners( InboxPack inboxPack )
		{
			inboxPack.InboxItemStateTransitioned += HandleInboxItemStateTransitioned;
		}

		private void UnregisterListeners( InboxPack inboxPack )
		{
			inboxPack.InboxItemStateTransitioned -= HandleInboxItemStateTransitioned;
		}
		#endregion

		#region Event Handlers
		private void HandleInboxItemStateTransitioned( object o, InboxItemAndStateTransitionEventArgs args )
		{
			if( args.NewState == InboxItemController.State.Showing )
			{
				mNumberOfElementsRemaining--;
			}

			if( mNumberOfElementsRemaining == 0 )
			{
				CompleteOpenPackInboxState();
			}
		}
		#endregion
	}
}