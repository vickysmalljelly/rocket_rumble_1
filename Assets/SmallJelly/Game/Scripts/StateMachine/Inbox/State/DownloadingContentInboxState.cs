﻿using UnityEngine;
using System.Collections;
using SmallJelly.Framework;
using System.Collections.Generic;
using System;

namespace SmallJelly
{
	public class DownloadingContentInboxState : AbstractInboxState
	{
		#region Member Variables
		private string mInboxId;

		private RewardData mRewardData;

		private bool mFinishedDownloading;
		private bool mFinishedAnimating;

		private InboxPack mInboxPack;
		#endregion

		#region Public Methods
		public override void OnConstruct( StateConstructionData stateConstructionData )
		{
			base.OnConstruct( stateConstructionData );

			SJLogger.AssertCondition( stateConstructionData is StringStateConstructionData, "If we're transitioning to select a deck then we need some decks to list!" );

			mInboxId = ( ( StringStateConstructionData ) stateConstructionData ).String;

			SJLogger.LogMessage( MessageFilter.George, "Start downloading content with the ID {0}", mInboxId );
		}

		public override void OnEntering()
		{
			base.OnEntering();

			//We animate in as we're downloading, we don't finish the state until all content has been downloaded and the pack has
			//finished animating in
		
			//Create the inbox pack
			mInboxPack = UIManager.Get.AddMenu< InboxPack >( FileLocations.InboxPackPrefab, UIManager.Get.GetMenuController< InboxMenuController >().GetComponentInChildren<SJRenderCanvas>().transform );
			UIManager.Get.ShowMenu< InboxPack >();

			//Remove selected element from the inbox list
			InboxList inboxList = UIManager.Get.GetMenuController< InboxList >();
			List< InboxItemData > inboxItemDataList = new List< InboxItemData >( inboxList.InboxItemData );
			inboxItemDataList.RemoveAll( x => x.UniqueId == mInboxId );

			//Refresh inbox list with the selected element removed
			inboxList.Refresh( inboxItemDataList.ToArray() );

			mInboxPack.OnInsertState( InboxPackController.State.MovingIn );

			ConsumeItem();

			RegisterListeners();
		}

		public override void OnLeaving()
		{
			base.OnLeaving();

			UnregisterListeners();

			Reset();
		}
		#endregion

		#region Protected Methods
		public override void OnInboxEventDataReceived( InboxEventData inboxEventData )
		{
			if( inboxEventData is ConsumeItemInboxEventData )
			{
				mRewardData = ( ( ConsumeItemInboxEventData ) inboxEventData ).RewardData;

				mFinishedDownloading = true;

				CheckCompletion();
			}
		}

		public override void OnInboxResponseFailure( string message )
		{
			//TODO - We need to take a second look at inbox server error handling but this will be sufficient for now
			DialogManager.Get.ShowMessagePopup( "Error", message, new ButtonData( "Retry", HandleHiddenErrorMessage ) );

			//Remove the inbox pack - we're prematurely terminating the downloading content flow
			UIManager.Get.RemoveMenu< InboxPack >();
		}

		protected override void HandleInboxConnectionFailed()
		{
			base.HandleInboxConnectionFailed();

			//Remove the inbox pack - we're prematurely terminating the downloading content flow
			UIManager.Get.RemoveMenu< InboxPack >();
		}
		#endregion

		#region Private Methods
		private void CheckCompletion()
		{
			if( mFinishedDownloading && mFinishedAnimating )
			{
				ExitToOpeningPackState();
			}
		}

		private void ConsumeItem()
		{
			InboxManager.Get.ConsumeItem( mInboxId );
		}

		private void ExitToOpeningPackState()
		{
			StateConstructionData stateConstructionData = new TypedStateConstructionData< RewardData >( InboxStateMachine.INBOX_STATE_OPENING_PACK, mRewardData );
			OnStateCompleted( stateConstructionData );
		}

		private void Reset()
		{
			mFinishedDownloading = false;
			mFinishedAnimating = false;

			mRewardData = default( RewardData );
		}
		#endregion

		#region Event Registration
		private void RegisterListeners()
		{
			mInboxPack.AnimationFinished += HandleAnimationFinished;
		}

		private void UnregisterListeners()
		{
			mInboxPack.AnimationFinished -= HandleAnimationFinished;
		}
		#endregion


		#region Event Handlers
		private void HandleHiddenErrorMessage()
		{
			DialogManager.Get.HideMessagePopup();

			ConsumeItem();
		}

		private void HandleAnimationFinished( object o, EventArgs args )
		{
			mFinishedAnimating = true;
			CheckCompletion();
		}
		#endregion

	}
}