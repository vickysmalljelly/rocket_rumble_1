﻿using SmallJelly.Framework;

namespace SmallJelly
{
    /// <summary>
    /// Gets the username for the player to set
    /// </summary>
    public sealed class UsernameTextEntryState : StateMachine.State 
    {
        #region Member Variables
        private UsernameEntryMenuController mUsernameEntryMenuController;
        #endregion

        public override void OnEntering()
        {
            base.OnEntering();

            mUsernameEntryMenuController = UIManager.Get.AddMenu< UsernameEntryMenuController >( FileLocations.UsernameEntryMenuPrefab, UIManager.Get.transform );
            UIManager.Get.ShowMenu< UsernameEntryMenuController >();

            RegisterListeners();
        }

        public override void OnLeaving()
        {
            UnregisterListeners();

            UIManager.Get.RemoveMenu< UsernameEntryMenuController >();
            Destroy( mUsernameEntryMenuController.gameObject );

            base.OnLeaving();
        }

        #region Event Registration
        private void RegisterListeners()
        {
            mUsernameEntryMenuController.SubmittedUsername += HandleSubmittedUsername;
        }

        private void UnregisterListeners()
        {
            mUsernameEntryMenuController.SubmittedUsername -= HandleSubmittedUsername;
        }
        #endregion

        private void HandleChangeUsernameSuccess()
        {
            DialogManager.Get.HideSpinner();

            TransitionState(UsernameEntryStateMachine.USERNAME_ENTRY_UPDATE_PLAYER_DETAILS);
        }

        private void HandleSubmittedUsername(string username)       
        {
            DialogManager.Get.ShowSpinner();

            ClientAuthentication.ChangeUserName(username, HandleChangeUsernameSuccess, HandleChangeUsernameError);
        }

        private void HandleChangeUsernameError(ServerError error)
        {
            DialogManager.Get.HideSpinner();

            switch(error.ErrorType)
            { 
                case ServerErrorType.Timeout:
                    GameManager.Get.ConnectionFailedDuringUsernamEntry();
                    break;
                default:
                    mUsernameEntryMenuController.ReportUsernameErrorToUser(error);
                    break;
            }
        }
    }
}
