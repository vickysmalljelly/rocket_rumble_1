﻿using UnityEngine;
using System;
using WellFired.DevelopmentConsole;

// VJS: Think we need this to get debug output in the editor?  Don't have time to properly investigate
using Object = UnityEngine.Object;

public static class Debug
{   
    public static void LogError(DebugLog.Filter filter, object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.LogError(combinedMessage);
        #endif
    }

    public static void LogError(object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogError(message);
        #endif
    }

    public static void LogError(DebugLog.Filter filter, object message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.LogError(combinedMessage, ping);
        #endif
    }

    public static void LogError(object message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogError(message, ping);
        #endif
    }

    public static void LogErrorFormat(DebugLog.Filter filter, string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedMessage = String.Format(message, parameters);
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedMessage);
        UnityEngine.Debug.LogError(combinedMessage);
        #endif
    }

    public static void LogErrorFormat(string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogError(String.Format(message, parameters));
        #endif
    }

    public static void LogWarning(DebugLog.Filter filter, object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.LogWarning(combinedMessage);
        #endif
    }

    public static void LogWarning(object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogWarning(message);
        #endif
    }

    public static void LogWarningFormat(DebugLog.Filter filter, string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedMessage = String.Format(message, parameters);
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedMessage);
        UnityEngine.Debug.LogWarning(combinedMessage);
        #endif
    }

    public static void LogWarningFormat(string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogWarning(String.Format(message, parameters));
        #endif
    }

    public static void LogWarningFormat(DebugLog.Filter filter, string message, Object ping, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedMessage = String.Format(message, parameters);
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedMessage);
        UnityEngine.Debug.LogWarning(combinedMessage, ping);
        #endif
    }

    public static void LogWarningFormat(string message, Object ping, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogWarning(String.Format(message, parameters), ping);
        #endif
    }

    public static void LogWarningFormat(DebugLog.Filter filter, object message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.LogWarning(combinedMessage, ping);
        #endif
    }

    public static void LogWarning(object message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogWarning(message, ping);
        #endif
    }

    public static void Log(DebugLog.Filter filter, object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.Log(combinedMessage);
        #endif
    }

    public static void Log(object message)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(message);
        #endif
    }

    public static void Log(DebugLog.Filter filter, string message, Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var colouredMessage = String.Format("<color={0}>{1}</color>", color.ToString(), message);
        var combinedMessage = string.Format("[{0}] {1}", filter, colouredMessage);
        UnityEngine.Debug.Log(combinedMessage);
        #endif
    }

    public static void Log(string message, Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format("<color={0}>{1}</color>", color.ToString(), message));
        #endif
    }

    public static void Log(DebugLog.Filter filter, string message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var combinedMessage = string.Format("[{0}] {1}", filter, message);
        UnityEngine.Debug.Log(combinedMessage, ping);
        #endif
    }

    public static void Log(string message, Object ping)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(message, ping);
        #endif
    }

    public static void Log(DebugLog.Filter filter, string message, Object ping, Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var colouredMessage = String.Format("<color={0}>{1}</color>", color.ToString(), message);
        var combinedMessage = string.Format("[{0}] {1}", filter, colouredMessage);
        UnityEngine.Debug.Log(combinedMessage, ping);
        #endif
    }

    public static void Log(string message, Object ping, Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format("<color={0}>{1}</color>", color.ToString(), message), ping);
        #endif
    }

    public static void LogFormat(DebugLog.Filter filter, string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedMessage = String.Format(message, parameters);
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedMessage);
        UnityEngine.Debug.Log(combinedMessage);
        #endif
    }

    public static void LogFormat(string message, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format(message, parameters));
        #endif
    }

    public static void LogFormat(DebugLog.Filter filter, string message, Color color, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedColouredMessage = String.Format("<color={0}>{1}</color>", color.ToString(), String.Format(message, parameters));
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedColouredMessage);
        UnityEngine.Debug.Log(combinedMessage);
        #endif
    }

    public static void LogFormat(string message, Color color, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format("<color={0}>{1}</color>", color.ToString(), String.Format(message, parameters)));
        #endif
    }

    public static void LogFormat(DebugLog.Filter filter, string message, Object ping, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedMessage = String.Format(message, parameters);
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedMessage);
        UnityEngine.Debug.Log(combinedMessage, ping);
        #endif
    }

    public static void LogFormat(string message, Object ping, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format(message, parameters), ping);
        #endif
    }

    public static void LogFormat(DebugLog.Filter filter, string message, Object ping, Color color, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        var formattedColouredMessage = String.Format("<color={0}>{1}</color>", color.ToString(), String.Format(message, parameters));
        var combinedMessage = string.Format("[{0}] {1}", filter, formattedColouredMessage);
        UnityEngine.Debug.Log(combinedMessage, ping);
        #endif
    }

    public static void LogFormat(string message, Object ping, Color color, params object[] parameters)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Log(String.Format("<color={0}>{1}</color>", color.ToString(), String.Format(message, parameters)), ping);
        #endif
    }

    public static void LogException(Exception exception)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.LogException(exception);
        #endif
    }

    public static void ClearDeveloperConsole()
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.ClearDeveloperConsole();
        #endif
    }

    public static void DebugBreak()
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DebugBreak();
        #endif
    }

    public static void DrawLine(Vector3 start, Vector3 end)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawLine(start, end);
        #endif
    }

    public static void DrawLine(Vector3 start, Vector3 end, UnityEngine.Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawLine(start, end, color);
        #endif
    }

    public static void DrawLine(Vector3 start, Vector3 end, UnityEngine.Color color, float duration)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawLine(start, end, color, duration);
        #endif
    }

    public static void DrawLine(Vector3 start, Vector3 end, UnityEngine.Color color, float duration, bool depthTest)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
        #endif
    }

    public static void DrawRay(Vector3 start, Vector3 dir)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawRay(start, dir);
        #endif
    }

    public static void DrawRay(Vector3 start, Vector3 dir, UnityEngine.Color color)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawRay(start, dir, color);
        #endif
    }

    public static void DrawRay(Vector3 start, Vector3 dir, UnityEngine.Color color, float duration)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawRay(start, dir, color, duration);
        #endif
    }

    public static void DrawRay(Vector3 start, Vector3 dir, UnityEngine.Color color, float duration, bool depthTest)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest);
        #endif
    }

    public static void Break()
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        UnityEngine.Debug.Break();
        #endif
    }

    public static void Assert(bool condition, string message = null, string message1 = null)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE
        if(!condition)
        { 
            throw new Exception(message);
        }
        #endif
    }

    public static void WriteLine(string line)
    {
        #if !DISABLE_DEVELOPMENT_CONSOLE

        #endif
    }

    public enum Color
    {
        black,
        blue,
        brown,
        cyan,
        darkblue,
        fuchsia,
        green,
        grey,
        lightblue,
        lime,
        magenta,
        maroon,
        navy,
        olive,
        orange,
        purple,
        red,
        silver,
        teal,
        white,
        yellow
    }
}
