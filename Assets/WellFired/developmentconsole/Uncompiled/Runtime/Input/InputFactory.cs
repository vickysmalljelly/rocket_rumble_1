using UnityEngine;
using System.Collections;

namespace WellFired.DevelopmentConsole
{
	/// <summary>
	/// If you want to provide specific input support for your platform, you can add your platform to this Input Factory, returning
	/// a new instance of your custom IInputField.
	/// </summary>
	public static class InputFieldFactory
	{
		private static IInputField platformInputField;
		
		public static IInputField GetInputField(LogHistoryGUI logHistoryView)
		{
			if(platformInputField != default(IInputField))
			{
				return platformInputField;
			}
			
			// Deal with IPhone and Android only.
			if(Application.platform == RuntimePlatform.IPhonePlayer)
			{
				platformInputField = new TouchInputField(logHistoryView);
			}
			else if(Application.platform == RuntimePlatform.Android)
			{
				platformInputField = new TouchInputField(logHistoryView);
			}
			else if(Application.platform == RuntimePlatform.WP8Player)
			{
				platformInputField = new TouchInputField(logHistoryView);
			}
            /*
			else if(Application.platform == RuntimePlatform.BB10Player)
			{
				platformInputField = new TouchInputField(logHistoryView);
			}
            */         
			
			// If we have no email sender, simply use an emtpy email sender, which defaults to doing nothing.
			if(platformInputField == default(IInputField))
			{
				platformInputField = new KeyboardInputField(logHistoryView);
			}
			
			return platformInputField;
		}
	}
}