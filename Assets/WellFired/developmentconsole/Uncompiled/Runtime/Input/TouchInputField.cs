﻿using System.Linq;
using UnityEngine;
using WellFired.Shared;

namespace WellFired.DevelopmentConsole
{
	class TouchInputField : IInputField
	{
		#region Fields
	    private string commandInput = string.Empty;
		#endregion

		#region Properties
		public bool HasFocus
		{
			get { return currentTouchScreenKeyboard != null; }
		}
		
		public Rect Rect
		{
			get;
			set;
		}

		private LogHistoryGUI LogHistoryView
		{
			get;
			set;
		}

		private TouchScreenKeyboard currentTouchScreenKeyboard
		{
			get;
			set;
		}

		public string[] PreviousCompleteParameters
		{
			get;
			set;
		}

		public string PreviousCompleteInput
		{
			get;
			set;
		}
		
		public int CurrentParameterIndex
		{
			get
			{
				int currentParameter = PreviousCompleteParameters.Length - 2;
				
				if (PreviousCompleteInput.EndsWith(" "))
				{
					currentParameter++;
				}
				return currentParameter;
			}
		}

	    public string Input
	    {
	        get
	        {
	            return commandInput;
	        }
	        set
	        {
	            commandInput = value;
	
	            ShowInputForConsole();
	            currentTouchScreenKeyboard.text = value;
	        }
	    }
		#endregion
	
		#region Methods
		public TouchInputField(LogHistoryGUI logHistoryView)
	    {
			this.PreviousCompleteInput = string.Empty;
			this.PreviousCompleteParameters = new string[] {};
			this.LogHistoryView = logHistoryView;
	    }
	
		public void Draw()
		{
			using(new GUIBeginHorizontal())
			using(new GUIChangeColor(new Color(1, 1, 1, 2)))
			using(new GUIEnable(false))
			{
	        	GUI.SetNextControlName("Input");
	        	GUILayout.TextField(commandInput, GUILayout.ExpandWidth(true));
				
	        	using(new GUIEnable(true))
				{
	        		if(Event.current.type == EventType.Repaint)
	        		{
	        		    Rect = GUILayoutUtility.GetLastRect();
	        		}
					
	        		if(currentTouchScreenKeyboard == null)
	        		{
	        		    if(Event.current.type == EventType.MouseDown)
	        		    {
	        		        if(Rect.Contains(Event.current.mousePosition))
							{
	        		            ShowInputForConsole();
							}
	        		    }
	        		}
	        		else
	        		{
	        		    commandInput = currentTouchScreenKeyboard.text;
					
	        		    if(currentTouchScreenKeyboard.done)
	        		    {
	        		        LoseFocus();
					
							DevelopmentCommands.HandleCommand(commandInput);
	        		        Input = string.Empty;
	        		        LogHistoryView.AutoScrolling = true;
	        		    }
	        		    else if(currentTouchScreenKeyboard.wasCanceled || !currentTouchScreenKeyboard.active)
						{
	        		        LoseFocus();
						}
	        		}
				}
			}
	
			FinaliseInput();
	    }
	
	    public void LoseFocus()
	    {
	        if(currentTouchScreenKeyboard != null)
	        {
	            DevelopmentConsole.Instance.ForceMinimize = false;
	            currentTouchScreenKeyboard.active = false;
	            currentTouchScreenKeyboard = null;
	        }
	    }
	
	    public void Focus()
	    {
	        ShowInputForConsole();
		}

		public void FinaliseInput()
		{
			PreviousCompleteParameters = WellFired.Shared.StringExtensions.SplitCommandLine(Input).ToArray();
			PreviousCompleteInput = Input;
		}
		#endregion
	
		#region Implementation
	    private void ShowInputForConsole()
	    {
	        DevelopmentConsole.Instance.ForceMinimize = true;
	        TouchScreenKeyboard.hideInput = true;
	        currentTouchScreenKeyboard = TouchScreenKeyboard.Open(commandInput, TouchScreenKeyboardType.Default, false, false, false, true);
			DevelopmentConsole.Instance.HideAllOpenPopups();
	    }
		#endregion
	}
}