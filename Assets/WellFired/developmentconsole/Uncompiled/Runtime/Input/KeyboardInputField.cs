using System;
using System.Linq;
using UnityEngine;
using WellFired.Shared;

namespace WellFired.DevelopmentConsole
{
	class KeyboardInputField : IInputField
	{
		#region Fields
		private string commandInput = string.Empty;
		#endregion

		#region Methods
		public bool HasFocus
		{
			get { return GUI.GetNameOfFocusedControl() == "Input"; }
		}

		private LogHistoryGUI LogHistoryView
		{
			get;
			set;
		}

		public TextEditor TextEditor
		{
			get;
			private set;
		}

		private int TabIndex
		{
			get;
			set;
		}
		
		public Rect Rect
		{
			get;
			private set;
		}
		
		public string PreviousCompleteInput
		{
			get;
			private set;
		}

		public string[] PreviousCompleteParameters
		{
			get;
			private set;
		}
		
		public int CurrentParameterIndex
		{
			get
			{
				var currentParameter = PreviousCompleteParameters.Length - 2;
				
				if(PreviousCompleteInput.EndsWith(" "))
				{
					currentParameter++;
				}
				
				return currentParameter;
			}
		}

		public string Input
		{
			get
			{
				return commandInput;
			}
			set
			{
				commandInput = value;

                TextEditor.text = new GUIContent(commandInput).text;
				TextEditor.MoveLineEnd();

				var fakeEvent = new Event();
				fakeEvent.keyCode = KeyCode.RightArrow;
				fakeEvent.type = EventType.keyDown;
				TextEditor.HandleKeyEvent(fakeEvent);
			}
		}
	
		public KeyboardInputField(LogHistoryGUI logHistoryGUIView)
		{
			PreviousCompleteInput = string.Empty;
			PreviousCompleteParameters = new string[] {};
			LogHistoryView = logHistoryGUIView;
		}
	
		public void Draw()
		{
			if(Event.current.type == EventType.KeyDown)
			{
				if(Event.current.keyCode == KeyCode.Return)
				{
					if(commandInput != string.Empty)
					{
						DevelopmentCommands.HandleCommand(commandInput);
						commandInput = string.Empty;
						LogHistoryView.AutoScrolling = true;
						FinaliseInput();
					}
				}
				else if(Event.current.keyCode == KeyCode.Tab)
				{
					HandleTabKeyPressed();
				}
			}
	
			if(Event.current.isKey && (Event.current.keyCode == KeyCode.Tab || Event.current.character == 9))
			{
				Event.current.type = EventType.Used;
			}

			GUI.SetNextControlName("Input");
			var previousCommandInput = commandInput;
			commandInput = DevelopmentConsole.Instance.CheckInputForTilde(GUILayout.TextField(commandInput, StyleContainer.ConsoleSkin.ConsoleTextField, GUILayout.ExpandWidth(true)));
	
			if(Event.current.type == EventType.Repaint)
			{
				Rect = GUILayoutUtility.GetLastRect();
			}

			if(DevelopmentConsole.Instance.JustMadeVisible)
			{
				DevelopmentConsole.Instance.JustMadeVisible = false; 
				GUI.FocusControl("Input");
				TextEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
			}
	
			if(previousCommandInput != commandInput)
			{
				TabIndex = 0;
				FinaliseInput();
			}
		}
	
		private void HandleTabKeyPressed()
		{
			if(PreviousCompleteParameters == null || PreviousCompleteParameters.Length == 0)
			{
				TabIndex++;
				return;
			}

			var currentParameter = PreviousCompleteParameters.Length - 2;
	
			if(PreviousCompleteInput.EndsWith(" "))
			{
				currentParameter++;
			}

			// Completing the command or the parameter
			if(currentParameter < 0)
			{
				var closestMatch = DevelopmentCommands.FindCommandFromPartial(PreviousCompleteParameters[0], TabIndex);
				if(closestMatch != null)
				{
					Input = closestMatch.CommandName;
				}

				TabIndex++;
				return;
			}

			var commandHandler = DevelopmentCommands.GetCommandWrapper(PreviousCompleteParameters[0]);
			if(commandHandler == default(CommandWrapper))
			{
				TabIndex++;
				return;
			}

			var parameters = commandHandler.Parameters;
			if(currentParameter < parameters.Length)
			{
				var parameterValue = PreviousCompleteParameters.Length < currentParameter + 2 ? string.Empty : PreviousCompleteParameters[currentParameter + 1];
				var possibleParameters = parameters[currentParameter].GetParameterPossibleValues(parameterValue, PreviousCompleteParameters).ToArray();
				if(possibleParameters.Length != 0)
				{
					var closestMatchingParameterValue = possibleParameters[TabIndex % possibleParameters.Length];
					if(!String.IsNullOrEmpty(closestMatchingParameterValue))
					{
						var parts = String.Join(" ", PreviousCompleteParameters.SubArray(0, currentParameter + 1));
						Input = parts + " " + closestMatchingParameterValue;
					}
				}
			}

			TabIndex++;
		}
	
		public void LoseFocus()
		{
			TabIndex = 0;
		}
	
		public void Focus()
		{
			GUI.FocusControl("Input");
		}

		public void FinaliseInput()
		{
			PreviousCompleteParameters = WellFired.Shared.StringExtensions.SplitCommandLine(Input).ToArray();
			PreviousCompleteInput = Input;
		}
		#endregion
	}
}