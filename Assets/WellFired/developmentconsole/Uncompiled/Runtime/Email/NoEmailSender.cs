﻿using UnityEngine;
using System.Collections;

namespace WellFired.DevelopmentConsole
{
	public class NoEmailSender : IEmailSender
	{
		public bool CanSendEmail()
		{
			return false;
		}

		public void Email(string filePathToAttachment, string mimeType, string attachmentFilename, string recipientAddress, string subject, string body)
		{

		}
	}
}