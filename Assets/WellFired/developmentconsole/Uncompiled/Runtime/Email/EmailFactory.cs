﻿using UnityEngine;
using System.Collections;

namespace WellFired.DevelopmentConsole
{
	/// <summary>
	/// If you want to provide specific email support for your platform, you can add your platform to this Email Factory, returning
	/// a new instance of your custom IEmailSender.
	/// </summary>
	public static class EmailFactory
	{
		private static IEmailSender platformEmailSender;
	
		public static IEmailSender GetEmailSender()
		{
			if(platformEmailSender != default(IEmailSender))
			{
				return platformEmailSender;
			}
	
			// Deal with IPhone and Android only.
			if(Application.platform == RuntimePlatform.IPhonePlayer)
			{
				platformEmailSender = new IPhoneEmailSender();
			}
			else if(Application.platform == RuntimePlatform.Android)
			{
				platformEmailSender = new AndroidEmailSender();
			}
	
			// If we have no email sender, simply use an emtpy email sender, which defaults to doing nothing.
			if(platformEmailSender == default(IEmailSender))
			{
				platformEmailSender = new NoEmailSender();
			}
	
			return platformEmailSender;
		}
	}
}