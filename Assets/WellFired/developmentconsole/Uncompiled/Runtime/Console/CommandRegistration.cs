﻿namespace WellFired.DevelopmentConsole
{
	/// <summary>
	/// This class is here to provide users with a single easy location to register all command objects with the Development Console.
	/// 
	/// Note: You can Register Commands from anywhere you like, if you do it here however, you'll be able to keep the 
	/// registration / unregistration tidy, all methods in this static class will be called automatically for you.
	/// 
	/// </summary>
	public static class CommandRegistration 
	{
		/// <summary>
		/// Called automatically when the Development Console launches, you can add your own Registrations here.
		/// Don't forget to match your DevelopmentCommands.Register call with a DevelopmentCommands.Unregister call
		/// by calling DevelopmentCommands.Unregister in CommandRegistration.UnRegisterCommandsOnConsoleExit
		/// 
		/// Note: You can Register Commands from anywhere you like, if you do it here however, you'll be able to keep the 
		/// registration / unregistration tidy, this method will be called automatically for you at a sensible time.
		/// 
		/// </summary>
		public static void RegisterCommandsOnConsoleStartup()
		{
			// Register Commands by type
			/// Add your own Registration by Type here:
			DevelopmentCommands.Register(typeof(SampleCommands));
	
			// Register Commands by object
			/// Add your own Registration by Object here:
			DevelopmentCommands.Register(DevelopmentConsole.Instance);
		}
		
		/// <summary>
		/// Called automatically when the Development Console is destroyed, you can add your own Unregistrations here.
		/// 
		/// Note: You can Unregister Commands from anywhere you like, if you do it here however, you'll be able to keep the 
		/// registration / unregistration tidy, this method will be called automatically for you at a sensible time.
		/// 
		/// </summary>
		public static void UnRegisterCommandsOnConsoleExit()
		{
			// Un Register Commands by type
			/// Add your own Un Registration by Type here:
			DevelopmentCommands.Unregister(typeof(SampleCommands));
			
			// Un Register Commands by object
			/// Add your own Un Registration by Object here:
			DevelopmentCommands.Unregister(DevelopmentConsole.Instance);
		}
	}
}