using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WellFired.DevelopmentConsole
{
	/// <summary>
	/// A selection of provided sample commands that will help you get started with writing your own.
	/// </summary>
	public static class SampleCommands
	{
		private static readonly float MIN_CONSOLE_SIZE = 120.0f;
		private static readonly float MAX_CONSOLE_SIZE = 2000.0f;

		#region Implementation
		/// <summary>
		/// A simple Sample Command that outputs the Device' ID.
		/// You can mark up the Method with the attribute ConsoleCommand, passing a custom Name of Description, as seen in this example.
		/// 
		/// [ConsoleCommand(Name = "DeviceID", Description = "Outputs the device's ID")]
		/// private static void DeviceID()
		/// {
		/// 	Debug.Log("Device ID: " + SystemInfo.deviceUniqueIdentifier);
		/// }
		/// </summary>
		[ConsoleCommand(Name = "DeviceID", Description = "Outputs the device's ID")]
		private static void DeviceID()
		{
			Debug.Log("Device ID: " + SystemInfo.deviceUniqueIdentifier);
		}
	
		/// <summary>
		/// This Sample Console Command, simply adds the ConsoleCommand Attribute, the rest is set up automatically.
		/// We use a custom Suggestion Attribute that should give the user some hints and easy to use buttons. 
		/// </summary>
		/// <param name="consoleSize">Console size.</param>
		[ConsoleCommand]
		public static void ConsoleSize([Suggestion(typeof(ConsoleSizeSuggestion))]int consoleSize)
		{
			var existingSize = StyleContainer.GetDPI();
			var newSize = existingSize + consoleSize;

			PlayerPrefs.SetInt("WellFired.ConsoleSize", (int)Mathf.Clamp(newSize, MIN_CONSOLE_SIZE, MAX_CONSOLE_SIZE));
			StyleContainer.ReSizeStyleForScreen();
		}
		#endregion

		#region Suggestions
		/// <summary>
		/// Our Custom Implementation of an Suggestion, the Method IEnumerable<string> Suggestion
		/// Must be implemented as per the interface. We return some user readable size modifications.
		/// </summary>
		class ConsoleSizeSuggestion : ISuggestion
		{
			public IEnumerable<string> Suggestion(IEnumerable<string> previousArguments)
			{
				return new [] { "-150", "-100", "-50", "+50", "+100", "+150" };
			}
		}
		#endregion
	}
}