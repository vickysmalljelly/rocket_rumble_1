﻿using System.Collections;
using UnityEngine;
using WellFired.DevelopmentConsole;

public class DebugConsoleLoader : MonoBehaviour 
{
#if !DISABLE_DEVELOPMENT_CONSOLE
	[SerializeField]
	private bool isVisibleOnLaunch = false;
	[SerializeField]
	private bool showToggleConsoleButton = false;
#endif

	private void Start()
	{
#if !DISABLE_DEVELOPMENT_CONSOLE
		if(DevelopmentConsole.Instance == default(DevelopmentConsole))
		{
			DevelopmentConsole.Load("NightShade");
			DevelopmentConsole.Instance.IsVisible = isVisibleOnLaunch;
			DevelopmentConsole.Instance.DrawShowConsoleButton = showToggleConsoleButton;
			Debug.Log(DebugLog.Filter.DevelopmentConsole, "Loading Development Console");
		}
#endif

		GameObject.DestroyImmediate(gameObject);
	}
}