
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTPlayCommon : MonoBehaviour
{
	public delegate void Callback(string s);

	public static void InitKTPlay () {
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.Init();
		#endif
	}

	public static void startWithAppKey(string appKey ,string appSecret)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.startWithAppKey(appKey,appSecret);
		Debug.Log("[KTPLAY.CS] startWithAppKey ");
		#elif UNITY_IOS
		KTPlayiOS.startWithAppKey(appKey,appSecret);
		Debug.Log("[KTPLAY.CS] startWithAppKey ");
		#endif

	}

	public static void HanldeApplicationPause(bool pauseStatus){
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.HanldeApplicationPause(pauseStatus);
		#endif
	}

	/// <summary>
	///  设置监听者，监听打开KTPlay主窗口事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetViewDidAppearCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetViewDidAppearCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetViewDidAppearCallback(obj,callbackMethod);
		#endif
	}
	/// <summary>
	///  设置监听者，监听关闭KTPlay主窗口事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetViewDidDisappearCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetViewDidDisappearCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetViewDidDisappearCallback(obj,callbackMethod);
		#endif
	}
	
	public static void SetOnSoundStartCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetOnSoundStartCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetOnSoundStartCallback(obj,callbackMethod);
		#endif
	}

	public static void SetOnSoundStopCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetOnSoundStopCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetOnSoundStopCallback(obj,callbackMethod);
		#endif
	}

	/// <summary>
	///   设置监听者，监听奖励发放事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetDidDispatchRewardsCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetDidDispatchRewardsCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetDidDispatchRewardsCallback(obj,callbackMethod);
		#endif
	}
	/// <summary>
	///   设置监听者，监听用户新动态
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetActivityStatusChangedCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetActivityStatusChangedCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetActivityStatusChangedCallback(obj,callbackMethod);
		#endif
	}
	/// <summary>
	///   设置监听者，监听KTPlay SDK的可用状态变更
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetAvailabilityChangedCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetAvailabilityChangedCallback(obj, callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetAvailabilityChangedCallback(obj,callbackMethod);
		#endif
	}
	/// <summary>
	///   设置监听者，监听DeepLink事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetDeepLinkCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetDeepLinkCallback(obj,callbackMethod);
		#elif UNITY_IOS
		KTPlayiOS.SetDeepLinkCallback(obj,callbackMethod);
		#endif
	}
	/// <summary>
	///   打开KTPlay主窗口
	/// </summary>
	public static void Show()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.Show();
		#elif UNITY_IOS
		KTPlayiOS.Show();
		#endif
	}
	
	

	/// <summary>
	///   打开KTPlay（插屏通知窗口）
	/// </summary>
	public static void ShowInterstitialNotification(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod, string identifier)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.showInterstitialNotification(obj,callbackMethod,identifier);
		#elif UNITY_IOS
		KTPlayiOS.ShowInterstitialNoti(obj, callbackMethod, identifier);
		#endif
	}
	
	public static bool HasInterstitialNotification(string identifier)
	{
		#if UNITY_EDITOR
		return false;
		#elif UNITY_ANDROID
		return KTPlayAndroid.hasInterstitialNotification(identifier);
		#elif UNITY_IOS
		return KTPlayiOS.HasInterstitialNotification(identifier);
	
		#endif
	}
	
	public static void RequestInterstitialNotification(string identifier)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.requestInterstitialNotification(identifier);
		#elif UNITY_IOS
		KTPlayiOS.RequestInterstitialNotification(identifier);
		#endif
	}
	
	/// <summary>
	///   关闭KTPlay主窗口
	/// </summary>
	public static void Dismiss()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.Dismiss();
		#elif UNITY_IOS
		KTPlayiOS.Dismiss();
		#endif
	}

	/// <summary>
	///   判断KTplay是否可用
	///    	KTPlay不可用的情况包括：
	///    	1、设备不被支持
	///     2、在Portal上关闭
	///    	3、未获取到服务器配置信息（断网）
	/// </summary>
	/// <return >KTplay是否可用</return>
	
	public static bool IsEnabled()
	{
		#if UNITY_EDITOR
		return false;
		#elif UNITY_ANDROID
		return KTPlayAndroid.IsEnabled();
		#elif UNITY_IOS
		return KTPlayiOS.IsEnabled();

		#endif
	}
	/// <summary>
	///   判断KTplay主窗口是否处于打开状态
	/// </summary>
	/// <return>KTplay主窗口是否打开</return>
	public static bool IsShowing()
	{
		#if UNITY_EDITOR
		return false;
		#elif UNITY_ANDROID
		return KTPlayAndroid.IsShowing();
		#elif UNITY_IOS
		return KTPlayiOS.IsShowing();
		#endif
	}
	




	/// <summary>
	///   分享图片/文本到KTPlay社区
	/// </summary>
	/// <param name="imagePath">图片的绝对路径,为nil时，没有默认图片</param>
	/// <param name="description">图片的描述,为nil时，没有默认内容描述</param>
	public static void ShareImageToKT(string imagePath, string title, string description)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.ShareImageToKT(imagePath, title, description);
		#elif UNITY_IOS
		KTPlayiOS.ShareImageToKT(imagePath, title, description);
		#endif
	}


	
	/// <summary>
	///   分享视频/文本到KTPlay社区
	/// </summary>
	/// <param name="videoPath">视频的绝对路径,为nil时，没有默认视频</param>
	/// <param name="description">视频的描述,为nil时，没有默认内容描述</param>
	public static void ShareVideoToKT(string videoPath, string title, string description)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.ShareVideoToKT(videoPath, title, description);
		#elif UNITY_IOS
		KTPlayiOS.ShareVideoToKT(videoPath, title, description);
		#endif
	}
	
	/// <summary>
	///   启用/禁用通知功能
	/// </summary>
	/// <param name="enabled">YES/NO 启用/禁用</param>
	public static void SetNotificationEnabled(bool enabled)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTPlayAndroid.SetNotificationEnabled(enabled);
		#elif UNITY_IOS
		KTPlayiOS.SetNotificationEnabled(enabled);
		#endif
	}


	public static bool SetLanguage(string preferredLanguage, string alternateLanguage)
	{

		#if UNITY_EDITOR
		return false;
		#elif UNITY_ANDROID
		return KTPlayAndroid.SetLanguage(preferredLanguage,alternateLanguage);
		#elif UNITY_IOS
		return KTPlayiOS.SetLanguage(preferredLanguage,alternateLanguage);
		#endif
	}

	public static void OpenDeepLink(string deepLinkId)
	{

		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		 KTPlayAndroid.OpenDeepLink(deepLinkId);
		#elif UNITY_IOS
		 KTPlayiOS.OpenDeepLink(deepLinkId);
		#endif
	}
	
}
