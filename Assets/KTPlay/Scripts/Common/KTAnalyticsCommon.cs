
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTAnalyticsCommon : MonoBehaviour
{
	public static void SetUserProperty(string name, object value)
	{

		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTAnalyticsAndroid.SetUserProperty(name, value);
		#elif UNITY_IOS
		 KTAnalyticsiOS.SetUserProperty(name, value);
		#endif
	}

	public static void LogEvent(string name, Hashtable properties)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		KTAnalyticsAndroid.LogEvent(name, properties);
		#elif UNITY_IOS
		KTAnalyticsiOS.LogEvent(name, properties);
		#endif
	}
}
