
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_EDITOR

#elif UNITY_ANDROID

public class KTAnalyticsAndroid : MonoBehaviour {

	public static void SetUserProperty(string name, object value)
	{
		if(value is int)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
		else if(value is long)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Long", (long)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}

		else if(value is double)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Double", (double)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
		else if(value is string)
		{
			//AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, (string)value);
		}
		else if(value is bool)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Boolean", (bool)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
	}

	public static void LogEvent(string name, Hashtable properties)
	{
		if(properties == null)
		{
		     return;
		}
		AndroidJavaObject map = new AndroidJavaObject("java.util.HashMap");

//		int i = 123;
//		long l = 123456789;
//		double d = 123456789.1234;
//		
//		Hashtable dict=new Hashtable(); 
//		
//		dict.Add ("int", i);
//		dict.Add ("long", l);
//		dict.Add ("string", "123456789");
//		dict.Add ("double", d);
//		dict.Add ("bool3", true);
//		dict.Add ("bool4", false);

		foreach(DictionaryEntry kvp in properties)
		{
			if(kvp.Value is int)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			else if(kvp.Value is long)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Long", (long)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			
			else if(kvp.Value is double)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Double", (double)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			else if(kvp.Value is string)
			{
				map.Call<string>("put", kvp.Key, (string)kvp.Value);
			}
			else if(kvp.Value is bool)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Boolean", (bool)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
		}
		
		
//		Dictionary<string,object> dict = new Dictionary<string,object>();
//		dict.Add ("int", i);
//		dict.Add ("long", l);
//		dict.Add ("string", "123456789");
//		dict.Add ("double", d);
//		dict.Add ("bool3", true);
//		dict.Add ("bool4", false);

//		foreach(KeyValuePair<string,object> kvp in dict){
//
//			if(kvp.Value is int)
//			{
//				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)kvp.Value);
//				map.Call<string>("put", kvp.Key, jo);
//			}
//			else if(kvp.Value is long)
//			{
//				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Long", (long)kvp.Value);
//				map.Call<string>("put", kvp.Key, jo);
//			}
//			
//			else if(kvp.Value is double)
//			{
//				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Double", (double)kvp.Value);
//				map.Call<string>("put", kvp.Key, jo);
//			}
//			else if(kvp.Value is string)
//			{
//				map.Call<string>("put", kvp.Key, (string)kvp.Value);
//			}
//			else if(kvp.Value is bool)
//			{
//				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Boolean", (bool)kvp.Value);
//				map.Call<string>("put", kvp.Key, jo);
//			}
//		}

		KTPlayAndroid.joKTPlayAdapter.CallStatic("logEvent", name, map);
	}
}
#endif