function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Small Jelly')
      .addItem('Update CardDisplayText', 'updateCardDisplayText')
      .addSeparator()
      .addSubMenu(ui.createMenu('Sub-menu')
          .addItem('Second item', 'menuItem2'))
      .addToUi();
}

function updateCardDisplayText() {
  
  // Get indices for CardText sheet
  var cardTextSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("CardText");
  Logger.log('Found sheet %s', cardTextSheet.getSheetName());
  var ctDisplayTextColIndex = getColumnIndexByName(cardTextSheet, "DisplayText");
  Logger.log('CT DisplayText has index %s', ctDisplayTextColIndex);
  
  // Get indices for entitySheet
  var sheet = SpreadsheetApp.getActiveSheet();
  var entityIdIndex = getColumnIndexByName(sheet, "CardTextId");
  Logger.log('Entity CardTextId has index %s', entityIdIndex);
  var entityParam1Index = getColumnIndexByName(sheet, "CardParam1");
  Logger.log('Entity CardParam1 has index %s', entityParam1Index);
  var entityParam2Index = getColumnIndexByName(sheet, "CardParam2");
  Logger.log('Entity CardParam2 has index %s', entityParam2Index);
  var entityTriggerIndex = getColumnIndexByName(sheet, "CardTextTrigger");
  Logger.log('Entity CardTextTrigger has index %s', entityTriggerIndex); 
  var entityDisplayTextIndex = getColumnIndexByName(sheet, "CardDisplayText");
  Logger.log('Entity CardDisplayText has index %s', entityDisplayTextIndex); 
  
  
  //var range = sheet.getRange(2, columnIndex + 1, sheet.getLastRow() - 1);
  var numRows = sheet.getMaxRows() - 1;
  var numColumns = sheet.getMaxColumns();
  var numUpdated = 0;
  
  Logger.log('entity sheet rows = %s', numRows);
  Logger.log('entity sheet cols = %s', numColumns);
  
  // For each row in the entity spreadsheet
  for (var row = 2; row <= numRows; row++) {
    // Get the row as a range
    var range = sheet.getRange(row, 1, 1, numColumns);
    
    // Get the CardTextId
    var cardTextId = range.getCell(1, entityIdIndex + 1).getValue();
    
    if(cardTextId) {
      Logger.log('updating %s', cardTextId);
      
      var cell = range.getCell(1, entityDisplayTextIndex + 1);
      var currentDisplayText = cell.getValue();
      
      // Construct the display text
      
      // Find the row in the CardText sheet
      var cardTextRowIndex = getRowIndexByName(cardTextSheet, cardTextId);
      Logger.log('CardTextId is row %s', cardTextId);
      var cardTextRange = cardTextSheet.getRange(cardTextRowIndex + 1, 1, 1, cardTextSheet.getMaxColumns());
      Logger.log('looking at range for  %s', cardTextRange.getCell(1, 1).getValue());
      var displayTextTemplate = cardTextRange.getCell(1, ctDisplayTextColIndex + 1).getValue();
      Logger.log('displayTextTemplate is %s', displayTextTemplate);
      
      var param1 = range.getCell(1, entityParam1Index + 1).getValue();
      Logger.log('param1 is %s', param1);
      var param2 = range.getCell(1, entityParam2Index + 1).getValue();
      Logger.log('param2 is %s', param2);
      
      var newDisplayText = generateDisplayText(displayTextTemplate, param1, param2);
      if(currentDisplayText != newDisplayText) {
        cell.setValue(newDisplayText);
        numUpdated++;
      }
    }
  }
  
  Browser.msgBox('You updated ' + numUpdated + ' CardDisplayText!');
}

function menuItem2() {
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
     .alert('You clicked the second menu item!');
}

function generateDisplayText(template, param1, param2) {
  
  var res = template.replace("<Param1>", param1);
  res = res.replace("<Param2>", param2);
  return res;
}


function getColumnIndexByName(sheet, name) {
  var range = sheet.getRange(1, 1, 1, sheet.getMaxColumns());
  var values = range.getValues();
  
  for (var row in values) {
    for (var col in values[row]) {
      //Logger.log('getColumnIndexByName [%s,%s] = %s', row, col, values[row][col]);
      if (values[row][col] == name) {
        return parseInt(col);
      }
    }
  }
  
  throw 'failed to get column by name';
}
  
function getRowIndexByName(sheet, name) {
  var range = sheet.getRange(1, 1, sheet.getMaxRows(), 1);
  var values = range.getValues();
  
  for (var row in values) {
    for (var col in values[row]) {
      //Logger.log('getRowIndexByName [%s,%s] = %s', row, col, values[row][col]);
      if (values[row][col] == name) {
        return parseInt(row);
      }
    }
  }
  
  throw 'failed to get row by name';
}