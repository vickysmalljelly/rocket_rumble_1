var searchData=
[
  ['damagemodifierepic',['DamageModifierEpic',['../class_small_jelly_1_1_ship_component_globals.html#ae878ad336732159dc8c6a4c22eccc680',1,'SmallJelly::ShipComponentGlobals']]],
  ['damagemodifierledgendary',['DamageModifierLedgendary',['../class_small_jelly_1_1_ship_component_globals.html#a26cb0c5c0b6aa40b98a9d83e490b2e57',1,'SmallJelly::ShipComponentGlobals']]],
  ['damagemodifierrare',['DamageModifierRare',['../class_small_jelly_1_1_ship_component_globals.html#aa4d5e7552af4bce690a4eb08668d2ac8',1,'SmallJelly::ShipComponentGlobals']]],
  ['data',['data',['../classmod__pbxproj_1_1_xcode_project.html#a98632d03dd4067b9b297871c68d9ad04',1,'mod_pbxproj.XcodeProject.data()'],['../class_small_jelly_1_1_card_controller.html#a7ec04250fdb252c8e97be5d34a77b5d4',1,'SmallJelly.CardController.Data()']]],
  ['debugconfig',['DebugConfig',['../class_small_jelly_1_1_game_manager.html#a743a79ab961dc0b9d7a07a68fe695ba7',1,'SmallJelly::GameManager']]],
  ['debugid',['DebugId',['../class_small_jelly_1_1_ship_component_data.html#a0687a800512cd55bc381edf334554786',1,'SmallJelly::ShipComponentData']]],
  ['defendingcard',['DefendingCard',['../class_small_jelly_1_1_combat_turn_state_machine.html#a22e4784a2d7501997c519957da172fea',1,'SmallJelly::CombatTurnStateMachine']]],
  ['defendingplayer',['DefendingPlayer',['../class_small_jelly_1_1_combat_turn_state_machine.html#a2edc7dd892b08242a89ffa11a29983c9',1,'SmallJelly::CombatTurnStateMachine']]],
  ['description',['Description',['../class_small_jelly_1_1_chance_card_data.html#a39ebfe2da0bcceb4a63c08fb0443e7b8',1,'SmallJelly.ChanceCardData.Description()'],['../class_small_jelly_1_1_ship_component_attributes.html#a028fb2c5238068057ef8b795051e707c',1,'SmallJelly.ShipComponentAttributes.Description()'],['../class_small_jelly_1_1_ship_data.html#a308fba292755649025d87b221f51cb8e',1,'SmallJelly.ShipData.Description()']]],
  ['displayname',['DisplayName',['../class_small_jelly_1_1_chance_card_data.html#add3b3416f5303b01a2ba41671a4e96b4',1,'SmallJelly.ChanceCardData.DisplayName()'],['../class_small_jelly_1_1_ship_component_attributes.html#a9cf03f3ec557851b636eebae097bc7f6',1,'SmallJelly.ShipComponentAttributes.DisplayName()'],['../class_small_jelly_1_1_ship_data.html#aa65553f37e295c608a5dcf9f3df54202',1,'SmallJelly.ShipData.DisplayName()'],['../class_small_jelly_1_1_card_front_view.html#ab6924cfa23810a21d6b58250f5b71986',1,'SmallJelly.CardFrontView.DisplayName()'],['../class_small_jelly_1_1_ship_stats_view.html#a8df8cc899355d6f667c3273e9b2139a7',1,'SmallJelly.ShipStatsView.DisplayName()']]],
  ['dontimportmaterials',['DontImportMaterials',['../class_small_jelly_1_1_s_j_asset_processor.html#aae804c4467765d589438ecdd54c00f66',1,'SmallJelly::SJAssetProcessor']]]
];
