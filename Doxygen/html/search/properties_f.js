var searchData=
[
  ['sdk',['SDK',['../class_game_sparks_1_1_platforms_1_1_platform_base.html#a60252dd4a7d5ada9c0003494db2c486c',1,'GameSparks::Platforms::PlatformBase']]],
  ['sdkversion',['SdkVersion',['../class_game_sparks_settings.html#a7a87c59d271ff2d4b986b083c1ff4bf8',1,'GameSparksSettings']]],
  ['serviceurl',['ServiceUrl',['../class_game_sparks_settings.html#a3cef76ca88d0766b114e89b31b9a86aa',1,'GameSparksSettings.ServiceUrl()'],['../class_game_sparks_1_1_platforms_1_1_platform_base.html#ae5f8eee2c887622a08fdf1a470371f2e',1,'GameSparks.Platforms.PlatformBase.ServiceUrl()']]],
  ['shipclass',['ShipClass',['../class_small_jelly_1_1_combat_player.html#a4eb58db6be6e3245faa2a5502714b537',1,'SmallJelly::CombatPlayer']]],
  ['shipcomponentglobals',['ShipComponentGlobals',['../class_small_jelly_1_1_resource_locations.html#ab9c15002fc93305dc01949bcb9cf6c2e',1,'SmallJelly::ResourceLocations']]],
  ['shipcomponentglobalscsv',['ShipComponentGlobalsCsv',['../class_small_jelly_1_1_file_locations.html#aa8370cffadcd1962401df83fb46a04bc',1,'SmallJelly::FileLocations']]],
  ['shipcomponentglobalsxml',['ShipComponentGlobalsXml',['../class_small_jelly_1_1_file_locations.html#a24e1c51a09d711cefb40598502feb54a',1,'SmallJelly::FileLocations']]],
  ['shipcomponenttemplates',['ShipComponentTemplates',['../class_small_jelly_1_1_resource_locations.html#a05b6ce50b3594d98b9a7aa736f75dbc1',1,'SmallJelly::ResourceLocations']]],
  ['shipcomponenttemplatescsv',['ShipComponentTemplatesCsv',['../class_small_jelly_1_1_file_locations.html#aaa25a86ea85ec799540b7414cc435957',1,'SmallJelly::FileLocations']]],
  ['shipcomponenttemplatesxml',['ShipComponentTemplatesXml',['../class_small_jelly_1_1_file_locations.html#addc66f0da4e8645518cc1dd9aec72d6b',1,'SmallJelly::FileLocations']]],
  ['shipname',['ShipName',['../class_small_jelly_1_1_combat_player.html#a8b4f8f13a42798ae17f80b0afc2c6dd2',1,'SmallJelly::CombatPlayer']]],
  ['ships',['Ships',['../class_small_jelly_1_1_game_manager.html#a939b0b1af128979a5c7b5b682d84d01e',1,'SmallJelly::GameManager']]],
  ['socketid',['socketId',['../interface_game_sparks_web_socket.html#af4ec91d43af16eb0b331de796a6c892a',1,'GameSparksWebSocket::socketId()'],['../interface_game_sparks_1_1_platforms_1_1_i_controlled_web_socket.html#a5c62f6993a8b19311b52de796c56dc1c',1,'GameSparks.Platforms.IControlledWebSocket.SocketId()']]],
  ['sockets',['sockets',['../interface_socket_controller.html#aabbc8bde1a76f2ce9c4f5505e90b8b0c',1,'SocketController']]],
  ['sr_5fsslpinnedcertificates',['SR_SSLPinnedCertificates',['../category_n_s_u_r_l_request_07_certificate_additions_08.html#ac203a382806012257062218cf9a079d1',1,'NSURLRequest(CertificateAdditions)::SR_SSLPinnedCertificates()'],['../category_n_s_mutable_u_r_l_request_07_certificate_additions_08.html#ae77357501a6c31aba53c8261aae37630',1,'NSMutableURLRequest(CertificateAdditions)::SR_SSLPinnedCertificates()']]]
];
