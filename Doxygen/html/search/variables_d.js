var searchData=
[
  ['name',['Name',['../class_small_jelly_1_1_framework_1_1_test_data.html#a83f428d24b4c27b5e17846676bfd290c',1,'SmallJelly.Framework.TestData.Name()'],['../namespacepost__process.html#a796533dbf5025e81c580786c843e0ab4',1,'post_process.name()']]],
  ['networkrunloop',['networkRunLoop',['../_s_r_web_socket_8m.html#ae7536de97f578fb6d0844b02b2ad977d',1,'SRWebSocket.m']]],
  ['networkthread',['networkThread',['../_s_r_web_socket_8m.html#a701afb0fdd480ddb9a850444875a2378',1,'SRWebSocket.m']]],
  ['newgameclicked',['NewGameClicked',['../class_small_jelly_1_1_main_menu_controller.html#a81e6108c2a6782a5e2fae44452bc0845',1,'SmallJelly::MainMenuController']]],
  ['nextplayerid',['NextPlayerId',['../class_small_jelly_1_1_framework_1_1_challenge_manager.html#a8a267af36b5f170009302f792ac5d8c9',1,'SmallJelly::Framework::ChallengeManager']]],
  ['nextstateid',['NextStateId',['../class_small_jelly_1_1_combat_fx_state.html#ac6f0f8e2f6e7eb8115a227d9e455dc7e',1,'SmallJelly::CombatFxState']]],
  ['numericalproperties',['NumericalProperties',['../class_small_jelly_1_1_framework_1_1_base_data.html#a01a8e835d097314d98975eb5dae75c77',1,'SmallJelly::Framework::BaseData']]]
];
