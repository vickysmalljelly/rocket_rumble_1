var searchData=
[
  ['deactivateonawake',['DeactivateOnAwake',['../class_small_jelly_1_1_framework_1_1_deactivate_on_awake.html',1,'SmallJelly::Framework']]],
  ['debugconfig',['DebugConfig',['../class_debug_config.html',1,'']]],
  ['defaultplatform',['DefaultPlatform',['../class_game_sparks_1_1_platforms_1_1_default_platform.html',1,'GameSparks::Platforms']]],
  ['dialogmanager',['DialogManager',['../class_small_jelly_1_1_framework_1_1_dialog_manager.html',1,'SmallJelly::Framework']]],
  ['dontdestroyonload',['DontDestroyOnLoad',['../class_small_jelly_1_1_framework_1_1_dont_destroy_on_load.html',1,'SmallJelly::Framework']]],
  ['doxywindow',['DoxyWindow',['../class_small_jelly_1_1_framework_1_1_doxy_window.html',1,'SmallJelly::Framework']]]
];
