var searchData=
[
  ['servererrortype',['ServerErrorType',['../namespace_small_jelly_1_1_framework.html#a6e69cffc38dbeeef70285f89955aee51',1,'SmallJelly::Framework']]],
  ['shipclass',['ShipClass',['../class_small_jelly_1_1_ship_data.html#ad7874dc052b969f8129f4bf62f4387f6',1,'SmallJelly::ShipData']]],
  ['shipcomponenttype',['ShipComponentType',['../class_small_jelly_1_1_ship_component_attributes.html#adab5767b4c839e85448abb74288623c2',1,'SmallJelly::ShipComponentAttributes']]],
  ['sropcode',['SROpCode',['../_s_r_web_socket_8m.html#a48136badb4f7e8cef51c961ffbfaa4e1',1,'SRWebSocket.m']]],
  ['srreadystate',['SRReadyState',['../_s_r_web_socket_8h.html#ab0963b26e8f8afb26080a2a5018cf95c',1,'SRWebSocket.h']]],
  ['srstatuscode',['SRStatusCode',['../_s_r_web_socket_8h.html#aeca33f71811a72832fd36ab2ebbd9cc3',1,'SRWebSocket.h']]],
  ['status',['Status',['../class_small_jelly_1_1_framework_1_1_xml_serialization.html#adc37f901b290be7cbc8627e37aaddc0a',1,'SmallJelly::Framework::XmlSerialization']]]
];
