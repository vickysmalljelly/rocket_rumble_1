var searchData=
[
  ['rarity',['Rarity',['../class_small_jelly_1_1_ship_component_data.html#a962a7a0dda7678315cddef3c7aecb00e',1,'SmallJelly::ShipComponentData']]],
  ['readtocurrentframe',['readToCurrentFrame',['../interface_s_r_i_o_consumer.html#ae5328f4738b13514641a068913688aec',1,'SRIOConsumer']]],
  ['readystate',['readyState',['../interface_s_r_web_socket.html#a76257bf57775f013d43cae26f1db2239',1,'SRWebSocket::readyState()'],['../category_s_r_web_socket_07_08.html#a4a139d2adb53cbbbf78cc8d60501cbc0',1,'SRWebSocket()::readyState()']]],
  ['reason',['Reason',['../class_small_jelly_1_1_combat_turn_state_check.html#ab30324b981fb4e855d23443da9a1f33a',1,'SmallJelly::CombatTurnStateCheck']]],
  ['receivingplayer',['ReceivingPlayer',['../class_small_jelly_1_1_combat_fx.html#aeb81e932db20816d1a6c52045725519d',1,'SmallJelly::CombatFx']]],
  ['requesttimeoutseconds',['RequestTimeoutSeconds',['../class_game_sparks_1_1_platforms_1_1_platform_base.html#a8970df32dc9990687dd0e7b7fba9d8ec',1,'GameSparks::Platforms::PlatformBase']]],
  ['runloop',['runLoop',['../interface___s_r_run_loop_thread.html#a76fc0ceee483b6a6d2041f93ec287c62',1,'_SRRunLoopThread']]]
];
