var searchData=
[
  ['handlenewgameclicked',['HandleNewGameClicked',['../class_small_jelly_1_1_main_menu_controller.html#ab17eb73426ced0f11e532a1cba2f728f',1,'SmallJelly::MainMenuController']]],
  ['handleplaybuttonclicked',['HandlePlayButtonClicked',['../class_small_jelly_1_1_combat_turn_state_select_card.html#acf0e4e563b3357a8181bf7cc3501f678',1,'SmallJelly::CombatTurnStateSelectCard']]],
  ['has_5fbuild_5ffile',['has_build_file',['../classmod__pbxproj_1_1_p_b_x_build_phase.html#ae8c7576040bbaa86857f49e9875a4280',1,'mod_pbxproj::PBXBuildPhase']]],
  ['has_5fchild',['has_child',['../classmod__pbxproj_1_1_p_b_x_group.html#a1d620acd2ec7cb2bb86a0a81617e35e7',1,'mod_pbxproj::PBXGroup']]],
  ['hide',['Hide',['../class_small_jelly_1_1_card_controller.html#a2b98c78f7b02b578b6c48358b57aa508',1,'SmallJelly::CardController']]],
  ['hideendturnbutton',['HideEndTurnButton',['../class_small_jelly_1_1_combat_menu_controller.html#a733d1dcf2aeffbcf6daf2890e0162b60',1,'SmallJelly::CombatMenuController']]],
  ['hidemenu',['HideMenu',['../class_small_jelly_1_1_framework_1_1_u_i_manager.html#abee50a634d67ce235c8da6366db84a49',1,'SmallJelly::Framework::UIManager']]],
  ['hidespinner',['HideSpinner',['../class_small_jelly_1_1_framework_1_1_dialog_manager.html#a6a409b87071cd548d67e5ec3f391f292',1,'SmallJelly::Framework::DialogManager']]],
  ['hidespinnerwithmessage',['HideSpinnerWithMessage',['../class_small_jelly_1_1_framework_1_1_dialog_manager.html#a68a3912e9de18a02be42827ce093244a',1,'SmallJelly::Framework::DialogManager']]]
];
